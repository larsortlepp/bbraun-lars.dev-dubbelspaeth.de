## DSS Module print-manager

## Version ##
 * Module Version: 1.0.0

## Requirements ##
 * DSS: 3.1.0
 * PrintHtmlTemplatePage
 * dompdf library
 * Silverstripe: 3.1

## Description ##
Adds a "Druck Manager" tab to the CMS where we can save multiple print templates with custom FoodCategory or Headline, Dishes and FootNote that can then be printed as custom PDFs.
With the print manager we are independet of the DailyMenu page and can assign any Dish from the database of the selected RestaurantPage - independet of DailyMenuPage and FoodCategory.

The template setup itself is saved as PrintManagerConfig. The config is always assigned to a RestaurantPage. Only RestaurantPages that the user has canView() permissions to are selectable.
Furthermore we have canCreate/View/Edit/Delete permission on the objects that can be assigned via roles or groups.

A PrintManagerConfig can have one PrintManagerTemplate, that defines the Theme and TemplateName and PDF dimensions. 
These templates are specific to a theme and can only be assigned to PrintManagerConfigs who use the same theme (assigned via the Subsites SiteConfig of the selected RestaurantPage).

The PDFs are created over the PrintManagerController which is accessible from the frontend via mysite.com/printmanager/... (assigned via static routes).
There is a "/pdf" and "/renderhtml" url controller that will create the pdf / render the html template necessary for creating the pdf.
The "/renderhtml" can´t be protected (against unauthorized public access) using permission checks, because the "/pdf" controller calling the function is anonymous.
That´s why we use an "AuthKey" that is submitted via url parameter for the function (mysite.com/printmanager/html/1?k=1234567890987654321abcdef) to do a permission check.

## Release Notes ##
