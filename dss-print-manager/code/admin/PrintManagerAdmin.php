<?php

/**
 * Class to manage PrintManagerConfig and PrintManagerTemplate objects
 * 
 * @requires mysite/code/interfaces/ControllerRestaurantPageRelation.php
 */

class PrintManagerAdmin extends ModelAdmin implements ControllerRestaurantPageRelation {
	
	public $RestaurantPageID = null;

	public static $managed_models = array(
		'PrintManagerConfig',
		'PrintManagerTemplate'
	);

	private static $url_segment = 'printmanager'; // will be linked as /admin/products
	private static $menu_title = 'Druck Manager';
	
	private static $menu_icon = 'dss-print-manager/images/menu-icons/print.png';

	public $showImportForm = false;
	

	/**
	 * Customize behaviour of GridField
	 */
	public function getEditForm($id = null, $fields = null) {
		// set vars for storing RestaurantPageID based on current URL and model
		$this->set_vars();
		
		$form = parent::getEditForm($id, $fields);
		
		$model = singleton($this->modelClass);
		$gridField = $form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass));
		if($gridField instanceof GridField) {
			$gfConfig = $gridField->getConfig();

			// remove Export and Print Button
			$gfConfig->removeComponentsByType('GridFieldExportButton');
			$gfConfig->removeComponentsByType('GridFieldPrintButton');

			// Add sorting if we have a field for it
			// set page length to 9999 to always show all records
			// because we can´t sort across paginated pages
			if(
				class_exists('GridFieldOrderableRows')
				&& $model->hasField('Sort')
			) {
				$gfConfig->addComponent(new GridFieldOrderableRows('Sort'));
				$gfConfig->getComponentByType('GridFieldPaginator')->setItemsPerPage(9999);
			}
		}
		
		return $form;
	}
	
	/* DishRatings are subsite-aware, so we can show the menu option in subsites, too */
	public function subsiteCMSShowInMenu(){
		return $this->ModuleIsEnabled();
	}
	
	/**
	 * Returns current module status for Subsite: true if enabled, false if diabled
	 * 
	 * @return boolean Note: Wen need to return an explicit false (not 0 or anything else)
	 */
	public static function ModuleIsEnabled() {
		$config = SiteConfig::current_site_config();
		if(!$config) return true;
		return $config->EnablePrintManager ? true : false;
	}
	
	/**
	 * Alternate access checks that are included run in LeftAndMain::canView()
	 * 
	 * @return boolean
	 */
	public function alternateAccessCheck() {
		return $this->ModuleIsEnabled();
	}
	
	/**
	 * Extension of original ModelAdmin::getManagedModels()
	 * Implements permission checks on managed models and only shows models
	 * that the member is allowed to view / edit based on PermissionProvider.
	 * 
	 * The Model must implement class PermissionProvider and
	 * have a canView() and canEdit() function.
	 * 
	 * If that is not the case, the model will not be touched and stay in the $models array.
	 * 
	 * @todo When $models is an empty array we always get an error "Invalid Model Class"
	 *       instead of being redirected to /admin/pages
	 * 
	 * @return array $models Map of class name to an array of 'title' (see {@link $managed_models})
	 */
	public function getManagedModels() {
		$models = parent::getManagedModels();
		
		// remove model if it implements PermissionProvider and member has no permission set
		// only check if member is logged in - otherwise we don´t need to do permission checks
		// as user will be redirected to login by LeftAndMain::init()
		if(Member::logged_in_session_exists()) {
			foreach($models as $classname => $title) {
				// get all interfaces of current class (implements)
				$interfaces = class_implements($classname);
				if(
					isset($interfaces['PermissionProvider']) &&
					method_exists($classname, 'canView') && method_exists($classname, 'canEdit') &&
					(!singleton($classname)->canView() && !singleton($classname)->canEdit())
				) {
					unset($models[$classname]);
				}
			}
		}
		
		return $models;
	}
	
	// INTERFACE ControllerRestaurantPageRelation
	
	/**
	 * Set variables on current Object
	 * This function should call $this->setRestaurantPageID()
	 * and all other functions that might be needed additionaly.
	 * 
	 * $this->set_vars() should then be called in (based on Class):
	 * - Controller: handleAction()
	 * - ModelAdmin: getEditForm()
	 */
	public function set_vars() {
		$this->setRestaurantPageID();
	}
	
	/**
	 * Assign RestaurantPageID dynamically based on curren 'url' request parameter
	 */
	public function setRestaurantPageID() {
		$restaurantpage = false;
		// extract DataObject ID from URL 
		// (as there is no function in Silverstripe that can get us the ID directly)
		// e.g.: /admin/printmanager/PrintManagerConfig/EditForm/field/PrintManagerConfig/item/3/edit
		$modelclass = Convert::raw2sql($this->request->param('ModelClass'));
		$url = Convert::raw2sql($this->requestParams['url']);
		if(preg_match('/\/EditForm\/field\/'.$modelclass.'\/item\/(\d*)/', $url, $matches)) {
			$modelID = Convert::raw2sql($matches['1']);
			$model = $modelclass::get()->byID($modelID);
			if($model && $model->RestaurantPageID) {
				$restaurantpage = Page::get()->byID($model->RestaurantPageID);
			}
		}
		$this->RestaurantPageID = $restaurantpage ? $restaurantpage->ID : null;
	}
	
	/**
	 * Returns the RestaurantPage based on $this->RestaurantPageID
	 * The function should contain:
	 * return RestaurantPage::get()->byID($this->RestaurantPageID);
	 * 
	 * @return RestaurantPage
	 */
	public function RestaurantPage() {
		return RestaurantPage::get()->byID($this->RestaurantPageID);
	}
}
