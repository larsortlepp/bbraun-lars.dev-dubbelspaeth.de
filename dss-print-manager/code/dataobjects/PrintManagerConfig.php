<?php

/**
 * Configuration for saving a custom print setup consisting of:
 * - PrintManagerTemplate
 * - RestaurantPage (defines which Dishes we can access)
 * - Dishes
 * - HeadLine (printed at the top of the PDF)
 * - FootNote (printed at the bottom of the PDF)
 * 
 * Any Dishes of the selected RestaurantPage 
 * (independent of their FoodCategory or any DailymenuPage)
 * can be assigned in any order.
 * 
 * The Dishes can then be printed as PDF via the print button.
 */
class PrintManagerConfig extends DataObject implements PermissionProvider {
	
	static $singular_name = "Druckvorlage";
	static $plural_name = "Druckvorlagen";
	
	private static $db = array (
        'Title' => 'Varchar(255)',
		'Headline' => 'Text',
		'Infotext' => 'HTMLText',
		'FootNote' => 'Text',
		'AuthKey' => 'Varchar(25)'
    );
	
	private static $has_one = array(
		'RestaurantPage' => 'RestaurantPage',
		'FoodCategory' => 'FoodCategory',
		'PrintManagerTemplate' => 'PrintManagerTemplate'
	);
	
	private static $many_many = array(
		'Dishes' => 'Dish'
	);
	
	private static $many_many_extraFields = array(
		'Dishes' => array(
			'SortOrder' => 'Int'
		)
	);
	
	private static $default_sort = array(
		'Title'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'Headline' => 'Überschrift',
		'HeadlineCMS' => 'Überschrift',
		'Infotext' => 'Infotext',
		'PrintManagerTemplate' => 'Template',
		'PrintManagerTemplate.Title' => 'Template',
		'FootNote' => 'Fußzeile',
		'RestaurantPage' => 'Restaurant',
		'RestaurantPage.Title' => 'Restaurant',
	);
	
	private static $summary_fields = array(
		'RestaurantPage.Title',
		'Title',
		'HeadlineCMS',
		'PrintManagerTemplate.Title'
	);
	
	private static $searchable_fields = array(
		'Title',
		'PrintManagerTemplate.Title',
		'RestaurantPage.Title'
	);
	
	public function getCMSFields() {
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		// add print link if the method exists
		if($this->PrintManagerTemplate()->validSettings()) {
			$ctrlr = Controller::curr();
			$f->addFieldToTab(
				'Root.Main',
				LiteralField::create(
					'PrintLink', 
					'<a href="'.Controller::join_links(Director::protocolAndHost(), PrintManagerController::$url_segment, 'pdf', $this->ID).'?k='.$this->AuthKey.'" target="_blank" class="custom-button icon-doc-3">PDF drucken</a>'
				)
			);
		} else {
			$f->addFieldToTab(
				'Root.Main',
				LiteralField::create(
					'ControllerWarning',
					'<div class="message error">Das verwendete Template ist fehlerhaft konfiguriert.<br /> Es kann kein Ausdruck erstellt werden.</div>'
				)
			);
		}
		
		$f->addFieldToTab(
			'Root.Main', 
			TextField::create(
				'Title',
				PrintManagerConfig::$field_labels['Title']
			)
				->setDescription('Beschreibung der Druckvorlage')
		);
		
		// Dropdown of all RestaurantPages the user can access
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'RestaurantPageID',
				PrintManagerConfig::$field_labels['RestaurantPage'],
				RestaurantPage::AllowedRestaurantPages()->map()
			)
				->setEmptyString('Bitte auswählen')
		);
		
		// Dropdown of all PrintManagerTemplates that have the same Theme as the current SiteConfig
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'PrintManagerTemplateID',
				PrintManagerConfig::$field_labels['PrintManagerTemplate'],
				PrintManagerTemplate::AllowedPrintManagerTemplates()->map()
			)
				->setEmptyString('Bitte auswählen')
		);
		
		// Add FoodCategory and Dishes GridField if we have already assigned a RestaurantPageID
		if(!$this->RestaurantPageID) {
			$f->addFieldToTab(
				'Root.Main', 
				LiteralField::create(
					'DishesMessage',
					'<div class="message notice">Bitte zunächst ein Restaurant zuordnen und Druckvorlage Speichern, bevor Essenskategorie und Gerichte hinzugefügt werden können.</div>'
				)
			);
		}
		else {
			
			// Dropdown of all FoodCategories of the current RestaurantPage
			$f->addFieldToTab(
				'Root.Main', 
				DropdownField::create(
					'FoodCategoryID',
					'Essenskategorie',
					FoodCategory::get()->filter(array('RestaurantPageID' => $this->RestaurantPageID))->map()
				)
					->setEmptyString('Bitte auswählen')
			);

			$f->addFieldToTab(
				'Root.Main', 
				TextareaField::create(
					'Headline',
					PrintManagerConfig::$field_labels['Headline']
				)
					->setDescription('Optional. Erscheint anstatt der Essenskategorie als Überschrift am Beginn jeder Seite')
			);
			
			$query = $this->DishesGridFieldQuery();
			MyGridField::create('Dishes', $this->DishesGridFieldTitle(), $query, 'relation')
				->setCols($this->DishesGridFieldCols())
				->setRowsPerPage($this->DishesGridFieldRowsPerPage())
				->setAutoCompleterPlaceholderText($this->DishesGridFieldAutoCompleterPlaceholderText())
				->setAutoCompleterSearchList($this->DishesGridFieldAutoCompleterSearchList())
				->setAutoCompleterSearchCols($this->DishesGridFieldAutoCompleterSearchCols())
				->setAutoCompleterResultsFormat($this->DishesGridFieldAutoCompleterResultFormat())
				->setAutoCompleterOnAfterAdd($this->DishesGridFieldAutoCompleterOnAfterAdd())
				->setAllowCustomSort($this->DishesGridFieldAllowCustomSort())
				->setExtraSortCols($this->DishesGridFieldExtraSortCols())
				->setReorderColumnNumber($this->DishesGridFieldReorderColumnNumber())
				->addToTab($f, 'Main');
			$dishesGF = $f->fieldByName('Root.Main.Dishes');
			$dishesGFConfig = $dishesGF->getConfig();
			$dishesGFConfig->removeComponentsByType('GridFieldExportButton');
			$dishesGFConfig->removeComponentsByType('GridFieldPrintButton');
			
			// Remove "Add new Dish" button if member has no permission
			if(!singleton('Dish')->canCreate()) $dishesGFConfig->removeComponentsByType('GridFieldAddNewButton');
		}
		
		$f->addFieldToTab(
			'Root.Main', 
			HTMLEditorField::create(
				'Infotext',
				PrintManagerConfig::$field_labels['Infotext']
			)
				->setDescription('Optional. Wird abhängig vom verwendeten Template ausgegeben.')
		);
		
		$f->addFieldToTab(
			'Root.Main', 
			TextareaField::create(
				'FootNote',
				PrintManagerConfig::$field_labels['FootNote']
			)
				->setDescription('Fußzeile erscheint am Ende jeder Seite')
		);
		
		// set fields to read only if user can not view / edit them
		// show info message
		if($this->RestaurantPage()->can("Edit"))
		
		return $f;	
	}
	
	/**
	 * Workaround to get the assigned RestaurantPage in templates
	 * For some unknown reason $RestaurantPage in templates returns null
	 * (which calls the corresponding getRestaurantPage() function for the has_one relation)
	 * But if we define the function manually we cann call it from the templates.
	 * 
	 * @return RestaurantPage
	 */
	public function getRestaurantPage() {
		return RestaurantPage::get()->byID($this->RestaurantPageID);
	}
	
	/**
	 * Returns the title of the assigned FoodCategory or (if this is not set)
	 * the Headline value.
	 * This is used for display in CMS GridField.
	 * 
	 * @return string
	 */
	public function HeadlineCMS() {
		$fc = $this->FoodCategory();
		if ($this->Headline) {
			return $this->Headline;
		}
		else if ($fc && $fc->exists()) {
			return $fc->Title;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Returns (Global|Subsite)Additives that are used by assigned Dishes
	 *
	 * GlobalAdditives come first followed by SubsiteAdditives, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function UsedAdditives($includeSideDishes = false) {
		$usedGlobalAdditives = new ArrayList();
		$usedSubsiteAdditives = new ArrayList();

		foreach ($this->Dishes() as $dish) {
			/* Get each dish's GlobalAdditives */
			$globalAdditives = $dish->GlobalAdditives();
			if ($globalAdditives) {
				foreach ($globalAdditives as $additive) {
					if (!$usedGlobalAdditives->find('ID', $additive->ID)) {
						$usedGlobalAdditives->push($additive);
					}
				}
			}
			// Get each SideDish´s GlobalAdditives */
			if($includeSideDishes) {
				foreach($dish->AllSideDishes() as $sideDish) {
					$sideDishGlobalAdditives = $sideDish->GlobalAdditives();
					if ($sideDishGlobalAdditives) {
						foreach ($sideDishGlobalAdditives as $additive) {
							if (!$usedGlobalAdditives->find('ID', $additive->ID)) {
								$usedGlobalAdditives->push($additive);
							}
						}
					}
				}
			}
			$usedGlobalAdditives = $usedGlobalAdditives->sort('SortOrder');

			/* Get each dish's SubsiteAdditives */
			$subsiteAdditives = $dish->SubsiteAdditives();
			if ($subsiteAdditives) {
				foreach ($subsiteAdditives as $additive) {
					if (!$usedSubsiteAdditives->find('ID', $additive->ID)) {
						$usedSubsiteAdditives->push($additive);
					}
				}
			}
			// Get each SideDish´s SubsiteAdditives */
			if($includeSideDishes) {
				foreach($dish->AllSideDishes() as $sideDish) {
					$sideDishSubsiteAdditives = $sideDish->SubsiteAdditives();
					if ($sideDishSubsiteAdditives) {
						foreach ($sideDishSubsiteAdditives as $additive) {
							if (!$usedSubsiteAdditives->find('ID', $additive->ID)) {
								$usedSubsiteAdditives->push($additive);
							}
						}
					}
				}
			}
			$usedSubsiteAdditives = $usedSubsiteAdditives->sort('SortOrder');
		}

		return MyListUtil::concatenateLists($usedGlobalAdditives, $usedSubsiteAdditives);
	}
	
	/**
	 * Returns (Global|Subsite)Allergens that are used by assigned Dishes
	 *
	 * GlobalAllergens come first followed by SubsiteAllergens, each of them
	 * in turn sorted by their defined SortOrder.
	 * 
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function UsedAllergens($includeSideDishes = false) {
		$usedGlobalAllergens = new ArrayList();
		$usedSubsiteAllergens = new ArrayList();
		
		foreach ($this->Dishes() as $dish) {
			/* Get each dish's GlobalAllergens */
			$globalAllergens = $dish->GlobalAllergens();
			if ($globalAllergens) {
				foreach ($globalAllergens as $allergen) {
					if (!$usedGlobalAllergens->find('ID', $allergen->ID)) {
						$usedGlobalAllergens->push($allergen);
					}
				}
			}
			// Get each SideDish´s GlobalAllergens */
			if($includeSideDishes) {
				foreach($dish->AllSideDishes() as $sideDish) {
					$sideDishGlobalAllergens = $sideDish->GlobalAllergens();
					if ($sideDishGlobalAllergens) {
						foreach ($sideDishGlobalAllergens as $allergen) {
							if (!$usedGlobalAllergens->find('ID', $allergen->ID)) {
								$usedGlobalAllergens->push($allergen);
							}
						}
					}
				}
			}
			$usedGlobalAllergens = $usedGlobalAllergens->sort('SortOrder');

			/* Get each dish's SubsiteAllergens */
			$subsiteAllergens = $dish->SubsiteAllergens();
			if ($subsiteAllergens) {
				foreach ($subsiteAllergens as $allergen) {
					if (!$usedSubsiteAllergens->find('ID', $allergen->ID)) {
						$usedSubsiteAllergens->push($allergen);
					}
				}
			}
			// Get each SideDish´s SubsiteAdditives */
			if($includeSideDishes) {
				foreach($dish->AllSideDishes() as $sideDish) {
					$sideDishSubsiteAllergens = $sideDish->SubsiteAllergens();
					if ($sideDishSubsiteAllergens) {
						foreach ($sideDishSubsiteAllergens as $allergen) {
							if (!$usedSubsiteAllergens->find('ID', $allergen->ID)) {
								$usedSubsiteAllergens->push($allergen);
							}
						}
					}
				}
			}
			$usedSubsiteAllergens = $usedSubsiteAllergens->sort('SortOrder');
		}
		
		return MyListUtil::concatenateLists($usedGlobalAllergens, $usedSubsiteAllergens);
	}
	
	/**
	 * Returns (Global|Subsite)DishLabels that are used by assigned Dishes
	 *
	 * GlobalDishLabels come first followed by SubsiteDishLabels, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function UsedDishLabels($includeSideDishes = false) {
		$usedGlobalDishLabels = new ArrayList();
		$usedSubsiteDishLabels = new ArrayList();
		
		foreach ($this->Dishes() as $dish) {
			/* Get each dish's GlobalDishLabels */
			$globalDishLabels = $dish->GlobalDishLabels();
			if ($globalDishLabels) {
				foreach ($globalDishLabels as $dishlabel) {
					if (!$usedGlobalDishLabels->find('ID', $dishlabel->ID)) {
						$usedGlobalDishLabels->push($dishlabel);
					}
				}
			}
			// Get each SideDish´s GlobalDishLabels */
			if($includeSideDishes) {
				foreach($dish->AllSideDishes() as $sideDish) {
					$sideDishGlobalDishLabels = $sideDish->GlobalDishLabels();
					if ($sideDishGlobalDishLabels) {
						foreach ($sideDishGlobalDishLabels as $dishlabel) {
							if (!$usedGlobalDishLabels->find('ID', $dishlabel->ID)) {
								$usedGlobalDishLabels->push($dishlabel);
							}
						}
					}
				}
			}
			$usedGlobalDishLabels = $usedGlobalDishLabels->sort('SortOrder');

			/* Get each dish's SubsiteDishLabels */
			$subsiteDishLabels = $dish->SubsiteDishLabels();
			if ($subsiteDishLabels) {
				foreach ($subsiteDishLabels as $dishlabel) {
					if (!$usedSubsiteDishLabels->find('ID', $dishlabel->ID)) {
						$usedSubsiteDishLabels->push($dishlabel);
					}
				}
			}
			// Get each SideDish´s SubsiteDishLabels */
			if($includeSideDishes) {
				foreach($dish->AllSideDishes() as $sideDish) {
					$sideDishSubsiteDishLabels = $sideDish->SubsiteDishLabels();
					if ($sideDishSubsiteDishLabels) {
						foreach ($sideDishSubsiteDishLabels as $dishlabel) {
							if (!$usedSubsiteDishLabels->find('ID', $dishlabel->ID)) {
								$usedSubsiteDishLabels->push($dishlabel);
							}
						}
					}
				}
			}
			$usedSubsiteDishLabels = $usedSubsiteDishLabels->sort('SortOrder');
		}

		return MyListUtil::concatenateLists($usedGlobalDishLabels, $usedSubsiteDishLabels);
	}
	
	/**
	 * Functions that modify 'Dishes' GridField behaviour
	 * (can be extended by 'updateDishesGridField...()' functions in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateDishesGridFieldAutoCompleterResultFormat(String &$string) {
	 *     $string = '$Description.RAW ($ID)';
	 * }
	 * 
	 */
	public function DishesGridFieldQuery() {
		$query = $this->Dishes()
			          ->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			          ->leftJoin('FoodCategory', 'FoodCategory.ID=RestaurantDish.FoodCategoryID')
			          ->where('RestaurantDish.RestaurantPageID = '.$this->RestaurantPageID);
		$this->extend('updateDishesGridFieldQuery', $query);
		return $query;
	}
	
	public function DishesGridFieldTitle() {
		$string = 'Zugeordnete Gerichte';
		$this->extend('updateDishesGridFieldTitle', $string);
		return $string;
	}
	
	public function DishesGridFieldCols() {
		$cols = array(
			'Image_CMSThumbnail' => 'Foto',
			'DescriptionGridField' => 'Gericht',
			'PriceNice' => 'Preis',
			'PriceExternNice' => 'Preis extern',
			'DishLabelImagesCMS' => 'Kennzeichnungen'
		);
		// Show Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->ShowDishImportID) {
			$cols = array_slice($cols, 0, 1, true) +
				array('ImportID' => 'ID') +
				array_slice($cols, 1, count($cols) - 1, true);
		}
		$this->extend('updateDishesGridFieldCols', $cols);
		return $cols;
	}
	
	public function DishesGridFieldRowsPerPage() {
		$integer = 99999;
		$this->extend('updateDishesGridFieldRowsPerPage', $integer);
		return $integer;
	}
	
	public function DishesGridFieldAutoCompleterResultFormat() {
		$string = '$Description.RAW';
		// Allow searching for Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->EnableDishSearchForImportID) $string = '$ImportIDForSearch '.$string;
		$this->extend('updateDishesGridFieldAutoCompleterResultFormat', $string);
		return $string;
	}
	
	public function DishesGridFieldAutoCompleterPlaceholderText() {
		$postfix = 'eines vorhandenen Gerichts';
		$string = 'Name';
		// Allow searching for Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->EnableDishSearchForImportID) $string .= ' oder ID';
		$string .= ' '.$postfix;
		$this->extend('updateDishesGridFieldAutoCompleterPlaceholderText', $string);
		return $string;
	}
	
	public function DishesGridFieldAutoCompleterSearchList() {
		$list = Dish::get()->filter('Dish.SubsiteID', $this->owner->SubsiteID);
		$this->extend('updateDishesGridFieldAutoCompleterSearchList', $list);
		return $list;
	}
	
	public function DishesGridFieldAutoCompleterSearchCols() {
		$cols = array(
			'Description:PartialMatch', 
			'Description_en_US:PartialMatch'
		);
		// Add filter based on ImportID
		if(SiteConfig::current_site_config()->EnableDishSearchForImportID) array_push($cols, 'ImportID:ExactMatch');
		$this->extend('updateDishesGridFieldAutoCompleterSearchCols', $cols);
		return $cols;
	}
	
	public function DishesGridFieldAutoCompleterOnAfterAdd() {
		// Get ID and object of Dish that got added
		$func = function($gridField, $dataList, $addRelation) {
			// Get ID and object of Dish that got added
			$objectID = Convert::raw2sql($addRelation);
			if ($objectID && $dish = Dish::get()->byId($objectID)) {
				
				// add next highest SortOrder
				$maxSort = DB::query("SELECT MAX(SortOrder) FROM PrintManagerConfig_Dishes WHERE PrintManagerConfigID = $this->ID")->value();
				$this->Dishes()->add($dish, array('SortOrder' => ($maxSort+1))); // add next highest SortOrder to many_many_extrafields by default
				
				// Check if associated RestaurantDish exists
				$restaurantDish = RestaurantDish::get()
					->filter(array(
						'DishID:ExactMatch' => $objectID,
						'RestaurantPageID:ExactMatch' => $this->RestaurantPageID
					))
					->limit(1)
					->first();
				if(!$restaurantDish || !$restaurantDish->DishID) {
					// Not yet, so go create one...
					$restaurantDish = new RestaurantDish();
					$restaurantDish->DishID = $objectID;
					$restaurantDish->write();

					// ...and redirect to immediately edit it
					$link = Controller::join_links($gridField->Link('item'), $objectID, 'edit');
					Controller::curr()->redirect($link);
				}
			}
			return $dataList;
		};
		$this->extend('updateDishesGridFieldAutoCompleterOnAfterAdd', $func);
		return $func;
	}
	
	public function DishesGridFieldAllowCustomSort() {
		$boolean = true;
		$this->extend('updateDishesGridFieldAllowCustomSort', $boolean);
		return $boolean;
	}
	
	public function DishesGridFieldExtraSortCols() {
		$string = null;
		$this->extend('updateDishesGridFieldExtraSortCols', $string);
		return $string;
	}
	
	public function DishesGridFieldReorderColumnNumber() {
		$integer = 0;
		$this->extend('updateDishesGridFieldReorderColumnNumber', $integer);
		return $integer;
	}
	
	public function onBeforeWrite() {
		parent::onBeforeWrite();
		
		// if the object hasn´t been written yet, or we have no AuthKey
		// create a random AuthKey and save it to the object
		// The AuthKey will be used when calling the HTML template on the frontend:
		// the template will only be returned if the correct AuthKey for the
		// PrintManagerConfig is provided
		if(!$this->ID || !$this->AuthKey) {
			$this->AuthKey = bin2hex(mcrypt_create_iv(25, MCRYPT_DEV_URANDOM));
		}
	}
	
	
	 // PERMISSIONS
	public function canCreate($member = null) {
        return Permission::check('CREATE_PRINTMANAGERCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
		// show only PrintManagerConfigs that are related to RestaurantPages the user has access to
		$canEditRestaurantPage = $this->RestaurantPage()->can('Edit');
        return Permission::check('VIEW_PRINTMANAGERCONFIG', 'any', $member) && $canEditRestaurantPage;
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_PRINTMANAGERCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_PRINTMANAGERCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_PRINTMANAGERCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" erstellen',
				'category' => 'Modul "Druck Manager"',
				'sort' => 10
			),
			"VIEW_PRINTMANAGERCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" betrachten',
				'category' => 'Modul "Druck Manager"',
				'sort' => 20
			),
			"EDIT_PRINTMANAGERCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Modul "Druck Manager"',
				'sort' => 30
			),
			"DELETE_PRINTMANAGERCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" löschen',
				'category' => 'Modul "Druck Manager"',
				'sort' => 40
			)
		);
	}
}
