<?php

/**
 * Defines the settings for PDF creation on a per-theme basis
 * (Settings such as Theme, TemplateName, DocumentWidth and DocumentHeight)
 * 
 * A PrintManagerTemplate will be selectable on each PrintManagerConfig,
 * that is assigned to a RestaurantPage whose Subsite uses the same Theme
 * as the PrintManagerTemplate.
 * 
 * The TemplateName must exist as a template file under the given Theme
 * in the first level of the 'templates' folder.
 * Example:
 * Theme = 'default-theme'
 * TemplateName = 'PrintManager_Category_DinA4'
 * The template file must be: /themes/default-theme/templates/PrintManager_Category_DinA4.ss
 */
class PrintManagerTemplate extends DataObject implements PermissionProvider {
	
	static $singular_name = "Template";
	static $plural_name = "Templates";
	
	private static $db = array (
        'Title' => 'Varchar(255)',
		'TemplateName' => 'Varchar(255)',
		'Theme' => 'Varchar(255)',
		'DocumentWidth' => 'Int',
		'DocumentHeight' => 'Int',
		'SizeUnit' => "Enum('mm,px','mm')",
		'Sort' => 'Int'
    );
	
	private static $has_many = array(
		'PrintManagerConfigs' => 'PrintManagerConfig'
	);
	
	private static $default_sort = array(
		'Sort'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'TemplateName' => 'Template Name',
		'Theme' => 'Theme',
		'Dimensions' => 'Abmessungen',
		'Status' => 'Status'
	);
	
	private static $summary_fields = array(
		'Title',
		'TemplateName',
		'Theme',
		'Dimensions',
		'Status'
	);

	private static $searchable_fields = array(
		'Title',
		'TemplateName',
		'Theme'
	);

	public function getCMSFields() {
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		if(!$this->validSettings()) {
			$f->addFieldToTab(
				'Root.Main',
				LiteralField::create(
					'WarningInvalidSettings',
					'<div class="message error"><b>Die Template Einstellungen sind unvollständig.</b><br />Bitte füllen Sie alle Felder aus damit das Template verwendet werden kann.</div>'
				)
			);
		}
		
		if(!$this->validTemplateName()) {
			$f->addFieldToTab(
				'Root.Main',
				LiteralField::create(
					'WarningInvalidTemplateName',
					'<div class="message error"><b>Das angegebene Template existiert nicht im Theme oder wurde noch nicht gespeichert.</b></div>'
				)
			);
		}
		
		$f->addFieldToTab(
			'Root.Main', 
			TextField::create(
				'Title',
				PrintManagerTemplate::$field_labels['Title']
			)
				->setDescription('Beschreibung des Templates')
		);
		
		$f->addFieldToTab(
			'Root.Main', 
			TextField::create(
				'TemplateName',
				PrintManagerTemplate::$field_labels['TemplateName']
			)
				->setDescription('Dateiname des Templates (ohne Endung .ss)')
		);
		
		// Dropdown of all RestaurantPages the user can access
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'Theme',
				PrintManagerTemplate::$field_labels['Theme'],
				SiteConfig::current_site_config()->getAvailableThemes()
			)
				->setEmptyString('Bitte auswählen')
		);
		
		// set dimensions
		$f->addFieldToTab(
			'Root.Main', 
			FieldGroup::create(
				DropdownField::create(
					'SizeUnit', 
					'', 
					singleton($this->ClassName)->dbObject('SizeUnit')->enumValues()
				)
			)->setTitle('Maßeinheit')
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'DocumentWidth', 
				'Breite Dokument'
			)
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'DocumentHeight', 
				'Höhe Dokument'
			)
		);
		
		return $f;	
	}
	
	public static function isSelectedTheme() {
		return $this->Theme == SiteConfig::current_site_config()->Theme;
	}
	
	/**
	 * Returns string with dimension of $DocumentWidth x $DocumentHeight and the current $SizeUnit
	 * e.g. "210x297 mm"
	 * 
	 * @return string
	 */
	public function Dimensions() {
		return $this->DocumentWidth.'x'.$this->DocumentHeight.' '.$this->SizeUnit;
	}
	
	/**
	 * Returns all PrintManagerTemplates that have the same Theme as the current SiteConfig
	 * 
	 * @return DataList
	 */
	public static function AllowedPrintManagerTemplates() {
		return PrintManagerTemplate::get()->filter(array('Theme:ExactMatch' => SiteConfig::current_site_config()->Theme));
	}
	
	/**
	 * Functions checks if $this->TemplateName is valid
	 * -> corresponding html template needs to exist
	 * 
	 * @return boolean
	 */
	public function validTemplateName() {
		if(
			!$this->ID ||
			( $this->ID && $this->Theme && $this->TemplateName &&
				(
					is_file(Director::baseFolder().'/themes/'.$this->Theme.'/templates/'.$this->TemplateName.'.ss')
				)
			)
		) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if all necessary settings for creating a PDF are set and valid
	 * 
	 * @return boolean
	 */
	public function validSettings() {
		if(
			!$this->ID ||	
			($this->ID && $this->validTemplateName() && $this->DocumentWidth && $this->DocumentHeight)
		){
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Returns the validSetting boolean value as Booelan DBField
	 * so we acn use modifying function like validSettingsField.Nice etc.
	 * @return Boolean
	 */
	public function Status() {
		return $this->validSettings() ? 'OK' : 'Fehler';
	}
	
	 // PERMISSIONS
	public function canCreate($member = null) {
        return Permission::check('CREATE_PRINTMANAGERTEMPLATE', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_PRINTMANAGERTEMPLATE', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_PRINTMANAGERTEMPLATE', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_PRINTMANAGERTEMPLATE', 'any', $member);
    }
	
	public function providePermissions() {
		// Doc: http://www.balbuss.com/adding-new-group-permissions/
		return array(
			"CREATE_PRINTMANAGERTEMPLATE" => array(
				'name' => 'Kann "'.$this::$singular_name.'" erstellen',
				'category' => 'Modul "Druck Manager"',
				'help' => '',
				'sort' => 120
			),
			"VIEW_PRINTMANAGERTEMPLATE" => array(
				'name' => 'Kann "'.$this::$singular_name.'" betrachten',
				'category' => 'Modul "Druck Manager"',
				'help' => '',
				'sort' => 120
			),
			"EDIT_PRINTMANAGERTEMPLATE" => array(
				'name' => 'Kann "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Modul "Druck Manager"',
				'help' => '',
				'sort' => 130
			),
			"DELETE_PRINTMANAGERTEMPLATE" => array(
				'name' => 'Kann "'.$this::$singular_name.'" löschen',
				'category' => 'Modul "Druck Manager"',
				'help' => '',
				'sort' => 140
			)
		);
	}
}

