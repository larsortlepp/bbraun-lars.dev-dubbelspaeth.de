<?php

/**
 * Adds belongs_many_many relation from Dish to PrintManagerConfig
 * 
 * @package dss-print-manager
 */
class PrintManagerDishExtension extends DataExtension {

	private static $belongs_many_many = array(
		'PrintManagerConfigs' => 'PrintManagerConfig'
	);
}
