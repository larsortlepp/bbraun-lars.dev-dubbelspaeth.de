<?php

/**
 * Adds site wide configuration options for this module.
 *
 * With this setting the dss-print-manager module can be enabled or disabled 
 * per Subsite in the SiteConfig
 * 
 * Permission checks for showing or accessing models and objects of this
 * module are performed in the classes itself.
 * 
 * @package dss-print-manager
 * @requires MySiteConfigExtension.php
 * 
 */
class PrintManagerSiteConfigExtension extends DataExtension {

	static $db = array(
		'EnablePrintManager' => 'Boolean'
	);
	static $defaults = array(
		'EnablePrintManager' => 0
	);

	public function updateCMSFields(FieldList $f) {

		// Are we in the "main site" or a subsite?
		if (Subsite::CurrentSubsite() && Permission::check('VIEW_SITECONFIG_TAB_MODULES')) {
			// Add subsite-specific fields to "Modules" tab
			$f->addFieldToTab('Root.Modules', new HeaderField('HeaderModulePrintManager', 'Modul "Druck Manager"'));
			$f->addFieldToTab('Root.Modules', new LabelField('DescriptionModulePrintManager', 'Erstellen und Verwalten von Druckvorlagen mit beliebigen Gerichten und indivudller Überschrift und Fußzeile.'));
			$f->addFieldToTab('Root.Modules', new FieldGroup('Modul "Druck Manager"', array(new CheckboxField('EnablePrintManager',  'aktiviert'))));
			$f->fieldByName('Root.Modules')->setTitle('Module');
		}
	}
}