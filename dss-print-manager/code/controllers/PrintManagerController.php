<?php

/**
 * Controller is accessible via mydomain.de/printmanager
 * (defined via _config/routes.yml)
 * It renders the print view via templates directly over the controller
 * without the need to have a real page in the SiteTree
 * 
 * @requires mysite/code/interfaces/ControllerRestaurantPageRelation.php
 */
class PrintManagerController extends Controller implements ControllerRestaurantPageRelation {
	
	// urlsegment for controller that is registered via _config/routes.yml
	public static $url_segment = 'printmanager';
	
	public $PrintManagerConfigID = null;
	public $RestaurantPageID = null;
	
	private static $allowed_actions = array(
		'pdf',
		'renderhtml'
	);
	
	private static $url_handlers = array(
		'pdf/$ID' => 'pdf',
		'renderhtml/$ID' => 'renderhtml'
	);
	
	/**
	 * Creates pdf file for the given PrintManagerConfig 
	 * and returns it to browser output as stream.
	 * 
	 * Before rendering the PDF we perform a permission check:
	 * Member has to be logged in, PrintManager must be enabled for current Subsite
	 * and Member must have canView() permission on PrintManagerConfig.
	 * If permission check fails we will redirect to 404 page.
	 */
	public function pdf() {		
		// get PrintManagerConfig for given ID
		$printmanagerconfig = $this->PrintManagerConfig();
		if(!$printmanagerconfig) return $this->redirect('/error-404.html', 404);
		
		// check permission
		if(Member::logged_in_session_exists() && PrintManagerAdmin::ModuleIsEnabled() && $printmanagerconfig->canView() && $this->validAuthKey()) {
			
			// get print template
			$template = $printmanagerconfig->PrintManagerTemplate();
			
			// define theme font path for DOMPDF
			define("DOMPDF_THEME_FOLDER", Director::baseFolder().'/themes/'.$template->Theme."/dompdf-fonts/");
			// include DOMPDF class
			require_once(Director::baseFolder().'/dompdf/dompdf_config.inc.php');
			
			// create PDF with given settings from PrintManagerConfig
			$size = array(
				$template->DocumentWidth,
				$template->DocumentHeight,
				$template->SizeUnit	
			);
			
			$authkey = Convert::raw2sql($_GET['k']);
			$templateurl = Controller::join_links(Director::protocolAndHost(), PrintManagerController::$url_segment, 'renderhtml', $printmanagerconfig->ID)."?k=$authkey";
		
			return $this->createPDF($size, $templateurl, $printmanagerconfig->Title);
		}
		// return permission failure and redirect to login
		else {
			return $this->redirect('/error-404.html', 404);
		}
		
	}
	
	/**
	 * Render HTML view of the PrintManagerConfig
	 * Using the Theme and Template as set in PrintManagerConfig->PrintManagerTemplate
	 * 
	 * @return 
	 */
	public function renderhtml() {		
		$printmanagerconfig = $this->PrintManagerConfig();
		if(!$printmanagerconfig) return $this->redirect('/error-404.html', 404);
		
		// check for valid AuthKey in URL params
		if($this->validAuthKey()) {
			$template = $printmanagerconfig->PrintManagerTemplate();
			SSViewer::set_theme($template->Theme);
			return $printmanagerconfig->renderWith($template->TemplateName);
		}
		else {
			return $this->redirect('/error-404.html', 404);
		}
	}
	
	/**
	 * Creates pdf file and returns it to browser output as stream
	 * 
	 * @param array $size custom size as array(w,h,unit) in mm
	 * @param string $templateurl Absolute URL to html template that will be rendered into a PDF (e.g. http://www.mysite.com/printmanager/renderhtml/1)
	 * @param string $filename filename without extension (e.g. 'MyFilename' or 'My Separated Filename'). Optional
	 */
	public function createPDF($size, $templateurl, $filename = null){
		// init dompdf
		$dompdf = new DOMPDF();
	
		/*
		 * size array map: 
		 * 
		 * $size[0]:	width
		 * $size[1]:	height
		 * $size[2]:	unit (mm)
		 *
		 */	
		
		// convert mm to points:
		// example:
		// A4 ( 297 x 210 ) results to  841 x 595 points
		switch($size[2]){
			
			case 'mm': 
				$DocumentWidth = PrintHtmlTemplatePage_Controller::mm_to_points($size[0]);
				$DocumentHeight = PrintHtmlTemplatePage_Controller::mm_to_points($size[1]);
			break;
		
			case 'px': 
				$DocumentWidth = PrintHtmlTemplatePage_Controller::px_to_points($size[0]);
				$DocumentHeight = PrintHtmlTemplatePage_Controller::px_to_points($size[1]);
			break;
			
		}
		
		// set custom paper size
		$DocumentSize = array(0, 0, $DocumentWidth, $DocumentHeight);
	
		$dompdf->set_paper($DocumentSize);
	
		// load template file
		$dompdf->load_html_file($templateurl);
				
		// return PDF as file download under the given filename
		$filename = $filename ? $filename : 'Ausdruck';
		$filename .= '.pdf';
		
		// final output  of pdf file
		$dompdf->render();
		$dompdf->stream($filename);
	}
	
	/**
	 * Checks if a valid AuthKey for the given P
	 * rintManagerconfig exists in URL params.
	 * The AuthKey must be provided in in 'k' value of URL
	 * e.g. mysite.com/printmanager/pdf/1?k=1hha72gdh173123be1232
	 * 
	 * @return boolean
	 */
	public function validAuthKey() {
		$printmanagerconfig = $this->PrintManagerConfig();
		$authkey = Convert::raw2sql($_GET['k']);
		return ($printmanagerconfig && $authkey && $authkey == $printmanagerconfig->AuthKey) ? true : false;
	}
	
	// INTERFACE ControllerRestaurantPageRelation
	
	/**
	 * Set variables on current Object
	 * This function should call $this->setRestaurantPageID()
	 * and all other functions that might be needed additionaly.
	 * 
	 * $this->set_vars() should then be called in (based on Class):
	 * - Controller: handleAction()
	 * - ModelAdmin: getEditForm()
	 */
	public function set_vars() {
		$this->setPrintManagerConfigID();
		$this->setRestaurantPageID();
	}
	
	/**
	 * Assign RestaurantPageID dynamically based on current PrintManagerConfig
	 */
	public function setRestaurantPageID() {
		$printmanagerconfig = $this->PrintManagerConfig();
		if(!$printmanagerconfig) return;
		$RestaurantPageID = $printmanagerconfig->RestaurantPage()->ID;
		$this->RestaurantPageID = is_numeric($RestaurantPageID) ? $RestaurantPageID : null;
	}
	
	/**
	 * Returns the RestaurantPage based on $this->RestaurantPageID
	 * The function should contain:
	 * return RestaurantPage::get()->byID($this->RestaurantPageID);
	 * 
	 * @return RestaurantPage
	 */
	public function RestaurantPage() {
		return RestaurantPage::get()->byID($this->RestaurantPageID);
	}
	
	// Additional methods to assign and return the current PrintManagerConig dynamically
	
	/**
	 * Assigns the ID for the current PrintManagerConfig from the current URL
	 * Based on the $url_handler settings the ID will be submitted as request parameter 'ID'
	 * 
	 * Example URL: 
	 * - mydomain.com/printmanager/renderhtml/4
	 * - mydomain.com/printmanager/pdf/4
	 */
	public function setPrintManagerConfigID() {
		$PrintManagerConfigID = Convert::raw2sql($this->getRequest()->param('ID'));
		$this->PrintManagerConfigID = is_numeric($PrintManagerConfigID) ? $PrintManagerConfigID : null;
	}
	
	/**
	 * Returns the PrintManagerConfig for the ID that is given via request parameter
	 * @return RestaurantPage
	 */
	public function PrintManagerConfig() {
		return PrintManagerConfig::get()->byID($this->PrintManagerConfigID);
	}
	
	/**
	 * Controller's default action handler.  It will call the method named in $Action, if that method exists.
	 * If $Action isn't given, it will use "index" as a default.
	 * 
	 * We set the static variables for PrintManagaerConfigId and RestaurantPageID
	 */
	protected function handleAction($request, $action) {
		$this->set_vars();
		
		return parent::handleAction($request, $action);
	}
}
