<?php
/**
 * Replacement for SilverStripe 3.x's stock GridFieldDeleteAction component.
 *
 * When created with $removeRelation == false, GridFieldDeleteAction will
 * hide the "deleterecord" action with the red cross-circle icon if canDelete()
 * returns "false" (as is the case for Dishes that are used in other subsites), leaving the
 * user possibly confused.
 *
 * This class instead ALWAYS shows an icon, but if canDelete() returns "false",
 * we change the icon to a black version, indicating it is disabled, and show
 * an error popup with an explanation if the user clicks on it.
 */
class MyGridFieldDeleteAction extends GridFieldDeleteAction {
	/**
	 * @param GridField $gridField
	 * @param DataObject $record
	 * @param string $columnName
	 * @return string - the HTML for the column
	 */
	public function getColumnContent($gridField, $record, $columnName) {
		Requirements::javascript("mysite/javascript/MyGridFieldDeleteAction.js");

		if($this->removeRelation) {
			return parent::getColumnContent($gridField, $record, $columnName);
		} else {
			$field = GridField_FormAction::create($gridField,  'DeleteRecord'.$record->ID, false, "deleterecord",
					array('RecordID' => $record->ID))
				->addExtraClass('gridfield-button-delete')
				->setAttribute('title', _t('GridAction.Delete', "Delete"))
				->setAttribute('data-icon', 'cross-circle')
				->setDescription(_t('GridAction.DELETE_DESCRIPTION','Delete'));

			if(!$record->canDelete()) {
				$field->addExtraClass('gridfield-button-delete-disabled');
				$field->setAttribute('data-icon', 'cross-circle_disabled');
			}
		}
		return $field->Field();
	}
}
?>
