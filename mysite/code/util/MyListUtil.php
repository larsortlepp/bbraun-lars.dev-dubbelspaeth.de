<?php
/*
 * Helper class to make dealing with SilverStripe 3.x's DataLists and
 * ArrayLists easier.
 */
class MyListUtil {
	/**
	 * Converts an DataList into an ArrayList.
	 *
	 * @param DataList $list: The list to convert
	 *
	 * @return ArrayList
	 */
	public static function ToArrayList($list) {
		$arrayList = ArrayList::create();
		foreach ($list as $elem) {
			$arrayList->push($elem);
		}
		return $arrayList;
	}

	/**
	 * Concatenates the elements of two lists to an ArrayList.
	 *
	 * The returned ArrayList will consist of the elements from $list1,
	 * followed by the elements from $list2.
	 *
	 * @param ArrayList|DataList $list1: The first list
	 * @param ArrayList|DataList $list2: The second list
	 *
	 * @return ArrayList
	 */
	public static function ConcatenateLists($list1, $list2) {
		$concatenatedList = ArrayList::create();

		foreach (array($list1, $list2) as $list) {
			if ($list) {
				foreach ($list as $obj) {
					$concatenatedList->push($obj);
				}
			}
		}

		return $concatenatedList;
	}
}
