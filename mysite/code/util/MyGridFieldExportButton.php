<?php
/**
 * Replacement for SilverStripe 3.x's stock GridFieldExportButton component.
 *
 * GridFieldExportButton does not remove line breaks from CSV values which
 * breaks the Excel import as line breaks indicate a new import row. This
 * class fixes this.
 */
class MyGridFieldExportButton extends GridFieldExportButton {
	/**
	 * Generate export fields for CSV.
	 *
	 * @param GridField $gridField
	 * @return array
	 */
	public function generateExportFileData($gridField) {
		$separator = $this->csvSeparator;
		$csvColumns = ($this->exportColumns)
			? $this->exportColumns
			: singleton($gridField->getModelClass())->summaryFields();
		$fileData = '';
		$columnData = array();
		$fieldItems = new ArrayList();

		if($this->csvHasHeader) {
			$headers = array();

			// determine the CSV headers. If a field is callable (e.g. anonymous function) then use the
			// source name as the header instead
			foreach($csvColumns as $columnSource => $columnHeader) {
				$headers[] = (!is_string($columnHeader) && is_callable($columnHeader)) ? $columnSource : $columnHeader;
			}

			$fileData .= "\"" . implode("\"{$separator}\"", array_values($headers)) . "\"";
			$fileData .= "\n";
		}

		//Remove GridFieldPaginator as we're going to export the entire list.
		$gridField->getConfig()->removeComponentsByType('GridFieldPaginator');

		$items = $gridField->getManipulatedList();

		// @todo should GridFieldComponents change behaviour based on whether others are available in the config?
		foreach($gridField->getConfig()->getComponents() as $component){
			if($component instanceof GridFieldFilterHeader || $component instanceof GridFieldSortableHeader) {
				$items = $component->getManipulatedData($gridField, $items);
			}
		}

		foreach($items->limit(null) as $item) {
			$columnData = array();

			foreach($csvColumns as $columnSource => $columnHeader) {
				if(!is_string($columnHeader) && is_callable($columnHeader)) {
					if($item->hasMethod($columnSource)) {
						$relObj = $item->{$columnSource}();
					} else {
						$relObj = $item->relObject($columnSource);
					}

					$value = $columnHeader($relObj);
				} else {
					$value = $gridField->getDataFieldValue($item, $columnSource);
				}

				/*
				 * Old behavior: replace CR and LF with LF
				 *
				 *   $value = str_replace(array("\r", "\n"), "\n", $value);
				 *
				 * New behavior: replace CR, LF and CR+LF with Space
				 *               and trim the value afterwards
				 */
				$value = str_replace(array("\r", "\n", "\r\n"), " ", $value);
				$value = preg_replace('/\s+/', ' ', $value);

				$columnData[] = '"' . str_replace('"', '\"', $value) . '"';
			}
			$fileData .= implode($separator, $columnData);
			$fileData .= "\n";

			$item->destroy();
		}

		return $fileData;
	}
}
