<?php
/*
 * Helper class to make GridFields and their related components more
 * usable. Allows for chaining calls to setter methods as in JavaScript,
 * resulting in cleaner code. Example:
 *
 *   MyGridField::create('PrettyThings', 'Pretty things', $this->PrettyThings())
 *     ->setCols(array(
 *       'Name' => 'Name',
 *       'Desc' => 'Description'
 *     ))
 *     ->setRowsPerPage(25)
 *     ->addToTab($f, 'Things');
 */
class MyGridField {
	protected $name = null,
	          $title = null,
	          $data = null,
	          $type = null,
	          $cols = null,
	          $rowsPerPage = 9999,
	          $addNewButtonDataObjectType = null,
	          $autoCompleterPlaceholderText = null,
	          $autoCompleterSearchList = null,
	          $autoCompleterSearchCols = null,
	          $autoCompleterResultsFormat = null,
	          $autoCompleterOnAfterAdd = null,
			  $autoCompleterResultsLimit = null, // if null: limit will be set to MySiteConfigExtension.DishesAutoCompleterResultsLimit 
	          $allowCustomSort = false,
	          $customSortCol = 'SortOrder',
	          $extraSortCols = null,
	          $reorderColumnNumber = 0;

	/*
	 * Creates a new MyGridField instance.
	 *
	 * @param string $name The internal name for the GridField.
	 * @param string $title The title for the GridField.
	 * @param SS_List $data The data to show in the GridField.
	 * @param string $type Either 'record' or 'relation', default: 'record'
	 * @return MyGridField Returns the new MyGridField object.
	 */
	public function __construct($name, $title, $data, $type = 'record') {
		$this->name = $name;
		$this->title = $title;
		$this->data = $data;
		$this->type = $type;
	}

	/*
	 * Factory method that returns a new MyGridField instance.
	 *
	 * @param string $name The internal name for the GridField.
	 * @param string $title The title for the GridField.
	 * @param SS_List $data The data to show in the GridField.
	 * @param string $type Either 'record' or 'relation', default: 'record'
	 * @return MyGridField Returns the new MyGridField object.
	 */
	public static function create($name, $title, $data, $type = 'record') {
		return new MyGridField($name, $title, $data, $type);
	}

	/*
	 * Sets the columns to show in the GridField and their headings.
	 *
	 * @param array $cols An array with the column names as keys and the
	 * headings as values.
	 * @return MyGridField Returns the updated MyGridField object.
	 */
	public function setCols($cols) {
		$this->cols = $cols;
		return $this;
	}

	/*
	 * Sets the number of data rows to shows per page.
	 *
	 * @param integer $rowsPerPage The number of rows per page.
	 * @return MyGridField Returns the updated MyGridField object.
	 */
	public function setRowsPerPage($rowsPerPage) {
		$this->rowsPerPage = $rowsPerPage;
		return $this;
	}

	/*
	 * Overrides the data object type shown in the AddNewButton component.
	 *
	 * @param string $ttype The type to show.
	 * @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAddNewButtonDataObjectType($type) {
		$this->addNewButtonDataObjectType = $type;
		return $this;
	}

	/*
	 * Sets the placeholder text for AddExistingAutoCompleter's text field.
	 *
	 * @param string $text The text to show until a search term gets entered.
	 * @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAutoCompleterPlaceholderText($text) {
		$this->autoCompleterPlaceholderText = $text;
		return $this;
	}

	/*
	 * Sets the DataList/ArrayList to search when typing text into the
	 * AddExistingAutoCompleter field.
	 *
	 * Calling this is usually only necessary if the list scaffolded by
	 * the GridFieldAutoCompleter component itself does not match the
	 * data list behind the GridFieldDataColumns content, eg. because
	 * the latter has been extended by SQL joins.
	 *
	 * @param DataList|ArrayList $list The list to search.
	 * @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAutoCompleterSearchList($list) {
		$this->autoCompleterSearchList = $list;
		return $this;
	}

	/*
	 * Sets the columns to search when typing text into the
	 * AddExistingAutoCompleter field.
	 *
	 * @param array $cols An array of column names to search.
	 * @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAutoCompleterSearchCols($cols) {
		$this->autoCompleterSearchCols = $cols;
		return $this;
	}

	/*
	* Sets the format for the AddExistingAutoCompleter's result list.
	*
	* @param string $format A string with column names as variables to use as
	* format.
	* @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAutoCompleterResultsFormat($format) {
		$this->autoCompleterResultsFormat = $format;
		return $this;
	}

	/*
	* Sets a function to be called after the AddExistingAutoCompleter
	* has added an object to the GridField's underlying relation.
	*
	* @param function $func The function to be called.
	* @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAutoCompleterOnAfterAdd($func) {
		$this->autoCompleterOnAfterAdd = $func;
		return $this;
	}
	
	/*
	* Sets the maximum number of results for autocompleter result set
	*
	* @param Integer $limit Number of maximum results that will be rendered in result set
	* @return MyGridField Returns the updated MyGridField object.
	*/
	public function setAutoCompleterResultsLimit($limit) {
		$this->autoCompleterResultsLimit = $limit;
		return $this;
	}

	/*
	 * Specifies whether the user may change the sort order through
	 * drag-n-drop.
	 *
	 * @param boolean $customsort Whether to allow changing the sort order.
	 * @return MyGridField Returns the updated MyGridField object.
	 */
	public function setAllowCustomSort($customsort) {
		$this->allowCustomSort = $customsort;
		return $this;
	}

	/*
	 * Sets the column in which the user's custom sort order gets stored.
	 *
	 * @param string $col The column in which to store the custom sort oder.
	 * @return MyGridField Returns the updated MyGridField object.
	 */
	public function setCustomSortCol($col) {
		$this->customSortCol = $col;
		return $this;
	}

	/*
	 * Sets the number of the column containing the reorder handles.
	 *
	 * @param integer $colno The column number
	 * @return MyGridField Returns the updated MyGridField object.
	 */
	public function setReorderColumnNumber($colno) {
		$this->reorderColumnNumber = $colno;
		return $this;
	}

	/*
	 * Sets extra sort columns to be considered before the user's custom sort.
	 *
	 * @param string|array $extraSortCols A sort statement of an array of sort
	 * columns
	 * @return MyGridField Returns the updated MyGridField object.
	 */
	public function setExtraSortCols($extraSortCols) {
		$this->extraSortCols = $extraSortCols;
		return $this;
	}

	/*
	 * Generates the actual GridField object based on the current configuration.
	 *
	 * @return GridField The actual generated GridField component.
	 */
	public function get() {
		// Create a new GridField configuration
		if ($this->type == 'relation') {
			// Get current SiteConfig
			$currSiteConfig = SiteConfig::current_site_config();
			
			$gfCfg = GridFieldConfig_RelationEditor::create($this->rowsPerPage);

			// Replace GridFieldAddExistingAutoCompleter with our own
			// component
			$gfCfg->removeComponentsByType('GridFieldAddExistingAutoCompleter');
			$gfAddExistingAutoCompleter = new MyGridFieldAddExistingAutocompleter('buttons-before-right');
			if($this->autoCompleterResultsLimit) {
				$gfAddExistingAutoCompleter->setResultsLimit($this->autoCompleterResultsLimit);
			}
			else if($currSiteConfig->DishesAutoCompleterResultsLimit) {
				$gfAddExistingAutoCompleter->setResultsLimit($currSiteConfig->DishesAutoCompleterResultsLimit);
			}
			$gfCfg->addComponent($gfAddExistingAutoCompleter);

			// Make sure the GridFieldAddExistingAutoCompleter uses the
			// same underlying data list for its searches.
			if ($this->autoCompleterSearchList)
				$gfAddExistingAutoCompleter->setSearchList($this->autoCompleterSearchList);

			// Adjust GridFieldAddExistingAutoCompleter search fields?
			if ($this->autoCompleterSearchCols || $this->autoCompleterResultsFormat) {
				if ($this->autoCompleterPlaceholderText)
					$gfAddExistingAutoCompleter->setPlaceholderText($this->autoCompleterPlaceholderText);
				if ($this->autoCompleterSearchCols)
					$gfAddExistingAutoCompleter->setSearchFields($this->autoCompleterSearchCols);
				if ($this->autoCompleterResultsFormat)
					$gfAddExistingAutoCompleter->setResultsFormat($this->autoCompleterResultsFormat);
			}

			// onAfterAdd function specified?
			if ($this->autoCompleterOnAfterAdd)
				$gfAddExistingAutoCompleter->setOnAfterAdd($this->autoCompleterOnAfterAdd);
		} else {
			$gfCfg = GridFieldConfig_RecordEditor::create($this->rowsPerPage);

			// Replace delete action with our own
			$gfCfg->removeComponentsByType('GridFieldDeleteAction');
			$gfCfg->addComponent(new MyGridFieldDeleteAction());
		}

		// Override AddNewButton's title if a custom DataObject type was
		// configured
		if ($this->addNewButtonDataObjectType) {
			$gfAddNewButton = $gfCfg->getComponentByType('GridFieldAddNewButton');
			$gfAddNewButton->setButtonName(
				_t('GridField.Add', 'Add {name}', array('name' => $this->addNewButtonDataObjectType))
			);
		}

		// Make GridField drag-n-drop sortable?
		if ($this->allowCustomSort) {
			// From a UX perspective, it doesn't make sense to provide both
			// custom drap-n-drop sorting AND column headers that allow
			// sorting by different columns. GridFieldOrderableRows will
			// remove its drag-n-drop handles as soon as a column header is
			// clicked, however we still consider that to be too confusing,
			// so we remove the clickable column headers completely.
			$gfCfg->removeComponentsByType('GridFieldSortableHeader');
			$gfCfg->addComponent(new GridFieldTitleHeader());

			$gfCfg->addComponent($gfOrderableRows = new GridFieldOrderableRows($this->customSortCol));
			$gfOrderableRows->setExtraSortFields($this->extraSortCols);
			$gfOrderableRows->setReorderColumnNumber($this->reorderColumnNumber);
		}

		// Setup column headings
		$gfDataCols = $gfCfg->getComponentByType('GridFieldDataColumns');
		$gfDataCols->setDisplayFields($this->cols);

		// Add bottom button row
		$gfCfg->addComponent(new GridFieldButtonRow('after'));

		// Add export and print buttons
		$gfCfg->addComponent(new GridFieldExportButton('buttons-after-left'));
		$gfCfg->addComponent(new GridFieldPrintButton('buttons-after-left'));

		// Create a new GridField
		return new GridField($this->name, $this->title, $this->data, $gfCfg);
	}

	/**
	 * Adds the constructed GridField component to a tab.
	 *
	 * @param mixed $fields The fields object as returned by
	 * parent::getCMSFields()
	 * @param string $tabtitle The tab to add to
	 */
	public function addToTab($fields, $tabtitle) {
		$fields->addFieldToTab("Root.${tabtitle}", $this->get());
	}
}
