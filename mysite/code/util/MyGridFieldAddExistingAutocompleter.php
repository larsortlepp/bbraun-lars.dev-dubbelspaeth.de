<?php
/**
 * Replacement for SilverStripe 3.x's stock GridFieldAddExistingAutocompleter
 * component
 *
 * This class is used by MyGridField to provide the possibility to hook
 * up additional functionality after adding an object to a relation.
 */
class MyGridFieldAddExistingAutocompleter extends GridFieldAddExistingAutocompleter {
	/**
	 * A function called after adding an object to a DataList:
	 *
	 * $dataList = $onAfterAdd($gridField, $dataList, $addRelation);
	 *
	 * @var function $onAfterAdd
	 */
	protected $onAfterAdd;

	/**
	 * @param function
	 */
	public function setOnAfterAdd($func) {
		$this->onAfterAdd = $func;
		return $this;
	}

	/**
	 * Overwrites GridFieldAddExistingAutoCompleter's getManipulatedData()
	 * method with one providing a hook
	 *
	 * @param GridField $gridField
	 * @param SS_List $dataList
	 * @return SS_List
	 */
	public function getManipulatedData(GridField $gridField, SS_List $dataList) {
		// $gridField->State->GridFieldAddRelation gets cleared by
		// the parent's getManipulatedData() method, so we backup it
		$addRelation = $gridField->State->GridFieldAddRelation;
		if ($addRelation) {
			// Call parent's getManipulatedData()
			$dataList = parent::getManipulatedData($gridField, $dataList);

			if ($this->onAfterAdd) {
				$f = $this->onAfterAdd;
				$dataList = $f($gridField, $dataList, $addRelation);
			}
		}

		return $dataList;
	}
}
