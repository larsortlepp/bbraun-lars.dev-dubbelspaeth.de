<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class DateUtil {

	public static $date_format = 'Y-m-d'; #Date format
	public static $start_of_week = 1; # Number for the day the week starts - 1 = monday, 7 = sunday (default: 0)

	/**
	 * Returns the current date: e.g. '2012-08-03'
	 *
	 * @return String
	 */
	public static function currentDate() {
		return date(self::$date_format);
	}

	/**
	 * Functions returns the date in german (or given) format
	 *
	 * @param String $date The Date to be formatted
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String Formatted date
	 */
	public static function dateFormat($date, $format = '%d.%m.%Y') {
		return strftime($format, strtotime($date));
	}

	/**
	 * Function returns the startdate of the week for any given date
	 * Assumption: Start of the week is monday
	 *
	 * @param	String	$date				Date in format 'YYYY-mm-dd'
	 *
	 * @return	String						Date of the start of the week the given date is in
	 */
	public static function startDateOfTheWeek($date) {
		if(!$date) false;
		$time = strtotime($date);

		if(date('D', $time) == 'Mon') {
			$startDateOfTheWeek = $date;
		}
		else {
			$startDateOfTheWeek = date('Y-m-d', strtotime('last monday', $time));
		}

		return $startDateOfTheWeek;
	}

	/**
	 * Returns the StartDate and EndDate for a given date and number of days.
	 *
	 * @param	String	$date			The start date in form 'YYYY-MM-DD'
	 * @param	Integer	$dayCount		Number of days (default: 1)
	 *
	 * @return Array	$startEndDate	Array with StartDate and EndDate array('StartDate' => 'YYYY-mm-dd', 'EndDate' => 'YYYY-mm-dd')
	 */
	public static function dayDateRange($date, $dayCount = 1) {
		$startEndDate = array();
		$startEndDate['StartDate'] = $date;
		// calculate number of days to add
		$numDays = $dayCount - 1;
		$startEndDate['EndDate'] = date(self::$date_format, strtotime($startEndDate['StartDate'] . "+$numDays days"));
		return $startEndDate;
	}

	/**
	 * Returns the StartDate and EndDate for a given date and number of weeks.
	 * Calculates the start of the week, based on the given date.
	 *
	 * @param	String	$date			A date on any day of a week in form 'YYYY-MM-DD' (does not need to be the start of the week)
	 * @param	Integer	$weekCount		Number of weeks (default: 1)
	 *
	 * @return Array	$startEndDate	Array with StartDate and EndDate array('StartDate' => 'YYYY-mm-dd', 'EndDate' => 'YYYY-mm-dd')
	 */
	public static function weekDateRange($date, $weekCount = 1) {
		$startEndDate = array();
		$startEndDate['StartDate'] = self::startDateOfTheWeek($date);
		// calculate number of days to add
		$numDays = ($weekCount * 7) - 1;
		$startEndDate['EndDate'] = date(self::$date_format, strtotime($startEndDate['StartDate'] . "+$numDays days"));
		return $startEndDate;
	}

	/**
	 * Returns one date for every day between the start and the end date.
	 * If $onlyWeekDays is set to true function returns weekdays only (based on start of th week set via DateUtil::$start_of_week)	 *
	 *
	 * @param String $startdate		Start date in format 'YYYY-mm-dd'
	 * @param String $enddate		End date in format 'YYYY-mm-dd' (optional)
	 * @param Boolea $onlyWeekDays	True if function should only return week days (Mo - Fr) (default: false)
	 * @return Array $dateArray		Array with all dates between start- and enddate. e.g. array('2012-07-30','2012-07-31','2012-08-01')
	 */
	public static function singleDatesForDateRange($startdate, $enddate = null, $onlyWeekDays = false) {
		$dateArray = array();
		// add start date to array
		$dateArray[] = $startdate;

		// if enddate is set
		if($enddate) {
			// make timestamp from startdate and enddate
			$startTime = strtotime($startdate);
			$endTime = strtotime($enddate);
			$datediff = $endTime - $startTime;
			$days_between = floor($datediff/(60*60*24));

			// Add date of every following day
			for($i = 1; $i <= $days_between; $i++) {
				$dateArray[] = date(self::$date_format, strtotime($startdate . "+$i days"));
			}

			// Remove weekends
			if($onlyWeekDays) {

				$filteredDateArray = array();

				// get day numbers of weekend days based on start of the week
				$weekendDay1 = (self::$start_of_week + 5 <= 7) ? self::$start_of_week + 5 : ((self::$start_of_week + 5) - 7);
				$weekendDay2 = (self::$start_of_week + 6 <= 7) ? self::$start_of_week + 6 : ((self::$start_of_week + 6) - 7);

				// save names of weekend days, based on the $this->start_of_week
				$weekEndDayNames[] = date('l', strtotime( date('Y') . 'W' . date('W') . $weekendDay1));
				$weekEndDayNames[] = date('l', strtotime( date('Y') . 'W' . date('W') . $weekendDay2));

				// remove weekends from result
				foreach($dateArray as $key => $date) {
					// if date is a weekend day -> add it to the array
					if(!in_array(date('l', strtotime($date)), $weekEndDayNames)) $filteredDateArray[] = $date;
				}

				$dateArray = $filteredDateArray;

			}
		}
		return $dateArray;
	}

	/**
	 * Set locale to given $locale - to work with english date format in strftime() functions
	 *
	 * @param String $locale Country locale to set for date format (e.g.
	 * 'en_US', default is 'de_DE')
	 */
	public static function setlocale($locale = 'de_DE') {
		switch($locale) {
			case 'en_US':
				setlocale(LC_TIME, 'en_US.utf8', 'en_US.UTF-8', 'en_US@euro', 'en_US', 'en');
				break;
			default:
				setlocale(LC_TIME, 'de_DE.utf8', 'de_DE.UTF-8', 'de_DE@euro', 'de_DE', 'de', 'ge');
		}
	}
}
