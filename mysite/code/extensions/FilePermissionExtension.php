<?php

/* 
 * Adds canView() and canUpload() Permissions to Files (and Folders)
 * Permissions are only checked in "assets" view (AssetAdmin)
 * For UploadFields in al other CMS areas we need to implement permission checks
 * in the UploadField itself (via extension "CustomUploadField")
 * 
 * NOTE:
 * In an UploadField, setting canView() permission on a folder to 'false'
 * will result in detaching a file from the DataObject.
 * We have to use UploadField::setFilterFunction() to hide folders from the user
 * in the folder selection, but still letting her see the file that is assigned 
 * from a folder she does not have canView() rights on.
 * 
 * TODO
 * - find a way to hide the "edit" button in gridfield for all Files that user does not have canEditFolder() permission for
 *   -> implementing it in canEdit() leads to removing all Upload and CreateFolder buttons from child Folders and Files
 *   -> we need to just remove the "edit" link in GridField
 */
class FilePermissionExtension extends DataExtension implements PermissionProvider {
	
	private static $db = array(
		'CanViewType' => 'Enum("Anyone,LoggedInUsers,OnlyTheseUsers,Inherit","Inherit")',
		'CanEditFolderType' => 'Enum("Anyone,LoggedInUsers,OnlyTheseUsers,Inherit","Inherit")',
		'CanDeleteType' => 'Enum("Anyone,LoggedInUsers,OnlyTheseUsers,Inherit","Inherit")',
		'CanUploadType' => 'Enum("Anyone,LoggedInUsers,OnlyTheseUsers,Inherit","Inherit")'
	);

	private static $many_many = array(
		'ViewerGroups' => 'Group',
		'EditorFolderGroups' => 'Group',
		'DeleterGroups' => 'Group',
		'UploaderGroups' => 'Group'
	);
	
	private static $defaults = array(
		'CanViewType' => 'Inherit',
		'CanEditFolderType' => 'Inherit',
		'CanDeleteType' => 'Inherit',
		'CanUploadType' => 'Inherit'
	);
	
	/**
	 * Access tab, copied from SiteTree
	 */
	public function updateCMSFields(FieldList $fields) {
		if(($this->owner instanceof Folder) && $this->owner->ID) {
			$options = array();
			$options['Inherit'] = 'Von übergeordnetem Verzeichnis erben';
			$options['Anyone'] = _t('SiteTree.ACCESSANYONE', 'Anyone');
			$options['LoggedInUsers'] = _t('SiteTree.ACCESSLOGGEDIN', 'Logged-in users');
			$options['OnlyTheseUsers'] = _t('SiteTree.ACCESSONLYTHESE', 'Only these people (choose from list)');

			// canView permissions
			$fields->push(
				new HeaderField(
					'WhoCanViewHeader',
					'Wer kann Dateien dieses Verzeichnisses betrachten?',
					2
				)
			);
			$fields->push(
				new OptionsetField(
					'CanViewType',
					'',
					$options
				)
			);
			$fields->push(
				new TreeMultiselectField(
					'ViewerGroups',
					_t('SiteTree.VIEWERGROUPS', 'Viewer Groups')
				)
			);
			
			// canEdit permissions
			$fields->push(
				new HeaderField(
					'WhoCanEditFolderHeader',
					'Wer kann dieses Verzeichnis bearbeiten?',
					2
				)
			);
			$fields->push(
				new OptionsetField(
					'CanEditFolderType',
					'',
					$options
				)
			);
			$fields->push(
				new TreeMultiselectField(
					'EditorFolderGroups',
					'Bearbeitergruppen'
				)
			);
			
			// canDelete permissions
			$fields->push(
				new HeaderField(
					'WhoCanDeleteHeader',
					'Wer kann dieses Verzeichnis löschen?',
					2
				)
			);
			$fields->push(
				new OptionsetField(
					'CanDeleteType',
					'',
					$options
				)
			);
			$fields->push(
				new TreeMultiselectField(
					'DeleterGroups',
					'Löschgruppen'
				)
			);
			
			// canUpload permissions
			$fields->push(
				new HeaderField(
					'WhoCanUploadHeader',
					'Wer kann Dateien in dieses Verzichnis hochladen?',
					2
				)
			);
			$fields->push(
				new OptionsetField(
					'CanUploadType',
					'',
					$options
				)
			);
			$fields->push(
				new TreeMultiselectField(
					'UploaderGroups',
					'Hochladegruppen'
				)
			);
		}
	}
	
	public function canViewTypePermission($member = null) {
		if(!$member || !(is_a($member, 'Member')) || is_numeric($member)) {
			$member = Member::currentUser();
		}

		// admin override
		if($member && Permission::checkMember($member, "ADMIN")) {
			return true;
		}
		
		// permission for root folder
		$rootPermission = Permission::check('VIEW_FILE_ROOT');

		if ($this->owner instanceof Folder) {
			switch ($this->owner->CanViewType) {
				case 'LoggedInUsers':
					return (bool)$member;
				case 'Inherit':
					if ($this->owner->ParentID) return $this->owner->Parent()->canViewTypePermission($member);
					else if($this->owner->ID) {
						return $rootPermission;
					}
					else return true;
				case 'OnlyTheseUsers':
					if ($member && $member->inGroups($this->owner->ViewerGroups())) return true;
					else return false;
				case 'Anyone':
				default:
					return true;
			}
		}

		// File DataObjects created by SearchForm don't have a ParentID, which we need
		// We fix this by re-getting the File object by it's ID if the ParentID is missing and use that
		$file = $this->owner;

		// if parent exists: apply parent permissions
		// otherwise apply permission for root folder based on users group permission
		if($file->Parent()->exists()) {
			return $file->Parent()->canViewTypePermission($member);
		}
		return $rootPermission;
	}
	
	/**
	 * Implements custom canView permissions:
	 * If in "assets" tab (AssetAdmin): use the permssions set via 'CanViewType'
	 * otherwise return true by default (always make it visible)
	 * This is neccessary beacuse UploadField will remove an assigned File
	 * from a DataObject if the user has no canView() rights.
	 * 
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$controller = Controller::curr();
		if($controller instanceof AssetAdmin) {
			return $this->owner->canViewTypePermission();
		}
		else {
			return true;
		}
	}
	
	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEditFolder($member = null) {
		if(!$member || !(is_a($member, 'Member')) || is_numeric($member)) {
			$member = Member::currentUser();
		}

		// admin override
		if($member && Permission::checkMember($member, "ADMIN")) {
			return true;
		}
		
		// permission for root folder
		$rootPermission = Permission::check('EDITFOLDER_FILE_ROOT');

		if ($this->owner instanceof Folder) {
			switch ($this->owner->CanEditFolderType) {
				case 'LoggedInUsers':
					return (bool)$member;
				case 'Inherit':
					if ($this->owner->ParentID) return $this->owner->Parent()->canEditFolder($member);
					else return $rootPermission;
				case 'OnlyTheseUsers':
					if ($member && $member->inGroups($this->owner->EditorFolderGroups())) return true;
					else return false;
				case 'Anyone':
				default:
					return true;
			}
		}

		// File DataObjects created by SearchForm don't have a ParentID, which we need
		// We fix this by re-getting the File object by it's ID if the ParentID is missing and use that
		$file = $this->owner;

		// if parent exists: apply parent permissions
		// otherwise apply permission for root folder based on users group permission
		if($file->Parent()->exists()) {
			return $file->Parent()->canEditFolder($member);
		}
		return $rootPermission;
	}
	
	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		if(!$member || !(is_a($member, 'Member')) || is_numeric($member)) {
			$member = Member::currentUser();
		}

		// admin override
		if($member && Permission::checkMember($member, "ADMIN")) {
			return true;
		}
		
		// permission for root folder
		$rootPermission = Permission::check('DELETE_FILE_ROOT');

		if ($this->owner instanceof Folder) {
			switch ($this->owner->CanDeleteType) {
				case 'LoggedInUsers':
					return (bool)$member;
				case 'Inherit':
					if ($this->owner->ParentID) return $this->owner->Parent()->canDelete($member);
					else return $rootPermission;
				case 'OnlyTheseUsers':
					if ($member && $member->inGroups($this->owner->DeleterGroups())) return true;
					else return false;
				case 'Anyone':
				default:
					return true;
			}
		}

		// File DataObjects created by SearchForm don't have a ParentID, which we need
		// We fix this by re-getting the File object by it's ID if the ParentID is missing and use that
		$file = $this->owner;

		// if parent exists: apply parent permissions
		// otherwise apply permission for root folder based on users group permission
		if($file->Parent()->exists()) {
			return $file->Parent()->canDelete($member);
		}
		return $rootPermission;
	}
	
	public function canUpload($member = null) {
		if(!$member || !(is_a($member, 'Member')) || is_numeric($member)) {
			$member = Member::currentUser();
		}

		// admin override
		if($member && Permission::checkMember($member, "ADMIN")) {
			return true;
		}
		
		// permission for root folder
		$rootPermission = Permission::check('UPLOAD_FILE_ROOT');

		if ($this->owner instanceof Folder) {
			switch ($this->owner->CanUploadType) {
				case 'LoggedInUsers':
					return (bool)$member;
				case 'Inherit':
					if ($this->owner->ParentID) return $this->owner->Parent()->canUpload($member);
					else return $rootPermission;
				case 'OnlyTheseUsers':
					if ($member && $member->inGroups($this->owner->UploaderGroups())) return true;
					else return false;
				case 'Anyone':
				default:
					return true;
			}
		}

		// File DataObjects created by SearchForm don't have a ParentID, which we need
		// We fix this by re-getting the File object by it's ID if the ParentID is missing and use that
		$file = $this->owner;

		// if parent exists: apply parent permissions
		// otherwise apply permission for root folder based on users group permission
		if($file->Parent()->exists()) {
			return $file->Parent()->canUpload($member);
		}
		return $rootPermission;
	}
	
	/**
	 * Add user permissions
	 */
	public function providePermissions() {
		return array(
			'VIEW_FILE_ROOT' => array(
				'name' => 'Kann Dateien und Verzeichnisse im root Verzeichnis betrachten',
				'category' => 'Dateien',
				'sort' => 10
			),
			'EDITFOLDER_FILE_ROOT' => array(
				'name' => 'Kann Dateien und Verzeichnisse im root Verzeichnis bearbeiten',
				'category' => 'Dateien',
				'sort' => 20
			),
			'DELETE_FILE_ROOT' => array(
				'name' => 'Kann Dateien und Verzeichnisse im root Verzeichnis löschen',
				'category' => 'Dateien',
				'sort' => 30
			),
			'UPLOAD_FILE_ROOT' => array(
				'name' => 'Kann Dateien und Verzeichnisse im root Verzeichnis hochladen',
				'category' => 'Dateien',
				'sort' => 40
			),
		);
	}
	
}
