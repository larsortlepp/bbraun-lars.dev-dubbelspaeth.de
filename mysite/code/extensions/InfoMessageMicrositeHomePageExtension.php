<?php
/**
 * Extension adds custom fields for InfoMessage used on MicrositeHomePage
 * Is currently not used on InfoMessageTemplate, 
 * because we couldn´t pick a proper SiteTreeID for LinkTo
 * (as this has to be Microsite specific) 
 */
class InfoMessageMicrositeHomePageExtension extends DataExtension {
	
	private static $db = array(
		"LinkType" => "Enum('Internal,External','Internal')",
		"ExternalURL" => "Varchar(2083)", // 2083 is the maximum length of a URL in Internet Explorer.
	);
	
	private static $has_one = array(
		"LinkTo" => "SiteTree",
		'Download' => 'File'
	);
	
	private static $defaults = array(
		"LinkType" => "Internal"
	);
	
	public function updateCMSFields($f) {
		
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		
		$micrositeContainer = MicrositeContainer::get()
			->filter(array(
				'ParentID' => $restaurantPage->ID
			))
			->limit(1)
			->first();
		
		$insertBefore = ($headerEN = $f->fieldByName('Root.Main.InfoMessageHeaderEN')) ? $headerEN : $f->fieldByName('Root.Main.TimingHeader');
		$insertBeforeName = ($insertBefore) ? $insertBefore->getName() : null;
		
		$f->addFieldToTab('Root.Main', HeaderField::create('LinkHeader', 'Link'), $insertBeforeName);
		
		$f->addFieldToTab(
			'Root.Main', 
			OptionsetField::create(
				"LinkType", 
				'Link Typ',
				array(
					"Internal" => _t('RedirectorPage.REDIRECTTOPAGE', "A page on your website"),
					"External" => _t('RedirectorPage.REDIRECTTOEXTERNAL', "Another website")
				)
			),
			$insertBeforeName
		);
		
		// Limit SiteTree dropdown to pages of current Microsite
		$f->addFieldToTab('Root.Main', TreeDropdownField::create(	
				"LinkToID", 
				_t('RedirectorPage.YOURPAGE', "Page on your website"), 
				"SiteTree"
			)
			->setTreeBaseID($micrositeContainer->ID),
			$insertBeforeName
		);
		
		$f->addFieldToTab(
			'Root.Main', 
			TextField::create(
				"ExternalURL", 
				_t('RedirectorPage.OTHERURL', "Other website URL")
			),
			$insertBeforeName
		);
		
	}
	
	public function Link() {
		if($this->owner->LinkType == 'External') {
			if($this->owner->ExternalURL) {
				return $this->owner->ExternalURL;
			}
		} 
		else if($this->owner->LinkType == 'Internal') {
			$linkTo = $this->owner->LinkToID ? DataObject::get_by_id("SiteTree", $this->owner->LinkToID) : null;
			if($linkTo) return $linkTo->Link();
		}
	}
	
	public function LinkTypeNice() {
		switch($this->owner->LinkType) {
			case 'Internal':
				return _t('RedirectorPage.REDIRECTTOPAGE', "A page on your website");
			default:
				return _t('RedirectorPage.REDIRECTTOEXTERNAL', "Another website");
		}
	}
	
	public function onBeforeWrite() {
		// Prefix the URL with "http://" if no prefix is found
		if($this->owner->ExternalURL && (strpos($this->owner->ExternalURL, '://') === false)) {
			$this->owner->ExternalURL = 'http://' . $this->owner->ExternalURL;
		}
	}
}
