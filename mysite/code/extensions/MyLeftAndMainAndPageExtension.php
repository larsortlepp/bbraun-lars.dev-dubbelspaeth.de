<?php
/**
 * This extension does two things:
 * - it replaces the CMS Menu's "Help" link with a custom one
 * - it enables the CMS to do custom sorting of pages, for both tree and
 *   list views. To this purpose, both LeftAndMain and Page classes require
 *   this extension with separate methods because tree and list views use
 *   different code paths.
 *
 *   Each page holder (eg. MenuContainer) can have a property "$customSort"
 *   defined, which can either be an array($column, $sortDir) or just a
 *   string with the column name, in which case ascending order is assumed.
 *
 *   To activate this class add this to _config.php:
 *   Object::add_extension('LeftAndMain', 'MyLeftAndMainAndPageExtension');
 *   Object::add_extension('Page', 'MyLeftAndMainAndPageExtension');
 */
class MyLeftAndMainAndPageExtension extends DataExtension {
	/**
	 * Extension init used to replace the CMS menu's "Help" link
	 */
	function init() {
		CMSMenu::remove_menu_item('Help');
		CMSMenu::add_link(
			'Help',
			_t('LeftAndMain.HELP', 'Help', 'Menu title'),
			'static-pages/cms-help.html',
			-1,
			array('target' => '_blank') // attributes to include in the link
		);
	}

	/*
	 * Returns $obj's custom sort column and order, if defined.
	 *
	 * @param obj A page holder object that might have $customSort defined.
	 * @return list
	 */
	private function getSortColumn($obj) {
		if (property_exists($obj, "customSort") || $obj->hasMethod("getCustomSort")) {
			$customSort = (property_exists($obj, "customSort")) ? (array)$obj->customSort : (array)$obj->getCustomSort();
			return array($customSort[0], (count($customSort) > 1) ? array_pop($customSort) : "ASC");
		}
		return null;
	}

	/*
	 * This callback function gets called from Hierarchy's
	 * doAllChildrenIncludingDeleted() method if Page gets extended by us.
	 * It takes care of the tree view.
	 *
	 * Note that $stageChildren MUST be passed by-reference since ArrayLists
	 * became immutable in SilverStripe 3.1.
	 *
	 * @param ArrayList $stageChildren Pages shown in the tree view.
	 * @param unknown_type $context Unused here.
	 */
	public function augmentAllChildrenIncludingDeleted(&$stageChildren, $context) {
		// Check $this->owner, the page holder class, for customSort property
		if (list($col, $order) = $this->getSortColumn($this->owner)) {
			// Replace $stageChildren with a sorted copy
			$stageChildren = $stageChildren->sort($col, $order);
		}
	}

	/*
	 * This callback function gets called from CMSMain's ListViewForm() method
	 * if LeftAndMain gets extended by us. It takes care of the list view.
	 *
	 * @param CMSForm $listview Form that contains the GridField listing the pages.
	 */
	public function updateListView($listview) {
		// Retrieve page holder class through request's ParentID
		$parentID = $listview->getController()->getRequest()->requestVar('ParentID');
		if ($parentID && $parent = Page::get()->byId($parentID)) {
			// Check $parent, the page holder class, for customSort property
			if (list($col, $order) = $this->getSortColumn($parent)) {
				// Get list view's GridField (always named 'Page')
				if ($gridField = $listview->Fields()->fieldByName('Page')) {
					// Get underlying DataList
					$list = $gridField->getList();

					// The DataList's query by default only refers to the
					// SiteTree table which would limit the columns $customSort
					// can refer to. Therefore we have to extend the query to
					// all possible children page types, we can't know what
					// table's fields $customSort refers to.
					foreach ($parent->allowedChildren() as $classname) {
						$list = $list->leftJoin($classname, "SiteTree.ID=${classname}.ID");
					}

					// Finally replace the list with a sorted copy
					$gridField->setList($list->sort($col, $order));
				}
			}
		}
	}
}
