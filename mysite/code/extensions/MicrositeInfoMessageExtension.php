<?php
/**
 * Extension adds function to get related InfoMessages
 */
class MicrositeInfoMessageExtension extends DataExtension {
	
	/**
	 * Return published InfoMessages
	 *
	 * @return DataList
	 */
	public function InfoMessagesPublished(){
		return $this->owner->InfoMessages('Publish = 1');
	}
	
	/**
	 * Only returns InfoMessages for the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return ArrayList
	 */
	public function InfoMessagesAtDateTime($datetime = null) {
		$infomessages = $this->owner->InfoMessagesPublished();
		$returnInfoMessages = new ArrayList();
		foreach($infomessages as $infomessage) {
			if($infomessage->ShowAtDateTime($datetime)) {
				$returnInfoMessages->push($infomessage);
			}
		}
		return $returnInfoMessages->count() >= 1 ? $returnInfoMessages : false;
	}
}
