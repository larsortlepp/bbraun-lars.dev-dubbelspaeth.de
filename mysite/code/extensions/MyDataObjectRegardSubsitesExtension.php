<?php

/**
 * Extension for the SiteTree object to add subsites support
 */
class MyDataObjectRegardSubsitesExtension extends DataExtension {

	static function add_to_class($class, $extensionClass, $args = null) {
		if (method_exists('DataExtension', 'add_to_class')) {
			Config::inst()->update($class, 'has_one', array(
				'Subsite' => 'Subsite', // The subsite that this page belongs to
			));
		}
		parent::add_to_class($class, $extensionClass, $args);
	}

	/**
	 * Update any requests to limit the results to the current site
	 */
	function augmentSQL(SQLQuery &$query) {
		if(Subsite::$disable_subsite_filter) return;

		// Don't run on delete queries, since they are always tied to
		// a specific ID.
		if ($query->getDelete()) return;

		// If you're querying by ID, ignore the sub-site - this is a bit ugly...
		// if(!$query->where || (strpos($query->where[0], ".\"ID\" = ") === false && strpos($query->where[0], ".`ID` = ") === false && strpos($query->where[0], ".ID = ") === false && strpos($query->where[0], "ID = ") !== 0)) {
		$where = $query->getWhere();
		if (!$where || (!preg_match('/\.(\'|"|`|)ID(\'|"|`|)( ?)=/', $where[0]))) {

			if (Subsite::$force_subsite) $subsiteID = Subsite::$force_subsite;
			else {
				$subsiteID = Subsite::currentSubsite()->ID;
			}
			if(!$subsiteID) return;

			// The foreach is an ugly way of getting the first key :-)
			foreach($query->getFrom() as $tableName => $info) {
				// The tableName should be SiteTree or SiteTree_Live...
				#if(strpos($tableName,'SiteTree') === false) break;
				$query->addWhere("\"$tableName\".\"SubsiteID\" IN ($subsiteID)");
				break;
			}
		}
	}

	function onBeforeWrite() {
		if (!$this->owner->ID && !$this->owner->SubsiteID)
			$this->owner->SubsiteID = Subsite::currentSubsite()->ID;

		parent::onBeforeWrite();
	}

	/**
	 * Returns the SiteConfig of the current page and subsite (in the CMS)
	 *
	 * @return DataObject
	 */
	public function SiteConfig() {
		return SiteConfig::get()->filter('SubSiteID', Subsite::currentSubsite()->ID)->First();
	}

	/**
	 * Returns True if Lang_en_US on the current RestaurantPage is set
	 * (indicating that english translations are enabled)
	 *
	 * @return Boolean
	 */
	public function showLang_en_US() {
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		return ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
	}
}
