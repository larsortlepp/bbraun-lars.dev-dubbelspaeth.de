<?php
/**
 * This extension implements a CustomCMSVersion() function of LeftAndMain:
 * - it adds the version number of the Digital Signage Systems to the
 *   version string that is added in the CMS
 *
 * The version number is taken from the file '/mysite/version'.
 * A textfile containing a version number.
 * 
 * NOTE: 
 * Version Number is added in publish process in a seperate branch.
 * During development the version number should be empty to avoid 
 * issues when comitting different branches.
 * 
 *
 * To activate this class add this to _config/config.yml
 * LeftAndMain:
 *   extensions:
 *     - MyLeftAndMainExtension
 */
class MyLeftAndMainExtension extends DataExtension {
	/**
	 * Push version number of Digital Signage System to cms
	 * 
	 * @return string
	 */
	public function CustomCMSVersion() {
		$versionfilepath = BASE_PATH . '/mysite/version';
		$dssVersion = _t('LeftAndMain.VersionUnknown', 'Unknown');
		
		// get version from owner class
		// contains comma seperated string in form: MODULENAME: 3.1.9, MODULENAME: 3.2.0
		$versionsstring = $this->owner->CMSVersion();
		
		// get version number of Digital Signage System
		if(file_exists($versionfilepath) && $dssVersionNumber = file_get_contents($versionfilepath))
			$dssVersion = $dssVersionNumber;
		
		// Prepend DSS version to $versiostring
		$versionsstring = 'DSS: '.$dssVersion.', '.$versionsstring;
		
		return $versionsstring;
	}
}
