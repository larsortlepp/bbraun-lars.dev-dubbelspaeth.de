<?php
/**
 * This extension removes fields from CMSFields that are not used in the
 * speiseplaene system.
 *
 * SiteTree:
 *     extensions:
 *       - MySiteTreeExtension
 */
class MySiteTreeRemoveCMSFieldsExtension extends DataExtension {
	public function updateCMSFields(FieldList $fields) {
		$fields->removeByName(array(
			'Metadata',
			'CopyToSubsiteID',
			'copytosubsite'
		));
	}
}
