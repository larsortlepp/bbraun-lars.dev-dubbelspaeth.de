<?php
/**
 * Extension adds functionionaliy to duplicate has_many relations
 * This will result in cloning the target DataObjects because in a has_many
 * the DataObject always has a has_one relation that we can not change.
 * So we have to create a new DataObject and point it to our clone.
 * 
 * duplicate() function has to implemented in the class itself,
 * to provide the source and the destination object
 * - in onAfterDuplicate() of extension class we don´t have a reference 
 * to the destination object - the clone)
 * 
 * public function duplicate() {
 *		$clone = parent::duplicate();
 *		$clone = $this->duplicateHasManyRelations($this, $clone);
 *		return $clone;
 *	}
 * 
 */
class DuplicateHasManyExtension extends Extension {
	
	/**
	 * Copies the has_many relations from one object to another instance of the name of object
	 * The destinationObject must be written to the database already and have an ID. Writing is performed
	 * automatically when adding the new relations.
	 *
	 * @param $sourceObject the source object to duplicate from
	 * @param $destinationObject the destination object to populate with the duplicated relations
	 * @return DataObject with the new has_many relations copied in
	 */
	public function duplicateHasManyRelations($sourceObject, $destinationObject) {
		if (!$destinationObject || $destinationObject->ID < 1) {
			user_error("Can't duplicate relations for an object that has not been written to the database",
				E_USER_ERROR);
		}
		
		if(class_exists('Subsite')) {
			// we are inside a SiteTreeSubsites::duplicateToSubsite() loop, 
			// so the current Subsite is set to the destination Subsite
			$destinationSubsiteID = Subsite::currentSubsiteID();
			Subsite::disable_subsite_filter($disabled = true);
		}
		
		// duplicating every has_many item of $sourceObject and set its
		// has_one reltaion to $destinationObject->ID
		if($sourceObject->has_many()) foreach($sourceObject->has_many() as $name => $type) {
			foreach($sourceObject->{$name}() as $item) {
				// duplicate the relation target object
				$itemClone = $item->duplicate();
				// set has_one ID on $itemClone to $destinationObject->ID
				$hasOneRelation = get_class($destinationObject) . 'ID';
				$itemClone->{$hasOneRelation} = $destinationObject->ID;
				
				// set new SubsiteID is DataObject has Subsite ID
				if(class_exists('Subsite') && $itemClone->SubsiteID) {
					$itemClone->SubsiteID = $destinationSubsiteID;
				}
				$itemClone->write();
			}
		}
		
		if(class_exists('Subsite')) {
			Subsite::disable_subsite_filter($disabled = false);
		}

		return $destinationObject;
	}
}
