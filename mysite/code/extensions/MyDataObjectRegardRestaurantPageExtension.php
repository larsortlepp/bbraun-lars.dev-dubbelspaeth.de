<?php
/*
 * This extensions associates decorated Pages and DataObjects with a
 * parent RestaurantPage, adding a RestaurantPageID and related functions
 * (similar to the Subsites module).
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * activate this class:
 *
 *   DataObject:
 *     extensions:
 *       - MyDataObjectRegardRestaurantPageExtension
 */
class MyDataObjectRegardRestaurantPageExtension extends DataExtension {
	public static function add_to_class($class, $extensionClass, $args = null) {
		if (method_exists('DataExtension', 'add_to_class')) {
			Config::inst()->update($class, 'has_one', array(
				'RestaurantPage' => 'RestaurantPage', // The RestaurantPage that this object belongs to
			));
		}
		parent::add_to_class($class, $extensionClass, $args);
	}

	/**
	 * Update RestaurantPageID on every write
	 */
	function onBeforeWrite() {
		// only add RestaurantPageID if we are inside the CMS
		// (only here we have a SiteTree structure to find a parent RestaurantPage)
		// otherwise we have to manually provide a RestaurantPageID
		if (is_subclass_of(Controller::curr(), "LeftAndMain")) {
			$this->owner->RestaurantPageID = RestaurantPage::getNearestRestaurantPageID();
		}

		parent::onBeforeWrite();
	}
}
