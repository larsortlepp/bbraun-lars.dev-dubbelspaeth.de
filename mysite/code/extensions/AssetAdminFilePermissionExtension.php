<?php

/* 
 * Adds canUpload() permissions to Files (and Folders)
 * Removes UploadButton, AddFolderButton and SyncButton based on current users
 * permission on the current folder (canView and canUpload permissions)
 * 
 */
class AssetAdminFilePermissionExtension extends Extension {
	
	/**
	 * Change visibility of UI elements, based on current users permissions
	 */
	public function updateEditForm($form) {
		// get current folder
		$folder = $this->owner->currentPage();
		$fields = $form->Fields();
		
		// remove TreeView if not admin
		// otherwise whole file site structure is visible for user
		// :TODO: use AssetAdmin::SiteTreeAsUL() function and add a custom $filterFunction baed on the permissions
		$member = Member::currentUser();
		if($member && !Permission::checkMember($member, "ADMIN")) {
			$fields->removeFieldFromTab('Root', 'TreeView');
		}
		
		// remove UI elements, based on user permissions
		if($folder) {
			if(!$folder->canEditFolder()) $fields->removeFieldFromTab('Root', 'DetailsView');
			if(!$folder->canEditFolder()) $fields->removeByName('AddFolderButton');
			if(!$folder->canUpload()) $fields->removeByName('UploadButton');
		}
	}
}
