<?php

/**
 * Adds site wide configuration options for the restaurant.
 * 1 Domain = 1 Restaurant = 1 SiteTree
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * activate this class:
 *
 *   SiteConfig:
 *     extensions:
 *       - MySiteConfigExtension
 * 
 * @requires MySiteConfigExtensionPermissions.php
 * 
 */
class MySiteConfigExtension extends DataExtension {

	static $db = array(
		'KeepDaysDailyMenuPages' => 'Int',
		'AllergenDisclaimer' => 'Text',
		'AllergenDisclaimer_en_US' => 'Text',
		'NutritivesUnit' => 'Varchar(255)',
		'NutritivesUnit_en_US' => 'Varchar(255)',
		'Currency' => 'Varchar(20)',
		'DefaultFolderType' => "Enum('Restaurant,Mandant,Global','Restaurant')",
		'DefaultFolderRestaurantLogo' => 'Varchar',
		'DefaultFolderDishImage' => 'Varchar',
		'DefaultFolderInfoMessage' => 'Varchar',
		'DefaultFolderPrint' => 'Varchar',
		'DefaultFolderMicrosite' => 'Varchar',
		'DefaultFolderFoodCategory' => 'Varchar',
		'DefaultFolderDishLabel' => 'Varchar',
		'DefaultFolderSpecialLabel' => 'Varchar',
		'DishesAutoCompleterResultsLimit' => 'Int',
		'EnableDishSearchForImportID' => 'Boolean',
		'ShowDishImportID' => 'Boolean'
	);
	
	static $defaults = array(
		'KeepDaysDailyMenuPages' => 14,
		'NutritivesUnit' => 'pro Portion',
		'NutritivesUnit_en_US' => 'per serving',
		'Currency' => 'EUR',
		'DefaultFolderRestaurantLogo' => 'Logos',
		'DefaultFolderType' => 'Restaurant',
		'DefaultFolderDishImage' => 'Fotos-Gerichte',
		'DefaultFolderInfoMessage' => 'News',
		'DefaultFolderPrint' => 'Druck',
		'DefaultFolderMicrosite' => 'Microsite',
		'DefaultFolderFoodCategory' => 'Essenskategorien',
		'DefaultFolderDishLabel' => 'Kennzeichnungen',
		'DefaultFolderSpecialLabel' => 'Aktionslabels',
		'DishesAutoCompleterResultsLimit' => 25,
		'EnableDishSearchForImportID' => 0,
		'ShowDishImportID' => 0
	);

	public function updateCMSFields(FieldList $f) {

		// Are we in the "main site" or a subsite?
		if (Subsite::CurrentSubsite()) {
			// Add subsite-specific fields
			$f->renameField('Title', 'Mandantenname');
			$f->addFieldToTab('Root.Main', new NumericField('KeepDaysDailyMenuPages',  'Speisepläne die älter als Anzahl Tage sind löschen (0 = keine Speisepläne löschen)'));
			$f->addFieldToTab('Root.Main', new HeaderField('NutritivesUnitHeader', 'Nährwerte'));
			$f->addFieldToTab('Root.Main', new Textfield('NutritivesUnit',  'Einheit für Nährwertangaben (z.B "pro 100g" oder "pro Portion")'));
			$f->addFieldToTab('Root.Main', new Textfield('NutritivesUnit_en_US',  'Einheit für Nährwertangaben englisch (z.B "per 100g" oder "per serving")'));
			$f->addFieldToTab('Root.Main', new HeaderField('CurrencyHeader', 'Währung'));
			$f->addFieldToTab('Root.Main', new Textfield('Currency',  'Währungseinheit (z.B "EUR")'));
			$f->addFieldToTab('Root.Main', new HeaderField('DefaultFolderHeader', 'Dateien'));
			$f->addFieldToTab('Root.Main', LiteralField::create('DefaultFolderMessage', '<p class="message warning"><strong>ACHTUNG: Konfiguration vor Einrichten von Restaurants durchführen!</strong><br />Änderungen generieren neue Verzeichnisse. Bestehende Dateien und Verzeichnisse werden nicht verschoben.</p>'));
			$f->addFieldToTab('Root.Main', DropdownField::create('DefaultFolderType',  'Verzeichnis Ebene', singleton('SiteConfig')->dbObject('DefaultFolderType')->enumValues())->setDescription('Legt fest in welcher Verzeichnis Ebene Dateien für &bdquo;Restaurant Logo&ldquo;, &bdquo;Fotos Gerichte&ldquo;, &bdquo;News&ldquo;, &bdquo;Druck&ldquo; und &bdquo;Microsite&ldquo; standardmäßig gespeichert werden'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderRestaurantLogo',  'Verzeichnis für Restaurant Logo'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderDishImage',  'Verzeichnis für Fotos Gerichte'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderInfoMessage',  'Verzeichnis für News'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderPrint',  'Verzeichnis für Druck'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderMicrosite',  'Verzeichnis für Microsite'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderFoodCategory',  'Verzeichnis für Essenskategrien')->setDescription('Verzeichnis wird immer unter aktuellem Restaurant angelegt.'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderDishLabel',  'Verzeichnis für Kennzeichnungen')->setDescription('Verzeichnis wird immer unter aktuellem Mandant angelegt.'));
			$f->addFieldToTab('Root.Main', Textfield::create('DefaultFolderSpecialLabel',  'Verzeichnis für Aktionslabels')->setDescription('Verzeichnis wird immer unter aktuellem Mandant angelegt.'));
			$f->addFieldToTab('Root.Main', new HeaderField('DailyMenuPageHeader', 'Speiseplan'));
			$f->addFieldToTab('Root.Main', new NumericField('DishesAutoCompleterResultsLimit', 'Max. Anzahl Suchergebnisse Gerichte in Speiseplan'));
			$f->addFieldToTab('Root.Main', FieldGroup::create('EnableDishSearchForImportIDGroup', CheckboxField::create('EnableDishSearchForImportID', 'ja'))->setTitle('Suche nach ImportID bei Gerichten aktivieren'));
			$f->addFieldToTab('Root.Main', FieldGroup::create('ShowDishImportIDInGridFieldGroup', CheckboxField::create('ShowDishImportID', 'ja'))->setTitle('ImportID der Gerichte anzeigen'));
			$f->removeFieldFromTab('Root.Main', 'Tagline');
		} else {
			// No sense in showing certain tabs in the main site
			$f->removeByName('Main');
			$f->removeByName('Access');
		}

		// Add tabs for global or subsite-specific DataObjects but only if we
		// have a real object ($this->owner->ID set)
		if ($this->owner->ID) {
			// (Global|Subsite)SpecialLabel
			MyGridField::create(
				Subsite::currentSubsite() ? 'SubsiteSpecialLabels' : 'GlobalSpecialLabels',
				Subsite::currentSubsite() ? 'Mandantenspezifische Aktionen' : 'Globale Aktionen',
				Subsite::currentSubsite() ? SubsiteSpecialLabel::get() : GlobalSpecialLabel::get()
			)
				->setCols(array(
					'Image_CMSThumbnail' => 'Bild',
					'Title' => 'Titel',
					'Title_en_US' => 'Titel (englisch)'
				))
				->setAllowCustomSort(true)
				->setAddNewButtonDataObjectType('Aktion')
				->addToTab($f, 'SpecialLabels');
			$f->fieldByName('Root.SpecialLabels')->setTitle('Aktionen');

			// (Global|Subsite)DishLabel
			MyGridField::create(
				Subsite::currentSubsite() ? 'SubsiteDishLabels' : 'GlobalDishLabels',
				Subsite::currentSubsite() ? 'Mandantenspezifische Kennzeichnungen' : 'Globale Kennzeichnungen',
				Subsite::currentSubsite() ? SubsiteDishLabel::get() : GlobalDishLabel::get()
			)
				->setCols(array(
					'Image_CMSThumbnail' => 'Bild',
					'Title' => 'Titel',
					'Title_en_US' => 'Titel (englisch)',
					'Color' => 'Farbe'
				))
				->setAllowCustomSort(true)
				->setAddNewButtonDataObjectType('Kennzeichnung')
				->addToTab($f, 'DishLabels');
			$f->fieldByName('Root.DishLabels')->setTitle('Kennzeichnungen');

			// (Global|Subsite)Additives
			MyGridField::create(
				Subsite::currentSubsite() ? 'SubsiteAdditives' : 'GlobalAdditives',
				Subsite::currentSubsite() ? 'Mandantenspezifische Zusatzstoffe' : 'Globale Zusatzstoffe',
				Subsite::currentSubsite() ? SubsiteAdditive::get() : GlobalAdditive::get()
			)
				->setCols(array_merge(
					array('Number' => 'Nr'),
					array('Title' => 'Zusatzstoff'),
					array('Title_en_US' => 'Zusatzstoff (englisch)')
				))
				->setAllowCustomSort(true)
				->setAddNewButtonDataObjectType('Zusatzstoff')
				->addToTab($f, 'Additives');
			$f->fieldByName('Root.Additives')->setTitle('Zusatzstoffe');

			// (Global|Subsite)Allergens
			MyGridField::create(
				Subsite::currentSubsite() ? 'SubsiteAllergens' : 'GlobalAllergens',
				Subsite::currentSubsite() ? 'Mandantenspezifische Allergene' : 'Globale Allergene',
				Subsite::currentSubsite() ? SubsiteAllergen::get() : GlobalAllergen::get()
			)
				->setCols(array_merge(
					array('Number' => 'Nr'),
					array('Title' => 'Zusatzstoff'),
					array('Title_en_US' => 'Zusatzstoff (englisch)')
				))
				->setAllowCustomSort(true)
				->setAddNewButtonDataObjectType('Allergen')
				->addToTab($f, 'Allergens');
			if (Subsite::currentSubsite()) {
				// Allergen Disclaimer
				$f->addFieldToTab(
					'Root.Allergens',
					new TextareaField(
						'AllergenDisclaimer',
						'Haftungsausschluss Allergene'
					)
				);
				$f->addFieldToTab(
					'Root.Allergens',
					new TextareaField(
						'AllergenDisclaimer_en_US',
						'Haftungsausschluss Allergene (englisch)'
					)
				);
			}
			$f->fieldByName('Root.Allergens')->setTitle('Allergene');
			
			/**
			 * Permissions
			 */
			if($f->fieldByName('Root.Main') && !Permission::check('VIEW_SITECONFIG_TAB_MAIN')) $f->removeFieldFromTab('Root', 'Main');
			if($f->fieldByName('Root.Access') && !Permission::check('VIEW_SITECONFIG_TAB_ACCESS')) $f->removeFieldFromTab('Root', 'Access');
			if($f->fieldByName('Root.SpecialLabels') && !Permission::check('VIEW_SITECONFIG_TAB_SPECIALLABELS')) $f->removeFieldFromTab('Root', 'SpecialLabels');
			if($f->fieldByName('Root.DishLabels') && !Permission::check('VIEW_SITECONFIG_TAB_DISHLABELS')) $f->removeFieldFromTab('Root', 'DishLabels');
			if($f->fieldByName('Root.Additives') && !Permission::check('VIEW_SITECONFIG_TAB_ADDITIVES')) $f->removeFieldFromTab('Root', 'Additives');
			if($f->fieldByName('Root.Allergens') && !Permission::check('VIEW_SITECONFIG_TAB_ALLERGENS')) $f->removeFieldFromTab('Root', 'Allergens');
			if($f->fieldByName('Root.Modules') && !Permission::check('VIEW_SITECONFIG_TAB_MODULES')) $f->removeFieldFromTab('Root', 'Modules');
			if($f->fieldByName('Root.Main') && !Permission::check('VIEW_SITECONFIG_FIELDS_DEFAULTFOLDER')) {
				$f->removeFieldFromTab('Root.Main', 'DefaultFolderHeader');
				$f->removeFieldsFromTab('Root.Main', array(
					'DefaultFolderHeader',
					'DefaultFolderMessage',
					'DefaultFolderType',
					'DefaultFolderDishImage',
					'DefaultFolderInfoMessage',
					'DefaultFolderPrint',
					'DefaultFolderFoodCategory',
					'DefaultFolderDishLabel',
					'DefaultFolderSpecialLabel'
				));
			}
		}
	}
	
	/**
	 * Returns Title as URLSegment
	 * For use as web safe folder names
	 */
	public function URLSegment() {
		return Convert::raw2url($this->owner->Title);
	}
	
	/**
	 * Returns the default folder path - without 'assets/'
	 * for use in UploadField::setFolderName()
	 * Based on the SiteConfig::DefaultFolderType
	 * Exmaple: 'mandant/restaurant/'
	 * The folder will automatically be created if it does not exist,
	 * when calling UploadField::setFolderName()
	 * 
	 * @param Integer $RestaurantPageID RestaurantPage ID for dtermining the path to the current Restaurant folder
	 * @return boolean|string Default folder path - without 'assets/'
	 */
	public function DefaultFolderPath($RestaurantPageID = null) {
		switch($this->owner->DefaultFolderType) {
			case 'Restaurant':
				if(!$RestaurantPageID) return false;
				$path = $this->owner->URLSegment().'/'.RestaurantPage::get()->filter('ID', $RestaurantPageID)->first()->URLSegment;
				break;
			case 'Mandant':
				$path = $this->owner->URLSegment();
				break;
			default:
				$path = ''; // root folder
		}
		return $path;
	}
	
	/**
	 * Default folder for Restaurant Logo
	 * Will be created based on SiteConfig::DefaultFolderType
	 * (in root, under mandator or under mandator and restaurant)
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/restaurant/Logos')
	 */
	public function DefaultFolderRestaurantLogo($RestaurantPageID = null) {
		return $this->owner->DefaultFolderPath($RestaurantPageID).'/'.$this->owner->DefaultFolderRestaurantLogo;
	}
	
	/**
	 * Default folder for Dish images
	 * Will be created based on SiteConfig::DefaultFolderType
	 * (in root, under mandator or under mandator and restaurant)
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/restaurant/Fotos-Gerichte')
	 */
	public function DefaultFolderDishImagePath($RestaurantPageID = null) {
		return $this->owner->DefaultFolderPath($RestaurantPageID).'/'.$this->owner->DefaultFolderDishImage;
	}
	
	/**
	 * Default folder for InfoMessage images
	 * Will be created based on SiteConfig::DefaultFolderType
	 * (in root, under mandator or under mandator and restaurant)
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/News')
	 */
	public function DefaultFolderInfoMessagePath($RestaurantPageID = null) {
		return $this->owner->DefaultFolderPath($RestaurantPageID).'/'.$this->owner->DefaultFolderInfoMessage;
	}
	
	/**
	 * Default folder for Print images
	 * Will be created based on SiteConfig::DefaultFolderType
	 * (in root, under mandator or under mandator and restaurant)
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/Druck')
	 */
	public function DefaultFolderPrintPath($RestaurantPageID = null) {
		return $this->owner->DefaultFolderPath($RestaurantPageID).'/'.$this->owner->DefaultFolderPrint;
	}
	
	/**
	 * Default folder for Microsite files
	 * Will be created based on SiteConfig::DefaultFolderType
	 * (in root, under mandator or under mandator and restaurant)
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/Microsite')
	 */
	public function DefaultFolderMicrositePath($RestaurantPageID = null) {
		return $this->owner->DefaultFolderPath($RestaurantPageID).'/'.$this->owner->DefaultFolderMicrosite;
	}
	
	/**
	 * Default folder for FoodCategory images
	 * Will always be created under current mandator and restaurant
	 * because FoodCategory are always specific to a RestaurantPage
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/restaurant/Essenskategorien')
	 */
	public function DefaultFolderFoodCategoryPath($RestaurantPageID = null) {
		if(!$RestaurantPageID) return false;
		return $this->owner->URLSegment().'/'.RestaurantPage::get()->filter('ID', $RestaurantPageID)->first()->URLSegment.'/'.$this->owner->DefaultFolderFoodCategory;
	}
	
	/**
	 * Default folder for DishLabel images
	 * Will always be created under current mandator
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/Kennzeichnungen')
	 */
	public function DefaultFolderDishLabelPath() {
		return $this->owner->URLSegment().'/'.$this->owner->DefaultFolderDishLabel;
	}
	
	/**
	 * Default folder for SpecialLabel images
	 * Will always be created under current mandator
	 * 
	 * @return string Name of folder for use in Folder::find_or_make() (without 'assets/' and ending slash '/': e.g. 'mandant/Kennzeichnungen')
	 */
	public function DefaultFolderSpecialLabelPath() {
		return $this->owner->URLSegment().'/'.$this->owner->DefaultFolderSpecialLabel;
	}
}

/**
 * Helper Class to add permissions to MySiteConfigExtension
 * 
 * Needed as we can´t implement PermissionProvider on an Extension or DataExtension
 * This class only declares permissions and has no further functionality
 * 
 * @requires MySiteConfigExtension.php
 *
 */
class MySiteConfigExtensionPermissions extends DataObject implements PermissionProvider {
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_SITECONFIG_TAB_MAIN' => array(
				'name' => 'Kann Tab "Hauptteil" betrachten',
				'category' => 'Einstellungen',
				'sort' => 10
			),
			'VIEW_SITECONFIG_TAB_ACCESS' => array(
				'name' => 'Kann Tab "Zugriff" betrachten',
				'category' => 'Einstellungen',
				'sort' => 20
			),
			'VIEW_SITECONFIG_TAB_SPECIALLABELS' => array(
				'name' => 'Kann Tab "Aktionen" betrachten',
				'category' => 'Einstellungen',
				'sort' => 30
			),
			'VIEW_SITECONFIG_TAB_DISHLABELS' => array(
				'name' => 'Kann Tab "Kennzeichnungen" betrachten',
				'category' => 'Einstellungen',
				'sort' => 40
			),
			'VIEW_SITECONFIG_TAB_ADDITIVES' => array(
				'name' => 'Kann Tab "Zusatzstoffe" betrachten',
				'category' => 'Einstellungen',
				'sort' => 50
			),
			'VIEW_SITECONFIG_TAB_ALLERGENS' => array(
				'name' => 'Kann Tab "Allergene" betrachten',
				'category' => 'Einstellungen',
				'sort' => 60
			),
			'VIEW_SITECONFIG_TAB_MODULES' => array(
				'name' => 'Kann Tab "Module" betrachten',
				'category' => 'Einstellungen',
				'sort' => 70
			),
			'VIEW_SITECONFIG_FIELDS_DEFAULTFOLDER' => array(
				'name' => 'Kann Felder für Standard Verzeichnisse betrachten',
				'category' => 'Einstellungen',
				'sort' => 100
			)
		);
	}
}
