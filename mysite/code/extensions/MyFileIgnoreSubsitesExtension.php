<?php
/**
 * This is an extension that modifies the subsites module's FileSubsites
 * extension, forcing all files and folders to be globally accessible
 * by all subsites.
 */
class MyFileIgnoreSubsitesExtension extends DataExtension {
	// Consider all created folders to be global
	static $default_root_folders_global = true;

	// Remove the Subsite ID editing field and message added by the
	// FileSubSites extension
	function updateCMSFields(FieldList $fields) {
		if($this->owner instanceof Folder) {
			$fields->removeByName('SubsiteID');
			$fields->removeByName('Message');
			$fields->push(new LiteralField(
				'GlobalAvailabilityNotice',
				'<p class="message notice">'.
				_t('CustomFileSubsites.SUBSITENOTICE', 'Folders and files are generally accessible by all subsites.')
				.'</p>'
			));
		}
	}

	// Disable Subsite Filtering
	function augmentSQL(SQLQuery &$query) {
	}

	// Set Subsite ID to 0 to make it globally accessible
	function onBeforeWrite() {
		$this->owner->SubsiteID = 0;
	}

	// Let SubSiteID stay at 0
	function onAfterUpload() {
	}
}
