<?php
/**
 * Extension adds $has_may relation to MicrositeLinkItems and MicrositeContentItems
 * and microsite specific functions (r.g. MicrositeMenu())
 */
class MicrositePageExtension extends DataExtension {
	
	private static $db = array(
		'Content_en_US' => 'HTMLText',
		'MicrositeLinkItemsTitle' => 'Varchar(255)',
		'MicrositeLinkItemsTitle_en_US' => 'Varchar(255)',
	);
	
	private static $has_many = array(
		'MicrositeLinkItems' => 'MicrositeLinkItem',
		'MicrositeContentItems' => 'MicrositeContentItem'
	);
	
	public function updateCMSFields($f) {
		
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		
		if($showLang_en_US) $f->addFieldToTab('Root.Main', HTMLEditorField::create('Content_en_US', 'Inhalt (englisch)'));
		
		// MicrositeLinkItems
		if(Permission::check('VIEW_MICROSITELINKITEM') || Permission::check('EDIT_MICROSITELINKITEM')) {
			
			$f->addFieldToTab('Root.Main', TextField::create('MicrositeLinkItemsTitle', 'Überschrift Links'));
			if($showLang_en_US) $f->addFieldToTab('Root.Main', TextField::create('MicrositeLinkItemsTitle_en_US', 'Überschrift Links (englisch)'));
			$f->addFieldToTab('Root.Main', $gfMicrositeLinkItems = GridField::create(
				'MicrositeLinkItems',
				'Links',
				$this->owner->MicrositeLinkItems(),
				GridFieldConfig_RecordEditor::create()
			));
			$configMicrositeLinkItems = $gfMicrositeLinkItems->getConfig();
			$configMicrositeLinkItems->addComponent(GridFieldOrderableRows::create());
			$configMicrositeLinkItems->getComponentByType('GridFieldPaginator')->setItemsPerPage(9999);
		}
		
		// MicrositeContentItems
		if(Permission::check('VIEW_MICROSITECONTENTITEM') || Permission::check('EDIT_MICROSITECONTENTITEM')) {
			$f->addFieldToTab('Root.Main', $gfMicrositeContentItems = GridField::create(
				'MicrositeContentItems',
				'Inhalts Blöcke',
				$this->owner->MicrositeContentItems(),
				GridFieldConfig_RecordEditor::create()
			));
			$configMicrositeContentItems = $gfMicrositeContentItems->getConfig();
			$configMicrositeContentItems->addComponent(GridFieldOrderableRows::create());
			$configMicrositeContentItems->getComponentByType('GridFieldPaginator')->setItemsPerPage(9999);
		}
	}
	
	public function MicrositeContainer() {
		$micrositeContainer = MicrositeContainer::get()
			->filter(array(
				'ParentID' => RestaurantPage::getNearestRestaurantPageID()
			))
			->limit(1)
			->first();
		return $micrositeContainer ? $micrositeContainer : false;
	}
	
	public function MicrositeHomePage() {
		$micrositeContainer = $this->owner->MicrositeContainer();
		if(!$micrositeContainer) return;
		$micrositeHomePage = MicrositeHomePage::get()
			->filter(array(
				'ParentID' => $micrositeContainer->ID
			))
			->limit(1)
			->first();
		if(!$micrositeHomePage) return;
		return $micrositeHomePage;
	}
	
	/**
	 * Returns the first level menu of the current Microsite
	 * 
	 * @return DataList
	 */
	public function MicrositeMenu() {
		$micrositeContainer = $this->owner->MicrositeContainer();
		if(!$micrositeContainer) return;
		return $micrositeContainer->Children();
	}
	
	/**
	 * Returns all MicrositeContainers of the current Subsite
	 * sorted by Title of their RestaurantPage
	 * Used to generate a Restaurant Dropdown for restaurant selection
	 * 
	 * @param String|Array Sort Order for Microsites, based on their RestaurantPage (fields must exist on RestaurantPage). Default: 'Title ASC'
	 * @return ArrayList All Microsites of the current Subsite, sorted by Title of their RestaurantPage
	 */
	public function AllMicrositesCurrentSubsite($sort = 'Title ASC') {
		$restaurantPages = RestaurantPage::get()->sort($sort);
		$microsites = ArrayList::create();
		foreach($restaurantPages as $restaurantPage) {
			$microsite = MicrositeContainer::get()->filter('ParentID', $restaurantPage->ID)->first();
			if($microsite) $microsites->push($microsite);
		}
		return $microsites;
	}
}
