<?php

/**
 * Class to manage DishRating DataObjects
 */

class DishRatingAdmin extends ModelAdmin {

	public static $managed_models = array(
		'DishRating'
	);

	private static $url_segment = 'dishrating'; // will be linked as /admin/products
	private static $menu_title = 'Gerichtebewertungen';

	public $showImportForm = false;

	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id, $fields);
		$gfCfg = $form->Fields()->fieldByName($this->sanitiseClassName($this->modelClass))->getConfig();

		// Return 'Created' Date in german Date Format (via DishRating->getSubmissionDate())
		$griddatacols = $gfCfg->getComponentByType('GridFieldDataColumns');
		$griddatacols->setFieldFormatting(array('Date' => '$SubmissionDate'));
		
		// remove Add DishRating button -> DishRatings should only be submitted via frontend
		$gfCfg->removeComponentsByType('GridFieldAddNewButton');

		// Replace GridFieldExportButton with MyGridFieldExportButton
		// which sanitizes the CSV values by removing line breaks
		$gfCfg->removeComponentsByType('GridFieldExportButton');
		$exportButton = new MyGridFieldExportButton('before');
		$exportButton->setExportColumns($this->getExportFields());
		$gfCfg->addComponent($exportButton);

		return $form;
	}

	/* DishRatings are subsite-aware, so we can show the menu option in subsites, too */
	public function subsiteCMSShowInMenu(){
		return true;
	}
}
