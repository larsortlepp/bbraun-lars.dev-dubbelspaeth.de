<?php

class ProcessTaskAdmin extends ModelAdmin {

    private static $managed_models = array(
		'ImportMenuXmlConfig',
		'ImportRestaurantConfig',
		'ImportVisitorCounterConfig',
		'ExportMenuXmlConfig',
		'ExportDisplayMenuXmlConfig',
		'ExportMenuHtmlConfig',
		'ImportFilterConfig'
    );

    private static $url_segment = 'tasks';

    private static $menu_title = 'Aufgabenplanung';
	
	/* DishRatings are subsite-aware, so we can show the menu option in subsites, too */
	public function subsiteCMSShowInMenu(){
		return true;
	}
	
	public function getEditForm($id = null, $fields = null) {

		$list = $this->getList();
		$exportButton = new GridFieldExportButton('buttons-before-left');
		$exportButton->setExportColumns($this->getExportFields());
		$listField = GridField::create(
			$this->sanitiseClassName($this->modelClass),
			false,
			$list,
			$fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
				->addComponent($exportButton)
				->removeComponentsByType('GridFieldFilterHeader')
				->addComponents(new GridFieldPrintButton('buttons-before-left'))
		);
		 
		  // Validation
         if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
             $detailValidator = singleton($this->modelClass)->getCMSValidator();
             $listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
         }
		 
		 // Remove "Add New" Button in "Hauptsite", 
		 // because we can´t add a RestaurantPage or MenuContainer here
		 // so we are unable to create a new ProcessTask
		  // Add custom fields for ImportMenuXmlConfig
		if(
			!Subsite::CurrentSubsite() &&
			(
				$this->modelClass == 'ImportMenuXmlConfig' ||
				$this->modelClass == 'ImportRestaurantConfig' ||
				$this->modelClass == 'ExportMenuXmlConfig' ||
				$this->modelClass == 'ExportDisplayMenuXmlConfig' ||
				$this->modelClass == 'ExportMenuHtmlConfig'
			)
		) {
			$listField->getConfig()->removeComponentsByType('GridFieldAddNewButton');
		}
		
		// Add custom fields for ImportMenuXmlConfig
		if(
			$this->modelClass == 'ImportFilterConfig'
		) {
			$listFields = array();

			// Adding Extra Fields
			
			// FoodCategoryBlacklistConfig
			if(Permission::check('CREATE_FOODCATEGORYBLACKLISTCONFIG') || Permission::check('VIEW_FOODCATEGORYBLACKLISTCONFIG') || Permission::check('EDIT_FOODCATEGORYBLACKLISTCONFIG')) {
				$listFields[] =  HeaderField::create('FoodCategoryBlacklistConfigHeader', FoodCategoryBlacklistConfig::$plural_name);
				$listFields[] = GridField::create(
					'FoodCategoryBlacklistConfig',
					false, 
					FoodCategoryBlacklistConfig::get(),
					GridFieldConfig_RecordEditor::create()
						->removeComponentsByType('GridFieldFilterHeader', '')  
				);
			}
			
			// AdditivesBlacklistConfig
			if(Permission::check('CREATE_ADDITIVESBLACKLISTCONFIG') || Permission::check('VIEW_ADDITIVESBLACKLISTCONFIG') || Permission::check('EDIT_ADDITIVESBLACKLISTCONFIG')) {
				$listFields[] = HeaderField::create('AdditivesBlacklistConfigHeader', AdditivesBlacklistConfig::$plural_name);

				$listFields[] = GridField::create(
					'AdditivesBlacklistConfig',
					false,
					AdditivesBlacklistConfig::get(),
					GridFieldConfig_RecordEditor::create()
						->removeComponentsByType('GridFieldFilterHeader')  
				);
			}
			
			// AllergensBlacklistConfig
			if(Permission::check('CREATE_ALLERGENSBLACKLISTCONFIG') || Permission::check('VIEW_ALLERGENSBLACKLISTCONFIG') || Permission::check('EDIT_ALLERGENSBLACKLISTCONFIG')) {
				$listFields[] = HeaderField::create('AllergensBlacklistConfigHeader', AllergensBlacklistConfig::$plural_name);

				$listFields[] = GridField::create(
					'AllergensBlacklistConfig',
					false, 
					AllergensBlacklistConfig::get(),
					GridFieldConfig_RecordEditor::create()
						->removeComponentsByType('GridFieldFilterHeader')  
				);
			}
			
			// DishLabelsBlacklistConfig
			if(Permission::check('CREATE_DISHLABELSBLACKLISTCONFIG') || Permission::check('VIEW_DISHLABELSBLACKLISTCONFIG') || Permission::check('EDIT_DISHLABELSBLACKLISTCONFIG')) {
				$listFields[] = HeaderField::create('DishlabelsBlacklistConfigHeader', DishLabelsBlacklistConfig::$plural_name);

				$listFields[] = GridField::create(
					'DishLabelsBlacklistConfig',
					false, 
					DishLabelsBlacklistConfig::get(),
					GridFieldConfig_RecordEditor::create()->removeComponentsByType('GridFieldFilterHeader')
				   );
			}
			
			// SpecialLabelsBlacklistConfig
			if(Permission::check('CREATE_SPECIALLABELSBLACKLISTCONFIG') || Permission::check('VIEW_SPECIALLABELSBLACKLISTCONFIG') || Permission::check('EDIT_SPECIALLABELSBLACKLISTCONFIG')) {
				$listFields[] = HeaderField::create('SpecialLabelsBlacklistConfigHeader', SpecialLabelsBlacklistConfig::$plural_name);

				$listFields[] = GridField::create(
					'SpecialLabelsBlacklistConfig',
					false, 
					SpecialLabelsBlacklistConfig::get(),
					GridFieldConfig_RecordEditor::create()->removeComponentsByType('GridFieldFilterHeader')  
				);
			}
			
			// NutritivesMapConfig
			if(Permission::check('CREATE_NUTRITIVESMAPCONFIG') || Permission::check('VIEW_NUTRITIVESMAPCONFIG') || Permission::check('EDIT_NUTRITIVESMAPCONFIG')) {
				$listFields[] = HeaderField::create('NutritivesMapConfigHeader', NutritivesMapConfig::$plural_name);

				$listFields[] = GridField::create(
					'NutritivesMapConfig',
					false, 
					NutritivesMapConfig::get(),
					GridFieldConfig_RecordEditor::create()->removeComponentsByType('GridFieldFilterHeader')  
				);
			}
		}
		else {
			$listFields = $listField;
		}
		$form = CMSForm::create(
			$this, 
			'EditForm', 
			new FieldList($listFields), 
			new FieldList()
		)
			->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
		$form->addExtraClass('cms-edit-form cms-panel-padded center');
		$form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
		$editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
		$form->setFormAction($editFormAction);
		$form->setAttribute('data-pjax-fragment', 'CurrentForm');

		$this->extend('updateEditForm', $form);

		return $form;
	}

	/**
     * Extension of original ModelAdmin::getManagedModels()
     * Implements permission checks on managed models and only shows models
     * that the member is allowed to view / edit based on PermissionProvider.
     * 
     * The Model must implement class PermissionProvider and
     * have a canView() and canEdit() function.
     * 
     * If that is not the case, the model will not be touched and stay in the $models array.
     * 
     * @todo When $models is an empty array we always get an error "Invalid Model Class"
     *       instead of being redirected to /admin/pages
     * 
     * @return array $models Map of class name to an array of 'title' (see {@link $managed_models})
     */
    public function getManagedModels() {
        $models = parent::getManagedModels();
        
        // remove model if it implements PermissionProvider and member has no permission set
        // only check if member is logged in - otherwise we don´t need to do permission checks
        // as user will be redirected to login by LeftAndMain::init()
        if(Member::logged_in_session_exists()) {
            foreach($models as $classname => $title) {
                // get all interfaces of current class (implements)
                $interfaces = class_implements($classname);
                if(
                    isset($interfaces['PermissionProvider']) &&
                    method_exists($classname, 'canView') && method_exists($classname, 'canEdit') &&
                    (!singleton($classname)->canView() && !singleton($classname)->canEdit())
                ) {
                    unset($models[$classname]);
                }
            }
        }
        
        return $models;
    }
}
