<?php

/**
 * Class to manage InfoMessageTemplate DataObjects
 * 
 * @todo Add custom 'Publish to Restaurants' Acrtion button. 
 *       See: https://www.silverstripe.org/community/forums/customising-the-cms/show/29132
 */

class InfoMessageTemplateAdmin extends ModelAdmin {

	public static $managed_models = array(
		'InfoMessageTemplateMobileTemplatePage',
		'InfoMessageTemplateMicrositeHomePage',
		'InfoMessageTemplateMicrositeNewsPage',
		'InfoMessageTemplateDisplayTemplatePageLandscape',
		'InfoMessageTemplateDisplayTemplatePagePortrait',
		'InfoMessageTemplatePrintTemplatePage'
	);

	private static $url_segment = 'news'; // will be linked as /admin/products
	private static $menu_title = 'News Vorlagen';

	public $showImportForm = false;

	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id, $fields);

		$listField = $form->Fields()->fieldByName($this->modelClass);
		if ($gridField = $listField->getConfig()->getComponentByType('GridFieldDetailForm'))
			$gridField->setItemRequestClass('InfoMessageTemplateGridFieldDetailForm_ItemRequest');

		return $form;
	}
}

class InfoMessageTemplateGridFieldDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest {
	
	private static $allowed_actions = array(
		'edit',
		'view',
		'ItemEditForm'
	);

	/**
	 * Add custom buttons, based on DataObjects getCMSActions() function
	 */
	public function ItemEditForm() {
		$form = parent::ItemEditForm();
		$formActions = $form->Actions();

		if ($actions = $this->record->getCMSActions())
			foreach ($actions as $action)
				$formActions->push($action);

		return $form;
	}

	/**
	 * Copy InfoMessageTemplate to every TemplatePage of the appropriate ClassName
	 * If a copy already exists, we skip creating a copy for this TemplatePage
	 */
	public function doCopyToTemplatePages($data, $form, $request) {
		
		$requestParams = $request->allParams();
		$sourceID = $requestParams['ID'];
		$sourceClassName = $requestParams['ModelClass'];
		
		// get InfoMessageTemplate
		$infoMessageTemplate = DataObject::get_by_id($sourceClassName, $sourceID);
		
		Debug::message("Getting InfoMessageTemplate: sourceID: $sourceID,  sourceClassName: $sourceClassName");
		Debug::show($infoMessageTemplate);

		if(!$infoMessageTemplate || !$infoMessageTemplate->ID)  {
			$form->sessionMessage("News Template wurde nicht gefunden. Bitte speichern Sie das Template vor dem Kopieren.", 'error');
		}
		else {

			Subsite::disable_subsite_filter();
			
			$infoMessageCopyCount = 0;
			
			// get InfoMessage ClassName from template
			$targetClass = $sourceClassName::$target_classname; 
			
			// get alle target relation classes from template (e.g. 'MobileTemplatePage')
			// that will hold the InfoMessage relation
			foreach($sourceClassName::$target_relation_classnames as $targetRelationClass) {
				
				Debug::message("Getting target relation class: $targetRelationClass for target class $targetClass");
			
				// get all TemplatePages
				foreach($targetRelationClass::get() as $targetRelationPage) {

					// skip copying if Infomessage already exists in $targetRelationPage
					if(
						$targetClass::get()->filter(array(
							$targetRelationClass.'ID' => $targetRelationPage->ID,
							$sourceClassName.'ID' => $sourceID
						))->Count() >= 1					 
					) {
						Debug::message("News Vorlage für $targetClass existiert bereits in Template ".$targetRelationPage->ID);
						continue;
					}

					// create new InfoMessage and copy Data from InfoMessageTemplate
					$infoMessage = $infoMessageTemplate->duplicate(false); // create a copy of the template without writing it to the database
					$infoMessage->ClassName = $targetClass; // set ClassName of InfoMessage
					$infoMessage->{$sourceClassName.'ID'} = $sourceID; // set relation to current InfoMessageTemplate
					$infoMessage->{$targetRelationClass.'ID'} = $targetRelationPage->ID; // assign $has_one relation to TemplatePage
					$infoMessage->SubsiteID = $targetRelationPage->SubsiteID; // set SubsiteID of assigned TemplatePage

					Debug::message("Neue InfoMessage $targetClass erstellt:");
					Debug::show($infoMessage);
					// Add InfoMessage with SortOrder '0' by default to make element, first element
					// insert new InfoMessage always at first position
					// augment SortOrder of all InfoMessages of the current TemplatePage,
					// recreate SortOrder of existing InfoMessages starting with 1
					$SortOrder = 1;
					foreach($targetRelationPage->InfoMessages()->sort('SortOrder') as $infoMessageSort) {
						$infoMessageSort->SortOrder = $SortOrder;
						$infoMessageSort->write();
						$SortOrder++;
					}
					// write InfoMessage to database so we can add TimerRecurringDaysOfWeek
					$infoMessage->write();
					// Copy TimerRecurringDaysOfWeek
					foreach($infoMessageTemplate->TimerRecurringDaysOfWeek() as $timer) {
						$infoMessage->TimerRecurringDaysOfWeek()->add($timer);
					}

					$infoMessageCopyCount++;
				}
			}
			Subsite::disable_subsite_filter(false);

			if($infoMessageCopyCount >= 1) {
				$form->sessionMessage("Die News ".$infoMessageTemplate->Title." wurde in $infoMessageCopyCount Ausgabe-Templates von Restaurants kopiert", 'good');
			}
			else {
				$form->sessionMessage("Die News ".$infoMessageTemplate->Title." wurde nicht kopiert. Entweder sind keine Ausgabe-Templates angelegt oder die News existiert bereits in den Ausgabe-Templates.", 'notice');
			}
		}
		
        if ($this->gridField->getList()->byId($this->record->ID)) {
            return $this->edit(Controller::curr()->getRequest());
        } else {
            $noActionURL = Controller::curr()->removeAction($data['url']);
            Controller::curr()->getRequest()->addHeader('X-Pjax', 'Content');
            return Controller::curr()->redirect($noActionURL, 302);
        }
	}

}
