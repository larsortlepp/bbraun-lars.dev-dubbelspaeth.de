<?php

/**
 * Extend UploadField
 * - setSort() to define sorting of Files in GridField view
 * - setItemsPerPage() to set the number of files per page in GridField
 * - larger preview images
 * - file permissions to show / hide UI elements (Upload button , etc.) 
 *   and folder structure in select list based on user permissions
 */
class CustomUploadField extends UploadField {
	
	/**
	 * @var array
	 */
	private static $allowed_actions = array(
		'upload',
		'attach',
		'handleItem',
		'handleSelect',
		'fileexists'
	);

	/**
	 * @var array
	 */
	private static $url_handlers = array(
		'item/$ID' => 'handleItem',
		'select' => 'handleSelect',
		'$Action!' => '$Action',
	);
	
	/**
	 *
	 * @var string|array
	 * @example 'Name' // default ASC sorting
     * @example 'Name DESC' // DESC sorting
     * @example 'Name', 'ASC'
     * @example array('Name'=>'ASC,'Age'=>'DESC')
	 */
	protected $sort = 'Title ASC';
	
	/**
	 * Files per page in GridField file view
	 * 
	 * @var integer
	 */
	protected $items_per_page = 8;
	
	/**
	 * Sorts the Files in GridField by one or more fields. You can either pass in a single
	 * field name and direction, or a map of field names to sort directions.
	 *
	 * Note that columns may be double quoted as per ANSI sql standard
	 *
	 * @see SS_List::sort()
     * @example $list->sort('Name'); // default ASC sorting
     * @example $list->sort('Name DESC'); // DESC sorting
     * @example $list->sort('Name', 'ASC');
     * @example $list->sort(array('Name'=>'ASC,'Age'=>'DESC'));
	 * 
	 * @param string|array $sort Sort order for files in GridField, default: 'Title ASC'
	 */
	public function setSort($sort) {
		$this->sort = $sort;
	}
	
	public function setItemsPerPage($items_per_page) {
		$this->items_per_page = $items_per_page;
	}
	
	/**
	 * Sets the upload folder name
	 * Sets visibility of UI elements, based on current users folder permissions
	 * (setCanPreviewFolder(), setCanAttachExisting(), setcanUpload())
	 * 
	 * @param string $folderName
	 * @return FileField Self reference
	 */
	public function setFolderName($folderName) {
		parent::setFolderName($folderName);
		
		// set visibility of UI items, based on current users folder permissions
		$folder = File::find($folderName);
		if($folder) {
			$this->setCanPreviewFolder($folder->canViewTypePermission()); // show / hide folder path in Upload field
			$this->setCanAttachExisting($folder->canViewTypePermission()); // set permissions to attach existing files (show / hide "From Files" button)
			$this->setCanUpload($folder->canUpload()); // set permissions for uploading new files into current folder (show / hide "Upload from computer" button)
		}
		return $this;
	}

	/**
	 * @param SS_HTTPRequest $request
	 * @return UploadField_ItemHandler
	 */
	public function handleSelect(SS_HTTPRequest $request) {
		if(!$this->canAttachExisting()) return $this->httpError(403);
		return CustomUploadField_SelectHandler::create($this, $this->getFolderName());
	}
}
	

class CustomUploadField_SelectHandler extends UploadField_SelectHandler {
	
	/**
	 * @param $folderID The ID of the folder to display.
	 * @return FormField
	 */
	public function getListField($folderID) {
		// Generate the folder selection field.
		$folderField = new TreeDropdownField('ParentID', _t('HtmlEditorField.FOLDER', 'Folder'), 'Folder');
		$folderField->setValue($folderID);
		
		// Remove all folders that the user can´t view
		// based on FilePermissionExtension::canViewType
		$folderField->setFilterFunction(function($obj) { 
			return $obj->canViewTypePermission(); 
		});
		
		// Generate the file list field.
		$config = GridFieldConfig::create();
		$config->addComponent(new GridFieldSortableHeader());
		$config->addComponent(new GridFieldFilterHeader());
		$config->addComponent($columns = new GridFieldDataColumns());
		$columns->setDisplayFields(array(
			'CMSThumbnail' => '', // bigger CMSThumbnail instead of StripThumbnail
			'Name' => 'Name',
			'Title' => 'Title'
		));
		$config->addComponent(new GridFieldPaginator($this->parent->items_per_page)); 

		// If relation is to be autoset, we need to make sure we only list compatible objects.
		$baseClass = $this->parent->getRelationAutosetClass();

		// Create the data source for the list of files within the current directory.
		$files = DataList::create($baseClass)->filter('ParentID', $folderID);

		$fileField = new GridField('Files', false, $files->Sort($this->parent->sort), $config); // custom sort
		$fileField->setAttribute('data-selectable', true);
		if($this->parent->getAllowedMaxFileNumber() !== 1) {
			$fileField->setAttribute('data-multiselect', true);
		}

		$selectComposite = new CompositeField(
			$folderField,
			$fileField
		);

		return $selectComposite;
	}
}
