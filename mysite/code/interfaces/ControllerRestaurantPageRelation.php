<?php

/**
 * Interface for Controllers that are not related to SiteTree
 * but need a relation to a RestaurantPage.
 * We can assign them a RestaurantPageID dynamically (e.g. from request parameters)
 * and also return the RestaurantPage dynamically for external functions.
 * 
 * This is especially needed if we have a call to RestaurantPage::getNearestRestaurantPage()
 * somewhere in the class or any subsequent execution.
 * E.g. Dishes always rely on a relation to a RestaurantPage.
 * 
 * We need to add the following properties to the class that implements this interface:
 * public $RestaurantPageID = null;
 * 
 */
interface ControllerRestaurantPageRelation {
	
	/**
	 * Initially set variables of this interface on current Object
	 * This function should call $this->setRestaurantPageID()
	 * and all other functions that might be needed additionaly.
	 * 
	 * $this->set_vars() must be called when initializing the object
	 * Best ways to do so are (based on Class):
	 * - Controller: handleAction()
	 * - ModelAdmin: getEditForm()
	 */
	public function set_vars();
	
	/**
	 * Assign RestaurantPageID to current controller in property $this->RestaurantPageID
	 * The RestaurantPageID will be fetched dynamically
	 * e.g. from the current controller request params 
	 */
	public function setRestaurantPageID();
	
	/**
	 * Returns the RestaurantPage based on $this->RestaurantPageID
	 * The function should contain:
	 * return RestaurantPage::get()->byID($this->RestaurantPageID);
	 * 
	 * @return RestaurantPage
	 */
	public function RestaurantPage();
	
}
