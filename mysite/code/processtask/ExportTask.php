<?php

class ExportTask extends ProcessTask implements ControllerRestaurantPageRelation {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ExportConfig';
	
	// Variables for INTERFACE ControllerRestaurantPageRelation
	
	public $RestaurantPageID = null;
	public $TaskConfigID = null;
	
	public static $allowed_actions = array (
		'task'
	);
	
	/**
	 * Method task() is for executing a single task identified by ApiKey
	 * Execution can be through button in TaskConfig CMS interface or by URL
	 * 
	 * Function must return string 'success' if everything was right.
	 * This will be used in the ajax request function to process error and success messages in CMS.
	 * 
	 * @return String $return 'success' if successfull, 'error' if error occured
	 */
	public function task($request) {
		
		$this->taskstartdatetime = date('Y-m-d H:i:s');
		$taskObjectClassName = $this->getTaskObjectClassName();
		Subsite::disable_subsite_filter($disabled = true);
		$taskconfig = $taskObjectClassName::get()->filter('ApiPublicKey', $this->getRequest()->param('ApiKey'))->first();
		if (!$taskconfig) return $this->redirect('/error-404.html', 404);
		if(Director::is_cli() || $this->can_run_single_task()){
			$success = $this->execute_task($taskconfig, $request);
			Subsite::disable_subsite_filter($disabled = false);
			return $success ? 'success' : 'error';
		}
		else {
			return Security::permissionFailure();
		}

	}
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
	}
	
	// INTERFACE ControllerRestaurantPageRelation
	
	/**
	 * Set variables on current Object
	 * This function should call $this->setRestaurantPageID()
	 * and all other functions that might be needed additionaly.
	 * 
	 * $this->set_vars() should then be called in (based on Class):
	 * - Controller: handleAction()
	 * - ModelAdmin: getEditForm()
	 */
	public function set_vars() {
		$this->setTaskConfigID();
		$this->setRestaurantPageID();
	}
	
	/**
	 * Assign RestaurantPageID dynamically based on current TaskConfig
	 * Function RestaurantPage() must be implemented in the *Config
	 */
	public function setRestaurantPageID() {
		$config = $this->TaskConfig();
		if(!$config) return;
		$RestaurantPageID = $config->RestaurantPage()->ID;
		$this->RestaurantPageID = is_numeric($RestaurantPageID) ? $RestaurantPageID : null;
	}
	
	/**
	 * Returns the RestaurantPage based on $this->RestaurantPageID
	 * The function should contain:
	 * return RestaurantPage::get()->byID($this->RestaurantPageID);
	 * 
	 * @return RestaurantPage
	 */
	public function RestaurantPage() {
		return RestaurantPage::get()->byID($this->RestaurantPageID);
	}
	
	/**
	 * Controller's default action handler.  It will call the method named in $Action, if that method exists.
	 * If $Action isn't given, it will use "index" as a default.
	 * 
	 * We set the static variables for PrintManagaerConfigId and RestaurantPageID
	 */
	protected function handleAction($request, $action) {
		$this->set_vars();
		
		return parent::handleAction($request, $action);
	}
	
	// Additional methods to assign and return the current PrintManagerConfig dynamically
	
	/**
	 * Assigns the ID for the current TaskConfig from the current URL
	 * Based on the $url_handler settings the ApiKey of the config 
	 * will be submitted as request parameter 'ID'
	 * 
	 * NOTE:
	 * ApiKey is not when running the 'index' task (processing all *Config.
	 * But in this case we do not need an ApiKey in URL, 
	 * because we will query all *Config objects from the database
	 * 
	 * Example URL:
	 * - mydomain.com/printmanager/task/12345abcde
	 */
	public function setTaskConfigID() {
		$ApiKey = Convert::raw2sql($this->getRequest()->param('ApiKey'));
		$taskConfigClassName = $this->getTaskObjectClassName();
		Subsite::disable_subsite_filter(true);
		$config = $taskConfigClassName::get()->filter('ApiPublicKey', $ApiKey)->limit(1)->first();
		Subsite::disable_subsite_filter(false);
		$this->TaskConfigID = $config ? $config->ID : null;
	}
	
	/**
	 * Returns the *Config for the ID that is given via request parameter
	 * 
	 * @return RestaurantPage
	 */
	public function TaskConfig() {
		$taskConfigClassName = $this->getTaskObjectClassName();
		return $taskConfigClassName::get()->byID($this->TaskConfigID);
	}
}
