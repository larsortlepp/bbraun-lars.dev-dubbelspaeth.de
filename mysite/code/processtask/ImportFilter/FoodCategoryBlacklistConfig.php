<?php

class FoodCategoryBlacklistConfig extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Filter Essenskategorie";

	public static $plural_name = "Filter Essenskategorien";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_many = array(
		'FoodCategoryBlacklistObjects' => 'FoodCategoryBlacklistObject'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				LiteralField::create(
					'Message',
					'<div class="message notice">Alle hier aufgeführten Essenskategorien werden beim Import nicht importiert</div>'
				),
				TextField::create(
					'Title',
					'Titel'
				)
					->setDescription('Interne Bezeichnung für Auswahl in Import Aufgabenplanung'),
				GridField::create(
					'FoodCategoryBlacklistObjects',
					FoodCategoryBlacklistObject::$plural_name, 
					$this->FoodCategoryBlacklistObjects(),
					GridFieldConfig_RecordEditor::create()   
				)					
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_FOODCATEGORYBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_FOODCATEGORYBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_FOODCATEGORYBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_FOODCATEGORYBLACKLISTCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_FOODCATEGORYBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 210
			),
			"VIEW_FOODCATEGORYBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 220
			),
			"EDIT_FOODCATEGORYBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 230
			),
			"DELETE_FOODCATEGORYBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 240
			)
		);
	}
}
