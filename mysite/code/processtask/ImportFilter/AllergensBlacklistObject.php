<?php

class AllergensBlacklistObject extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Allergen";

	public static $plural_name = "Allergene";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
		
	private static $has_one = array(
		'AllergensBlacklistConfig' => 'AllergensBlacklistConfig'
	);
	
	private static $field_labels = array(
		'Title' => 'Nummer / Bezeichnung'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				TextField::create(
					'Title',
					'Nummer / Bezeichnung'
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_ALLERGENSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_ALLERGENSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_ALLERGENSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_ALLERGENSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_ALLERGENSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AllergensBlacklistConfig::$singular_name.' erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 450
			),
			"VIEW_ALLERGENSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AllergensBlacklistConfig::$singular_name.' betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 460
			),
			"EDIT_ALLERGENSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AllergensBlacklistConfig::$singular_name.' bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 470
			),
			"DELETE_ALLERGENSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AllergensBlacklistConfig::$singular_name.' löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 480
			)
		);
	}
	
}
