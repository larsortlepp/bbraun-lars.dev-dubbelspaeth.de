<?php

class AdditivesBlacklistConfig extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Filter Zusatzstoff";

	public static $plural_name = "Filter Zusatzstoffe";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_many = array(
		'AdditivesBlacklistObjects' => 'AdditivesBlacklistObject'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				LiteralField::create(
					'Message',
					'<div class="message notice">Alle hier aufgeführten Zusatzstoffe werden beim Import nicht importiert</div>'
				),
				TextField::create(
					'Title',
					'Titel'
				)
					->setDescription('Interne Bezeichnung für Auswahl in Import Aufgabenplanung'),
				GridField::create(
					'AdditivesBlacklistObjects',
					AdditivesBlacklistObject::$plural_name, 
					$this->AdditivesBlacklistObjects(),
					GridFieldConfig_RecordEditor::create()
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_ADDITIVESBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_ADDITIVESBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_ADDITIVESBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_ADDITIVESBLACKLISTCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_ADDITIVESBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 310
			),
			"VIEW_ADDITIVESBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 320
			),
			"EDIT_ADDITIVESBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 330
			),
			"DELETE_ADDITIVESBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 340
			)
		);
	}
}
