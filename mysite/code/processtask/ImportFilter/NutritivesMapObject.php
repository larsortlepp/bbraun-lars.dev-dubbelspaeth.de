<?php

class NutritivesMapObject extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Nährwert";

	public static $plural_name = "Nährwerte";
	
	private static $db = array(
		'ExternalKey' => 'Varchar(255)',
		'InternalKey' => 'Varchar(255)'
	);
		
	private static $has_one = array(
		'NutritivesMapConfig' => 'NutritivesMapConfig'
	);
		
	private static $field_labels = array(
		'ExternalKey' => 'Import Bezeichnung',
		'InternalKey' => 'Interne Zuordnung'
	);
	
	private static $summary_fields = array(
		'ExternalKey',
		'InternalKey'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				TextField::create(
					'ExternalKey',
					'Import Bezeichnung'
				),
				TextField::create(
					'InternalKey',
					'Interne Zuordnung'
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_NUTRITIVESMAPOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_NUTRITIVESMAPOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_NUTRITIVESMAPOBJECT', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_NUTRITIVESMAPOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_NUTRITIVESMAPOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.NutritivesMapConfig::$singular_name.' erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 750
			),
			"VIEW_NUTRITIVESMAPOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.NutritivesMapConfig::$singular_name.' betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 760
			),
			"EDIT_NUTRITIVESMAPOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.NutritivesMapConfig::$singular_name.' bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 770
			),
			"DELETE_NUTRITIVESMAPOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.NutritivesMapConfig::$singular_name.' löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 780
			)
		);
	}
}
