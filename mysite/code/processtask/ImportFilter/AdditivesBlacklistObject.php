<?php

class AdditivesBlacklistObject extends DataObject implements PermissionProvider  {
	
	public static $singular_name = "Zusatzstoff";

	public static $plural_name = "Zusatzstoff";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_one = array(
		'AdditivesBlacklistConfig' => 'AdditivesBlacklistConfig'
	);

	private static $field_labels = array(
		'Title' => 'Nummer / Bezeichnung'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				TextField::create(
					'Title',
					'Nummer / Bezeichnung'
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_ADDITIVESBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_ADDITIVESBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_ADDITIVESBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_ADDITIVESBLACKLISTOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_ADDITIVESBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AdditivesBlacklistConfig::$singular_name.' erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 350
			),
			"VIEW_ADDITIVESBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AdditivesBlacklistConfig::$singular_name.' betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 360
			),
			"EDIT_ADDITIVESBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AdditivesBlacklistConfig::$singular_name.' bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 370
			),
			"DELETE_ADDITIVESBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.AdditivesBlacklistConfig::$singular_name.' löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 380
			)
		);
	}
	
}
