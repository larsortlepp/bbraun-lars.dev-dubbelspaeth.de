<?php

class ImportFilterConfig extends DataObject implements PermissionProvider {

	public static $singular_name = "Import Filter";
	
	public static $plural_name = "Import Filter";
	
	private static $searchable_fields = array();
	
	public function canView($member = null) {
        return Permission::check('VIEW_IMPORTFILTERCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"VIEW_IMPORTFILTERCONFIG" => array(
				'name' => 'Kann Tab "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 110
			)
		);
	}
	
}

