<?php

class DishLabelsBlacklistConfig extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Filter Kennzeichnung";

	public static $plural_name = "Filter Kennzeichnungen";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_many = array(
		'DishLabelsBlacklistObjects' => 'DishLabelsBlacklistObject'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				LiteralField::create(
					'Message',
					'<div class="message notice">Alle hier aufgeführten Kennzeichnungen werden beim Import nicht importiert</div>'
				),
				TextField::create(
					'Title',
					'Titel'
				)
					->setDescription('Interne Bezeichnung für Auswahl in Import Aufgabenplanung'),
				GridField::create(
					'DishlabelsBlacklistObjects',
					DishLabelsBlacklistObject::$plural_name, 
					$this->DishlabelsBlacklistObjects(),
					GridFieldConfig_RecordEditor::create()
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_DISHLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_DISHLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_DISHLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_DISHLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_DISHLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 510
			),
			"VIEW_DISHLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 520
			),
			"EDIT_DISHLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 530
			),
			"DELETE_DISHLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 540
			)
		);
	}
}
