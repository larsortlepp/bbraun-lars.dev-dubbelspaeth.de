<?php

class FoodCategoryBlacklistObject extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Essenskategorie";

	public static $plural_name = "Essenskategorien";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_one = array(
		'FoodCategoryBlacklistConfig' => 'FoodCategoryBlacklistConfig'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				TextField::create(
					'Title',
					'Titel'
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_FOODCATEGORYBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_FOODCATEGORYBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_FOODCATEGORYBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_FOODCATEGORYBLACKLISTOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_FOODCATEGORYBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.FoodCategoryBlacklistConfig::$singular_name.' erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 250
			),
			"VIEW_FOODCATEGORYBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.FoodCategoryBlacklistConfig::$singular_name.' betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 260
			),
			"EDIT_FOODCATEGORYBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.FoodCategoryBlacklistConfig::$singular_name.' bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 270
			),
			"DELETE_FOODCATEGORYBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.FoodCategoryBlacklistConfig::$singular_name.' löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 280
			)
		);
	}
	
}
