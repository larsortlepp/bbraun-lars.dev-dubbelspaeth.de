<?php

class AllergensBlacklistConfig extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Filter Allergen";

	public static $plural_name = "Filter Allergene";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_many = array(
		'AllergensBlacklistObjects' => 'AllergensBlacklistObject'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				LiteralField::create(
					'Message',
					'<div class="message notice">Alle hier aufgeführten Allergene werden beim Import nicht importiert</div>'
				),
				TextField::create(
					'Title',
					'Titel'
				)
					->setDescription('Interne Bezeichnung für Auswahl in Import Aufgabenplanung'),
				GridField::create(
					'AllergensBlacklistObjects',
					AllergensBlacklistObject::$plural_name, 
					$this->AllergensBlacklistObjects(),
					GridFieldConfig_RecordEditor::create()
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_ALLERGENSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_ALLERGENSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_ALLERGENSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_ALLERGENSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_ALLERGENSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 410
			),
			"VIEW_ALLERGENSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 420
			),
			"EDIT_ALLERGENSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 430
			),
			"DELETE_ALLERGENSBLACKLISTCONFIG" => array(
				'name' => 'Kann "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 440
			)
		);
	}
}
