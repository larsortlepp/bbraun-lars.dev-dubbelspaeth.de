<?php

class DishLabelsBlacklistObject extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Kennzeichnung";

	public static $plural_name = "Kennzeichnungen";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
		
	private static $has_one = array(
		'DishLabelsBlacklistConfig' => 'DishLabelsBlacklistConfig'
	);
		
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				TextField::create(
					'Title',
					'Titel'
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_DISHLABELSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_DISHLABELSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_DISHLABELSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_DISHLABELSBLACKLISTOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_DISHLABELSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.DishLabelsBlacklistConfig::$singular_name.' erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 550
			),
			"VIEW_DISHLABELSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.DishLabelsBlacklistConfig::$singular_name.' betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 560
			),
			"EDIT_DISHLABELSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.DishLabelsBlacklistConfig::$singular_name.' bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 570
			),
			"DELETE_DISHLABELSBLACKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.DishLabelsBlacklistConfig::$singular_name.' löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 580
			)
		);
	}
	
}
