<?php

class SpecialLabelsBlacklistObject extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Aktion";

	public static $plural_name = "Aktionen";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
		
	private static $has_one = array(
		'SpecialLabelsBlacklistConfig' => 'SpecialLabelsBlacklistConfig'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				TextField::create(
					'Title',
					'Titel'
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_SPECIALLABELSBLANKLISTOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_SPECIALLABELSBLANKLISTOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_SPECIALLABELSBLANKLISTOBJECT', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_SPECIALLABELSBLANKLISTOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_SPECIALLABELSBLANKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.SpecialLabelsBlacklistConfig::$singular_name.' erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 650
			),
			"VIEW_SPECIALLABELSBLANKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.SpecialLabelsBlacklistConfig::$singular_name.' betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 660
			),
			"EDIT_SPECIALLABELSBLANKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.SpecialLabelsBlacklistConfig::$singular_name.' bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 670
			),
			"DELETE_SPECIALLABELSBLANKLISTOBJECT" => array(
				'name' => 'Kann "'.$this::$singular_name.'" für '.SpecialLabelsBlacklistConfig::$singular_name.' löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 680
			)
		);
	}
	
}
