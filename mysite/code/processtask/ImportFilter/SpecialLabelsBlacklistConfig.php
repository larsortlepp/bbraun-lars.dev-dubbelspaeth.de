<?php

class SpecialLabelsBlacklistConfig extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Filter Aktion";

	public static $plural_name = "Filter Aktionen";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_many = array(
		'SpecialLabelsBlacklistObjects' => 'SpecialLabelsBlacklistObject'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				LiteralField::create(
					'Message',
					'<div class="message notice">Alle hier aufgeführten Aktionen werden beim Import nicht importiert</div>'
				),
				TextField::create(
					'Title',
					'Titel'
				)
					->setDescription('Interne Bezeichnung für Auswahl in Import Aufgabenplanung'),
				GridField::create(
					'SpecialLabelsBlacklistObjects',
					SpecialLabelsBlacklistObject::$plural_name, 
					$this->SpecialLabelsBlacklistObjects(),
					GridFieldConfig_RecordEditor::create()
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_SPECIALLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_SPECIALLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_SPECIALLABELSBLACKLISTCONFIG', 'any', $member); // call this without a function and have providePeröissions funtion
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_SPECIALLABELSBLACKLISTCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		// Doc: http://www.balbuss.com/adding-new-group-permissions/
		return array(
			"CREATE_SPECIALLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 610
			),
			"VIEW_SPECIALLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 620
			),
			"EDIT_SPECIALLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 630
			),
			"DELETE_SPECIALLABELSBLACKLISTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 640
			)
		);
	}
}
