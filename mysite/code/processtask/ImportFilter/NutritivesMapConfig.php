<?php

class NutritivesMapConfig extends DataObject implements PermissionProvider {
	
	public static $singular_name = "Zuordnung Nährwert";

	public static $plural_name = "Zuordnung Nährwerte";
	
	private static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $has_many = array(
		'NutritivesMapObjects' => 'NutritivesMapObject'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel'
	);
	
	private static $summary_fields = array(
		'Title'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				LiteralField::create(
					'Message',
					'<div class="message notice">Zuordnung von Nährwerten aus Import-Quelle</div>'
				),
				TextField::create(
					'Title',
					'Titel'
				)
					->setDescription('Interne Bezeichnung für Auswahl in Import Aufgabenplanung'),
				GridField::create(
					'NutritivesMapObjects',
					NutritivesMapObject::$plural_name, 
					$this->NutritivesMapObjects(),
					GridFieldConfig_RecordEditor::create()
				)
			)
		);
		
		return $f;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_NUTRITIVESMAPCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_NUTRITIVESMAPCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_NUTRITIVESMAPCONFIG', 'any', $member); // call this without a function and have providePeröissions funtion
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_NUTRITIVESMAPCONFIG', 'any', $member);
    }
	
	public function providePermissions() {
		// Doc: http://www.balbuss.com/adding-new-group-permissions/
		return array(
			"CREATE_NUTRITIVESMAPCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 710
			),
			"VIEW_NUTRITIVESMAPCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 720
			),
			"EDIT_NUTRITIVESMAPCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 730
			),
			"DELETE_NUTRITIVESMAPCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung - '.ImportFilterConfig::$singular_name,
				'help' => '',
				'sort' => 740
			)
		);
	}
}
