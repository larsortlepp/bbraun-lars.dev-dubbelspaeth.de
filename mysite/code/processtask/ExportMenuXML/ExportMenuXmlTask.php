<?php

class ExportMenuXmlTask extends ExportXmlTask {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ExportMenuXmlConfig';

	private static $url_handlers = array(
		'task/$ApiKey' => 'task'
    );
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
		$exportFunction = $taskconfig->ExportFunction;
		if(method_exists($this, $exportFunction)) return $this->$exportFunction($taskconfig);

	}
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		$result = Permission::check("RUN_ALL_EXPORTMENUXMLTASK");
		return  $result;
	}
	
	/**
	 * Provide permissions for current member to run the task "task()"
	 * 1. Member needs "RUN_SINGLE_EXPORTMENUXMLTASK" permission
	 * 2. Member needs permission to edit the RestaurantPage that the assigned MenuContainer belongs to
	 *  
	 * @return Boolean
	 */
	public function can_run_single_task() {
		$result = false;
		
		// $this->RestaurantPage() only exists if we run a single task by providing the ApiKey via URL
		// (e.g. mydomain.com/tasks/exportmenuhtmltask/task/123456789)
		// it will call ExportTask::setTaskConfigID() which will try to get the ApiKey
		// from the current $request
		$restaurantPage = $this->RestaurantPage();
		if($restaurantPage && $restaurantPage->exists()) {
			if(
				$this->can_run_task() ||
				(Permission::check("RUN_SINGLE_EXPORTMENUXMLTASK") && $restaurantPage->canEdit(Member::currentUser()))
			) {
				$result = true;
			}
		}
		return  $result;
	}
	
	public function export_default($taskconfig, $request = null) {
		
		// Doc creation
		$doc = $this->CreateDOMElementRoot();
		$root = $doc->documentElement;
		
		// create and append DOMElement <Weekdays>
		$WeekDays = $doc->createElement('WeekDays');
		$root->appendChild($WeekDays);
		
		// Getting DailyMenuPages
		$DailyMenuPages = $taskconfig->getDailyMenuPagesByDate();
		foreach ($DailyMenuPages as $DailyMenuPage) {
			
			// create and append DOMElement for DailyMenuPage
			$WeekDayElement = $this->CreateDOMElementDailyMenuPage($doc, $DailyMenuPage);
			$WeekDays->appendChild($WeekDayElement);
			
			// Create FoodCategories node for each day
			$FoodCategoriesElement = $doc->createElement('FoodCategories');
			
			// Appending Food Categories
			$FoodCategories = $DailyMenuPage->FoodCategories();
			foreach ($FoodCategories as $FoodCategorie) {
				
				// create DOMElement for FoodCategory
				$FoodCategory = $this->CreateDOMElementFoodCategory($doc, $FoodCategorie);
				
				// Create Dishes node for each FoodCategory
				$DishesElement = $doc->createElement('Dishes');
				
				// Appending Dishes
				$Dishs = $DailyMenuPage->DishesForCategory($FoodCategorie->ID);
				foreach ($Dishs as $Dish) {
					// create and append DOMElement <Dish>
					$DishElement = $this->RenderDOMNodeDish($doc, $Dish);
					$DishesElement->appendChild($DishElement);
				}
				// Append Dishes element to FoodCategory
				$FoodCategory->appendChild($DishesElement);
				
				// Append FoodCategory element to FoodCategories node
				$FoodCategoriesElement->appendChild($FoodCategory);
			}
			
			// Add Foodcategories element to current WeekDay
			$WeekDayElement->appendChild($FoodCategoriesElement);
		}
		$doc->saveXML();
		$baseFolder = Director::baseFolder();
		$exportFolder = $baseFolder . "/" . $taskconfig->Path;
		$exportPath = $exportFolder . $taskconfig->Name . "." . $taskconfig->file_extension();
		if (!file_exists($exportFolder)) {
			mkdir($exportFolder);
			chmod($exportFolder, 0777);
		}
		$doc->save($exportPath);
		// set correct permissions on file
		chmod($exportPath, 0777);
		return true;
	}
}
