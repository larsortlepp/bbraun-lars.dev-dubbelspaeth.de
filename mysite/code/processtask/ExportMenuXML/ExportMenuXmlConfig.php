<?php

class ExportMenuXmlConfig extends ExportConfig implements PermissionProvider  {

	public static $singular_name = "Export Speiseplan XML";

	public static $plural_name = "Export Speisepläne XML";
	
	private static $db = array (
		'ExportFunction' =>  "Enum('export_default','export_default')",
		'DatePeriod' => "Enum('Tage,Wochen','Tage')",
		'DateRange' => 'Int',
		'StartDateType' => "Enum('Aktuellem-Datum,Anfangsdatum','Aktuellem-Datum')",
		'StartDate' => 'Date'
    );
	
	private static $has_one = array(
		'MenuContainer' => 'MenuContainer'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv',
		
		'Name' => 'Dateiname',
		'Path' => 'Dateipfad',
		'Filename' => 'Datei',
		'AllowApiAccess' => 'API Zugriff',
		'AllowApiAccess.Nice' => 'API Zugriff',
		'ApiPublicKey' => 'API Key',
		
		'RestaurantPageTitle' => 'Restaurant',
		'MenuContainer.Title' => 'Speiseplan',
		'ExportFunction' =>  'Export Funktion',
		'DatePeriod' => 'Export Zeitraum',
		'DateRange' => 'Tage / Wochen',
		'StartDateType' => 'Export ab',
		'StartDate' => 'Anfangsdatum'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'RestaurantPageTitle',
		'MenuContainer.Title',
		'Title',
		'Filename',
		'ExportFunction',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'AllowApiAccess.Nice',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'MenuContainer.Title',
		'ExportFunction',
		'Title',
		'Name',
		'Path',
		'AllowApiAccess',
		'ApiPublicKey',
		'LastStarted',
		'LastRun'
	);
	
    public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// Add Button to execute task
		if(
			$this->ID && $this->MenuContainerID && $this->ExportFunction && $this->Name && $this->Path &&
			($this->canRunAllTask() || $this->canRunSingleTask())
		){
			// get the URL the Member is currently viewing (Director::absoluteBaseURL())
			// instead of the URL of the Subsite that the *Config belongs to
			// to avoid a new login to the different URL when downloading the file
			// (Logins are URL based - as they are stored in a Session Cookie per URL)
			$absoluteBaseURL = Director::absoluteBaseURL();
			$f->addFieldToTab('Root.Main', LiteralField::create('RunTask', '<a href="'.$absoluteBaseURL."tasks/exportmenuxml/task/".$this->ApiPublicKey.'" target="_blank" class="custom-button icon-arrows-ccw ajax-executeSingleProcessTask">'.$this::$singular_name.' ausführen</a>'), 'GroupField');
		}
		
		$menuContainers = MenuContainer::get();
		if($menuContainers->count() >= 1) {
			$menuContainersMod = ArrayList::create();
			foreach($menuContainers as $menuContainer) {
				$restaurantPage = $menuContainer->parent();
				$do = MenuContainer::create();
				$do->ID = $menuContainer->ID;
				$do->Title = $restaurantPage->Title.' - '.$menuContainer->Title;
				$menuContainersMod->push($do);
			}
			$f->addFieldToTab(
				'Root.Main',
				DropdownField::create(
					'MenuContainerID',
					'Speiseplan',
					$menuContainersMod->map('ID', 'Title')
				)
					->setEmptyString('Bitte auswählen'),
				'Name'
			);
		}
		else {
			$f->addFieldToTab(
				'Root.Main', 
				FieldGroup::create(
					'MenuContainerMessage',
					LiteralField::create(
						'MenuContainerMessage',
						'Der Speiseplan kann in der Hauptsite nicht geändert werden'
					)
				)
					->setTitle('Speiseplan'),
				'Name'
			);
		}
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'ExportFunction',
				'Export Funktion',
				$this->dbObject('ExportFunction')->enumValues()
			)
				->setEmptyString('Bitte auswählen'),
			'Name'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			HeaderField::create(
				'ExportSettingsHeader',
				'Export Einstellungen'
			),
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'DateRange',
				'Anzahl Tage / Wochen'
			), 
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'DatePeriod',
				'Export Zeitraum',
				$this->dbObject('DatePeriod')->enumValues()
			),
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'StartDateType',
				'Export ab',
				$this->dbObject('StartDateType')->enumValues()
			),
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			DateField::create(
				'StartDate',
				'Anfangsdatum'
			)
				->setDescription('(optional)')
				->setLocale(i18n::get_locale())
				->setConfig('showcalendar', true),
			'ApiHeader'
		);
		
		return $f;

    }
	
	/**
	 * Returns the RestaurantPage based on the MenuContainer relation
	 * 
	 * @return RestaurantPage|boolean
	 */
	public function RestaurantPage() {
		if(!$this->MenuContainerID) return false;
		$menuContainer = MenuContainer::get()->byID($this->MenuContainerID);
		return ($menuContainer && $menuContainer->exists()) ? $menuContainer->parent() : false;
	}
	
	/**
	 * Returns the title of the RestaurantPage based on the MenuContainer relation
	 * 
	 * @return string|boolean
	 */
	public function RestaurantPageTitle() {
		$restaurantPage = $this->RestaurantPage();
		return ($restaurantPage && $restaurantPage->exists()) ? $restaurantPage->Title : false;
	}
	
	/**
	 * Returns the start and end date for the TemplatePage based on the settings in the cms
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return Array $dates Array with 'StartDate' and 'EndDate'
	 */
	public function getDateRangeAsArray($datePeriod = null, $dateRange = null) {
		$dates = array();

		// if user is logged in and urlparameter 'showpreview' ist set -> use this date as start date
		$startDate = $this->getPreviewStartDate();

		if(!$startDate) {
			// set start date - depending on CMS setting: a) current date b) fix start date
			$startDate = $this->StartDateType == 'Aktuellem-Datum' ? DateUtil::currentDate() : $this->StartDate;
		}

		// get DateRange and DatePeriod from variables, otherwise from current TemplatePage
		$datePeriod = $datePeriod ? $datePeriod : $this->DatePeriod;
		$dateRange = $dateRange ? $dateRange : $this->DateRange;

		// calculate start- and end date for a) days or b) weeks
		if($datePeriod == 'Tage') {
			$dateRange = DateUtil::dayDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = ($dateRange > 1) ? $dateRange['EndDate'] :  null;
		}
		else {
			$dateRange = DateUtil::weekDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = $dateRange['EndDate'];
		}
		return $dates;
	}

	/*
	 * Returns DailyMenuPages for the current restaurant that fall within
	 * a given date range, sorted by date.
	 *
	 * @return DataList
	 */
	public function getDailyMenuPagesByDate() {
		$dateRangeArray = $this->getDateRangeAsArray();

		$menuContainer = $this->MenuContainer();

		return DailyMenuPage::get()->filter(array_merge(
			array('ParentID' => $menuContainer->ID),
			array('Date:GreaterThanOrEqual' => $dateRangeArray['StartDate']),
			$dateRangeArray['EndDate'] ? array('Date:LessThanOrEqual' => $dateRangeArray['EndDate']) : array()
		))->sort('Date');
	}
	
	public function selectedMenuContainer() {
		return $this->MenuContainerID;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_EXPORTMENUXMLCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_EXPORTMENUXMLCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_EXPORTMENUXMLCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_EXPORTMENUXMLCONFIG', 'any', $member);
    }
	
	public function canRunAllTask($member = null) {
        return Permission::check('RUN_ALL_EXPORTMENUXMLTASK', 'any', $member);
    }
	public function canRunSingleTask($member = null) {
        return Permission::check('RUN_SINGLE_EXPORTMENUXMLTASK', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_EXPORTMENUXMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 310
			),
			"VIEW_EXPORTMENUXMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 320
			),
			"EDIT_EXPORTMENUXMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 330
			),
			"DELETE_EXPORTMENUXMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 340
			),
			"RUN_ALL_EXPORTMENUXMLTASK" => array(
				'name' => 'Kann alle Aufgaben "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 350
			),
			"RUN_SINGLE_EXPORTMENUXMLTASK" => array(
				'name' => 'Kann einzelne Aufgabe "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 360
			)
		);
	}
}

