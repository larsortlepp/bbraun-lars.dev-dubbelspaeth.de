<?php

/**
 * Exports HTML data (including neccessary CSS, JS, etc.) by rendering it with
 * a template defined in the ExportMenuHtmlConfig and stores it as .zip file on the server.
 * Used to generate static menus (e.g. a weekly menu) that can be included or 
 * downloaded by external applications.
 * 
 * For template programming please follow the example under docs/templates.
 * The template file must be placed in the current theme in the templates folder.
 * e.g. /themes/mytheme/templates/
 */
class ExportMenuHtmlTask extends ExportTask {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ExportMenuHtmlConfig';
	
	public static $allowed_actions = array (
		'renderhtml'
	);

	private static $url_handlers = array(
		'task/$ApiKey' => 'task',
		'renderhtml/$ApiKey' => 'renderhtml'
    );
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
		return $this->export_default($taskconfig, $request);
	}
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		$result = Permission::check("RUN_ALL_EXPORTMENUHTMLTASK");
		return  $result;
	}
	
	/**
	 * Provide permissions for current member to run the task "task()"
	 * 1. Member needs "RUN_SINGLE_EXPORTMENUHTMLTASK" permission
	 * 2. Member needs permission to edit the Subsite that the assigned MenuContainer belongs to
	 *  
	 * @return Boolean
	 */
	public function can_run_single_task() {
		$result = false;
		
		// $this->RestaurantPage() only exists if we run a single task by providing the ApiKey via URL
		// (e.g. mydomain.com/tasks/exportmenuhtmltask/task/123456789)
		// it will call $this->setExportMenuHtmlConfigID() which will try to get the ApiKey
		// from the current $request
		// Member::currentUser() only exists if we call the task from the domain the Member has logged in
		$restaurantPage = $this->RestaurantPage();
		$member = Member::currentUser();
		if($restaurantPage && $restaurantPage->exists()) {
			if(
				$this->can_run_task() ||
				(Permission::check("RUN_SINGLE_EXPORTMENUHTMLTASK") && $restaurantPage->canEdit($member))
			) {
				$result = true;
			}
		}
		return  $result;
	}
	
	/**
	 * Render HTML view of the PrintManagerConfig
	 * Using the Theme and Template as set in ExportMenuHtmlConfig->TemplateName
	 * 
	 * @return String The rendered HTML template
	 */
	public function renderhtml() {
		
		$taskconfig = null;
		
		// get $taskconfig, based on ApiKey
		$taskObjectClassName = $this->getTaskObjectClassName();
		Subsite::disable_subsite_filter($disabled = true);
		$apiKey = $this->getRequest()->param('ApiKey');
		if ($apiKey) {
			$taskconfig = $taskObjectClassName::get()->filter('ApiPublicKey', $apiKey)->first();
		}
		if (!$apiKey || !$taskconfig || !$taskconfig->MenuContainerID) return $this->redirect('/error-404.html', 404);
		
		// No permission checks at this point, because renderhtml() will be called 
		// over the export_default() function 
		// and we don´t have a Member at this point to run permission checks
		// because we are requesting renderhtml() as anonymous DOMDocument::loadHTMLFile() function
		Subsite::disable_subsite_filter(true);
		$menuContainer = MenuContainer::get()->byID($taskconfig->MenuContainerID);
		$siteConfig = SiteConfig::get()->filter('SubsiteID', $menuContainer->SubsiteID)->limit(1)->first();
		Subsite::disable_subsite_filter(false);
		$template = $taskconfig->TemplateName;
		SSViewer::set_theme($siteConfig->Theme);
		return $taskconfig->renderWith($taskconfig->TemplateName);
	}
	
	
	
	/**
	 * Exports the given $taskconfig to a zip-file and stores the files on
	 * the server.
	 * The zip-file contains the HTML file and all linked assets in a subfolder.
	 * 
	 * ATTENTION: 
	 * If calling a task over the frontend (not as command line) 
	 * the Member requires permissions to access the Subsite and the task he is calling:
	 * Each task is rendered over the frontend controller, using the URL
	 * of the Subsite it belongs to.
	 * 
	 * @param type $taskconfig
	 * @param type $request
	 */
	public function export_default($taskconfig, $request = null) {
		
		$error = false;
		
		// load html file
		$doc = new DOMDocument();
		// Avoid any malformed HTML errors and
		// create and populate the array $http_response_header with HTTP status codes
		libxml_use_internal_errors(true);
		$docLoaded = @$doc->loadHTMLFile($taskconfig->renderHtmlLink());
		
		// Check if document was loaded properly
		// This will be false if we get a 404 error or anything else goes wrong
		if($docLoaded) {
			
			$folderPath = $taskconfig->AbsolutePath();
			$filePath = $folderPath.$taskconfig->Name.'.'.$taskconfig->file_extension();
			
			// create temporary folder
			$tmpFolderName = uniqid('html-export');
			$tmpFolderPath = $folderPath.$tmpFolderName;
			if (!file_exists($tmpFolderPath)) {
				@mkdir($tmpFolderPath, 0777, true);
			}
			
			// get head
			$head = $doc->getElementsByTagName('head')->item(0);
			// get baseHref
			$baseTag = $head->getElementsByTagName('base')->item(0);
			$baseHref = ($baseTag && $baseTag->hasAttribute('href')) ? $baseTag->getAttribute('href') : null;
			
			// Store and rewrite all CSS files in <head>
			$this->CollectAndRewriteTags('link', $head, $baseHref, $tmpFolderPath);
			
			// Store and rewrite all JavaScript files in <head>
			$this->CollectAndRewriteTags('script', $doc, $baseHref, $tmpFolderPath);
			
			// get and rewrite all CSS and JavaScript files in HTML comments in <head>
			// this is how we target files that are wrapped in conditional comments
			// @link http://stackoverflow.com/questions/3879108/php-domdocument-alter-conditional-comments
			foreach($head->childNodes as $node) {
				if($node instanceof DOMComment) {
					$html_comment = new DOMDocument();
					$html_comment->loadHTML($node->nodeValue);
					// save and rewrite css files in comments
					$this->CollectAndRewriteTags('link', $html_comment, $baseHref, $tmpFolderPath);
					// save and rewrite javascript files in comments
					$this->CollectAndRewriteTags('script', $html_comment, $baseHref, $tmpFolderPath);
					// create clean HTML string from replaced content of $html_comment
					// we need to replace all HTML tags that are added by DomDocument::saveHTML()
					// and do a html_entity_decode
					// @link http://php.net/manual/en/domdocument.savehtml.php#85165
					$newCommentValue = html_entity_decode(preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<head>', '</head>', '<body>', '</body>', '<p>', '</p>'), array('', '', '', ''), $html_comment->saveHTML())));
					// replcace string of comment
					$node->replaceData(0, strlen($node->nodeValue), $newCommentValue);
				}
			}
			
			// remove baseTag
			if($baseHref) {
				$baseTag->parentNode->removeChild($baseTag);
			}
			
			// save modified html data to temporary folder
			if(file_put_contents($tmpFolderPath.'/index.html', $doc->saveHTML()) === false) $error = "Fehler beim Schreiben der HTML Datei";
			
			// set permissions on all files recursively
			exec("chmod -R 755 $tmpFolderPath");
			
			// zip all files in temporary folder
			if(!$error) {
				$this->createZipFile($tmpFolderPath, $filePath);
				exec("chmod -R 777 $filePath");
			}
			
			// remove temporary folder
			$this->deleteTree($tmpFolderPath);
			return ($error) ? false : true;
			
		}
		return false;
	}
	
	/**
	 * Save CSS or Javascript files to a temporary folder ($targetFolder)
	 * and rewrite their links to just the filename (stripping the path).
	 * Function selects all tags with $tagName from the given $domElement
	 * 
	 * @param String $tagName 'link' to target all CSS files or 'script' to target all JavaScript files
	 * @param DOMNode $domElement A DomNode whose content will be processed
	 * @param String $baseHref The base url part that will be prepended to the link
	 * @param String $targetFolder The absolute path to the target folder where the files will be saved to
	 */
	public function CollectAndRewriteTags($tagName, $domElement, $baseHref, $targetFolder) {
		
		// set attributes for $tagName
		$tagAttributes = array(
			// CSS files
			'link' => array(
				'TypeAttribute' => 'rel',
				'TypeValue' => 'stylesheet',
				'LinkAttribute' => 'href'
			),
			// Javascript files
			'script' => array(
				'TypeAttribute' => 'type',
				'TypeValue' => 'text/javascript',
				'LinkAttribute' => 'src'
			)
		);
		
		if(!array_key_exists($tagName, $tagAttributes)) return false;
		
		$allTags = $domElement->getElementsByTagName($tagName);
		foreach($allTags as $tag) {
			// process if the tag has the needed attribute and value and a link attribute
			if(
				$tag->hasAttribute($tagAttributes[$tagName]['TypeAttribute']) && 
				strtolower($tag->getAttribute($tagAttributes[$tagName]['TypeAttribute'])) == $tagAttributes[$tagName]['TypeValue'] && 
				$tag->hasAttribute($tagAttributes[$tagName]['LinkAttribute'])
			) {
				$tagLink = $tag->getAttribute($tagAttributes[$tagName]['LinkAttribute']);
				$absolutePath = $this->createAbsolutePath($tagLink, $baseHref);

				// save file to temporary folder, change link 'href' attribute
				if($absolutePath['absolutepath']) {
					$path_parts = pathinfo($tagLink);
					$filename = $path_parts['basename']; // get the filename + extension from full path
					$targetPath = $targetFolder.'/'.$filename; 
					$this->saveFileTo($absolutePath['absolutepath'], $targetPath, $absolutePath['islocalpath']);
					// modify href of link to be relative
					$tag->setAttribute($tagAttributes[$tagName]['LinkAttribute'], $filename);
				}

				// get further external ressources for CSS files
				// indicated by data-additional-resources-relative attribute
				// contains a comma seperated string of external resources which can be folders or files (e.g. "myfolder/mysubfolder,myfolder/myfile.ext")
				// file paths must be relative to the current css-file (exist in subfolders)
				if($tagName == 'link' && $tag->hasAttribute('data-additional-resources-relative')) {
					$additionalResources = explode(',', $tag->getAttribute('data-additional-resources-relative'));
					foreach($additionalResources as $additionalRessource) {
						$additionalRessourcePath = trim($additionalRessource);
						// get path to current css file to create the full path to additional ressource which is placed in a folder relative to css-file
						$currentPath_parts = pathinfo($tagLink);
						$currentPath = $currentPath_parts['dirname'];
						$additionalRessourceAbsolutePath_array = $this->createAbsolutePath($currentPath.'/'.$additionalRessourcePath, $baseHref);
						$additionalRessourceAbsolutePath = $additionalRessourceAbsolutePath_array['absolutepath'];

						// get parts of path of external file an create new target path (relative to css-file)
						$additionalResourcePath_parts = pathinfo($additionalRessourceAbsolutePath);
						$additionalRessourceTargetPath = $targetFolder.'/'.$additionalRessourcePath;

						// path is file (has an extension) => save file
						if($additionalResourcePath_parts['extension']) {
							$this->saveFileTo($additionalRessourceAbsolutePath, $additionalRessourceTargetPath, $absolutePath['islocalpath']);
						}
						// path is folder => save folder and subfolders
						else {
							$this->saveFolderTo($additionalRessourceAbsolutePath, $additionalRessourceTargetPath, $absolutePath['islocalpath']);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Get file from local or external URL via CURL
	 * and save it to the local file system.
	 * If the local file does not exist, it will be created,
	 * if it exists it will be overwritten.
	 * If the folder path does not exist, it will be created.
	 * 
	 * @param String $source URL or local file path of source file
	 * @param String $destination local file path (relative or absolute) that the file will be saved to
	 * @param Boolean $localSource true if the source file is a local file (not a URL). default: false
	 */
	public function saveFileTo($source = null, $destination = null, $localSource = false) {
		if (!$source || !$destination) return false;
		$destinationFolderParts = pathinfo($destination);
		if (!file_exists($destinationFolderParts['dirname'])) {
			@mkdir($destinationFolderParts['dirname'], 0777, true);
		}
		$sourcefile = file_get_contents($source, $localSource);
		file_put_contents($destination, $sourcefile);
	}
	
	/**
	 * Get $source folder and copy all files and subfolders recursively to $destination
	 * If $recursive is set to false, no subfolders will be copied.
	 * 
	 * @param String $source local file path of source file
	 * @param String $destination local file path (relative or absolute) that the file will be saved to
	 * @param Boolean $recursive true if alls subfolders should be copied recursively default: true
	 */
	public function saveFolderTo($source = null, $destination = null, $recursive = true) {
		$dir = opendir($source);
		@mkdir($destination); 
		while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
				if ( $recursive && is_dir($source . '/' . $file) ) {
					$this->saveFolderTo($source . '/' . $file, $destination . '/' . $file); 
				} 
				else {
					$this->saveFileTo($source . '/' . $file,$destination . '/' . $file);
				} 
			} 
		} 
		closedir($dir); 
	}
	
	/**
	 * Delete given directory recursively (including subfolders and files
	 * 
	 * @param type $dir
	 * @return type
	 */
	public function deleteTree($dir) {
		$files = array_diff(scandir($dir), array('.', '..'));
		foreach ($files as $file) {
			if(is_dir("$dir/$file")) {
				$this->deleteTree("$dir/$file");
			}
			else {
				unlink("$dir/$file");
			}
		}
		return @rmdir($dir);
	}

	/**
	 * Creates absolute path by adding the base folder (from server root) for local paths
	 * or the baseHref (given by $baseHref attribute) for external files.
	 * If $path is local or external is determined by leading '/' in pathname
	 * if leading '/' exists we assume we are working with a local file.
	 * Then we also add 'islocalpath' = true to return value.
	 * 
	 * @param string $path path to a file on local filesystem or via external url
	 * @param string $baseHref (optional) the base url for external files (e.g. http://www.myurl.com/). Will be used for generating external files paths
	 * @return array $return Array('absolutepath' => Absolute path to file, 'islocalpath' => true if path is on local file system (not a external url))
	 */
	public function createAbsolutePath($path = null, $baseHref = null) {
		$return = array(
			'absolutepath' => false,
			'islocalpath' => false
		);
		
		if(!$path) return $return;
		
		// check if link is absolute to web root (with leading '/') or relative (requires <base> tag in head)
		if($this->isPathFromRoot($path)) {
			// absolute path so we can add the Basefolder
			$return['absolutepath'] = Director::baseFolder() . $path;
			$return['islocalpath'] = true;
		}
		else if($baseHref) {
			$return['absolutepath'] = $baseHref.$path;
		}
		return $return;
	}
	
	/**
	 * If $path begins with '/' it is recognised as 'path from root'
	 * then true will be returned, else false
	 * 
	 * @param string $path Absolute or relative path name or url
	 * @return boolean
	 */
	public function isPathFromRoot($path = null) {
		return (substr($path, 0, 1) == '/') ? true : false;
	}
	
	/**
	 * Creates a zip file from the $source folder (and subfolders and files)
	 * and stores to the destination path
	 * 
	 * @param String $source local file path of source file
	 * @param String $destination local file path (relative or absolute) that the file will be saved to 
	 */
	public function createZipFile($source =  null, $destination = null) {
		if (!extension_loaded('zip') || !file_exists($source)) {
			return false;
		}
		
		// delete file if it exists
		if(file_exists($destination)) unlink ($destination);

		$zip = new ZipArchive();
		if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
			return false;
		}

		$source = str_replace('\\', '/', realpath($source));

		if (is_dir($source) === true)
		{
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

			foreach ($files as $file)
			{
				$file = str_replace('\\', '/', $file);

				// Ignore "." and ".." folders
				if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
					continue;

				$file = realpath($file);

				if (is_dir($file) === true)
				{
					$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
				}
				else if (is_file($file) === true)
				{
					$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
				}
			}
		}
		else if (is_file($source) === true)
		{
			$zip->addFromString(basename($source), file_get_contents($source));
		}

		return $zip->close();
	}	
}
