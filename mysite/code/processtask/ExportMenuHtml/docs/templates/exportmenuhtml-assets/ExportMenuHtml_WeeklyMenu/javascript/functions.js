$(document).ready(function() {
		
	$('.toggle-nutritives input').click(function(){					
		$('body').toggleClass('hide-nutritives');
	});
	
	$('.toggle-nutritives span').click(function(){	
		var cb = $('.toggle-nutritives').find(':checkbox');
		cb.prop("checked", !cb.prop("checked"));		
		$('body').toggleClass('hide-nutritives');
	});

	// accordion: open current day
	date = new Date();
	day = date.getDay();
	currentDay = day-1; // accordion requires index start at 0, not at 1
		
	$('#accordion').accordion({
		active: currentDay,
		header: ".accordion-header-trigger",
		collapsible: true,
		autoHeight: false,
		heightStyle: "content"
	});

	// tabs in footer
	$('.footnote-tabs').tabs();

});