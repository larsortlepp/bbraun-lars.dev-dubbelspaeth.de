<?php

class ExportMenuHtmlConfig extends ExportConfig implements PermissionProvider  {

	public static $singular_name = "Export Speiseplan HTML";

	public static $plural_name = "Export Speisepläne HTML";
	
	private static $db = array (
		'TemplateName' =>  "Varchar(255)",
		'DatePeriod' => "Enum('Tage,Wochen','Tage')",
		'DateRange' => 'Int',
		'StartDateType' => "Enum('Aktuellem-Datum,Anfangsdatum','Aktuellem-Datum')",
		'StartDate' => 'Date'
    );
	
	private static $has_one = array(
		'MenuContainer' => 'MenuContainer'
	);
	
	private static $many_many = array(
		'FoodCategories' => 'FoodCategory'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv',
		
		'Name' => 'Dateiname',
		'Path' => 'Dateipfad',
		'Filename' => 'Datei',
		'AllowApiAccess' => 'API Zugriff',
		'AllowApiAccess.Nice' => 'API Zugriff',
		'ApiPublicKey' => 'API Key',
		
		'RestaurantPageTitle' => 'Restaurant',
		'MenuContainer.Title' => 'Speiseplan',
		'TemplateName' =>  'Template Name',
		'DatePeriod' => 'Export Zeitraum',
		'DateRange' => 'Tage / Wochen',
		'StartDateType' => 'Export ab',
		'StartDate' => 'Anfangsdatum'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'RestaurantPageTitle',
		'MenuContainer.Title',
		'Title',
		'Filename',
		'TemplateName',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'AllowApiAccess.Nice',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'MenuContainer.Title',
		'TemplateName',
		'Title',
		'Name',
		'Path',
		'AllowApiAccess',
		'ApiPublicKey',
		'LastStarted',
		'LastRun'
	);
	
	/**
	 * File extension for the export file
	 * @var string
	 */
	public static $file_extension = 'zip';
	
    public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// get MenuContainer for the current *Config
		$currMenuContainer = $currSubsite = false;
		if($this->MenuContainerID) {
			$currMenuContainer = MenuContainer::get()->byID($this->MenuContainerID);
		}
		
		// Add Button to execute task
		if(
			$this->ID && $this->MenuContainerID && $this->TemplateName && $this->Name && $this->Path &&
			($this->canRunAllTask() || $this->canRunSingleTask())
		){
			// get the URL the Member is currently viewing (Director::absoluteBaseURL())
			// instead of the URL of the Subsite that the *Config belongs to
			// to avoid a new login to the different URL when downloading the file
			// (Logins are URL based - as they are stored in a Session Cookie per URL)
			$absoluteBaseURL = Director::absoluteBaseURL();
			$f->addFieldToTab('Root.Main', LiteralField::create('RunTask', '<a href="'.$absoluteBaseURL."tasks/exportmenuhtml/task/".$this->ApiPublicKey.'" target="_blank" class="custom-button icon-arrows-ccw ajax-executeSingleProcessTask">'.$this::$singular_name.' ausführen</a>'), 'GroupField');
		}
		
		$menuContainers = MenuContainer::get();
		if($menuContainers->count() >= 1) {
			$menuContainersMod = ArrayList::create();
			foreach($menuContainers as $menuContainer) {
				$restaurantPage = $menuContainer->parent();
				$do = MenuContainer::create();
				$do->ID = $menuContainer->ID;
				$do->Title = $restaurantPage->Title.' - '.$menuContainer->Title;
				$menuContainersMod->push($do);
			}
			$f->addFieldToTab(
				'Root.Main',
				DropdownField::create(
					'MenuContainerID',
					'Speiseplan',
					$menuContainersMod->map('ID', 'Title')
				)
					->setEmptyString('Bitte auswählen'),
				'Name'
			);
		}
		else {
			$f->addFieldToTab(
				'Root.Main', 
				FieldGroup::create(
					'MenuContainerMessage',
					LiteralField::create(
						'MenuContainerMessage',
						'Der Speiseplan kann in der Hauptsite nicht geändert werden'
					)
				)
					->setTitle('Speiseplan'),
				'Name'
			);
		}
		$f->addFieldToTab(
			'Root.Main', 
			TextField::create(
				'TemplateName',
				'Template Name'
			)
				->setDescription('Dateiname des Templates (ohne Endung .ss)'),
			'Name'
		);
		
		
		// FoodCategories
		if($this->MenuContainerID) {
			$foodCategoriesMap = ($foodCategories = FoodCategory::get()->filter('RestaurantPageID', $currMenuContainer->parent->ID)) ? $foodCategories->map()->toArray() : false;
			if($foodCategoriesMap) {
				$f->addFieldToTab(
					'Root.Main',
					CheckboxSetField::create(
						'FoodCategories',
						'Essenskategorien',
						$foodCategoriesMap
					),
					'ApiHeader'
				);
			}
		}
		else {
			$f->addFieldToTab(
				'Root.Main',
				LiteralField::create(
					'FoodCategories',
					'<div class="message notice">Bitte zunächst einen Speiseplan auswählen um Essenskategorien zuzuweisen.</div>'
				),
				'ApiHeader'
			);
		}
		
		$f->addFieldToTab(
			'Root.Main',
			HeaderField::create(
				'ExportSettingsHeader',
				'Export Einstellungen'
			),
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'DateRange',
				'Anzahl Tage / Wochen'
			), 
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'DatePeriod',
				'Export Zeitraum',
				$this->dbObject('DatePeriod')->enumValues()
			),
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'StartDateType',
				'Export ab',
				$this->dbObject('StartDateType')->enumValues()
			),
			'ApiHeader'
		);
		$f->addFieldToTab(
			'Root.Main',
			DateField::create(
				'StartDate',
				'Anfangsdatum'
			)
				->setDescription('(optional)')
				->setLocale(i18n::get_locale())
				->setConfig('showcalendar', true),
			'ApiHeader'
		);
		
		return $f;

    }
	
	/**
	 * Returns absolute URL including the ApiPublicKey for html view of this *Config
	 * Absolute URL is created using Subsite->absoluteBaseURL(), 
	 * based on the Subsite of assigned MenuContainer.
	 * 
	 * @return String
	 */
	public function renderHtmlLink() {
		$menuContainer = $this->MenuContainer();
		$subsite = Subsite::get()->byID($menuContainer->SubsiteID);
		return $subsite ? $subsite->absoluteBaseURL()."tasks/exportmenuhtml/renderhtml/".$this->ApiPublicKey : false;
	}
	
	/**
	 * Returns the absolute Path for the folder
	 * (based on $this->Path)
	 * 
	 * @return String
	 */
	public function AbsolutePath() {
		return Director::baseFolder().'/'.$this->Path;
	}
	
	public function selectedMenuContainer() {
		return $this->MenuContainerID;
	}
	
	/**
	 * Returns the RestaurantPage based on the MenuContainer relation
	 * Required for ExportTask::setRestaurantPageID() to work
	 * 
	 * @return RestaurantPage|boolean
	 */
	public function RestaurantPage() {
		if(!$this->MenuContainerID) return false;
		$menuContainer = MenuContainer::get()->byID($this->MenuContainerID);
		return ($menuContainer && $menuContainer->exists()) ? $menuContainer->parent() : false;
	}
	
	/**
	 * Returns the title of the RestaurantPage based on the MenuContainer relation
	 * 
	 * @return string|boolean
	 */
	public function RestaurantPageTitle() {
		$restaurantPage = $this->RestaurantPage();
		return ($restaurantPage && $restaurantPage->exists()) ? $restaurantPage->Title : false;
	}
	
	/**
	 * Returns the start and end date for the TemplatePage based on the settings in the cms
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return Array $dates Array with 'StartDate' and 'EndDate'
	 */
	public function getDateRangeAsArray($datePeriod = null, $dateRange = null) {
		$dates = array();

		// if user is logged in and urlparameter 'showpreview' ist set -> use this date as start date
		$startDate = $this->getPreviewStartDate();

		if(!$startDate) {
			// set start date - depending on CMS setting: a) current date b) fix start date
			$startDate = $this->StartDateType == 'Aktuellem-Datum' ? DateUtil::currentDate() : $this->StartDate;
		}

		// get DateRange and DatePeriod from variables, otherwise from current TemplatePage
		$datePeriod = $datePeriod ? $datePeriod : $this->DatePeriod;
		$dateRange = $dateRange ? $dateRange : $this->DateRange;

		// calculate start- and end date for a) days or b) weeks
		if($datePeriod == 'Tage') {
			$dateRange = DateUtil::dayDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = ($dateRange > 1) ? $dateRange['EndDate'] :  null;
		}
		else {
			$dateRange = DateUtil::weekDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = $dateRange['EndDate'];
		}
		return $dates;
	}

	/*
	 * Returns DailyMenuPages for the current restaurant that fall within
	 * a given date range, sorted by date.
	 *
	 * @return DataList
	 */
	public function DailyMenuPages() {
		$dateRangeArray = $this->getDateRangeAsArray();
		$menuContainer = $this->MenuContainer();
		return DailyMenuPage::get()->filter(array_merge(
			array('ParentID' => $menuContainer->ID),
			array('Date:GreaterThanOrEqual' => $dateRangeArray['StartDate']),
			$dateRangeArray['EndDate'] ? array('Date:LessThanOrEqual' => $dateRangeArray['EndDate']) : array()
		))->sort('Date');
	}
	
	/**
	 * Returns one-dimensional array with IDs of selected FoodCategories
	 * 
	 * @return array
	 */
	public function FoodCategoriesArray() {
		$FoodCategories = $this->FoodCategories();
		return $FoodCategories ? array_keys($this->FoodCategories()->map('ID')->toArray()) : false;
	}
	
	/**
	 * Returns comma seperated string with IDs of selected FoodCategories
	 * 
	 * @return string
	 */
	public function FoodCategoryIDs() {
		$FoodCategories = $this->FoodCategoriesArray();
		return $FoodCategories ? implode(', ', $this->FoodCategoriesArray()) : false;
	}
	
	/**
	 * Returns a DataList of all Dishes that are assigned to the given DailyMenuPage
	 * filtered by the FoodCategories selected in the current config
	 * 
	 * @param integer $dailyMenuPageID ID of the DailyMenuPage for requesting assigned Dishes
	 * @return DataList
	 */
	public function DishesForDailyMenuPage($dailyMenuPageID) {
		return Dish::get()
			->where('FoodCategory.ID IN('.$this->FoodCategoryIDs().')')
			->filter(
				array(
					'DailyMenuPages.ID' => $dailyMenuPageID
				)
			)
			->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			->leftJoin('FoodCategory', 'FoodCategory.ID=RestaurantDish.FoodCategoryID')
			->sort('FoodCategory.SortOrder ASC');
	}
	
	/**
	 * Returns a Grouped of all Dishes that are assigned to the given DailyMenuPage
	 * filtered by the FoodCategories selected in the current config
	 * grouped by FoodCategory
	 * 
	 * @param integer $dailyMenuPageID ID of the DailyMenuPage for requesting assigned Dishes
	 * @return GroupedList
	 */
	public function GroupedDishes($dailyMenuPageID) {
		return GroupedList::create(
			$this->DishesForDailyMenuPage($dailyMenuPageID)
		);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)Relations 
	 * (based on the $relationName) that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 * 
	 * $relationName has to be a class name for which 
	 * a 'Global'.$relationName and a 'Subsite'.$relationName class exists.
	 * e.g.: $relationName = 'Allergen' => 'GlobalAllergen' and 'SubsiteAllergen'
	 *
	 * GlobalRealtions come first followed by SubsiteRelations, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param String $relationName Classname that the 'Global' und 'Subsite' relation classes are generated from
	 * @param Boolean $includeSideDishes (optional) True if Allergens of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedDishRelations($relationName, $includeSideDishes = false, $foodCategoryIDs = null) {
		$foodCategoryIDArray = $foodCategoryIDs ? explode(",", $foodCategoryIDs) : $this->FoodCategoriesArray();
		$dailyMenuPages = $this->DailyMenuPages();
		if(!$foodCategoryIDArray || !$dailyMenuPages) return false;

		$usedGlobalRelations = new ArrayList();
		$usedSubsiteRelations = new ArrayList();
		foreach ($dailyMenuPages as $dailyMenuPage) {
			$usedDishes = $this->DishesForDailyMenuPage($dailyMenuPage->ID);
			$usedDishesArray = array_keys($usedDishes->map('ID')->toArray());
			
			// get SideDishes
			if($includeSideDishes) {
				$usedSideDishes = new ArrayList();
				foreach($usedDishes as $usedDish) {
					foreach($usedDish->AllSideDishes() as $sideDish) {
						$usedSideDishes->push($sideDish);
					}
				}
				$usedSideDishes->removeDuplicates();
				// add SideDishes to array of used Dishes
				$usedDishesArray = array_merge($usedDishesArray, array_keys($usedSideDishes->map('ID')));
			}
			
			// Get GlobalRelations
			$globalClassName = 'Global'.$relationName;
			$globalRelations = $globalClassName::get()
				->filter(array('Dishes.ID' => $usedDishesArray));
			// add all GlobalRelations to ArrayList - duplicates will be removed later
			foreach ($globalRelations as $additive) {
				$usedGlobalRelations->push($additive);
			}
			
			// Get SubsiteRelations
			$subsiteClassName = 'Subsite'.$relationName;
			$subsiteRelations = $subsiteClassName::get()
				->filter(array('Dishes.ID' => $usedDishesArray));
			// add all GlobalRelations to ArrayList - duplicates will be removed later
			foreach ($subsiteRelations as $additive) {
				$usedSubsiteRelations->push($additive);
			}
		}
		
		// remove duplicates and sort
		$usedGlobalRelations->removeDuplicates();
		$usedGlobalRelations = $usedGlobalRelations->sort('SortOrder');
		$usedSubsiteRelations->removeDuplicates();
		$usedSubsiteRelations = $usedSubsiteRelations->sort('SortOrder');

		return MyListUtil::concatenateLists($usedGlobalRelations, $usedSubsiteRelations);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)Additives 
	 * that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 *
	 * GlobalAdditives come first followed by SubsiteAdditives, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedAdditives($includeSideDishes = false, $foodCategoryIDs = null) {
		return $this->UsedDishRelations('Additive', $includeSideDishes, $foodCategoryIDs);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)Allergens 
	 * that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 *
	 * GlobalAllergens come first followed by SubsiteAllergens, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Allergens of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedAllergens($includeSideDishes = false, $foodCategoryIDs = null) {
		return $this->UsedDishRelations('Allergen', $includeSideDishes, $foodCategoryIDs);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)DishLabels 
	 * that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 *
	 * GlobalDishLabels come first followed by SubsiteDishLabels, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Allergens of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedDishLabels($includeSideDishes = false, $foodCategoryIDs = null) {
		return $this->UsedDishRelations('DishLabel', $includeSideDishes, $foodCategoryIDs);
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_EXPORTMENUHTMLCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_EXPORTMENUHTMLCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_EXPORTMENUHTMLCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_EXPORTMENUHTMLCONFIG', 'any', $member);
    }
	
	public function canRunAllTask($member = null) {
        return Permission::check('RUN_ALL_EXPORTMENUHTMLTASK', 'any', $member);
    }
	
	public function canRunSingleTask($member = null) {
        return Permission::check('RUN_SINGLE_EXPORTMENUHTMLTASK', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_EXPORTMENUHTMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung',
				'sort' => 510
			),
			"VIEW_EXPORTMENUHTMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung',
				'sort' => 520
			),
			"EDIT_EXPORTMENUHTMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung',
				'sort' => 530
			),
			"DELETE_EXPORTMENUHTMLCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung',
				'sort' => 540
			),
			"RUN_ALL_EXPORTMENUHTMLTASK" => array(
				'name' => 'Kann alle Aufgaben "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'sort' => 550
			),
			"RUN_SINGLE_EXPORTMENUHTMLTASK" => array(
				'name' => 'Kann einzelne Aufgabe "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'sort' => 560
			)
		);
	}
}

