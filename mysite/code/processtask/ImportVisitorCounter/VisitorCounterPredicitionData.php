<?php

class VisitorCounterPredictionData extends DataObject {
	private static $db = array(
		'Time' => 'Time',
		'MaxVisitors' => 'Int',
		'MaxVisitorsPercent' => 'Int',
		'WeekdayNum' => 'Int', // ISO-8601 weekday number (1 = monday - 7 = sunday)
		'Month' => 'Int', // 2-digit month number (01 - 12)
		'Year' => 'Int', // 4-digit year (2016)
		'PredictionBase' => "Enum('LastWeek,Continous,SameMonthLastYear','Continous')",
		'PredictionDataItemCount' => 'Int',
		'LastAddedDateTime' => 'SS_DateTime' // datetime of the VisitorCounterData that was last added to this item
	);
	
	private static $has_one = array(
		'ImportVisitorCounterConfig' => 'ImportVisitorCounterConfig'
	);
}

