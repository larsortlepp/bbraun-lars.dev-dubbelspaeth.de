<?php

class VisitorCounterData extends DataObject {
	private static $db = array(
		'DateTime' => 'SS_DateTime',
		'Time' => 'Time',
		'MaxVisitors' => 'Int',
		'MaxVisitorsPercent' => 'Int'
	);
	
	private static $has_one = array(
		'ImportVisitorCounterConfig' => 'ImportVisitorCounterConfig'
	);
	
	public function TimeNice() {
		return date('H:i', strtotime($this->Time));
	}
}

