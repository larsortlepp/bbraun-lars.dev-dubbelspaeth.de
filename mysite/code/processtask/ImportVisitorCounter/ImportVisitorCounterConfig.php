<?php
/**
 * Import data from external visitor counter servives
 * and assign the data to DisplayTemplatePages and MobileTemplatePages
 * 
 * Use a javascript chart API like http://www.chartjs.org/ to render
 * data inte templates
 * 
 */
class ImportVisitorCounterConfig extends ProcessTaskObject implements PermissionProvider {

	public static $singular_name = "Import Besucherzähler";

	public static $plural_name = "Import Besucherzähler";
	
	private static $db = array (
        'URL' => 'Varchar(2083)',
		'ApiHttpMethod' => "Enum('GET,POST,PUT','PUT')",
		'ApiRequestBody' => 'Text',
		'ImportFunction' =>  "Enum('import_sensalytics','import_sensalytics')",
		'PredictionBase' => "Enum('LastWeek,Continous,SameMonthLastYear','Continous')",
		'Capacity' => 'Int',
		'CounterDataStart' => 'Time',
		'CounterDataEnd' => 'Time',
		'CounterDataIntervall' => 'Int',
		'VisitorThresholdMedium' => 'Int',
		'VisitorThresholdHigh' => 'Int',
		'PredicitionDataLastCalculated' => 'SS_DateTime',
		'ChartMinY' => 'Int',
		'ChartMaxY' => 'Int'
    );
	
	private static $has_many = array(
		'VisitorCounterDatas' => 'VisitorCounterData',
		'VisitorCounterPredictionDatas' => 'VisitorCounterPredictionData',
		'MobileTemplatePages' => 'MobileTemplatePage',
		'DisplayTemplatePages' => 'DisplayTemplatePage'
	);
	
	private static $defaults = array(
		'ChartMinY' => '0',
		'ChartMaxY' => '100'
	);
	
	private static $field_labels = array(
		'URL' => 'URL',
		'ApiHttpMethod' => 'API HTTP Methode',
		'ApiRequestBody' => 'API Request Body',
		'Title' => 'Titel',
		'ImportFunction' => 'Import Funktion',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv',
		'PredictionBase' => 'Prognose Basis',
		'Capacity' => 'Anzahl Sitzplätze Restaurant',
		'CounterDataStart' => 'Uhrzeit Beginn Datenausgabe',
		'CounterDataEnd' => 'Uhrzeit Ende Datenausgabe',
		'CounterDataIntervall' => 'Intervall Datenerfassung',
		'PredicitionDataLastCalculated' => 'Prognose berechnet',
		'ChartMinY' => 'Start Skalierung Y-Achse',
		'ChartMaxY' => 'Ende Skalierung Y-Achse'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'Title',
		'ImportFunction',
		'URL',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'PredictionBase',
		'PredicitionDataLastCalculated',
		'Published.Nice'		
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'ImportFunction',
		'Title',
		'URL',
		'LastStarted',
		'LastRun',
		'PredictionBase',
		'PredicitionDataLastCalculated'
	);

    public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// Add Button to execute task
		if(
			$this->ID && $this->URL && $this->ImportFunction && 
			($this->canRunAllTask() || $this->canRunSingleTask())
		){
			$f->addFieldToTab('Root.Main', new LiteralField('RunTask', '<a href="'.Director::absoluteBaseURL()."tasks/importvisitorcounterdata/task/".$this->ID.'" target="_blank" class="custom-button icon-arrows-ccw ajax-executeSingleProcessTask">'.$this::$singular_name.' ausführen</a>'), 'GroupField');
		}
		
		$f->addFieldToTab(
			'Root.Main',
			TextField::create(
				'URL',
				'URL'
			),
			'Schedule'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'ApiHttpMethod',
				'API HTTP Methode',
				singleton($this->ClassName)->dbObject('ApiHttpMethod')->enumValues()
			)
				->setEmptyString('Bitte auswählen'),
			'Schedule'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			TextareaField::create(
				'ApiRequestBody',
				'API Request Body'
			)
				->setDescription('Request im JSON Format'),
			'Schedule'
		);
		
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'ImportFunction',
				'Import Funktion',
				singleton($this->ClassName)->dbObject('ImportFunction')->enumValues()
			)
				->setEmptyString('Bitte auswählen'),
			'Schedule'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			HeaderField::create(
				'RestaurantSettingsHeader',
				'Einstellungen Restaurant'
			), 
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'Capacity',
				'Anzahl Sitzplätze Restaurant'
			), 
			'LastStarted'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			HeaderField::create(
				'ChartSettingsHeader',
				'Einstellungen Diagramm'
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'PredictionBase',
				'Berechnung Prognose',
				singleton($this->ClassName)->dbObject('PredictionBase')->enumValues()
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			TimeField::create(
				'CounterDataStart',
				'Uhrzeit Beginn Datenausgabe'
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			TimeField::create(
				'CounterDataEnd',
				'Uhrzeit Ende Datenausgabe'
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'CounterDataIntervall',
				'Intervall Datenerfassung'
			)
				->setDescription('Minuten'),
			'LastStarted'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'ChartMinY',
				'Start Skalierung Y-Achse'
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'ChartMaxY',
				'Ende Skalierung Y-Achse'
			),
			'LastStarted'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'VisitorThresholdMedium',
				'Schwellwert Auslastung Mittel'
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			NumericField::create(
				'VisitorThresholdHigh',
				'Schwellwert Auslastung Hoch'
			),
			'LastStarted'
		);
		
		$f->addFieldToTab(
			'Root.Main',
			ReadonlyField::create(
				'PredicitionDataLastCalculated',
				'Letzte Berechnung Prognose'
			)
		);
		
		return $f;

    }
	
	/**
	 * Returns array with value for each datainterval between start end end time
	 * @return ArrayList
	 */
	public function CounterDataTimeInterval() {

		$start = $time = $this->CounterDataStart;
		$end = $this->CounterDataEnd;
		$intervalldata = ArrayList::create();
		$intervalldata->push(DataObject::create(
			array(
				'Time' => $start,
				'TimeNice' => date('H:i', strtotime($start))
			)
		));
		while($time < $end) {
			$time = date('H:i:s', strtotime($time.' + '.$this->CounterDataIntervall.' minutes'));
			$intervalldata->push(DataObject::create(
				array(
					'Time' => $time,
					'TimeNice' => date('H:i', strtotime($time))
				)
			));
		}
		return $intervalldata;
		 
	}
	
	/**
	 * Return visitor counter data for the current date
	 * Values are normalized using $this->NormalizeData()
	 * 
	 * @return ArrayList
	 */
	public function CurrentVisitorCounterData() {
		$data = VisitorCounterData::get()->where("ImportVisitorCounterConfigID = $this->ID AND DATE(DateTime) = '".date('Y-m-d')."'")->sort('DateTime ASC');
		// create ArrayList with an entry for every CounterDataTimeInterval
		// inserting 'NaN' when there is no current data in the database
		$visitordata = ArrayList::create();
		foreach($this->CounterDataTimeInterval() as $time) {
			$visitordataitem = ($data && $data->exists()) ? $data->find('Time', $time->Time) : false;
			if(!$visitordataitem) {
				$visitordataitem = VisitorCounterData::create();
				$visitordataitem->Time = $time->Time;
				$visitordataitem->MaxVisitors = 'NaN';
				$visitordataitem->MaxVisitorsPercent = 'NaN';
			}
			$visitordata->push($visitordataitem);
		}
		return $this->NormalizeData($visitordata);
	}
	
	public function CurrentMaxVisitors() {
		return $this->CurrentVisitorField('MaxVisitors');
		
	}
	
	public function CurrentMaxVisitorsPercent() {
		return $this->CurrentVisitorField('MaxVisitorsPercent');
		
	}
	
	public function CurrentVisitorField($fieldname = 'MaxVisitorsPercent') {
		$default = 0;
		if(date('H:i:s') > date('H:i:s', strtotime($this->CounterDataEnd.' + '.$this->CounterDataIntervall.' minutes' ))) return $default;
		$VisitorCounterData = $this->LatestVisitorData();
		return ($VisitorCounterData  && $VisitorCounterData->exists()) ? $VisitorCounterData->$fieldname : $default;
		
	}
	
	public function LatestVisitorData() {
		$VisitorCounterData = VisitorCounterData::get()->where("ImportVisitorCounterConfigID = $this->ID AND DATE(DateTime) = '".date('Y-m-d')."' ")->sort('DateTime DESC')->limit(1);
		return $VisitorCounterData->exists() ? $VisitorCounterData->first() : false;
		
	}
	
	/**
	 * Return prediction data for visitor counter, baes on $PredictionBase setting.
	 * Values are normalized using $this->NormalizeData().
	 * Prediction values will start after $this->LatestVisitorData() entry.
	 * 
	 * @param boolean $insertConnectionData If true the last CurrentVisitorCounterData vaue will be inserted (necessary for line charts to connect 2 lines)
	 * @return ArrayList
	 */
	public function CurrentVisitorCounterPredictionData($insertConnectionData = false) {
		$weekday = date('N');
		$month = date('m');
		$year = (date('Y')-1);
		
		// set starttime for prediction data
		$latestVisitorData = $this->LatestVisitorData();
		// if visitordata exists use the time from that as starttime, 
		// if not use the CounterDataStart time - 1 minute
		$starttime = $latestVisitorData ? $latestVisitorData->Time : date('H:i:s', '2000-01-01 '.$this->CounterDataStart.' - 1 minute');
		
		// create basic query, based on $this->PredictionBase
		$query = "ImportVisitorCounterConfigID = $this->ID AND PredictionBase = '$this->PredictionBase' AND WeekdayNum = $weekday AND Time > '$starttime'";
		
		// extend query based on $this->PredictionBase
		if($this->PredictionBase == 'SameMonthLastYear') {
			$query .= " AND Month = $month AND Year = '$year'";
		}
		
		// get prediction data based on $PredictionBase
		$data = VisitorCounterPredictionData::get()->where($query);
		
		// get latest VisitorCounterData
		$connectionData = false;
		if($insertConnectionData && $starttime < $this->CounterDataEnd) {
			$connectionDataSet = ArrayList::create(array($latestVisitorData));
			$connectionData = $connectionDataSet;
			if($connectionData) $connectionData = $connectionData->First();
		}
		
		// create ArrayList with an entry for every CounterDataTimeInterval
		// inserting 'NaN' when there is no current data in the database
		$visitordata = ArrayList::create();
		foreach($this->CounterDataTimeInterval() as $time) {
			$visitordataitem = false;
			if($connectionData && $connectionData->Time == $time->Time) {
				$visitordataitem = $connectionData;
			}
			else if($data && $data->exists()) {
				$visitordataitem = $data->find('Time', $time->Time);
			}
			if(!$visitordataitem) {
				$visitordataitem = VisitorCounterPredictionData::create();
				$visitordataitem->Time = $time->Time;
				$visitordataitem->MaxVisitors = 'NaN';
				$visitordataitem->MaxVisitorsPercent = 'NaN';
			}
			$visitordata->push($visitordataitem);
		}
		
		return $this->NormalizeData($visitordata);
	}
	
	/**
	 * Returns the normalized MaxVisitorsPercent value,
	 * based on $ChartMinY setting and the prevuis and following values.
	 * Normalizing will set values out of scope to 'NaN' and the first or last values
	 * that is out of scope the the $ChartMinY value
	 * 
	 * Hint: Expects values to be a parabel: first rising, then a peak, then falling
	 * Can´t handle data that will have different peaks with multiple values
	 * in between that fall below $ChartMinY
	 * 
	 * @param ArrayList $dataset with VisitorCounterData or VisitorCounterPreodictinData
	 * @return ArrayList
	 */
	public function NormalizeData($dataset) {
		if( $this->ChartMinY == 0 || !$dataset || !$dataset->First()) return $dataset;
		
		$total = $dataset->Count();
		$dataset_array = $dataset->toArray();
		$normalized_dataset = ArrayList::create();
		
		$start_value_set = false;
		$end_value_set = false;
		
		for($count = 0; $count < $total; $count++) {
			$data_normalized = $dataset_array[$count]->duplicate($doWrite = false);
			
			// if first element has 'NaN' value, the data has already been processed and a start vaue has been set
			if($count == 0 && $dataset_array[$count]->MaxVisitorsPercent == 'NaN') $start_value_set = true;
			
			// values on falling graph
			if(
				$start_value_set &&
				$count >= 1
			) {

				// set item to minimum Y-value if:
				// current element is below minimum value and previous element is above minimum value
				if(
					!$end_value_set &&
					$dataset_array[$count]->MaxVisitorsPercent < $this->ChartMinY && 
					$dataset_array[$count-1]->MaxVisitorsPercent >= $this->ChartMinY
					
				) {
					$data_normalized->MaxVisitorsPercent = $this->ChartMinY;
					$end_value_set = true;
				}
				// set item to NaN (to not render it in the chart)
				// current and previous element is below minimum value
				else if (
					$end_value_set ||
					(
						$dataset_array[$count]->MaxVisitorsPercent < $this->ChartMinY && 
						$dataset_array[$count-1]->MaxVisitorsPercent < $this->ChartMinY
					)
				) {
					$data_normalized->MaxVisitorsPercent = 'NaN';
				}
			}
			
			// values on raising graph
			else if(
				!$start_value_set &&	
				($count+1) < $total
			) {

				// first value is equal or greater than minimum value, but not set to 'NaN'
				if($count == 0 && $dataset_array[$count]->MaxVisitorsPercent != 'NaN' && $dataset_array[$count]->MaxVisitorsPercent >= $this->ChartMinY) $start_value_set = true;
				
				// set item to minimum Y-value if:
				// current element is below minimum value and next element is above minimum value
				else if (
					!$start_value_set &&
					$dataset_array[$count]->MaxVisitorsPercent < $this->ChartMinY && 
					$dataset_array[$count+1]->MaxVisitorsPercent > $this->ChartMinY
				) {
					$data_normalized->MaxVisitorsPercent = $this->ChartMinY;
					$start_value_set = true;
				}
				// set item to NaN (to not render it in the chart)
				// current and next element is below minimum value
				else if (
					$dataset_array[$count]->MaxVisitorsPercent < $this->ChartMinY && 
					$dataset_array[$count+1]->MaxVisitorsPercent < $this->ChartMinY
				) {
					$data_normalized->MaxVisitorsPercent = 'NaN'; // return 'not a number' so that value will not be rendered
				}
				// current value below and next exactly minimum value
				else if (
					!$start_value_set &&
					$dataset_array[$count]->MaxVisitorsPercent < $this->ChartMinY && 
					$dataset_array[$count+1]->MaxVisitorsPercent == $this->ChartMinY
				) {
					$data_normalized->MaxVisitorsPercent = 'NaN';
					$start_value_set = true;
				}
				
				
			}
			
			$normalized_dataset->push($data_normalized);

		}
		
		return $normalized_dataset;
	}
	
	/**
	 * Returns a string ('low', 'mdeium', 'high') based
	 * on the current MaxVisitorsPercent in the restaurant and the threshold set
	 * via $this->VisitorThresholdMedium and $this->VisitorThresholdHigh
	 * 
	 * @return string low|mdeium|high string that can be used as a css class for rendering the current capacity
	 */
	public function CapacityClass() {
		$currentmaxvisitorspercent = $this->CurrentMaxVisitorsPercent();
		if($currentmaxvisitorspercent < $this->VisitorThresholdMedium) {
			return 'low';
		}
		else if($currentmaxvisitorspercent >= $this->VisitorThresholdMedium && $currentmaxvisitorspercent < $this->VisitorThresholdHigh) {
			return 'medium';
		}
		else if($currentmaxvisitorspercent >= $this->VisitorThresholdHigh) {
			return 'high';
		}
		else {
			return '';
		}
	}
	
	public function PredictionBaseNice() {
		switch ($this->PredictionBase) {
			case 'LastWeek':
				return 'letzter Woche';
			case 'SameMonthLastYear':
				return date('F'). ' ' . date('Y', strtotime(date('Y').' -1'));
			default:
				return 'kontinuierlicher Berechnung';	
		}
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_IMPORTVISITORCOUNTER', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_IMPORTVISITORCOUNTER', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_IMPORTVISITORCOUNTER', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_IMPORTVISITORCOUNTER', 'any', $member);
    }
	
	public function canRunAllTask($member = null) {
        return Permission::check('RUN_ALL_IMPORTVISITORCOUNTERTASK', 'any', $member);
    }
	
	public function canRunSingleTask($member = null) {
        return Permission::check('RUN_SINGLE_IMPORTVISITORCOUNTERTASK', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_IMPORTVISITORCOUNTER" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 210
			),
			"VIEW_IMPORTVISITORCOUNTER" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 220
			),
			"EDIT_IMPORTVISITORCOUNTER" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 230
			),
			"DELETE_IMPORTVISITORCOUNTER" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 240
			),
			"RUN_ALL_IMPORTVISITORCOUNTERTASK" => array(
				'name' => 'Kann alle Aufgaben "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 250
			),
			"RUN_SINGLE_IMPORTVISITORCOUNTERTASK" => array(
				'name' => 'Kann einzelne Aufgabe "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 260
			)
		);
	}
}

