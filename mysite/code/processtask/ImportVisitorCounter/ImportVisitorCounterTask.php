<?php

class ImportVisitorCounterTask extends ImportTask {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ImportVisitorCounterConfig';
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
		if(!method_exists($this, $taskconfig->ImportFunction)) return false;
		return $this->{$taskconfig->ImportFunction}($taskconfig,$request);
	}
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		$result = Permission::check("RUN_ALL_IMPORTVISITORCOUNTERTASK");
		return  $result;
	}
	
	/**
	 * Provide permissions for current member to run the task "task()"
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_single_task() {
		$result = $this->can_run_task() || Permission::check("RUN_SINGLE_IMPORTVISITORCOUNTERTASK");
		return  $result;
	}
	
	/**
	 * Parse visitor counter data from sensalytics for the current day
	 * Return value will be JSON.
	 * 
	 * Note: sensalytics returns values in 10 minute intervalls AFTER 10 minutes.
	 * The value for 11:30 will be changed until 11:40. 
	 * At 11:40 the value for 11:30 is final, and wonÂ´t be changed anymore.
	 * It hen contains the maximum visitors between 11:30 and 11:39.
	 * 
	 * For a more user firendly (and faster) display 
	 * we shift the values by +10 minutes when importing them:
	 * We will import the value '11:30' at time 11:40 and then save it with Time '11:40'.
	 * Like this we have a fix value at the time we are importing. 
	 * That value contains the maximum visitors of the past 10 minutes.
	 * 
	 * Example code of URL:
	 * 
	 * {
	 *   "executionDateTime":
	 *   {
	 *     "unixTimeStamp":1469213753,
	 *     "timeAgo":"vor einem Augenblick",
	 *     "formattedDate":"22.07.2016 18:55:53"
	 *   },
	 *   "executionCode":"4cd5836c331970897b38fd322f213f8a",
	 *   "rd":
	 *   [
	 *     [
	 *       {
	 *         "v":"10-Minuten",
	 *         "t":"RHH"
	 *       },
	 *       {
	 *         "v":"Max. Auslastung (Relativ)",
	 *         "t":"CH",
	 *         "m":true
	 *       },
	 *       {
	 *         "v":"Max. Auslastung",
	 *         "t":"CH",
	 *         "m":true
	 *       }
	 *     ],
	 *     [
	 *       {
	 *         "k":"1130",
	 *         "v":"11:30",
	 *         "t":"RH",
	 *         "s":"11:30"
	 *       },
	 *       {
	 *         "v":"19,4 %",
	 *         "t":"DC",
	 *         "s":"0.19"
	 *       },
	 *       {
	 *         "v":"109",
	 *         "t":"DC",
	 *         "s":"109.39"
	 *       }
	 *     ],
	 *     ...
	 *   ]
	 * }
	 * 
	 * @param type $taskconfig
	 * @param type $request
	 * @return boolean
	 */
	public function import_sensalytics($taskconfig, $request = null) {
		
		// send Httpful request - response will automatically be parsed in the appropriate format
		// e.g. JSON or XML will be parsed as PHP object
		$response = $this->httpful_request($taskconfig->URL, $taskconfig->ApiHttpMethod, $taskconfig->ApiRequestBody);
		
		// abort processing if no response or an error was returned
		if(!$response || $response->body->errorInfo) return false;

		$json = json_decode($response, $as_array = true);
		
		// we expect the following structure:
		// $json->rd[x][0][k]: Time with format '11:30'
		// $json->rd[x][1][s]: Max. occupation relative (percentage) as float value with 2 digits '0.32'
		// $json->rd[x][2][v]: Max. occupation absolute as integer: 145
		
		// loop over all existing entries and store their values
		$numItems = count($json['rd']);
		Debug::message('Anzahl Datensätze '.$numItems);
		for($i = 1; $i < $numItems; $i++) {
			// create datetime value of current entry
			$dateTime = date('Y-m-d').' '.$json['rd'][$i][0]['v'].':00';
			// shift real date time of entry by one CounterDataIntervall (e.g. -10 minutes)
			$importDateTime = date('Y-m-d H:i:s', strtotime($dateTime.' + '.$taskconfig->CounterDataIntervall.' minutes'));
			
			// skip latest entry if it is not older than the current CounterDataIntervall
			// values in external source are ready to import after their timestamp + the intervall time 
			// e.g. current time: 11:30, interval: 10 Minutes.
			// Value of 11:30 can imported from 11:40 on
			Debug::message("Item no. $i (".($numItems-1)."): $dateTime - should be imported at ".date('Y-m-d H:i:s', strtotime($dateTime.' + '.$taskconfig->CounterDataIntervall.' minutes')));
			if(
				$i == ($numItems-1) && 
				date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($dateTime.' + '.$taskconfig->CounterDataIntervall.' minutes'))
			) {
				Debug::message("Skipping current value - data not ready: $dateTime");
				continue;
			}
			
			// check if entry for the date and time already exists
			$visitorcounterdata = VisitorCounterData::get()->where("ImportVisitorCounterConfigID = $taskconfig->ID AND DateTime = '$importDateTime'");
			// only create a new entry if it does not exist yet
			if(!$visitorcounterdata || !$visitorcounterdata->exists()) {
				$visitorcounterdata = VisitorCounterData::create();
				$visitorcounterdata->DateTime = $importDateTime;
				$visitorcounterdata->Time = date('H:i:s', strtotime($importDateTime));
				$visitorcounterdata->MaxVisitors = max($json['rd'][$i][2]['v'], 0); // make negative values 0
				
				// calculate percentage data based on capacity set in config
				$maxVisitorsPercent = round(($visitorcounterdata->MaxVisitors / $taskconfig->Capacity) * 100);
				// limit to 100% maximum
				if($maxVisitorsPercent > 100) $maxVisitorsPercent = 100;
				$visitorcounterdata->MaxVisitorsPercent = $maxVisitorsPercent;
				
				// use percentage data as calculated in source (relies on capacity value set in external system)
				#$visitorcounterdata->MaxVisitorsPercent = ($json['rd'][$i][1]['s']*100);
				$visitorcounterdata->ImportVisitorCounterConfigID = $taskconfig->ID;
				$visitorcounterdata->write();
				
				Debug::message("Creating new entry: $importDateTime (original DateTime: $dateTime): $visitorcounterdata->MaxVisitors / $visitorcounterdata->MaxVisitorsPercent");
			}
			
		}
		
		// calculate predicition data once per day for yesterday
		Debug::message('Checking for date being smaller than: '.date('Y-m-d', strtotime($taskconfig->PredicitionDataLastCalculated)));
		if( true || !$taskconfig->PredicitionDataLastCalculated || date('Y-m-d') > date('Y-m-d', strtotime($taskconfig->PredicitionDataLastCalculated))) {
			Debug::message("Calling calculate_predictiondata()");
			$this->calculate_predictiondata($taskconfig, $request);
			
			// save datetime of last caluclation of predicition data
			$taskconfig->PredicitionDataLastCalculated = date('Y-m-d H:i:s');
			$taskconfig->write();
		}
		
		return true;
	}
	
	public function calculate_predictiondata($taskconfig, $request = null) {
		
		$yesterday = strtotime('yesterday');
		
		Debug::message("calculating prediction data for ".date('Y-m-d', $yesterday));
		
		// we generate dataobjects for the current date 
		// and the time interval that exists in the VisitorCounterData
		$weekday = date('N', $yesterday);
		$month = date('m', $yesterday);
		$year = date('Y', $yesterday);
		
		// get existing visitor data from yesterday
		$visitorcounterdata = VisitorCounterData::get()->where("ImportVisitorCounterConfigID = $taskconfig->ID AND DATE(DateTime) = '".date('Y-m-d', $yesterday)."'")->sort('DateTime ASC');
		
		// add data for PredictionBase = 'LastWeek' based on the current date
		//$lastaddedpredictiondata = VisitorCounterData::get()->where('VisitorCounterPredictionData', "PredictionBase = 'LastWeek' AND WeekdayNum = $weekday")->sort('LastAddedDateTime DESC');
		foreach($visitorcounterdata as $data) {
			
			$predicitiondata_last_week = false;
			
			Debug::message("Calculating data for visitor counter item with date $data->DateTime and for ImportVisitorCounterConfigID $taskconfig->ID");
			
			
			// PredictionBase = 'LastWeek'

			// get predicition data for the given Time, Weekday and PredictionBase
			$existingpredictiondata_last_week = VisitorCounterPredictionData::get()->where("ImportVisitorCounterConfigID = $taskconfig->ID AND PredictionBase = 'LastWeek' AND WeekdayNum = $weekday AND Time = '".date('H:i:s', strtotime($data->DateTime))."'")->limit(1);
			if($existingpredictiondata_last_week && $existingpredictiondata_last_week->exists()) $existingpredictiondata_last_week = $existingpredictiondata_last_week->first();
			
			// initially create prediction data if it does not exist
			// or update existing predictiondata to current values
			if(!$existingpredictiondata_last_week || !$existingpredictiondata_last_week->exists()) {
				Debug::message("Creating new Predicition Data 'LastWeek' for weekday $weekday and Time ".date('H:i:s', strtotime($data->DateTime)));
				$predicitiondata_last_week = VisitorCounterPredictionData::create();
				$predicitiondata_last_week->WeekdayNum = $weekday;
				$predicitiondata_last_week->PredictionBase = 'LastWeek';
				$predicitiondata_last_week->PredictionDataItemCount = 1;
			}
			// update predictiondata if the current data hasn´t been added yet
			else if ($existingpredictiondata_last_week && $existingpredictiondata_last_week->exists() && $data->DateTime > $existingpredictiondata_last_week->LastAddedDateTime) {
				Debug::message("Predicition Data already exists: Updating it - ".$existingpredictiondata_last_week->ID);
				$predicitiondata_last_week = $existingpredictiondata_last_week;
			}
			
			if($predicitiondata_last_week) {
			
				$predicitiondata_last_week->Time = $data->Time;
				$predicitiondata_last_week->MaxVisitors = $data->MaxVisitors;
				$predicitiondata_last_week->MaxVisitorsPercent = $data->MaxVisitorsPercent;
				$predicitiondata_last_week->LastAddedDateTime = $data->DateTime;
				$predicitiondata_last_week->ImportVisitorCounterConfigID = $taskconfig->ID;

				$predicitiondata_last_week->write();

				Debug::message("Created Predition Data Last Week");	
			}
			
			// PredictionBase = 'Continous'

			// get predicition data for the given Time, Weekday and PredictionBase
			$existingpredictiondata_continous = VisitorCounterPredictionData::get()->where("ImportVisitorCounterConfigID = $taskconfig->ID AND PredictionBase = 'Continous' AND WeekdayNum = $weekday AND Time = '".date('H:i:s', strtotime($data->DateTime))."'")->limit(1);
			if($existingpredictiondata_continous && $existingpredictiondata_continous->exists()) $existingpredictiondata_continous = $existingpredictiondata_continous->first();
			
			// initially create prediction data if it does not exist
			// or update existing predictiondata to current values
			if(!$existingpredictiondata_continous || !$existingpredictiondata_continous->exists()) {
				Debug::message("Creating new Predicition Data 'Continous' for weekday $weekday and Time ".date('H:i:s', strtotime($data->DateTime)));
				$predicitiondata_continous = VisitorCounterPredictionData::create();
				$predicitiondata_continous->WeekdayNum = $weekday;
				$predicitiondata_continous->PredictionBase = 'Continous';
				$predicitiondata_continous->MaxVisitors = $data->MaxVisitors;
				$predicitiondata_continous->MaxVisitorsPercent = $data->MaxVisitorsPercent;
				$predicitiondata_continous->PredictionDataItemCount = 1;
			}
			// update predictiondata if the current data hasn´t been added yet
			else if ($existingpredictiondata_continous && $existingpredictiondata_continous->exists() && $data->DateTime > $existingpredictiondata_continous->LastAddedDateTime) {
				Debug::message("Predicition Data already exists: Updating it - ".$existingpredictiondata_continous->ID);
				$predicitiondata_continous = $existingpredictiondata_continous;
				
				// recalculate average continous visitor data
				$predicitiondata_continous->MaxVisitors = $this->calculate_avg($existingpredictiondata_continous->MaxVisitors, $existingpredictiondata_continous->PredictionDataItemCount, $data->MaxVisitors);
				$predicitiondata_continous->MaxVisitorsPercent = $this->calculate_avg($existingpredictiondata_continous->MaxVisitorsPercent, $existingpredictiondata_continous->PredictionDataItemCount, $data->MaxVisitorsPercent);
				$predicitiondata_continous->PredictionDataItemCount++;
			}
			
			if($predicitiondata_continous) {
			
				$predicitiondata_continous->Time = $data->Time;
				$predicitiondata_continous->LastAddedDateTime = $data->DateTime;
				$predicitiondata_continous->ImportVisitorCounterConfigID = $taskconfig->ID;

				$predicitiondata_continous->write();

				Debug::message("Created Predition Data Continous");	
			}
			
			// PredictionBase = 'SameMonthLastYear'

			// get predicition data for the given Time, Weekday and PredictionBase
			$existingpredictiondata_samemonthlastyear = VisitorCounterPredictionData::get()->where("ImportVisitorCounterConfigID = $taskconfig->ID AND PredictionBase = 'SameMonthLastYear' AND WeekdayNum = $weekday AND Month = $month AND Year = '".date('Y', strtotime($year-1))."' AND Time = '".date('H:i:s', strtotime($data->DateTime))."'")->limit(1);
			if($existingpredictiondata_samemonthlastyear && $existingpredictiondata_samemonthlastyear->exists()) $existingpredictiondata_samemonthlastyear = $existingpredictiondata_samemonthlastyear->first();
			
			// initially create prediction data if it does not exist
			// or update existing predictiondata to current values
			if(!$existingpredictiondata_samemonthlastyear || !$existingpredictiondata_samemonthlastyear->exists()) {
				Debug::message("Creating new Predicition Data 'SameMonthLastYear' for WeekdayNum = $weekday AND Month = $month AND Year = '".date('Y', strtotime($year-1))."' AND Time = '".date('H:i:s', strtotime($data->DateTime))."'");
				$predicitiondata_samemonthlastyear = VisitorCounterPredictionData::create();
				$predicitiondata_samemonthlastyear->WeekdayNum = $weekday;
				$predicitiondata_samemonthlastyear->Month = $month;
				$predicitiondata_samemonthlastyear->Year = date('Y', strtotime($year-1));
				$predicitiondata_samemonthlastyear->PredictionBase = 'SameMonthLastYear';
				$predicitiondata_samemonthlastyear->MaxVisitors = $data->MaxVisitors;
				$predicitiondata_samemonthlastyear->MaxVisitorsPercent = $data->MaxVisitorsPercent;
				$predicitiondata_samemonthlastyear->PredictionDataItemCount = 1;
			}
			// update predictiondata if the current data hasn´t been added yet
			else if ($existingpredictiondata_samemonthlastyear && $existingpredictiondata_samemonthlastyear->exists() && $data->DateTime > $existingpredictiondata_samemonthlastyear->LastAddedDateTime) {
				Debug::message("Predicition Data already exists: Updating it - ".$existingpredictiondata_samemonthlastyear->ID);
				$predicitiondata_samemonthlastyear = $existingpredictiondata_samemonthlastyear;
				
				// recalculate average continous visitor data
				$predicitiondata_samemonthlastyear->MaxVisitors = $this->calculate_avg($existingpredictiondata_samemonthlastyear->MaxVisitors, $existingpredictiondata_samemonthlastyear->PredictionDataItemCount, $data->MaxVisitors);
				$predicitiondata_samemonthlastyear->MaxVisitorsPercent = $this->calculate_avg($existingpredictiondata_samemonthlastyear->MaxVisitorsPercent, $existingpredictiondata_samemonthlastyear->PredictionDataItemCount, $data->MaxVisitorsPercent);
				$predicitiondata_samemonthlastyear->PredictionDataItemCount++;
			}
			
			if($predicitiondata_samemonthlastyear) {
			
				$predicitiondata_samemonthlastyear->Time = $data->Time;
				$predicitiondata_samemonthlastyear->LastAddedDateTime = $data->DateTime;
				$predicitiondata_samemonthlastyear->ImportVisitorCounterConfigID = $taskconfig->ID;

				$predicitiondata_samemonthlastyear->write();

				Debug::message("Created Predition Data SameMonthLastYear");	
			}
		}
		
	}
	
	/**
	 * Calculate average value that is continuously recalulated
	 * based on a growing number if items.
	 * 
	 * @param integer $existing_value Exisiting average value
	 * @param integer $existing_count Number of values that the current average value is based on
	 * @param integer $new_value The new value, that will be added to the existing value, with a factor based on $existing_count
	 * @return integer the caluclated average value with $new_value added
	 */
	public function calculate_avg($existing_value, $existing_count, $new_value) {
		return round( ( ($existing_value * $existing_count) + $new_value) / ($existing_count + 1) );
	}

}
