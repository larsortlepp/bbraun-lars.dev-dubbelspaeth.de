<?php

/**
 * The BackgroundTask will call a defined set of ProcessTasks
 * (as defined in $tasks_to_call).
 * 
 * The goal is to have one single cronjob to trigger multiple tasks,
 * instead of having to set up a separate cronjob for each single task.
 * 
 * It is meant to be called via a cronjob with CLI or sake.
 * 
 * Call the task from the commandline via "sake BackgroundTask" 
 * or over HTTP via "http://mydomain.com/BackgroundTask".
 * 
 * Setup a cronjob as following (example that runs every 15 minutes):
 * NOTE: remove space between * and /15
 * 
 * `* /15 * * * * /your/site/folder/sake BackgroundTask`
 * 
 * NOTE: Every ProcessTask we call defines its own cron schedule 
 * (at which intervall it will be run).
 * If we set up a cronjob for the BackroundTask to run every 5 minutes,
 * but a ProcessTask is scheduled to run everyday at 13:11h,
 * this task will be run at 13:15h.
 * Because thats the intervall in which the BackgroundTask will be called 
 * which then calls the ProcessTaks and checks if it is due to be run.
 * 
 */
class BackgroundTask extends Controller {
	
	private static $allowed_actions = array(
		'index'
	);
	
	private static $tasks_to_call = array(
		'ExportDisplayMenuXmlTask',
		'ExportMenuHtmlTask',
		'ExportMenuXmlTask',
		'ImportMenuXmlTask',
		'ImportRestaurantTask',
		'ImportVisitorCounterTask'
	);
	
	public function get_tasks_to_call() {
		return $this::$tasks_to_call;
	}
	
	/**
	 * Provide permissions for current member to run the task
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		return Permission::check("ADMIN");
	}
	
	public function init() {
		parent::init();
		// Check access permission
		// Allow access from command line and from can_run_task() function
		if(!Director::is_cli() && !$this->can_run_task()) {
			return Security::permissionFailure();
		}
	}
	
	public function index() {
		foreach($this->get_tasks_to_call() as $taskclass) {
			echo $taskclass . "\n";
			$task = new $taskclass();
			$task->init();
			$task->index();
		}
	}
}
