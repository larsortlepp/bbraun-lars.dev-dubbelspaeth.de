<?php

class ImportMenuXmlTask extends ImportTask {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ImportMenuXmlConfig';
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
		$importFunction = $taskconfig->ImportFunction;
		if(method_exists($this, $importFunction)) return $this->$importFunction($taskconfig);
	}
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		$result = Permission::check("RUN_ALL_IMPORTMENUXMLTASK");
		return  $result;
	}
	
	/**
	 * Provide permissions for current member to run the task "task()"
	 * 1. Member needs "RUN_SINGLE_IMPORTMENUXMLTASK" permission
	 * 2. Member needs permission to edit the RestaurantPage that the assigned MenuContainer belongs to
	 *  
	 * @return Boolean
	 */
	public function can_run_single_task() {
		$result = false;
		
		if($this->can_run_task() || Permission::check("RUN_SINGLE_IMPORTMENUXMLTASK")) {
			$taskObjectClassName = $this->getTaskObjectClassName();
			$taskconfig = $taskObjectClassName::get()->filter('ID', $this->getRequest()->param('ID'))->First();
			if($taskconfig) {
				$menuContainer = $taskconfig->MenuContainer();
				$restaurantPage = $menuContainer->parent();
				if($restaurantPage && $restaurantPage->exists() && $restaurantPage->canEdit(Member::currentUser())){
					$result = true;
				}
			}
		}
		return  $result;
	}
	
	public function import_lz_catering($taskconfig) {
		Debug::message("ImportXMLLz_Catering()");
		
		// clean up database before importing new data
		$status = $this->cleanup_db_lz_catering($taskconfig);

		// Array that defines mapping of nutritives from xml file to Dish variables
		// array( [XMLnutritiveName] => [DishNutritiveVariableName], ...)
		$nutritives_map = array();
		$NutritivesMapConfig = $taskconfig->NutritivesMapConfig();
		if ($NutritivesMapConfig && $NutritivesMapConfig->exists()) {
			$NutritivesMapObjects = $NutritivesMapConfig->NutritivesMapObjects();
			if ($NutritivesMapObjects && $NutritivesMapObjects->exists()) {
				$nutritives_map = $NutritivesMapObjects->map('ExternalKey', 'InternalKey')->toArray();
			}
		}
		Debug::show($nutritives_map);

		// Array that defines a blacklist of FoodCategories
		// which are not imported
		$foodcategory_blacklist = array();
		$FoodCategoryBlacklistConfig = $taskconfig->FoodCategoryBlacklistConfig();
		if ($FoodCategoryBlacklistConfig && $FoodCategoryBlacklistConfig->exists()) {
			$FoodCategoryBlacklistObjects = $FoodCategoryBlacklistConfig->FoodCategoryBlacklistObjects();
			if ($FoodCategoryBlacklistObjects && $FoodCategoryBlacklistObjects->exists()) {
				$foodcategory_blacklist = $FoodCategoryBlacklistObjects->map()->toArray();
			}
		}
		Debug::show($foodcategory_blacklist);

		// Array that defines a blacklist of Additives
		$additives_blacklist = array();
		$AdditivesBlacklistConfig = $taskconfig->AdditivesBlacklistConfig();
		if ($AdditivesBlacklistConfig && $AdditivesBlacklistConfig->exists()) {
			$AdditivesBlacklistObjects = $AdditivesBlacklistConfig->AdditivesBlacklistObjects();
			if ($AdditivesBlacklistObjects && $AdditivesBlacklistObjects->exists()) {
				$additives_blacklist = $AdditivesBlacklistObjects->map()->toArray();
			}
		}
		Debug::show($additives_blacklist);

		// Array that defines a blacklist of Allergens
		$allergens_blacklist = array();
		$AllergensBlacklistConfig = $taskconfig->AllergensBlacklistConfig();
		if ($AllergensBlacklistConfig && $AllergensBlacklistConfig->exists()) {
			$AllergensBlacklistObjects = $AllergensBlacklistConfig->AllergensBlacklistObjects();
			if ($AllergensBlacklistObjects && $AllergensBlacklistObjects->exists()) {
				$allergens_blacklist = $AllergensBlacklistObjects->map()->toArray();
			}
		}
		Debug::show($allergens_blacklist);

		// Array that defines a blacklist of DishLabels
		$dishlabels_blacklist = array();
		$DishlabelsBlacklistConfig = $taskconfig->DishLabelsBlacklistConfig();
		if ($DishlabelsBlacklistConfig && $DishlabelsBlacklistConfig->exists()) {
			$DishlabelsBlacklistObjects = $DishlabelsBlacklistConfig->DishLabelsBlacklistObjects();
			if ($DishlabelsBlacklistObjects && $DishlabelsBlacklistObjects->exists()) {
				$dishlabels_blacklist = $DishlabelsBlacklistObjects->map()->toArray();
			}
		}
		Debug::show($dishlabels_blacklist);

		// Array that defines a blacklist of $speciallabels_blacklist
		$speciallabels_blacklist = array();
		$SpecialLabelsBlacklistConfig = $taskconfig->SpecialLabelsBlacklistConfig();
		if ($SpecialLabelsBlacklistConfig && $SpecialLabelsBlacklistConfig->exists()) {
			$SpecialLabelsBlacklistObjects = $SpecialLabelsBlacklistConfig->SpecialLabelsBlacklistObjects();
			if ($SpecialLabelsBlacklistObjects && $SpecialLabelsBlacklistObjects->exists()) {
				$speciallabels_blacklist = $SpecialLabelsBlacklistObjects->map()->toArray();
			}
		}
		Debug::show($speciallabels_blacklist);

		// set to stage mode to create all data in Stage and then publish to Live
		$currStage = Versioned::current_stage();
		Versioned::set_reading_mode(''); // set to stage
		// collect necessary data
		$menuContainerID = $taskconfig->MenuContainerID;
		$menuContainer = MenuContainer::get()->byID($menuContainerID);
		$restaurantPageID = $menuContainer->parent->ID;
		$subsiteID = $menuContainer->SubsiteID;
		$siteConfig = SiteConfig::get()->filter('SubsiteID', $subsiteID)->limit(1)->first();

		Debug::message("MenuContainer (" . $menuContainerID . "): " . $menuContainer->Title . " - SubsiteID: " . $menuContainer->SubsiteID . " -ResturantPageID: " . $restaurantPageID);

		// Disable Subsite filter
		// needs to be set for every loop as Subsitefilter is enabled somewhere unknown in the script
		Subsite::disable_subsite_filter(true);

		// set default FoodCategory.ID for SideDishes (here: "Beilagen")
		$foodCategorySideDishes = $this->createOrGetFoodCategory(
			// array with fields and values that are used as filter to get an existing DataObject
			array(
				'Title' => "Beilagen",
				'RestaurantPageID' => $restaurantPageID
			),
			// array with fields and values that are assigned to the DataObject
			array(
				'Title_en_US' => "Sides"
			)
		);

		$foodCategorySideDishesID = $foodCategorySideDishes->exists() ? $foodCategorySideDishes->ID : false;

		// get SimpleXMLElement of file
		$xml = $this->readXMLFileFromURL($taskconfig->URL);
		// abort is error loading file
		if(!$xml) {
			Debug::message("Could not read from xml file");
			return false;
		}

		// Loop through all wekdays
		foreach ($xml->WeekDays->children() as $xmlDailyMenuPage) {

			// save IDs of all dishes of the current dailymenupage
			$dailyMenuPageDishes = array();
			// create sort order for Dishes of a DailyMenu page (order as in xml import)
			$dishSortOrder = 0;

			// get weekday date
			$DailyMenuPageDate = date('Y-m-d', strtotime($xmlDailyMenuPage["Date"]));
			// DAILYMENUPAGE
			Debug::message(" DAY: " . $xmlDailyMenuPage["Date"]);

			// create uniqueFilter for DailyMenuPage
			$uniqueFilter = array(
				'Date' => $DailyMenuPageDate,
				'ParentID' => $menuContainerID,
				'SubsiteID' => $subsiteID
			);
			$dailyMenuPageData = $uniqueFilter;

			// get or create DailyMenuPage for given date
			$DailyMenuPage = $this->createOrGetDailyMenuPage(
				$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
				$dailyMenuPageData, // array with fields and values that are assigned to the DataObject
				$removeAssignedDishes = true // if true all assigned Dishes are removed from an existing DailyMenuPage
			);
			foreach ($xmlDailyMenuPage->MenuLine as $xmlFoodCategoryContainer) {

				// break current loop is SetMenu does not exist
				if (!isset($xmlFoodCategoryContainer->SetMenu)) break;

				// FOODCATEGORY

				$xmlFoodCategory = $xmlFoodCategoryContainer->SetMenu;

				$foodCategoryTitle = Convert::raw2sql($xmlFoodCategory["Name"]);

				// only import if FoodCategory is not in blacklist (strict checking)
				if (!in_array($foodCategoryTitle, $foodcategory_blacklist, true)) {

					// set trigger for importing dish as freitext to false (default)
					$importAsFreitext = false;
					// set trigger for importing Dish as Tagesangebot to false (default)
					$importAsTagesangebot = false;

					// generate array with FoodCategoryData data
					// key of Array needs to map the attribute of FoodCategory object 
					// (e.g. $data['Title_en_US'] will be written to FoodCategory->Title_en_US
					$foodCategoryData = array();

					Debug::message(" - - FOODCATEGORY: " . $xmlFoodCategory["Name"]);

					// find english translation
					foreach ($xmlFoodCategory->SetMenuTranslation as $xmlFoodcategoryTranslation) {
						// ENGLISH TRANSLATION FOODCATEGORY
						if ($xmlFoodcategoryTranslation["lang"] == 'en-gb') {
							Debug::message("	- - FOODCATEGORY TRANSLATION: " . $xmlFoodcategoryTranslation["Name"]);
							$foodCategoryData['Title_en_US'] = Convert::raw2sql($xmlFoodcategoryTranslation["Name"]);
							break;
						}
					}

					// MAP FoodCategory Titles
					// remove all numeric values from titles and map them to their base name
					// Heimat 1, Heimat 2, Heimat 3 are all mapped to "Heimat"
					// workaround to assign multiple dishes to the same FoodCategory
					// -> delegate only supports 1 Dish per FoodCategory
					$foodCategoryTitle = trim(preg_replace('/[0-9]+/', '', $foodCategoryTitle));
					if (array_key_exists('Title_en_US', $foodCategoryData)) $foodCategoryData['Title_en_US'] = trim(preg_replace('/[0-9]+/', '', $foodCategoryData['Title_en_US']));

					// create uniqueFilter for FoodCategory
					$uniqueFilter = array(
						'Title' => $foodCategoryTitle,
						'RestaurantPageID' => $restaurantPageID
					);
					// get or create FoodCategory for title
					$FoodCategory = $this->createOrGetFoodCategory(
						$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
						$foodCategoryData // array with fields and values that are assigned to the DataObject
					);
					
					// SPECIAL TRIGGER: 'Tagesangebot'
					// if Foodcategory is 'Tagesangebot'
					if($foodCategoryTitle == 'Tagesangebot') {
						$importAsTagesangebot = true;
					}

					// DISHES
					$dishCount = 0;
					$mainDishRestaurantDishID = 0;
					$sideDishIDs = array();

					foreach ($xmlFoodCategory->Component as $xmlDish) {

						// generate array with dish data
						// key of Array needs to map the attribute of Dish object 
						// (e.g. $data['Title_en_US'] will be written to Dish->Title_en_US
						$dishData = array();
						$restaurantDishData = array();
						$dishUniqueFilter = array();

						// generate arrays for storing connections to Additives, Allergens, DishLabels and SpecialLabels
						// we store the IDs of each DataObject in the array to assign tem to the Dish after creating it
						$dishAdditives = new ArrayList();
						$dishAllergens = new ArrayList();
						$dishDishLabels = new ArrayList();
						$dishSpecialLabels = new ArrayList();
						
						// Assumption for dishes:
						// - we import Components only
						// - the PLU is the unique identifier for components. So we use them as ImportID
						// - first Component of MenuLine is the main Dish
						// - all following Components are SideDishes and stored as SideDish to main Dish
						// - all SideDishes are assigned to FoodCategory "Beilagen", but not assigned to DailyMenuPage
						// create importID from Component["CompType"] and Component["CompID"]
						//$dishImportID = $xmlDish["CompType"].'-'.$xmlDish["CompID"];
						$dishImportID = Convert::raw2sql($xmlDish->ComponentDetails->ProductInfo->Product[0]['PLU']);

						$dishData['Description'] = $this->convert_xml_to_string($xmlDish->ComponentDetails->GastDesc["value"]);
						Debug::message("IMPORTING DISH: ID: $dishImportID, Name: " . $dishData['Description']);
						
						// Import as Dish-per-Restaurant (by adding RestaurantPageID)
						if (
							$siteConfig->EnableIndividualDishesPerRestaurant &&
							singleton('Dish')->hasExtension('DishesPerRestaurantDishExtension')
						) {
							// if Dish name contains special trigger "(LOKAL)"
							// add RestaurantPageID
							if(strpos($dishData['Description'], '(LOKAL)') !== false) {
								// remove special trigger string from description
								$dishData['Description'] = trim(str_replace('(LOKAL)', '', $dishData['Description']));
								// Add RestaurantPageID to unique filter
								// This will limit search for Dishes to the RestaurantPage 
								// and also add the RestaurantPageID if the Dish is created.
								// The dish with the ImportID is only unique to the restaurant
								// there can be multiple Dishes with the same ImportID but different RestaurantPageIDs
								$dishUniqueFilter['RestaurantPageID'] = $restaurantPageID;
								Debug::message('DISH IS ASSIGNED AS DISH PER RESTAURANT');
							}
							// Add RestaurantPageID with 0
							// to avoid conflicts or overwriting exisiting Dishes with the same ImportID
							// but different RestaurantPageIDs
							else {
								$dishUniqueFilter['RestaurantPageID'] = 0;
							}
						}

						// Add FoodCategory ID to RestaurantDish data
						// First Component is assigned to FoodCategory, all others to "Beilagen"
						if ($dishCount == 0) {
							$restaurantDishData['FoodCategoryID'] = $FoodCategory->ID;
						}
						else {
							$restaurantDishData['FoodCategoryID'] = $foodCategorySideDishesID;
						}

						// ENGLISH TRANSLATION DISH DESCRIPTION
						foreach ($xmlDish->ComponentDetails->GastDesc->GastDescTranslation as $dishTranslation) {
							if ($dishTranslation["lang"] == 'en-gb') {
								Debug::message("	- - - * DISH TRANSLATION: " . $dishTranslation["value"]);
								$dishData['Description_en_US'] = $this->convert_xml_to_string($dishTranslation["value"]);
								break;
							}
						}


						// get Prices (stored in <ProductInfo><Product>) 
						// we assume the first product is always the "main" product with the correct prices
						// -> Mapping of "name" of the <SetMenu> and <Product> does not work in all cases
						$xmlDishProduct = $xmlDish->ComponentDetails->ProductInfo->Product[0];
						$restaurantDishData['Price'] = Dish::convertToFloat(Convert::raw2sql($xmlDishProduct['ProductPrice']));

						// :TEMP:
						// Import Price from Component.SalesPrice (as requested by Fr. König 9.10.)
						#$restaurantDishData['Price'] = Dish::convertToFloat(Convert::raw2sql($xmlDish['SalesPrice']));
						Debug::message("ADDING Price to RestaurantDish: " . $restaurantDishData['Price']);

						// get Nutritives
						foreach ($xmlDish->ComponentDetails->NutritionInfo->Nutrient as $xmlNutrient) {
							Debug::message("	- - - * NUTRITIVE: " . $xmlNutrient["name"] . " – value: " . $xmlNutrient["value"]);
							$xmlNutrientName = Convert::raw2sql($xmlNutrient["name"]);
							//Figure out the nutritives_map
							if (array_key_exists($xmlNutrientName, $nutritives_map)) {
								Debug::message("... Nutrient $xmlNutrientName is mapped to: " . $nutritives_map[$xmlNutrientName] . " – value: " . $xmlNutrient["value"]);
								$dishData[$nutritives_map[$xmlNutrientName]] = Convert::raw2sql($xmlNutrient["value"]);
							}
						}

						// get all Additives, Allergens, DishLabels and SpecialLabels
						foreach ($xmlDish->ComponentDetails->FoodLabelInfo->FoodLabelGroup as $xmlFoodLabelGroup) {

							// ALLERGENS
							if (isset($xmlFoodLabelGroup->Allergens) && isset($xmlFoodLabelGroup->Allergens->FoodLabel)) {

								$allergenData = array();
								$allergenNumber = strtoupper(Convert::raw2sql($xmlFoodLabelGroup["code"]));
								$allergenData["Title"] = Convert::raw2sql($xmlFoodLabelGroup->Allergens->FoodLabel[0]["name"]);

								Debug::message("IMPORTING ALLERGEN: " . $allergenData["Title"]);

								// when Allergen is not in blacklist (check for title)
								if (!in_array($allergenData["Title"], $allergens_blacklist, true)) {

									// :TODO: Import description from $xmlFoodLabelGroup->Allergens->FoodLabel->FoodLabelTranslation (that does not exist right now)
									// find english translation
									foreach ($xmlFoodLabelGroup->FoodLabelGroupTranslation as $allergenTranslation) {
										if ($allergenTranslation["lang"] == 'en-gb') {
											Debug::message("	- - - * ALLERGEN TRANSLATION: " . Convert::raw2sql($allergenTranslation["name"]));
											$allergenData['Title_en_US'] = Convert::raw2sql($allergenTranslation["name"]);
											break;
										}
									}

									// create uniqueFilter for Allergen
									// by adding the SubsiteID we define that we want to create a SubsiteAllergen
									$uniqueFilter = array(
										'Number' => $allergenNumber,
										'SubsiteID' => $subsiteID
									);
									// get or create SubsiteAllergen for Number
									$Allergen = $this->createOrGetAllergen(
										$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
										$allergenData  // array with fields and values that are assigned to the DataObject
									);
									// assign Allergen ID
									if ($Allergen) {
										$dishAllergens->push($Allergen);
									}

									Debug::show($allergenData);
								}
							}

							// ADDITIVES
							else if (isset($xmlFoodLabelGroup->Additives)) {

								$additiveData = array();
								$additiveData["Title"] = $xmlFoodLabelGroup["name"];
								Debug::message("	- - - * ADDITIVE - Additives exist inside: " . $additiveData["Title"]);
								// ADDITIVES TRANSLATION
								foreach ($xmlFoodLabelGroup->FoodLabelGroupTranslation as $additiveTranslation) {
									if ($additiveTranslation["lang"] == 'en-gb') {
										Debug::message("	- - - * ADDITIVE TRANSLATION: " . $additiveTranslation["name"]);
									}
								}

								$additiveNumber = Convert::raw2sql($xmlFoodLabelGroup["code"]);
								$additiveData["Title"] = Convert::raw2sql($xmlFoodLabelGroup["name"]);

								Debug::message("IMPORTING ADDITIVE - Additives exist inside: " . $additiveData["Title"]);

								// when Additive is not in blacklist (check for title)
								if (!in_array($additiveData["Title"], $additives_blacklist, true)) {

									// find english translation
									foreach ($xmlFoodLabelGroup->FoodLabelGroupTranslation as $additiveTranslation) {
										if ($additiveTranslation["lang"] == 'en-gb') {
											Debug::message("	- - - * ADDITIVE TRANSLATION: " . Convert::raw2sql($additiveTranslation["name"]));
											$additiveData['Title_en_US'] = Convert::raw2sql($additiveTranslation["name"]);
											break;
										}
									}

									// create uniqueFilter for Additive
									// by adding the SubsiteID we define that we want to create a SubsiteAdditive
									$uniqueFilter = array(
										'Number' => $additiveNumber,
										'SubsiteID' => $subsiteID
									);
									// get or create SubsiteAdditive for title
									$Additive = $this->createOrGetAdditive(
										$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
										$additiveData  // array with fields and values that are assigned to the DataObject
									);
									if ($Additive) {
										$dishAdditives->push($Additive);
									}
								}
							}

							// SPECIALLABELS
							else if ($xmlFoodLabelGroup["name"] == "Aktion" && isset($xmlFoodLabelGroup->Information->FoodLabel)) {
								foreach ($xmlFoodLabelGroup->Information->FoodLabel as $specialLabel) {

									$specialLabelData = array();
									$specialLabelTitle = Convert::raw2sql($specialLabel["name"]);

									Debug::message("IMPORTING SPECIALLABEL: " . $specialLabelTitle);
									
									// when SpecialLabel is not in blacklist (check for title)
									if(!in_array($specialLabelTitle, $speciallabels_blacklist, true)) {

										// find english translation
										foreach ($specialLabel->FoodLabelTranslation as $specialLabelTranslation) {
											if ($specialLabelTranslation["lang"] == 'en-gb') {
												Debug::message("	- - - * SPECIALLABEL TRANSLATION: " . Convert::raw2sql($specialLabelTranslation["name"]));
												$specialLabelData['Title_en_US'] = Convert::raw2sql($specialLabelTranslation["name"]);
												break;
											}
										}

										// create uniqueFilter for SpecialLabel
										// by adding the SubsiteID we define that we want to create a SubsiteAdditive
										$uniqueFilter = array(
											'Title' => $specialLabelTitle,
											'SubsiteID' => $subsiteID
										);
										// get or create SubsiteSpecialLabel for title
										$SpecialLabel = $this->createOrGetSpecialLabel(
											$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
											$specialLabelData  // array with fields and values that are assigned to the DataObject
										);
										if ($SpecialLabel) {
											$dishSpecialLabels->push($SpecialLabel);
										}
									}
								}
							}

							// DISHLABELS
							else if (isset($xmlFoodLabelGroup->Information->FoodLabel)) {

								foreach ($xmlFoodLabelGroup->Information->FoodLabel as $xmlDishLabel) {

									// SPECIAL TRIGGER
									// If Component is assigned "Freitext" it will be imported without ImportID
									// Freitext is a Dish based on always the same ImportID, but with changing description and attributes
									if ($xmlDishLabel["name"] == "Freitext" && isset($xmlFoodCategory->SetMenuDetails->GastDesc)) {

										Debug::message("FREITEXT FOUND: " . $xmlFoodCategory->SetMenuDetails->GastDesc["value"]);

										$importAsFreitext = true;

										// freitext is available in SetMenu > SetMenuDetails > GastDesc
										$freitextDescription = $this->convert_xml_to_string($xmlFoodCategory->SetMenuDetails->GastDesc["value"]);
										$dishData['Description'] = $freitextDescription;

										// find english translation
										foreach ($xmlFoodCategory->SetMenuDetails->GastDesc->GastDescTranslation as $freitextDescriptionTranslation) {
											if ($freitextDescriptionTranslation["lang"] == 'en-gb') {
												Debug::message("FREITEXT TRANSLATION: " . Convert::raw2sql($freitextDescriptionTranslation["value"]));
												$dishData['Description_en_US'] = Convert::raw2sql($freitextDescriptionTranslation["value"]);
												break;
											}
										}

										break;
									}
									else {

										$dishLabelData = array();
										$dishLabelTitle = Convert::raw2sql($xmlDishLabel["name"]);

										Debug::message("	- - - * DISHLABEL: " . $dishLabelTitle);

										// do not import DishLabel when DishLabel is in blacklist (check for title)
										if (!in_array($dishLabelTitle, $dishlabels_blacklist, true)) {

											// find english translation
											foreach ($xmlDishLabel->FoodLabelTranslation as $dishLabelTranslation) {

												if ($dishLabelTranslation["lang"] == 'en-gb') {
													Debug::message(" ENGLISH TRANSLATION: " . Convert::raw2sql($dishLabelTranslation["name"]));
													$dishLabelData['Title_en_US'] = Convert::raw2sql($dishLabelTranslation["name"]);
													break;
												}
											}

											// create uniqueFilter for DishLabel
											// by adding the SubsiteID we define that we want to create a SubsiteDishLabel
											$uniqueFilter = array(
												'Title' => $dishLabelTitle,
												'SubsiteID' => $subsiteID
											);
											// get or create GlobalDishLabel for Title
											$DishLabel = $this->createOrGetDishLabel(
												$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
												$dishLabelData  // array with fields and values that are assigned to the DataObject
											);
											if ($DishLabel) {
												$dishDishLabels->push($DishLabel);
											}
										}
									}
								}
							}
						}

						// SPECIAL TRIGGER
						if ($dishCount == 0) {

							// PREFIX "HINWEIS: ": DailyMenuPage Message
							// if Description is prefixed with "HINWEIS" -> do not import as Dish but add a holiday info to DailyMenuPage
							if (strpos($dishData['Description'], "HINWEIS: ") !== false) {
								// get beginning of first occurence of "HINWEIS: "
								$pos = strpos($dishData['Description'], "HINWEIS: ");
								// get "HINWEIS and everything that follows it
								$DailyMenuPage->Message = str_replace("HINWEIS: ", "", substr($dishData['Description'], $pos));
								if (array_key_exists('Description_en_US', $dishData)) {
									// get beginning of first occurence of "HINWEIS: "
									$pos = strpos($dishData['Description_en_US'], "HINWEIS: ");
									$DailyMenuPage->Message_en_US = str_replace("HINWEIS: ", "", substr($dishData['Description_en_US'], $pos));
								}
								$DailyMenuPage->write();
								$DailyMenuPage->publish('Stage', 'Live'); // publish from Stage to Live

								Debug::message("... SPECIAL TRIGGER 'HINWEIS' FOUND: Adding Infotext: " . $DailyMenuPage->Message);

								// abort processing of current dish and following sidedishes (skip current FoodCategory loop)
								continue 2;
							}

							// POSTFIX "EMPFEHLUNG: ": Dish Infotext
							// if Description is contains new line beginning with "EMPFEHLUNG: " -> do add this to Dish.Infotext and remove the string from Dish.Description
							preg_match("/EMPFEHLUNG: (.*)/", $dishData['Description'], $matchEmpfehlung);
							if ($matchEmpfehlung) {
								// get "EMPFEHLUNG: " and everything that follows it
								$dishData['Infotext'] = $matchEmpfehlung[1];
								// remove "EMPFEHLUNG: " text from Dish.Description
								$dishData['Description'] = preg_replace("/EMPFEHLUNG: (.*)/", "", $dishData['Description']);

								// find english translation
								if (array_key_exists('Description_en_US', $dishData)) {
									preg_match("/EMPFEHLUNG: (.*)/", $dishData['Description_en_US'], $matchEmpfehlung_en_US);
									if ($matchEmpfehlung_en_US) {
										$dishData['Infotext_en_US'] = $matchEmpfehlung_en_US[1];
										$dishData['Description_en_US'] = preg_replace("/EMPFEHLUNG: (.*)/", "", $dishData['Description_en_US']);
									}
								}

								Debug::message("... SPECIAL TRIGGER 'EMPFEHLUNG' FOUND: Adding Infotext: " . $dishData['Infotext']);
							}
						}

						// - makes it only visible / editable to current restaurant
						// - Can be used by cron job that deletes all custom (not imported) Dishes
						// If Dish is freitext: Always create a new Dish (without checking if a dish with same description or import id already exists)
						// 
						if ($importAsFreitext) {
							// Add Subsite ID to $dishData
							$dishData['SubsiteID'] = $subsiteID;
							// Add ImportID "Freitext" to Dish to mark it as imported freitext Dish
							// can be used to delete all freitext Dishes by a cronjob to clean up the database
							$dishData['ImportID'] = "Freitext";
							$Dish = $this->createDish($dishData);
						}
						// import as Tagesangebot
						// - import as restaurant specific Dish
						// - add ImportID "Tagesangebot" which can be used to delete all Tagesangebot dishes by cron job
						else if($importAsTagesangebot) {
							// Add Subsite ID to $dishData
							$dishData['SubsiteID'] = $subsiteID;
							// Add ImportID "Tagesangebot" to Dish to mark it as imported Tagesangebot Dish
							$dishData['ImportID'] = "Tagesangebot";
							$Dish = $this->createDish($dishData);
						}
						// else: create or get a Dish with given ImportID
						else {
							// add values to uniqueFilter for Dish
							$dishUniqueFilter['ImportID'] = $dishImportID;
							$dishUniqueFilter['SubsiteID'] = $subsiteID;
							// get or create Dish for given ImportID
							$Dish = $this->createOrGetDish(
								$dishUniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
								$dishData  // array with fields and values that are assigned to the DataObject
							);
						}
						// DISH / RESTAURANT DISH ASSIGNEMENTS
						if ($Dish) {

							// Add RestaurantDishID to Dish if it is marked as 'Freitext' or 'Tagesangebot'
							// and the DishesPerRestaurantDishExtension exists
							// -> restricts access to Dish to the current Restaurant
							if (
								($importAsFreitext || $importAsTagesangebot) &&
								$siteConfig->EnableIndividualDishesPerRestaurant &&
								$Dish->hasExtension('DishesPerRestaurantDishExtension')
							) {
								$Dish->RestaurantPageID = $restaurantPageID;
								$Dish->write();
							}

							// create uniqueFilter for RestaurantDish
							$uniqueFilter = array(
								'DishID' => $Dish->ID,
								'RestaurantPageID' => $restaurantPageID
							);
							// get or create RestaurantDish for DishID
							$RestaurantDish = $this->createOrGetRestaurantDish(
								$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
								$restaurantDishData // array with fields and values that are assigned to the DataObject
							);

							// Dish is main component: store as Dish
							// assign Dish to DailyMenuPage
							if ($dishCount == 0) {
								Debug::message("SAVING AS MAIN DISH  SortOrder: " . $dishSortOrder);
								$this->addDishToDailyMenuPage($Dish->ID, $DailyMenuPage->ID, $dishSortOrder);
								$mainDishRestaurantDishID = $RestaurantDish->ID;
								// count up dish sort order (per DailyMenuPage)
								$dishSortOrder++;
							}
							// Dish is side component: add ID to $sideDishIDs
							// :TODO: assign sideDishes not to DailyMenuPage?
							else {
								$sideDishIDs[] = $Dish->ID;
								Debug::message("SAVING AS SIDE DISH ");
							}

							// ASSIGN MANY_MANY RELATIONS
							// assign Additives
							foreach ($dishAdditives as $additive) {
								$this->addManyManyToDataObject($Dish, $additive);
							}
							// remove all but the imported Additives
							$this->removeManyManyFromDataObject($Dish, 'SubsiteAdditive', array_keys($dishAdditives->map()));

							// assign Allergens
							foreach ($dishAllergens as $allergen) {
								$this->addManyManyToDataObject($Dish, $allergen);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteAllergen', array_keys($dishAllergens->map()));

							// assign DishLabels
							foreach ($dishDishLabels as $dishlabel) {
								$this->addManyManyToDataObject($Dish, $dishlabel);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteDishLabel', array_keys($dishDishLabels->map()));

							// assign SpecialLabels
							foreach ($dishSpecialLabels as $speciallabel) {
								$this->addManyManyToDataObject($Dish, $speciallabel);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteSpecialLabel', array_keys($dishSpecialLabels->map()));
						}
						// count up dishcount to determin if we are working with a Dish (dishcount = 0) or a SideDish (dishcount > 0)
						$dishCount++;
					}
					// Add SideDishes to MainRestaurantDish
					// remove all sideDishes from RestaurantDish
					$this->removeSideDishesFromRestaurantDish($mainDishRestaurantDishID);
					// add sideDishes to main Dish (assigning Dish.ID to RestaurantDish)
					foreach ($sideDishIDs as $sideDishID) {
						$this->addSideDishToRestaurantDish($mainDishRestaurantDishID, $sideDishID);
					}
				}
			}
		}

		// Enable Subsite filter
		// needs to be set for every loop as Subsitefilter is enabled somewhere unknown in the script
		Subsite::disable_subsite_filter(false);
		$succed = True;
		return $succed;
	}
	
	/**
	 * Clean up database before importing Dishes to MenuContainer.
	 * 
	 * Delete all Dishes of the Subsite the $taskconfig is connected to
	 * - that are not assigned to a DailyMenuPage of the current Subsite
	 *   or who are assigned to a DailyMenuPage of the current Subsite 
	 *   AND whose Date is before the current week
	 * - AND whose ImportID is "Freitext" or "Tagesangebot"
	 * 
	 * This removes all unused or no more visible Dishes with a generic ImportID
	 * (which will not be overwritten by import but will be imported agein 
	 * with every import)
	 * 
	 * @param ImportMenuXmlConfig $taskconfig The ImportMenuXmlConfig that holds the settings for the current task
	 */
	public function cleanup_db_lz_catering($taskconfig) {
		$start_date_curr_week = DateUtil::startDateOfTheWeek(date('Y-m-d'));
		
		// Disable Subsite filter
		Subsite::disable_subsite_filter(true);
		
		// Get all DailyMenuPages of the current Subsite
		// whose Date is greater or equal with the first day in the current week
		$DailyMenuPages = DailyMenuPage::get()->filter(array(
			'SubsiteID' => $taskconfig->MenuContainer()->SubsiteID,
			'Date:GreaterThanOrEqual' => $start_date_curr_week
		));
		
		// Select all Dishes with ImportID "Tagesangebot" and "Freitext" 
		// that are not connected with a current or future DailyMenuPage
		// OR who are not connected to a DailyMneuPage at all
		$DeleteDishes = Dish::get()
			->filter(array(
			'ImportID' => array('Freitext', 'Tagesangebot'),
			))
			->filterAny(array(
				'DailyMenuPages.ID:not' => $DailyMenuPages->map()->keys(),
				'DailyMenuPages.ID' => NULL
			));
		
		// Delete those dishes
		// Note: Relations of Dish will be deleted as well. 
		// See: Dish::onBeforeDelete()
		foreach($DeleteDishes as $DeleteDish) {
			Debug::message("Deleting Dish $DeleteDish->ID: $DeleteDish->Description");
			$DeleteDish->delete();
		}
		
		// Enable Subsite filter
		Subsite::disable_subsite_filter(false);
	}

	public function import_roche_rotkreuz($taskconfig) {
		Debug::message("ImportXMLRocheRotkreuze()");
		
		// Array that defines mapping of nutritives from xml file to Dish variables
		// array( [XMLnutritiveName] => [DishNutritiveVariableName], ...)
		$nutritives_map = array();
		$NutritivesMapConfig = $taskconfig->NutritivesMapConfig();
		if($NutritivesMapConfig && $NutritivesMapConfig->exists()) {
			$NutritivesMapObjects = $NutritivesMapConfig->NutritivesMapObjects();
			if($NutritivesMapObjects && $NutritivesMapObjects->exists()) {
				$nutritives_map = $NutritivesMapObjects->map('ExternalKey','InternalKey')->toArray();
			}
		}
		Debug::show($nutritives_map);
		
		// Array that defines a blacklist of FoodCategories
		// which are not imported
		$foodcategory_blacklist = array();
		$FoodCategoryBlacklistConfig = $taskconfig->FoodCategoryBlacklistConfig();
		if($FoodCategoryBlacklistConfig && $FoodCategoryBlacklistConfig->exists()) {
			$FoodCategoryBlacklistObjects = $FoodCategoryBlacklistConfig->FoodCategoryBlacklistObjects();
			if($FoodCategoryBlacklistObjects && $FoodCategoryBlacklistObjects->exists()) {
				$foodcategory_blacklist = $FoodCategoryBlacklistObjects->map()->toArray();
			}
		}
		Debug::show($foodcategory_blacklist);
		
		// set to stage mode to create all data in Stage and then publish to Live
		$currStage = Versioned::current_stage();
		Versioned::set_reading_mode(''); // set to stage
		
		
		// collect necessary data
		$menuContainerID = $taskconfig->MenuContainerID;
		$menuContainer = MenuContainer::get()->byID($menuContainerID);
		$restaurantPageID = $menuContainer->parent->ID;
		$subsiteID = $menuContainer->SubsiteID;
		
		
		Debug::message("MenuContainer (".$menuContainerID."): ".$menuContainer->Title." - SubsiteID: ".$menuContainer->SubsiteID." -ResturantPageID: ".$restaurantPageID);
		
		
		// Disable Subsite filter
		// needs to be set for every loop as Subsitefilter is enabled somewhere unknown in the script
		Subsite::disable_subsite_filter(true);
		
		// get SimpleXMLElement of file
		$xml = $this->readXMLFileFromURL($taskconfig->URL);
		// abort is error loading file
		if(!$xml) return false;
		
		// Loop through all wekdays
		foreach ($xml->WeekDays->children() as $xmlDailyMenuPage) {
		// save IDs of all dishes of the current dailymenupage
		$dailyMenuPageDishes = array();
		// create sort order for Dishes of a DailyMenu page (order as in xml import)
		$dishSortOrder = 0;
			
		// DAILYMENUPAGE
		// get weekday date
		$DailyMenuPageDate = date('Y-m-d', strtotime($xmlDailyMenuPage["Date"]));
		Debug::message("IMPORTIERE TAG: $DailyMenuPageDate - menuContainerID: $menuContainerID - subsiteID: $subsiteID");
		
		// create uniqueFilter for DailyMenuPage
		$uniqueFilter = array(
			'Date'		=> $DailyMenuPageDate,
			'ParentID'	=> $menuContainerID,
			'SubsiteID'	=> $subsiteID
		);
		$dailyMenuPageData = $uniqueFilter;

		// get or create DailyMenuPage for given date
		$DailyMenuPage = $this->createOrGetDailyMenuPage(
			$uniqueFilter,					// array with fields and values that are used as filter to get an existing DataObject
			$dailyMenuPageData,				// array with fields and values that are assigned to the DataObject
			$removeAssignedDishes = true	// if true all assigned Dishes are removed from an existing DailyMenuPage
			);
		
		// FOODCATEGORY
		foreach($xmlDailyMenuPage->MenuLine->SetMenu as $xmlFoodCategory) {
				
				$foodCategoryTitle = Convert::raw2sql($xmlFoodCategory["Name"]);
				
				// only import if FoodCategory is not in blacklist
				if(!in_array($foodCategoryTitle, $foodcategory_blacklist)) {
				
					// generate array with FoodCategoryData data
					// key of Array needs to map the attribute of FoodCategory object 
					// (e.g. $data['Title_en_US'] will be written to FoodCategory->Title_en_US
					$foodCategoryData = array();

					Debug::message("----------<br />IMPORTING FOODCATEGORY: ".Convert::raw2sql($xmlFoodCategory["Name"]));

					// find english translation
					foreach($xmlFoodCategory->SetMenuTranslation as $xmlFoodcategoryTranslation) {
						if($xmlFoodcategoryTranslation["lang"] == 'en-gb') {
							Debug::message("FOODCATEGORY ENGLISH TRANSLATION: ".Convert::raw2sql($xmlFoodcategoryTranslation["Name"]));
							$foodCategoryData['Title_en_US'] = Convert::raw2sql($xmlFoodcategoryTranslation["Name"]);
							break;
						}
					}

					// MAP FoodCategory Titles
					$foodCategoryTitleLower = strtolower($foodCategoryTitle);

						// map FoodCategories beginning with "suppe" to "suppe" e.g. "suppe 1" => "suppe"
						if(substr($foodCategoryTitleLower, 0, 5) == "suppe") {
							$foodCategoryTitle = "suppe";
							if(array_key_exists('Title_en_US', $foodCategoryData)) $foodCategoryData['Title_en_US'] = "soup";
						}


					// create uniqueFilter for FoodCategory
					$uniqueFilter = array(
						'Title'				=> $foodCategoryTitle,
						'RestaurantPageID'	=> $restaurantPageID
					);
					// get or create FoodCategory for title
					$FoodCategory = $this->createOrGetFoodCategory(
						$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
						$foodCategoryData	// array with fields and values that are assigned to the DataObject
					);
					

					// DISHES
					foreach($xmlFoodCategory->SetMenuDetails as $xmlDish) {

						// generate array with dish data
						// key of Array needs to map the attribute of Dish object 
						// (e.g. $data['Title_en_US'] will be written to Dish->Title_en_US
						$dishData = array();
						$restaurantDishData = array();

						// generate arrays for storing connections to Additives, Allergens, DishLabels and SpecialLabels
						// we store the IDs of each DataObject in the array to assign tem to the Dish after creating it
						$dishAdditives = new ArrayList();
						$dishAllergens = new ArrayList();
						$dishDishLabels = new ArrayList();
						$dishSpecialLabels = new ArrayList();

						$dishData['Description'] = $this->convert_xml_to_string($xmlDish->GastDesc["value"]);

						Debug::message("---------- IMPORTING DISH: ".$dishData['Description']);

						// Add FoodCategory ID to RestaurantDish data
						$restaurantDishData['FoodCategoryID'] = $FoodCategory->ID;

						// find english translation
						/**
						 * TEMPORARY DISABLED
						 * english translation for GastDesc in SetMenuDetails is not exportet in xml file
						 * so we use the english descriptions of all components instead 
						 */
						/*
						foreach($xmlDish->GastDesc->GastDescTranslation as $dishTranslation) {
							Debug::message("looking for english translation of Dish");
							if($dishTranslation["lang"] == 'en-gb') {
								Debug::message("ENGLISH TRANSLATION: ".Convert::raw2sql($dishTranslation["value"]));
								$dishData['Description_en_US'] = $importController->convert_xml_to_string($dishTranslation["value"]);
								break;
							}
						}
						*/
						$dishDescription_en_USArray = array();
						foreach($xmlFoodCategory->Component as $xmlDishComponent) {
							foreach($xmlDishComponent->ComponentDetails->GastDesc->GastDescTranslation as $dishTranslation) {
								if($dishTranslation["lang"] == 'en-gb') {
									Debug::message("ENGLISH TRANSLATION DISH : ".Convert::raw2sql($dishTranslation["value"]));
									// remove line breaks and spaces from end of string
									$dishDescription_en_USArray[] = rtrim($this->convert_xml_to_string($dishTranslation["value"]));
									break;
								}
							}
						}
						if(count($dishDescription_en_USArray)) $dishData['Description_en_US'] = implode("\r\n", $dishDescription_en_USArray);

						// SPECIAL TRIGGER

						// PREFIX "INF": DailyMenuPage InfoText
						// if Description is prefixed with "INF" -> do not import as Dish but add a hliday info to DailyMenuPage
						if(substr($dishData['Description'], 0, 3) == "INF") {
							$DailyMenuPage->InfoText = $this->convert_xml_to_string(str_replace("INF ", "", $dishData['Description']));
							if(array_key_exists('Description_en_US', $dishData)) $DailyMenuPage->InfoText_en_US = $this->convert_xml_to_string(substr(str_replace("INF ", "", $dishData['Description_en_US'])));
							$DailyMenuPage->write();
							$DailyMenuPage->publish('Stage', 'Live'); // publish from Stage to Live

							Debug::message(".. SPECIAL TRIGGER 'INF' FOUND: Adding Infotext: ".$DailyMenuPage->InfoText);

							// abort processing of current dish
							break;
						}

						// PREFIX "AKTION": SpecialLabel
						// if description is prefixed with "AKTION" 
						// -> get the following string on the same line and import it as Title for SpecialLabel
						if(substr($dishData['Description'], 0, 6) == "AKTION") {

							$specialLabelData = array();
							$tmpDishDescription = '';
							$tmpDishDescription_en_US = '';
							$linecount = 0;

							// extract german SpecialLabel data
							foreach(preg_split("/((\r?\n)|(\n?\r))/", $dishData['Description']) as $line){
								// extract title for SpecialLabel from first line
								if($linecount == 0) {
									$specialLabelTitle = str_replace("AKTION ", "", $line);
								}
								// add all following lines as Dish.Description
								else {
									if($linecount > 1) $tmpDishDescription .= "\r\n";
									$tmpDishDescription .= $line;
								}
								$linecount++;
							}
							// assign Dish Description
							$dishData['Description'] = $this->convert_xml_to_string($tmpDishDescription);

							Debug::message(".. SPECIAL TRIGGER 'AKTION' FOUND: Adding SpecialLabel: ".$specialLabelTitle."  and Description: ".$tmpDishDescription ."- ".$dishData['Description_en_US']);						

							// extract english SpecialLabel data
							if(array_key_exists('Description_en_US', $dishData) && substr($dishData['Description_en_US'], 0, 6) == "AKTION") {
								foreach(preg_split("/((\r?\n)|(\n?\r))/", $dishData['Description_en_US']) as $line){
									// extract title for SpecialLabel from first line
									if($linecount == 0) {
										$specialLabelData['Title_en_US'] = str_replace("AKTION ", "", $line);
									}
									// add all following lines as Dish.Description_en_US
									else {
										if($linecount > 1) $tmpDishDescription_en_US .= "\r\n";
										$tmpDishDescription_en_US .= $line;
									}
									$linecount++;
								}
								// assign Dish Description_en_US
								$dishData['Description_en_US'] = $tmpDishDescription;

								Debug::message("... found Aktion english description: ".$specialLabelData['Title_en_US']." - ".$dishData['Description_en_US']);	
							}

							// SPECIAL LABEL
							// create uniqueFilter for SpecialLabel
							// by adding the SubsiteID we define that we want to create a SubsiteSpecialLabel
							$uniqueFilter = array(
								'Title'		=> $specialLabelTitle,
								'SubsiteID' => $subsiteID
							);
							// get or create GlobalSpecialLabel for Title
							$SpecialLabel = $this->createOrGetSpecialLabel(
								$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
								$specialLabelData	// array with fields and values that are assigned to the DataObject
							);
							// assign SpecialLabel ID
							if($SpecialLabel) {
								$dishSpecialLabels->push($SpecialLabel);
							}
						}

						// get Import ID
						// :TODO: Check if IdentNr of SetMenu is really a unique identifier for the menu
						$dishImportID = Convert::raw2sql($xmlFoodCategory['IdentNr']);

						// get Prices (stored in <SetMenu> which corresponds to FoodCategory)
						$restaurantDishData['Price'] = Convert::raw2sql($xmlFoodCategory['ProductPrice']);
						$restaurantDishData['PriceExtern'] = Convert::raw2sql($xmlFoodCategory['ProductPrice3']);
						$restaurantDishData['PriceExternSmall'] = Convert::raw2sql($xmlFoodCategory['ProductPrice6']);
						
						// get Nutritives
						foreach($xmlDish->NutritionInfo->Nutrient as $xmlNutrient) {
							Debug::message("----------IMPORTING NUTRITIVE: ".$xmlNutrient["name"]);
							$xmlNutrientName = Convert::raw2sql($xmlNutrient["name"]);
							if(array_key_exists($xmlNutrientName, $nutritives_map)) {
								Debug::message("... Nutrient $xmlNutrientName is mapped to: ".$nutritives_map[$xmlNutrientName]." – value: ".round($xmlNutrient["value"]));
								$dishData[$nutritives_map[$xmlNutrientName]] = round(Convert::raw2sql($xmlNutrient["value"]));
							}
						}
						
						
						
						// get all Additives, Allergens and DishLabels
						foreach($xmlDish->FoodLabelInfo->FoodLabelGroup as $xmlFoodLabelGroup) {

							// ALLERGENS
							if($xmlFoodLabelGroup["name"] == "Allergene" && ($xmlFoodLabelGroup->Allergens != null && $xmlFoodLabelGroup->Allergens->FoodLabel != null)) {

								foreach($xmlFoodLabelGroup->Allergens->FoodLabel as $xmlAllergen) {
									$allergenData = array();
									$allergenNumber = strtoupper(Convert::raw2sql($xmlAllergen["code"]));
									$allergenData["Title"] = Convert::raw2sql($xmlAllergen["name"]);								

									Debug::message("----------IMPORTING ALLERGEN: ".$allergenData["Title"]);

									// find english translation
									foreach($xmlAllergen->FoodLabelTranslation as $allergenTranslation) {
										if($allergenTranslation["lang"] == 'en-gb') {
											Debug::message("ENGLISH TRANSLATION ALLERGEN : ".Convert::raw2sql($allergenTranslation["name"]));
											$allergenData['Title_en_US'] = Convert::raw2sql($allergenTranslation["name"]);
											break;
										}
									}

									// create uniqueFilter for Allergen
									// by adding the SubsiteID we define that we want to create a SubsiteAllergen
									$uniqueFilter = array(
										'Number' => $allergenNumber,
										'SubsiteID' => $subsiteID
									);
									// get or create GlobalAllergen for Number
									$Allergen = $this->createOrGetAllergen(
										$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
										$allergenData		// array with fields and values that are assigned to the DataObject
									);
									// assign Allergen ID
									if($Allergen) {
										$dishAllergens->push($Allergen);
									}
									//For Debug
//									Debug::show($allergenData);

								}
							}

							// DISHLABELS and INFOTEXT
							else if($xmlFoodLabelGroup["name"] == "Anzeige Visuals" && ($xmlFoodLabelGroup->Information != null && $xmlFoodLabelGroup->Information->FoodLabel != null)) {

								foreach($xmlFoodLabelGroup->Information->FoodLabel as $xmlDishLabel) {
									$dishLabelData = array();
									$dishLabelTitle = Convert::raw2sql($xmlDishLabel["name"]);

									Debug::message("----------<br />IMPORTING DISHLABEL: ".$dishLabelTitle);

									// find english translation
									foreach($xmlDishLabel->FoodLabelTranslation as $dishLabelTranslation) {
										if($dishLabelTranslation["lang"] == 'en-gb') {
											Debug::message("ENGLISH TRANSLATION DISHLABEL : ".Convert::raw2sql($dishLabelTranslation["name"]));
											$dishLabelData['Title_en_US'] = Convert::raw2sql($dishLabelTranslation["name"]);
											break;
										}
									}

									// SPECIAL CASE
									// Herkunftsland
									if(substr($dishLabelTitle, 0, 8) == "Herkunft") {

										// Add "Herkunftsland" to Dish.InfoText
										$dishData["Infotext"] = str_replace("Herkunft ", "", $dishLabelTitle); // remove "Herkunft " from beginning of string
										if(array_key_exists('Title_en_US', $dishLabelData)) $dishData["Infotext_en_US"] = str_replace("Origin ", "", $dishLabelData["Title_en_US"]); // remove "Origin " from beginning of string

										Debug::message("----------<br />DISHLABEL IS HERKUNFTSLAND: ".$dishData["InfoText"]." - ".$dishData["InfoText_en_US"]);

									}
									// DEFAULT
									// get or create DishLabel
									else {
										// create uniqueFilter for DishLabel
										// by adding the SubsiteID we define that we want to create a SubsiteDishLabel
										$uniqueFilter = array(
											'Title' => $dishLabelTitle,
											'SubsiteID' => $subsiteID
										);
										// get or create GlobalDishLabel for Title
										$DishLabel = $this->createOrGetDishLabel(
											$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
											$dishLabelData		// array with fields and values that are assigned to the DataObject
										);
										if($DishLabel) {
											$dishDishLabels->push($DishLabel);
										}
									}
								}
							}

							// ADDITIVES
							else if($xmlFoodLabelGroup->Additives != null) {

								$additiveData = array();
								$additiveTitle = Convert::raw2sql($xmlFoodLabelGroup["name"]);

								Debug::message("----------IMPORTING ADDITIVE - Additives exist inside: ".$additiveTitle);

								// find english translation
								foreach($xmlFoodLabelGroup->FoodLabelGroupTranslation as $additiveTranslation) {
									if($additiveTranslation["lang"] == 'en-gb') {
										Debug::message("ENGLISH TRANSLATION ADDITIVE : ".Convert::raw2sql($additiveTranslation["name"]));
										$additiveData['Title_en_US'] = Convert::raw2sql($additiveTranslation["name"]);
										break;
									}
								}

								// create uniqueFilter for Additive
								// by adding the SubsiteID we define that we want to create a SubsiteAdditive
								$uniqueFilter = array(
									'Title'		=> $additiveTitle,
									'SubsiteID' => $subsiteID
								);
								// get or create GlobalAdditive for title
								$Additive = $this->createOrGetAdditive(
									$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
									$additiveData		// array with fields and values that are assigned to the DataObject
								);
								if($Additive) {
									$dishAdditives->push($Additive);
								}
							}
						}

//						Debug::message('IMPORTED DISH DATA');
//						Debug::show($dishData);
//						Debug::show($restaurantDishData);
//						Debug::message('ASSIGEND ADDITIVES');
//						Debug::show($dishAdditives);
//						Debug::message('ASSIGEND ALLERGENS');
//						Debug::show($dishAllergens);
//						Debug::message('ASSIGEND DISH LABELS');
//						Debug::show($dishDishLabels);
//						Debug::message('ASSIGEND SPECIAL LABELS');
//						Debug::show($dishSpecialLabels);

						// create uniqueFilter for Dish
						$uniqueFilter = array(
							'Description' => $dishData['Description'],
							#'RestaurantDish.FoodCategoryID' => $restaurantDishData['FoodCategoryID'], // add FoodCategory ID to unique query, so that a new dish is created if the same Dish Description is assigned to different FoodCategories
							'SubsiteID' => $subsiteID
						);
						// get or create Dish for title
						$Dish = $this->createOrGetDish(
							$uniqueFilter,	// array with fields and values that are used as filter to get an existing DataObject
							$dishData		// array with fields and values that are assigned to the DataObject
						);
						// RESTAURANTDISH
						if($Dish) {
							// create uniqueFilter for RestaurantDish
							$uniqueFilter = array(
								'DishID' => $Dish->ID,
								'RestaurantPageID' => $restaurantPageID
							);
							// get or create RestaurantDish for DishID
							$RestaurantDish = $this->createOrGetRestaurantDish(
								$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
								$restaurantDishData	// array with fields and values that are assigned to the DataObject
							);
							// assign Dish to DailyMenuPage
							$this->addDishToDailyMenuPage($Dish->ID, $DailyMenuPage->ID, $dishSortOrder);
							
							// ASSIGN MANY_MANY RELATIONS

							// assign Additives
							foreach($dishAdditives as $additive) {
								$this->addManyManyToDataObject($Dish, $additive);
							}
							// remove all but the imported Additives
							$this->removeManyManyFromDataObject($Dish, 'SubsiteAdditive', array_keys($dishAdditives->map()));
							
							// assign Allergens
							foreach($dishAllergens as $allergen) {
								$this->addManyManyToDataObject($Dish, $allergen);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteAllergen', array_keys($dishAllergens->map()));
							
							// assign DishLabels
							foreach($dishDishLabels as $dishlabel) {
								$this->addManyManyToDataObject($Dish, $dishlabel);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteDishLabel', array_keys($dishDishLabels->map()));
							
							// assign SpecialLabels
							foreach($dishSpecialLabels as $speciallabel) {
								$this->addManyManyToDataObject($Dish, $speciallabel);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteSpecialLabel', array_keys($dishSpecialLabels->map()));
							
						}

						// count up dish sort order (per DailyMenuPage)
						$dishSortOrder++;
					}		
				}
			}	
		}	
		
		// Enable Subsite filter
		// needs to be set for every loop as Subsitefilter is enabled somewhere unknown in the script
		Subsite::disable_subsite_filter(false);
		$succed = True;
		return $succed;
	}
	
	/**
	 * Import Routine for Restaurant Roche Rotkreuz
	 * Fixes import of new delegate price structur implemented from v 10.00
	 * which moves all ProctPrices to separte <ProductInfo><Product> structure
	 */
	public function import_roche_rotkreuz_productinfo($taskconfig) {
		
		Debug::message("ImportXMLRocheRotkreuz_ProductInfo()");
		
		// Array that defines mapping of nutritives from xml file to Dish variables
		// array( [XMLnutritiveName] => [DishNutritiveVariableName], ...)
		$nutritives_map = array();
		$NutritivesMapConfig = $taskconfig->NutritivesMapConfig();
		if($NutritivesMapConfig && $NutritivesMapConfig->exists()) {
			$NutritivesMapObjects = $NutritivesMapConfig->NutritivesMapObjects();
			if($NutritivesMapObjects && $NutritivesMapObjects->exists()) {
				$nutritives_map = $NutritivesMapObjects->map('ExternalKey','InternalKey')->toArray();
			}
		}
		Debug::message($nutritives_map);
		
		// Array that defines a blacklist of FoodCategories
		// which are not imported
		$foodcategory_blacklist = array();
		$FoodCategoryBlacklistConfig = $taskconfig->FoodCategoryBlacklistConfig();
		if($FoodCategoryBlacklistConfig && $FoodCategoryBlacklistConfig->exists()) {
			$FoodCategoryBlacklistObjects = $FoodCategoryBlacklistConfig->FoodCategoryBlacklistObjects();
			if($FoodCategoryBlacklistObjects && $FoodCategoryBlacklistObjects->exists()) {
				$foodcategory_blacklist = $FoodCategoryBlacklistObjects->map()->toArray();
			}
		}
		Debug::message($foodcategory_blacklist);
		
		// set to stage mode to create all data in Stage and then publish to Live
		$currStage = Versioned::current_stage();
		Versioned::set_reading_mode(''); // set to stage
		
		
		// collect necessary data
		$menuContainerID = $taskconfig->MenuContainerID;
		$menuContainer = MenuContainer::get()->byID($menuContainerID);
		$restaurantPageID = $menuContainer->parent->ID;
		$subsiteID = $menuContainer->SubsiteID;
		
		
		Debug::message("MenuContainer (".$menuContainerID."): ".$menuContainer->Title." - SubsiteID: ".$menuContainer->SubsiteID);
		
		// get SimpleXMLElement of file
		$xml = $this->readXMLFileFromURL($taskconfig->URL);
		// abort is error loading file
		if(!$xml) return false;
		
		// Disable Subsite filter
		// needs to be set for every loop as Subsitefilter is enabled somewhere unknown in the script
		Subsite::disable_subsite_filter(true);
		
		foreach ($xml->WeekDays->children() as $xmlDailyMenuPage) {
			// save IDs of all dishes of the current dailymenupage
			$dailyMenuPageDishes = array();
			// create sort order for Dishes of a DailyMenu page (order as in xml import)
			$dishSortOrder = 0;
			
			// DAILYMENUPAGE
			// get weekday date
			$DailyMenuPageDate = date('Y-m-d', strtotime($xmlDailyMenuPage["Date"]));
			Debug::message("----------IMPORTIERE TAG: ".$DailyMenuPageDate." - menuContainerID: ".$menuContainerID." - subsiteID: ".$subsiteID);
			
			// create uniqueFilter for DailyMenuPage
			$uniqueFilter = array(
				'Date'		=> $DailyMenuPageDate,
				'ParentID'	=> $menuContainerID,
				'SubsiteID'	=> $subsiteID
			);
			$dailyMenuPageData = $uniqueFilter;

			// get or create DailyMenuPage for given date
			$DailyMenuPage = $this->createOrGetDailyMenuPage(
				$uniqueFilter,					// array with fields and values that are used as filter to get an existing DataObject
				$dailyMenuPageData,				// array with fields and values that are assigned to the DataObject
				$removeAssignedDishes = true	// if true all assigned Dishes are removed from an existing DailyMenuPage
			);
			
			// FOODCATEGORY
			foreach($xmlDailyMenuPage->MenuLine->SetMenu as $xmlFoodCategory) {
				
				$foodCategoryTitle = Convert::raw2sql($xmlFoodCategory["Name"]);
				
				// only import if FoodCategory is not in blacklist
				if(!in_array($foodCategoryTitle, $foodcategory_blacklist)) {
				
					// generate array with FoodCategoryData data
					// key of Array needs to map the attribute of FoodCategory object 
					// (e.g. $data['Title_en_US'] will be written to FoodCategory->Title_en_US
					$foodCategoryData = array();

					Debug::message("----------<br />IMPORTING FOODCATEGORY: ".Convert::raw2sql($xmlFoodCategory["Name"]));

					// find english translation
					foreach($xmlFoodCategory->SetMenuTranslation as $xmlFoodcategoryTranslation) {
						if($xmlFoodcategoryTranslation["lang"] == 'en-gb') {
							Debug::message("ENGLISH TRANSLATION FOODCATEGORY : ".Convert::raw2sql($xmlFoodcategoryTranslation["Name"]));
							$foodCategoryData['Title_en_US'] = Convert::raw2sql($xmlFoodcategoryTranslation["Name"]);
							break;
						}
					}

					// MAP FoodCategory Titles
					$foodCategoryTitleLower = strtolower($foodCategoryTitle);

						// map FoodCategories beginning with "suppe" to "suppe" e.g. "suppe 1" => "suppe"
						if(substr($foodCategoryTitleLower, 0, 5) == "suppe") {
							$foodCategoryTitle = "suppe";
							if(array_key_exists('Title_en_US', $foodCategoryData)) $foodCategoryData['Title_en_US'] = "soup";
						}


					// create uniqueFilter for FoodCategory
					$uniqueFilter = array(
						'Title'				=> $foodCategoryTitle,
						'RestaurantPageID'	=> $restaurantPageID
					);
					// get or create FoodCategory for title
					$FoodCategory = $this->createOrGetFoodCategory(
						$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
						$foodCategoryData	// array with fields and values that are assigned to the DataObject
					);

					Debug::message("FOODCATEGORY DATA:");
					
					// DISHES
					foreach($xmlFoodCategory->SetMenuDetails as $xmlDish) {

						// generate array with dish data
						// key of Array needs to map the attribute of Dish object 
						// (e.g. $data['Title_en_US'] will be written to Dish->Title_en_US
						$dishData = array();
						$restaurantDishData = array();

						// generate arrays for storing connections to Additives, Allergens, DishLabels and SpecialLabels
						// we store the IDs of each DataObject in the array to assign tem to the Dish after creating it
						$dishAdditives = new ArrayList();
						$dishAllergens = new ArrayList();
						$dishDishLabels = new ArrayList();
						$dishSpecialLabels = new ArrayList();

						$dishData['Description'] = $this->convert_xml_to_string($xmlDish->GastDesc["value"]);

						Debug::message("----------IMPORTING DISH: ".$dishData['Description']);

						// Add FoodCategory ID to RestaurantDish data
						$restaurantDishData['FoodCategoryID'] = $FoodCategory->ID;

						// find english translation
						/**
						 * TEMPORARY DISABLED
						 * english translation for GastDesc in SetMenuDetails is not exportet in xml file
						 * so we use the english descriptions of all components instead 
						 */
						/*
						foreach($xmlDish->GastDesc->GastDescTranslation as $dishTranslation) {
							Debug::message("... looking for english translation of Dish");
							if($dishTranslation["lang"] == 'en-gb') {
								Debug::message("ENGLISH TRANSLATION: ".Convert::raw2sql($dishTranslation["value"]));
								$dishData['Description_en_US'] = $importController->convert_xml_to_string($dishTranslation["value"]);
								break;
							}
						}
						*/
						$dishDescription_en_USArray = array();
						foreach($xmlFoodCategory->Component as $xmlDishComponent) {
							foreach($xmlDishComponent->ComponentDetails->GastDesc->GastDescTranslation as $dishTranslation) {
								if($dishTranslation["lang"] == 'en-gb') {
									Debug::message("ENGLISH TRANSLATION DISH : ".Convert::raw2sql($dishTranslation["value"]));
									// remove line breaks and spaces from end of string
									$dishDescription_en_USArray[] = rtrim($this->convert_xml_to_string($dishTranslation["value"]));
									break;
								}
							}
						}
						if(count($dishDescription_en_USArray)) $dishData['Description_en_US'] = implode("\r\n", $dishDescription_en_USArray);

						// SPECIAL TRIGGER

						// PREFIX "INF": DailyMenuPage InfoText
						// if Description is prefixed with "INF" -> do not import as Dish but add a hliday info to DailyMenuPage
						if(substr($dishData['Description'], 0, 3) == "INF") {
							$DailyMenuPage->InfoText = $this->convert_xml_to_string(str_replace("INF ", "", $dishData['Description']));
							if(array_key_exists('Description_en_US', $dishData)) $DailyMenuPage->InfoText_en_US = $this->convert_xml_to_string(substr(str_replace("INF ", "", $dishData['Description_en_US'])));
							$DailyMenuPage->write();
							$DailyMenuPage->publish('Stage', 'Live'); // publish from Stage to Live

							Debug::message("... SPECIAL TRIGGER 'INF' FOUND: Adding Infotext: ".$DailyMenuPage->InfoText);

							// abort processing of current dish
							break;
						}
						// PREFIX "AKTION": SpecialLabel
						// if description is prefixed with "AKTION" 
						// -> get the following string on the same line and import it as Title for SpecialLabel
						if(substr($dishData['Description'], 0, 6) == "AKTION") {

							$specialLabelData = array();
							$tmpDishDescription = '';
							$tmpDishDescription_en_US = '';
							$linecount = 0;

							// extract german SpecialLabel data
							foreach(preg_split("/((\r?\n)|(\n?\r))/", $dishData['Description']) as $line){
								// extract title for SpecialLabel from first line
								if($linecount == 0) {
									$specialLabelTitle = str_replace("AKTION ", "", $line);
								}
								// add all following lines as Dish.Description
								else {
									if($linecount > 1) $tmpDishDescription .= "\r\n";
									$tmpDishDescription .= $line;
								}
								$linecount++;
							}
							// assign Dish Description
							$dishData['Description'] = $this->convert_xml_to_string($tmpDishDescription);

						Debug::message("... SPECIAL TRIGGER 'AKTION' FOUND: Adding SpecialLabel: ".$specialLabelTitle." and Description: $tmpDishDescription - ".$dishData['Description_en_US']);						

							// extract english SpecialLabel data
							if(array_key_exists('Description_en_US', $dishData) && substr($dishData['Description_en_US'], 0, 6) == "AKTION") {
								foreach(preg_split("/((\r?\n)|(\n?\r))/", $dishData['Description_en_US']) as $line){
									// extract title for SpecialLabel from first line
									if($linecount == 0) {
										$specialLabelData['Title_en_US'] = str_replace("AKTION ", "", $line);
									}
									// add all following lines as Dish.Description_en_US
									else {
										if($linecount > 1) $tmpDishDescription_en_US .= "\r\n";
										$tmpDishDescription_en_US .= $line;
									}
									$linecount++;
								}
								// assign Dish Description_en_US
								$dishData['Description_en_US'] = $tmpDishDescription;

								Debug::message("... found Aktion english description: ".$specialLabelData['Title_en_US']." - ".$dishData['Description_en_US']);	
							}

							// SPECIAL LABEL
							// create uniqueFilter for SpecialLabel
							// by adding the SubsiteID we define that we want to create a SubsiteSpecialLabel
							$uniqueFilter = array(
								'Title'		=> $specialLabelTitle,
								'SubsiteID' => $subsiteID
							);
							// get or create GlobalSpecialLabel for Title
							$SpecialLabel = $this->createOrGetSpecialLabel(
								$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
								$specialLabelData	// array with fields and values that are assigned to the DataObject
							);
							// assign SpecialLabel ID
							if($SpecialLabel) {
								$dishSpecialLabels->push($SpecialLabel);
							}
						}
						
						// get Import ID
						// :TODO: Check if IdentNr of SetMenu is really a unique identifier for the menu
						$dishImportID = Convert::raw2sql($xmlFoodCategory['IdentNr']);

						// get Prices (stored in <ProductInfo><Product>) 
						// we assume the first product is always the "main" product with the correct prices
						// -> Mapping of "name" of the <SetMenu> and <Product> does not work in all cases
						$xmlDishProduct = $xmlDish->ProductInfo->Product[0];
						$restaurantDishData['Price'] = Convert::raw2sql($xmlDishProduct['ProductPrice']);
						$restaurantDishData['PriceExtern'] = Convert::raw2sql($xmlDishProduct['ProductPrice3']);
						$restaurantDishData['PriceExternSmall'] = Convert::raw2sql($xmlDishProduct['ProductPrice6']);

						// get Nutritives
						foreach($xmlDish->NutritionInfo->Nutrient as $xmlNutrient) {
							Debug::message("----------IMPORTING NUTRITIVE: ".$xmlNutrient["name"]);
							$xmlNutrientName = Convert::raw2sql($xmlNutrient["name"]);
							if(array_key_exists($xmlNutrientName, $nutritives_map)) {
								Debug::message("... Nutrient ".$xmlNutrientName." is mapped to: ".$nutritives_map[$xmlNutrientName]." – value: ".round($xmlNutrient["value"]));
								$dishData[$nutritives_map[$xmlNutrientName]] = round(Convert::raw2sql($xmlNutrient["value"]));
							}
						}
						
						// get all Additives, Allergens and DishLabels
						foreach($xmlDish->FoodLabelInfo->FoodLabelGroup as $xmlFoodLabelGroup) {

							// ALLERGENS
							if($xmlFoodLabelGroup["name"] == "Allergene" && ($xmlFoodLabelGroup->Allergens != null && $xmlFoodLabelGroup->Allergens->FoodLabel != null)) {

								foreach($xmlFoodLabelGroup->Allergens->FoodLabel as $xmlAllergen) {
									$allergenData = array();
									$allergenNumber = strtoupper(Convert::raw2sql($xmlAllergen["code"]));
									$allergenData["Title"] = Convert::raw2sql($xmlAllergen["name"]);								

									Debug::message("IMPORTING ALLERGEN: ".$allergenData["Title"]);

									// find english translation
									foreach($xmlAllergen->FoodLabelTranslation as $allergenTranslation) {
										if($allergenTranslation["lang"] == 'en-gb') {
											Debug::message("ENGLISH TRANSLATION ALLERGEN : ".Convert::raw2sql($allergenTranslation["name"]));
											$allergenData['Title_en_US'] = Convert::raw2sql($allergenTranslation["name"]);
											break;
										}
									}

									// create uniqueFilter for Allergen
									// by adding the SubsiteID we define that we want to create a SubsiteAllergen
									$uniqueFilter = array(
										'Number' => $allergenNumber,
										'SubsiteID' => $subsiteID
									);
									// get or create GlobalAllergen for Number
									$Allergen = $this->createOrGetAllergen(
										$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
										$allergenData		// array with fields and values that are assigned to the DataObject
									);
									// assign Allergen ID
									if($Allergen) {
										$dishAllergens->push($Allergen);
									}

//									Debug::message($allergenData);

								}
							}

							// DISHLABELS and INFOTEXT
							else if($xmlFoodLabelGroup["name"] == "Anzeige Visuals" && ($xmlFoodLabelGroup->Information != null && $xmlFoodLabelGroup->Information->FoodLabel != null)) {

								foreach($xmlFoodLabelGroup->Information->FoodLabel as $xmlDishLabel) {
									$dishLabelData = array();
									$dishLabelTitle = Convert::raw2sql($xmlDishLabel["name"]);

									Debug::message("IMPORTING DISHLABEL: ".$dishLabelTitle);

									// find english translation
									foreach($xmlDishLabel->FoodLabelTranslation as $dishLabelTranslation) {
										if($dishLabelTranslation["lang"] == 'en-gb') {
											Debug::message("ENGLISH TRANSLATION DISHLABEL : ".Convert::raw2sql($dishLabelTranslation["name"]));
											$dishLabelData['Title_en_US'] = Convert::raw2sql($dishLabelTranslation["name"]);
											break;
										}
									}

									// SPECIAL CASE
									// Herkunftsland
									if(substr($dishLabelTitle, 0, 8) == "Herkunft") {

										// Add "Herkunftsland" to Dish.InfoText
										$dishData["Infotext"] = str_replace("Herkunft ", "", $dishLabelTitle); // remove "Herkunft " from beginning of string
										if(array_key_exists('Title_en_US', $dishLabelData)) $dishData["Infotext_en_US"] = str_replace("Origin ", "", $dishLabelData["Title_en_US"]); // remove "Origin " from beginning of string

										Debug::message("DISHLABEL IS HERKUNFTSLAND: ".$dishData["InfoText"]." - ".$dishData["InfoText_en_US"]);

									}
									// DEFAULT
									// get or create DishLabel
									else {
										// create uniqueFilter for DishLabel
										// by adding the SubsiteID we define that we want to create a SubsiteDishLabel
										$uniqueFilter = array(
											'Title' => $dishLabelTitle,
											'SubsiteID' => $subsiteID
										);
										// get or create GlobalDishLabel for Title
										$DishLabel = $this->createOrGetDishLabel(
											$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
											$dishLabelData		// array with fields and values that are assigned to the DataObject
										);
										if($DishLabel) {
											$dishDishLabels->push($DishLabel);
										}
									}
								}
							}

							// ADDITIVES
							else if($xmlFoodLabelGroup->Additives != null) {

								$additiveData = array();
								$additiveTitle = Convert::raw2sql($xmlFoodLabelGroup["name"]);

								Debug::message("IMPORTING ADDITIVE - Additives exist inside: ".$additiveTitle);

								// find english translation
								foreach($xmlFoodLabelGroup->FoodLabelGroupTranslation as $additiveTranslation) {
									if($additiveTranslation["lang"] == 'en-gb') {
										Debug::message("ENGLISH TRANSLATION ADDITIVE : ".Convert::raw2sql($additiveTranslation["name"]));
										$additiveData['Title_en_US'] = Convert::raw2sql($additiveTranslation["name"]);
										break;
									}
								}

								// create uniqueFilter for Additive
								// by adding the SubsiteID we define that we want to create a SubsiteAdditive
								$uniqueFilter = array(
									'Title'		=> $additiveTitle,
									'SubsiteID' => $subsiteID
								);
								// get or create GlobalAdditive for title
								$Additive = $this->createOrGetAdditive(
									$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
									$additiveData		// array with fields and values that are assigned to the DataObject
								);
								if($Additive) {
									$dishAdditives->push($Additive);
								}
							}
						}
						

//						Debug::message('IMPORTED DISH DATA');
//						Debug::message($dishData);
//						Debug::message($restaurantDishData);
//						Debug::message('ASSIGEND ADDITIVES');
//						Debug::message($dishAdditives);
//						Debug::message('ASSIGEND ALLERGENS');
//						Debug::message($dishAllergens);
//						Debug::message('ASSIGEND DISH LABELS');
//						Debug::message($dishDishLabels);
//						Debug::message('ASSIGEND SPECIAL LABELS');
//						Debug::message($dishSpecialLabels);

						
						// create uniqueFilter for Dish
						$uniqueFilter = array(
							'Description' => $dishData['Description'],
							#'RestaurantDish.FoodCategoryID' => $restaurantDishData['FoodCategoryID'], // add FoodCategory ID to unique query, so that a new dish is created if the same Dish Description is assigned to different FoodCategories
							'SubsiteID' => $subsiteID
						);
						// get or create Dish for title
						$Dish = $this->createOrGetDish(
							$uniqueFilter,	// array with fields and values that are used as filter to get an existing DataObject
							$dishData		// array with fields and values that are assigned to the DataObject
						);
						// RESTAURANTDISH
						if($Dish) {
							// create uniqueFilter for RestaurantDish
							$uniqueFilter = array(
								'DishID' => $Dish->ID,
								'RestaurantPageID' => $restaurantPageID
							);
							// get or create RestaurantDish for DishID
							$RestaurantDish = $this->createOrGetRestaurantDish(
								$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
								$restaurantDishData	// array with fields and values that are assigned to the DataObject
							);
							// assign Dish to DailyMenuPage
							$this->addDishToDailyMenuPage($Dish->ID, $DailyMenuPage->ID, $dishSortOrder);
							
							// ASSIGN MANY_MANY RELATIONS

							// assign Additives
							foreach($dishAdditives as $additive) {
								$this->addManyManyToDataObject($Dish, $additive);
							}
							// remove all but the imported Additives
							$this->removeManyManyFromDataObject($Dish, 'SubsiteAdditive', array_keys($dishAdditives->map()));
							
							// assign Allergens
							foreach($dishAllergens as $allergen) {
								$this->addManyManyToDataObject($Dish, $allergen);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteAllergen', array_keys($dishAllergens->map()));
							
							// assign DishLabels
							foreach($dishDishLabels as $dishlabel) {
								$this->addManyManyToDataObject($Dish, $dishlabel);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteDishLabel', array_keys($dishDishLabels->map()));
							
							// assign SpecialLabels
							foreach($dishSpecialLabels as $speciallabel) {
								$this->addManyManyToDataObject($Dish, $speciallabel);
							}
							// remove all but the imported Allergens
							$this->removeManyManyFromDataObject($Dish, 'SubsiteSpecialLabel', array_keys($dishSpecialLabels->map()));
							
						}

						// count up dish sort order (per DailyMenuPage)
						$dishSortOrder++;

					}
				}
			}	
		}
		// Enable Subsite filter
		// needs to be set for every loop as Subsitefilter is enabled somewhere unknown in the script
		Subsite::disable_subsite_filter(false);
		$succed = True;
		return $succed;
	}	
	/**
	 * Function returns DailyMenuPage for given $uniqueFilter
	 * If no DailyMenuPage exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or acombination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need Date, ParentID and SubsiteID.
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * @param Boolean $removeAssignedDishes If true all Dishes that are assigned to the DailyMenuPage are removed (not deleted)
	 * 
	 * @return DailyMenuPage  The existing or newly created DailyMenuPage
	 */
	public function createOrGetDailyMenuPage($uniqueFilter = null, $data = null, $removeAssignedDishes = false) {
		
			Debug::message("CreateOrGetDailyMenuPage()");
		
		if(!$uniqueFilter) return false;

		// Get DailyMenuPage for given date
		$DailyMenuPage = DailyMenuPage::get()->filter($uniqueFilter)->limit(1)->first();
		
		Debug::message("...DailyMenuPage::get()");

		// Create DailyMenuPage if it does not exist
		if (!$DailyMenuPage) {
			
			Debug::message("...Creating new DailyMenuPage");
			
			$DailyMenuPage = DailyMenuPage::create();
			// add all initial variables from $uniqueFilter array
			$DailyMenuPage = $this->assign_data_to_dataobject($DailyMenuPage, $uniqueFilter);

			// save and publish DailyMenuPage
			$DailyMenuPage->write(); // write to create ID entry in DB
			$DailyMenuPage->write(); // write second time to populate fields with onBeforeWrite() function
			$DailyMenuPage->publish('Stage', 'Live'); // publish from Stage to Live
		}
		
		// Remove assigned Dishes from DaiylMenuPage
		if($removeAssignedDishes) {
			Debug::message("...Remove assigned Dishes");
			$this->removeDishesFromDailyMenuPage($DailyMenuPage->ID);
		}
		Debug::message("...Return DailyMenuPage: ".$DailyMenuPage->Date);
		return $DailyMenuPage;
	}
		
	/**
	 * Removes all Dishes from DailyMenuPage for given ID 
	 * while keeping the Dishes that are provided with ID via $keepDishIDs array
	 * 
	 * @param Integer $dailyMenuPageID The ID of the DailyMenuPage
	 * @param Array $keepDishIDs Array with IDs of the Dishes that should not be removed (optional) $keepDishIDs = array(ID => Title, ...)
	 */
	public function removeDishesFromDailyMenuPage($dailyMenuPageID = null, $keepDishIDs = array()) {
		
		if (!$dailyMenuPageID) return false;
		
		$dishDBQueryAdd = null;
		if (count($keepDishIDs) >= 1) {
			$dishDBQueryAdd = "AND DishID NOT IN (" . implode(',', $keepDishIDs) . ")";
		}
		DB::query("
			DELETE FROM DailyMenuPage_Dishes
			WHERE DailyMenuPageID = $dailyMenuPageID
			$dishDBQueryAdd
		");
	}	
	
	/**
	 * Function sets all SideDish database fields of RestaurantDish to null
	 * 
	 * @param Integer $restaurantDishID ID of the RestaurantDish from whoch all SideDish entries are removed (by setting them to NULL)
	 * 
	 * @return Boolean true if success, else false
	 */
	public function removeSideDishesFromRestaurantDish($restaurantDishID = null) {
		
		if (!$restaurantDishID) return false;
		
		// RESTAURANTDISH (RestaurantPage specific)
		$RestaurantDish = RestaurantDish::get()->filter('ID', $restaurantDishID)->limit(1)->first();

		if ($RestaurantDish) {
			for($sideDishNr = 1; $sideDishNr <= 5; $sideDishNr++) {
				$RestaurantDish->{'SideDish'.$sideDishNr.'ID'} = null;
			}
			// save and publish
			$RestaurantDish->write();
		}
		else {
			return false;
		}
	}
	
	/**
	 * Function assigns a Dish object as SideDish to a RestaurantDish
	 * The next unset SideDish field will be used to assign the Dish.
	 * e.g. RestaurantDish->SideDish1ID is already set > Dish will be assigned to RestaurantDish->SideDish2ID
	 * 
	 * @param Integer $restaurantDishID ID of the RestaurantDish from whoch all SideDish entries are removed (by setting them to NULL)
	 * @param Integer $dishID ID of the Dish object that will be assigned as SideDish
	 * 
	 * @return Boolean true if success, else false
	 */
	public function addSideDishToRestaurantDish($restaurantDishID = null, $dishID = null) {
		
		if (!$restaurantDishID || !$dishID) return false;
		
		// RESTAURANTDISH (RestaurantPage specific)
		$RestaurantDish = RestaurantDish::get()->filter('ID', $restaurantDishID)->limit(1)->first();

		if ($RestaurantDish) {
			for($sideDishNr = 1; $sideDishNr <= 5; $sideDishNr++) {
				
				// Assign Dish to next empty SideDish database field
				if($RestaurantDish->{'SideDish'.$sideDishNr.'ID'} == 0) {
					$RestaurantDish->{'SideDish'.$sideDishNr.'ID'} = $dishID;
					// save and publish
					$RestaurantDish->write();
					return true;
				}
			}
		}
		
		// in all other cases we were not able to assign the Dish as Side Dish
		return false;
	}
}
