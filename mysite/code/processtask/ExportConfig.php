<?php

/**
 * TODO
 * - implement AllowApiAccess with seperate URL Controller (e.g. domain.com/tasks/exportmenuhtml/api/XXXXX)
 */
class ExportConfig extends ProcessTaskObject  {
	
	private static $db = array (
		'Name' => 'Varchar(255)',
		'Path' => 'Varchar(255)',
		'AllowApiAccess' => 'Boolean',
		'ApiPublicKey' => 'Varchar(255)'
    );
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv',
		
		'Name' => 'Dateiname',
		'Path' => 'Dateipfad',
		'Filename' => 'Datei',
		'AllowApiAccess' => 'API Zugriff',
		'AllowApiAccess.Nice' => 'API Zugriff',
		'ApiPublicKey' => 'API Key'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'Title',
		'Filename',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'AllowApiAccess.Nice',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'Title',
		'Name',
		'Path',
		'AllowApiAccess',
		'ApiPublicKey',
		'LastStarted',
		'LastRun'
	);
	
	/**
	 * File extension for the export file
	 * @var string
	 */
	public static $file_extension = 'xml';
	
    public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->addFieldToTab(
			'Root.Main',
			TextField::create(
				'Name',
				'Dateiname'
			)
				->setDescription('ohne Dateiendung'),
			'Schedule'
		);
		$f->addFieldToTab(
			'Root.Main',
			TextField::create(
				'Path',
				'Dateipfad'
			)
				->setDescription('Absoluter Dateipfad ab DSS root Verzeichnis (z.B. assets/xml-export/)'), 
			'Schedule'
		);
		if ($this->Name && $this->Path) {
			$f->addFieldToTab(
				'Root.Main',
				FieldGroup::create(
					'Link',
					LiteralField::create(
						'Link',
						// get the URL the Member is currently viewing (Director::absoluteBaseURL())
						// instead of the URL of the Subsite that the *Config belongs to
						// to avoid a new login to the different URL when downloading the file
						// (Logins are URL based - as they are stored in a Session Cookie per URL)
						'<a href="'.Director::absoluteBaseURL().$this->Path.$this->Name.".".$this->file_extension().'"target="_blank">'.Director::absoluteBaseURL().$this->Path.$this->Name.".".$this->file_extension().'</a>'
					)
				)
					->setTitle('Download Exportdatei'),
				'Schedule'
			);
		}
		
		$f->addFieldToTab(
			'Root.Main',
			HeaderField::create(
				'ApiHeader',
				'API'
			),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			FieldGroup::create(
				'AllowApiAccessGroup',
				CheckboxField::create(
					'AllowApiAccess',
					'ja'
				)
			)
				->setTitle('API Zugriff aktivieren'),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			ReadonlyField::create(
				'ApiPublicKey',
				'API Key'
			)
				->setDescription('wird automatisch generiert'), 
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			HeaderField::create(
				'StatusHeader',
				'Status'
			),
			'LastStarted'
		);
		
		return $f;

    }
	
	/**
	 * Returns the combined $Path, $Name and $file_extension if set, otherwise false
	 * 
	 * @return string
	 */
	public function Filename() {
		$pathandname = $this->Path;
		if($this->Name) $pathandname .= $this->Name;
		return (strlen($pathandname) > 0) ? $pathandname.'.'.$this->file_extension() : false;
	}
	
	/**
	 * Returns the value for $file_extension
	 * As set in static variable.
	 * 
	 * @return string
	 */
	public function file_extension() {
		return static::$file_extension;
	}
	
	/**
	 * Returns start date for preview if it is set via URL
	 * e.g. mydomain.com/mytemplate?showpreview=2013-11-11 
	 *
	 * @return String
	 */
	public function getPreviewStartDate() {
		return (isset($_GET['showpreview'])) ? Convert::raw2sql($_GET['showpreview']) : false;
	}
	
	/**
	 * Returns the start date for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeStartDate($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		return DateUtil::dateFormat($dateRange['StartDate'], $format);
	}

	/**
	 * Returns the end date for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeEndDate($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		return DateUtil::dateFormat($dateRange['EndDate'], $format);
	}

	/**
	 * Returns the end of the week date (Friday) for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeEndDateWeek($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		$endOfWeek = $dateRange['EndDate'];
		while(date('l', strtotime($endOfWeek)) != 'Friday' && $endOfWeek >= $dateRange['StartDate']) {
			$endOfWeek = date('Y-m-d', strtotime($endOfWeek." -1 day"));
		}
		return $endOfWeek >= $dateRange['StartDate'] ? DateUtil::dateFormat($endOfWeek, $format) : false;
	}
	
	/**
	 * Assign random unique value to API key 
	 * We do this in onAfterWrite because we need the ID of the current DataObject
	 * This way we know the key is unique
	 */
	public function onAfterWrite() {
		
		parent::onAfterWrite();
		
		if(!isset($this->ApiPublicKey) || $this->ApiPublicKey == NULL) {
			$this->ApiPublicKey = bin2hex(openssl_random_pseudo_bytes(10)).md5($this->ID);
			$this->write();
		}
	}
}

