<?php

/**
 * Export data of selected DisplayTemplatePage as XML file
 * 
 * TODO
 * - Add node with VisitorCounterConfig
 */
class ExportDisplayMenuXmlTask extends ExportXmlTask {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ExportDisplayMenuXmlConfig';
	
	private static $url_handlers = array(
		'task/$ApiKey' => 'task'
    );
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
		$exportFunction = $taskconfig->ExportFunction;
		if(method_exists($this, $exportFunction)) return $this->$exportFunction($taskconfig);
	}
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		$result = Permission::check("RUN_ALL_EXPORTDISPLAYTASK");
		return  $result;
	}
	
	/**
	 * Provide permissions for current member to run the task "task()"
	 * 1. Member needs "RUN_SINGLE_EXPORTMENUHTMLTASK" permission
	 * 2. Member needs permission to edit the RestaurantPage that the assigned MenuContainer belongs to
	 *  
	 * @return Boolean
	 */
	public function can_run_single_task() {
		$result = false;
		
		// $this->RestaurantPage() only exists if we run a single task by providing the ApiKey via URL
		// (e.g. mydomain.com/tasks/exportmenuhtmltask/task/123456789)
		// it will call ExportTask::setTaskConfigID() which will try to get the ApiKey
		// from the current $request
		// Member::currentUser() only exists if we call the task from the domain the Member has logged in
		$restaurantPage = $this->RestaurantPage();
		$member = Member::currentUser();
		if($restaurantPage && $restaurantPage->exists()) {
			if(
				$this->can_run_task() ||
				(Permission::check("RUN_SINGLE_EXPORTDISPLAYTASK") && $restaurantPage->canEdit($member))
			) {
				$result = true;
			}
		}
		return  $result;
	}
	
	public function export_default($taskconfig, $request = null) {
		
		// Doc creation
		$doc = $this->CreateDOMElementRoot();
		$root = $doc->documentElement;
		
		// Getting DisplayTemplatePage
		$DisplayTemplatePage = $taskconfig->DisplayTemplatePage();
		
		// create and append DOMElement <DisplaySetting> 
		$DisplaySetting = $doc->createElement('DisplaySetting');
		$root->appendChild($DisplaySetting);
		
		// create and add attributes for <DisplaySetting>
		$this->CreateAndSetDOMAttributeTo('ID', $DisplayTemplatePage->ID, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('Title', $DisplayTemplatePage->Title, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TemplateName', $DisplayTemplatePage->TemplateName, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('StartDateType', $DisplayTemplatePage->StartDateType, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('StartDate', $DisplayTemplatePage->StartDate, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('DatePeriod', $DisplayTemplatePage->DatePeriod, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('DateRange', $DisplayTemplatePage->DateRange, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('FootNote', $DisplayTemplatePage->FootNote, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TimerStartDate', $DisplayTemplatePage->TimerStartDate, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TimerStartTime', $DisplayTemplatePage->TimerStartTime, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TimerEndDate', $DisplayTemplatePage->TimerEndDate, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TimerEndTime', $DisplayTemplatePage->TimerEndTime, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('InfoMessagesOnly', $DisplayTemplatePage->InfoMessagesOnly, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('ScreenRotation', $DisplayTemplatePage->ScreenRotation, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('ReloadInterval', $DisplayTemplatePage->ReloadInterval, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('ShowScreenDuration', $DisplayTemplatePage->ShowScreenDuration, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TransitionDuration', $DisplayTemplatePage->TransitionDuration, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('TransitionEffect', $DisplayTemplatePage->TransitionEffect, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('DisplayTemplatePage_WeeklyMenuTableTemplate', $DisplayTemplatePage->DisplayTemplatePage_WeeklyMenuTableTemplate, $doc, $DisplaySetting);
		$this->CreateAndSetDOMAttributeTo('DisplayVisitorCounterData', $DisplayTemplatePage->DisplayVisitorCounterData, $doc, $DisplaySetting);
		// :TODO: Add VisitorCounterConfig data
		
		// create and append TimerRecurringDaysOfWeek 
		$TimerRecurringDaysOfWeeks = $DisplayTemplatePage->TimerRecurringDaysOfWeek();
		$TimerRecurringDaysOfWeeksElement = $this->RenderDOMNodeTimerRecurringDaysOfWeek($doc, $TimerRecurringDaysOfWeeks);
		$DisplaySetting->appendChild($TimerRecurringDaysOfWeeksElement);
		
		// create and append InfoMessages
		$InfoMessages = $DisplayTemplatePage->InfoMessages();
		$InfoMessagesElement = $this->RenderDOMNodeInfoMessages($doc, $InfoMessages);
		$root->appendChild($InfoMessagesElement);
		
		// Getting DailyMenuPages
		$DailyMenuPages = $taskconfig->getDailyMenuPagesByDate();

		// create and append DOMElement <Weekdays>
		$WeekDays = $doc->createElement('WeekDays');
		$root->appendChild($WeekDays);
		
		// Getting DailyMenuPages
		$DailyMenuPages = $taskconfig->getDailyMenuPagesByDate();
		foreach ($DailyMenuPages as $DailyMenuPage) {
			
			// create and append DOMElement for DailyMenuPage
			$WeekDayElement = $this->CreateDOMElementDailyMenuPage($doc, $DailyMenuPage);
			$WeekDays->appendChild($WeekDayElement);
			
			// Create FoodCategories node for each day
			$FoodCategoriesElement = $doc->createElement('FoodCategories');
			
			// Appending Food Categories
			$FoodCategories = $DailyMenuPage->FoodCategories();
			foreach ($FoodCategories as $FoodCategorie) {

				// create DOMElement for FoodCategory
				$FoodCategory = $this->CreateDOMElementFoodCategory($doc, $FoodCategorie);
					
				// Create Dishes node for each FoodCategory
				$DishesElement = $doc->createElement('Dishes');
				
				// Appending Dishes
				$Dishs = $DailyMenuPage->DishesForCategory($FoodCategorie->ID);
				foreach ($Dishs as $Dish) {
					// create and append DOMElement <Dish>
					$DishElement = $this->RenderDOMNodeDish($doc, $Dish);
					$DishesElement->appendChild($DishElement);
				}
				// Append Dishes element to FoodCategory
				$FoodCategory->appendChild($DishesElement);
				
				// Append FoodCategory element to FoodCategories node
				$FoodCategoriesElement->appendChild($FoodCategory);
			}
				
			// Add Foodcategories element to current WeekDay
			$WeekDayElement->appendChild($FoodCategoriesElement);
		}
		$doc->saveXML();
		$baseFolder = Director::baseFolder();
		$exportFolder = $baseFolder . "/" . $taskconfig->Path;
		$exportPath = $exportFolder . $taskconfig->Name . "." . $taskconfig->file_extension();
		if (!file_exists($exportFolder)) {
			mkdir($exportFolder);
			chmod($exportFolder, 0777);
		}
		$doc->save($exportPath);
		// set correct permissions on file
		chmod($exportPath, 0777);
		return true;
	}
}
