<?php

class ExportDisplayMenuXmlConfig extends ExportConfig  implements PermissionProvider {

	public static $singular_name = "Export Bildschirm XML";

	public static $plural_name = "Export Bildschirme XML";
	
	private static $db = array (
		'ExportFunction' =>  "Enum('export_default','export_default')"
    );
	
	private static $has_one = array(
		'DisplayTemplatePage' => 'DisplayTemplatePage'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'Status' => 'Status',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv',
		
		'Name' => 'Dateiname',
		'Path' => 'Dateipfad',
		'Filename' => 'Datei',
		'AllowApiAccess' => 'API Zugriff',
		'AllowApiAccess.Nice' => 'API Zugriff',
		'ApiPublicKey' => 'API Key',
		
		'RestaurantPageTitle' => 'Restaurant',
		'DisplayTemplatePage.Title' => 'Bildschirm',
		'ExportFunction' =>  'Export Funktion'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'RestaurantPageTitle',
		'DisplayTemplatePage.Title',
		'Title',
		'Filename',
		'ExportFunction',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'AllowApiAccess.Nice',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'DisplayTemplatePage.Title',
		'ExportFunction',
		'Title',
		'Name',
		'Path',
		'AllowApiAccess',
		'ApiPublicKey',
		'LastStarted',
		'LastRun'
	);
	
    public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// Add Button to execute task
		if(
			$this->ID && $this->DisplayTemplatePageID && $this->ExportFunction && $this->Name && $this->Path &&
			($this->canRunAllTask() || $this->canRunSingleTask())
		){
			// get the URL the Member is currently viewing (Director::absoluteBaseURL())
			// instead of the URL of the Subsite that the *Config belongs to
			// to avoid a new login to the different URL when downloading the file
			// (Logins are URL based - as they are stored in a Session Cookie per URL)
			$absoluteBaseURL = Director::absoluteBaseURL();
			$f->addFieldToTab('Root.Main', LiteralField::create('RunTask', '<a href="'.$absoluteBaseURL."tasks/exportdisplaymenuxml/task/".$this->ApiPublicKey.'" target="_blank" class="custom-button icon-arrows-ccw ajax-executeSingleProcessTask">'.$this::$singular_name.' ausführen</a>'), 'GroupField');
		}
		
		$displayTemplatePages = DisplayTemplatePage::get();
		if($displayTemplatePages->count() >= 1) {
			$displayTemplatePagesMod = ArrayList::create();
			foreach($displayTemplatePages as $displayTemplatePage) {
				$restaurantPage = $displayTemplatePage->parent();
				$do = MenuContainer::create();
				$do->ID = $displayTemplatePage->ID;
				$do->Title = $restaurantPage->Title.' - '.$displayTemplatePage->Title;
				$displayTemplatePagesMod->push($do);
			}
			$f->addFieldToTab(
				'Root.Main',
				DropdownField::create(
					'DisplayTemplatePageID',
					'Bildschirm',
					$displayTemplatePagesMod->map('ID', 'Title')
				)
					->setEmptyString('Bitte auswählen')
					->setDescription('Alle Einstellungen des Bildschirms werden beim Export berücksichtigt (Essenskategorien, News, Zeitplan, etc.)'),
				'Schedule'
			);
		}
		else {
			$f->addFieldToTab(
				'Root.Main', 
				FieldGroup::create(
					'DisplayTemplatePage',
					LiteralField::create(
						'DisplayTemplatePageMessage',
						'Der Bildschirm kann in der Hauptsite nicht geändert werden')
				)
					->setTitle('Bildschirm'),
				'Schedule'
			);
		}
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'ExportFunction',
				'Export Funktion',
				$this->dbObject('ExportFunction')->enumValues()
			)
				->setEmptyString('Bitte auswählen'),
			'Schedule'
		);
		
		return $f;

    }
	
	/**
	 * Returns the RestaurantPage based on the MenuContainer relation
	 * Required for ExportTask::setRestaurantPageID() to work
	 * 
	 * @return RestaurantPage|boolean
	 */
	public function RestaurantPage() {
		if(!$this->DisplayTemplatePageID) return false;
		$displayTemplatePage = DisplayTemplatePage::get()->byID($this->DisplayTemplatePageID);
		return ($displayTemplatePage && $displayTemplatePage->exists()) ? $displayTemplatePage->parent() : false;
	}
	
	/**
	 * Returns the title of the RestaurantPage based on the MenuContainer relation
	 * 
	 * @return string|boolean
	 */
	public function RestaurantPageTitle() {
		$restaurantPage = $this->RestaurantPage();
		return ($restaurantPage && $restaurantPage->exists()) ? $restaurantPage->Title : false;
	}
	
	/**
	 * Returns the start and end date for the TemplatePage based on the settings in the cms
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return Array $dates Array with 'StartDate' and 'EndDate'
	 */
	public function getDateRangeAsArray($datePeriod = null, $dateRange = null) {
		$dates = array();

		// if user is logged in and urlparameter 'showpreview' ist set -> use this date as start date
		$startDate = $this->getPreviewStartDate();
		
		$displayTemplatePage = $this->DisplayTemplatePage();

		if(!$startDate) {
			// set start date - depending on CMS setting: a) current date b) fix start date
			$startDate = $displayTemplatePage->StartDateType == 'Aktuellem-Datum' ? DateUtil::currentDate() : $displayTemplatePage->StartDate;
		}

		// get DateRange and DatePeriod from variables, otherwise from current TemplatePage
		$datePeriod = $datePeriod ? $datePeriod : $displayTemplatePage->DatePeriod;
		$dateRange = $dateRange ? $dateRange : $displayTemplatePage->DateRange;

		// calculate start- and end date for a) days or b) weeks
		if($datePeriod == 'Tage') {
			$dateRange = DateUtil::dayDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = ($dateRange > 1) ? $dateRange['EndDate'] :  null;
		}
		else {
			$dateRange = DateUtil::weekDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = $dateRange['EndDate'];
		}
		return $dates;
	}

	/*
	 * Returns DailyMenuPages for the current restaurant that fall within
	 * a given date range, sorted by date.
	 *
	 * @return DataList
	 */
	public function getDailyMenuPagesByDate() {
		$dateRangeArray = $this->getDateRangeAsArray();
		$menuContainer = MenuContainer::get()->filter('ID' , $this->DisplayTemplatePage()->MenuContainerID)->first();
		$endDateCondition = "";
		if ($dateRangeArray['EndDate']) {
			$endDateCondition = " Date <= '".$dateRangeArray['EndDate']."'";
		}
		$dailyMenuPages = DailyMenuPage::get()->filter(array_merge(
			array('ParentID' => $menuContainer->ID),
			array('Date:GreaterThanOrEqual' => $dateRangeArray['StartDate']),
			$dateRangeArray['EndDate'] ? array('Date:LessThanOrEqual' => $dateRangeArray['EndDate']) : array()
		))->sort('Date');
		return $dailyMenuPages;
	}
	
	/*
	 * Returns FoodCategories for the current DisplayTemplatePage
	 *
	 * @return DataList
	 */
	public function getFoodCategories() {
		Subsite::disable_subsite_filter(true);
		$FoodCategories = $this->DisplayTemplatePage()->FoodCategories();
		Subsite::disable_subsite_filter(false);
		return $FoodCategories;
	}
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_EXPORTDISPLAYCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_EXPORTDISPLAYCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_EXPORTDISPLAYCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_EXPORTDISPLAYCONFIG', 'any', $member);
    }
	
	public function canRunAllTask($member = null) {
        return Permission::check('RUN_ALL_EXPORTDISPLAYTASK', 'any', $member);
    }
	public function canRunSingleTask($member = null) {
        return Permission::check('RUN_SINGLE_EXPORTDISPLAYTASK', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_EXPORTDISPLAYCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 410
			),
			"VIEW_EXPORTDISPLAYCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 420
			),
			"EDIT_EXPORTDISPLAYCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 430
			),
			"DELETE_EXPORTDISPLAYCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 440
			),
			"RUN_ALL_EXPORTDISPLAYTASK" => array(
				'name' => 'Kann alle Aufgaben "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 450
			),
			"RUN_SINGLE_EXPORTDISPLAYTASK" => array(
				'name' => 'Kann einzelne Aufgabe "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 460
			)
		);
	}
	
}

