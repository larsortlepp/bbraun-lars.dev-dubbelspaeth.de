<?php

class ImportConfig extends ProcessTaskObject  {

	
	private static $db = array (
        'URL' => 'Varchar(2083)',
    );
	
	private static $has_one = array(
		'NutritivesMapConfig' => 'NutritivesMapConfig',
		'FoodCategoryBlacklistConfig' => 'FoodCategoryBlacklistConfig',
		'AdditivesBlacklistConfig' => 'AdditivesBlacklistConfig',
		'AllergensBlacklistConfig' => 'AllergensBlacklistConfig',
		'DishLabelsBlacklistConfig' => 'DishLabelsBlacklistConfig',
		'SpecialLabelsBlacklistConfig' => 'SpecialLabelsBlacklistConfig'
	);
	
	private static $field_labels = array(
		'URL' => 'URL',
		'Title' => 'Titel',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'Title',
		'URL',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'Title',
		'URL',
		'LastStarted',
		'LastRun'
	);

    public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->addFieldToTab(
			'Root.Main', 
			TextField::create('URL')
				->setDescription('Absolute URL oder absoluter lokaler Dateipfad zu XML Import Datei'),
			'Schedule'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'FoodCategoryBlacklistConfigID',
				FoodCategoryBlacklistConfig::$plural_name,
				FoodCategoryBlacklistConfig::get()->map('ID', 'Title')
			)
				->setEmptyString('Bitte auswählen'),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'AdditivesBlacklistConfigID',
				AdditivesBlacklistConfig::$plural_name,
				AdditivesBlacklistConfig::get()->map('ID', 'Title')
			)
				->setEmptyString('Bitte auswählen'),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'AllergensBlacklistConfigID',
				AllergensBlacklistConfig::$plural_name,
				AllergensBlacklistConfig::get()->map('ID', 'Title')
			)
				->setEmptyString('Bitte auswählen'),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'DishLabelsBlacklistConfigID',
				DishLabelsBlacklistConfig::$plural_name,
				DishLabelsBlacklistConfig::get()->map('ID', 'Title')
			)
				->setEmptyString('Bitte auswählen'),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'SpecialLabelsBlacklistConfigID',
				SpecialLabelsBlacklistConfig::$plural_name,
				SpecialLabelsBlacklistConfig::get()->map('ID', 'Title')
			)
				->setEmptyString('Bitte auswählen'),
			'LastStarted'
		);
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'NutritivesMapConfigID',
				NutritivesMapConfig::$plural_name,
				NutritivesMapConfig::get()->map('ID', 'Title')
			)
			->setEmptyString('Bitte auswählen'),
			'LastStarted'
		);
		
		return $f;

    }
}

