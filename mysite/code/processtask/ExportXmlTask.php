<?php

/**
 * Class adds XML functionality to create DOMDocument structure,
 * including helper classes to create nodes and subnodes for often used
 * DataObjects (e.G. DailyMenuPage, FoodCategory, Dish, Allergen, etc.)
 */
class ExportXmlTask extends ExportTask {
	
	/**
	 * Will create a DOMAttribute with $name and $value inside the given
	 * DOMDocument $document and append it to DomElement $appendTo.
	 * $value will be encoded using htmlspecialchars().
	 * 
	 * @param String $name Name of the attribute
	 * @param String $value Value of the attribte. Will be escaped with htmlspecialchars()
	 * @param DOMDocument $document Instance of DOMDocument
	 * @param DOMElement $appendTo Reference to the DomElement that the DOMAttribute will be appended to.
	 */
	public function CreateAndSetDOMAttributeTo($name, $value, $document, &$appendTo) {
		$attribute = $document->createAttribute($name);
		$attribute->value = htmlspecialchars($value);
		$appendTo->appendChild($attribute);
	}
	
	/**
	 * Creates and returns a DOMDocument with utf-8 encoding and adds a
	 * root DOMElement with $RootElementName.
	 * We can access the root DOMElement in the returned DOMDocument with
	 * $doc->documentElement;
	 * 
	 * @param String $RootElementName The Name of the root DOMElement. Default: 'Root'
	 * @return \DOMDocument $doc The DOMDocument containing the root DOMElement
	 */
	public function CreateDOMElementRoot($rootElementName = 'Root') {
		// create DOMDocument with utf-8 encoding
		$document = new DOMDocument('1.0', 'utf-8');
		// we want a nice output
		$document->formatOutput = true;
		// create and append root element
		$root = $document->createElement($rootElementName);
		$document->appendChild($root);
		return $document;
	}
	
	/**
	 * Creates and returns a default DOMElement for class DailyMenuPage
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The DailyMenuPage, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'WeekDay'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementDailyMenuPage($document, $data, $nodeName = 'WeekDay') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Date', $data->Date, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Message', $data->Message, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Message_en_US', $data->Message_en_US, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class FoodCategory
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The FoodCategory, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'FoodCategory'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementFoodCategory($document, $data, $nodeName = 'FoodCategory') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title', $data->Title, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title_en_US', $data->Title_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Infotext', $data->Infotext, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Infotext_en_US', $data->Infotext_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Color', $data->Color, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('IconUnicodeCode', $data->IconUnicodeCode, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('IsMain', $data->IsMain, $document, $domElement);
		$ImageValue = ($data->Image()->ID != 0) ? $data->Image()->getAbsoluteURL() : "";
		$this->CreateAndSetDOMAttributeTo('Image', $ImageValue, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class Dish (including RestaurantDish data)
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The FoodCategory, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'Dish'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementDish($document, $data, $nodeName = 'Dish') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes for Dish to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ImportID', $data->ImportID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Description', $data->Description, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Description_en_US', $data->Description_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Infotext', $data->Infotext, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Infotext_en_US', $data->Infotext_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Calories', $data->Calories, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Kilojoule', $data->Kilojoule, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Fat', $data->Fat, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('FattyAcids', $data->FattyAcids, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Carbohydrates', $data->Carbohydrates, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Sugar', $data->Sugar, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Fibres', $data->Fibres, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Protein', $data->Protein, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Salt', $data->Salt, $document, $domElement);
		
		// create and add attributes for RestaurantDish to DOMElement
		$this->CreateAndSetDOMAttributeTo('Price', $data->getPrice(), $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('PriceExtern', $data->getPriceExtern(), $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('PriceSmall', $data->getPriceSmall(), $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('PriceExternSmall', $data->getPriceExternSmall(), $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ShowOnDisplayTemplatePage', $data->getShowOnDisplayTemplatePage(), $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ShowOnPrintTemplatePage', $data->getShowOnPrintTemplatePage(), $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ShowOnMobileTemplatePage', $data->getShowOnMobileTemplatePage(), $document, $domElement);
		
		// add image
		$ImageValue = ($data->Image()->ID != 0) ? $data->Image()->getAbsoluteURL() : "";
		$this->CreateAndSetDOMAttributeTo('Image', $ImageValue, $document, $domElement);
		
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class Additive
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The DailyMenuPage, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'Additive'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementAdditive($document, $data, $nodeName = 'Additive') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ClassName', $data->ClassName, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Number', $data->Number, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title', $data->Title, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title_en_US', $data->Title_en_US, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class Allergen
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The DailyMenuPage, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'Allergen'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementAllergen($document, $data, $nodeName = 'Allergen') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ClassName', $data->ClassName, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Number', $data->Number, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title', $data->Title, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title_en_US', $data->Title_en_US, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class DishLabel
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The DailyMenuPage, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'DishLabel'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementDishLabel($document, $data, $nodeName = 'DishLabel') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ClassName', $data->ClassName, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title', $data->Title, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title_en_US', $data->Title_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Color', $data->Color, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('IconUnicodeCode', $data->IconUnicodeCode, $document, $domElement);
		$ImageValue = ($data->Image()->ID != 0) ? $data->Image()->getAbsoluteURL() : "";
		$this->CreateAndSetDOMAttributeTo('Image', $ImageValue, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class SpecialLabel
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The DailyMenuPage, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'SpecialLabel'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementSpecialLabel($document, $data, $nodeName = 'SpecialLabel') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ClassName', $data->ClassName, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title', $data->Title, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title_en_US', $data->Title_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Color', $data->Color, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('IconUnicodeCode', $data->IconUnicodeCode, $document, $domElement);
		$ImageValue = ($data->Image()->ID != 0) ? $data->Image()->getAbsoluteURL() : "";
		$this->CreateAndSetDOMAttributeTo('Image', $ImageValue, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a nested DOMElement for class Dish
	 * with the given $nodeName and assigns data provided via $data.
	 * Renders Dish data RestaurantDish data and all Relations
	 * (SideDishes, Additives, Allergens, DishLabels, SpecialLabels)
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The Dish, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'Dish'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function RenderDOMNodeDish($document, $data, $nodeName = 'Dish') {
		// create DOMElement
		$domElement = $this->CreateDOMElementDish($document, $data, $nodeName);
		// Additives
		$Additives = $data->getAllAdditives();
		if ($Additives->count() != 0) {
			$AdditivesElement = $document->createElement('Additives');
			foreach ($Additives as $Additive) {
				// create and append DOMElement Additive
				$AdditiveElement = $this->CreateDOMElementAdditive($document, $Additive);
				$AdditivesElement->appendChild($AdditiveElement);
			}
			$domElement->appendChild($AdditivesElement);
		}

		// Allergens
		$Allergens = $data->getAllAllergens();
		if ($Allergens->count() != 0) {
			$AllergensElement = $document->createElement('Allergens');
			foreach ($Allergens as $Allergen) {
				// create and append DOMElement Allergen
				$AllergenElement = $this->CreateDOMElementAllergen($document, $Allergen);
				$AllergensElement->appendChild($AllergenElement);
			}
			$domElement->appendChild($AllergensElement);
		}
		
		// DishLabels
		$DishLabels = $data->getAllDishLabels();
		if ($DishLabels->count() != 0) {
			$DishLabelsElement = $document->createElement('DishLabels');
			foreach ($DishLabels as $DishLabel) {
				// create and append DOMElement DishLabel
				$DishLabelElement = $this->CreateDOMElementDishLabel($document, $DishLabel);
				$DishLabelsElement->appendChild($DishLabelElement);
			}
			$domElement->appendChild($DishLabelsElement);
		}
		
		// SpecialLabels
		$SpecialLabels = $data->getAllSpecialLabels();
		if ($SpecialLabels->count() != 0) {
			$SpecialLabelsElement = $document->createElement('SpecialLabels');
			foreach ($SpecialLabels as $SpecialLabel) {
				// create and append DOMElement SpecialLabel
				$SpecialLabelElement = $this->CreateDOMElementSpecialLabel($document, $SpecialLabel);
				$SpecialLabelsElement->appendChild($SpecialLabelElement);
			}
			$domElement->appendChild($SpecialLabelsElement);
		}
		
		// SideDishes
		$SideDishsElement = $document->createElement('SideDishes');
		if ($data->getSideDish(1)->ID !=0){
			$SideDishsElement->appendChild($this->RenderDOMNodeDish($document, $data->getSideDish(1), 'SideDish'));
		}
		if ($data->getSideDish(2)->ID !=0){
			$SideDishsElement->appendChild($this->RenderDOMNodeDish($document, $data->getSideDish(2), 'SideDish'));
		}
		if ($data->getSideDish(3)->ID !=0){
			$SideDishsElement->appendChild($this->RenderDOMNodeDish($document, $$data->getSideDish(3), 'SideDish'));
		}
		if ($data->getSideDish(4)->ID !=0){
			$SideDishsElement->appendChild($this->RenderDOMNodeDish($document, $data->getSideDish(4), 'SideDish'));
		}
		if ($data->getSideDish(5)->ID !=0){
			$SideDishsElement->appendChild($this->RenderDOMNodeDish($document, $data->getSideDish(5), 'SideDish'));
		}
		$domElement->appendChild($SideDishsElement);
		
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class RecurringDayOfWeek
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The RecurringDayOfWeek, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'TimerRecurringDaysOfWeek'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementTimerRecurringDaysOfWeek($document, $data, $nodeName = 'TimerRecurringDaysOfWeek') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('Value', $data->Value, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Skey', $data->Skey, $document, $domElement);
		return $domElement;
	}
	
	/**
	 * Creates and returns a nested DOMNode 'TimerRecurringDaysOfWeeks'
	 * with child nodes of class RecurringDayOfWeek
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataList|ArrayList $data List of RecurringDaysOfWeek DataObjects
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'TimerRecurringDaysOfWeeks'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function RenderDOMNodeTimerRecurringDaysOfWeek($document, $data, $nodeName = 'TimerRecurringDaysOfWeeks') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and append single DOMElements for each item
		foreach ($data as $dataobject) {
			$domElement->appendChild($this->CreateDOMElementTimerRecurringDaysOfWeek($document, $dataobject));
		}
		return $domElement;
	}
	
	/**
	 * Creates and returns a default DOMElement for class InfoMessage
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataObject $data The InfoMessage, used to get data that is assigned to DOMElement attributes
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'InfoMessage'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function CreateDOMElementInfoMessage($document, $data, $nodeName = 'InfoMessage') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and add attributes to DOMElement
		$this->CreateAndSetDOMAttributeTo('ID', $data->ID, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Publish', $data->Publish, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title', $data->Title, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Title_en_US', $data->Title_en_US, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Content', $data->Content, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('Content_en_US', $data->Content_en_US, $document, $domElement);
		$ImageValue = ($data->Image()->ID != 0) ? $data->Image()->getAbsoluteURL() : "";
		$this->CreateAndSetDOMAttributeTo('Image', $ImageValue, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('ImageOnly', $data->ImageOnly, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('TimerStartDate', $data->TimerStartDate, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('TimerStartTime', $data->TimerStartTime, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('TimerEndDate', $data->TimerEndDate, $document, $domElement);
		$this->CreateAndSetDOMAttributeTo('TimerEndTime', $data->TimerEndTime, $document, $domElement);
		
		// create and append TimerRecurringDaysOfWeek 
		$TimerRecurringDaysOfWeeks = $data->TimerRecurringDaysOfWeek();
		$TimerRecurringDaysOfWeeksElement = $this->RenderDOMNodeTimerRecurringDaysOfWeek($document, $TimerRecurringDaysOfWeeks);
		$domElement->appendChild($TimerRecurringDaysOfWeeksElement);
		
		return $domElement;
	}
	
	/**
	 * Creates and returns a nested DOMNode 'InfoMessages'
	 * with child nodes of class InfoMessage
	 * with the given $nodeName and assigns data provided via $data.
	 * 
	 * @param DOMDocument $document Instance of the current DOMDocument
	 * @param DataList|ArrayList $data List of RecurringDaysOfWeek DataObjects
	 * @param String $nodeName The DOMElement node name that will be created. Default: 'TimerRecurringDaysOfWeeks'
	 * @return DOMElement $domElement The DOMElement with attached attributes
	 */
	public function RenderDOMNodeInfoMessages($document, $data, $nodeName = 'InfoMessages') {
		// create DOMElement
		$domElement = $document->createElement($nodeName);
		// create and append single DOMElements for each item
		foreach ($data as $dataobject) {
			$domElement->appendChild($this->CreateDOMElementInfoMessage($document, $dataobject));
		}
		return $domElement;
	}
	
}
