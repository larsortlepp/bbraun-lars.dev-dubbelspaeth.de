<?php

/**
 * Parent Class for all ImportTasks containing general methods and
 * helper functions, for reading, querying and parsing external sources
 * and generating DataObjects and relations.
 * 
 * @requires \Httpful\Httpful (@link http://phphttpclient.com/)
 */
class ImportTask extends ProcessTask {
	
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ImportConfig';
	
	public static $allowed_actions = array (
		'task'
	);
	
	private static $url_handlers = array(
		'task/$ID' => 'task'
    );
	
	/**
	 * Method task() is for executing a single task identified by ID
	 * Execution can be through button in TaskConfig CMS interface or by URL
	 * 
	 * Function must return string 'success' if everything was right.
	 * This will be used in the ajax request function to process error and success messages in CMS.
	 * 
	 * @return String $return 'success' if successfull, 'error' if error occured
	 */
	public function task($request) {
		
		$this->taskstartdatetime = date('Y-m-d H:i:s');
		$taskObjectClassName = $this->getTaskObjectClassName();
		Subsite::disable_subsite_filter($disabled = true);
		$taskconfig = $taskObjectClassName::get()->filter('ID', $this->getRequest()->param('ID'))->first();
		if (!$taskconfig) return $this->redirect('/error-404.html', 404);
		if(Director::is_cli() || $this->can_run_single_task()){
			$success = $this->execute_task($taskconfig, $request);
			Subsite::disable_subsite_filter($disabled = false);
			return $success ? 'success' : 'error';
		}
		else {
			return Security::permissionFailure();
		}

	}
		
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
	}
	
	/**
	 * Reads xml file from external url
	 * Usese $this->owner->XMLFileURL as file source
	 * 
	 * @return SimpleXMLElement $xml SimpleXMLElement der entsprechenden Datei
	 * 
	 */
	public function readXMLFileFromURL($url = null) {
		// check if ile exists
		$file_headers = @get_headers($url);
		if (stripos($file_headers[0],"404") >0  || (stripos($file_headers[0], "302") > 0 || stripos($file_headers[7],"404") > 0)) {
			return false;
		}
		return simplexml_load_file($url);
	}
	
	/**
	 * Does an API call using the Httpful library and returns the response.
	 * Currently supports HTTP methods 'GET', 'POST' and 'PUT'
	 * and can send a JSON string as $body with 'POST' and 'PUT' request.
	 * The response will automatically be transformed into a PHP object,
	 * if it is in JOSN or XML format (and proper MIME type is sent).
	 * 
	 * @requires \Httpful\Httpful (@link http://phphttpclient.com/)
	 * 
	 * @param String $url The API URL. May contain additional URL parameters (e.g. mydomain.com/api/method?key=1234)
	 * @param String $method The HTTP method that will be used doing the API call. 'POST', 'GET' or 'PUT'. Note: DELETE, HEAD and OPTIONS not implmeneted yet.
	 * @param String $body The request body that will be sent with a 'POST' or 'PUT' request. Currently only JSON string is supported.
	 * @return HttpfulResponse|Boolean $respone The response from the API call. If response is JSON or XML it will automaticaly be converted into a PHP object.
	 */
	function httpful_request($url, $method, $body = false) {
		// load Httpful library through its own autoloader
		require(Director::baseFolder().'/httpful/bootstrap.php');
		
		// send request based on $method (GET,POST,PUT). Default: 'GET'
		switch($method) {
			
			case 'POST':
				// NOTE: currently only supports JSON as body
				$response = \Httpful\Request::post($url)
					->sendsJson()
					->body($body)
					->send();
				break;
			
			case 'PUT':
				// NOTE: currently only supports JSON as body
				$response = \Httpful\Request::put($url)
					->sendsJson()
					->body($body)
					->send();
				break;
			
			default:
				$response = \Httpful\Request::get($url)
					->send();
				break;
			
		}
		return $response;
	}

	/**
	 * Converts a String that was imported from XML file to a proper string
	 * 
	 * 1. converting escaped line breaks &#xD; and &#xA; to \n
	 * MySQL strips out all other line feeds (\r (Mac) or \r\n (Windows))
	 * and replaces them with \n. So the data gets stored only with \n.
	 * To be able to compare a string with an existing string in the database
	 * it does need to contain only \n - otherwise it would not match.
	 * 
	 * 2. removing multiple spaces to a single space
	 * 
	 * 3. remove spaces from beginning and end of each line (trim lines)
	 * 
	 * @param String $xmlString The String that was imported from XML file
	 * @return String $returnString The cleaned up string with proper line breaks
	 */
	public static function convert_xml_to_string($xmlString = '') {
		$returnString = $xmlString;
		// replace all possible new line characters by standard UNIX newline ("\n")
		$returnString = preg_replace("/[\r]+/", "\n", $returnString);
		$returnString = preg_replace("/[\n]+/", "\n", $returnString);
		// remove multiple spaces, and tabs to a single space
		$returnString = preg_replace("/[ \t]+/", " ", $returnString);
		// remove spaces from beginning and end of each line of the string
		$returnString = implode("\n", array_map('trim', explode("\n", $returnString)));
		return $returnString;
	}
	
	/**
	 * Adds Data from a associative array to the corresponding fields
	 * in the given DataObject.
	 * If $performCheck is true: before assigning a value it will be checked 
	 * if the database field exists on the dataobject.
	 * 
	 * @param DataObject $dataobject The DataObject where the data will be assigned
	 * @param Array $data Associative array with fieldnames and values that will be assigned to the dataobject
	 * @param Boolean $performCheck If true (default) values from $data array are only assigned if the given databasefield exists on object. If false: no check will be performed.
	 * @return DataObject
	 */
	public static function assign_data_to_dataobject($dataobject = null, $data = null, $performCheck = true) {
		// add all Variables from $data array
		$dbFields = DataObject::custom_database_fields($dataobject->ClassName);
		foreach($data as $key => $value) {
			Debug::message(" CHECKING ".$key.":". $value." on ".$dataobject->ClassName);
			if( ($performCheck && array_key_exists($key, $dbFields)) || $performCheck == false || $key == 'ParentID' || $key == 'SubsiteID') {
				$dataobject->$key = $value;
				Debug::message("... ASSIGNED");
			}
		}
		return $dataobject;
	}
	/**
	 * Add a many_many connections between $dataobject and $manymanyobject
	 * Only adds the connection if it does not already exist
	 * 
	 * @param DataObject $dataobject
	 * @param DataObject $manymanyclassname
	 */
	public function addManyManyToDataObject($dataobject = null, $manymanyobject = null) {
		
		$doClassName = $dataobject->ClassName;
		$manymanyClassName = $manymanyobject->ClassName;
		
		// check if many many realtion already exists
		$manymanyExists = DB::query("
			SELECT COUNT(*) 
			FROM ".$doClassName."_".$manymanyClassName."s
			WHERE ".$doClassName."ID = ".$dataobject->ID."
			AND ".$manymanyClassName."ID = ".$manymanyobject->ID."
		")->value();
		
		if(!$manymanyExists) {
			DB::query("
				INSERT INTO ".$doClassName."_".$manymanyClassName."s
				(
					".$doClassName."ID,
					".$manymanyClassName."ID
				)
				VALUES (
					".$dataobject->ID.",
					".$manymanyobject->ID."
				)
			");
		}
	}
	
	/**
	 * Delete all many_many connections between $manymanyclassname and given $dataobject
	 * IDs provided in $manymanyexceptions will be excluded
	 * 
	 * @param DataObject $dataobject
	 * @param String $manymanyclassname
	 * @param Array $manymanyexceptionids An array with IDs of DataObjects that are not removed from many_many connection
	 */
	public function removeManyManyFromDataObject($dataobject = null, $manymanyclassname = null, $manymanyexceptionids = null) {
		$doClassName = $dataobject->ClassName;
		// exclusion filter
		$exclusionfilter = ($manymanyexceptionids) ? " AND ".$manymanyclassname."ID NOT IN (".implode(',', $manymanyexceptionids).")" : '';
		
		Debug::message("removeManyManyFromDataObject EXCEPT ".implode(',', $manymanyexceptionids));
		// delete all given many_many relations
		DB::query("
			DELETE FROM ".$doClassName."_".$manymanyclassname."s
			WHERE ".$doClassName."ID = ".$dataobject->ID."
			$exclusionfilter
		")->value();
	}
	
	/**
	 * Function returns FoodCategory for given $uniqueFilter
	 * If no FoodCategory exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or acombination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need Title and RestaurantPageID.
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return FoodCategory  The existing or newly created FoodCategory
	 */
	public function createOrGetFoodCategory($uniqueFilter = null, $data = null) {
		
		if (!$uniqueFilter) return false;
		
		// Disable Subsite filter
		Subsite::disable_subsite_filter(true);
		
		Debug::message("createOrGetFoodCategoryForTitle()");
		
		// Get FoodCategory
		$FoodCategory = FoodCategory::get()->filter($uniqueFilter)->limit(1)->first();

		// Create FoodCategory if it does not exist and has a valid value
		if (!$FoodCategory) {
			
			Debug::message("... Creating new FoodCategory");
			
			$FoodCategory = FoodCategory::create();
			$FoodCategory->SortOrder = (DB::query("SELECT MAX(SortOrder) FROM FoodCategory WHERE RestaurantPageID = ".$uniqueFilter['RestaurantPageID'])->value() + 1); // add next highest SortOrder for new FoodCategory
			// add all initial variables from $uniqueFilter array
			$FoodCategory = $this->assign_data_to_dataobject($FoodCategory, $uniqueFilter);
		}
		
		// add all variables from $data array
		$FoodCategory = $this->assign_data_to_dataobject($FoodCategory, $data);
		
		Debug::message("... Set FoodCategory Values: Title = ".$FoodCategory->Title.", Title_en_US = ".$FoodCategory->Title_en_US.", SortOrder: ".$FoodCategory->SortOrder);
		
		// save and publish
		$FoodCategory->write();
		
		
		return $FoodCategory;
	}
	
	/**
	 * Function returns RestaurantDish for given $uniqueFilter
	 * If no RestaurantDish exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or a combination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need DishID and RestaurantPageID.
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return Dish  The existing or newly created RestaurantDish
	 */
	public function createOrGetRestaurantDish($uniqueFilter = null, $data = null) {
		
		if (!$uniqueFilter) return false;
		
		// RESTAURANTDISH (RestaurantPage specific)
		$RestaurantDish = RestaurantDish::get()->filter($uniqueFilter)->limit(1)->first();
		// Create RestaurantDish if it does not exist
		if (!$RestaurantDish) {
			
			$RestaurantDish = RestaurantDish::create();
			// add all initial variables from $uniqueFilter array
			$RestaurantDish = $this->assign_data_to_dataobject($RestaurantDish, $uniqueFilter);
		}
		
		// add all variables from $data array
		$RestaurantDish = $this->assign_data_to_dataobject($RestaurantDish, $data);		
		// save Dish
		$RestaurantDish->write();
		// the problem comes from the extension : RestaurantDish --> MyDataObjectRegardRestaurantPageExtension
		return $RestaurantDish;
	}
	
	/**
	 * Function returns Dish for given $uniqueFilter
	 * If no Dish exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are 
	 * assigned to the corresponding fields on DataObject.
	 * In this case: on Dish and RestaurantDish (see notice below)
	 * 
	 * NOTICE
	 * When calling Dish->write() the depending functions Dish->onBeforeWrite() 
	 * and Dish->onAfterWrite() are automatically called as well!
	 * The latter also sets all variables on the RestaurantDish object.
	 * So we need to provide these variables in the $data array as well.
	 * Otherwise all values on the RestaurantDish will be set to null
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or a combination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need SubsiteID and ImportID or Description.
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return Dish  The existing or newly created Dish
	 */
	public function createOrGetDish($uniqueFilter = null, $data = null) {
		
		if (!$uniqueFilter) return false;
		
		// CAUTION:
		// comparison of Description with multiple lines does only work
		// when query string only contains "\n" as line feed 
		// because database entry only contains "\n"
		// if query string contains "\r" or "\r\n" the query does not match
		// DISH (Subsite specific)
		$Dish = Dish::get()->leftJoin('RestaurantDish', 'RestaurantDish.DishID = Dish.ID')->filter($uniqueFilter)->limit(1)->first();
		// Create Dish if it does not exist
		if (!$Dish) {
			$Dish = Dish::create();
			// add all initial variables from $uniqueFilter array
			$Dish = $this->assign_data_to_dataobject($Dish, $uniqueFilter);
		}
		// add all variables from $data array
		$Dish = $this->assign_data_to_dataobject($Dish, $data);		
		// save Dish
		$Dish->write();
		return $Dish;
	}
	
	/**
	 * Function creates a new Dish and assigns $data
	 * Before returning the DataObject all values that are provided in $data are 
	 * assigned to the corresponding fields on DataObject.
	 * 
	 * NOTICE
	 * When calling Dish->write() the depending functions Dish->onBeforeWrite() 
	 * and Dish->onAfterWrite() are automatically called as well!
	 * The latter also sets all variables on the RestaurantDish object.
	 * So we need to provide these variables in the $data array as well.
	 * Otherwise all values on the RestaurantDish will be set to null
	 * 
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return Dish  The existing or newly created Dish
	 */
	public function createDish($data = null) {
		
		$Dish = Dish::create();
		// add all initial variables from $uniqueFilter array
		$Dish = $this->assign_data_to_dataobject($Dish, $data);		
		// save Dish
		$Dish->write();
		
		Debug::message("CREATING NEW DISH: ".$Dish->ID);
		
		return $Dish;
	}
	
	/**
	 * Function returns DishLabel for given $uniqueFilter
	 * If no DishLabel exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or a combination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need Title and optionally SubsiteID. If a SubsiteID is given a SubsiteDishLabel for the Subsite will be created, otherwise a GlobalDishLabel will be created (available to all Subsites)
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return [Global|Subsite]DishLabel  The existing or newly created DishLabel
	 */
	public function createOrGetDishLabel($uniqueFilter = null, $data =  null) {
		
		Debug::message("CreateOrGetDishLabelForTitle()");
			
		if (!$uniqueFilter)	return false;
		
		$dishLabelClassName = 'GlobalDishLabel';
		$sortFilter = '';
		
		// if SubsiteID is given: create a SubsiteDishLabel
		if(array_key_exists('SubsiteID', $uniqueFilter)) {
			$dishLabelClassName = 'SubsiteDishLabel';
			$sortFilter = "WHERE SubsiteID = ".$uniqueFilter['SubsiteID'];
			Debug::message("Creating ".$dishLabelClassName);
		}
		
		// Disable Subsite filter
		Subsite::disable_subsite_filter(true);
		
		// DISH LABEL
		$DishLabel = $dishLabelClassName::get()->filter($uniqueFilter)->limit(1)->first();

		// Create DishLabel if it does not exist
		if (!$DishLabel) {
			$DishLabel = $dishLabelClassName::create();
			$DishLabel->SortOrder = (DB::query("SELECT MAX(SortOrder) FROM $dishLabelClassName $sortFilter")->value() + 1); // add next highest SortOrder
			// add all initial variables from $uniqueFilter array
			$DishLabel = $this->assign_data_to_dataobject($DishLabel, $uniqueFilter);
		}
		
		// add all Variables from $data array
		$DishLabel = $this->assign_data_to_dataobject($DishLabel, $data);

		// save DishLabel
		$DishLabel->write();
		
//		if(MenuContainerImportXMLExtension::$debug) print_r($DishLabel);
		
		return $DishLabel;
	}
	
	/**
	 * Function returns SpecialLabel for given $uniqueFilter
	 * If no SpecialLabel exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or a combination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need Title and optionally SubsiteID. If a SubsiteID is given a SubsiteSpecialLabel for the Subsite will be created, otherwise a GlobalSpecialLabel will be created (available to all Subsites)
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return [Global|Subsite]SpecialLabel  The existing or newly created SpecialLabel
	 */
	public function createOrGetSpecialLabel($uniqueFilter = null, $data =  null) {
		
		Debug::message("createOrGetSepcialLabelForTitle()");
			
		if (!$uniqueFilter)	return false;
		
		$specialLabelClassName = 'GlobalSpecialLabel';
		$sortFilter = '';
		
		// if SubsiteID is given: create a SubsiteDishLabel
		if(array_key_exists('SubsiteID', $uniqueFilter)) {
			$specialLabelClassName = 'SubsiteSpecialLabel';
			$sortFilter = "WHERE SubsiteID = ".$uniqueFilter['SubsiteID'];
			Debug::message("Creating ".$specialLabelClassName);
		}
		
		// Disable Subsite filter
		Subsite::disable_subsite_filter(true);
		
		// SPECIAL LABEL
		$SpecialLabel = $specialLabelClassName::get()->filter($uniqueFilter)->limit(1)->first();

		// Create SpecialLabel if it does not exist
		if (!$SpecialLabel) {
			$SpecialLabel = $specialLabelClassName::create();
			$SpecialLabel->SortOrder = (DB::query("SELECT MAX(SortOrder) FROM $specialLabelClassName $sortFilter")->value() + 1); // add next highest SortOrder
			// add all initial variables from $uniqueFilter array
			$SpecialLabel = $this->assign_data_to_dataobject($SpecialLabel, $uniqueFilter);
		}
		
		// add all Variables from $data array
		$SpecialLabel = $this->assign_data_to_dataobject($SpecialLabel, $data);

		// save SpecialLabel
		$SpecialLabel->write();
		
		return $SpecialLabel;
	}
	
	/**
	 * Function returns Additive for given $uniqueFilter
	 * If no Additive exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or a combination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need Title or Number and optionally SubsiteID. If a SubsiteID is given a SubsiteAdditive for the Subsite will be created, otherwise a GlobalAdditive will be created (available to all Subsites)
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return [Global|Subsite]Allergen  The existing or newly created Allergen
	 */
	public function createOrGetAdditive($uniqueFilter = null, $data = null) {
		
		Debug::message("createOrGetAdditiveForTitle()");
			
		if (!$uniqueFilter) return false;
		
		$additiveClassName = 'GlobalAdditive';
		$sortFilter = '';
		
		// if SubsiteID is given: create a SubsiteAdditive
		if(array_key_exists('SubsiteID', $uniqueFilter)) {
			$additiveClassName = 'SubsiteAdditive';
			$sortFilter = "WHERE SubsiteID = ".$uniqueFilter['SubsiteID'];
			Debug::message("Creating ".$additiveClassName);
		}
		
		// Disable Subsite filter
		Subsite::disable_subsite_filter(true);
		
		// Additive
		$Additive = $additiveClassName::get()->filter($uniqueFilter)->limit(1)->first();

		// Create Additive if it does not exist
		if (!$Additive) {
			$Additive = $additiveClassName::create();
			$Additive->SortOrder = (DB::query("SELECT MAX(SortOrder) FROM $additiveClassName $sortFilter")->value() + 1); // add next highest SortOrder
			// add all initial variables from $uniqueFilter array
			$Additive = $this->assign_data_to_dataobject($Additive, $uniqueFilter);
		}
		
		// add all Variables from $data array
		$Additive = $this->assign_data_to_dataobject($Additive, $data);
		// save Additive
		$Additive->write();
		
		return $Additive;
	}
	
	/**
	 * Function returns Allergen for given $uniqueFilter
	 * If no Allergen exists, it will be created.
	 * Before returning the DataObject all values that are provided in $data are assigned to the corresponding fields on DataObject.
	 * 
	 * @param Array $uniqueFilter Associative array that defines a unique field or a combination of multiple fields on the DataObject that will be used to get the DataObject from the database. E.g. array('Date' => '2015-03-24') will look up a DataObject that has the given date. In this case we need Title or Number and optionally SubsiteID. If a SubsiteID is given a SubsiteAllergen for the Subsite will be created, otherwise a GlobalAllergen will be created (available to all Subsites)
	 * @param Array $data Associative array with data for DataObject mapping array(FieldName => Value). E.g. array('Title_en_US' => 'My english title')
	 * 
	 * @return [Global|Subsite]Allergen  The existing or newly created Allergen
	 */
	public function createOrGetAllergen($uniqueFilter = null, $data = null) {
		
		Debug::message("createOrGetAllergenForNumber()");
			
		if (!$uniqueFilter) return false;
		
		$allergenClassName = 'GlobalAllergen';
		$sortFilter = '';
		
		// if SubsiteID is given: create a SubsiteAllergen
		if(array_key_exists('SubsiteID', $uniqueFilter)) {
			$allergenClassName = 'SubsiteAllergen';
			$sortFilter = "WHERE SubsiteID = ".$uniqueFilter['SubsiteID'];
			Debug::message("Creating ".$allergenClassName);
		}
		
		// Disable Subsite filter
		Subsite::disable_subsite_filter(true);
		
		// ALLERGEN
		$Allergen = $allergenClassName::get()->filter($uniqueFilter)->limit(1)->first();

		// Create Allergen if it does not exist
		if (!$Allergen) {
			$Allergen = $allergenClassName::create();
			$Allergen->SortOrder = (DB::query("SELECT MAX(SortOrder) FROM $allergenClassName $sortFilter")->value() + 1); // add next highest SortOrder
			// add all initial variables from $uniqueFilter array
			$Allergen = $this->assign_data_to_dataobject($Allergen, $uniqueFilter);
		}
		
		// add all Variables from $data array
		$Allergen = $this->assign_data_to_dataobject($Allergen, $data);

		// save Allergen
		$Allergen->write();
		
		//if(MenuContainerImportXMLExtension::$debug) print_r($Allergen);
		
		return $Allergen;
	}
	
	/**
	 * Add Dish for given $dishID to DailyMenuPage for given ID
	 * adding the given SortOrder
	 * while avoiding duplicate entries by checking if Dish already exists on DailyMenuPage
	 * 
	 * 
	 * @param Integer $dishID ID of the Dish that is added to the DailyMenuPage
	 * @param Integer $dailyMenuPageID The ID of the DailyMenuPage
	 * @param Integer $sortOrder SortOrder for Dishes assigned to the DailyMenuPage
	 */
	public function addDishToDailyMenuPage($dishID = null, $dailyMenuPageID = null, $sortOrder = null) {
		
		Debug::message("AddDishToDailyMenuPage(".$dailyMenuPageID.",". $dishID.",". $sortOrder.")");
		
		if(!$dailyMenuPageID || !$dishID) return false;
		
		$DishExists = DB::query("
			SELECT COUNT(*) 
			FROM DailyMenuPage_Dishes
			WHERE DailyMenuPageID = $dailyMenuPageID
			AND DishID = $dishID
		")->value();
		
		Debug::message(".. Dish Count is: ".$DishExists);
		
		// Add Dish to DailyMenuPage if it isn´t already assigned
		if(!$DishExists) {
			Debug::message(" .. Adding Dish to DailyMenuPage");
			DB::query("
				INSERT INTO DailyMenuPage_Dishes
				(
					DailyMenuPageID,
					DishID,
					SortOrder
				)
				VALUES (
					$dailyMenuPageID,
					$dishID,
					$sortOrder
				)
			");
		}
		// Update SortOrder for existing Dish
		else {
			Debug::message(" .. Updating SortOrder of existing Dish on DailyMenuPage to: ".$sortOrder);
			DB::query("
				UPDATE DailyMenuPage_Dishes
				SET SortOrder = $sortOrder
				WHERE DailyMenuPageID = $dailyMenuPageID
				AND DishID = $dishID
			");
		}
	}
}
