<?php

class ImportRestaurantConfig extends ImportConfig  implements PermissionProvider {

	public static $singular_name = "Import Restaurant";

	public static $plural_name = "Import Restaurants";
	
	private static $db = array (
		 'ImportFunction' =>  "Enum('import_lz_catering','import_lz_catering')"
    );
	
	private static $has_one = array(
		'RestaurantPage' => 'RestaurantPage'
	);
	
	private static $field_labels = array(
		'URL' => 'URL',
		'Title' => 'Titel',
		'ImportFunction' => 'Import Funktion',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'RestaurantPage.Title' => 'Restaurant',
		'Published.Nice' => 'aktiv',
		'Published' => 'aktiv'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'RestaurantPage.Title',
		'Title',
		'ImportFunction',
		'URL',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'RestaurantPage.Title',
		'ImportFunction',
		'Title',
		'URL',
		'LastStarted',
		'LastRun'
	);

    public function getCMSFields() {
		$f = parent::getCMSFields();
		$f->removeFieldFromTab('Root.Main', 'ImportFunction');
		
		// Add Button to execute task
		if(
			$this->ID && $this->URL && $this->RestaurantPageID && $this->ImportFunction && 
			($this->canRunAllTask() || $this->canRunSingleTask())
		){
			$f->addFieldToTab('Root.Main', new LiteralField('RunTask', '<a href="'.Director::absoluteBaseURL()."tasks/importrestaurant/task/".$this->ID.'" target="_blank" class="custom-button icon-arrows-ccw ajax-executeSingleProcessTask">'.$this::$singular_name.' ausführen</a>'), 'GroupField');
		}
		//Add DropDown for Restaurant
		$restaurantPages = RestaurantPage::get();
		if($restaurantPages->count() >= 1) {
			$f->addFieldToTab(
				'Root.Main', 
				DropdownField::create(
					'RestaurantPageID', 
					'Restaurant', 
					$restaurantPages->map('ID', 'Title')
				)
					->setEmptyString('Bitte auswählen')
					->setDescription('Import der Daten erfolgt in den ausgewählten Speiseplan'),
				'Schedule'
			);
		}
		else {
			$f->addFieldToTab(
				'Root.Main', 
				FieldGroup::create(
					'RestaurantPageMessage',
					LiteralField::create(
						'RestaurantPageMessage',
						'Das Restaurant kann in der Hauptsite nicht geändert werden'
					)
				)
					->setTitle('Restaurant'),
				'Schedule'
			);
		}
		$f->addFieldToTab(
			'Root.Main', 
			DropdownField::create(
				'ImportFunction',
				'Import Funktion',
				$this->dbObject('ImportFunction')->enumValues()
			)
				->setEmptyString('Bitte auswählen'),
			'Schedule'
		);
		return $f;

    }
	
	public function canCreate($member = null) {
        return Permission::check('CREATE_IMPORTRESTAURANTCONFIG', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_IMPORTRESTAURANTCONFIG', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_IMPORTRESTAURANTCONFIG', 'any', $member);
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_IMPORTRESTAURANTCONFIG', 'any', $member);
    }
	
	public function canRunAllTask($member = null) {
        return Permission::check('RUN_ALL_IMPORTRESTAURANTTASK', 'any', $member);
    }
	
	public function canRunSingleTask($member = null) {
        return Permission::check('RUN_SINGLE_IMPORTRESTAURANTTASK', 'any', $member);
    }
	
	public function providePermissions() {
		return array(
			"CREATE_IMPORTRESTAURANTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 110
			),
			"VIEW_IMPORTRESTAURANTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 120
			),
			"EDIT_IMPORTRESTAURANTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 130
			),
			"DELETE_IMPORTRESTAURANTCONFIG" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 140
			),
			"RUN_ALL_IMPORTRESTAURANTTASK" => array(
				'name' => 'Kann alle Aufgaben "'.$this::$singular_name.'" ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 150
			),
			"RUN_SINGLE_IMPORTRESTAURANTTASK" => array(
				'name' => 'Kann einzelne Aufgabe "'.$this::$singular_name.'" für Restaurant ausführen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 160
			)
		);
	}
}

