<?php

/* 
 * Task importing Dish to selected RestaurantPage without adding them to DailyMenuPage
 */
class ImportRestaurantTask extends ImportTask {
	
	
	public static $singular_name = "Import Restaurant";

	public static $plural_name = "Import Restaurant";
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ImportRestaurantConfig';
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig, $request = null) {
		$importFunction = $taskconfig->ImportFunction;
		if(method_exists($this, $importFunction)) return $this->$importFunction($taskconfig);
	}
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		$result = Permission::check("RUN_ALL_IMPORTRESTAURANTTASK");
		return  $result;
	}
	
	/**
	 * Provide permissions for current member to run the task "task()"
	 * 1. Member needs "RUN_SINGLE_IMPORTRESTAURANTTASK" permission
	 * 2. Member needs permission to edit the RestaurantPage that the assigned MenuContainer belongs to
	 *  
	 * @return Boolean
	 */
	public function can_run_single_task() {
		$result = false;
		
		if($this->can_run_task() || Permission::check("RUN_SINGLE_IMPORTRESTAURANTTASK")) {
			$taskObjectClassName = $this->getTaskObjectClassName();
			$taskconfig = $taskObjectClassName::get()->filter('ID', $this->getRequest()->param('ID'))->First();
			if($taskconfig) {
				$restaurantPage = $taskconfig->RestaurantPage();
				if($restaurantPage && $restaurantPage->exists() && $restaurantPage->canEdit(Member::currentUser())){
					$result = true;
				}
			}
		}
		return  $result;
	}
	
	public function import_lz_catering($taskconfig){
		Debug::message("import_lz_catering()");
		
		// Array that defines mapping of nutritives from xml file to Dish variables
		// array( [XMLnutritiveName] => [DishNutritiveVariableName], ...)
		$nutritives_map = array();
		$NutritivesMapConfig = $taskconfig->NutritivesMapConfig();
		if($NutritivesMapConfig && $NutritivesMapConfig->exists()) {
			$NutritivesMapObjects = $NutritivesMapConfig->NutritivesMapObjects();
			if($NutritivesMapObjects && $NutritivesMapObjects->exists()) {
				$nutritives_map = $NutritivesMapObjects->map('ExternalKey','InternalKey')->toArray();
			}
		}

		
		// Array that defines a blacklist of FoodCategories
		// which are not imported
		$foodcategory_blacklist = array();
		$FoodCategoryBlacklistConfig = $taskconfig->FoodCategoryBlacklistConfig();
		if($FoodCategoryBlacklistConfig && $FoodCategoryBlacklistConfig->exists()) {
			$FoodCategoryBlacklistObjects = $FoodCategoryBlacklistConfig->FoodCategoryBlacklistObjects();
			if($FoodCategoryBlacklistObjects && $FoodCategoryBlacklistObjects->exists()) {
				$foodcategory_blacklist = $FoodCategoryBlacklistObjects->map()->toArray();
			}
		}

		
		// Array that defines a blacklist of Additives
		$additives_blacklist = array();
		$AdditivesBlacklistConfig = $taskconfig->AdditivesBlacklistConfig();
		if($AdditivesBlacklistConfig && $AdditivesBlacklistConfig->exists()) {
			$AdditivesBlacklistObjects = $AdditivesBlacklistConfig->AdditivesBlacklistObjects();
			if($AdditivesBlacklistObjects && $AdditivesBlacklistObjects->exists()) {
				$additives_blacklist = $AdditivesBlacklistObjects->map()->toArray();
			}
		}

		
		// Array that defines a blacklist of Allergens
		$allergens_blacklist = array();
		$AllergensBlacklistConfig = $taskconfig->AllergensBlacklistConfig();
		if($AllergensBlacklistConfig && $AllergensBlacklistConfig->exists()) {
			$AllergensBlacklistObjects = $AllergensBlacklistConfig->AllergensBlacklistObjects();
			if($AllergensBlacklistObjects && $AllergensBlacklistObjects->exists()) {
				$allergens_blacklist = $AllergensBlacklistObjects->map()->toArray();
			}
		}

		
		// Array that defines a blacklist of DishLabels
		$dishlabels_blacklist = array();
		$DishlabelsBlacklistConfig = $taskconfig->DishLabelsBlacklistConfig();
		if($DishlabelsBlacklistConfig && $DishlabelsBlacklistConfig->exists()) {
			$DishlabelsBlacklistObjects = $DishlabelsBlacklistConfig->DishLabelsBlacklistObjects();
			if($DishlabelsBlacklistObjects && $DishlabelsBlacklistObjects->exists()) {
				$dishlabels_blacklist = $DishlabelsBlacklistObjects->map()->toArray();
			}
		}

		
		// Array that defines a blacklist of $speciallabels_blacklist
		$speciallabels_blacklist = array();
		$SpecialLabelsBlacklistConfig = $taskconfig->SpecialLabelsBlacklistConfig();
		if($SpecialLabelsBlacklistConfig && $SpecialLabelsBlacklistConfig->exists()) {
			$SpecialLabelsBlacklistObjects = $SpecialLabelsBlacklistConfig->SpecialLabelsBlacklistObjects();
			if($SpecialLabelsBlacklistObjects && $SpecialLabelsBlacklistObjects->exists()) {
				$speciallabels_blacklist = $SpecialLabelsBlacklistObjects->map()->toArray();
			}
		}
		
		// set to stage mode to create all data in Stage and then publish to Live
		$currStage = Versioned::current_stage();
		Versioned::set_reading_mode(''); // set to stage

		// collect necessary data
		$restaurantPageID = $taskconfig->RestaurantPageID;
		$restaurantPage = RestaurantPage::get()->byID($restaurantPageID);
		$subsiteID = $restaurantPage->SubsiteID;
		$siteConfig = SiteConfig::get()->filter('SubsiteID', $subsiteID)->limit(1)->first();
		
		Debug::message("RestaurantPage (".$restaurantPageID."): ".$restaurantPage->Title." - SubsiteID: ".$subsiteID);
		
		// get SimpleXMLElement of file
		$xml = $this->readXMLFileFromURL($taskconfig->URL);
		// abort is error loading file
		if(!$xml) {
			Debug::message("Could not read from xml file");
			return false;
		}
		
		// set trigger for importing dish as freitext to false (default)
		$importAsFreitext = false;
		
		// DISHES
		$mainDishRestaurantDishID = 0;
		foreach ($xml->children() as $xmlDish) {
			
			// Exclude Dish with Name containing INAKTIV
			if(strpos($xmlDish['Name'], 'INAKTIV') !== false) {
				Debug::message("Excluding DISH: ".$xmlDish['Name']);
				continue;
			}
			
			// generate array with dish data
			// key of Array needs to map the attribute of Dish object 
			// (e.g. $data['Title_en_US'] will be written to Dish->Title_en_US
			$dishData = array();
			$restaurantDishData = array();
			$dishUniqueFilter = array();

			// generate arrays for storing connections to Additives, Allergens, DishLabels and SpecialLabels
			// we store the IDs of each DataObject in the array to assign tem to the Dish after creating it
			$dishAdditives = new ArrayList();
			$dishAllergens = new ArrayList();
			$dishDishLabels = new ArrayList();
			$dishSpecialLabels = new ArrayList();
			
			// Assumption for dishes:
			// - we import Components only
			// - the PLU is the unique identifier for components. So we use them as ImportID
			// - we assign the ComponentGroup as FoodCategory
				
			// create importID from Component["CompType"] and Component["CompID"]
			$dishImportID = Convert::raw2sql($xmlDish['ProductPLU']);

			$dishData['Description'] = $this->convert_xml_to_string($xmlDish->ComponentDetails->GastDesc["value"]);
			Debug::message("IMPORTING DISH: ID: $dishImportID, Name: ".$dishData['Description']);
			
			// Import as Dish-per-Restaurant (by adding RestaurantPageID)
			if (
				$siteConfig->EnableIndividualDishesPerRestaurant &&
				singleton('Dish')->hasExtension('DishesPerRestaurantDishExtension')
			) {
				// if Dish name contains special trigger "(LOKAL)"
				// add RestaurantPageID
				if(strpos($dishData['Description'], '(LOKAL)') !== false) {
					// remove special trigger string from description
					$dishData['Description'] = trim(str_replace('(LOKAL)', '', $dishData['Description']));
					// Add RestaurantPageID to unique filter
					// This will limit search for Dishes to the RestaurantPage 
					// and also add the RestaurantPageID if the Dish is created.
					// The dish with the ImportID is only unique to the restaurant
					// there can be multiple Dishes with the same ImportID but different RestaurantPageIDs
					$dishUniqueFilter['RestaurantPageID'] = $restaurantPageID;
					Debug::message('DISH IS ASSIGNED AS DISH PER RESTAURANT');
				}
				// Add RestaurantPageID with 0
				// to avoid conflicts or overwriting exisiting Dishes with the same ImportID
				// but different RestaurantPageIDs
				else {
					$dishUniqueFilter['RestaurantPageID'] = 0;
				}
			}
			
			//Dish translation
			foreach($xmlDish->ComponentDetails->GastDesc->children() as $xmlDishTranslation) {
				// ENGLISH TRANSLATION Dish
				if($xmlDishTranslation["lang"] == 'en-gb') {
					Debug::message("	- - DISH TRANSLATION: ".$xmlDishTranslation["value"]);
					//$foodCategoryData['Title_en_US'] = Convert::raw2sql($xmlFoodcategoryTranslation["Name"]);
					break;
				}	
			}
			
			// get Prices (stored in <ProductInfo><Product>) 
			// we assume the first product is always the "main" product with the correct prices
			// -> Mapping of "name" of the <SetMenu> and <Product> does not work in all cases
			#$xmlDishProduct = $xmlDish->ComponentDetails->ProductInfo->Product[0];
			#$restaurantDishData['Price'] = Dish::convertToFloat(Convert::raw2sql($xmlDishProduct['ProductPrice']));
					
			// :TEMP:
			// Import Price from Component.SalesPrice (as requested by Fr. König 9.10.)
			$restaurantDishData['Price'] = Dish::convertToFloat(Convert::raw2sql($xmlDish['ProductPrice']));
			Debug::message("ADDING Price to RestaurantDish: ".$restaurantDishData['Price']);
			
			// FOODCATEGORY	
			$foodCategoryTitle = Convert::raw2sql($xmlDish["ComponentGroup"]);
				
			// only import if FoodCategory is not in blacklist (strict checking)
			if($foodCategoryTitle && (!in_array($foodCategoryTitle, $foodcategory_blacklist, true))) {
					
				// generate array with FoodCategoryData data
				// key of Array needs to map the attribute of FoodCategory object 
				// (e.g. $data['Title_en_US'] will be written to FoodCategory->Title_en_US
				$foodCategoryData = array();

				Debug::message( " - - FOODCATEGORY: ".$foodCategoryTitle);


				// remove all numeric values from titles and map them to their base name
				// Heimat 1, Heimat 2, Heimat 3 are all mapped to "Heimat"
				// workaround to assign multiple dishes to the same FoodCategory
				// -> delegate only supports 1 Dish per FoodCategory
				$foodCategoryTitle = trim(preg_replace('/[0-9]+/', '', $foodCategoryTitle));

				// create uniqueFilter for FoodCategory
				$uniqueFilter = array(
					'Title'				=> $foodCategoryTitle,
					'RestaurantPageID'	=> $restaurantPageID
				);
				// get or create FoodCategory for title
				$FoodCategory = $this->createOrGetFoodCategory(
					$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
					$foodCategoryData	// array with fields and values that are assigned to the DataObject
				);
				// Assign a FoodCategory to a DISH
				$restaurantDishData['FoodCategoryID'] = $FoodCategory->ID;
			
				// get Nutritives
				foreach($xmlDish->ComponentDetails->NutritionInfo->Nutrient as $xmlNutrient) {
					Debug::message("	- - - * NUTRITIVE: ".$xmlNutrient["name"]." – value: ".$xmlNutrient["value"]);
						$xmlNutrientName = Convert::raw2sql($xmlNutrient["name"]);
						//Figure out the nutritives_map
						if(array_key_exists($xmlNutrientName, $nutritives_map)) {
							Debug::message("... Nutrient $xmlNutrientName is mapped to: ".$nutritives_map[$xmlNutrientName]." – value: ".$xmlNutrient["value"]);
							$dishData[$nutritives_map[$xmlNutrientName]] = Convert::raw2sql($xmlNutrient["value"]);
						}
				}

				// get all Additives, Allergens, DishLabels and SpecialLabels
				foreach($xmlDish->ComponentDetails->FoodLabelInfo->FoodLabelGroup as $xmlFoodLabelGroup) {

					// Exclude FoodLabelGroup with Name containing INAKTIV
					if(strpos($xmlFoodLabelGroup['name'], 'INAKTIV') !== false) {
						Debug::message("Excluding FoodGroup: ".$xmlFoodLabelGroup['name']);
						continue;
					}
					// ALLERGENS
					if(isset($xmlFoodLabelGroup->Allergens) && isset($xmlFoodLabelGroup->Allergens->FoodLabel)) {

						$allergenData = array();
						$allergenNumber = strtoupper(Convert::raw2sql($xmlFoodLabelGroup["code"]));
						$allergenData["Title"] = Convert::raw2sql($xmlFoodLabelGroup->Allergens->FoodLabel[0]["name"]);								

						Debug::message("IMPORTING ALLERGEN: ".$allergenData["Title"]);

						// when Allergen is not in blacklist (check for title)
						if(!in_array($allergenData["Title"], $allergens_blacklist, true)) {

							// :TODO: Import description from $xmlFoodLabelGroup->Allergens->FoodLabel->FoodLabelTranslation (that does not exist right now)
							// find english translation
							foreach($xmlFoodLabelGroup->FoodLabelGroupTranslation as $allergenTranslation) {
								if($allergenTranslation["lang"] == 'en-gb') {
									Debug::message("	- - - * ALLERGEN TRANSLATION: ".Convert::raw2sql($allergenTranslation["name"]));
									$allergenData['Title_en_US'] = Convert::raw2sql($allergenTranslation["name"]);
									break;
								}
							}

							// create uniqueFilter for Allergen
							// by adding the SubsiteID we define that we want to create a SubsiteAllergen
							$uniqueFilter = array(
								'Number' => $allergenNumber,
								'SubsiteID' => $subsiteID
							);
							// get or create SubsiteAllergen for Number
							$Allergen = $this->createOrGetAllergen(
								$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
								$allergenData		// array with fields and values that are assigned to the DataObject
							);
							// assign Allergen ID
							if($Allergen) {
								$dishAllergens->push($Allergen);
							}

	//						if(MenuContainerImportXMLExtension::$debug) print_r($allergenData);
						}


					}
					// ADDITIVES
					else if(isset($xmlFoodLabelGroup->Additives)) {

						$additiveData = array();
						$additiveData["Title"] = $xmlFoodLabelGroup["name"];
						Debug::message("	- - - * ADDITIVE - Additives exist inside: ".$additiveData["Title"]);
						// ADDITIVES TRANSLATION
						foreach($xmlFoodLabelGroup->FoodLabelGroupTranslation as $additiveTranslation) {
							if($additiveTranslation["lang"] == 'en-gb') {
								Debug::message("	- - - * ADDITIVE TRANSLATION: ".$additiveTranslation["name"]);
							}	
						}	

						$additiveNumber = Convert::raw2sql($xmlFoodLabelGroup["code"]);
						$additiveData["Title"] = Convert::raw2sql($xmlFoodLabelGroup["name"]);

						Debug::message("IMPORTING ADDITIVE - Additives exist inside: ".$additiveData["Title"]);

						// when Additive is not in blacklist (check for title)
						if(!in_array($additiveData["Title"], $additives_blacklist, true)) {

							// find english translation
							foreach($xmlFoodLabelGroup->FoodLabelGroupTranslation as $additiveTranslation) {
								if($additiveTranslation["lang"] == 'en-gb') {
									Debug::message("	- - - * ADDITIVE TRANSLATION: ".Convert::raw2sql($additiveTranslation["name"]));
									$additiveData['Title_en_US'] = Convert::raw2sql($additiveTranslation["name"]);
									break;
								}
							}

							// create uniqueFilter for Additive
							// by adding the SubsiteID we define that we want to create a SubsiteAdditive
							$uniqueFilter = array(
								'Number'	=> $additiveNumber,
								'SubsiteID' => $subsiteID
							);
							// get or create SubsiteAdditive for title
							$Additive = $this->createOrGetAdditive(
								$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
								$additiveData		// array with fields and values that are assigned to the DataObject
							);
							if($Additive) {
								$dishAdditives->push($Additive);
							}
						}
					}

					// SPECIALLABELS
					else if($xmlFoodLabelGroup["name"] == "Aktion" && isset($xmlFoodLabelGroup->Information->FoodLabel)) {
						
						foreach($xmlFoodLabelGroup->Information->FoodLabel as $specialLabel) {

							$specialLabelData = array();
							$specialLabelTitle = Convert::raw2sql($specialLabel["name"]);

							Debug::message("IMPORTING SPECIALLABEL: ".$specialLabelTitle);
							
							// when SpecialLabel is not in blacklist (check for title)
							if(!in_array($specialLabelTitle, $speciallabels_blacklist, true)) {

								// find english translation
								foreach($specialLabel->FoodLabelTranslation as $specialLabelTranslation) {
									if($specialLabelTranslation["lang"] == 'en-gb') {
										Debug::message("	- - - * SPECIALLABEL TRANSLATION: ".Convert::raw2sql($specialLabelTranslation["name"]));
										$specialLabelData['Title_en_US'] = Convert::raw2sql($specialLabelTranslation["name"]);
										break;
									}
								}

								// create uniqueFilter for SpecialLabel
								// by adding the SubsiteID we define that we want to create a SubsiteAdditive
								$uniqueFilter = array(
									'Title'		=> $specialLabelTitle,
									'SubsiteID' => $subsiteID
								);
								// get or create SubsiteSpecialLabel for title
								$SpecialLabel = $this->createOrGetSpecialLabel(
									$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
									$specialLabelData		// array with fields and values that are assigned to the DataObject
								);
								if($SpecialLabel) {
									$dishSpecialLabels->push($SpecialLabel);
								}
							}
						}
					}

					// DISHLABELS
					else if (isset($xmlFoodLabelGroup->Information->FoodLabel)) {

						foreach ($xmlFoodLabelGroup->Information->FoodLabel as $xmlDishLabel) {

							// SPECIAL TRIGGER
							// If Component is assigned "Freitext" it will be imported without ImportID
							// Freitext is a Dish based on always the same ImportID, but with changing description and attributes
							if ($xmlDishLabel["name"] == "Freitext") {

								Debug::message("FREITEXT FOUND: " . $xmlDish->ComponentDetails->GastDesc["value"]);

								$importAsFreitext = true;

								break;
							} 
							else {

								$dishLabelData = array();
								$dishLabelTitle = Convert::raw2sql($xmlDishLabel["name"]);

								Debug::message("	- - - * DISHLABEL: " . $dishLabelTitle);


								// when DishLabel is not in blacklist (check for title)
								if ((!($dishLabelTitle == "Tagesgericht")) && !in_array($dishLabelTitle, $dishlabels_blacklist, true)) {

									// find english translation
									foreach ($xmlDishLabel->FoodLabelTranslation as $dishLabelTranslation) {

										if ($dishLabelTranslation["lang"] == 'en-gb') {
											Debug::message(" ENGLISH TRANSLATION: " . Convert::raw2sql($dishLabelTranslation["name"]));
											$dishLabelData['Title_en_US'] = Convert::raw2sql($dishLabelTranslation["name"]);
											break;
										}
									}

									// create uniqueFilter for DishLabel
									// by adding the SubsiteID we define that we want to create a SubsiteDishLabel
									$uniqueFilter = array(
										'Title' => $dishLabelTitle,
										'SubsiteID' => $subsiteID
									);
									// get or create GlobalDishLabel for Title
									$DishLabel = $this->createOrGetDishLabel(
											$uniqueFilter, // array with fields and values that are used as filter to get an existing DataObject
											$dishLabelData  // array with fields and values that are assigned to the DataObject
									);
									if ($DishLabel) {
										$dishDishLabels->push($DishLabel);
									}
								}
							}
						}
					}
				}
				
				// SPECIAL TRIGGER
				// POSTFIX "EMPFEHLUNG: ": Dish Infotext
				// if Description is contains new line beginning with "EMPFEHLUNG: " -> do add this to Dish.Infotext and remove the string from Dish.Description
				preg_match("/EMPFEHLUNG: (.*)/", $dishData['Description'], $matchEmpfehlung);
				if ($matchEmpfehlung) {
					// get "EMPFEHLUNG: " and everything that follows it
					$dishData['Infotext'] = $matchEmpfehlung[1];
					// remove "EMPFEHLUNG: " text from Dish.Description
					$dishData['Description'] = preg_replace("/EMPFEHLUNG: (.*)/", "", $dishData['Description']);

					// find english translation
					if (array_key_exists('Description_en_US', $dishData)) {
						preg_match("/EMPFEHLUNG: (.*)/", $dishData['Description_en_US'], $matchEmpfehlung_en_US);
						if ($matchEmpfehlung_en_US) {
							$dishData['Infotext_en_US'] = $matchEmpfehlung_en_US[1];
							$dishData['Description_en_US'] = preg_replace("/EMPFEHLUNG: (.*)/", "", $dishData['Description_en_US']);
						}
					}

					Debug::message("... SPECIAL TRIGGER 'EMPFEHLUNG' FOUND: Adding Infotext: " . $dishData['Infotext']);
				}

				// If Dish is freitext: Always create a new Dish (without checking if a dish with same description or ImportID already exists)
				if($importAsFreitext) {
					// Add Subsite ID to $dishData
					$dishData['SubsiteID'] = $subsiteID;
					// Add ImportID "Freitext" to Dish to mark it as imported freitext Dish
					// can be used to delete all freitext Dishes by a cronjob to clean up the database
					$dishData['ImportID'] = "Freitext";
					$Dish = $this->createDish($dishData);
				}
				// else: create or get a Dish with given ImportID
				else {
					// add values to uniqueFilter for Dish
					$dishUniqueFilter['ImportID'] = $dishImportID;
					$dishUniqueFilter['SubsiteID'] = $subsiteID;
					// get or create Dish for given ImportID
					$Dish = $this->createOrGetDish(
						$dishUniqueFilter,	// array with fields and values that are used as filter to get an existing DataObject
						$dishData		// array with fields and values that are assigned to the DataObject
					);
				}
				// DISH / RESTAURANT DISH ASSIGNEMENTS
				if($Dish) {

					// Add RestaurantPageID to Dish if it is marked as 'Freitext'
					// and the DishesPerRestaurantDishExtension exists
					// -> restricts access to Dish to the current Restaurant
					if(
						$importAsFreitext &&
						$siteConfig->EnableIndividualDishesPerRestaurant &&
						$Dish->hasExtension('DishesPerRestaurantDishExtension')
					) {
						$Dish->RestaurantPageID = $restaurantPageID;
						$Dish->write();
					}

					// create uniqueFilter for RestaurantDish
					$uniqueFilter = array(
						'DishID' => $Dish->ID,
						'RestaurantPageID' => $restaurantPageID
					);
					// get or create RestaurantDish for DishID
					$RestaurantDish = $this->createOrGetRestaurantDish(
						$uniqueFilter,		// array with fields and values that are used as filter to get an existing DataObject
						$restaurantDishData	// array with fields and values that are assigned to the DataObject
					);


					// ASSIGN MANY_MANY RELATIONS

					// assign Additives
					foreach($dishAdditives as $additive) {
						$this->addManyManyToDataObject($Dish, $additive);
					}
					// remove all but the imported Additives
					$this->removeManyManyFromDataObject($Dish, 'SubsiteAdditive', array_keys($dishAdditives->map()));

					// assign Allergens
					foreach($dishAllergens as $allergen) {
						$this->addManyManyToDataObject($Dish, $allergen);
					}
					// remove all but the imported Allergens
					$this->removeManyManyFromDataObject($Dish, 'SubsiteAllergen', array_keys($dishAllergens->map()));

					// assign DishLabels
					foreach($dishDishLabels as $dishlabel) {
						$this->addManyManyToDataObject($Dish, $dishlabel);
					}
					// remove all but the imported Allergens
					$this->removeManyManyFromDataObject($Dish, 'SubsiteDishLabel', array_keys($dishDishLabels->map()));

					// assign SpecialLabels
					foreach($dishSpecialLabels as $speciallabel) {
						$this->addManyManyToDataObject($Dish, $speciallabel);
					}
					// remove all but the imported Allergens
					$this->removeManyManyFromDataObject($Dish, 'SubsiteSpecialLabel', array_keys($dishSpecialLabels->map()));

				}
			}
		}
		
		$succed = True;
		return $succed;
	}

}
