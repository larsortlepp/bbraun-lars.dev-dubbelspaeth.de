<?php

/**
 * Update Task for Updates from v 3.0.0alpha to 3.1.0
 *
 */
class UpdateTask_3_1_0 extends Controller {
	
	private static $allowed_actions = array(
		'index'
	);
	
	/**
	 * Provide permissions for current member to run the task
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		return Permission::check("ADMIN");
	}
	
	public function init() {
		parent::init();
		// Check access permission
		// Allow access from command line and from can_run_task() function
		if(!Director::is_cli() && !$this->can_run_task()) {
			return Security::permissionFailure();
		}
	}
	
	public function index() {
		ini_set('max_execution_time', 300); // execution 5 minutes
		$this->cleanup_db();
		$this->update_siteconfig();
		$this->update_displaytemplatepages();
		$this->update_menucontainer();
		// @todo manually delete all MenuContainer "Handelswaren" and all PrintTemplatePages "Druck Handelswaren A8"
		$this->update_infomessages();
		$this->update_printtemplatepages();
		$this->update_exporthtmltemplatepages();
		$this->create_importrestaurantconfig();
		// batch create ImportRestaurantConfig for every restaurant - set URL
	}
	
	public function cleanup_db() {
		// delete obsolete tables
		DB::query("DROP TABLE IF EXISTS phpmysqlautobackup");
		DB::query("DROP TABLE IF EXISTS phpmysqlautobackup_log");
		
		DB::query("DROP TABLE IF EXISTS SimplifyPermission");
		DB::query("DROP TABLE IF EXISTS Page_SimplifyHideDeleteGroups");
		DB::query("DROP TABLE IF EXISTS Page_SimplifyHideUnpublishGroups");
	}
	
	public function update_siteconfig() {
		Debug::message("Updating SiteConfig");
		Subsite::disable_subsite_filter(true);
		
		$check = DB::query("SELECT * FROM SiteConfig LIMIT 1")->first();
		if($check['DishImageFolderType']) {
		
			$siteconfigs = SiteConfig::get();
			Debug::message("Number of SiteConfigs: ".$siteconfigs->count());
			$defaults = Config::inst()->get('SiteConfig', 'defaults');
			foreach($siteconfigs as $siteconfig) {
				if($siteconfig->SubsiteID == 0) continue;
				Debug::message("Updating SiteConfig: $siteconfig->ID");
				$origsiteconfig = DB::query("SELECT * FROM SiteConfig WHERE ID = $siteconfig->ID")->first();
				
				$siteconfig->populateDefaults();
				
				$siteconfig->DefaultFolderType = $origsiteconfig['DishImageFolderType'];
				$siteconfig->Title = $origsiteconfig['Title'];
				$siteconfig->Tagline = '';

				$siteconfig->write();
			}
			// remove obsolete fields
			DB::query("ALTER TABLE SiteConfig DROP COLUMN DishImageFolderType");
		}
		Subsite::disable_subsite_filter(false);
	}
	
	public function update_displaytemplatepages() {
		$newclassname = "DisplayTemplatePagePortrait";
		Debug::message("Updating DisplayTemplatePages - setting ClassName $newclassname");
		DB::query("UPDATE SiteTree SET ClassName = '$newclassname' WHERE ClassName = 'DisplayTemplatePage'");
		DB::query("UPDATE SiteTree_Live SET ClassName = '$newclassname' WHERE ClassName = 'DisplayTemplatePage'");
		DB::query("UPDATE SiteTree_versions SET ClassName = '$newclassname' WHERE ClassName = 'DisplayTemplatePage'");
		Debug::message("successfull");
	}
	
	public function update_menucontainer() {
		Subsite::disable_subsite_filter(true);
		$menucontainers = DB::query("SELECT * FROM MenuContainer");
		$menucontainer_count = $menucontainers->numRecords();
		if(array_key_exists('XMLFileURL', $menucontainers->first()) && $menucontainer_count >= 1) {
			Debug::message("Creating ProccessTask XMLImportJobs from old MenuContainer settings");
			
			for($i = 0; $i < $menucontainer_count; $i++) {
				if($i == 0) {
					$menucontainer = $menucontainers->current();
				}
				else {
					$menucontainer = $menucontainers->next();
				}
				
				// MenuContainer Object
				$menucontainerobject = MenuContainer::get()->byID($menucontainer['ID']);
				
				if($menucontainer['XMLFileURL'] == '' || $menucontainerobject->Title == 'Handelsware') continue;
				
				Debug::message("Creating ProcessTask with URL: ".$menucontainer['XMLFileURL']);
				
				$importconfig = ImportMenuXmlConfig::create();
				$importconfig->URL = $menucontainer['XMLFileURL'];
				$importconfig->Title = $menucontainerobject->Title;
				$importconfig->Schedule = '0 4 * * *';
				$importconfig->Timeout = 3;
				$importconfig->ImportFunction = 'import_lz_catering';
				$importconfig->MenuContainerID = $menucontainerobject->ID;
				$importconfig->SubsiteID = $menucontainerobject->SubsiteID;
				
				// assign default import filters
				$importconfig->NutritivesMapConfigID = 1;
				$importconfig->AdditivesBlacklistConfigID = 1;
				$importconfig->AllergensBlacklistConfigID = 1;
				$importconfig->DishLabelsBlacklistConfigID = 1;
				
				$importconfig->write();
			}
			// Delete obsolete columns
			DB::query("ALTER TABLE MenuContainer DROP COLUMN XMLFileURL, DROP COLUMN XMLImportControllerClass, DROP COLUMN XMLImportController, DROP COLUMN XMLImportRunning");
			DB::query("ALTER TABLE MenuContainer_Live DROP COLUMN XMLFileURL, DROP COLUMN XMLImportControllerClass, DROP COLUMN XMLImportController, DROP COLUMN XMLImportRunning");
			DB::query("ALTER TABLE MenuContainer_versions DROP COLUMN XMLFileURL, DROP COLUMN XMLImportControllerClass, DROP COLUMN XMLImportController, DROP COLUMN XMLImportRunning");
		}
		Subsite::disable_subsite_filter(false);
	}
	
	public function update_infomessages() {
		Subsite::disable_subsite_filter(true);
		Debug::message("Updating old InfoMessages");
		// get all Subsites
		$infomessages = InfoMessage::get()->sort('ID');
		foreach($infomessages as $infomessage) {
			
			// only process old InfoMessages
			if($infomessage->ClassName != 'InfoMessage') continue;
			Debug::message("-------");
			Debug::message("InfoMessageID: $infomessage->ID");
			
			// getting TemplatePageID from db (not part of the InfoMessage any more)
			$templatepageid = DB::query("SELECT TemplatePageID FROM InfoMessage WHERE ID = $infomessage->ID LIMIT 1")->value();
			
			// get TemplatePage for InfoMessage
			$templatepage = SiteTree::get()->byID($templatepageid);
			if($templatepage && $templatepage->exists()) {
				// Create new InfoMessage based on the ClassName of the TemplatePage
				$templatepageclassname = $templatepage->ClassName;
				
				if($templatepageclassname == 'DisplayMultiTemplatePage') {
					$infomessage->delete();
					continue;
				}
				if($templatepageclassname == 'PrintHtmlTemplatePage') $templatepageclassname = 'PrintTemplatePage';
				
				$infomessageclassname = 'InfoMessage'.$templatepageclassname;

				Debug::message("Template Classname: $templatepageclassname");
				Debug::message("InfoMessage Classname: $infomessageclassname");
				$newinfomessage = $infomessageclassname::create();
				// assign ID of TemplatePage
				$newinfomessage->{$templatepage->ClassName.'ID'} = $infomessage->TemplatePageID;

				// assign all data from original infomessage
				foreach($infomessage->db() as $field => $type) {
					if($field != 'ClassName' && $field != 'ID') {
						Debug::message("Copying field $field");
						$newinfomessage->$field = $infomessage->$field;
					}
				}
				foreach($infomessage->has_one() as $field => $type) {
					$newinfomessage->{$field.'ID'} = $infomessage->{$field.'ID'};
				}
				// assign TimerRecurringDaysOfWeeks
				foreach($infomessage->TimerRecurringDaysOfWeek() as $timer) {
					$newinfomessage->TimerRecurringDaysOfWeek()->add($timer);
				}
				$newinfomessage->write();
				// assign InfoMessage to new TemplatePage
				$newtemplatepage = $templatepageclassname::get()->byID($templatepageid);
				$newtemplatepage->InfoMessages()->add($newinfomessage);

				Debug::message("Original InfoMessage ID: $infomessage->ID | New InfoMessage ID: $newinfomessage->ID");
				Debug::message("Reassigning InfoMessage for $templatepageclassname ($templatepageid): $infomessageclassname / $newinfomessage->ClassName");
				
			}
			// delete old infomessage
			$infomessage->delete();
		}
		Subsite::disable_subsite_filter(false);
		
		// Delete oobsolete columns
		$check = DB::query("SELECT * FROM InfoMessage LIMIT 1")->first();
		if(array_key_exists('TemplatePageID', $check)) {
			DB::query("ALTER TABLE InfoMessage DROP COLUMN TemplatePageID");
		}
	}
	
	public function update_printtemplatepages() {
		Debug::message("Updating PrintTemplatePages to PrintHtmlTemplatePages");
		
		$currStage = Versioned::current_stage();
		
		Subsite::disable_subsite_filter(true);
		Versioned::reading_stage('Stage');
		
		$printtemplatepages = PrintTemplatePage::get()->filter('ClassName', 'PrintTemplatePage');
		
		
		foreach($printtemplatepages as $printtemplatepage) {
			$printtemplatepagerecord = DB::query("SELECT * FROM PrintTemplatePage WHERE ID = $printtemplatepage->ID")->first();

			#Debug::show($printtemplatepagerecord);

			$newPage = $printtemplatepage->newClassInstance('PrintHtmlTemplatePage');
			$newPage->write();

			// set data from original record
			$newPage->TemplateName = $printtemplatepagerecord['TemplateName'];
			$newPage->DocumentWidth = $printtemplatepagerecord['DocumentWidth'];
			$newPage->DocumentHeight = $printtemplatepagerecord['DocumentHeight'];
			$newPage->SizeUnit = $printtemplatepagerecord['SizeUnit'];
			$newPage->HeaderImageID = $printtemplatepagerecord['HeaderImageID'];

			$newPage->write();
			$newPage->publish('Stage', 'Live');

			Debug::message("Transformed PrintTemplatePage to ClassName: $newPage->ClassName, ID: $newPage->ID");
			#Debug::show($newPage);
		}
		Subsite::disable_subsite_filter(false);
		
		Debug::message("--- Update PrintTemplatePages finished ---");
		Versioned::reading_stage($currStage);
	}
	
	public function update_exporthtmltemplatepages() {
		Subsite::disable_subsite_filter(true);
		$check = DB::query("SHOW TABLES LIKE 'ExportHTMLTemplatePage'")->value();
		if($check >= 1) {
			$templatepages = DB::query("SELECT * FROM ExportHTMLTemplatePage LEFT JOIN TemplatePage ON ExportHTMLTemplatePage.ID = TemplatePage.ID LEFT JOIN SiteTree ON ExportHTMLTemplatePage.ID = SiteTree.ID");
			
			$templatepages_count = $templatepages->numRecords();
			if($templatepages_count >= 1) {
				Debug::message("Creating ProccessTask ExportMenuHtml from old ExportHtmlTemplatePage settings");

				for($i = 0; $i < $templatepages_count; $i++) {
					if($i == 0) {
						$templatepage = $templatepages->current();
					}
					else {
						$templatepage = $templatepages->next();
					}

					Debug::message("Creating ProcessTask with URL: ".$templatepage['ExportFilename']);

					$exportconfig = ExportMenuHtmlConfig::create();

					$exportconfig->Name = $templatepage['ExportFilename'];
					$exportconfig->TemplateName = $templatepage['TemplateName'];
					$exportconfig->Path = $templatepage['ExportPath'].'/';
					$exportconfig->DatePeriod = $templatepage['DatePeriod'];
					$exportconfig->DateRange = $templatepage['DateRange'];
					$exportconfig->StartDateType = $templatepage['StartDateType'];
					$exportconfig->StartDate = $templatepage['StartDate'];
					$exportconfig->MenuContainerID = $templatepage['MenuContainerID'];
					$exportconfig->SubsiteID = $templatepage['SubsiteID'];

					$exportconfig->AllowApiAccess = 1;
					$exportconfig->Title = "Export HTML Speiseplan";
					// @toto Köln Flughafen Schedule: "0,30 6-18 * * *"
					$exportconfig->Schedule = '0 9 * * *';
					$exportconfig->Timeout = 3;

					$exportconfig->write();

					// Assign FoodCategories
					$foodcategories = DB::query("SELECT * FROM TemplatePage_FoodCategories WHERE TemplatePageID = ".$templatepage['ID']);
					foreach($foodcategories as $foodcategory) {
						$foodcategoryobject = FoodCategory::get()->byID($foodcategory['FoodCategoryID']);
						Debug::message("Adding FoodCategory $foodcategoryobject->Title");
						$exportconfig->FoodCategories()->add($foodcategoryobject);
					}

					// delete ExportHTMLTemplatePage entries
					DB::query("DELETE FROM TemplatePage WHERE ID = ".$templatepage['ID']);
					DB::query("DELETE FROM TemplatePage_Live WHERE ID = ".$templatepage['ID']);
					DB::query("DELETE FROM TemplatePage_versions WHERE RecordID = ".$templatepage['ID']);
					DB::query("DELETE FROM SiteTree WHERE ID = ".$templatepage['ID']);
					DB::query("DELETE FROM SiteTree_Live WHERE ID = ".$templatepage['ID']);
					DB::query("DELETE FROM SiteTree_versions WHERE RecordID = ".$templatepage['ID']);
					DB::query("DELETE FROM TemplatePage_FoodCategories WHERE TemplatePageID = ".$templatepage['ID']);

				}
				// Delete obsolete tables
				DB::query("DROP TABLE ExportHTMLTemplatePage");
				DB::query("DROP TABLE ExportHTMLTemplatePage_Live");
				DB::query("DROP TABLE ExportHTMLTemplatePage_versions");
			}
		}
		Subsite::disable_subsite_filter(false);
		Debug::message("--- Update ExportHTMLTemplatePages finished ---");
	}
	
	public function create_importrestaurantconfig() {
		Subsite::disable_subsite_filter(true);
		Debug::message("Start Creating ProccessTask ImportRestaurantConfigs");
		$restaurantpages = RestaurantPage::get();
		if($restaurantpages->count() >= 1) {
			Debug::message("Creating ProccessTask ImportRestaurantConfigs");
			
			foreach($restaurantpages as $restaurantpage) {
				
				// don´t create a ImportRestaurantConfig if it already exists for the current RestaurantPage
				$importconfigcheck = DB::query("SELECT COUNT(ID) FROM ImportRestaurantConfig WHERE RestaurantPageID = ".$restaurantpage->ID)->value();
				if ($importconfigcheck >= 1) continue;
				
				Debug::message("Creating ImportRestaurantConfig for Restaurant: $restaurantpage->Title ($restaurantpage->ID)");
				
				$importconfig = ImportRestaurantConfig::create();
				
				$importconfig->URL = 'https://lzcwawi.lufthansa-technik.com/SPEISEPLAN/Komponenten_XX/doc1.xml';
				$importconfig->Title = 'Import Restaurant';
				$importconfig->Schedule = '0 3 1 * *';
				$importconfig->Timeout = 5;
				$importconfig->ImportFunction = 'import_lz_catering';
				$importconfig->RestaurantPageID = $restaurantpage->ID;
				$importconfig->SubsiteID = $restaurantpage->SubsiteID;
				
				// assign default import filters
				$importconfig->NutritivesMapConfigID = 1;
				$importconfig->AdditivesBlacklistConfigID = 1;
				$importconfig->AllergensBlacklistConfigID = 1;
				$importconfig->DishLabelsBlacklistConfigID = 1;
				
				$importconfig->write();
			}
		}
		Subsite::disable_subsite_filter(false);
		Debug::message("--- Create ImportRestaurantConfigs finished ---");
	}
}
