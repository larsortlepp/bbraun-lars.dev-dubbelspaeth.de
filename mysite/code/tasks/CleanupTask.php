<?php

/**
 * The CleanupTask can be used to run a variety of 
 * cleanup functions for the system via cronjob at a fixed interval.
 * See `index()` function for details on which functions are called.
 * 
 * We need to set up a cronjob as following
 * (example that runs every day at 01:00 o´clock):
 * 
 * `1 0 * * * /your/site/folder/sake CleanupTask`
 *
 */
class CleanupTask extends Controller {
	
	private static $allowed_actions = array(
		'index'
	);
	
	/**
	 * Provide permissions for current member to run the task
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		return Permission::check("ADMIN");
	}
	
	public function init() {
		parent::init();
		// Check access permission
		// Allow access from command line and from can_run_task() function
		if(!Director::is_cli() && !$this->can_run_task()) {
			return Security::permissionFailure();
		}
	}
	
	public function index() {
		$this->delete_old_dailymenupages();
	}
	
	/**
	 * Deletes all DailyMenuPages of each Subsite that are older than 
	 * `SiteConfig->KeepDaysDailyMenuPages` days. 
	 * This will not delete any Dishes that are connected with a DailyMenuPage.
	 * 
	 * The number of days can be set in the SiteConfig of each Subsite (via CMS)
	 * If the number of days is 0 no DailyMenuPages will be deleted
	 */
	public function delete_old_dailymenupages() {
		Subsite::disable_subsite_filter(true);
		// get all Subsites
		$subsites = Subsite::get();
		foreach($subsites as $subsite) {
			// get SiteConfig of Subsite
			$siteconfig = SiteConfig::get()->filter('SubsiteID', $subsite->ID)->first();
			// skip current Subsite if 
			if($siteconfig->KeepDaysDailyMenuPages == 0) continue;
			// Calculate date for deletion KeepDaysDailyMenuPages is set to 0
			$maxDate = date('Y-m-d', strtotime('-'.$siteconfig->KeepDaysDailyMenuPages.' days'));
			// get DailyMenuPages for current subsite and calculated date
			$dailymenupages = DailyMenuPage::get()->filter(array(
				'SubsiteID' => $subsite->ID,
				'Date:LessThan' => $maxDate
			));
			// Delete DailyMenuPages (will delete from Stage and Live)
			foreach($dailymenupages as $dailymenupage) {
				// Order is important: First delete from Stage, then from Live
				$dailymenupage->deleteFromStage("Stage");
				$dailymenupage->deleteFromStage("Live");
				$dailymenupage->delete();
			}
		}
		Subsite::disable_subsite_filter(false);
	}
}
