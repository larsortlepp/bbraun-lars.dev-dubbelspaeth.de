<?php
class MenuContainer extends Page implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/menucontainer";

	private static $db = array(
		'EmptyField' => 'Boolean'
	);

	private static $has_many = array(
		'TemplatePage' => 'TemplatePage'
	);
	
	public static $belongs_many_many = array(
		'MobileTemplatePages' => 'MobileTemplatePage'
	);

	private static $allowed_children = array(
		'DailyMenuPage'
	);

	private static $default_child = 'DailyMenuPage';

	private static $can_be_root = false;

	public $customSort = array('Date', 'ASC');

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new HeaderField('MenuContainerHeader', 'Diese Datenbank enthält alle Tages-Speisepläne'), 'Title');

		// remove fields
		$f->removeFieldFromTab('Root.Main', 'Content');

		return $f;
	}

	/*
	 * Returns DailyMenuPages for the current restaurant that fall within
	 * a given date range, sorted by date.
	 *
	 * @param String $startdate Startdate in form 'YYYY-MM-DD'. If no startdate is set the current date is used by default.
	 * @param String $enddate Startdate in form 'YYYY-MM-DD' (default = null)
	 *
	 * @return DataList
	 */
	public function getDailyMenuPagesByDate($startdate = null, $enddate = null) {
		// if no startdate is set use the current date
		$startdate = $startdate ? $startdate : DateUtil::currentDate();

		return DailyMenuPage::get()->filter(array_merge(
			array('ParentID' => $this->ID),
			array('Date:GreaterThanOrEqual' => $startdate),
			$enddate ? array('Date:LessThanOrEqual' => $enddate) : array()
		))->sort('Date');
	}

	/**
	 * Return an ArrayList with the DailyMenuPages of the given date range and the given FoodCategories,
	 *
	 * DailyMenuPages are ordered by Date, ascending.
	 * Every DailyMenuPage has a DataList "FoodCategories": Call DailyMenuPage->FoodCategories()
	 * Every FoodCategory has an ArrayList "DishesForDateAndCategory" which contains the Dishes of the current Date: Call DailyMenuPage->FoodCategories->DishesForDateAndCategory()
	 *
	 * @param Array $foodCategoryIDs array with food categories to be returned
	 * @param String $startdate The start date in form 'YYYY-MM-DD'
	 * @param String $enddate The end date in form 'YYYY-MM-DD'
	 */
	public function DailyMenuPagesByCategoryAndDate($foodCategoryIDs = false, $startdate = null, $enddate = null) {
		$finalDailyMenuPages = new ArrayList();

		$dailyMenuPages = self::getDailyMenuPagesByDate($startdate, $enddate);

		if ($dailyMenuPages) {
			// get the food categories for every DailyMenuPage
			foreach($dailyMenuPages as $dailyMenuPage) {
				// Add FoodCategory Ids to final ArrayList
				$dailyMenuPage->FoodCategoryIDs = $foodCategoryIDs;
				$finalDailyMenuPages->push($dailyMenuPage);
			}
			// return the final ArrayList
			return $finalDailyMenuPages;
		}
		else {
			return false;
		}
	}

	/**
	 * Functions returns the date in german (or given) format
	 *
	 * @param String $date The Date to be formatted
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String Formatted date
	 */
	public function DateFormat($format = '%d.%m.%Y') {
		return DateUtil::dateFormat($this->Date, $format);
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_MENUCONTAINER');
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_MENUCONTAINER');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_MENUCONTAINER');
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MENUCONTAINER' => array(
				'name' => 'Kann Seitentyp "Speiseplan Datenbank" erstellen',
				'category' => 'Speiseplan Datenbank',
				'sort' => 10
			),
			'DELETE_MENUCONTAINER' => array(
				'name' => 'Kann Seitentyp "Speiseplan Datenbank" löschen',
				'category' => 'Speiseplan Datenbank',
				'sort' => 20
			)
		);
	}

}

class MenuContainer_Controller extends Page_Controller {

}
