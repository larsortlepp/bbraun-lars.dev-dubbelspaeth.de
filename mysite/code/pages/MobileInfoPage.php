<?php
class MobileInfoPage extends Page {

	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/footernavigationholder";

	public static $db = array(
		'Title_en_US' => 'Varchar(255)',
		'Content_en_US' => 'HTMLText'
	);
	
	public static $has_one = array(
		'Image' => 'Image'
	);
	
	private static $allowed_children = 'none';

	private static $can_be_root = false;

	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// get current RestaurantPage
		$restaurantPage = $this->RestaurantPage();
		
		// show english language
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		$siteConfig = $this->CurrentSiteConfig();	
		
		// add fields
		if($showLang_en_US) {
			$f->addFieldToTab('Root.Main', new TextField('Title_en_US', 'Seitenname (englisch)'));
			$f->addFieldToTab('Root.Main', new HtmlEditorField('Content_en_US', 'Inhalt (englisch)'));
		}
		
		$f->addFieldToTab('Root.Main', $uploadField = CustomUploadField::create('Image', 'Bild (optional)'), 'Content');
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default InfoMessage folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderInfoMessagePath(RestaurantPage::getNearestRestaurantPageID()));

		return $f;
	}
	
	/**
	 * Return current RestaurantPage
	 * assumption: MobileInfoPage is grandchild of RestaurantPage,
	 * so that we can get the RestaurantPage via $this->parent->parent
	 * 
	 * @return RestaurantPage If RestaurantPage exists return it as DataObject, else return false
	 */
	public function RestaurantPage() {
		return ($this->parent && $this->parent->parent) ? $this->parent->parent : false;
	}

	/**
	 * Return the Id of the current RestaurantPage
	 * 
	 * @return Integer
	 */
	public function RestaurantPageID() {
		return ($restaurantPage = $this->RestaurantPage()) ? $restaurantPage->ID : false;
	}
	
	
}

class MobileInfoPage_Controller extends Page_Controller {
}
