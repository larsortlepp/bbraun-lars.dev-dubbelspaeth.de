<?php
class MicrositeHomePage extends TemplatePage implements PermissionProvider {

	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/micrositehomepage";
	
	private static $db = array(
		'ShowMenu' => 'Boolean',
		'ShowOpeningHours' => 'Boolean'
	);
	
	private static $has_many = array(
		'InfoMessages' => 'InfoMessageMicrositeHomePage'
	);
	
	private static $defaults = array(
		'ShowInMenus' => 1
	);
	
	public static $allowed_children = false;
	
	private static $can_be_root = false;

	public function getCMSFields() {
		
		$f = parent::getCMSFields();
		
		$this->extend('updateCMSFields', $f);
		
		// add Content field
		$f->insertAfter(HTMLEditorField::create('Content', 'Inhalt'), 'URLSegment'); // insert after 'URLSegment' because we can´t insert before 'Content_en_US' as that might not exist
		
		$f->addFieldToTab('Root.Main',
			FieldGroup::create(
				CheckboxField::create(
					'ShowOpeningHours', 
					'Öffnungszeiten anzeigen'
				)
			)
				->setName('ShowOpeningHoursGroup')
				->setTitle('Öffnungszeiten')
				->setDescription('Öffnungszeiten werden unter Restaurant &gt; Öffnnungszeiten definiert.'),
			'MicrositeContentItems'
		);
		
		// SETTINGS
		// reorder fields: add fields for menu settings at the end
		$f->addFieldToTab('Root.Main', Headerfield::create('SettingsHeader', 'Speiseplan Einstellungen'));
		$f->addFieldToTab(
			'Root.Main', 
			FieldGroup::create(
				CheckboxField::create(
					'ShowMenu', 
					'ja'
				)
			)
				->setName('ShowMenuGroup')
				->setTitle('Speiseplan anzeigen')
		);
		$moveFieldNames = array(
			'MenuContainerID',
			'StartDateType',
			'StartDate',
			'DatePeriod',
			'DateRange'
		);
		foreach($moveFieldNames as $fieldName) {
			$field = $f->fieldByName('Root.Main.'.$fieldName);
			$f->removeByName($fieldName);
			$f->addFieldToTab('Root.Main', $field);
		}
		// Add FoodCategories
		// add FoodCategories
		$foodCategoriesMap = ($foodCategories = FoodCategory::get()->filter('RestaurantPageID', RestaurantPage::getNearestRestaurantPageID())) ? $foodCategories->map()->toArray() : false;
		if($foodCategoriesMap) $f->addFieldToTab('Root.Main',new CheckboxSetField('FoodCategories', 'Essenskategorien', $foodCategoriesMap));
		
		// remove fields
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItemsTitle');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItemsTitle_en_US');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItems');
		
		// PERMISSIONS
		
		// InfoMessages
		if(
			!Permission::check('VIEW_INFOMESSAGEMICROSITEHOMEPAGE') &&
			!Permission::check('EDIT_INFOMESSAGEMICROSITEHOMEPAGE') &&
			!Permission::check('CREATE_INFOMESSAGEMICROSITEHOMEPAGE') &&
			!Permission::check('DELETE_INFOMESSAGEMICROSITEHOMEPAGE')
		) {
			$f->removeFieldFromTab('Root', 'InfoMessages');
		}		

		return $f;
	}
	
	/**
	 * Returns all DailyMenuPages for the date span set in the CMS (1-n days or weeks), starting from the current day / current week
	 * Adds a String with IDs of all selected FoodCategories to every DailyMenuPage.
	 * This way we can call DailyMenuPage->FoodCategories() with the preselected FoodCategories from the CMS.
	 *
	 * If DateRange is "Wochen", a DailyMenuPage per Day will be returned, even if there is no actual Page in the SiteTree.
	 * (neccessary for consistent rendering of Data Tables)
	 *
	 * @param Integer $menuContainerID mandatory: ID of the MenuContainer the DailyMenuPages should be rendered from
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return ArrayList
	 */
	public function DailyMenuPagesForMenuContainerID($menuContainerID = null, $datePeriod = null, $dateRange = null) {
		
		// if no menuContainerID is set exit
		if(!$menuContainerID) return false;

		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange);

		// implode IDs of all selected FoodCateogiers to a comma seperated string
		$foodCategoryIDs = $this->FoodCategoryIDs();

		// return the DailyMenuPages for the date range and FoodCategories
		$menuContainer = MenuContainer::get()->byID($menuContainerID);
		$dailyMenuPages = $menuContainer->DailyMenuPagesByCategoryAndDate($foodCategoryIDs, $dateRange['StartDate'], $dateRange['EndDate']);

		return $dailyMenuPages;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_MICROSITEHOMEPAGE')) ? true : false;
		return $canCreate;
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_MICROSITEHOMEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canDelete = (Permission::check('DELETE_MICROSITEHOMEPAGE')) ? true : false;
		return $canDelete;
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MICROSITEHOMEPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite Startseite" erstellen',
				'category' => 'Microsite',
				'sort' => 510
			),
			'DELETE_MICROSITEHOMEPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite Startseite" löschen',
				'category' => 'Microsite',
				'sort' => 520
			)
		);
	}
}

class MicrositeHomePage_Controller extends TemplatePage_Controller {
}
