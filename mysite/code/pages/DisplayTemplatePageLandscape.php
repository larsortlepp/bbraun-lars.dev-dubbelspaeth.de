<?php
class DisplayTemplatePageLandscape extends DisplayTemplatePage {
	
	static $hide_ancestor = "DisplayTemplatePage"; // hide parent meta class from create dropdowns

	private static $has_many = array(
		'InfoMessages' => 'InfoMessageDisplayTemplatePageLandscape'
	);
	
}

class DisplayTemplatePageLandscape_Controller extends DisplayTemplatePage_Controller {
	
}
