<?php
class RestaurantPage extends Page implements TemplateGlobalProvider, PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/homepage";

	private static $db = array(
		'Lang_en_US' => 'Boolean',
		'OpeningHours' => 'Text',
		'Imprint' => 'HTMLText',
		'ContactCompanyName' => 'Text',
		'ContactPersonName' => 'Varchar',
		'ContactStreetAddress' => 'Text',
		'ContactPhone' => 'Varchar',
		'ContactFax' => 'Varchar',
		'ContactEmail' => 'Varchar',
		'ContactCopyrightCompany' => 'Varchar(255)'
	);

	private static $has_one = array(
		'RestaurantLogo' => 'Image'
	);

	private static $has_many = array(
		'FoodCategories' => 'FoodCategory'
	);

	private static $allowed_children = array(
		'MenuContainer',
		'TemplatePage',
		'DishVotingContainer',
		'RecipeSubmissionContainer',
		'MicrositeContainer'
	);

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// show english language
		$showLang_en_US = $this->owner->Lang_en_US;

		// rename Title's label
		$f->renameField('Title', 'Restaurantname');

		// add fields
		$f->addFieldToTab(
			'Root.Main',
			FieldGroup::create(
				new CheckboxField('Lang_en_US',  'Englische Übersetzungen verwenden')
			)->setTitle('Mehrsprachigkeit')
		);

		if($this->owner->ID) {
			
			$f->addFieldToTab('Root.Main', $uploadField = CustomUploadField::create('RestaurantLogo', 'Restaurant Logo'));
			$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
			// set folder to default RestaurantLogo folder
			$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderRestaurantLogo($this->ID));

			// Food categories
			if($this->canViewTabFoodCategories()) {
				MyGridField::create('FoodCategories', 'Essenskategorien', $this->FoodCategories())
					->setCols(array_merge(
						array('Title' => 'Titel'),
						$showLang_en_US ? array('Title_en_US' => 'Titel (englisch)') : array(),
						array('Color' => 'Farbe'),
						array('Image.CMSThumbnail' => 'Icon')
					))
					->setAllowCustomSort(true)
					->addToTab($f, 'FoodCategories');
				$f->fieldByName('Root.FoodCategories')->setTitle('Essenskategorien');
			}

			// Add tab to edit Dishes
			if($this->canViewTabDishes()) {
				$query = $this->DishesGridFieldQuery();
				MyGridField::create('Dishes', $this->DishesGridFieldTitle(), $query)
					->setCols($this->DishesGridFieldCols())
					->setRowsPerPage($this->DishesGridFieldRowsPerPage())
					->addToTab($f, 'Gerichte');
			}
		}

		if($this->canViewTabOpeningHours()) {
			$f->addFieldToTab('Root.OpeningHours', new TextareaField('OpeningHours', 'Öffnungszeiten'));
			$f->fieldByName('Root.OpeningHours')->setTitle('Öffnungszeiten');
		}
		
		// CONTACT DATA
		$f->addFieldToTab('Root.ContactData', TextareaField::create('ContactCompanyName', 'Restaurant Bezeichnung'));
		$f->addFieldToTab('Root.ContactData', TextareaField::create('ContactStreetAddress', 'Adresse')->setDescription('Straße, Hausnummer, Postleitzahl und Ort'));
		$f->addFieldToTab('Root.ContactData', TextField::create('ContactPersonName', 'Name Ansprechpartner'));
		$f->addFieldToTab('Root.ContactData', TextField::create('ContactPhone', 'Telefon'));
		$f->addFieldToTab('Root.ContactData', TextField::create('ContactFax', 'Fax'));
		$f->addFieldToTab('Root.ContactData', EmailField::create('ContactEmail', 'E-Mail'));
		$f->addFieldToTab('Root.ContactData', TextField::create('ContactCopyrightCompany', 'Firmenbezeichnung')->setDescription('Für Copyright Vermerk'));
		$f->fieldByName('Root.ContactData')->setTitle('Kontakt');
		
		// can NOT view and NOT edit --> remove tab
		if(!Permission::check('RESTAURANTPAGE_VIEW_CONTACTDATA') && !Permission::check('RESTAURANTPAGE_EDIT_CONTACTDATA')) {
			$f->removeByName('ContactData');
		}
		// can NOT edit but view -> readonly fields
		else if(!Permission::check('RESTAURANTPAGE_EDIT_CONTACTDATA') && Permission::check('RESTAURANTPAGE_VIEW_CONTACTDATA')) {
			$f->replaceField('ContactCompanyName', $f->fieldByName('Root.ContactData.ContactCompanyName')->performReadonlyTransformation());
			$f->replaceField('ContactPersonName', $f->fieldByName('Root.ContactData.ContactPersonName')->performReadonlyTransformation());
			$f->replaceField('ContactStreetAddress', $f->fieldByName('Root.ContactData.ContactStreetAddress')->performReadonlyTransformation());
			$f->replaceField('ContactPhone', $f->fieldByName('Root.ContactData.ContactPhone')->performReadonlyTransformation());
			$f->replaceField('ContactFax', $f->fieldByName('Root.ContactData.ContactFax')->performReadonlyTransformation());
			$f->replaceField('ContactEmail', $f->fieldByName('Root.ContactData.ContactEmail')->performReadonlyTransformation());
		}
		
		if($this->canViewTabImprint()) {
			$f->addFieldToTab('Root.Imprint', new HtmlEditorField('Imprint', 'Impressum'));
			$f->fieldByName('Root.Imprint')->setTitle('Impressum');
		}

		// remove fields
		$f->removeFieldFromTab('Root.Main', 'Content');
		
		// Add custom extension class that can be used to modify CMS fields 
		// AFTER getCMSFields() is called
		// note: updateCMSFields() is always called BEFORE getCMSFields()
		$this->extend('onAfterUpdateCMSFields', $f);

		return $f;
	}
	
	/**
	 * Functions that modify 'Dishes' GridField behaviour
	 * (can be extended by 'updateDishesGridField...()' functions in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateDishesGridFieldAutoCompleterResultFormat(String &$string) {
	 *     $string = '$Description.RAW ($ID)';
	 * }
	 * 
	 */
	public function DishesGridFieldQuery() {
		$query = Dish::get()
			->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			->leftJoin('FoodCategory', 'FoodCategory.ID=RestaurantDish.FoodCategoryID')
			->sort('FoodCategory.SortOrder ASC, Dish.Description ASC');
		$this->extend('updateDishesGridFieldQuery', $query);
		return $query;
	}
	
	public function DishesGridFieldTitle() {
		$string = 'Alle Gerichte';
		$this->extend('updateDishesGridFieldTitle', $string);
		return $string;
	}
	
	public function DishesGridFieldCols() {
		$cols = array(
			'FoodCategoryTitle' => 'Essenskategorie',
			'Image_CMSThumbnail' => 'Foto',
			'DescriptionGridField' => 'Gericht',
			'PriceNice' => 'Preis',
			'PriceExternNice' => 'Preis extern',
			'DishLabelImagesCMS' => 'Kennzeichnungen',
			'ShowOnTemplatePages' => 'Ausgabe'
		);
		// Show Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->ShowDishImportID) {
			$cols = array_slice($cols, 0, 1, true) +
				array('ImportID' => 'ID') +
				array_slice($cols, 2, count($cols) - 1, true);
		}
		$this->extend('updateDishesGridFieldCols', $cols);
		return $cols;
	}
	
	public function DishesGridFieldRowsPerPage() {
		$integer = 25;
		$this->extend('updateDishesGridFieldRowsPerPage', $integer);
		return $integer;
	}

	/**
	 * Returns the related RestaurantPage
	 * Note: 
	 * All contents must be either
	 * - placed as children of a RestaurantPage
	 * - or implement the ControllerRestaurantPageRelation interface 
	 *   (e.g. DataObjects or Controllers outside of the SiteTree)
	 *
	 * @return RestaurantPage The nearest ancestor RestaurantPage or
	 *                        the RestaurantPage that was assigned through
	 *                        ControllerRestaurantPageRelation interface
	 *                        or False if no RestaurantPage was found
	 */
	public static function getNearestRestaurantPage() {
		/*
		 * If we get called from outside the "cms" module (eg. from a
		 * template page action), there is no currentPage() method and
		 * we need some extra work.
		 */
		$ctrlr = Controller::curr();
		$page = false;
		if (method_exists($ctrlr, "currentPage")) {
			$page = $ctrlr->currentPage();			
		}
		if (!$page && $ctrlr->dataRecord && $ctrlr->dataRecord->ID) {
			$page = Page::get()->byID($ctrlr->dataRecord->ID);
		}
		if (!$page && method_exists($ctrlr, "RestaurantPage") ) {
			// generic method for Controllers independet of SiteTree
			// where we can´t get the RestaurantPage from parent page relation
			// return RestaurantPage from a custom function
			// based on interface ControllerRestaurantPageRelation
			$page = $ctrlr->RestaurantPage();
		}
		if (!$page)	return false;
		
		while ($page->ClassName != 'RestaurantPage' && $page->ParentID != 0) {
			$page = DataObject::get_by_id('SiteTree', $page->ParentID);
		}
		return $page->ClassName == 'RestaurantPage' ? $page : false;
	}

	/**
	 * Returns the ID of the related RestaurantPage
	 *
	 * @return Integer ID of the related RestaurantPage or False
	 *                 if no RestaurantPage was found
	 */
	public static function getNearestRestaurantPageID() {
		$restaurantPage =  self::getNearestRestaurantPage();
		return $restaurantPage && $restaurantPage->exists() ? $restaurantPage->ID : false;
	}

	/**
	 * Add $RestaurantPage to all SSViewers
	 */
	public static function get_template_global_variables() {
		return array(
			'RestaurantPage' => 'getNearestRestaurantPage',
		);
	}
	
	/**
	 * Returns all RestaurantPages of the current Subsite 
	 * (or of all Subsites if we are on the "main site")
	 * that the user can access
	 * 
	 * @return ArrayList
	 */
	public static function AllowedRestaurantPages() {
		/*
		 * If we can't determine a subsite, we're in the "main site" and must
		 * disable the subsite filter which otherwise automatically appends
		 * a zero SubsiteID to RestaurantPage::get() below
		 */
		if (!Subsite::currentSubsite())
			Subsite::disable_subsite_filter();

		// RestaurantPage filter
		$subsiteRestaurantPages = RestaurantPage::get();
		$viewableRestaurantPages = new ArrayList();
		if ($subsiteRestaurantPages) {
			foreach ($subsiteRestaurantPages as $subsiteRestaurantPage) {
				if ($subsiteRestaurantPage->can("Edit"))
					$viewableRestaurantPages->push($subsiteRestaurantPage);
			}
		}
		return $viewableRestaurantPages;
	}
	
	/**
	 * Duplicate all has_many relations
	 */
	public function duplicate() {
		$clone = parent::duplicate();
		// uses function of class DuplicateHasManyExtension
		$clone = $this->duplicateHasManyRelations($this, $clone);
		return $clone;
	}
	
	/**
	 * Implements custom canView permissions for tab Foodcategories
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabFoodCategories($member = null) {
		$canView = (Permission::check('RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES')) ? true : false;
		return $canView;
	}
	
	/**
	 * Implements custom canView permissions for tab Dishes
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabDishes($member = null) {
		$canView = (Permission::check('RESTAURANTPAGE_VIEW_TAB_DISHES')) ? true : false;
		return $canView;
	}
	
	/**
	 * Implements custom canView permissions for tab OpeningHours
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabOpeningHours($member = null) {
		$canView = (Permission::check('RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS')) ? true : false;
		return $canView;
	}
	
	/**
	 * Implements custom canView permissions for tab Imprint
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabImprint($member = null) {
		$canView = (Permission::check('RESTAURANTPAGE_VIEW_TAB_IMPRINT')) ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES' => array(
				'name' => 'Kann Tab Essenskategorien sehen',
				'category' => 'Restaurant',
				'sort' => 10
			),
			'RESTAURANTPAGE_VIEW_TAB_DISHES' => array(
				'name' => 'Kann Tab Gerichte sehen',
				'category' => 'Restaurant',
				'sort' => 20
			),
			'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS' => array(
				'name' => 'Kann Tab Öffnungszeiten sehen',
				'category' => 'Restaurant',
				'sort' => 30
			),
			'RESTAURANTPAGE_VIEW_TAB_IMPRINT' => array(
				'name' => 'Kann Tab Impressum sehen',
				'category' => 'Restaurant',
				'sort' => 40
			),
			'RESTAURANTPAGE_VIEW_CONTACTDATA' => array(
				'name' => 'Kann Kontaktdaten sehen',
				'category' => 'Restaurant',
				'sort' => 50
			),
			'RESTAURANTPAGE_EDIT_CONTACTDATA' => array(
				'name' => 'Kann Kontaktdaten bearbeiten',
				'category' => 'Restaurant',
				'sort' => 60
			)
			// :TODO: canView/canEdit field Title, MenuTitle, URLSegment, Lang_en_US, RestaurantLogo
		);
	}
}

class RestaurantPage_Controller extends Page_Controller {

}
