<?php

/**
 * DishVotingPage
 *
 * Object for voting multiple dishes (like a poll)
 *
 */
class DishVotingPage extends Page implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/dishvotingpage";

	private static $db = array(
		'VotingDate' => 'Date',
		'Publish' => 'Boolean'
	);

	private static $has_many = array(
		'DishVotingItems' => 'DishVotingItem'
	);

	private static $defaults = array(
		'ShowInMenus' => false,
		'ShowInSearch' => false
	);

	private static $allowed_children = 'none';

	private static $can_be_root = false;

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new HeaderField('DishVotingPage', 'Voting Gerichte'), 'Title');

		$votingDateField = new DateField('VotingDate', 'Voting für Datum');
		$votingDateField->setLocale(i18n::get_locale());
		$votingDateField->setConfig('showcalendar', true);
		$f->addFieldToTab('Root.Main', $votingDateField, 'Content');

		$f->addFieldToTab('Root.Main', new CheckboxField('Publish', 'Voting aktivieren'));

		// Add Tab with DishVotingItems - only add it if we are working with a real object ($this->ID is set)
		if($this->ID) {
			MyGridField::create('DishVotingItems', 'Voting Gerichte', $this->DishVotingItems(), 'record')
				->setCols(array(
					'DishDescription' => 'Gericht',
					'VotesPercent' => 'Prozent',
					'Votes' => 'Stimmen'
				))
				->setRowsPerPage(50)
				->setAllowCustomSort(true)
				->addToTab($f, 'DishVotingItems');
			$f->fieldByName('Root.DishVotingItems')->setTitle('Votings Gerichte');
		}

		// remove fields
		$f->removeFieldFromTab('Root.Main', 'Title');
		$f->removeFieldFromTab('Root.Main', 'MenuTitle');
		$f->removeFieldFromTab('Root.Main', 'Content');

		return $f;
	}

	public function VotingDateFormat($format = '%d.%m.%Y') {
		return DateUtil::dateFormat($this->VotingDate, $format);
	}

	/**
	 * Set static Title an Menu Title: "Speiseplan 26.07.2012"
	 * Generate unique URLSegment: "speiseplan-2012-07-26"
	 *
	 */
	public function onBeforeWrite() {

		if($this->ID) {
			
			// parent ID (may not be set if not published)
			$parentID = ($this->parent) ? $this->parent->ID : false;

			// generate Title
			$date = $this->VotingDate ? $this->VotingDate : 'Neu';
			$dateNice = $this->VotingDate ? DateUtil::dateFormat($this->VotingDate, '%d.%m.%Y') : 'Neu';

			// set Title and MenuTitle
			$this->Title = $dateNice;
			$this->MenuTitle = $dateNice;

			// generate URLSegment from title
			$urlSegment = SiteTree::generateURLSegment($date);

			// check if DishVotingPage with this URLSegment exists
			// add number to the end of URLSegment end recheck if URLSegment still exists
			$count = 1;
			$stage = (Versioned::current_stage() == 'Live') ? '_Live' : '';
			while (DishVotingPage::get()->filter(array(
				'ParentID' => $parentID,
				'URLSegment' => $urlSegment
			))->exclude(array(
				'ID' => $this->ID
			))->count() > 0) {
				$urlParts = explode('-', $urlSegment);
				// check if we already added a sequence number
				if(strlen($urlParts[count($urlParts)-1]) != 2 && $urlParts[count($urlParts)-1] != 'neu') {
					unset($urlParts[count($urlParts)-1]);
					$urlSegment = implode('-', $urlParts);
				}
				$urlSegment = $urlSegment . '-' . $count;
				$count++;
			}

			// add URLSegment to Object
			$this->URLSegment = $urlSegment;
		}

		parent::onBeforeWrite();
	}

	public function getDishVotesTotal() {
		$total = 0;
		$dishVotingItems = $this->DishVotingItems();
		if($dishVotingItems) {
			foreach($dishVotingItems as $item) {
				$total += $item->Votes;
			}
		}
		return $total;
	}

	public function getDishVotingResults() {

		$total = $this->getDishVotesTotal();
		$results = false;

		$dishVotingItems = $this->DishVotingItems();
		if($dishVotingItems) {
			$results = 'Anzahl Stimmen gesamt: $total';
			foreach($dishVotingItems as $item) {

				$dish = $item->Dish();
				$results .= $dish->Description.': '.(round($item->Votes/$total*100)).'% <br>';
			}
		}

		return $results ? $results : 'Es liegen noch keine Abstimmungsdaten vor';
	}

	public function DishVotingItemsSorted($fieldname = 'Votes', $direction = 'DESC') {
		return $this->DishVotingItems(null, $fieldname.' '.$direction);
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = (Permission::check('DELETE_DISHVOTINGPAGE')) ? true : false;
		return $canView;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canView = (Permission::check('DELETE_DISHVOTINGPAGE')) ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'DELETE_DISHVOTINGPAGE' => array(
				'name' => 'Kann Seitentyp "Voting Gerichte" löschen',
				'category' => 'Voting Gerichte',
				'sort' => 10
			)
		);
	}
}

class DishVotingPage_Controller extends Page_Controller {

}
