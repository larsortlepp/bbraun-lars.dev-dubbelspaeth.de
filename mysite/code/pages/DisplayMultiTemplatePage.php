<?php
class DisplayMultiTemplatePage extends DisplayTemplatePage {

	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/displaytemplatepage";

	public static $db = array(
	);

	public static $has_many = array(
		'DisplayTemplateObjects' => 'DisplayTemplateObject'
	);

	protected static function map_days_of_week()
	{
		return RecurringDayOfWeek::get()->sort('Value ASC')->map('ID', 'Skey')->toArray();
	}

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// remove fields (that are used in DisplayTemplateObject)
		$f->removeFieldFromTab('Root.Main', 'FootNote');
		$f->removeFieldFromTab('Root.Main', 'MenuContainerID');
		$f->removeFieldFromTab('Root.Main', 'StartDateType');
		$f->removeFieldFromTab('Root.Main', 'StartDate');
		$f->removeFieldFromTab('Root.Main', 'DatePeriod');
		$f->removeFieldFromTab('Root.Main', 'DateRange');

		$f->removeFieldFromTab('Root.Main', 'DisplayTemplatePage_WeeklyMenuTableTemplate');
		$f->removeFieldFromTab('Root.Main', 'FoodCategories');

		// Timing
		$f->removeFieldFromTab('Root.Main', 'TimingHeader');

		$f->removeFieldFromTab('Root.Main', 'TimerStartTime');
		$f->removeFieldFromTab('Root.Main', 'TimerEndTime');
		$f->removeFieldFromTab('Root.Main', 'TimerStartDate');
		$f->removeFieldFromTab('Root.Main', 'TimerEndDate');

		$f->removeFieldFromTab('Root.Main', 'TimerRecurringDaysOfWeek');

		// add GridField for DisplayTemplateObjects - only add it if we are working with a real object ($this->ID is set)
		if(
			$this->ID && 
			(
				Permission::check('VIEW_DISPLAYTEMPLATEOBJECT') || 
				Permission::check('EDIT_DISPLAYTEMPLATEOBJECT') || 
				Permission::check('CREATE_DISPLAYTEMPLATEOBJECT') || 
				Permission::check('DELETE_DISPLAYTEMPLATEOBJECT')
			)
		) {
			$f->insertBefore(new Tab('DisplayTemplateObjects', 'Zeitplan Speisepläne'), 'InfoMessages', 'has_many');
			MyGridField::create('DisplayTemplateObjects', 'Template', $this->DisplayTemplateObjects())
				->setCols(array(
					'Title' => 'Titel',
					'CollapsedDateTimeGridField' => 'Anzeigedauer'
				))
				->setRowsPerPage(25)
				->setAllowCustomSort(true)
				->addToTab($f, 'DisplayTemplateObjects');
		}

		// make fields readonly if not ADMIN
		if(!Permission::check('ADMIN')) {
			$f->replaceField('Title', $f->fieldByName('Root.Main.Title')->performReadonlyTransformation());
			$f->replaceField('TemplateName', $f->fieldByName('Root.Main.TemplateName')->performReadonlyTransformation());
			$f->replaceField('ScreenRotation', $f->fieldByName('Root.Main.ScreenRotation')->performReadonlyTransformation());
			$f->replaceField('ReloadInterval', $f->fieldByName('Root.Main.ReloadInterval')->performReadonlyTransformation());
			$f->replaceField('TransitionDuration', $f->fieldByName('Root.Main.TransitionDuration')->performReadonlyTransformation());
			$f->replaceField('ShowScreenDuration', $f->fieldByName('Root.Main.ShowScreenDuration')->performReadonlyTransformation());
			$f->replaceField('TransitionEffect', $f->fieldByName('Root.Main.TransitionEffect')->performReadonlyTransformation());
		}

		return $f;
	}

	/**
	 * Only returns DisplayTemplateObjects for the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 * If preview date is given, all DisplayTemplateObjects will be returned
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return ArrayList
	 */
	public function DisplayTemplateObjectsAtDateTime($datetime = null) {
		$displaytemplateobjects = $this->DisplayTemplateObjects();
		$previewStartDate = $this->getPreviewStartDate();

		$returnDisplayInfoMessages = new ArrayList();
		foreach($displaytemplateobjects as $displaytemplateobject) {
			if($previewStartDate || $displaytemplateobject->ShowAtDateTime($datetime)) {
				$returnDisplayInfoMessages->push($displaytemplateobject);
			}
		}
		return $returnDisplayInfoMessages->count() >= 1 ? $returnDisplayInfoMessages : false;
	}

	// TIMING
	/**
	 * Functions returns true if DisplayTemplatePage (Menu) is setup to be displayed at the given date and time
	 * If no date and time is given, the current date and time will be used.
	 * If no date or time is set on DisplayTemplatePage, function will return true.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:00:30'
	 * @return Boolean	true if template is set to be displayed at the given date and time, else: false
	 */
	public function ShowAtDateTime($datetime = null) {
		$datetime = $datetime ? $datetime : date('Y-m-d H:i:s');
		$time = strtotime($datetime);
		$timeHIS = date('H:i:s', $time);
		$date = date('Y-m-d', $time);

		// check if time is set and is NOT in given time span
		if(
			$this->TimerStartTime && $this->TimerEndTime &&
			($timeHIS < $this->TimerStartTime || $timeHIS > $this->TimerEndTime)
		) {
			return false;
		}

		// check if given date is NOT in between given date span
		if(
			$this->TimerStartDate && $this->TimerEndDate &&
			($date < $this->TimerStartDate || $date > $this->TimerEndDate)

		) {
			return false;
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			// get day number from given date
			$dayNum = date('w', $time);
			// check if day number is selected for current InfoMessage
			if($recurringDays->count() >= 1 && !$recurringDays->find('Value', $dayNum)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * RenderMenu
	 * returns true if previewStartDate or a template is set for current date and time is set
	 * --> only show at set DateTime in Live mode, always show in preview mode
	 *
	 * @return Boolean
	 */
	public function RenderMenu() {
		return ($this->DisplayTemplateObjectsAtDateTime() || $this->getPreviewStartDate()) ? true : false;
	}
}

class DisplayMultiTemplatePage_Controller extends DisplayTemplatePage_Controller {

}
