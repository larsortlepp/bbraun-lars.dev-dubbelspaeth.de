<?php
class Page extends SiteTree implements PermissionProvider {

	private static $db = array(
	);

	private static $has_one = array(
	);

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add hidden Field URLSegment if it does not exist
		if(!$f->dataFieldByName('URLSegment')) $f->addFieldToTab('Root.Main', new HiddenField('URLSegment'));
		
		// show / hide tabs based on permissions
		// workaround: due to issue 1120 "Settings" and "History" Tab are not removable via php
		// @link https://github.com/silverstripe/silverstripe-cms/issues/1120
		// so we inject CSS to visually hide the tabs. They are still accessible via URL!
		if(!$this->canViewTabSettings()) Requirements::css('mysite/css/hide-settings-tab.css');
		if(!$this->canViewTabHistory()) Requirements::css('mysite/css/hide-history-tab.css');
		if(!$this->canViewTabDependent()) $f->removeFieldFromTab('Root', 'Dependent');

		return $f;
	}

	/**
	 * Returns the 2 charactes locale of the current language
	 * (e.g. 'de' for use in HTML language declaration)
	 *
	 * @return String
	 */
	public function ShortLocale() {
		return i18n::get_lang_from_locale(i18n::get_locale());
	}

	/**
	 * Returns the SiteConfig of the current Page
	 * $this->SiteConfig does not always work correctly
	 *
	 * @return SiteConfig
	 */
	public function CurrentSiteConfig() {
		return SiteConfig::current_site_config();
	}

	/**
	 * Returns the Domain URL for the current Subsite (with leading 'http://')
	 *
	 * @param Integer $subsiteID ID of the subsite to get the link for
	 * @param String $action Optional: URL Parameters
	 * @param boolean $protocol true: leading protocol (http://) will be added. default: true
	 * @param boolean $allDomains true if all domains for a subsite should be returned. default: false
	 * @return string | array
	 */
	public function SubsiteDomain($subsiteID, $action = null, $protocol = true, $allDomains = false) {
		$domain = false;
		$action = $action ? '?'.$action : '';

		Subsite::disable_subsite_filter();
		$subsite = Subsite::get()->filter("ID", $subsiteID);
		Subsite::disable_subsite_filter(false);

		// check for correct Domain names or clean up Domain
		if($subsite) {
			// return all domains for the current subsite
			if($allDomains) {
				Subsite::disable_subsite_filter();
				$subsiteDomains = SubsiteDomain::get()->filter("SubsiteID", $subsiteID);
				Subsite::disable_subsite_filter(false);
				foreach($subsiteDomains as $subsiteDomain) {
					$domain[] = $this->cleanUpURL($subsiteDomain->Domain, $protocol).urlencode($action);
				}
			}
			// return just the first domain for the current subsite
			else {
				$domain = $subsite->First()->domain();
				$domain = $this->cleanUpURL($domain, $protocol);
				$domain =  $domain ? $domain.urlencode($action) : false;
			}
		}

		return $domain;
	}

	/**
	 * cleans up the given url (wih or without leading 'http://')
	 *
	 * @param string $url The URL
	 * @param boolean $protocol true: leading protocol (http://) will be added. default: true
	 * @return string
	 */
	public function cleanUpURL($url, $protocol = true) {
		// remove 'http://' from Domain
		$url = preg_replace('#^https?://#', '', $url);

		// replace wildcard '*' with current Domain
		$url = preg_replace('/^\*/', $_SERVER['HTTP_HOST'], $url);

		// *Only* removes "intermediate" subdomains, so 'subdomain.www.domain.com' becomes 'subdomain.domain.com'
		$url = str_replace('.www.','.', $url);

		// Add leading 'http://'
		return $protocol ? 'http://'.$url : $url;
	}

	/**
	 * Returns an absolute Link to the current Page including the default URL for the current Subsite
	 *
	 * @param Integer $subsiteID ID of the subsite to get the link for
	 * @param String $action Optional: URL Parameters
	 * @return String
	 */
	public function AbsoluteSubsiteLink($subsiteID, $action = null) {
		$subsiteDomain = $this->SubsiteDomain($subsiteID);
		return $subsiteDomain.$this->Link($action);
	}

	/**
	 * Duplicate this Page and all its $has_many ans $many_many relations
	 *
	 * @return Page
	 */
	/*
	public function duplicate()
	{

		// add all relations you want to duplicate (field names of the relations)
		// e.g. public $has_many = array('Images' => 'Image');
		// --> use 'Images' in the $items_to_duplicate array below
		$items_to_duplicate = array(
			'Images'
		);

		$page = parent::duplicate();

		// duplicate has many items
		foreach ($this->has_many() as $key => $className) {
			if (in_array($key, $items_to_duplicate)) {
				foreach ($this->{$key}() as $item) {
					$newField = $item->duplicate();
					$id = get_class($this) . 'ID';
					$newField->{$id} = $page->ID;
					$newField->write();
				}
			}
		}

		// duplicate many_many items
		foreach ($this->many_many() as $key => $className) {
			if (in_array($key, $items_to_duplicate)) {
				$page->{$key}()->addMany($this->{$key}()->getIdList());
			}
		}

		return $page;
	}
	*/

	/**
	 * Delete all $has_many relations when Page is deleted completely
	 */
	/*
	public function onBeforeDelete()
	{
		// add all relations you want to delete (field names of the relations)
		// e.g. public $has_many = array('Images' => 'Image');
		// --> use 'Images' in the $items_to_delete array below
		$items_to_delete = array(
			'Images'
		);

		// check if Page still exists in live mode
		$livePage = Versioned::get_one_by_stage(get_class(), "Live", get_class()."_Live.ID = ".$this->ID);
		// check if Page still exists in stage mode
		$stagePage = Versioned::get_one_by_stage(get_class(), "Stage", get_class().".ID = ".$this->ID);

		// if Page only exists in Live OR Stage mode -> Page will be deleted comletely -> delete connected $has_many relations
		if(!($livePage && $stagePage)) {
			// delete has many items
			foreach ($this->has_many() as $key => $className) {
				if(in_array($key, $items_to_delete)) {
					if($this->{$key}()) {
						foreach ($this->{$key}() as $item) {
							$item->delete();
						}
					}
				}
			}
		}

		parent::onBeforeDelete();
	}
	*/

	/**
	 * :FIX:
	 * Allow iframes (youtube, vimeo, etc.) to be pasted into content
	 * @link http://www.silverstripe.org/general-questions/show/16438#post305472
	 */
	public function onBeforeWrite() {
		$this->Content = preg_replace('|<iframe(.*)/>|Uims', '<iframe\\1> </iframe>', $this->Content);
		parent::onBeforeWrite();
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_PAGE');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_PAGE');
	}
	
	/**
	 * Implements custom canView permissions for tab Settings
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabSettings($member = null) {
		return Permission::check('PAGE_VIEW_TAB_SETTINGS');
	}
	
	/**
	 * Implements custom canView permissions for tab History
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabHistory($member = null) {
		return Permission::check('PAGE_VIEW_TAB_HISTORY');
	}
	
	/**
	 * Implements custom canView permissions for tab Root.Dependent
	 * @param Member $member
	 * @return boolean
	 */
	public function canViewTabDependent($member = null) {
		return Permission::check('PAGE_VIEW_TAB_DEPENDENT');
	}
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'DELETE_PAGE' => array(
				'name' => 'Kann Seitentyp "Seite" löschen',
				'category' => 'Seite',
				'sort' => 40
			),
			'PAGE_VIEW_TAB_SETTINGS' => array(
				'name' => 'Kann Tab "Einstellungen" betrachten',
				'category' => 'Seite',
				'sort' => 50
			),
			'PAGE_VIEW_TAB_HISTORY' => array(
				'name' => 'Kann Tab "Historie" betrachten',
				'category' => 'Seite',
				'sort' => 60
			),
			'PAGE_VIEW_TAB_DEPENDENT' => array(
				'name' => 'Kann Tab "Abhängige Seiten" betrachten',
				'category' => 'Seite',
				'sort' => 70
			)
		);
	}
}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();

		// block these scripts in frontend
		if($this->getRequest()->getVar('Controller') != 'admin') {
			// block jquery in frontend because it is included by default in standard template
			Requirements::block(FRAMEWORK_DIR . '/thirdparty/jquery/jquery.js');
		}
	}
}
