<?php
class RecipeSubmissionContainer extends Page implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/recipesubmissioncontainer";

	private static $db = array(
	);

	private static $has_many = array(
	);

	private static $defaults = array(
		'CanViewType' => 'LoggedInUsers',
		'ShowInMenus' => false,
		'ShowInSearch' => false
	);

	private static $allowed_children = array(
		'RecipeSubmissionPage'
	);

	private static $default_child = 'RecipeSubmissionPage';

	private static $can_be_root = false;

	/*
	 * CMSFields
	 */
	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new HeaderField('Datenbank für Übermittlung Rezepte Wunschgerichte'), 'Title');

		// remove unneccessary fields
		$f->removeFieldFromTab('Root.Main', 'Content');

		return $f;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_RECIPESUBMISSIONCONTAINER');
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_RECIPESUBMISSIONCONTAINER');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_RECIPESUBMISSIONCONTAINER');
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_RECIPESUBMISSIONCONTAINER' => array(
				'name' => 'Kann Seitentyp "Wunschgerichte Datenbank" erstellen',
				'category' => 'Wunschgerichte Datenbank',
				'sort' => 10
			),
			'DELETE_RECIPESUBMISSIONCONTAINER' => array(
				'name' => 'Kann Seitentyp "Wunschgerichte Datenbank" löschen',
				'category' => 'Wunschgerichte Datenbank',
				'sort' => 20
			)
		);
	}
}
class RecipeSubmissionContainer_Controller extends Page_Controller {

}
