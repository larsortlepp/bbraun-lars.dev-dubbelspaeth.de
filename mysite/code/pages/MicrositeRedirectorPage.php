<?php
class MicrositeRedirectorPage extends RedirectorPage implements PermissionProvider {
	
	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/redirectorpage";
	
	private static $allowed_children = array(
		'*MicrositePage', // only MicrositePage, no subclasses
		'MicrositeRedirectorPage'
	);
	
	private static $can_be_root = false;
	
	public function getCMSFields() {
		
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		
		$micrositeContainer = MicrositeContainer::get()
			->filter(array(
				'ParentID' => $restaurantPage->ID
			))
			->limit(1)
			->first();
		
		$f = parent::getCMSFields();
		
		$this->extend('updateCMSFields', $f);
		
		// remove fields
		$f->removeFieldFromTab('Root.Main', 'Content_en_US');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItemsTitle');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItemsTitle_en_US');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItems');
		$f->removeFieldFromTab('Root.Main', 'MicrositeContentItems');
		
		// Limit SiteTree dropdown to pages of current Microsite
		$f->fieldByName('Root.Main.LinkToID')->setTreeBaseID($micrositeContainer->ID);
		
		return $f;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_MICROSITEREDIRECTORPAGE');
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_MICROSITEREDIRECTORPAGE');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_MICROSITEREDIRECTORPAGE');
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MICROSITEREDIRECTORPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite Weiterleitungs-Seite" erstellen',
				'category' => 'Microsite',
				'sort' => 410
			),
			'DELETE_MICROSITEREDIRECTORPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite Weiterleitungs-Seite" löschen',
				'category' => 'Microsite',
				'sort' => 420
			)
		);
	}
}

class MicrositeRedirectorPage_Controller extends RedirectorPage_Controller {

}
