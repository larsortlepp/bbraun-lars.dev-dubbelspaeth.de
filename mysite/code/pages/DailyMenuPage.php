<?php
class DailyMenuPage extends Page implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/dailymenupage";

	private static $db = array(
		'Date' => 'Date',
		'Message' => 'Text',
		'Message_en_US' => 'Text'
	);

	private static $many_many = array(
		'Dishes' => 'Dish'
	);

	private static $many_many_extraFields = array(
		'Dishes' => array(
			'SortOrder' => 'Int'
		)
	);

	private static $allowed_children = 'none';

	private static $can_be_root = false;

	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		// get current RestaurantPage
		$restaurantPage = $this->RestaurantPage();
		$restaurantPageID = $restaurantPage ? $restaurantPage->ID : false;

		// show english language
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;

		// remove fields
		$f->removeFieldFromTab('Root.Main', 'Title');
		$f->removeFieldFromTab('Root.Main', 'MenuTitle');
		$f->removeFieldFromTab('Root.Main', 'Content');

		// add fields
		$dateField = new DateField('Date', 'Datum');
		$dateField->setLocale(i18n::get_locale());
		$dateField->setConfig('showcalendar', true);
		$f->addFieldToTab('Root.Main', $dateField);

		$f->addFieldToTab('Root.Main', new TextareaField('Message', 'Hinweis (z.B. Wegen Feiertag geschlossen)'));
		if ($showLang_en_US) {
			$f->addFieldToTab('Root.Main', new TextareaField('Message_en_US', 'Hinweis (englisch)'));
		}

		// Add Print/Preview links for all available *TemplatePages
		$configuredControllerMethodOrTemplateExistsFunc = function($tmplPage) {
			return (
				method_exists(
					$tmplPage->ClassName.'_Controller',
					$tmplPage->ControllerName
				)
				||
				(
					$tmplPage->TemplateName &&
					is_file(sprintf(
						'%s/themes/%s/templates/%s.ss',
						Director::baseFolder(),
						$tmplPage->SiteConfig->Theme,
						$tmplPage->TemplateName
					))
				)
			);
		};
		$configuredTemplateExistsFunc = function($tmplPage) {
			return (
				$tmplPage->TemplateName &&
				is_file(sprintf(
					'%s/themes/%s/templates/%s.ss',
					Director::baseFolder(),
					$tmplPage->SiteConfig->Theme,
					$tmplPage->TemplateName
				))
			);
		};
		$tmplPageCategories = array(
			// Special case to support PrintTemplatePage and PrintHtmlTemplatePage
			'Print' => array(
				'tmplpages'   => $this->PrintTemplatePages(),
				'headertitle' => 'PDF drucken',
				'testfunc'    => $configuredControllerMethodOrTemplateExistsFunc,
				'cssclasses'  => 'custom-button icon-doc-3'
			),
			'Display' => array(
				'tmplpages'   => $this->DisplayTemplatePages(),
				'headertitle' => 'Vorschau Bildschirmausgabe',
				'testfunc'    => $configuredTemplateExistsFunc,
				'cssclasses'  => 'custom-button icon-monitor'
			),
			'Mobile' => array(
				'tmplpages'   => $this->MobileTemplatePages(),
				'headertitle' => 'Vorschau Mobile Ausgabe',
				'testfunc'    => $configuredTemplateExistsFunc,
				'cssclasses'  => 'custom-button icon-mobile-3'
			)
		);
		foreach ($tmplPageCategories as $tmplPageCategoryName => $tmplPageCategory) {

			if($tmplPageCategory['tmplpages']->count() >= 1) {
				$f->addfieldToTab('Root.Main', new HeaderField(
					$tmplPageCategoryName.'Header',
					$tmplPageCategory['headertitle']
				));
			}

			foreach ($tmplPageCategory['tmplpages'] as $tmplPage) {
				if ($tmplPageCategory['testfunc']($tmplPage)) {
					$f->addFieldToTab(
						'Root.Main',
						new LiteralField(
							sprintf(
								'%sLink%s',
								$tmplPageCategoryName,
								$tmplPage->URLSegment
							),
							sprintf(
								'<a href="%s" target="_blank" class="%s">%s</a>',
								$tmplPage->AbsoluteSubsiteLink(
									$this->owner->SubsiteID,
									$tmplPage->ControllerName . '?showpreview=' . $this->Date
								),
								$tmplPageCategory['cssclasses'],
								$tmplPage->MenuTitle
							)
						)
					);
				}
			}
		}

		// Add Tab with RestaurantDishes - only add it if we are working with a real object ($this->ID is set)
		if($this->ID) {
			$query = $this->DishesGridFieldQuery();
			MyGridField::create('Dishes', $this->DishesGridFieldTitle(), $query, 'relation')
				->setCols($this->DishesGridFieldCols())
				->setRowsPerPage($this->DishesGridFieldRowsPerPage())
				->setAutoCompleterPlaceholderText($this->DishesGridFieldAutoCompleterPlaceholderText())
				->setAutoCompleterSearchList($this->DishesGridFieldAutoCompleterSearchList())
				->setAutoCompleterSearchCols($this->DishesGridFieldAutoCompleterSearchCols())
				->setAutoCompleterResultsFormat($this->DishesGridFieldAutoCompleterResultFormat())
				->setAutoCompleterOnAfterAdd($this->DishesGridFieldAutoCompleterOnAfterAdd())
				->setAllowCustomSort($this->DishesGridFieldAllowCustomSort())
				->setExtraSortCols($this->DishesGridFieldExtraSortCols())
				->setReorderColumnNumber($this->DishesGridFieldReorderColumnNumber())
				->addToTab($f, 'Dishes');

			// Add Button for Dish Image syncing
			$f->addFieldToTab('Root.Dishes', new LiteralField('DishImageSyncLink', '<a href="'.$this->AbsoluteSubsiteLink($this->owner->SubsiteID, 'syncdishimages').'" target="site" class="custom-button icon-arrows-ccw ajax-dish-image-sync">Bilder Gerichte aktualisieren</a>'), 'Dishes');

			$f->fieldByName('Root.Dishes')->setTitle('Gerichte');
		}
		
		// Add custom extension class that can be used to modify CMS fields 
		// AFTER getCMSFields() is called
		// note: updateCMSFields() is always called BEFORE getCMSFields()
		$this->extend('onAfterUpdateCMSFields', $f);

		return $f;
	}
	
	/**
	 * Functions that modify 'Dishes' GridField behaviour
	 * (can be extended by 'updateDishesGridField...()' functions in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateDishesGridFieldAutoCompleterResultFormat(String &$string) {
	 *     $string = '$Description.RAW ($ID)';
	 * }
	 * 
	 */
	public function DishesGridFieldQuery() {
		$query = $this->Dishes()
			          ->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			          ->leftJoin('FoodCategory', 'FoodCategory.ID=RestaurantDish.FoodCategoryID')
			          ->where('RestaurantDish.RestaurantPageID = '.$this->owner->RestaurantPageID());
		$this->extend('updateDishesGridFieldQuery', $query);
		return $query;
	}
	
	public function DishesGridFieldTitle() {
		$string = 'Zugeordnete Gerichte';
		$this->extend('updateDishesGridFieldTitle', $string);
		return $string;
	}
	
	public function DishesGridFieldCols() {
		$cols = array(
			'FoodCategoryTitle' => 'Essenskategorie',
			'Image_CMSThumbnail' => 'Foto',
			'DescriptionGridField' => 'Gericht',
			'PriceNice' => 'Preis',
			'PriceExternNice' => 'Preis extern',
			'DishLabelImagesCMS' => 'Kennzeichnungen',
			'ShowOnTemplatePages' => 'Ausgabe'
		);
		// Show Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->ShowDishImportID) {
			$cols = array_slice($cols, 0, 1, true) +
				array('ImportID' => 'ID') +
				array_slice($cols, 2, count($cols) - 1, true);
		}
		$this->extend('updateDishesGridFieldCols', $cols);
		return $cols;
	}
	
	public function DishesGridFieldRowsPerPage() {
		$integer = 99999;
		$this->extend('updateDishesGridFieldRowsPerPage', $integer);
		return $integer;
	}
	
	public function DishesGridFieldAutoCompleterResultFormat() {
		$string = '$Description.RAW';
		// Allow searching for Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->EnableDishSearchForImportID) $string = '$ImportIDForSearch '.$string;
		$this->extend('updateDishesGridFieldAutoCompleterResultFormat', $string);
		return $string;
	}
	
	public function DishesGridFieldAutoCompleterPlaceholderText() {
		$postfix = 'eines vorhandenen Gerichts';
		$string = 'Name';
		// Allow searching for Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->EnableDishSearchForImportID) $string .= ' oder ID';
		$string .= ' '.$postfix;
		$this->extend('updateDishesGridFieldAutoCompleterPlaceholderText', $string);
		return $string;
	}
	
	public function DishesGridFieldAutoCompleterSearchList() {
		$list = Dish::get()->filter('Dish.SubsiteID', $this->owner->SubsiteID);
		$this->extend('updateDishesGridFieldAutoCompleterSearchList', $list);
		return $list;
	}
	
	public function DishesGridFieldAutoCompleterSearchCols() {
		$cols = array(
			'Description:PartialMatch', 
			'Description_en_US:PartialMatch'
		);
			// Add filter based on ImportID
		if(SiteConfig::current_site_config()->EnableDishSearchForImportID) array_push($cols, 'ImportID:ExactMatch');
		$this->extend('updateDishesGridFieldAutoCompleterSearchCols', $cols);
		return $cols;
	}
	
	public function DishesGridFieldAutoCompleterOnAfterAdd() {
		// Get ID and object of Dish that got added
		$func = function($gridField, $dataList, $addRelation) {
			// Get ID and object of Dish that got added
			$objectID = Convert::raw2sql($addRelation);
			if ($objectID && $dish = Dish::get()->byId($objectID)) {
				// Check if associated RestaurantDish exists
				$restaurantDish = $dish->getRestaurantDish();
				if(!$restaurantDish || !$restaurantDish->DishID) {
					// Not yet, so go create one...
					$restaurantDish = new RestaurantDish();
					$restaurantDish->DishID = $objectID;
					$restaurantDish->write();

					// ...and redirect to immediately edit it
					$link = Controller::join_links($gridField->Link('item'), $objectID, 'edit');
					Controller::curr()->redirect($link);
				}
			}
			return $dataList;
		};
		$this->extend('updateDishesGridFieldAutoCompleterOnAfterAdd', $func);
		return $func;
	}
	
	public function DishesGridFieldAllowCustomSort() {
		$boolean = true;
		$this->extend('updateDishesGridFieldAllowCustomSort', $boolean);
		return $boolean;
	}
	
	public function DishesGridFieldExtraSortCols() {
		$string = 'FoodCategory.SortOrder ASC';
		$this->extend('updateDishesGridFieldExtraSortCols', $string);
		return $string;
	}
	
	public function DishesGridFieldReorderColumnNumber() {
		$integer = 1;
		$this->extend('updateDishesGridFieldReorderColumnNumber', $integer);
		return $integer;
	}

	/**
	 * Return current RestaurantPage
	 * assumption: DailyMenuPage is grandchild of RestaurantPage,
	 * so that we can get the RestaurantPage via $this->parent->parent
	 * 
	 * @return RestaurantPage If RestaurantPage exists return it as DataObject, else return false
	 */
	public function RestaurantPage() {
		return ($this->parent && $this->parent->parent) ? $this->parent->parent : false;
	}

	/**
	 * Return the Id of the current RestaurantPage
	 * 
	 * @return Integer
	 */
	public function RestaurantPageID() {
		return ($restaurantPage = $this->RestaurantPage()) ? $restaurantPage->ID : false;
	}

	/**
	 * Set custom validation and required fields for CMS fields
	 * @requires DailyMenuPageValidator
	 */
	public function getCMSValidator() {
		// check if page exists on stage and / or live -> return the appropriate validator (to prevent validating fields that do not exist)
		return ($this->IsDeletedFromStage) ? (($this->ExistsOnLive) ? new DailyMenuPageValidator() : new RequiredFields()) : new DailyMenuPageValidator('Date');
	}

	/**
	 * Returns the date in the required format
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 */
	public function DateFormat($format = '%d.%m.%Y') {
		return DateUtil::dateFormat($this->Date, $format);
	}

	/**
	 * Returns the day name of $this->Date in german translation (e.g. "Montag")
	 *
	 * @return String
	 */
	public function DateDayName() {
		return DateUtil::dateFormat($this->Date, "%A");
	}

	/**
	 * Returns the day name of $this->Date in english translation (e.g. "Monday")
	 *
	 * @return String
	 */
	public function DateDayName_en_US() {
		// set locale to english to work with english date format
		DateUtil::setlocale('en_US');
		$dayName = DateUtil::dateFormat($this->Date, "%A");
		// set locale back to german
		DateUtil::setlocale();
		return $dayName;
	}

	/**
	 * Returns true if Date is current date
	 *
	 * @return Boolean
	 */
	public function IsCurrentDate() {
		return $this->Date == date('Y-m-d') ? true : false;
	}

	/**
	 * Function returns all FoodCategories that are used on Dishes of the current DailyMenuPage.
	 * If any FoodCategory IDs are requested it will return all the given FoodCategories (even if there are no Dishes connected to it on the current DailyMenuPage).
	 *
	 * @param String $foodCategoryIDs IDs of the FoodCategories that should be returned (e.g. '1, 3, 4') (default: false)
	 * @param Array $sort Field to sort the FoodCategories by. Seperate Multiple Sorts by comma (Field1, Field2). If Field is a has_one relation you can define the sorting by adding a point: 'FoodCategory.SortOrder'. (default: 'FoodCategory.SortOrder)
	 * @return ArrayList $dishes
	 */
	public function FoodCategories($foodCategoryIDs = false, $sort = array('FoodCategory.SortOrder')) {

		$foodCategoryIDs = $this->FoodCategoryIDs ? $this->FoodCategoryIDs : $foodCategoryIDs;

		$foodCategories = new ArrayList();

		$currentDate = DateUtil::currentDate();
		
		$restaurantPageID = $this->RestaurantPageID();

		// Get all requested FoodCategories of the current restaurant
		if($foodCategoryIDs) {

			$filter = array(
				'RestaurantPageID' => $restaurantPageID,
				'FoodCategory.ID' => $foodCategoryIDs
			);
			$tmpFoodCategories = FoodCategory::get()->filter($filter)->sort(implode(', ', $sort));

			// if Date is set on the current FoodCategory (Date of parent DailyMenuPage)
			// --> add it to the DataObjects
			foreach($tmpFoodCategories as $foodCategory) {
				$foodCategory->DailyMenuPageID = $this->ID;
				if($this->Date) $foodCategory->Date = $this->Date;
				$foodCategories->push($foodCategory);
			}
		}

		// Get only the FoodCategories that are used on dishes of the current page
		else {

			// get the IDs of all Foodcategories of the current DailyMenuPage ordered by FoodCategory.SortOrder and Dish.Title
			$sqlQuery = new SQLQuery();
			$sqlQuery->selectField(
				'FoodCategory.ID',
				'ID'
			);
			$sqlQuery->setFrom(array(
				'DailyMenuPage_Dishes',
				'LEFT JOIN Dish ON DailyMenuPage_Dishes.DishID = Dish.ID',
				'LEFT JOIN RestaurantDish ON RestaurantDish.DishID = Dish.ID',
				'LEFT JOIN FoodCategory ON RestaurantDish.FoodCategoryID = FoodCategory.ID'
			));
			$where = array();
			$where[] = 'FoodCategory.RestaurantPageID = '.$restaurantPageID;
			$where[] = 'DailyMenuPage_Dishes.DailyMenuPageID = '.$this->ID;

			// add FoodCategoryIDs to query
			if($foodCategoryIDs) $where[] = "FoodCategory.ID IN ($foodCategoryIDs)" ;

			$sqlQuery->setWhere($where);

			#$sqlQuery->setHaving("");
			$sqlQuery->setOrderBy(implode(', ', $sort));
			#$sqlQuery->setLimit("");
			$sqlQuery->setDistinct(true);

			// get the raw SQL
			#$rawSQL = $sqlQuery->sql();

			// execute and return a Query-object
			$result = $sqlQuery->execute();

			// create a new ArrayList with only the FoodCategories that belong to the current DailyMenuPage
			// Attention: $reslt will contain all Dishes that belong to the current DailyMenuPage
			// 'ID' will contain the ID of the FoodCategory (which may occur multiple times on multiple Dishes)
			// so we need to remove duplicate FoodCategories at the end
			foreach($result as $foodCategory) {
				if($foodCategory['ID']) {
					$foodCategory = FoodCategory::get()->byID($foodCategory['ID']);
					// Add Date of the current DailyMenuPage to the FoodCategory
					// so we can get the dishes of the current DailyMenuPage by Date
					if($this->Date) $foodCategory->Date = $this->Date;
					$foodCategories->push($foodCategory);
				}
				$foodCategories->removeDuplicates();
			}
		}

		return $foodCategories;

	}

	/**
	 * Returns the number of FoodCategories (with Dishes) and Dishes for the
	 * current template
	 *
	 * @param String $templateName Name of the template on which the
	 * FoodCategories will be rendered (e.g. 'DisplayTemplatePage')
	 * @return Array $numbers An associative array with the number of
	 * FoodCategories and Dishes: array('FoodCategories' => X, 'Dishes' => Y);
	 */
	public function FoodCategoryDishesCount($templateName = null) {
		$return = array('FoodCategories' => 0, 'Dishes' => 0);

		$foodCategories =  $this->FoodCategories();

		// check if Dishes exist for the curent FoodCategory
		foreach($foodCategories as $foodCategory) {
			$dishesForCategory = $this->DishesForCategory($foodCategory->ID, array('DailyMenuPage_Dishes.SortOrder'), $templateName);
			if($dishesForCategory && $dishesForCategory->count() >= 1) {
				$return['FoodCategories']++;
				$return['Dishes'] += intval($dishesForCategory->count());
			}
		}

		// return array with number of FoodCategories and Dishes
		return $return;
	}

	/**
	 * Returns the number of FoodCategories with Dishes for current template
	 * @return Integer Number of FoodCategories for the template
	 */
	public function FoodCategoryCount() {
		$return = $this->FoodCategoryDishesCount();
		return $return['FoodCategories'];
	}

	/**
	 * Returns the number of FoodCategories with Dishes for the PrintTemplatePage
	 * @return Integer Number of FoodCategories for the template
	 */
	public function FoodCategoryCountDisplayTemplatePage() {
		$return = $this->FoodCategoryDishesCount('DisplayTemplatePage');
		return $return['FoodCategories'];
	}

	/**
	 * Returns the number of FoodCategories with Dishes for the PrintTemplatePage
	 * @return Integer Number of FoodCategories for the template
	 */
	public function FoodCategoryCountPrintTemplatePage() {
		$return = $this->FoodCategoryDishesCount('PrintTemplatePage');
		return $return['FoodCategories'];
	}

	/**
	 * Returns the number of FoodCategories with Dishes for the PrintTemplatePage
	 * @return Integer Number of FoodCategories for the template
	 */
	public function FoodCategoryCountMobileTemplatePage() {
		$return = $this->FoodCategoryDishesCount('MobileTemplatePage');
		return $return['FoodCategories'];
	}

	/**
	 * Returns the number of Dishes with Dishes for current template
	 * @return Integer Number of FoodCategories for the template
	 */
	public function DishesCount() {
		$return = $this->FoodCategoryDishesCount();
		return $return['Dishes'];
	}

	/**
	 * Returns the number of Dishes for the PrintTemplatePage
	 * @return Integer Number of FoodCategories for the template
	 */
	public function DishesCountDisplayTemplatePage() {
		$return = $this->FoodCategoryDishesCount('DisplayTemplatePage');
		return $return['Dishes'];
	}

	/**
	 * Returns the number of Dishes for the PrintTemplatePage
	 * @return Integer Number of FoodCategories for the template
	 */
	public function DishesCountPrintTemplatePage() {
		$return = $this->FoodCategoryDishesCount('PrintTemplatePage');
		return $return['Dishes'];
	}

	/**
	 * Returns the number of Dishes for the PrintTemplatePage
	 * @return Integer Number of FoodCategories for the template
	 */
	public function DishesCountMobileTemplatePage() {
		$return = $this->FoodCategoryDishesCount('MobileTemplatePage');
		return $return['Dishes'];
	}

	/**
	 * Returns all dishes of the current DailyMenuPage and the given FoodCategory, sorted by $sort
	 *
	 * For use of Dishes that are sorted by:
	 * FoodCategory->DailyMenuPage->Dish
	 *
	 * Can be used by calling TemplatePage->FoodCategories(), FoodCategory->DailyMenuPagesForDateRange(), DailyMenuPage->DishesForCategory()
	 * Useful for creation of data tables with FoodCategories as row, and DailyMenuPages as columns.
	 *
	 * @param Int $foodCategoryID	ID of the FoodCategory (default: null)
	 * @param Array $sort			fields to sort the Dishes by (default: array('Dish.Description'))
	 * @param String $template		Template variable that is set on Dish (DisplayTemplatePage, MobileTemplatePage, PrintTemplatePage)
	 *
	 * @return ArrayList
	 */
	public function DishesForCategory($foodCategoryID = null, $sort = array('DailyMenuPage_Dishes.SortOrder'), $template = null) {

		$foodCategoryID = $this->FoodCategoryID ? $this->FoodCategoryID : $foodCategoryID;

		$dishes = new ArrayList();
		
		$restaurantPageID = $this->RestaurantPageID();

		if($foodCategoryID && $this->ID != 0) {

			// get the IDs of all Dishes of the current DailyMenuPage for the given FoodCategoryID
			$sqlQuery = new SQLQuery();
			$sqlQuery->selectField('Dish.ID', 'ID');
			$sqlQuery->setFrom(array(
				'DailyMenuPage_Dishes',
				'LEFT JOIN Dish ON DailyMenuPage_Dishes.DishID = Dish.ID',
				'LEFT JOIN RestaurantDish ON RestaurantDish.DishID = Dish.ID',
				'LEFT JOIN FoodCategory ON RestaurantDish.FoodCategoryID = FoodCategory.ID'
			));
			$where = array(
				'FoodCategory.RestaurantPageID = '.$restaurantPageID,
				'DailyMenuPage_Dishes.DailyMenuPageID = '.$this->ID,
				'FoodCategory.ID = '.$foodCategoryID
			);

			// only add dishes of the requested FoodCategory and the requested Template
			if($template != null) $where[] = 'RestaurantDish.ShowOn'.$template.' = 1';

			$sqlQuery->setWhere($where);

			#$sqlQuery->setHaving("");
			$sqlQuery->setOrderBy(implode(', ', $sort));
			#$sqlQuery->setLimit("");
			$sqlQuery->setDistinct(true);

			// get the raw SQL
			#$rawSQL = $sqlQuery->sql();

			// execute and return a Query-object
			$result = $sqlQuery->execute();

			// create a new ArrayList with only the dishes that belong to the current DailyMenuPage
			foreach($result as $tmpDish) {
				if($tmpDish['ID']) {
					$dish = Dish::get()->byID($tmpDish['ID']);
					// Add Date of the current DailyMenuPage to the FoodCategory
					// so we can get the dishes of the current DailyMenuPage by Date
					$dish->Date = $this->Date;
					$dishes->push($dish);
				}
			}
		}

		return ($foodCategoryID && $dishes->count() >= 1) ? $dishes : false;
	}

	public function DishesForCategoryAndDisplayTemplatePage($foodCategoryID = null, $sort = array('DailyMenuPage_Dishes.SortOrder')) {
		return $this->DishesForCategory($foodCategoryID, $sort, 'DisplayTemplatePage');
	}

	public function DishesForCategoryAndPrintTemplatePage($foodCategoryID = null, $sort = array('DailyMenuPage_Dishes.SortOrder')) {
		return $this->DishesForCategory($foodCategoryID, $sort, 'PrintTemplatePage');
	}

	public function DishesForCategoryAndMobileTemplatePage($foodCategoryID = null, $sort = array('DailyMenuPage_Dishes.SortOrder')) {
		return $this->DishesForCategory($foodCategoryID, $sort, 'MobileTemplatePage');
	}

	/**
	 * Returns all existing DisplayTemplatePages of the current Subsite and Restaurant
	 * that are assigned to the current MenuContainer
	 * for rendering list of a preview links
	 *
	 * @return DataList
	 */
	public function DisplayTemplatePages() {
		$restaurantPageID = $this->RestaurantPageID();		
		return DisplayTemplatePage::get()->filter(array(
			'ParentID' => $restaurantPageID
		))->filterAny(array(
			'MenuContainerID' => $this->parent->ID,
			'ClassName' => 'DisplayMultiTemplatePage'
		));
	}

	/**
	 * Returns all existing PrintTemplatePages and PrintHtmlTemplatePages
	 * of the current Subsite and Restaurant
	 * that are assigned to the current MenuContainer
	 * sorted by their SiteTree.Sort
	 * for rendering list of a print links (in CMS)
	 *
	 * @return ArrayList
	 */
	public function PrintTemplatePages() {
		$restaurantPageID = $this->RestaurantPageID();
		$PrintTemplatePages = PrintTemplatePage::get()->filter(array(
			'ParentID' => $restaurantPageID,
			'MenuContainerID' => $this->parent->ID
		));
		$PrintHtmlTemplatePages = PrintHtmlTemplatePage::get()->filter(array(
			'ParentID' => $restaurantPageID,
			'MenuContainerID' => $this->parent->ID
		));
		return MyListUtil::concatenateLists($PrintTemplatePages, $PrintHtmlTemplatePages)->sort('Sort');
	}

	/**
	 * Returns all existing MobileTemplatePages of the current Subsite and Restaurant
	 * that are assigned to the current MenuContainer
	 * for rendering list of a print links (in CMS)
	 *
	 * @return DataList
	 */
	public function MobileTemplatePages() {
		// returns MobileTemplatePages
		return MenuContainer::get()->filter(array(
			'ID' => $this->parent->ID
		))->relation('MobileTemplatePages');
	}

	/**
	 * Reset Date to null after duplicating Page
	 * For a valid database a date may exist on only one DailyMenuPage per Restaurant
	 */
	public function onAfterDuplicate() {
		// reset Date
		$this->Date = NULL;
	}

	/**
	 * Set static Title an Menu Title: "Speiseplan 26.07.2012"
	 * Generate unique URLSegment: "speiseplan-2012-07-26"
	 *
	 */
	public function onBeforeWrite() {

		if($this->ID) {

			// generate Title
			$date = $this->Date ? $this->Date : 'Neu';
			$dateNice = $this->Date ? DateUtil::dateFormat($this->Date, '%A, %d.%m.%Y') : 'Neu';

			// set Title and MenuTitle
			$this->Title = $dateNice;
			$this->MenuTitle = $dateNice;

			// generate URLSegment from title
			$urlSegment = SiteTree::generateURLSegment($date);

			// check if DailyMenuPage with this URLSegment exists
			// add number to the end of URLSegment end recheck if URLSegment still exists
			$count = 1;
			$stage = (Versioned::current_stage() == 'Live') ? '_Live' : '';
			while (DailyMenuPage::get()->filter(array(
				'ParentID' => $this->parent->ID,
				'URLSegment' => $urlSegment
			))->exclude(array(
				'ID' => $this->ID
			))->count() > 0) {
				$urlParts = explode('-', $urlSegment);
				// check if we already added a sequence number
				if(strlen($urlParts[count($urlParts)-1]) != 2 && $urlParts[count($urlParts)-1] != 'neu') {
					unset($urlParts[count($urlParts)-1]);
					$urlSegment = implode('-', $urlParts);
				}
				$urlSegment = $urlSegment . '-' . $count;
				$count++;
			}

			// add URLSegment to Object
			$this->URLSegment = $urlSegment;
		}

		parent::onBeforeWrite();
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = (Permission::check('DELETE_DAILYMENUPAGE')) ? true : false;
		return $canView;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canView = (Permission::check('DELETE_DAILYMENUPAGE')) ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'DELETE_DAILYMENUPAGE' => array(
				'name' => 'Kann Seitentyp "Speiseplan" löschen',
				'category' => 'Speiseplan',
				'sort' => 10
			)
		);
	}
}

class DailyMenuPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
		'syncdishimages'
	);

	public function init() {
		parent::init();
	}

	/**
	 * Returns this DailyMenuPage
	 *
	 * @return DataList
	 */
	public function DailyMenuPages() {
		return DailyMenuPage::get()->byID($this->ID);
	}

	/**
	 * Snyc assets folder with Dish images
	 *
	 */
	public function syncdishimages() {
		Dish::syncDishImageFolder();
	}

}

/**
 * Validation for this class
 */
class DailyMenuPageValidator extends RequiredFields {

	protected $customRequired = array();

	/**
	 * Constructor
	 */
	public function __construct() {
		$required = func_get_args();
		if(isset($required[0]) && is_array($required[0])) {
			$required = $required[0];
		}
		$required = array_merge($required, $this->customRequired);

		parent::__construct($required);
	}

	/**
	 * Custom validation functions and check for required fields are performed on formfields
	 * Before custom validation, the regular validation of the parent class is performed
	 *
	 * @param <type> $data
	 * @return boolean $valid TRUE if validation was successfull, FALSE if not
	 */
	public function php($data) {
		// start validation of all formfields via parent class RequiredFields
		$valid = parent::php($data);

		$currentPage = Controller::curr()->currentPage();

		if ($data['Date']) {
			// check for other DailyMenuPages with same date
			$dailyMenuPagesWithDate = DailyMenuPage::get()->filter(array(
				'ParentID' => $currentPage->parent->ID,
				'Date' => $data['Date'],
			))->exclude(array(
				'ID' => $currentPage->ID
			));
			if ($dailyMenuPagesWithDate->count()) {
				// Date is invalid, because it already exists
				$this->validationError('Date', 'Es existiert bereits ein Speiseplan mit diesem Datum', 'validation');
				$valid = false;
			}
		}

		return $valid;
	}
}
