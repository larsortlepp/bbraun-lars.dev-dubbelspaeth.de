<?php
class MicrositeNewsPage extends MicrositePage implements PermissionProvider {
	
	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/pressreleasepage";
	
	private static $has_many = array(
		'InfoMessages' => 'InfoMessageMicrositeNewsPage'
	);
	
	private static $allowed_children = false;
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItemsTitle');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItemsTitle_en_US');
		$f->removeFieldFromTab('Root.Main', 'MicrositeLinkItems');
		$f->removeFieldFromTab('Root.Main', 'MicrositeContentItems');
		
		// InfoMessages
		MyGridField::create('InfoMessages', 'News', $this->InfoMessages())
			->setCols(array(
				'Image_CMSThumbnail' => 'Bild',
				'Title' => 'Titel',
				'ContentGridField' => 'Inhalt',
				'CollapsedDateTimeGridField' => 'Anzeigedauer',
				'Publish.Nice' => 'Veröffentlicht'
			))
			->setRowsPerPage(25)
			->setAllowCustomSort(true)
			->addToTab($f, 'InfoMessages');
		$f->fieldByName('Root.InfoMessages')->setTitle('News');
		
		if(
			!Permission::check('VIEW_INFOMESSAGEMICROSITENEWSPAGE') &&
			!Permission::check('EDIT_INFOMESSAGEMICROSITENEWSPAGE') &&
			!Permission::check('CREATE_INFOMESSAGEMICROSITENEWSPAGE') &&
			!Permission::check('DELETE_INFOMESSAGEMICROSITENEWSPAGE')
		) {
			$f->removeFieldFromTab('Root', 'InfoMessages');
		}
		
		return $f;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_MICROSITENEWSPAGE');
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_MICROSITENEWSPAGE');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_MICROSITENEWSPAGE');
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MICROSITENEWSPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite News Seite" erstellen',
				'category' => 'Microsite',
				'sort' => 310
			),
			'DELETE_MICROSITENEWSPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite News Seite" löschen',
				'category' => 'Microsite',
				'sort' => 320
			)
		);
	}
}

class MicrositeNewsPage_Controller extends MicrositePage_Controller {

}
