<?php
class DishVotingContainer extends Page implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/dishvotingcontainer";

	private static $db = array(
	);

	private static $has_many = array(
	);

	private static $defaults = array(
		'CanViewType' => 'LoggedInUsers',
		'ShowInMenus' => false,
		'ShowInSearch' => false
	);

	private static $allowed_children = array(
		'DishVotingPage'
	);

	private static $default_child = 'DishVotingPage';

	private static $can_be_root = false;

	/*
	 * CMSFields
	 */
	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new HeaderField('Datenbank für Abstimmungen Wunschgerichte'), 'Title');

		// remove unneccessary fields
		$f->removeFieldFromTab('Root.Main', 'Content');

		return $f;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_DISHVOTINGCONTAINER');
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_DISHVOTINGCONTAINER');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_DISHVOTINGCONTAINER');
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_DISHVOTINGCONTAINER' => array(
				'name' => 'Kann Seitentyp "Voting Gerichte Datenbank" erstellen',
				'category' => 'Voting Gerichte Datenbank',
				'sort' => 10
			),
			'DELETE_DISHVOTINGCONTAINER' => array(
				'name' => 'Kann Seitentyp "Voting Gerichte Datenbank" löschen',
				'category' => 'Voting Gerichte Datenbank',
				'sort' => 20
			)
		);
	}
}
class DishVotingContainer_Controller extends Page_Controller {

}
