<?php
class DisplayTemplatePagePortrait extends DisplayTemplatePage {
	
	static $hide_ancestor = "DisplayTemplatePage"; // hide parent meta class from create dropdowns

	private static $has_many = array(
		'InfoMessages' => 'InfoMessageDisplayTemplatePagePortrait'
	);
	
}

class DisplayTemplatePagePortrait_Controller extends DisplayTemplatePage_Controller {
	
}
