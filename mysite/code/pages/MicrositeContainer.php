<?php

/**
 * Container for Microsite
 * Will always redirect to the child MicrositeHomePage
 */
class MicrositeContainer extends Page implements PermissionProvider {
	
	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/divisionpage";
	
	private static $db = array(
		'GoogleAnalyticsID' => 'Varchar(20)',
	);
	
	private static $defaults = array(
		'CanViewType' => 'Anyone'
	);
	
	private static $allowed_children = array(
		'MicrositePage',
		'MicrositeNewsPage',
		'MicrositeRedirectorPage',
		'MicrositeHomePage'
	);
	
	private static $can_be_root = false;
	

	public function getCMSFields() {
		$f = parent::getCMSFields();

		$f->removeFieldsFromTab(
			'Root.Main', 
			array(
				'Content'
			)
		);
		
		$f->addFieldToTab('Root.Main', LiteralField::create('UserMessage', '<p class="message notice">Container für Microsite. Leitet auf untergeordnete Microsite Startseite weiter.</p>'), 'Title');
		$f->addFieldToTab('Root.Main', TextField::create('GoogleAnalyticsID', 'Google Analytics Tracking ID'));
		
		// PERMISSIONS
		
		// GoogleAnalyticsID
		// can NOT view and NOT edit field --> remove field
		if(!Permission::check('MICROSITECONTAINER_VIEW_FIELD_GOOGLEANALYTICSID') && !Permission::check('MICROSITECONTAINER_EDIT_FIELD_GOOGLEANALYTICSID')) {
			$f->removeByName('GoogleAnalyticsID');
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('MICROSITECONTAINER_EDIT_FIELD_GOOGLEANALYTICSID') && Permission::check('MICROSITECONTAINER_VIEW_FIELD_GOOGLEANALYTICSID')) {
			$f->replaceField('GoogleAnalyticsID', $f->fieldByName('Root.Main.GoogleAnalyticsID')->performReadonlyTransformation());
		}
		
		return $f;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_MICROSITECONTAINER')) ? true : false;
		return $canCreate;
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_MICROSITECONTAINER')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canDelete = (Permission::check('DELETE_MICROSITECONTAINER')) ? true : false;
		return $canDelete;
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MICROSITECONTAINER' => array(
				'name' => 'Kann Seitentyp "Microsite" erstellen',
				'category' => 'Microsite',
				'sort' => 10
			),
			'DELETE_MICROSITECONTAINER' => array(
				'name' => 'Kann Seitentyp "Microsite" löschen',
				'category' => 'Microsite',
				'sort' => 20
			),
			'MICROSITECONTAINER_VIEW_FIELD_GOOGLEANALYTICSID' => array(
				'name' => 'Microsite: Kann Feld "Google Analytics ID" betrachten',
				'category' => 'Microsite',
				'sort' => 30
			),
			'MICROSITECONTAINER_EDIT_FIELD_GOOGLEANALYTICSID' => array(
				'name' => 'Microsite: Kann Feld "Google Analytics ID" bearbeiten',
				'category' => 'Microsite',
				'sort' => 40
			),
		);
	}

}

class MicrositeContainer_Controller extends Page_Controller {

	public function init() {
		parent::init();

		$micrositeHomePage = MicrositeHomePage::get()
			->filter(array(
				'ParentID' => $this->data()->ID
			))
			->limit(1)
			->first();
		if($micrositeHomePage && $micrositeHomePage->exists()) {
			$this->redirect($micrositeHomePage->Link());
		}
		return;
	}

	/**
	 * If we ever get this far, it means that the redirection failed.
	 */
	public function Content() {
		return "<p class=\"message-setupWithoutRedirect\">Es wurden noch keine Seiten angelegt</p>";
	}

}
