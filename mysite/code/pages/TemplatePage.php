<?php
class TemplatePage extends Page implements PermissionProvider {

	private static $db = array(
		'DatePeriod' => "Enum('Tage,Wochen','Tage')",
		'DateRange' => 'Int',
		'StartDateType' => "Enum('Aktuellem-Datum,Anfangsdatum','Aktuellem-Datum')",
		'StartDate' => 'Date',
		'FootNote' => 'Text'
	);

	private static $has_one = array(
		'MenuContainer' => 'MenuContainer'
	);

	private static $many_many = array(
		'FoodCategories' => 'FoodCategory',
	);

	private static $defaults = array(
		'DatePeriod' => 'Tage',
		'DateRange' => 1,
		'StartDateType' =>'Aktuellem-Datum',
		'StartDate' => null,
		'ShowInMenus' => 0,
		'ShowInSearch' => 0
	);

	private static $allowed_children = 'none';

	private static $can_be_root = false;

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// remove fields
		$f->removeFieldFromTab('Root.Main', 'MenuTitle');
		$f->removeFieldFromTab('Root.Main', 'Content');

		// add fields
		$f->addFieldToTab(
			'Root.Main',
			new DropdownField(
				'MenuContainerID',
				'Speiseplan',
				MenuContainer::get()->filter(array(
					'ParentID' => RestaurantPage::getNearestRestaurantPageID()
				))->map('ID', 'Title')
			)
		);
		$f->addFieldToTab('Root.Main', new OptionsetField('StartDateType', 'Beginn der Ausgabe ab', $this->dbObject('StartDateType')->enumValues()));
		$dateField = new DateField('StartDate', 'Anfangsdatum (optional)');
		$dateField->setLocale(i18n::get_locale());
		$dateField->setConfig('showcalendar', true);
		$f->addFieldToTab('Root.Main', $dateField);
		$f->addFieldToTab('Root.Main', new OptionsetField('DatePeriod','Einheit auszugebender Zeitraum', $this->dbObject('DatePeriod')->enumValues()));
		$f->addFieldToTab('Root.Main', new NumericField('DateRange', 'Anzahl Tage / Wochen ausgeben'));

		// add FoodCategories
		$foodCategoriesMap = ($foodCategories = FoodCategory::get()->filter('RestaurantPageID', $this->parent->ID)) ? $foodCategories->map()->toArray() : false;
		if($foodCategoriesMap) $f->addFieldToTab('Root.Main',new CheckboxSetField('FoodCategories', 'Essenskategorien', $foodCategoriesMap));

		// Add GridField for InfoMessages - remove it on subclasses if user has no permissions
		// We are implementing this here although there is no 'InfoMessage' relation on the current class
		// But it will be used on every child class - so we have to implement a 'InfoMessage' relation there
		MyGridField::create('InfoMessages', 'News', $this->InfoMessages())
			->setCols(array(
				'Image_CMSThumbnail' => 'Bild',
				'Title' => 'Titel',
				'ContentGridField' => 'Inhalt',
				'CollapsedDateTimeGridField' => 'Anzeigedauer',
				'Publish.Nice' => 'Veröffentlicht'
			))
			->setRowsPerPage(25)
			->setAllowCustomSort(true)
			->addToTab($f, 'InfoMessages');
		$f->fieldByName('Root.InfoMessages')->setTitle('News');
		
		// PERMISSIONS
		// We only apply "EDIT" permission (no "VIEW" permissions):
		// We are tranforming fields to disabled (so the user can still see tha values)
		// because removing them completely will cause errors when
		// adding new fields before a removed field (as done in sublcasses)
		
		// TemplateName has to be set on child classes
		// Footer has to be set on child classes
		
		// Title
		if(!Permission::check('TEMPLATEPAGE_EDIT_TITLE')) {
			$f->replaceField('Title', $f->fieldByName('Root.Main.Title')->performReadonlyTransformation());
		}
		
		// DateTime
		if(!Permission::check('TEMPLATEPAGE_EDIT_DATETIMEPERIOD')) {
			$f->replaceField('MenuContainerID', $f->fieldByName('Root.Main.MenuContainerID')->performReadonlyTransformation());
			$f->replaceField('StartDate', $f->fieldByName('Root.Main.StartDate')->performReadonlyTransformation());
			$f->replaceField('StartDateType', $f->fieldByName('Root.Main.StartDateType')->performDisabledTransformation()); // disabled transformation for all radio buttons, checkboxes, etc.
			$f->replaceField('DatePeriod', $f->fieldByName('Root.Main.DatePeriod')->performDisabledTransformation()); // disabled transformation for all radio buttons, checkboxes, etc.
			$f->replaceField('DateRange', $f->fieldByName('Root.Main.DateRange')->performReadonlyTransformation());
		}

		// FoodCategories
		if(!Permission::check('TEMPLATEPAGE_EDIT_FOODCATEGORIES')) {
			$f->replaceField('FoodCategories', $f->fieldByName('Root.Main.FoodCategories')->performDisabledTransformation()); // disabled transformation for all radio buttons, checkboxes, etc.
		}

		return $f;
	}

	/**
	 * Returns start date for preview if it is set via URL
	 * e.g. mydomain.com/mytemplate?showpreview=2013-11-11
	 *
	 * @return String
	 */
	public function getPreviewStartDate() {
		return (isset($_GET['showpreview'])) ? Convert::raw2sql($_GET['showpreview']) : false;
	}

	/**
	 * Returns the start and end date for the TemplatePage based on the settings in the cms
	 * 
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @param String $startDateOffset optional: Offset in relation to start date - can be used to get the date of tomorrow ($startDateOffset = '+1 day') or next week ($startDateOffset = '+1 week'). Use valid string that can be processed by strtotime function.
	 * @return Array $dates Array with 'StartDate' and 'EndDate'
	 */
	public function getDateRangeAsArray($datePeriod = null, $dateRange = null, $startDateOffset = null) {
		$dates = array();

		// if user is logged in and urlparameter 'showpreview' ist set -> use this date as start date
		$startDate = $this->getPreviewStartDate();

		if(!$startDate) {
			// set start date - depending on CMS setting: a) current date b) fix start date
			$startDate = $this->StartDateType == 'Aktuellem-Datum' ? DateUtil::currentDate() : $this->StartDate;
		}

		// get DateRange and DatePeriod from variables, otherwise from current TemplatePage
		$datePeriod = $datePeriod ? $datePeriod : $this->DatePeriod;
		$dateRange = $dateRange ? $dateRange : $this->DateRange;

		// calculate start- and end date for a) days or b) weeks
		if($datePeriod == 'Tage') {
			
			if($startDateOffset) {
				$startDate = date('Y-m-d' , strtotime("$startDate $startDateOffset"));
				// check if DailyMenuPage for that StartDate exists
				// if not: we could be at the end of the week (friday), 
				// so check if there is a DailyMenuPage at the beginning of the next week
				if(date('D', strtotime($startDate)) == 'Sat' && !DataObject::get_one('DailyMenuPage', "SubsiteID = ".$this->owner->SubsiteID." AND Date = '$startDate'")) {
					$nextWeekDateRange = DateUtil::weekDateRange(date('Y-m-d' , strtotime("$startDate +2 days"))); // add 2 days to startdate to get date of monday (assuming caluclated startdate is a saturday)
					$startDate = $nextWeekDateRange['StartDate'];
				}
			}
			
			$dateRange = DateUtil::dayDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = ($dateRange > 1) ? $dateRange['EndDate'] :  null;
		}
		else {
			$dateRange = DateUtil::weekDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = $dateRange['EndDate'];
		}
		return $dates;
	}

	/**
	 * Returns the start date for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeStartDate($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		return DateUtil::dateFormat($dateRange['StartDate'], $format);
	}

	/**
	 * Returns the end date for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeEndDate($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		return DateUtil::dateFormat($dateRange['EndDate'], $format);
	}

	/**
	 * Returns the end of the week date (Friday) for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeEndDateWeek($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		$endOfWeek = $dateRange['EndDate'];
		while(date('l', strtotime($endOfWeek)) != 'Friday' && $endOfWeek >= $dateRange['StartDate']) {
			$endOfWeek = date('Y-m-d', strtotime($endOfWeek." -1 day"));
		}
		return $endOfWeek >= $dateRange['StartDate'] ? DateUtil::dateFormat($endOfWeek, $format) : false;
	}

	/**
	 * Returns the IDs of a *TemplatePage's selected FoodCategories as array
	 *
	 * @param Integer $templatePageID optional: ID of the *TemplatePage to use
	 *
	 * @return array
	 */
	public function FoodCategoryIDs($templatePageID = null) {
		$templatePage = $templatePageID ? call_user_func($this->ClassName.'::get')->byID($templatePageID) : null;
		$foodCategories = $templatePage ? $templatePage->FoodCategories() : $this->FoodCategories();
		return $foodCategories ? array_keys($foodCategories->map("ID")->toArray()) : false;
	}

	/**
	 * Returns the HEX color code for the first FoodCategory that is selected for this Page
	 *
	 * @return String
	 */
	public function FirstCategoryColor() {
		$foodCategories = $this->FoodCategories();
		return $foodCategories->exists() ? $foodCategories->First()->Color : false;
	}

	/**
	 * Return a DataList of existing Additives
	 *
	 * @return DataList
	 */
	public function Additives() {
		return MyListUtil::ConcatenateLists(
			GlobalAdditive::get(),
			SubsiteAdditive::get()
		);
	}

	/**
	 * Return a DataList of existing Allergens
	 *
	 * @return DataList
	 */
	public function Allergens() {
		return MyListUtil::ConcatenateLists(
			GlobalAllergen::get(),
			SubsiteAllergen::get()
		);
	}

	/**
	 * Return a DataList of existing (Global|Subsite)DishLabels
	 *
	 * @return DataList
	 */
	public function DishLabels() {
		return MyListUtil::ConcatenateLists(
			GlobalDishLabel::get(),
			SubsiteDishLabel::get()
		);
	}

	/*
	 * Returns DailyMenuPages for the current restaurant that fall within
	 * a given date range, sorted by date.
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @param String $startDateOffset optional: Offset in relation to start date - can be used to get the date of tomorrow ($startDateOffset = '+1 day') or next week ($startDateOffset = '+1 week'). Use valid string that can be processed by strtotime function.
	 * @return DataList
	 */
	private function getDailyMenuPagesByDate($datePeriod = null, $dateRange = null, $startDateOffset = null) {
		$dateRangeArray = $this->getDateRangeAsArray($datePeriod, $dateRange, $startDateOffset);

		$menuContainer = $this->MenuContainer();

		return DailyMenuPage::get()->filter(array_merge(
			array('ParentID' => $menuContainer->ID),
			array('Date:GreaterThanOrEqual' => $dateRangeArray['StartDate']),
			$dateRangeArray['EndDate'] ? array('Date:LessThanOrEqual' => $dateRangeArray['EndDate']) : array()
		))->sort('Date');
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)Relations 
	 * (based on the $relationName) that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 * 
	 * $relationName has to be a class name for which 
	 * a 'Global'.$relationName and a 'Subsite'.$relationName class exists.
	 * e.g.: $relationName = 'Allergen' => 'GlobalAllergen' and 'SubsiteAllergen'
	 *
	 * GlobalRealtions come first followed by SubsiteRelations, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param String $relationName Classname that the 'Global' und 'Subsite' relation classes are generated from
	 * @param Boolean $includeSideDishes (optional) True if Allergens of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @param String $startDateOffset optional: Offset in relation to start date - can be used to get the date of tomorrow ($startDateOffset = '+1 day') or next week ($startDateOffset = '+1 week'). Use valid string that can be processed by strtotime function. 
	 * @return ArrayList
	 */
	public function UsedDishRelations($relationName, $includeSideDishes = false, $foodCategoryIDs = null, $startDateOffset = null) {
		$foodCategoryIDArray = $foodCategoryIDs ? explode(",", $foodCategoryIDs) : $this->FoodCategoryIDs();
		$dailyMenuPages = $this->getDailyMenuPagesByDate(null, null, $startDateOffset);
		if(!$foodCategoryIDArray || !$dailyMenuPages) return false;

		$usedGlobalRelations = new ArrayList();
		$usedSubsiteRelations = new ArrayList();
		foreach ($dailyMenuPages as $dailyMenuPage) {
			/*
			 * Get dishes that are used on the current DailyMenuPages and have
			 * a food category used on this *TemplatePage
			 */
			$usedDishes = $dailyMenuPage->Dishes()
			                            ->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			                            ->filter(array('FoodCategoryID' => $foodCategoryIDArray));
			$usedDishesArray = array_keys($usedDishes->map('ID')->toArray());
			
			// get SideDishes
			if($includeSideDishes) {
				$usedSideDishes = new ArrayList();
				foreach($usedDishes as $usedDish) {
					foreach($usedDish->AllSideDishes() as $sideDish) {
						$usedSideDishes->push($sideDish);
					}
				}
				$usedSideDishes->removeDuplicates();
				// add SideDishes to array of used Dishes
				$usedDishesArray = array_merge($usedDishesArray, array_keys($usedSideDishes->map('ID')));
			}
			
			// Get GlobalRelations
			$globalClassName = 'Global'.$relationName;
			$globalRelations = $globalClassName::get()
				->filter(array('Dishes.ID' => $usedDishesArray));
			// add all GlobalRelations to ArrayList - duplicates will be removed later
			foreach ($globalRelations as $additive) {
				$usedGlobalRelations->push($additive);
			}
			
			// Get SubsiteRelations
			$subsiteClassName = 'Subsite'.$relationName;
			$subsiteRelations = $subsiteClassName::get()
				->filter(array('Dishes.ID' => $usedDishesArray));
			// add all GlobalRelations to ArrayList - duplicates will be removed later
			foreach ($subsiteRelations as $additive) {
				$usedSubsiteRelations->push($additive);
			}
		}
		
		// remove duplicates and sort
		$usedGlobalRelations->removeDuplicates();
		$usedGlobalRelations = $usedGlobalRelations->sort('SortOrder');
		$usedSubsiteRelations->removeDuplicates();
		$usedSubsiteRelations = $usedSubsiteRelations->sort('SortOrder');

		return MyListUtil::concatenateLists($usedGlobalRelations, $usedSubsiteRelations);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)Additives 
	 * that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 *
	 * GlobalAdditives come first followed by SubsiteAdditives, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedAdditives($includeSideDishes = false, $foodCategoryIDs = null, $startDateOffset = null) {
		return $this->UsedDishRelations('Additive', $includeSideDishes, $foodCategoryIDs, $startDateOffset);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)Allergens 
	 * that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 *
	 * GlobalAllergens come first followed by SubsiteAllergens, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Allergens of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedAllergens($includeSideDishes = false, $foodCategoryIDs = null, $startDateOffset = null) {
		return $this->UsedDishRelations('Allergen', $includeSideDishes, $foodCategoryIDs, $startDateOffset);
	}
	
	/**
	 * Returns a concatenated ArrayList of (Global|Subsite)DishLabels 
	 * that are used by Dishes that
	 * - are used on the given DailyMenuPages and fall within the
	 *   date range set on the current *Config
	 * - belong to FoodCategories that are selected on the current *Config
	 *
	 * GlobalDishLabels come first followed by SubsiteDishLabels, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Allergens of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedDishLabels($includeSideDishes = false, $foodCategoryIDs = null, $startDateOffset = null) {
		return $this->UsedDishRelations('DishLabel', $includeSideDishes, $foodCategoryIDs, $startDateOffset);
	}

	// TIMING
	
	/**
	 * We are implementing the InfoMessages functions here
	 * although there is no $has_many 'InfoMessages' relation on the current class.
	 * But the function will be the same for every child class - so we don´ want to be repetitive.
	 * $has_many = array('InfoMessages' => 'InfoMessage') must be implemented
	 * on every child class!
	 */

	/**
	 * Return published InfoMessages
	 *
	 * @return Int
	 */
	public function InfoMessagesPublished(){
		return $this->InfoMessages('Publish = 1');
	}

	/**
	 * Return number of published InfoMessages for the current Date and Time
	 *
	 * @return Int
	 */
	public function InfoMessagesCount(){
		$im = $this->InfoMessagesAtDateTime();
		return ($im && $im->Count()) ? $im->Count() : 0;
	}

	/**
	 * Only returns InfoMessages for the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return ArrayList
	 */
	public function InfoMessagesAtDateTime($datetime = null) {
		$infomessages = $this->InfoMessagesPublished();
		$returnInfoMessages = new ArrayList();
		foreach($infomessages as $infomessage) {
			if($infomessage->ShowAtDateTime($datetime)) {
				$returnInfoMessages->push($infomessage);
			}
		}
		return $returnInfoMessages->count() >= 1 ? $returnInfoMessages : false;
	}

	/**
	 * Set MenuTitle to Title
	 */
	public function onBeforeWrite() {
		if($this->ID) {
			$this->MenuTitle = $this->Title;
			// set StartDate to null if StartDateType is 'Aktuellem-Datum'
			if($this->StartDateType == 'Aktuellem-Datum') $this->StartDate = null;
		}
		parent::onBeforeWrite();
	}
	
	/**
	 * Duplicate all has_many relations
	 */
	public function duplicate() {
		$clone = parent::duplicate();
		// uses function of class DuplicateHasManyExtension
		$clone = $this->duplicateHasManyRelations($this, $clone);
		return $clone;
	}
	
	/**
	 * Hide this wireframe class from "create new page" dialogue
	 * Must be enabled in sublcasses
	 */
	public function canCreate($member = null) {
		return false;
	}
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'TEMPLATEPAGE_EDIT_TITLE' => array(
				'name' => 'Kann Feld Titel bearbeiten',
				'category' => 'Ausgabe Templates',
				'sort' => 10
			),
			'TEMPLATEPAGE_EDIT_TEMPLATENAME' => array(
				'name' => 'Kann Feld Template Name bearbeiten',
				'category' => 'Ausgabe Templates',
				'sort' => 20
			),
			'TEMPLATEPAGE_EDIT_DATETIMEPERIOD' => array(
				'name' => 'Kann Felder Ausgabezeitraum bearbeiten',
				'category' => 'Ausgabe Templates',
				'sort' => 30
			),
			'TEMPLATEPAGE_EDIT_FOODCATEGORIES' => array(
				'name' => 'Kann Essenskategorien bearbeiten',
				'category' => 'Ausgabe Templates',
				'sort' => 40
			),
			'TEMPLATEPAGE_EDIT_FOOTER' => array(
				'name' => 'Kann Feld Fußzeile bearbeiten',
				'category' => 'Ausgabe Templates',
				'sort' => 50
			)
		);
	}
}

class TemplatePage_Controller extends Page_Controller {

	private static $allowed_actions = array(
	);
	
	/**
	 * Returns the DailyMenuPages shiftet by +1 day in the future,
	 * using the Date settings of the current TemplatePage that are set in the CMS.
	 * 
	 * If no next day exists, function will try to return the DailyMenuPage beginning from monday of the following week.
	 * (E.g. if current date is friday -> DailyMenuPageNextDay() will be monday)
	 * 
	 * @return ArrayList
	 */
	public function DailyMenuPageNextDay() {
		return $this->DailyMenuPagesForDateOffset("+1 day");
	}
	
	/**
	 * Returns the DailyMenuPages shiftet by $startDateOffset in the future,
	 * using the Date settings of the current TemplatePage that are set in the CMS.
	 * 
	 * If no next day exists, function will try to return the DailyMenuPage beginning from monday of the following week.
	 * (E.g. if current date is friday -> DailyMenuPageNextDay() will be monday)
	 * 
	 * @param String $startDateOffset Offset in relation to start date - can be used to get the date of tomorrow ($startDateOffset = '+1 day') or next week ($startDateOffset = '+1 week'). Use valid string that can be processed by strtotime function.
	 * @return ArrayList
	 */
	public function DailyMenuPagesForDateOffset($startDateOffset) {
		return $this->DailyMenuPages(null, null, $startDateOffset);
	}

	/**
	 * Returns all DailyMenuPages for the date span set in the CMS (1-n days or weeks), starting from the current day / current week
	 * Adds a String with IDs of all selected FoodCategories to every DailyMenuPage.
	 * This way we can call DailyMenuPage->FoodCategories() with the preselected FoodCategories from the CMS.
	 * 
	 * If DateRange is "Wochen", a DailyMenuPage per Day will be returned, even if there is no actual Page in the SiteTree.
	 * (neccessary for consistent rendering of Data Tables)
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @param String $startDateOffset optional: Offset in relation to start date - can be used to get the date of tomorrow ($startDateOffset = '+1 day') or next week ($startDateOffset = '+1 week'). Use valid string that can be processed by strtotime function.
	 * @return ArrayList  
	 */
	public function DailyMenuPages($datePeriod = null, $dateRange = null, $startDateOffset = null) {

		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange, $startDateOffset);

		// implode IDs of all selected FoodCateogiers to a comma seperated string
		$foodCategoryIDs = $this->FoodCategoryIDs();

		// return the DailyMenuPages for the date range and FoodCategories
		$menuContainer = $this->MenuContainer();
		$dailyMenuPages = $menuContainer->DailyMenuPagesByCategoryAndDate($foodCategoryIDs, $dateRange['StartDate'], $dateRange['EndDate']);

		return $dailyMenuPages;
	}

	public function HasDailyMenuPagesForWeeklyTable($datePeriod = 'Woche', $dateRange = 1) {
		return TemplatePage_Controller::DailyMenuPagesForWeeklyTable($datePeriod, $dateRange);
	}

	public function DailyMenuPagesForWeeklyTable($datePeriod = null, $dateRange = null) {

		$weeklyMenuTableTemplate = $this->WeeklyMenuTableTemplate();
		$weeklyMenuTableTemplateID = $weeklyMenuTableTemplate ? $weeklyMenuTableTemplate->ID : null;

		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange);

		// implode IDs of all selected FoodCateogiers to a comma seperated string
		$foodCategoryIDs = $this->FoodCategoryIDs($weeklyMenuTableTemplateID);

		// return the DailyMenuPages for the date range and FoodCategories
		$menuContainer = $this->MenuContainer();
		$dailyMenuPages = $menuContainer->DailyMenuPagesByCategoryAndDate($foodCategoryIDs, $dateRange['StartDate'], $dateRange['EndDate']);

		return $dailyMenuPages;
	}

	/**
	 * Returns all selected FoodCategories for Rendering of Data grouped by FoodCategory
	 *
	 * For use of Dishes that are sorted by:
	 * FoodCategory->DailyMenuPage->Dish
	 *
	 * Can be used by calling TemplatePage->FoodCategories(), FoodCategory->DailyMenuPagesForDateRange(), DailyMenuPage->DishesForCategory()
	 * Useful for creation of data tables with FoodCategories as row, and DailyMenuPages as columns.
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return ArrayList
	 */
	public function FoodCategories($datePeriod = null, $dateRange = null) {
		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange);

		// get selected FoodCategories
		$foodCategoryIDs = $this->FoodCategoryIDs();

		// get the FoodCategories for the date range and FoodCategory IDs
		$foodCategories = FoodCategory::get()->filter(array_merge(
			array('RestaurantPageID' => $this->parent->ID),
			$foodCategoryIDs ? array('FoodCategory.ID' => $foodCategoryIDs) : array()
		));

		// Add start and enddate to each foodcategory
		// -> Hook to get the dailymenupages from the foodcategory
		$arrayList = new ArrayList();
		foreach ($foodCategories as $foodCategory) {
			$foodCategory->StartDate = $dateRange['StartDate'];
			$foodCategory->EndDate   = $dateRange['EndDate'];
			$arrayList->push($foodCategory);
		}

		return $arrayList;
	}

	public function FoodCategoriesForWeeklyTable($datePeriod = null, $dateRange = null) {
		$weeklyMenuTableTemplate = $this->WeeklyMenuTableTemplate();
		$weeklyMenuTableTemplateID = $weeklyMenuTableTemplate ? $weeklyMenuTableTemplate->ID : null;

		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange);

		// get selected FoodCategories for weeklyMenuTableTemplate
		$foodCategoryIDs = $this->FoodCategoryIDs($weeklyMenuTableTemplateID);

		// get the FoodCategories for the date range and FoodCategories
		$foodCategories = FoodCategory::get()->filter(array_merge(
			array('RestaurantPageID' => $this->parent->ID),
			$foodCategoryIDs ? array('FoodCategory.ID' => $foodCategoryIDs) : array()
		));

		// Add start and enddate to each foodcategory
		// -> Hook to get the dailymenupages from the foodcategory
		$arrayList = new ArrayList();
		foreach ($foodCategories as $foodCategory) {
			$foodCategory->StartDate = $dateRange['StartDate'];
			$foodCategory->EndDate   = $dateRange['EndDate'];
			$arrayList->push($foodCategory);
		}

		return $arrayList;
	}
}
