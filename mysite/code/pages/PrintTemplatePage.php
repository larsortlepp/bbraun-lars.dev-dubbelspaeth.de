<?php
class PrintTemplatePage extends TemplatePage implements PermissionProvider {

	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/printtemplatepage";

	public static $db = array(
		'ControllerName' => 'Varchar(255)'
	);

	public static $has_one = array(
		'WeeklyTableHeaderImage' => 'Image'
	);
	
	private static $has_many = array(
		'InfoMessages' => 'InfoMessagePrintTemplatePage'
	);

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new TextField('ControllerName', 'Controller Name (print_dina5)'), 'MenuContainerID');
		// Image
		$f->addFieldToTab(
			'Root.Main',
			CustomUploadField::create(
				'WeeklyTableHeaderImage',
				'Bild'
			)
				->setDescription('jpg, gif, png')
				->setAllowedExtensions(array('jpg', 'gif', 'png'))
				->setFolderName(SiteConfig::current_site_config()->DefaultFolderPrintPath(RestaurantPage::getNearestRestaurantPageID())) // set folder to default dish image folder
		);
		$f->addFieldToTab('Root.Main', new TextareaField('FootNote', 'Text für Fußzeile'));

		// add print link if the method exists
		$f->addFieldToTab('Root.Main', new HeaderField('PrintHeader', 'Druckausgabe'), 'Title');
		if(method_exists($this->ClassName.'_Controller', $this->ControllerName)) {
			$f->addFieldToTab('Root.Main', new LiteralField('PrintLink', '<a href="'.$this->AbsoluteSubsiteLink($this->owner->SubsiteID).'" target="_blank" class="custom-button icon-doc-3">PDF drucken</a>'), 'Title');
		}
		else {
			$f->addFieldToTab('Root.Main', new LiteralField('ControllerWarning','<strong>Der angegebene Controller (Controller Name) existiert nicht.</strong> PDF Generierung nicht möglich.'), 'Title');
		}
		
		// PERMISSIONS
		
		if(
			!Permission::check('VIEW_INFOMESSAGEPRINTTEMPLATEPAGE') &&
			!Permission::check('EDIT_INFOMESSAGEPRINTTEMPLATEPAGE') &&
			!Permission::check('CREATE_INFOMESSAGEPRINTTEMPLATEPAGE') &&
			!Permission::check('DELETE_INFOMESSAGEPRINTTEMPLATEPAGE')
		) {
			$f->removeFieldFromTab('Root', 'InfoMessages');
		}
		
		// TemplateName
		if(!Permission::check('TEMPLATEPAGE_EDIT_TEMPLATENAME')) {
			$f->replaceField('ControllerName', $f->fieldByName('Root.Main.ControllerName')->performReadonlyTransformation());
		}
		
		// Footer
		if(!Permission::check('TEMPLATEPAGE_EDIT_FOOTER')) {
			$f->replaceField('FootNote', $f->fieldByName('Root.Main.FootNote')->performReadonlyTransformation());
		}
		
		// Image
		if(!Permission::check('PRINTTEMPLATEPAGE_EDIT_IMAGE')) {
			// remove Image - otherwise ID of null will be saved - which unlinks an assigned image
			$f->removeByName('WeeklyTableHeaderImage');
		}

		return $f;
	}

	/**
	 * Save ControllerName in lowercase and url safe
	 */
	public function onBeforeWrite() {
		if($this->ID) $this->ControllerName = str_replace('-', '_', SiteTree::generateURLSegment($this->ControllerName));
		parent::onBeforeWrite();
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_PRINTTEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_PRINTTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canDelete = (Permission::check('DELETE_PRINTTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_PRINTTEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Druck Ausgabe" erstellen',
				'category' => 'Druck Ausgabe',
				'sort' => 10
			),
			'DELETE_PRINTTEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Druck Ausgabe" löschen',
				'category' => 'Druck Ausgabe',
				'sort' => 20
			),
			'PRINTTEMPLATEPAGE_EDIT_IMAGE' => array(
				'name' => 'Kann Feld Bild bearbeiten',
				'category' => 'Druck Ausgabe',
				'sort' => 110
			)
		);
	}
}

class PrintTemplatePage_Controller extends TemplatePage_Controller {

	private static $allowed_actions = array(
		'index',
		'print_category_dina4_portrait',
		'print_category_dina4_landscape',
		'print_category_dina5',
		'print_category_dina7',
		'print_production_table',
		'print_temperature_table',
		'print_weekly_table_7days_de_de',
		'print_weekly_table_7days_en_us',
		'print_weekly_table_de_de',
		'print_weekly_table_en_us',
		'print_weekly_table_detail_7days_de_de',
		'print_weekly_table_detail_7days_en_us',
		'print_weekly_table_detail_de_de',
		'print_weekly_table_detail_en_us',
		'print_weekly_table_detail_nutritives_7days_de_de',
		'print_weekly_table_detail_nutritives_7days_en_us',
		'print_weekly_table_detail_nutritives_de_de',
		'print_weekly_table_detail_nutritives_en_us'
	);

	/**
	 * include PDF Class
	 */
	public function init() {
		parent::init();

		// include PDF class
		require_once(Director::baseFolder().'/fpdf/PDF.php');
		// include PDFTable class
		require_once(Director::baseFolder().'/fpdf/PDFTable.php');
	}

	/**
	 * Check if Member is logged in then return the generated PDF file,
	 * if user is not logged in a 404 error page is returned
	 */
	public function index() {
		if(Member::logged_in_session_exists()) {
			return $this->makePDF();
		}
		else {
			return $this->redirect('/error-404.html', 404);
		}
	}

	/**
	 * PDF generation
	 *
	 * calls the controller with "ControllerName" (as set via CMS)
	 * if no such controller exists a 404 error page is returned
	 */
	public function makePDF() {
		// use controller function as set in the CMS to render the PDF
		// note: Controller must de defined manually in this class!
		if(method_exists($this->ClassName.'_Controller', $this->ControllerName)) {
			header('Content-Type: text/html; charset=utf-8');
			return $this->{$this->ControllerName}();
		}
		else {
			return $this->redirect('/error-404.html', 404);
		}
	}

	/**
	 * PDF generation: print_category_dina4
	 *
	 * Renders a DIN A4 PDF of all Dishes of the selected FoodCategories and Date Range
	 * One DIN A4 page per category and day (containing all associated dishes)
	 *
	 * @param $cfg Array Parameters for either portrait or landscape orientation
	 */
	private function print_category_dina4($cfg) {
		$pageLeftColPosX  = $cfg['Page']['MarginLeft'] + $cfg['Page']['LeftColSmall'] + 3;
		$pageLeftCol2PosX = $pageLeftColPosX + 3 + $cfg['Page']['RightColSmall'];

		define('EURO', chr(128));
		define('LF', chr(10));

		// Basic PDF setup
		$pdf = new PDF();
		$pdf->AddFont('din-medium', '', 'dinot-medium.php');
		$pdf->AddFont('din-light', '', 'dinot-light.php');
		$pdf->AddFont('icon-dishlabels', '', 'icon-dishlabels.php');

		$dailyMenuPages = $this->DailyMenuPages();
		if($dailyMenuPages) {
			foreach ($dailyMenuPages as $dailyMenuPage) {
				// get all FoodCategories of the DailyMenuPage
				$FoodCategories = $dailyMenuPage->FoodCategories();

				// get the Date of the DailyMenuPage
				$DailyMenuDate = DateUtil::dateFormat($dailyMenuPage->Date);

				if($FoodCategories) {
					foreach ($FoodCategories as $FoodCategory) {
						$Dishes = $FoodCategory->DishesForDateAndCategoryAndPrintTemplatePage();
						if($Dishes) {

							// ArrayLists for storing all additives and allergens that are used on the page
							$usedAdditives = new ArrayList();
							$usedAllergens = new ArrayList();

							// Set Smaller Font Sizes if there are more than $smallTextTreshold Dishes
							if ($Dishes->Count() >= $cfg['SmallTextThreshold'])
								$textSizeFactor = $cfg['TextSizeFactorSmall'];
							else
								$textSizeFactor = 1;

							// Add new PDF-Page per FoodCategory
							// Use $this->DailyMenuPages() > $this->DailyMenuPages()->FoodCategories() > $this->DailyMenuPages()->FoodCategories()->Dishes()
							$pdf->AddPage($cfg['Orientation'], 'A4');
							$pdf->SetAutoPageBreak(1);

							if ($cfg['Orientation'] == 'P') {
								// Add FoodCategory Title
								#$rgb = $FoodCategory->ColorRGB('000000');
								$rgb = array('r' => 0, 'g' => 0, 'b' => 0);
								$pdf->writeText(
									utf8_decode($FoodCategory->Title), // Text
									$cfg['Title']['FontSize'],         // Font size
									$rgb['r'], $rgb['g'], $rgb['b'],   // Color (r,g,b)
									$cfg['Page']['MarginLeft'],        // x
									$cfg['Title']['MarginTop'],        // y
									$cfg['Page']['InnerWidth'],        // width
									$cfg['Title']['Height'],           // height / line-height
									'din-medium'
								);
							}

							// Add margin below title
							$pdf->SetY($cfg['Dish']['OffsetTop']);

							// set variable for dishes per category
							$dishCount = 0;

							// set variable when dishes have external prices
							$hasPriceExtern = false;
							foreach ($Dishes as $SingleDish) {
								if ($SingleDish->PriceExtern || $SingleDish->PriceExternSmall)
									$hasPriceExtern =  true;
							}

							foreach ($Dishes as $SingleDish) {
								$priceOffsetY = 0;

								// Dish Labels
								$DishLabels = $SingleDish->getAllDishLabels();

								// set description width based on dish labels
								#$tmpDescriptionWidth = $DishLabels ? $descriptionWidthNarrow : $descriptionWidth;

								// get starting line of description to position intern and extern prices
								$doublePricesOffsetY = $pdf->GetY();

								// calculate description width
								$descriptionWidth = ($hasPriceExtern) ? $cfg['Page']['LeftColSmall'] : $cfg['Page']['LeftCol'];

								// Description - german
								$pdf->writeText(
									utf8_decode($SingleDish->Description),
									$cfg['Dish']['FontSize'] * $textSizeFactor,
									0, 0, 0,
									$cfg['Page']['MarginLeft'],
									false,
									$descriptionWidth,
									$cfg['Dish']['Height'] * $textSizeFactor,
									'din-medium'
								);
								// calculate height of last dish description line -> position price on same baseline
								$priceOffsetY = $pdf->GetY() - ($cfg['Price']['Height'] * $textSizeFactor);

								// Description - english
								if ($this->parent->Lang_en_US && $SingleDish->Description_en_US) {
									$pdf->writeText(
										utf8_decode($SingleDish->Description_en_US),
										$cfg['Dish']['EnUsFontSize'] * $textSizeFactor,
										0, 0, 0,
										$cfg['Page']['MarginLeft'],
										false,
										$descriptionWidth,
										$cfg['Dish']['EnUsHeight'] * $textSizeFactor,
										'din-light'
									);
									// calculate height of last dish english description line -> position price on same baseline
									$priceOffsetY = $pdf->GetY() - ($cfg['Price']['Height'] * $textSizeFactor);
								}

								// Zusatzstoffe & Allergene
								$additives = $SingleDish->getAllAdditives();
								$allergens = $SingleDish->getAllAllergens();

								// Zusatzstoffe dieser Seite einmal in ArrayList speichern
								if($additives->count() >= 1) {
									$usedAdditives = MyListUtil::ConcatenateLists(
										$usedAdditives,
										$additives
									);
								}
								// Zusatzstoffe dieser Seite einmal in ArrayList speichern
								if($allergens->count() >= 1) {
									$usedAllergens = MyListUtil::ConcatenateLists(
										$usedAllergens,
										$allergens
									);
								}

								// Zusatzstoffe & Allergene in Klammern anfügen (Sortiert nach Nummer des Zusatzstoffs)
								$additivesAndAllergensString = $SingleDish->getAllAdditivesAndAllergensString();
								if (strlen($additivesAndAllergensString) >= 1) {
									$pdf->writeText(
										"($additivesAndAllergensString)",
										$cfg['Additives']['FontSize'] * $textSizeFactor,
										0, 0, 0,
										$cfg['Page']['MarginLeft'],
										false,
										$cfg['Page']['LeftCol'],
										$cfg['Additives']['Height'] * $textSizeFactor,
										'din-light'
									);
								}

								// store current Y-offset as staring point for next dish
								$currentOffsetY = $pdf->GetY();

								// Internal prices only
								if(!$hasPriceExtern) {
									// Price
									if($SingleDish->Price) {
										$pdf->writeText(
											EURO.' '.$SingleDish->PriceNice,
											$cfg['Price']['FontSize'] * $textSizeFactor,
											0, 0, 0,
											$cfg['Page']['InnerWidth'] - $cfg['Page']['RightCol'] + $cfg['Page']['MarginLeft'],
											$priceOffsetY,
											$cfg['Page']['RightCol'],
											$cfg['Price']['Height'] * $textSizeFactor,
											'din-medium',
											'R'
										);
									}
									// Small Price
									if($SingleDish->PriceSmall) {
										$pdf->writeText(
											'klein: '.EURO.' '.$SingleDish->PriceSmallNice,
											$cfg['Price']['SmallFontSize'] * $textSizeFactor,
											0, 0, 0,
											$cfg['Page']['InnerWidth'] - $cfg['Page']['RightCol'] + $cfg['Page']['MarginLeft'],
											false,
											$cfg['Page']['RightCol'],
											$cfg['Price']['SmallHeight'] * $textSizeFactor,
											'din-medium',
											'R'
										);
										// store current Y position as starting point for next dish
										// if it is greater than the previous current Y position
										if($pdf->GetY() > $currentOffsetY) $currentOffsetY = $pdf->GetY();
									}
								}
								// print intern and extern prices in 2 columns
								else {
									if ($cfg['Orientation'] == 'L')
										$doublePricesOffsetY += 3;

									// add headings for internal and external prices
									if($dishCount == 0) {
										$pdf->writeText(
											'intern',
											$cfg['Additives']['FontSize'],
											0, 0, 0,
											$pageLeftColPosX,
											$doublePricesOffsetY - ($cfg['Additives']['Height'] * $textSizeFactor),
											$cfg['Page']['RightColSmall'],
											$cfg['Additives']['Height'] * $textSizeFactor,
											'din-light',
											'R'
										);
										$pdf->writeText(
											'extern',
											$cfg['Additives']['FontSize'],
											0, 0, 0,
											$pageLeftCol2PosX,
											$doublePricesOffsetY - ($cfg['Additives']['Height'] * $textSizeFactor),
											$cfg['Page']['RightColSmall'],
											$cfg['Additives']['Height'] * $textSizeFactor,
											'din-light',
											'R'
										);
									}

									// Price intern
									if($SingleDish->Price) {
										$pdf->writeText(
											$SingleDish->PriceNice,
											$cfg['Price']['FontSize'] * $textSizeFactor,
											0, 0, 0,
											$pageLeftColPosX,
											$doublePricesOffsetY,
											$cfg['Page']['RightColSmall'],
											$cfg['Price']['Height'] * $textSizeFactor,
											'din-medium',
											'R'
										);
									}

									// Price intern Small
									if($SingleDish->PriceSmall) {
										$pdf->writeText(
											'klein',
											$cfg['Additives']['FontSize'],
											0, 0, 0,
											$pageLeftColPosX,
											$pdf->GetY() + $cfg['Price']['Offset'],
											$cfg['Page']['RightColSmall'],
											$cfg['Additives']['Height'] * $textSizeFactor,
											'din-light',
											'R'
										);
										$pdf->writeText(
											$SingleDish->PriceSmallNice,
											$cfg['Price']['FontSize'] * $textSizeFactor,
											0, 0, 0,
											$pageLeftColPosX,
											$pdf->GetY(),
											$cfg['Page']['RightColSmall'],
											$cfg['Price']['Height'] * $textSizeFactor,
											'din-medium',
											'R'
										);
									}

									// Price extern
									if($SingleDish->PriceExtern) {
										$pdf->writeText(
											$SingleDish->PriceExternNice,
											$cfg['Price']['FontSize'] * $textSizeFactor,
											0, 0, 0,
											$pageLeftCol2PosX,
											$doublePricesOffsetY,
											$cfg['Page']['RightColSmall'],
											$cfg['Price']['Height'] * $textSizeFactor,
											'din-medium',
											'R'
										);
									}
									// Price extern Small
									if($SingleDish->PriceExternSmall) {
										$pdf->writeText(
											'klein',
											$cfg['Additives']['FontSize'],
											0, 0, 0,
											$pageLeftCol2PosX,
											$pdf->GetY() + $cfg['Price']['Offset'],
											$cfg['Page']['RightColSmall'],
											$cfg['Additives']['Height'] * $textSizeFactor,
											'din-light',
											'R'
										);
										$pdf->writeText(
											$SingleDish->PriceExternSmallNice,
											$cfg['Price']['FontSize'] * $textSizeFactor,
											0, 0, 0,
											$pageLeftCol2PosX,
											$pdf->GetY(),
											$cfg['Page']['RightColSmall'],
											$cfg['Price']['Height'] * $textSizeFactor,
											'din-medium',
											'R'
										);

									}
									// store current Y position as starting point for next dish
									// if it is greater than the previous current Y position
									if($pdf->GetY() > $currentOffsetY)
										$currentOffsetY = $pdf->GetY();
								}

								// Add Dish Labels
								if($DishLabels) {
									$MarginLeft = $cfg['Page']['MarginLeft'] + $cfg['Page']['InnerWidth'] - $cfg['Labels']['Width'];
									// when only intern prices: label above price, otherwise: below price
									if ($hasPriceExtern)
										$yPosition = $pdf->GetY() + 2;
									else
										$yPosition = $priceOffsetY - $cfg['Labels']['Height'] + 2;

									foreach ($DishLabels as $DishLabel) {
										// Add Icon with icon Font
										if($DishLabel->IconUnicodeCode) {
											$rgb = $DishLabel->ColorRGB('000000');
											/* $rgb = array('r' => 0, 'g' => 0, 'b' => 0); */
											$pdf->writeText(
												utf8_decode($DishLabel->getIconUnicodeCharacter()), // Icon
												$cfg['Labels']['Height'] * 2.2,  // Font size
												$rgb['r'], $rgb['g'], $rgb['b'], // Color (r,g,b)
												$MarginLeft,                     // x
												$yPosition,                      // y
												$cfg['Labels']['Width'],         // width
												$cfg['Labels']['Height'],        // height / line-height
												'icon-dishlabels'
											);
										}
										// Add Icon image (fallback), if present
										elseif ($DishLabel->ImageID) {
											$pdf->Image(
												Director::baseFolder().$DishLabel->Image()->Link(),
												$MarginLeft,
												$yPosition,
												$cfg['Labels']['Width'],
												$cfg['Labels']['Height'],
												'png'
											);
										}

										$MarginLeft -= $cfg['Labels']['Offset'] + $cfg['Labels']['Width'];
									}
								}

								// set starting point for next dish
								$pdf->SetY($currentOffsetY);

								// add Linebreak width Dish offset
								$pdf->Ln($cfg['Dish']['Offset']);

								// count up dish counter
								$dishCount++;

								unset($DishLabels);
							}

							// Footer Text (with empty line afterwards)
							$footerText = $this->FootNote ? $this->FootNote.LF : '';

							// Add all Additives and Allergens that are used on this page at the bottom of the page
							$usedAdditives->removeDuplicates('Number');
							$usedAdditives = $usedAdditives->sort('Number');
							if ($usedAdditives->Count() >= 1) {
								$footerText .= "Zusatzstoffe";
								if ($this->parent->Lang_en_US)
									$footerText .= " / Additives";
								$footerText .= ": ";

								// Build String with Additives
								$additiveCnt = 0;
								foreach ($usedAdditives as $tmpAdditive) {
									if ($additiveCnt >= 1)
										$footerText .= ', ';
									$footerText .= $tmpAdditive->Number.' = '.$tmpAdditive->Title;
									if ($this->parent->Lang_en_US && $tmpAdditive->Title_en_US)
										$footerText .= ' / '.$tmpAdditive->Title_en_US;
									$additiveCnt++;
								}
								$footerText .= LF;
							}

							$usedAllergens->removeDuplicates('Number');
							$usedAllergens = $usedAllergens->sort('Number');
							if ($usedAllergens->Count() >= 1) {
								$footerText .= "Enthält Allergene";
								if ($this->parent->Lang_en_US)
									$footerText .= " / Contains Additives";
								$footerText .= ": ";

								// Build String with Allergens
								$allergenCnt = 0;
								foreach ($usedAllergens as $tmpAllergen) {
									if ($allergenCnt >= 1)
										$footerText .= ', ';
									$footerText .= $tmpAllergen->Number.' = '.$tmpAllergen->Title;
									if ($this->parent->Lang_en_US && $tmpAllergen->Title_en_US)
										$footerText .= ' / '.$tmpAllergen->Title_en_US;
									$allergenCnt++;
								}
							}

							// Draw footer with additives and allergens
							$pdf->drawTextBox(
								utf8_decode($footerText),     // strText
								$cfg['Page']['InnerWidth'],   // width
								$cfg['Footer']['Height'],     // height
								$cfg['Page']['MarginLeft'],   // x
								$cfg['Footer']['Y'],          // y
								'L',                          // align (L,C,R)
								'B',                          // valign (M,B,T)
								0,                            // border
								$cfg['Footer']['FontSize'],   // font size
								$cfg['Footer']['LineHeight'], // text line height
								'din-light',                  // font
								$r = 0, $g = 0, $b = 0        // color rgb
							);

							if ($cfg['Orientation'] == 'P') {
							// add current date to the bottom
								$footerText = utf8_decode($DailyMenuDate);
							} elseif ($cfg['Orientation'] == 'L') {
								// add current date and foodcategory to the bottom
								$footerText = utf8_decode($DailyMenuDate).' | '.utf8_decode($FoodCategory->Title);
							}

							// add current date to the bottom
							$pdf->writeText(
								$footerText,                  // Text
								$cfg['Footer']['FontSize'],   // Font size
								0, 0, 0,                      // Color (r,g,b)
								$cfg['Page']['MarginLeft'],   // x
								$cfg['Page']['MarginTop'],    // y
								$cfg['Page']['InnerWidth'],   // width
								$cfg['Footer']['LineHeight'], // height / line-height
								'din-light'                   // font
							);
						}
					}
				}
			}
		}

		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if($dateRange['EndDate'])
			$filenameDate .= '-'.$dateRange['EndDate'];

		// return PDF as file download under the given filename
		return $pdf->Output($this->URLSegment.'-'.$filenameDate.'.pdf', 'D');
	}

	/**
	 * PDF generation: print_category_dina4_portrait
	 *
	 * Renders a portrait DIN A4 PDF of all Dishes of the selectes FoodCategories and Date Range
	 * One DIN A4 page per category and day (containing all associated dishes)
	 */
	public function print_category_dina4_portrait() {

		$cfg = array(
			'Orientation' => 'P',
			'Page' => array(
				'MarginLeft' => 20,
				'MarginTop' => 287,
				'InnerWidth' => 170,
				'LeftCol' => 125,
				'RightCol' => 40,
				'LeftColSmall' => 110,
				'RightColSmall' => 28
			),
			'Title' => array(
				'MarginTop' => 40,
				'FontSize' => 90,
				'Height' => 30
			),
			'Dish' => array(
				'OffsetTop' => 115,
				'FontSize' => 35,
				'Height' => 13,
				'EnUsFontSize' => 30,
				'EnUsHeight' => 12,
				'Offset' => 8 // Y-Offset between 2 Dishes
			),
			'Additives' => array(
				'FontSize' => 15,
				'Height' => 7
			),
			'Price' => array(
				'FontSize' => 30,
				'Height' => 12,
				'SmallFontSize' => 15,
				'SmallHeight' => 7,
				'Offset' => 3,
			),
			'Labels' => array(
				'Height' => 15,
				'Width' => 15,
				'Offset' => 0
			),
			'SmallTextThreshold' => 3, // smaller Textsize for a dish count equal or greater
			'TextSizeFactorSmall' => 0.7, // factor that text size is reduced for many dishes
			'Footer' => array(
				'Y' => 247,
				'Height' => 20,
				'FontSize' => 8,
				'LineHeight' => 3.5
			)
		);

		return $this->print_category_dina4($cfg);
	}

	/**
	 * PDF generation: print_category_dina4_landscape
	 *
	 * Renders a landscape DIN A4 PDF of all Dishes of the selectes FoodCategories and Date Range
	 * One DIN A4 page per category and day (containing all associated dishes)
	 */
	public function print_category_dina4_landscape() {

		$cfg = array(
			'Orientation' => 'L',
			'Page' => array(
				'MarginLeft' => 20,
				'MarginTop' => 200,
				'InnerWidth' => 257,
				'LeftCol' => 200,
				'RightCol' => 55,
				'LeftColSmall' => 184,
				'RightColSmall' => 33
			),
			'Dish' => array(
				'OffsetTop' => 20,
				'FontSize' => 50,
				'Height' => 18,
				'EnUsFontSize' => 35,
				'EnUsHeight' => 14,
				'Offset' => 8 // Y-Offset between 2 Dishes
			),
			'Additives' => array(
				'FontSize' => 20,
				'Height' => 10
			),
			'Price' => array(
				'FontSize' => 35,
				'Height' => 14,
				'SmallFontSize' => 20,
				'SmallHeight' => 8,
				'Offset' => 3,
			),
			'Labels' => array(
				'Height' => 20,
				'Width' => 20,
				'Offset' => 0
			),
			'SmallTextThreshold' => 3, // smaller Textsize for a dish count equal or greater
			'TextSizeFactorSmall' => 0.7, // factor that text size is reduced for many dishes
			'Footer' => array(
				'Y' => 177,
				'Height' => 20,
				'FontSize' => 8,
				'LineHeight' => 3.5
			)
		);

		return $this->print_category_dina4($cfg);
	}

	/**
	 * PDF generation: print_category_dina5
	 *
	 * Renders a DIN A4 PDF in landscape with a DIN A5 area
	 * of all Dishes of the selectes FoodCategories and Date Range
	 * One DIN A5 page per category and day (containing all associated dishes)
	 */
	public function print_category_dina5() {
		// Variablen
		$pageMarginLeft = 162;
		$pageInnerWidth = 123;
		$pageLeftCol = 92;
		$pageRightCol = 26;

		$pageLeftColSmall = 80;
		$pageRightColSmall = 19;
		$pageLeftColPosX = $pageMarginLeft + $pageLeftColSmall + 2;
		$pageLeftCol2PosX = $pageLeftColPosX + 3 + $pageRightColSmall;

		$titleMarginTop = 25;
		$titleFontSize = 65;
		$titleHeight = 20;

		$dishOffsetTop = 73;
		$dishFontSize = 25;
		$dishHeight = 10;
		$dishEnUsFontSize = 20;
		$dishEnUsHeight = 9;
		$dishOffset = 5; // Y-Offset between 2 Dishes

		$additivesFontSize = 10;
		$additivesHeight = 5;

		$priceFontSize = $dishEnUsFontSize;
		$priceHeight = $dishEnUsHeight;
		$priceSmallFontSize = 10;
		$priceSmallHeight = 4;
		$priceOffset = 2;

		$labelsHeight = 10.5;
		$labelsWidth = 10.5;
		$labelOffset = 0;

		$smallTextTreshhold = 3; // smaller Textsize for a disch count equal or greater
		$textSizeFactorSmall = 0.7; // factor that text size is reduced for many dishes

		$footerY = 178;
		$footerHeight = 13;
		$footerFontSize = 6;
		$footerLineHeight = 2.5;

		define('EURO',chr(128));
		define('NBSP', chr(9));

		// Basic PDF setup
		$pdf = new PDF();
		$pdf->AddFont('din-medium','','dinot-medium.php');
		$pdf->AddFont('din-light','','dinot-light.php');
		$pdf->AddFont('icon-dishlabels','','icon-dishlabels.php');

		$dailyMenuPages = $this->DailyMenuPages();
		if($dailyMenuPages) {
			foreach ($dailyMenuPages as $dailyMenuPage) {
				// get all FoodCategories of the DailyMenuPage
				$FoodCategories = $dailyMenuPage->FoodCategories();
				// get the Date of the DailyMenuPage
				$DailyMenuDate = DateUtil::dateFormat($dailyMenuPage->Date);
				if($FoodCategories) {
					foreach ($FoodCategories as $FoodCategory) {

						$Dishes = $FoodCategory->DishesForDateAndCategoryAndPrintTemplatePage();
						if($Dishes) {

							// ArrayLists for storing all additives and allergens that are used on the page
							$usedAdditives = new ArrayList();
							$usedAllergens = new ArrayList();

							// Set Smaller Font Sizes if more than $smallTextTreshhold Dishes are
							$textSizeFactor = ($Dishes->Count() >= $smallTextTreshhold) ? $textSizeFactorSmall : 1;

							// Add new PDF-Page per FoodCategory
							// Use $this->DailyMenuPages() > $this->DailyMenuPages()->FoodCategories() > $this->DailyMenuPages()->FoodCategories()->Dishes()
							$pdf->AddPage('L','A4');
							$pdf->SetAutoPageBreak(1);

							// Add FoodCategory Title
							#$rgb = $FoodCategory->ColorRGB('000000');
							$rgb = array(
								'r' => 0,
								'g' => 0,
								'b' => 0
							);
							$pdf->writeText(
								utf8_decode($FoodCategory->Title),	// Text
								$titleFontSize,						// Font size
								$rgb['r'],$rgb['g'],$rgb['b'],		// Color (r,g,b)
								$pageMarginLeft,					// x
								$titleMarginTop,					// y
								$pageInnerWidth,					// width
								$titleHeight,						// height / line-height
								'din-medium'
							);

							// Add margin below title
							$pdf->SetY($dishOffsetTop);

							// set variable for dishes per category
							$dishCount = 0;

							foreach ($Dishes as $SingleDish) {

								$priceOffsetY = 0;

								// Dish Labels
								$DishLabels = $SingleDish->getAllDishLabels();

								// set description width based on dish labels
								#$tmpDescriptionWidth = $DishLabels ? $descriptionWidthNarrow : $descriptionWidth;

								// get startting line of description to position intern and extern prices
								$doublePricesOffsetY = $pdf->GetY();

								// set variable when dish has external prices
								$hasPriceExtern = ($SingleDish->PriceExtern || $SingleDish->PriceExternSmall) ? true : false;

								// calculate descirption width
								$descriptionWidth = ($hasPriceExtern) ? $pageLeftColSmall : $pageLeftCol;

								// Description - german
								$pdf->writeText(
									utf8_decode($SingleDish->Description),
									$dishFontSize * $textSizeFactor,
									0,0,0,
									$pageMarginLeft,
									false,
									$descriptionWidth,
									$dishHeight * $textSizeFactor,
									'din-medium'
								);
								// calculate height of last dish description line -> position price on same baseline
								$priceOffsetY = $pdf->GetY() - ($priceHeight * $textSizeFactor);

								// Description - english
								if ($this->parent->Lang_en_US && $SingleDish->Description_en_US) {
									$pdf->writeText(
										utf8_decode($SingleDish->Description_en_US),
										$dishEnUsFontSize * $textSizeFactor,
										0,0,0,
										$pageMarginLeft,
										false,
										$descriptionWidth,
										$dishEnUsHeight * $textSizeFactor,
										'din-light'
									);
									// calculate height of last dish english description line -> position price on same baseline
									$priceOffsetY = $pdf->GetY() - ($priceHeight * $textSizeFactor);
								}

								// Zusatzstoffe & Allergene
								$additives = $SingleDish->getAllAdditives();
								$allergens = $SingleDish->getAllAllergens();

								// Zusatzstoffe dieser Seite einmal in ArrayList speichern
								if($additives->count() >= 1) {
									$usedAdditives = MyListUtil::ConcatenateLists(
										$usedAdditives,
										$additives
									);
								}
								// Zusatzstoffe dieser Seite einmal in ArrayList speichern
								if($allergens->count() >= 1) {
									$usedAllergens = MyListUtil::ConcatenateLists(
										$usedAllergens,
										$allergens
									);
								}

								// Zusatzstoffe & Allergene in Klammern anfügen (Sortiert nach Nummer des Zusatzstoffs)
								$additivesAndAllergensString = $SingleDish->getAllAdditivesAndAllergensString();

								if (strlen($additivesAndAllergensString) >= 1) {
									$pdf->writeText(
										"($additivesAndAllergensString)",
										$additivesFontSize * $textSizeFactor,
										0,0,0,
										$pageMarginLeft,
										false,
										$pageLeftCol,
										$additivesHeight * $textSizeFactor,
										'din-light'
									);
								}

								// store current Y-offset as staring point for next dish
								$currentOffsetY = $pdf->GetY();

								// Internal prices only
								if(!$hasPriceExtern) {

									// Price
									if($SingleDish->Price) {
										$pdf->writeText(
											EURO.' '.$SingleDish->PriceNice,
											$priceFontSize * $textSizeFactor,
											0,0,0,
											$pageInnerWidth - $pageRightCol + $pageMarginLeft,
											$priceOffsetY,
											$pageRightCol,
											$priceHeight * $textSizeFactor,
											'din-medium',
											'R'
										);
									}
									// Small Price
									if($SingleDish->PriceSmall) {
										$pdf->writeText(
											'klein: '.EURO.' '.$SingleDish->PriceSmallNice,
											$priceSmallFontSize * $textSizeFactor,
											0,0,0,
											$pageInnerWidth - $pageRightCol + $pageMarginLeft,
											false,
											$pageRightCol,
											$priceSmallHeight * $textSizeFactor,
											'din-medium',
											'R'
										);
										// store current Y position as starting point for next dish
										// if it is greater than the previous current Y position
										if($pdf->GetY() > $currentOffsetY) $currentOffsetY = $pdf->GetY();
									}
								}

								// print intern and extern prices in 2 columns
								else {

									$doublePricesOffsetY += 1;

									// add headings for internal and external prices
									if($dishCount == 0) {
										$pdf->writeText(
											'intern',
											$additivesFontSize,
											0,0,0,
											$pageLeftColPosX,
											$doublePricesOffsetY - ($additivesHeight * $textSizeFactor),
											$pageRightColSmall,
											$additivesHeight * $textSizeFactor,
											'din-light',
											'R'
										);
										$pdf->writeText(
											'extern',
											$additivesFontSize,
											0,0,0,
											$pageLeftCol2PosX,
											$doublePricesOffsetY - ($additivesHeight * $textSizeFactor),
											$pageRightColSmall,
											$additivesHeight * $textSizeFactor,
											'din-light',
											'R'
										);
									}

									// Price intern
									if($SingleDish->Price) {
										$pdf->writeText(
											$SingleDish->PriceNice,
											$priceFontSize * $textSizeFactor,
											0,0,0,
											$pageLeftColPosX,
											$doublePricesOffsetY,
											$pageRightColSmall,
											$priceHeight * $textSizeFactor,
											'din-medium',
											'R'
										);
									}
									// Price intern Small
									if($SingleDish->PriceSmall) {
										$pdf->writeText(
											'klein',
											$additivesFontSize,
											0,0,0,
											$pageLeftColPosX,
											$pdf->GetY() + $priceOffset,
											$pageRightColSmall,
											$additivesHeight * $textSizeFactor,
											'din-light',
											'R'
										);
										$pdf->writeText(
											$SingleDish->PriceSmallNice,
											$priceFontSize * $textSizeFactor,
											0,0,0,
											$pageLeftColPosX,
											$pdf->GetY(),
											$pageRightColSmall,
											$priceHeight * $textSizeFactor,
											'din-medium',
											'R'
										);

									}
									// Price extern
									if($SingleDish->PriceExtern) {
										$pdf->writeText(
											$SingleDish->PriceExternNice,
											$priceFontSize * $textSizeFactor,
											0,0,0,
											$pageLeftCol2PosX,
											$doublePricesOffsetY,
											$pageRightColSmall,
											$priceHeight * $textSizeFactor,
											'din-medium',
											'R'
										);
									}
									// Price extern Small
									if($SingleDish->PriceExternSmall) {
										$pdf->writeText(
											'klein',
											$additivesFontSize,
											0,0,0,
											$pageLeftCol2PosX,
											$pdf->GetY() + $priceOffset,
											$pageRightColSmall,
											$additivesHeight * $textSizeFactor,
											'din-light',
											'R'
										);
										$pdf->writeText(
											$SingleDish->PriceExternSmallNice,
											$priceFontSize * $textSizeFactor,
											0,0,0,
											$pageLeftCol2PosX,
											$pdf->GetY(),
											$pageRightColSmall,
											$priceHeight * $textSizeFactor,
											'din-medium',
											'R'
										);

									}
									// store current Y position as starting point for next dish
									// if it is greater than the previous current Y position
									if($pdf->GetY() > $currentOffsetY) $currentOffsetY = $pdf->GetY();
								}

								// Add Dish Labels
								if($DishLabels) {
									$MarginLeft = $pageMarginLeft + $pageInnerWidth - $labelsWidth;
									// when only intern prices: label above price, otherwise: below price
									$yPosition = $hasPriceExtern ? ($pdf->GetY() + 1.5) : ($priceOffsetY - $labelsHeight + 1.5);

									foreach ($DishLabels as $DishLabel) {

										// Add Icon with icon Font
										if($DishLabel->IconUnicodeCode) {
											$rgb = $DishLabel->ColorRGB('000000');
											/*
											$rgb = array(
												'r' => 0,
												'g' => 0,
												'b' => 0
											);
											*/
											$pdf->writeText(
												utf8_decode($DishLabel->getIconUnicodeCharacter()),	// Icon
												$labelsHeight * 2.2,				// Font size
												$rgb['r'],$rgb['g'],$rgb['b'],		// Color (r,g,b)
												$MarginLeft,						// x
												$yPosition,							// y
												$labelsWidth,						// width
												$labelsHeight,						// height / line-height
												'icon-dishlabels'
											);

										}
										// Add Icon image (fallback), if present
										elseif ($DishLabel->ImageID) {
											$pdf->Image(
												Director::baseFolder().$DishLabel->Image()->Link(),
												$MarginLeft,
												$yPosition,
												$labelsWidth,
												$labelsHeight,
												'png'
											);
										}

										$MarginLeft -= $labelOffset + $labelsWidth;
									}
								}

								// set starting point for next dish
								$pdf->SetY($currentOffsetY);

								// add Linbreak width Dish offset
								$pdf->Ln($dishOffset);

								// count up dish counter
								$dishCount++;

								unset($DishLabels);
							}

							// Footer Text (with empty line afterwards)
							$footerText = $this->FootNote ? $this->FootNote."
" : '';

							// Add all Additives and Allergens that are used on this page at the bottom of the page
							$usedAdditives->removeDuplicates('Number');
							$usedAdditives = $usedAdditives->sort('Number');
							if ($usedAdditives->Count() >= 1) {
								$footerText .= "Zusatzstoffe";
								if ($this->parent->Lang_en_US)
									$footerText .= " / Additives";
								$footerText .= ": ";

								// Build String with Additives
								$additiveCnt = 0;
								foreach ($usedAdditives as $tmpAdditive) {
									if ($additiveCnt >= 1)
										$footerText .= ', ';
									$footerText .= $tmpAdditive->Number.' = '.$tmpAdditive->Title;
									if ($this->parent->Lang_en_US && $tmpAdditive->Title_en_US)
										$footerText .= ' / '.$tmpAdditive->Title_en_US;
									$additiveCnt++;
								}
								$footerText .= "
";
							}

							$usedAllergens->removeDuplicates('Number');
							$usedAllergens = $usedAllergens->sort('Number');
							if ($usedAllergens->Count() >= 1) {
								$footerText .= "Enthält Allergene";
								if ($this->parent->Lang_en_US)
									$footerText .= " / Contains Allergens";
								$footerText .= ": ";

								// Build String with Allergens
								$allergenCnt = 0;
								foreach ($usedAllergens as $tmpAllergen) {
									if ($allergenCnt >= 1)
										$footerText .= ', ';
									$footerText .= $tmpAllergen->Number.' = '.$tmpAllergen->Title;
									if ($this->parent->Lang_en_US && $tmpAllergen->Title_en_US)
										$footerText .= ' / '.$tmpAllergen->Title_en_US;
									$allergenCnt++;
								}
							}

							// Draw footer with additives and allergens
							$pdf->drawTextBox(
								utf8_decode($footerText),// strText
								$pageInnerWidth,		// width
								$footerHeight,			// height
								$pageMarginLeft,		// x
								$footerY,				// y
								'L',					// align (L,C,R)
								'B',					// valign (M,B,T)
								0,						// border
								$footerFontSize,		// font size
								$footerLineHeight,		// text line height
								'din-light',			// font
								$r = 0, $g = 0, $b = 0	// color rgb
							);

							// add current date to the bottom
							$pdf->writeText(
								utf8_decode($DailyMenuDate),		// Text
								$footerFontSize,					// Font size
								0,0,0,								// Color (r,g,b)
								$pageMarginLeft,					// x
								200,								// y
								$pageInnerWidth,					// width
								$footerLineHeight,					// height / line-height
								'din-light'							// font
							);

						}
					}
				}
			}
		}

		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if($dateRange['EndDate']) $filenameDate .= '-'.$dateRange['EndDate'];

		// return PDF as file download under the given filename
		return $pdf->Output($this->URLSegment.'-'.$filenameDate.'.pdf', 'D');

	}

	/**
	 * PDF generation: print_category_dina7
	 *
	 * Renders a DIN A4 PDF with a 105x70mm Cards popup of all Dishes of the selectes Categories and Date Range
	 */
	public function print_category_dina7() {
		// Variablen
		$cardWidth = 105;
		$cardHeight = 70;

		//$itemCount = 15;
		$itemsPerRow = 2;
		$itemsPerPage = 8;

		$pageOffsetTop = 8;

		$pageMarginTop = 12;
		$pageMarginLeft = 8;
		$pageLeftCol = 70;
		$pageRightCol = 30;

		$dishFontSize = 16;
		$dishHeight = 6.5;
		$dishEnUsFontSize = 13;
		$dishEnUsHeight = 5.5;

		$additivesFontSize = 8;
		$additivesHeight = 3.5;

		$priceFontSize = $dishEnUsFontSize;
		$priceHeight = $dishEnUsHeight;
		$priceSmallFontSize = 9;
		$priceSmallHeight = 3;

		$labelsHeight = 6;
		$labelsWidth = 6;
		$labelOffset = 0;

		$footerFontSize = 6;
		$footerLineHeight = 2.5;

		$xLeftCol = $xLeftCol_tmp = $pageMarginLeft;
		$xRightCol = $xRightCol_tmp = $cardWidth - $pageRightCol - $pageMarginLeft;

		// y-Positionen Elemente
		$yTxt1 = $yTxt1_tmp = $pageMarginTop + $pageOffsetTop;
		$yDate = $yDate_tmp = $pageOffsetTop + $cardHeight - 8;

		$i = 1;

		define('EURO',chr(128));
		define('NBSP', chr(9));

		$backgroundImagePath = Director::baseFolder().'/themes/'.$this->SiteConfig()->Theme.'/images/print-category-dina7-bg.jpg';

		// Basic PDF setup
		$pdf = new PDF();
		$pdf->AddFont('din-medium','','dinot-medium.php');
		$pdf->AddFont('din-light','','dinot-light.php');
		$pdf->AddFont('icon-dishlabels','','icon-dishlabels.php');

		$pdf->AddPage('P','A4');
		$pdf->SetAutoPageBreak(1);
		$pdf->Image($backgroundImagePath,0,0,210,297,'jpg');

		// need to get total number of dishes here
		$itemCount = 0;
		$dailyMenuPages = $this->DailyMenuPages();
		if($dailyMenuPages) {
			foreach ($dailyMenuPages as $dailyMenuPage) {
				$FoodCategories = $dailyMenuPage->FoodCategories();
				if($FoodCategories) {
					foreach ($FoodCategories as $FoodCategory) {
						$Dishes = $FoodCategory->DishesForDateAndCategoryAndPrintTemplatePage();
						if($Dishes) {
							foreach ($Dishes as $SingleDish) {
								$itemCount++;
							}
						}
					}
				}
			}
		}
		unset($Dishes);
		unset($FoodCategories);
		unset($dailyMenuPages);

		$dailyMenuPages = $this->DailyMenuPages();
		if($dailyMenuPages) {
			foreach ($dailyMenuPages as $dailyMenuPage) {
				$FoodCategories = $dailyMenuPage->FoodCategories();
				if($FoodCategories) {
					foreach ($FoodCategories as $FoodCategory) {
						$Dishes = $FoodCategory->DishesForDateAndCategoryAndPrintTemplatePage();
						if($Dishes) {
							foreach ($Dishes as $SingleDish) {

								$priceOffsetY = 0;

								// Dish Labels
								$DishLabels = $SingleDish->getAllDishLabels();

								// generate PDF
								// Use $this->DailyMenuPages() > $this->DailyMenuPages()->FoodCategories() > $this->DailyMenuPages()->FoodCategories()->Dishes()

								if ( $i % $itemsPerRow == 0 ) {
									$xLeftCol_tmp += $cardWidth;
									$xRightCol_tmp += $cardWidth;
								}
								else {
									$xLeftCol_tmp = $xLeftCol;
									$xRightCol_tmp = $xRightCol;
								}

								// Description
								$pdf->writeText(
									utf8_decode($SingleDish->Description),
									$dishFontSize,
									0,0,0,
									$xLeftCol_tmp,
									$yTxt1_tmp,
									$pageLeftCol,
									$dishHeight
								);

								// calculate height of last dish description line -> position price on same baseline
								$priceOffsetY = $pdf->GetY() - $priceHeight;

								// Description english
								if ($this->parent->Lang_en_US && $SingleDish->Description_en_US) {
									$pdf->writeText(
										utf8_decode($SingleDish->Description_en_US),
										$dishEnUsFontSize,
										0,0,0,
										$xLeftCol_tmp,
										false,
										$pageLeftCol,
										$dishEnUsHeight,
										'din-light'
									);
									// calculate height of last dish description line -> position price on same baseline
									$priceOffsetY = $pdf->GetY() - $priceHeight;
								}

								// Zusatzstoffe & Allergene
								$additives = $SingleDish->getAllAdditives();
								$allergens = $SingleDish->getAllAllergens();

								// Zusatzstoffe & Allergene in Klammern anfügen (Sortiert nach Nummer des Zusatzstoffs)
								$additivesAndAllergensString = $SingleDish->getAllAdditivesAndAllergensString();

								if (strlen($additivesAndAllergensString) >= 1) {

									$pdf->writeText(
										"($additivesAndAllergensString)",
										$additivesFontSize,
										0,0,0,
										$xLeftCol_tmp,
										false,
										$pageLeftCol,
										$additivesHeight,
										'din-light'
									);
								}

								// Price
								if($SingleDish->Price) {
									$pdf->writeText(
										EURO.' '.$SingleDish->PriceNice,
										$priceFontSize,
										0,0,0,
										$xRightCol_tmp,
										$priceOffsetY,
										$pageRightCol,
										$priceHeight,
										'din-medium',
										'R'
									);
								}

								// Small Price
								if($SingleDish->PriceSmall) {
									$pdf->writeText(
										'klein: '.EURO.' '.$SingleDish->PriceSmallNice,
										$priceSmallFontSize ,
										0,0,0,
										$xRightCol_tmp,
										false,
										$pageRightCol,
										$priceSmallHeight,
										'din-medium',
										'R'
									);
								}

								// Add Dish Labels
								if($DishLabels) {
									$MarginLeft = $xRightCol_tmp + $pageRightCol - $labelsWidth;
									$yPosition = $priceOffsetY - $labelsHeight + 1.5;

									foreach ($DishLabels as $DishLabel) {

										// Add Icon with icon Font
										if($DishLabel->IconUnicodeCode) {
											$rgb = $DishLabel->ColorRGB('000000');
											/*
											$rgb = array(
												'r' => 0,
												'g' => 0,
												'b' => 0
											);
											*/
											$pdf->writeText(
												utf8_decode($DishLabel->getIconUnicodeCharacter()),	// Icon
												$labelsHeight * 2.2,				// Font size
												$rgb['r'],$rgb['g'],$rgb['b'],		// Color (r,g,b)
												$MarginLeft,						// x
												$yPosition,							// y
												$labelsWidth,						// width
												$labelsHeight,						// height / line-height
												'icon-dishlabels'
											);

										}
										// Add Icon image (fallback), if present
										elseif ($DishLabel->ImageID) {
											$pdf->Image(
												Director::baseFolder().$DishLabel->Image()->Link(),
												$MarginLeft,
												$yPosition,
												$labelsWidth,
												$labelsHeight,
												'png'
											);
										}

										$MarginLeft -= $labelOffset + $labelsWidth;
									}
								}

								// DailyMenu Date
								$pdf->writeText(
									$dailyMenuPage->DateFormat(),
									$footerFontSize,
									0,0,0,
									$xLeftCol_tmp,
									$yDate_tmp,
									$cardWidth - 2 * $pageMarginLeft,
									$footerLineHeight,
									'din-light'
								);

								if ( $i % $itemsPerRow == 0 ) {
									$yTxt1_tmp += $cardHeight;
									$yDate_tmp += $cardHeight;
								}

								if ( $i % $itemsPerPage == 0 && $i != $itemCount) {
									$pdf->AddPage('P','A4');
									$pdf->SetAutoPageBreak(1);
									$pdf->Image($backgroundImagePath, 0, 0, 210, 297, 'jpg');

									$yTxt1_tmp = $yTxt1;
									$yDate_tmp = $yDate;
								}

								$i++;

							}
						}
					}
				}
			}
		}

		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if($dateRange['EndDate']) $filenameDate .= '-'.$dateRange['EndDate'];

		// return PDF as file download under the given filename
		return $pdf->Output($this->URLSegment.'-'.$filenameDate.'.pdf', 'D');
	}

	/**
	 * PDF generation: print_weekly_table_detail()
	 *
	 * Renders a DIN A4 portrait Menu with dishes of monday to friday (default)
	 *
	 * @param String $locale language code for PDF generation. default: 118n default locale
	 * @param Boolean $nutritives true if seperate nutritives table should be rendered
	 * @param Integer $onlyWeekDays Boolean default: true > Only monday to friday will be rendered. If false monday to sunday will be rendered
	 */
	private function print_weekly_table_detail($locale, $nutritives = false, $onlyWeekDays = true) {

		// Variablen
		$headerImageLeft = 5;
		$headerImageTop = 4;
		$headerImageWidth = 287;
		$headerImageHeight = 17;
		$headerTitleWidth = 90;
		$headerFontSize = 10;
		$tableWidth = 287;

		// 1. Seite Speiseplan
		$colsPerDay = $nutritives ? 3 : 2;
		$colWidthCategory = 27;
		$colWidthPrice = 10; //price column has fixed width
		$colWidthTextFactor = 0.83; // text column uses percentage of available width
		$colWidthFatpointsFactor = 0.17; // fatpoints column uses percentage of available width

		// 2. Seite Nährwerte
		$colsPerDayNutritives = 5;
		$colWidthTextNutritives = 23;
		$colWidthNutritives = 6.8;
		$colWidthCalories = 8;
		$colWidthProtein = 7.5;

		$foodCategoryFontSize = 8;
		$foodCategoryLineHeight = 4;
		$tableTextFontSize = 7;
		$tableTextLineHeight = 3;
		$priceFontSize = 6;
		$fatpointsFontSize = 6;
		$iconLabelsFontSize = 8;
		$additivesFontSize = 5;
		$allergensFontSize = 5;
		$nutritivesFontSize = 6;
		$specialLabelFontSize = 7;
		$footerFontSize = 6;

		// set locale
		$locale = $locale ? $locale : i18n::default_locale();
		$default_locale = i18n::get_locale();
		i18n::set_locale($locale);

		define('EURO', chr(128));
		define('NBSP', chr(9));
		define('ICONMENU', chr(74));
		$colorLightness = 0; // 0 - 100 factor to make FoodCategory Color Lighter (0 = no change, 100 = white)
		//Table Config
		$aCustomConfiguration = array(
			'HEADER' => array(
				'WIDTH'             => 6,                    //cell width
				'TEXT_COLOR'        => array(255,255,255),   //text color
				'TEXT_SIZE'         => 8,                    //font size
				'TEXT_FONT'         => 'din-medium',         //font family
				'TEXT_ALIGN'        => 'L',                  //horizontal alignment, possible values: LRC (left, right, center)
				'VERTICAL_ALIGN'    => 'B',                  //vertical alignment, possible values: TMB(top, middle, bottom)
				'TEXT_TYPE'         => '',                   //font type
				'LINE_SIZE'         => 4,                    //line size for one row
				'BACKGROUND_COLOR'  => array(12,36,50),      //background color
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
				'BORDER_TYPE'       => '1',                  //border type, can be: 0, 1 or a combination of: "LRTB"
				'TEXT'              => '',                   //text
				//padding
				'PADDING_TOP'       => 0.8,
				'PADDING_RIGHT'     => 0.3,
				'PADDING_LEFT'      => 0.3,
				'PADDING_BOTTOM'    => 0.8,
			),
			'ROW' => array(
				'TEXT_COLOR'        => array(0,0,0),         //text color
				'TEXT_SIZE'         => $tableTextFontSize,   //font size
				'TEXT_FONT'         => 'din-medium',         //font family
				'TEXT_ALIGN'        => 'L',                  //horizontal alignment, possible values: LRC (left, right, center)
				'VERTICAL_ALIGN'    => 'T',                  //vertical alignment, possible values: TMB(top, middle, bottom)
				'TEXT_TYPE'         => '',                   //font type
				'LINE_SIZE'         => $tableTextLineHeight, //line size for one row
				'BACKGROUND_COLOR'  => array(255,255,255),   //background color
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
				'BORDER_TYPE'       => '1',                  //border type, can be: 0, 1 or a combination of: "LRTB"
				//padding
				'PADDING_TOP'       => 0.8,
				'PADDING_RIGHT'     => 0.3,
				'PADDING_LEFT'      => 0.3,
				'PADDING_BOTTOM'    => 0.8,
			),
			'TABLE' => array(
				'TABLE_ALIGN'       => 'L',                  //table align on page
				'TABLE_LEFT_MARGIN' => 0,                    //space to the left margin
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
			)
		);

		//Initialisiere Tabellen Klassen
		$pdf = new PDFTable();
		$pdf->Open();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->SetMargins(5, 21, 5);
		$pdf->AddFont('din-medium','','dinot-medium.php');
		$pdf->AddFont('din-light','','dinot-light.php');
		$pdf->AddFont('icon-dishlabels','','icon-dishlabels.php');
		$pdf->AliasNbPages();

		//Generate Table Content
		$i = 0; // Zeilenzähler (FoodCategory)
		$k = 0; // zweiter Zeilenzähler (Dish - Gerichte) -> Je Gericht innerhalb der Kategorie eine Zeile
		$j = 1; // Spaltenzähler (DailyMenuPage - Tag)

		$TableData = array();

		// Array for all label styles
		$labelStyles = array();

		$FoodCategories = $this->FoodCategories();
		foreach ($FoodCategories as $FoodCategory) { //Essens Kategorien abfragen, entspricht einer Tabellenzeile
			$maxDishes = 0;

			// Titel und Farbe für die jeweilige Zeile zuweisen
			$FoodCategoryTitle = $FoodCategory->Title();
			$rgb = array('r' => 255, 'g' => 255, 'b' => '255');
			$rgbLight = $rgb;
			if ($FoodCategory->IsMain) {
				$rgb = $FoodCategory->ColorRGB();
				$rgbLight = $FoodCategory->ColorLighterRGB($colorLightness);
			}
			$TableData[$i][0][0] = array(
				'title' => $FoodCategoryTitle,
				'rgb' => $rgb,
				'rgbLight' => $rgbLight,
				'dishCount' => 0,
				'isMain' => $FoodCategory->IsMain
			);

			// get daily menupages for weekdays or whole week (including saturday and sunday)
			$dailyMenuPages = $FoodCategory->DailyMenuPagesForDateRange($this->MenuContainer()->ID, true, $onlyWeekDays);
			foreach ($dailyMenuPages as $dailyMenuPage) { //Tage abfragen, entspricht einer Tabellenspalte
				// Datums Array befüllen
				// datum auf englisch umstellen
				if ($locale != 'de_DE') DateUtil::setlocale('en_US');

				$weeklyDates[$j-1]['full']      = $dailyMenuPage->DateFormat('%A, %d.%m.%Y');
				$weeklyDates[$j-1]['fullbreak'] = $dailyMenuPage->DateFormat("%A,\n%d.%m.%Y");
				$weeklyDates[$j-1]['date']      = $dailyMenuPage->DateFormat('%d.%m.%Y');
				$weeklyDates[$j-1]['short']     = $dailyMenuPage->DateFormat('%d.%m.%y');
				// Datum auf deutsch zurückstellen
				if ($locale != 'de_DE') DateUtil::setlocale();
				// Wenn Hinweis auf Ferietag etc. -> Hinweis statt Dishes ausgeben
				$weeklyDates[$j-1]['message'] = ($dailyMenuPage->Message) ? utf8_decode($dailyMenuPage->Message) : false;

				$DishesForCategory = $dailyMenuPage->DishesForCategoryAndPrintTemplatePage();
				if ($DishesForCategory) {
					$dishCount = 0;
					$k = 0; // zweiter Zeilenzähler (Dish - Gerichte) -> Je Gericht pro der Kategorie und Tag
					foreach ($DishesForCategory as $SingleDish) { //Einzelne Gerichte pro Tag abfragen innerhalb einer Zelle
						$TableData[$i][$k][$j]['text'] = '';

						// Aktion falls vorhanden als Text ausgeben
						if($specialLabels = $SingleDish->getAllSpecialLabels()) {
							foreach($specialLabels as $sLabel) {
								$TableData[$i][$k][$j]['text'] .= '<speciallabel>'.utf8_decode(mb_strtoupper(_t('SpecialLabel.SINGULARNAME', 'Aktion').': '.$sLabel->Title))."</speciallabel>
";
							}
						}

						// Essens Beschreibung und hinzufügen
						$TableData[$i][$k][$j]['text'] .= utf8_decode($SingleDish->getDescriptionSingleLine());

						// Zusatzstoffe & Allergene
						$additivesAndAllergensString = $SingleDish->getAllAdditivesAndAllergensString();

						// Zusatzstoffe & Allergene in Klammern anfügen (Sortiert nach Nummer des Zusatzstoffs)
						$TableData[$i][$k][$j]['additives'] = '';
						if (strlen($additivesAndAllergensString) >= 1) {
							$TableData[$i][$k][$j]['additives'] = " <zu>($additivesAndAllergensString)</zu>";
						}

						// Dish Labels anfügen
						$TableData[$i][$k][$j]['dishlabels'] = '';
						$dishLabels = $SingleDish->getAllDishLabels();
						if ($dishLabels) {
							$labelStyles[$i][0] = 'label' . $i; // label tag name (e.g. label1)
							#$labelStyles[$i][1] = $FoodCategory->Color ? '255,255,255' : '0,0,0'; // label color
							$labelStyles[$i][1] = '0,0,0';

							// create array with all Dishlabels,
							// containing character for IconASCIICode or Title (as fallback)
							$dishLabelsArray = array();
							foreach ($dishLabels as $tmpDishLabel) {
								if($tmpDishLabel->IconUnicodeCode) {
									$dishLabelStr = '<' . $labelStyles[$i][0] . '>';
									// add seperate Tag for menu label
									if($tmpDishLabel->IconUnicodeCode == 74) {
										$dishLabelStr .= '<menuLabel>'.$tmpDishLabel->getIconUnicodeCharacter().'</menuLabel>';
									}
									else {
										$dishLabelStr .= $tmpDishLabel->getIconUnicodeCharacter();
									}
									$dishLabelStr .= '</' . $labelStyles[$i][0] . '>';
								}
								else {
									$dishLabelStr = $tmpDishLabel->Title;
								}
								$dishLabelsArray[] = $dishLabelStr;
							}

							$TableData[$i][$k][$j]['dishlabels'] = implode(' ', $dishLabelsArray);
						}

						// Preis
						if ($SingleDish->Price) {
							$TableData[$i][$k][$j]['price'] = EURO . " " . $SingleDish->PriceNice;
						}
						else {
							$TableData[$i][$k][$j]['price'] = "-";
						}
						// Externen Preis ergänzen
						if ($SingleDish->PriceExtern) {
							$TableData[$i][$k][$j]['price'] .= (!$SingleDish->Price) ? "
" : "";
							$TableData[$i][$k][$j]['price'] .= " " . _t('RestaurantDish.PRICEEXTERN','Extern') . " " . EURO . " " . $SingleDish->PriceExternNice;
						}

						// Nährwerte
						$TableData[$i][$k][$j]['fat'] = $SingleDish->FatNice;
						$TableData[$i][$k][$j]['calories'] = $SingleDish->CaloriesNice;
						$TableData[$i][$k][$j]['protein'] = $SingleDish->ProteinNice;
						$TableData[$i][$k][$j]['sugar'] = $SingleDish->Sugar;
						$TableData[$i][$k][$j]['carbohydrates'] = $SingleDish->CarbohydratesNice;

						// store maximum number of dishes for the current cateory (spanning the whole week)
						if ($k + 1 > $TableData[$i][0][0]['dishCount'])
							$TableData[$i][0][0]['dishCount'] = $k + 1;

						#echo "<br />Adding Dish Nr. $k for Category ".$TableData[$i][0][0]['title'].", Day " .$dailyMenuPage->DateFormat('%A, %d.%m.%Y')."- maximum number for this category: ".$TableData[$i][0][0]['dishCount'];

						$dishCount++;
						$k++; // Zähler für Dishes
					}
				}
				else {
					$TableData[$i][0][$j]['text'] = " ";
					$TableData[$i][0][$j]['additives'] = " ";
					$TableData[$i][0][$j]['dishlabels'] = " ";
					$TableData[$i][0][$j]['price'] = " ";
					$TableData[$i][0][$j]['fat'] = " ";
					$TableData[$i][0][$j]['calories'] = " ";
					$TableData[$i][0][$j]['protein'] = " ";
					$TableData[$i][0][$j]['sugar'] = " ";
					$TableData[$i][0][$j]['carbohydrates'] = " ";
				}
				$j++;
			}
			$i++;
			$j = 1;
		}

		// Begin Table Creation
		// get Number of FoodCategories
		$categoryCount = $i;

		// get week count
		$numWeeks = 1;
		// Anzahl in Wochen
		if ($this->DatePeriod == 'Wochen') {
			$numWeeks = $this->DateRange;
		}
		// Anzahl Tage größer als 1 Woche?
		else if ($this->DateRange > 7) {
			$numWeeks = round($this->DateRange / 7);
		}

		// get maximum number of rows
		$aRowCount = 0;
		foreach ($TableData as $category) {
			$aRowCount += $category[0][0]['dishCount'];
		}
		#echo "<br /><br />there are $aRowCount in total";

		// Für jede Woche eigene Seite mit Tabelle genereieren
		for ($weekCount = 0; $weekCount < $numWeeks; $weekCount++) {

			// set the number of days per week
			$daysPerWeek = $onlyWeekDays ? 5 : 7;

			// add counter for weekdays, depending on weekCount
			// 5 days per week:
			// $weeklyDates[0] = monday 1st week
			// $weeklyDates[5] = monday 2nd week
			// 7 day per week:
			// $weeklyDates[0] = monday 1st week
			// $weeklyDates[7] = monday 2nd week
			$weekDayCounter = $weekCount * $daysPerWeek;

			// calculate start and end date of week (end date is start date +4 days) monday - friday
			$weekStartDate = DateUtil::dateFormat(date('d-m-Y', strtotime($this->DateRangeStartDate()."+".($weekCount * 7)." days")));
			$weekEndDate = DateUtil::dateFormat(date('d-m-Y', strtotime($weekStartDate."+".($daysPerWeek-1)." days")));

			// 1. SEITE: SPEISEPLAN MIT PREISEN
			// Add one Page per table
			$pdf->AddPage('L', 'A4');
			//default text color
			$pdf->SetTextColor(0, 0, 0);

			// Header Image
			$headerImagePath = '/themes/'.$this->SiteConfig()->Theme.'/images/print-weeklytable-header.gif';
			$headerImageExtension = 'gif';
			if ($this->WeeklyTableHeaderImageID) {
				$headerImagePath = $this->WeeklyTableHeaderImage()->Link();
				$headerImageExtension = strtolower($this->WeeklyTableHeaderImage()->getExtension());
			}
			$pdf->Image(
					Director::baseFolder() . $headerImagePath,
					$headerImageLeft, // Left
					$headerImageTop, // Top
					$headerImageWidth, // Width
					$headerImageHeight, // Height
					$headerImageExtension // Filetype
			);

			// Header Text
			$pdf->drawTextBox(
					$weekStartDate . ' - ' . $weekEndDate . '', // strText
					60, // width
					10, // height
					232, // x :NEW: Date right aligned
					12.5, // y
					'R', // align (L,C,R)
					'T', // valign (M,B,T)
					0, // border
					$headerFontSize, // font size
					10, // text line height
					'din-medium', // font
					$r = 0, $g = 0, $b = 0 // color rgb
			);

			// Image footer right
			#$pdf->Image(Director::baseFolder().'/themes/default-theme/images/logo-eurest-print-weekly-table.jpg',277,194,15,11,'jpg');
			// Add new table per page
			$oTable = new fpdfTable($pdf);

			// set default text styles
			$oTable->SetStyle("zu","din-light","",$additivesFontSize,"0,0,0"); // Zusatzstoffe
			$oTable->SetStyle("zuWhite","din-light","",$additivesFontSize,"255,255,255"); // Zusatzstoffe
			$oTable->SetStyle("all","din-light","",$allergensFontSize,"0,0,0"); // Allergene
			$oTable->SetStyle("allWhite","din-light","",$allergensFontSize,"255,255,255"); // Allergene
			$oTable->SetStyle("t2","din-medium","",$foodCategoryFontSize,"0,0,0"); // Titel Essenskategorie
			$oTable->SetStyle("t2White","din-medium","",$foodCategoryFontSize,"255,255,255"); // Titel Essenskategorie
			$oTable->SetStyle("speciallabel","din-light","",$specialLabelFontSize,"0,0,0"); // Zusatzstoffe

			// get DishLabel for menu
			$dishLabelMenu = GlobalDishLabel::get()->filter('IconUnicodeCode', '74')->First();
			if($dishLabelMenu) {
				$dishLabelMenuRGB = FoodCategory::hex2rgb($dishLabelMenu->Color);
				$oTable->SetStyle("menuLabel", "icon-dishlabels", "", $iconLabelsFontSize, $dishLabelMenuRGB['r'].','.$dishLabelMenuRGB['g'].','.$dishLabelMenuRGB['b']);
			}
			// set all label styles
			foreach ($labelStyles as $labelStyle) {
				$oTable->SetStyle($labelStyle[0], "icon-dishlabels", "", $iconLabelsFontSize, "");
			}

			// calculate available width per day, based on number of days
			$cellDayWidth = ($tableWidth - $colWidthCategory) / $daysPerWeek;
			$tblWidthArray = array($colWidthCategory); // array with table cell widths
			$aHeader = array(array('TEXT'  => ' ')); // array with table header entries

			// calculate cell widths
			$colWidthText = $cellDayWidth - $colWidthPrice;

			// set different cell widths if nutritives are printed
			if($nutritives) {
				// calculate cell widths
				$colWidthText = ($cellDayWidth - $colWidthPrice) * $colWidthTextFactor;
				$colWidthFatpoints = ($cellDayWidth - $colWidthPrice) * $colWidthFatpointsFactor;
			}

			// set table cell widths and table headers based on given number of days per week
			for($i = 0; $i < $daysPerWeek; $i++ ) {
				// set table cell width
				$tblWidthArray[] = $colWidthText; // text
				$tblWidthArray[] = $colWidthPrice; // price
				if($nutritives) $tblWidthArray[] = $colWidthFatpoints; // fatpoints

				if($onlyWeekDays) {
					$aHeader[] = array('TEXT' => $weeklyDates[$weekDayCounter + $i]['full']); // date
				}
				else {
					$aHeader[] = array('TEXT' => $weeklyDates[$weekDayCounter + $i]['fullbreak']); // date with line break
				}
				$aHeader[] = array('TEXT' => ' '); // price
				if($nutritives) $aHeader[] = array('TEXT' => 'F'); // fatpoints
			}

			// initialize table
			$oTable->initialize($tblWidthArray, $aCustomConfiguration);
			//add the header
			$oTable->addHeader($aHeader);

			// Loop through categories
			for ($j = 0; $j < $categoryCount; $j++) {

				// Hintergrundfarbe Tabellenzelle
				$bgColor = array($TableData[$j][0][0]['rgb']['r'], $TableData[$j][0][0]['rgb']['g'], $TableData[$j][0][0]['rgb']['b']);
				$bgColorLight = array($TableData[$j][0][0]['rgbLight']['r'], $TableData[$j][0][0]['rgbLight']['g'], $TableData[$j][0][0]['rgbLight']['b']);

				#$colorClass = $TableData[$j][0][0]['isMain'] == 1 ? 'White' : '';

				// Loop through the maximum of existing dishes per category
				for ($k = 0; $k < $TableData[$j][0][0]['dishCount']; $k++) {

					$aRow = Array();

					// Add FoodCategory Title spanning all columns
					if ($k == 0) {
						$colorClass = ($TableData[$j][0][0]['isMain'] == 1) ? 'White' : '';
						$aRow[0]['TEXT'] = "<t2$colorClass>" . utf8_decode($TableData[$j][0][0]['title']) . "</t2$colorClass>";	//text for column 0
						$aRow[0]['LINE_SIZE'] = $foodCategoryLineHeight;
						$aRow[0]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'];
						$aRow[0]['BACKGROUND_COLOR'] = $bgColor;
					}

					// loop through all weekdays with additional columns
					for ($w = 1; $w <= $daysPerWeek; $w++) {

						$tmpRowCount = ($w * $colsPerDay) - $colsPerDay;

						// If entry for weekday and column exists
						if (isset($TableData[$j][$k][$weekDayCounter + $w]) || $weeklyDates[$weekDayCounter + $w - 1]['message']) {
							// MESSAGE Add message in first row, spanning all rows
							if(
								$weeklyDates[$weekDayCounter + $w - 1]['message'] &&
								$j == 0
							) {
								$aRow[$tmpRowCount + 1]['TEXT'] = $weeklyDates[$weekDayCounter + $w - 1]['message'];
								$aRow[$tmpRowCount + 2]['TEXT'] = " ";
								if($nutritives) $aRow[$tmpRowCount + 3]['TEXT'] = " ";

								// span current row over all rows
								$aRow[$tmpRowCount + 1]['ROWSPAN'] = $aRowCount;
								$aRow[$tmpRowCount + 2]['ROWSPAN'] = $aRowCount;
								if($nutritives) $aRow[$tmpRowCount + 3]['ROWSPAN'] = $aRowCount;
							}
							else {

								// TEXT COLUMN
								$aRow[$tmpRowCount + 1]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['text'].$TableData[$j][$k][$weekDayCounter + $w]['additives'];
								// PRICE COLUMN
								$aRow[$tmpRowCount + 2]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['price'].$TableData[$j][$k][$weekDayCounter + $w]['dishlabels'];
								$aRow[$tmpRowCount + 2]['TEXT_SIZE'] = $priceFontSize;
								// FAT
								if($nutritives) {
									if($TableData[$j][$k][$weekDayCounter + $w]['fat']) {
										$aRow[$tmpRowCount + 3]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['fat'];
									}
									else {
										$aRow[$tmpRowCount + 3]['TEXT'] = " ";
									}
									$aRow[$tmpRowCount + 3]['TEXT_SIZE'] = $fatpointsFontSize;
								}
								// set Background color
	//							$aRow[$tmpRowCount + 1]['BACKGROUND_COLOR'] = $bgColorLight;
	//							$aRow[$tmpRowCount + 2]['BACKGROUND_COLOR'] = $bgColorLight;
	//							$aRow[$tmpRowCount + 3]['BACKGROUND_COLOR'] = $bgColorLight;

								// if next column does not exist -> span current column over the missing columns
								if ($k < ($TableData[$j][0][0]['dishCount'] - 1) && !isset($TableData[$j][$k + 1][$weekDayCounter + $w])) {
									$aRow[$tmpRowCount + 1]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
									$aRow[$tmpRowCount + 2]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
									if($nutritives) $aRow[$tmpRowCount + 3]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
								}
							}

						}
					}
					//add the row
					$oTable->addRow($aRow);
				}
			}

			//close the table
			$oTable->close();

			// Add Footer
			// Get used Additives (if existing) as Array
			$additivesArray = ($additives = $this->UsedAdditives()) ? $additives->toNestedArray() : false;
			// Get used Allergens (if existing) as Array
			$allergensArray = ($allergens = $this->UsedAllergens()) ? $allergens->toNestedArray() : false;

			if ($this->FootNote || $additivesArray || $allergensArray) {

				// generate Additives String
				$additive_str = '';
				if ($additivesArray) {

					$additive_str = "

" . _t('Additive.PLURALNAME', 'Additives') . ": ";

					$additive_count = 0;
					foreach ($additivesArray as $additive) {
						if ($additive_count >= 1)
							$additive_str .= ' | ';
						$additive_str .= $additive['Number'] . ' ';
						// add localized title
						$additive_str .= (array_key_exists('Title_' . $locale, $additive) && strlen($additive['Title_' . $locale]) >= 1) ? $additive['Title_' . $locale] : $additive['Title'];
						$additive_count++;
					}
				}

				// generate Allergens String
				$allergen_str = '';
				$allergen_add = ($locale == 'de_DE') ? 'Enthält ' : 'Contains ';
				if($allergensArray) {

					$allergen_str = "
".$allergen_add._t('Allergen.PLURALNAME', 'Allergens').": ";

					$allergen_count = 0;
					foreach ($allergensArray as $allergen) {
						if ($allergen_count >= 1)
							$allergen_str .= ' | ';
						$allergen_str .= $allergen['Number'] . ' ';
						// add localized title
						$allergen_str .= (array_key_exists('Title_' . $locale, $allergen) && strlen($allergen['Title_' . $locale]) >= 1) ? $allergen['Title_' . $locale] : $allergen['Title'];
						$allergen_count++;
					}

					// add Allergen Disclaimer
					$allergen_str .= "
";
					$allergen_str .= ($locale == 'de_DE') ? $this->SiteConfig()->AllergenDisclaimer : $this->SiteConfig()->AllergenDisclaimer_en_US;

				}

				// Footer Text in multicell
				$pdf->tableFooter(utf8_decode($this->FootNote . $additive_str . $allergen_str), 0, 3, 3, 'din-light', $footerFontSize, 0, 0, 0);
			}

			// :NEW: Used DishLabels below Footer

			// DISH LABEL LEGEND BELOW FOOTER
			$dishLabelY = $pdf->GetY() + 3;
			$dishLabelX = 5; // page margin

			if($allDishLabels = $this->UsedDishLabels()) {

				foreach($allDishLabels as $tmpDishLabel) {

					// set icon color (default: black)
					$iconR = 0;
					$iconG = 0;
					$iconB = 0;

					if($tmpDishLabel->Color) {
						$iconColorRGB = $tmpDishLabel->ColorRGB();

						$iconR = $iconColorRGB['r'];
						$iconG = $iconColorRGB['g'];
						$iconB = $iconColorRGB['b'];
					}

					// draw icon
					$pdf->drawTextBox(
						$tmpDishLabel->getIconUnicodeCharacter(), // strText
						4.5, // width
						2.7, // height
						$dishLabelX, // x
						$dishLabelY, // y
						'L', // align (L,C,R)
						'T', // valign (M,B,T)
						0, // border
						$iconLabelsFontSize, // font size
						2.7, // text line height
						'icon-dishlabels', // font
						$iconR, $iconG, $iconB // color rgb
					);

					// draw text string
					$dishLabelX += 4;
					$iconTitle = $locale == 'de_DE' ? utf8_decode($tmpDishLabel->Title) : utf8_decode($tmpDishLabel->Title_en_US);
					$pdf->drawTextBox(
						$iconTitle, // strText
						18, // width
						2.7, // height
						$dishLabelX, // x = icon width + margin
						$dishLabelY, // y
						'L', // align (L,C,R)
						'T', // valign (M,B,T)
						0, // border
						$footerFontSize, // font size
						2.7, // text line height
						'din-light', // font
						$r = 0, $g = 0, $b = 0 // color rgb
					);

					// calculate new x and y values
					$dishLabelX += 19;

				}
			}

			// --END :NEW:

			// <--
			// ENDE 1. SEITE

			// -->
			// 2. SEITE: NÄHRWERTE
			// Add one Page per table

			if($nutritives)  {
				$pdf->AddPage('L', 'A4');
				//default text color
				$pdf->SetTextColor(0, 0, 0);

				// Header Image
				$headerImagePath = '/themes/'.$this->SiteConfig()->Theme.'/images/print-weeklytable-header.gif';
				if ($this->WeeklyTableHeaderImageID)
					$headerImagePath = $this->WeeklyTableHeaderImage()->Link();
				$pdf->Image(
						Director::baseFolder() . $headerImagePath,
						$headerImageLeft, // Left
						$headerImageTop, // Top
						$headerImageWidth, // Width
						$headerImageHeight, // Height
						'gif' // Filetype
				);

				// Header mit Datum
				$pdf->drawTextBox(
						$weekStartDate . ' - ' . $weekEndDate . '', // strText
						$headerTitleWidth, // width
						10, // height
						200, // x
						11, // y
						'R', // align (L,C,R)
						'T', // valign (M,B,T)
						0, // border
						$headerFontSize, // font size
						10, // text line height
						'din-medium', // font
						$r = 0, $g = 0, $b = 0 // color rgb
				);

				// Image footer right
				#$pdf->Image(Director::baseFolder().'/themes/default-theme/images/logo-eurest-print-weekly-table.jpg',277,194,15,11,'jpg');
				// Add new table per page
				$oTable = new fpdfTable($pdf);

				// set default text styles
				$oTable->SetStyle("zu","din-light","",$additivesFontSize,"0,0,0"); // Zusatzstoffe
				$oTable->SetStyle("zuWhite","din-light","",$additivesFontSize,"255,255,255"); // Zusatzstoffe
				$oTable->SetStyle("all","din-light","",$allergensFontSize,"0,0,0"); // Allergene
				$oTable->SetStyle("allWhite","din-light","",$allergensFontSize,"255,255,255"); // Allergene
				$oTable->SetStyle("t2","din-medium","",$foodCategoryFontSize,"0,0,0"); // Titel Essenskategorie
				$oTable->SetStyle("t2White","din-medium","",$foodCategoryFontSize,"255,255,255"); // Titel
				// set all label styles
				foreach ($labelStyles as $labelStyle) {
					$oTable->SetStyle($labelStyle[0], "icon-dishlabels", "", $iconLabelsFontSize, $labelStyle[1]);
				}

				$tblWidthArray = array($colWidthCategory); // array with table cell widths
				$aHeader = array(array('TEXT'  => ' ')); // array with table header entries

				// set table cell widths and table headers based on given number of days per week
				for($i = 0; $i < $daysPerWeek; $i++ ) {
					// :TODO: calculate cell width based on number of weekdays
					// set table cell width
					$tblWidthArray[] = $colWidthTextNutritives;
					$tblWidthArray[] = $colWidthCalories;
					$tblWidthArray[] = $colWidthProtein;
					$tblWidthArray[] = $colWidthNutritives;
					$tblWidthArray[] = $colWidthNutritives;

					$aHeader[] = array('TEXT' => $weeklyDates[$weekDayCounter + $i]['fullbreak']); // date with line break
					$aHeader[] = array('TEXT' => 'kcal');
					$aHeader[] = array('TEXT' => 'EW');
					$aHeader[] = array('TEXT' => 'ZU');
					$aHeader[] = array('TEXT' => 'KH');
				}

				// initialize table
				$oTable->initialize($tblWidthArray, $aCustomConfiguration);
				//add the header
				$oTable->addHeader($aHeader);

				// Loop through categories
				for ($j = 0; $j < $categoryCount; $j++) {

					// Hintergrundfarbe Tabellenzelle
					$bgColor = array($TableData[$j][0][0]['rgb']['r'], $TableData[$j][0][0]['rgb']['g'], $TableData[$j][0][0]['rgb']['b']);
					$bgColorLight = array($TableData[$j][0][0]['rgbLight']['r'], $TableData[$j][0][0]['rgbLight']['g'], $TableData[$j][0][0]['rgbLight']['b']);

					// Loop through the maximum of existing dishes per category
					for ($k = 0; $k < $TableData[$j][0][0]['dishCount']; $k++) {

						$aRow = Array();

						// Add FoodCategory Title spanning all columns
						if ($k == 0) {
							$colorClass = ($TableData[$j][0][0]['isMain'] == 1) ? 'White' : '';
							$aRow[0]['TEXT'] = "<t2$colorClass>" . utf8_decode($TableData[$j][0][0]['title']) . "</t2$colorClass>";	//text for column 0
							$aRow[0]['LINE_SIZE'] = $foodCategoryLineHeight;
							$aRow[0]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'];
							$aRow[0]['BACKGROUND_COLOR'] = $bgColor;
						}

						// loop through all weekdays with additional columns
						for ($w = 1; $w <= $daysPerWeek; $w++) {

							$tmpRowCount = ($w * $colsPerDayNutritives) - $colsPerDayNutritives;

							// If entry for weekday and column exists
							if (isset($TableData[$j][$k][$weekDayCounter + $w]) || $weeklyDates[$weekDayCounter + $w - 1]['message']) {
								// MESSAGE Add message in first column, spanning all other columns
								if(
									$weeklyDates[$weekDayCounter + $w - 1]['message'] &&
									$j == 0
								) {
									$aRow[$tmpRowCount + 1]['TEXT'] = $weeklyDates[$weekDayCounter + $w - 1]['message'];
									$aRow[$tmpRowCount + 2]['TEXT'] = " ";
									$aRow[$tmpRowCount + 3]['TEXT'] = " ";
									$aRow[$tmpRowCount + 4]['TEXT'] = " ";
									$aRow[$tmpRowCount + 5]['TEXT'] = " ";

									// span over all columns
									$aRow[$tmpRowCount + 1]['ROWSPAN'] = $aRowCount;
									$aRow[$tmpRowCount + 2]['ROWSPAN'] = $aRowCount;
									$aRow[$tmpRowCount + 3]['ROWSPAN'] = $aRowCount;
									$aRow[$tmpRowCount + 4]['ROWSPAN'] = $aRowCount;
									$aRow[$tmpRowCount + 5]['ROWSPAN'] = $aRowCount;

								}
								else {

									// TEXT COLUMN
									$aRow[$tmpRowCount + 1]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['text'];
									// KCAL
									if($TableData[$j][$k][$weekDayCounter + $w]['calories']) {
										$aRow[$tmpRowCount + 2]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['calories'];
									}
									else {
										$aRow[$tmpRowCount + 2]['TEXT'] = " ";
									}
									$aRow[$tmpRowCount + 2]['TEXT_SIZE'] = $nutritivesFontSize;
									// EIWEISS
									if($TableData[$j][$k][$weekDayCounter + $w]['protein']) {
										$aRow[$tmpRowCount + 3]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['protein'];
									}
									else {
										$aRow[$tmpRowCount + 3]['TEXT'] = " ";
									}
									$aRow[$tmpRowCount + 3]['TEXT_SIZE'] = $nutritivesFontSize;
									// BE
									if($TableData[$j][$k][$weekDayCounter + $w]['sugar']) {
										$aRow[$tmpRowCount + 4]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['sugar'];
									}
									else {
										$aRow[$tmpRowCount + 4]['TEXT'] = " ";
									}
									$aRow[$tmpRowCount + 4]['TEXT_SIZE'] = $nutritivesFontSize;
									// Kohlenhydrate
									if($TableData[$j][$k][$weekDayCounter + $w]['carbohydrates']) {
										$aRow[$tmpRowCount + 5]['TEXT'] = $TableData[$j][$k][$weekDayCounter + $w]['carbohydrates'];
									}
									else {
										$aRow[$tmpRowCount + 5]['TEXT'] = " ";
									}
									$aRow[$tmpRowCount + 5]['TEXT_SIZE'] = $nutritivesFontSize;

									// set Background color
		//							$aRow[$tmpRowCount + 1]['BACKGROUND_COLOR'] = $bgColorLight;
		//							$aRow[$tmpRowCount + 2]['BACKGROUND_COLOR'] = $bgColorLight;
		//							$aRow[$tmpRowCount + 3]['BACKGROUND_COLOR'] = $bgColorLight;
		//							$aRow[$tmpRowCount + 4]['BACKGROUND_COLOR'] = $bgColorLight;
		//							$aRow[$tmpRowCount + 5]['BACKGROUND_COLOR'] = $bgColorLight;

									// if next column does not exist -> span current column over the missing columns
									if ($k < ($TableData[$j][0][0]['dishCount'] - 1) && !isset($TableData[$j][$k + 1][$weekDayCounter + $w])) {
										$aRow[$tmpRowCount + 1]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
										$aRow[$tmpRowCount + 2]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
										$aRow[$tmpRowCount + 3]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
										$aRow[$tmpRowCount + 4]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
										$aRow[$tmpRowCount + 5]['ROWSPAN'] = $TableData[$j][0][0]['dishCount'] - $k;
									}
								}
							}
						}
						//add the row
						$oTable->addRow($aRow);
					}
				}

				//close the table
				$oTable->close();

				// Footer Text in multicell
				$pdf->tableFooter(utf8_decode(_t('PrintTemplatePage.NUTRITIVESLEGEND', 'EW = Eiweiß | BE = Broteinheiten | KH = Kohlenhydrate')), 0, 3, 3, 'din-light', $footerFontSize, 0, 0, 0);
			} // end page per week
		}

		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if ($dateRange['EndDate'])
			$filenameDate .= '-' . $dateRange['EndDate'];

		// reset locale
		i18n::set_locale($default_locale);

		// return PDF as file download under the given filename
		return $pdf->Output($this->URLSegment . '-' . $filenameDate . '.pdf', 'D');
	}

	/**
	 * PDF generation: print_weekly_table_detail_de_de()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - friday,
	 * containing all Dishes of the selectes Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_de_de() {
		return $this->print_weekly_table_detail('de_DE');
	}

	/**
	 * PDF generation: print_weekly_table_detail_7days_de_de()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - sunday,
	 * containing all Dishes of the selectes Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_7days_de_de() {
		return $this->print_weekly_table_detail('de_DE', false, false);
	}

	/**
	 * PDF generation: print_weekly_table_detail_en_us()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - friday,
	 * containing all Dishes of the selectes Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_en_us() {
		return $this->print_weekly_table_detail('en_US');
	}

	/**
	 * PDF generation: print_weekly_table_detail_7days_en_us()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - sunday,
	 * containing all Dishes of the selectes Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_7days_en_us() {
		return $this->print_weekly_table_detail('en_US');
	}

	/**
	 * PDF generation: print_weekly_table_detail_nutritives_de_de()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - friday,
	 * containing all Dishes and their nutritives
	 * of the selected Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_nutritives_de_de() {
		return $this->print_weekly_table_detail('de_DE', true);
	}

	/**
	 * PDF generation: print_weekly_table_detail_nutritives_7days_de_de()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - sunday,
	 * containing all Dishes and their nutritives
	 * of the selected Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_nutritives_7days_de_de() {
		return $this->print_weekly_table_detail('de_DE', true, false);
	}

	/**
	 * PDF generation: print_weekly_table_detail_nutritives_en_us()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - friday,
	 * containing all Dishes and their nutritives
	 * of the selected Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_nutritives_en_us() {
		return $this->print_weekly_table_detail('en_US', true);
	}

	/**
	 * PDF generation: print_weekly_table_detail_nutritives_7days_en_us()
	 *
	 * Renders a DIN A4 PDF with a weekly table monday - sunday,
	 * containing all Dishes and their nutritives
	 * of the selected Categories and Date Range
	 * with seperate columns for dish and price
	 */
	public function print_weekly_table_detail_nutritives_7days_en_us() {
		return $this->print_weekly_table_detail('en_US', true, false);
	}

	/**
	 * PDF generation: print_temperature_table()
	 *
	 * Renders a DIN A4 PDF with a table to track temperatures of daily dishes
	 */
	public function print_temperature_table() {
		define('EURO', chr(128));
		define('NBSP', chr(9));
		$colorLightness = 20; // 0 - 100 factor to make FoodCategory Color Lighter (0 = no change, 100 = white)
		$textSizeTitle = 24;
		$textSizeHeader1 = 12;
		$textSizeHeader2 = 10;
		$textSizeText = 8;

		//Table Config
		$aCustomConfiguration = array(
			'HEADER' => array(
				'WIDTH'             => 6,                    //cell width
				'TEXT_COLOR'        => array(0,0,0),         //text color
				'TEXT_SIZE'         => $textSizeHeader2,     //font size
				'TEXT_FONT'         => 'din-medium',         //font family
				'TEXT_ALIGN'        => 'L',                  //horizontal alignment, possible values: LRC (left, right, center)
				'VERTICAL_ALIGN'    => 'B',                  //vertical alignment, possible values: TMB(top, middle, bottom)
				'TEXT_TYPE'         => '',                   //font type
				'LINE_SIZE'         => 4,                    //line size for one row
				'BACKGROUND_COLOR'  => array(255, 255, 255), //background color
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
				'BORDER_TYPE'       => '1',                  //border type, can be: 0, 1 or a combination of: "LRTB"
				'TEXT'              => '',                   //text
				//padding
				'PADDING_TOP'       => 0.5,
				'PADDING_RIGHT'     => 0.8,
				'PADDING_LEFT'      => 0.8,
				'PADDING_BOTTOM'    => 0.5,
			),
			'ROW' => array(
				'TEXT_COLOR'        => array(0,0,0),         //text color
				'TEXT_SIZE'         => $textSizeText,        //font size
				'TEXT_FONT'         => 'din-medium',         //font family
				'TEXT_ALIGN'        => 'L',                  //horizontal alignment, possible values: LRC (left, right, center)
				'VERTICAL_ALIGN'    => 'T',                  //vertical alignment, possible values: TMB(top, middle, bottom)
				'TEXT_TYPE'         => '',                   //font type
				'LINE_SIZE'         => 3,                    //line size for one row
				'BACKGROUND_COLOR'  => array(255,255,255),   //background color
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
				'BORDER_TYPE'       => '1',                  //border type, can be: 0, 1 or a combination of: "LRTB"
				//padding
				'PADDING_TOP'       => 1.5,
				'PADDING_RIGHT'     => 0.8,
				'PADDING_LEFT'      => 0.8,
				'PADDING_BOTTOM'    => 1.5,
			),
			'TABLE' => array(
				'TABLE_ALIGN'       => 'L',                  //table align on page
				'TABLE_LEFT_MARGIN' => 0,                    //space to the left margin
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
			)
		);

		//Initialisiere Tabellen Klassen
		$pdf = new PDFTable();
		$pdf->Open();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->SetMargins(5, 21, 5);
		$pdf->AddFont('din-medium','','dinot-medium.php');
		$pdf->AliasNbPages();

		//Generate Table Content
		$pageCount = 0; // page counter (each day on a seperate page)
		$TableData = array();
		$dailyMenuPagesDates = array();

		$dailyMenuPages = $this->DailyMenuPages();
		if($dailyMenuPages) {
			foreach ($dailyMenuPages as $dailyMenuPage) {

				// reset dish counter for each page
				$dishCount = 0;

				$FoodCategories = $dailyMenuPage->FoodCategories();
				if($FoodCategories) {
					foreach ($FoodCategories as $FoodCategory) {
						$Dishes = $FoodCategory->DishesForDateAndCategory();
						if($Dishes) {

							// Add new table row for each Dish
							foreach ($Dishes as $SingleDish) {
								// Description
								$TableData[$pageCount][$dishCount] = utf8_decode($SingleDish->Description);
								$dishCount++;
							}
						}
					}
				}

				// save date of current DailyMenuPage
				$dailyMenuPagesDates[$pageCount] = $dailyMenuPage->DateFormat('%A, %d.%m.%Y');

				// Add another page
				$pageCount++;
			}
		}

		//Begin Table Creation

		// Für jeden Tag eigene Seite mit Tabelle genereieren
		for($dayCount = 0; $dayCount < $pageCount; $dayCount++) {

			// Add one Page per table
			$pdf->AddPage('P', 'A4');
			//default text color
			$pdf->SetTextColor(0, 0, 0);

			// Add Header

				// page title
				$pdf->SetFont('din-medium','',$textSizeTitle);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(5,10);
				$pdf->MultiCell(0,8,utf8_decode(_t('PrintTemplatePage.HEADERTEMPCHECK', 'Temperature measurement for production and counter')."\n"),0,'L');

				// date of DailyMenuPage
				$pdf->SetFont('din-medium','', 18);
				$pdf->MultiCell(0,8,utf8_decode($dailyMenuPagesDates[$dayCount]."\n\n"),0,'L');

				// restaurant name
				$pdf->SetFont('din-medium','', $textSizeHeader2);
				$pdf->MultiCell(0,4,utf8_decode(_t('PrintTemplatePage.COMPANY', 'Company').': '.$this->parent->Title."\n\n"),0,'L');

				// name of employee that checked the temperature
				$pdf->MultiCell(0,4,utf8_decode(_t('PrintTemplatePage.NAMEOFEMPLOYEE', 'Checked by (name)').': __________________________________________________________________'."\n\n\n"),0,'L');

				// FootNote
				$pdf->SetFont('din-medium','', $textSizeText);
				$pdf->MultiCell(0,4,utf8_decode($this->FootNote."\n\n"),0,'L');

			// Add new table per page
			$oTable = new fpdfTable($pdf);

			$nRows = count($TableData[$dayCount]);

			//Initialize the table class, 10 columns
			$oTable->initialize(array(45,17,17,17,17,17,17,17,17,17), $aCustomConfiguration);

			// first table header
			$aHeader = array(
				array(
					'TEXT'  => utf8_decode(_t('Dish.SINGULARNAME', 'Dish'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODUCTION', 'Production')),
					'COLSPAN' => 3
				),
				array(
					'TEXT'	=> ' '
				),
				array(
					'TEXT'	=> ' '
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.COUNTERFIRSTCHECK', 'Counter 1st check')),
					'COLSPAN' => 3
				),
				array(
					'TEXT'	=> ' '
				),
				array(
					'TEXT'	=> ' '
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.COUNTERSECONDCHECK', 'Counter 2nd check')),
					'COLSPAN' => 3
				)
			);
			$oTable->addHeader($aHeader);

			// Add notes for temperatures
			$aHeader = array(
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.TEMPERATUREDEFAULTS', 'Default temperature values')),
					'TEXT_SIZE'	=> $textSizeText
				),

				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.TEMPERATUREDEFAULTSPRODUCTION', "Cold dishes to +12°C\nWarm dishes min. +72°C / 10 Min.")),
					'TEXT_SIZE'	=> $textSizeText,
					'COLSPAN' => 3
				),
				array(
					'TEXT'  => ' ',
				),
				array(
					'TEXT'  => ' ',
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.TEMPERATUREDEFAULTSCOUNTER', "Cold dishes to +7°C (Maximum +10°C)\nWarm dihes more than +65°C")),
					'TEXT_SIZE'	=> $textSizeText,
					'COLSPAN' => 6
				),
				array(
					'TEXT'  => ' ',
				),
				array(
					'TEXT'  => ' ',
				),
				array(
					'TEXT'  => ' ',
				),
				array(
					'TEXT'  => ' ',
				),
				array(
					'TEXT'  => ' ',
				)
			);
			$oTable->addHeader($aHeader);

			// second table header
			$aHeader = array(
				array('TEXT'  => ' '),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.TIME', 'Time')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.CORETEMPERATURE', 'Core temp. in °C')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.CORRECTIVEACTION', 'C.A.')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.TIME', 'Time')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.CORETEMPERATURE', 'Core temp. in °C')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.CORRECTIVEACTION', 'C.A.')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.TIME', 'Time')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.CORETEMPERATURE', 'Core temp. in °C')),
					'TEXT_SIZE'	=> $textSizeText
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.CORRECTIVEACTION', 'C.A.')),
					'TEXT_SIZE'	=> $textSizeText
				)
			);
			$oTable->addHeader($aHeader);

			// Add rows: one row per Dish
			for ($j = 0; $j < $nRows; $j++)
			{

				$aRow = Array();
				$aRow[0]['TEXT'] = $TableData[$dayCount][$j];    //dish description in column 0
				$oTable->addRow($aRow);
			}

			//close the table
			$oTable->close();

			// Footer Text in multicell
			$pdf->SetFont('din-medium','',$textSizeText);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetXY(5,285);
			$pdf->MultiCell(
				0,5,
				utf8_decode(_t('PrintTemplatePage.PRINTEDNON', 'Printed on')).': '.date('d.m.Y').
				" - ".utf8_decode(_t('PrintTemplatePage.RETENTIONPERIOD', 'Retention period: 4 weeks'))
				,0,'R'
			);

		} // end page per day

		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if($dateRange['EndDate']) $filenameDate .= '-'.$dateRange['EndDate'];

		// return PDF as file download under the given filename
		return $pdf->Output($this->URLSegment.'-'.$filenameDate.'.pdf', 'D');
	}

	/**
	 * PDF generation: print_production_table()
	 *
	 * Renders a DIN A4 PDF with a table to track production quality of daily dishes
	 */
	public function print_production_table() {
		define('EURO', chr(128));
		define('NBSP', chr(9));
		$textSizeTitle = 24;
		$textSizeTitle2 = 10;
		$textSizeHeader = 8;
		$textSizeText = 8;

		//Table Config
		$aCustomConfiguration = array(
			'HEADER' => array(
				'WIDTH'             => 6,                    //cell width
				'TEXT_COLOR'        => array(0,0,0),         //text color
				'TEXT_SIZE'         => $textSizeHeader,      //font size
				'TEXT_FONT'         => 'din-medium',         //font family
				'TEXT_ALIGN'        => 'L',                  //horizontal alignment, possible values: LRC (left, right, center)
				'VERTICAL_ALIGN'    => 'B',                  //vertical alignment, possible values: TMB(top, middle, bottom)
				'TEXT_TYPE'         => '',                   //font type
				'LINE_SIZE'         => 4,                    //line size for one row
				'BACKGROUND_COLOR'  => array(255,255,255),   //background color
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
				'BORDER_TYPE'       => '1',                  //border type, can be: 0, 1 or a combination of: "LRTB"
				'TEXT'              => '',                   //text
				//padding
				'PADDING_TOP'       => 0.5,
				'PADDING_RIGHT'     => 0.8,
				'PADDING_LEFT'      => 0.8,
				'PADDING_BOTTOM'    => 0.5,
			),
			'ROW' => array(
				'TEXT_COLOR'        => array(0,0,0),         //text color
				'TEXT_SIZE'         => $textSizeText,        //font size
				'TEXT_FONT'         => 'din-medium',         //font family
				'TEXT_ALIGN'        => 'L',                  //horizontal alignment, possible values: LRC (left, right, center)
				'VERTICAL_ALIGN'    => 'T',                  //vertical alignment, possible values: TMB(top, middle, bottom)
				'TEXT_TYPE'         => '',                   //font type
				'LINE_SIZE'         => 3,                    //line size for one row
				'BACKGROUND_COLOR'  => array(255,255,255),   //background color
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
				'BORDER_TYPE'       => '1',                  //border type, can be: 0, 1 or a combination of: "LRTB"
				//padding
				'PADDING_TOP'       => 1.5,
				'PADDING_RIGHT'     => 0.8,
				'PADDING_LEFT'      => 0.8,
				'PADDING_BOTTOM'    => 1.5,
			),
			'TABLE' => array(
				'TABLE_ALIGN'       => 'L',                  //table align on page
				'TABLE_LEFT_MARGIN' => 0,                    //space to the left margin
				'BORDER_COLOR'      => array(0,0,0),         //border color
				'BORDER_SIZE'       => 0.1,                  //border size
			)
		);

		//Initialisiere Tabellen Klassen
		$pdf = new PDFTable();
		$pdf->Open();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->SetMargins(5, 21, 5);
		$pdf->AddFont('din-medium','','dinot-medium.php');
		$pdf->AliasNbPages();

		//Generate Table Content
		$pageCount = 0; // page counter (each day on a seperate page)
		$TableData = array();
		$dailyMenuPagesDates = array();

		$dailyMenuPages = $this->DailyMenuPages();
		if($dailyMenuPages) {
			foreach ($dailyMenuPages as $dailyMenuPage) {

				// reset dish counter for each page
				$dishCount = 0;

				$FoodCategories = $dailyMenuPage->FoodCategories();
				if($FoodCategories) {
					foreach ($FoodCategories as $FoodCategory) {
						$Dishes = $FoodCategory->DishesForDateAndCategory();
						if($Dishes) {

							// Add new table row for each Dish
							foreach ($Dishes as $SingleDish) {
								// Description
								$TableData[$pageCount][$dishCount] = utf8_decode($SingleDish->Description);
								$dishCount++;
							}
						}
					}
				}

				// save date of current DailyMenuPage
				$dailyMenuPagesDates[$pageCount] = $dailyMenuPage->DateFormat('%A, %d.%m.%Y');

				// Add another page
				$pageCount++;
			}
		}

		//Begin Table Creation

		// Für jeden Tag eigene Seite mit Tabelle genereieren
		for($dayCount = 0; $dayCount < $pageCount; $dayCount++) {

			// Add one Page per table
			$pdf->AddPage('P', 'A4');
			//default text color
			$pdf->SetTextColor(0, 0, 0);

			// Add Header

				// page title
				$pdf->SetFont('din-medium','',$textSizeTitle);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(5,10);
				$pdf->MultiCell(0,8,utf8_decode(_t('PrintTemplatePage.HEADERPRODUCTION', 'Produktionsblatt')."\n"),0,'L');

				// date of DailyMenuPage
				$pdf->SetFont('din-medium','', 18);
				$pdf->MultiCell(0,8,utf8_decode($dailyMenuPagesDates[$dayCount]."\n\n"),0,'L');

				// restaurant name
				$pdf->SetFont('din-medium','', $textSizeTitle2);
				$pdf->MultiCell(0,4,utf8_decode(_t('PrintTemplatePage.COMPANY', 'Company').': '.$this->parent->Title."\n\n"),0,'L');

				// FootNote
				$pdf->SetFont('din-medium','', $textSizeText);
				$pdf->MultiCell(0,4,utf8_decode($this->FootNote."\n\n"),0,'L');

			// Add new table per page
			$oTable = new fpdfTable($pdf);

			$nRows = count($TableData[$dayCount]);

			//Initialize the table class, 10 columns
			$oTable->initialize(array(45,17,17,17,17,17,17,17,17,17), $aCustomConfiguration);

			// first table header
			$aHeader = array(
				array(
					'TEXT'  => utf8_decode(_t('Dish.SINGULARNAME', 'Dish'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADWEIGHT', 'Gewicht pro Stück'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADQTY', 'Geplante Menge'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADPREPARED', "Vor-\nbereitet"))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADADDED', "Nach-\nproduziert"))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADTOTAL', 'Total'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADRETURNED', 'Zurück'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADPERSONELL', 'Personal'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADFREE', 'Gratis'))
				),
				array(
					'TEXT'  => utf8_decode(_t('PrintTemplatePage.PRODHEADSOLD', 'Verkauft'))
				)
			);
			$oTable->addHeader($aHeader);

			// Add rows: one row per Dish
			for ($j = 0; $j < $nRows; $j++)
			{

				$aRow = Array();
				$aRow[0]['TEXT'] = $TableData[$dayCount][$j]."\n\n\n";    //dish description in column 0
				$oTable->addRow($aRow);
			}

			//close the table
			$oTable->close();

			// Add Footer
			$pdf->SetFont('din-medium','',$textSizeText);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetXY(5,285);
			$pdf->MultiCell(
				0,5,
				utf8_decode(_t('PrintTemplatePage.PRINTEDNON', 'Printed on')).': '.date('d.m.Y')
				,0,'R'
			);
		} // end page per day

		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if($dateRange['EndDate']) $filenameDate .= '-'.$dateRange['EndDate'];

		// return PDF as file download under the given filename
		return $pdf->Output($this->URLSegment.'-'.$filenameDate.'.pdf', 'D');
	}

}
