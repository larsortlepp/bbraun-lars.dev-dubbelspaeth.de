<?php
class MicrositePage extends Page implements PermissionProvider {
	
	private static $defaults = array(
		'CanViewType' => 'Inherit'
	);
	
	private static $allowed_children = array(
		'*MicrositePage', // only MicrositePage, no subclasses
		'MicrositeRedirectorPage'
	);
	
	private static $can_be_root = false;
	
	public function getCMSFields() {
		$f = parent::getCMSFields();

		$this->extend('updateCMSFields', $f);

		return $f;
	}
	
	/**
	 * Duplicate all has_many relations
	 */
	public function duplicate() {
		$clone = parent::duplicate();
		// uses function of class DuplicateHasManyExtension
		$clone = $this->duplicateHasManyRelations($this, $clone);
		return $clone;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_MICROSITEPAGE');
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_MICROSITEPAGE');
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		return Permission::check('DELETE_MICROSITEPAGE');
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MICROSITEPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite Seite" erstellen',
				'category' => 'Microsite',
				'sort' => 210
			),
			'DELETE_MICROSITEPAGE' => array(
				'name' => 'Kann Seitentyp "Microsite Seite" löschen',
				'category' => 'Microsite',
				'sort' => 220
			)
		);
	}
}

class MicrositePage_Controller extends Page_Controller {

}
