<?php

/**
 * RecipeSubmissionPage
 *
 * Object for user submitted recipe submissions
 *
 */
class RecipeSubmissionPage extends Page implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/recipesubmissionpage";

	private static $db = array(
		'VotingDate' => 'Date',
		'Publish' => 'Boolean'
	);

	private static $has_many = array(
		'RecipeSubmissionItems' => 'RecipeSubmissionItem'
	);

	private static $defaults = array(
		'ShowInMenus' => false,
		'ShowInSearch' => false
	);

	private static $allowed_children = 'none';

	private static $can_be_root = false;

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new HeaderField('RecipeSubmissionPage', 'Wunschgericht Übermittlung'), 'Title');

		$votingDateField = new DateField('VotingDate', 'Abstimmung für Datum');
		$votingDateField->setLocale(i18n::get_locale());
		$votingDateField->setConfig('showcalendar', true);
		$f->addFieldToTab('Root.Main', $votingDateField, 'Content');

		$f->addFieldToTab('Root.Main', new CheckboxField('Publish', 'Übermittlung Wunschgerichte aktivieren'));

		// Add Tab with RecipeSubmissionItems - only add it if we are working with a real object ($this->ID is set)
		if($this->ID) {
			MyGridField::create('RecipeSubmissionItems', 'Wunschgericht', $this->RecipeSubmissionItems())
				->setCols(array(
					'CreatedNice' => 'Datum',
					'Recipe' => 'Gericht',
					'Name' => 'Name',
					'Email' => 'E-Mail'
				))
				->setRowsPerPage(50)
				->addToTab($f, 'RecipeSubmissionItems');
			$f->fieldByName('Root.RecipeSubmissionItems')->setTitle('Wunschgerichte');
		}

		// remove fields
		$f->removeFieldFromTab('Root.Main', 'Title');
		$f->removeFieldFromTab('Root.Main', 'MenuTitle');
		$f->removeFieldFromTab('Root.Main', 'Content');

		return $f;
	}

	public function VotingDateFormat($format = '%d.%m.%Y') {
		return DateUtil::dateFormat($this->VotingDate, $format);
	}

	/**
	 * Set static Title an Menu Title: "Speiseplan 26.07.2012"
	 * Generate unique URLSegment: "speiseplan-2012-07-26"
	 *
	 */
	public function onBeforeWrite() {

		if($this->ID) {
			
			// parent ID (may not be set if not published)
			$parentID = ($this->parent) ? $this->parent->ID : false;

			// generate Title
			$title = 'Wunschgerichte';

			// generate Title
			$date = $this->VotingDate ? $this->VotingDate : 'Neu';
			$dateNice = $this->VotingDate ? DateUtil::dateFormat($this->VotingDate, '%d.%m.%Y') : 'Neu';

			// set Title and MenuTitle
			$this->Title = $title.' '.$dateNice;
			$this->MenuTitle = $title.' '.$dateNice;

			// generate URLSegment from title
			$urlSegment = SiteTree::generateURLSegment($date);

			// check if menu with this URLSegment exists
			// add number to the end of URLSegment end recheck if URLSegment still exists
			$count = 1;
			$stage = (Versioned::current_stage() == 'Live') ? '_Live' : '';
			while (RecipeSubmissionPage::get()->filter(array(
				'ParentID' => $parentID,
				'URLSegment' => $urlSegment,
			))->exclude(array(
				'ID' => $this->ID
			))->count() > 0) {
				$urlParts = explode('-', $urlSegment);
				// check if we already added a sequence number
				if(strlen($urlParts[count($urlParts)-1]) != 2 && $urlParts[count($urlParts)-1] != 'neu') {
					unset($urlParts[count($urlParts)-1]);
					$urlSegment = implode('-', $urlParts);
				}
				$urlSegment = $urlSegment . '-' . $count;
				$count++;
			}

			// add URLSegment to Object
			$this->URLSegment = $urlSegment;

		}

		parent::onBeforeWrite();
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = (Permission::check('DELETE_RECIPESUBMISSIONPAGE')) ? true : false;
		return $canView;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canView = (Permission::check('DELETE_RECIPESUBMISSIONPAGE')) ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'DELETE_RECIPESUBMISSIONPAGE' => array(
				'name' => 'Kann Seitentyp "Wunschgericht Übermittlung" löschen',
				'category' => 'Wunschgericht Übermittlung',
				'sort' => 10
			)
		);
	}
}

class RecipeSubmissionPage_Controller extends Page_Controller {

}
