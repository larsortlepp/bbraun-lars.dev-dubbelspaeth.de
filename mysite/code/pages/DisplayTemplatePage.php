<?php
class DisplayTemplatePage extends TemplatePage implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/displaytemplatepage";

	private static $db = array(
		'TemplateName' => 'Varchar(255)',
		'InfoMessagesOnly' => 'Boolean',
		'ReloadInterval' => 'Int',
		'TransitionDuration' => 'Int',
		'ShowScreenDuration' => 'Int',
		'TransitionEffect' => "Enum('fade,slide','fade')",
		'ScreenRotation' => "Enum('keine,90° im Uhrzeigersinn,90° gegen den Uhrzeigersinn','keine')",
		'DisplayTemplatePage_WeeklyMenuTableTemplate' => 'Boolean',
		'DisplayVisitorCounterData' => 'Boolean',

		// Timing
		'TimerStartDate' => 'Date',
		'TimerStartTime' => 'Time',
		'TimerEndDate' => 'Date',
		'TimerEndTime' => 'Time'
	);
	
	public static $has_one = array(
		'ImportVisitorCounterConfig' => 'ImportVisitorCounterConfig'
	);

	private static $has_many = array(
		'DisplayVideos' => 'DisplayVideo'
	);

	private static $many_many = array (
		// Timing
		'TimerRecurringDaysOfWeek' => 'RecurringDayOfWeek'
	);

	private static $defaults = array(
		'CanViewType' => 'Anyone',
		'ReloadInterval' => 300000, # every 5 minutes
		'TransitionDuration' => 2000,
		'ShowScreenDuration' => 12000,
		'TransitionEffect' => 'fade',
		'ScreenRotation' => 'keine',
		'DisplayVisitorCounterData' => 0
	);

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', new TextField('TemplateName', 'Template Name (MyTemplate)'), 'MenuContainerID');
		$f->addFieldToTab('Root.Main', new CheckboxField('DisplayTemplatePage_WeeklyMenuTableTemplate', 'Template für Wochenspeiseplan Bildschirm'), 'DatePeriod');
		$f->addFieldToTab('Root.Main', new TextareaField('FootNote', 'Text für Fußzeile'));

		// Timing
		$f->addFieldToTab('Root.Main', new HeaderField('TimingHeader', 'Zeitgesteuerte Anzeige (optional)'));

		$f->addFieldToTab(
			'Root.Main',
			TimeField::create(
				'TimerStartTime', 
				'Zeit Beginn'
			)
		);
		$f->addFieldToTab(
			'Root.Main',
			TimeField::create(
				'TimerEndTime', 
				'Zeit Ende'
			)
		);

		$f->addFieldToTab(
			'Root.Main', 
			$startDateField = DateField::create(
				'TimerStartDate', 
				'Datum Beginn'
			)
			->setLocale(i18n::get_locale())
			->setConfig('showcalendar', true)
		);
		$f->addFieldToTab(
			'Root.Main', 
			$endDateField = DateField::create(
				'TimerEndDate', 
				'Datum Ende'
			)
			->setLocale(i18n::get_locale())
			->setConfig('showcalendar', true)
		);

		$f->addFieldToTab('Root.Main', new CheckboxSetField('TimerRecurringDaysOfWeek', 'An folgenden Tagen anzeigen', RecurringDayOfWeek::map_days_of_week()));

		// Screen Settings
		$f->addFieldToTab('Root.Main', new HeaderField('ScreenSettingsHeader', 'Bildschirm Einstellungen'));
		$f->addFieldToTab('Root.Main', new CheckboxField('InfoMessagesOnly', 'Nur News anzeigen (keinen Speiseplan)'));
		$f->addFieldToTab('Root.Main', new DropdownField('ScreenRotation', 'Drehung Bildschirm', $this->dbObject('ScreenRotation')->enumValues()));
		$f->addFieldToTab('Root.Main', new NumericField('ReloadInterval', 'Intervall zum Neu laden (in Millisekunden)'));
		$f->addFieldToTab('Root.Main', new NumericField('TransitionDuration', 'Dauer Übergang (in Millisekunden)'));
		$f->addFieldToTab('Root.Main', new NumericField('ShowScreenDuration', 'Anzeigedauer (in Millisekunden)'));
		$f->addFieldToTab('Root.Main', new OptionsetField('TransitionEffect', 'Effekt für Übergang', $this->dbObject('TransitionEffect')->enumValues()));
		$f->addFieldToTab('Root.Main', CheckboxField::create('DisplayVisitorCounterData', 'Daten Besucherzähler anzeigen'));
		$f->addFieldToTab('Root.Main', DropdownField::create('ImportVisitorCounterConfigID', 'Quelle Besucherzähler', ImportVisitorCounterConfig::get()->map())->setEmptyString('-- Besucherzähler auswählen --'));

		// add preview link
		$f->addfieldToTab('Root.Main', new HeaderField('DisplayHeader', 'Vorschau Bildschirmausgabe'), 'Title');
		if($this->TemplateName && is_file(Director::baseFolder().'/themes/'.$this->SiteConfig->Theme.'/templates/'.$this->TemplateName.'.ss')) {
			$f->addFieldToTab('Root.Main', new LiteralField('DisplayLink'.$this->URLSegment, '<a href="'.$this->AbsoluteSubsiteLink($this->owner->SubsiteID).'" target="site" class="custom-button icon-monitor">Vorschau anzeigen</a>'), 'Title');
		}
		else {
			$f->addFieldToTab('Root.Main', new LiteralField('TemplateWarning', '<strong>Das Template (Template Name) existiert nicht.</strong> Es kann keine Vorschau angezeigt werden.'), 'Title');
		}

		// add GridField for Videos - only add it if we are working with a real object ($this->ID is set)
		if($this->ID) {
			MyGridField::create('DisplayVideos', 'Video', $this->DisplayVideos())
				->setCols(array(
					'Title' => 'Titel',
					'VideoType' => 'Typ',
					'CollapsedDateTimeGridField' => 'Anzeigedauer',
					'Publish' => 'aktiv'
				))
				->setRowsPerPage(25)
				->setAllowCustomSort(true)
				->addToTab($f, 'InfoMessages');
			$f->fieldByName('Root.InfoMessages')->setTitle('News / Videos');

			$f->addFieldToTab('Root.InfoMessages', new HeaderField('DisplayVideosHeader', 'Videos'), 'DisplayVideos');
			$f->addFieldToTab('Root.InfoMessages', new LiteralField('DisplayVideoInfo', '<p>Es kann nur genau ein Video aktiviert werden.<br /> Bei aktiviertem Video werden keine News angezeigt.</p>'), 'DisplayVideos');
			$f->addFieldToTab('Root.InfoMessages', new HeaderField('InfoMessagesHeader', 'News'), 'InfoMessages');
		}
		
		// PERMISSIONS

		if(
			!Permission::check('VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE') &&
			!Permission::check('EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE') &&
			!Permission::check('CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE') &&
			!Permission::check('DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE')
		) {
			$f->removeFieldFromTab('Root', 'InfoMessages');
		}
		
		// TemplateName
		if(!Permission::check('TEMPLATEPAGE_EDIT_TEMPLATENAME')) {
			$f->replaceField('TemplateName', $f->fieldByName('Root.Main.TemplateName')->performReadonlyTransformation());
		}
		
		// Footer
		if(!Permission::check('TEMPLATEPAGE_EDIT_FOOTER')) {
			$f->replaceField('FootNote', $f->fieldByName('Root.Main.FootNote')->performReadonlyTransformation());
		}
		
		// Timer Settings
		if(!Permission::check('DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS')) {
			$f->replaceField('TimerStartTime', $f->fieldByName('Root.Main.TimerStartTime')->performReadonlyTransformation());
			$f->replaceField('TimerEndTime', $f->fieldByName('Root.Main.TimerEndTime')->performReadonlyTransformation());
			$f->replaceField('TimerStartDate', $f->fieldByName('Root.Main.TimerStartDate')->performReadonlyTransformation());
			$f->replaceField('TimerEndDate', $f->fieldByName('Root.Main.TimerEndDate')->performReadonlyTransformation());
			$f->replaceField('TimerRecurringDaysOfWeek', $f->fieldByName('Root.Main.TimerRecurringDaysOfWeek')->performDisabledTransformation()); // disabled transformation for all radio buttons, checkboxes, etc.
		}
		
		// Show InfoMessage only
		if(!Permission::check('DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY')) {
			$f->replaceField('InfoMessagesOnly', $f->fieldByName('Root.Main.InfoMessagesOnly')->performDisabledTransformation()); // disabled transformation for all radio buttons, checkboxes, etc.
		}
		
		// Display Settings
		if(!Permission::check('DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS')) {
			$f->replaceField('ScreenRotation', $f->fieldByName('Root.Main.ScreenRotation')->performReadonlyTransformation());
			$f->replaceField('ReloadInterval', $f->fieldByName('Root.Main.ReloadInterval')->performReadonlyTransformation());
			$f->replaceField('TransitionDuration', $f->fieldByName('Root.Main.TransitionDuration')->performReadonlyTransformation());
			$f->replaceField('ShowScreenDuration', $f->fieldByName('Root.Main.ShowScreenDuration')->performReadonlyTransformation());
			$f->replaceField('TransitionEffect', $f->fieldByName('Root.Main.TransitionEffect')->performReadonlyTransformation());
			$f->replaceField('DisplayVisitorCounterData', $f->fieldByName('Root.Main.DisplayVisitorCounterData')->performReadonlyTransformation());
			$f->replaceField('ImportVisitorCounterConfigID', $f->fieldByName('Root.Main.ImportVisitorCounterConfigID')->performReadonlyTransformation());
		}
		
		//ImportVisitorCounterConfig permission
		if(!Permission::check('VIEW_IMPORTVISITORCOUNTER')) {
			$f->removeByName('ImportVisitorCounterConfigID');  
        }
		
		return $f;
	}

	/**
	 * Functions returns class name for body tag, if rotate=true ist set via URL
	 * (e.g. mydomain/templatename?rotate=true)
	 *
	 * @return String
	 */
	public function rotateParam() {
		return array_key_exists('rotate', $_GET) ? 'rotated' : false;
	}

	// TIMING
	/**
	 * Functions returns true if DisplayTemplatePage (Menu) is setup to be displayed at the given date and time
	 * If no date and time is given, the current date and time will be used.
	 * If no date or time is set on InfoMessage, function will return true.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:00:30'
	 * @return Boolean	true if InfoMessage is set to be displayed at the given date and time, else: false
	 */
	public function ShowAtDateTime($datetime = null) {
		$datetime = $datetime ? $datetime : date('Y-m-d H:i:s');
		$time = strtotime($datetime);
		$timeHIS = date('H:i:s', $time);
		$date = date('Y-m-d', $time);

		// check if time is set and is NOT in given time span
		if(
			$this->TimerStartTime && $this->TimerEndTime &&
			($timeHIS < $this->TimerStartTime || $timeHIS > $this->TimerEndTime)
		) {
			return false;
		}

		// check if given date is NOT in between given date span
		if(
			$this->TimerStartDate && $this->TimerEndDate &&
			($date < $this->TimerStartDate || $date > $this->TimerEndDate)

		) {
			return false;
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			// get day number from given date
			$dayNum = date('w', $time);
			// check if day number is selected for current InfoMessage
			if($recurringDays->count() >= 1 && !$recurringDays->find('Value', $dayNum)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Return the single published DisplayVideo
	 * (only 1 DisplayVideo per DisplayTemplatePage is allowed)
	 *
	 * @return DataObject
	 */
	public function DisplayVideo() {
		return DisplayVideo::get()->filter(array('DisplayTemplatePageID' => $this->ID, 'Publish' => 1))->First();
	}

	/**
	 * Returns a single DisplayVideo for the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return ArrayList
	 */
	public function DisplayVideoAtDateTime($datetime = null) {
		$displayVideos = DisplayVideo::get()->filter(array('DisplayTemplatePageID' => $this->ID, 'Publish' => 1));
		$returnDisplayVideo = new ArrayList();
		if(count($displayVideos) >= 1) {
			foreach($displayVideos as $displayVideo) {
				if($displayVideo->ShowAtDateTime($datetime)) {
					$returnDisplayVideo->push($displayVideo);
				}
			}
		}
		return $returnDisplayVideo->count() >= 1 ? $returnDisplayVideo->First() : false;
	}

	/**
	 * RenderMenu
	 * returns true if previewStartDate or ShowAtDateTime is set
	 * --> only show at set DateTime in Live mode, always show in preview mode
	 *
	 * @return Boolean
	 */
	public function RenderMenu() {
		return ($this->ShowAtDateTime() || $this->getPreviewStartDate()) ? true : false;
	}

	/**
	 * RenderInfoMessages
	 * returns true if previewStartDate is set and InfoMessages exist
	 * --> always show InfoMessages in preview mode - no matter what their schedule is
	 *
	 * @return Boolean
	 */
	public function RenderInfoMessages() {
		return ($this->getPreviewStartDate() && $this->InfoMessagesPublished()->count() >= 1) ? true : false;
	}

	/**
	 * RenderDisplayVideos
	 * returns true if previewStartDate is set and a published DisplayVideo exist
	 * --> always show DisplayVideos in preview mode - no matter what their schedule is
	 *
	 * @return Boolean
	 */
	public function RenderDisplayVideos() {
		return ($this->getPreviewStartDate() && $this->DisplayVideo()) ? true : false;
	}

	/**
	 * Returns Classname depending on ScreenRotation
	 *
	 * @return String
	 */
	public function ScreenRotationClass() {
		$className = false;
		if($this->ScreenRotation == '90° im Uhrzeigersinn') $className = 'rotated__clockwise';
		if($this->ScreenRotation == '90° gegen den Uhrzeigersinn') $className = 'rotated';
		return $className;
	}

	/**
	 * Return TemplatePage (of the current Class) that has "WeeklyMenuTableTemplate" variable set
	 *
	 * Needed for Rendering WeeklyMenuTable: You can set the FoodCategories on the Template
	 * and use
	 * @return DataList
	 */
	public function WeeklyMenuTableTemplate() {
		return DisplayTemplatePage::get()->filter('DisplayTemplatePage_WeeklyMenuTableTemplate', 1)
		                                 ->filter('ParentID', $this->parent->ID);
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_DISPLAYTEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_DISPLAYTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canDelete = (Permission::check('DELETE_DISPLAYTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_DISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Bildschirm Ausgabe" erstellen',
				'category' => 'Bildschirm Ausgabe',
				'sort' => 10
			),
			'DELETE_DISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Bildschirm Ausgabe" löschen',
				'category' => 'Bildschirm Ausgabe',
				'sort' => 20
			),
			'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS' => array(
				'name' => 'Kann Einstellungen Zeitgesteuerte Anzeige bearbeiten',
				'category' => 'Bildschirm Ausgabe',
				'sort' => 110
			),
			'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY' => array(
				'name' => 'Kann Feld nur News anzeigen bearbeiten',
				'category' => 'Bildschirm Ausgabe',
				'sort' => 120
			),
			'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS' => array(
				'name' => 'Kann Bildschirm Einstellungen bearbeiten',
				'category' => 'Bildschirm Ausgabe',
				'sort' => 130
			)
			
		);
	}
}

class DisplayTemplatePage_Controller extends TemplatePage_Controller {

	private static $allowed_actions = array(
		'ajax'
	);

	/**
	 * Render Page with given TemplateName
	 */
	public function index() {
		// generate Array with suitable templates
		$templates = array(
			$this->ClassName,
			'Page'
		);
		if($this->TemplateName && is_file(Director::baseFolder().'/themes/'.$this->SiteConfig->Theme.'/templates/'.$this->TemplateName.'.ss')) array_unshift($templates, $this->TemplateName);
		return $this->renderWith($templates);
	}

	/**
	 * Adds anajax Controller to the current class
	 * The Ajax call renders the Page with $this->TemplateName + '_ajax'
	 *
	 * if $this->TemplateName = 'MyTemplate' ajax will use MyTemplate_ajax.ss
	 */
	public function ajax() {
		// Flush template cache
		SSViewer::flush_template_cache();
		// render with template
		return $this->renderWith(array($this->TemplateName.'_ajax'));
	}

}
