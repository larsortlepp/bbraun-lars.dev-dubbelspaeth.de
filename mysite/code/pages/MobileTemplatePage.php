<?php
class MobileTemplatePage extends TemplatePage implements PermissionProvider {
	
	static $hide_ancestor = "TemplatePage"; // hide parent meta class from create dropdowns

	// set icon for display in sitetree
	static $icon = "mysite/images/treeicons/mobiletemplatepage";

	public static $db = array(
		'TemplateName' => 'Varchar(255)',
		'GoogleAnalyticsID' => 'Varchar(20)',
		'EnableDishRatings' => 'Boolean',
		'ShowOpeningHours' => 'Boolean',
		'ShowLegend' => 'Boolean',
		'CacheLifetime' => 'Int', // Cache Lifetime in minutes
		'DisplayVisitorCounterData' => 'Boolean'
	);
	
	public static $has_one = array(
		'ImportVisitorCounterConfig' => 'ImportVisitorCounterConfig'
	);
	
	private static $has_many = array(
		'InfoMessages' => 'InfoMessageMobileTemplatePage'
	);
	
	public static $many_many = array(
		'MenuContainers' => 'MenuContainer'
	);

	public static $defaults = array(
		'CanViewType' => 'Anyone',
		'EnableDishRatings' => 1,
		'ShowOpeningHours' => 1,
		'ShowLegend' => 1,
		'CacheLifetime' => 10,
		'DisplayVisitorCounterData' => 0
	);
	
	public static $allowed_children = array(
		'MobileInfoPage'
	);

	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$siteConfig = $this->CurrentSiteConfig();	
		
		// add fields
		$menuContainersMap = ($menuContainers = MenuContainer::get()->filter('ParentID', $this->parent->ID)) ? $menuContainers->map()->toArray() : false;
		$f->addFieldToTab('Root.Main', new CheckboxSetField('MenuContainers', 'Speisepläne', $menuContainersMap), 'MenuContainerID');
		
		$f->addFieldToTab('Root.Main', new TextField('TemplateName', 'Template Name (MyTemplate)'), 'MenuContainers');
		$f->addFieldToTab('Root.Main', new TextareaField('FootNote', 'Text für Fußzeile'));

		$f->addFieldToTab('Root.Main', new HeaderField('WebAppSettingsHeader', 'Web-App Einstellungen'));
		$f->addFieldToTab('Root.Main', new CheckboxField('ShowOpeningHours', 'Öffnungszeiten anzeigen'));
		$f->addFieldToTab('Root.Main', new CheckboxField('ShowLegend', 'Legende für Zusatzstoffe / Allergene / Kennzeichnungen anzeigen'));
		$f->addFieldToTab('Root.Main', new CheckboxField('EnableDishRatings', 'Bewertung der Gerichte aktivieren'));
		$f->addFieldToTab('Root.Main', new TextField('GoogleAnalyticsID', 'Google Analytics Tracking ID'));
		$f->addFieldToTab('Root.Main', new NumericField('CacheLifetime', 'Cache Dauer (Minuten)'));
		$f->addFieldToTab('Root.Main', CheckboxField::create('DisplayVisitorCounterData', 'Daten Besucherzähler anzeigen'));
		$f->addFieldToTab('Root.Main', DropdownField::create('ImportVisitorCounterConfigID', 'Quelle Besucherzähler', ImportVisitorCounterConfig::get()->map())->setEmptyString('-- Besucherzähler auswählen --'));

		// add preview link for mobile
		$f->addfieldToTab('Root.Main', new HeaderField('MobileHeader', 'Vorschau Mobile Ausgabe'), 'Title');
		if($this->TemplateName && is_file(Director::baseFolder().'/themes/'.$siteConfig->Theme.'/templates/'.$this->TemplateName.'.ss')) {
			$f->addFieldToTab('Root.Main', new LiteralField('MobileLink'.$this->URLSegment, '<a href="'.$this->AbsoluteSubsiteLink($this->owner->SubsiteID).'" target="site" class="custom-button icon-mobile-3">Vorschau Mobile Ausgabe</a>'), 'Title');
		}
		else {
			$f->addFieldToTab('Root.Main', new LiteralField('TemplateWarning','<strong>Das Template ('.$this->TemplateName.'.ss) existiert nicht. </strong> Es kann keine Vorschau angezeigt werden.'), 'Title');
		}
		
		// PERMISSIONS
		
		// remove fields
		$f->removeFieldFromTab('Root.Main', 'MenuContainerID');
		if(
			!Permission::check('VIEW_INFOMESSAGEMOBILETEMPLATEPAGE') &&
			!Permission::check('EDIT_INFOMESSAGEMOBILETEMPLATEPAGE') &&
			!Permission::check('CREATE_INFOMESSAGEMOBILETEMPLATEPAGE') &&
			!Permission::check('DELETE_INFOMESSAGEMOBILETEMPLATEPAGE')
		) {
			$f->removeFieldFromTab('Root', 'InfoMessages');
		}
		
		// MenuContainer
		if(!Permission::check('TEMPLATEPAGE_EDIT_DATETIMEPERIOD')) {
			$f->replaceField('MenuContainers', $f->fieldByName('Root.Main.MenuContainers')->performDisabledTransformation());
		}
		
		// TemplateName
		if(!Permission::check('TEMPLATEPAGE_EDIT_TEMPLATENAME')) {
			$f->replaceField('TemplateName', $f->fieldByName('Root.Main.TemplateName')->performReadonlyTransformation());
		}
		
		// Footer
		if(!Permission::check('TEMPLATEPAGE_EDIT_FOOTER')) {
			$f->replaceField('FootNote', $f->fieldByName('Root.Main.FootNote')->performReadonlyTransformation());
		}

		// Web-App settings (enable/disable functionality)
		if(!Permission::check('MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY')) {
			$f->replaceField('ShowOpeningHours', $f->fieldByName('Root.Main.ShowOpeningHours')->performDisabledTransformation());
			$f->replaceField('ShowLegend', $f->fieldByName('Root.Main.ShowLegend')->performDisabledTransformation());
			$f->replaceField('EnableDishRatings', $f->fieldByName('Root.Main.EnableDishRatings')->performDisabledTransformation());
			$f->replaceField('DisplayVisitorCounterData', $f->fieldByName('Root.Main.DisplayVisitorCounterData')->performReadonlyTransformation());
			$f->replaceField('ImportVisitorCounterConfigID', $f->fieldByName('Root.Main.ImportVisitorCounterConfigID')->performReadonlyTransformation());
			
		}
		
		// GoogleAnalyticsID
		if(!Permission::check('MOBILETEMPLATEPAGE_EDIT_GOOGLEANALYTICSID')) {
			$f->replaceField('GoogleAnalyticsID', $f->fieldByName('Root.Main.GoogleAnalyticsID')->performReadonlyTransformation());
		}
		
		// CacheLifetime
		if(!Permission::check('MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME')) {
			$f->replaceField('CacheLifetime', $f->fieldByName('Root.Main.CacheLifetime')->performReadonlyTransformation());
		}
		
		//ImportVisitorCounterConfig permission
		if(!Permission::check('VIEW_IMPORTVISITORCOUNTER')) {
			$f->removeByName('ImportVisitorCounterConfigID');  
        }
		
		return $f;
	}
	
	/**
	 * Returns all DailyMenuPages for the date span set in the CMS (1-n days or weeks), starting from the current day / current week
	 * Adds a String with IDs of all selected FoodCategories to every DailyMenuPage.
	 * This way we can call DailyMenuPage->FoodCategories() with the preselected FoodCategories from the CMS.
	 *
	 * If DateRange is "Wochen", a DailyMenuPage per Day will be returned, even if there is no actual Page in the SiteTree.
	 * (neccessary for consistent rendering of Data Tables)
	 *
	 * @param Integer $menuContainerID mandatory: ID of the MenuContainer the DailyMenuPages should be rendered from
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return ArrayList
	 */
	public function DailyMenuPagesForMenuContainerID($menuContainerID = null, $datePeriod = null, $dateRange = null) {
		
		// if no menuContainerID is set exit
		if(!$menuContainerID) return false;

		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange);

		// implode IDs of all selected FoodCateogiers to a comma seperated string
		$foodCategoryIDs = $this->FoodCategoryIDs();

		// return the DailyMenuPages for the date range and FoodCategories
		$menuContainer = MenuContainer::get()->byID($menuContainerID);
		$dailyMenuPages = $menuContainer->DailyMenuPagesByCategoryAndDate($foodCategoryIDs, $dateRange['StartDate'], $dateRange['EndDate']);

		return $dailyMenuPages;
	}
	

	/*
	 * Returns RestaurantPages' MobileTemplatePages sharing a template name.
	 *
	 * This function is used in the Mobile_RestaurantOverview template to
	 * provide a restaurant selection, if necessary. It looks across all
	 * RestaurantPages for MobileTemplatePages sharing the same template
	 * name. If only one occurance is found, false is returned.
	 *
	 * @param string $TemplateName Optional parameter which, if specified, is
	 * used instead of the current MobileTemplatePage's template name.
	 * @return ArrayList | false
	 */
	public function MultipleMobileTemplatePagesWithSharedName($TemplateName = false) {
		$restaurantPages = RestaurantPage::get();
		if ($restaurantPages->Count() <= 1)
			return false;

		$mobileTemplatePagesWithSameName = new ArrayList();
		foreach ($restaurantPages as $restaurantPage) {
			$mobileTemplatePage = MobileTemplatePage::get()->filter(array(
				'ParentID' => $restaurantPage->ID,
				'TemplateName' => $TemplateName ? $TemplateName : $this->TemplateName
			))->First();
			if ($mobileTemplatePage) {
				$mobileTemplatePage->Domain = $mobileTemplatePage->SubsiteDomain(Subsite::currentSubsite()->ID);
				$mobileTemplatePage->AjaxURL = $mobileTemplatePage->Domain.$mobileTemplatePage->Link().'ajax';
				$mobileTemplatePage->RestaurantTitle = $restaurantPage->Title;
				$mobileTemplatePage->RestaurantPageID = $restaurantPage->ID;
				$mobileTemplatePagesWithSameName->push($mobileTemplatePage);
			}
		}

		return $mobileTemplatePagesWithSameName->Count() > 1 ? $mobileTemplatePagesWithSameName : false;
	}

	/**
	 * Returns the currently active DishVotingPage for the current restaurant.
	 *
	 * @return DataObject
	 */
	public function CurrentDishVotingPage() {
		// get DishVotingContainer belonging to RestaurantPage
		$dishVotingContainer = DishVotingContainer::get()->filter(array(
			'ParentID' => $this->parent->ID
		))->First();

		if($dishVotingContainer) {
			return DishVotingPage::get()->filter(array(
				'ParentID' => $dishVotingContainer->ID,
				'Publish' => 1,
				'VotingDate:GreaterThan' => date('Y-m-d')
			))->sort('VotingDate', 'ASC')->First();
		}
		else {
			return false;
		}
	}

	/**
	 * Returns the currently active RecipeSubmissionPage for the current
	 * restaurant.
	 *
	 * @return DataObject
	 */
	public function CurrentRecipeSubmissionPage() {
		// get RecipeSubmissionContainer belonging to RestaurantPage
		$recipeSubmissionContainer = RecipeSubmissionContainer::get()->filter(array(
			'ParentID' => $this->parent->ID
		))->First();

		if($recipeSubmissionContainer) {
			return RecipeSubmissionPage::get()->filter(array(
				'ParentID' => $recipeSubmissionContainer->ID,
				'Publish' => 1,
				'VotingDate:GreaterThan' => date('Y-m-d')
			))->sort('VotingDate', 'ASC')->First();
		}
		else {
			return false;
		}
	}
	
	/**
	 * Returns a number that can be used for caching
	 * A new number is returned every x Minutes.
	 * The Cache lifetime in minutes can be defined in template via $this->CacheLifetime
	 * 
	 * @return Integer
	 */
	public function MobileTemplatePageCacheTime() {
		$cacheLifetime = ($this->CacheLifetime > 0) ? $this->CacheLifetime : MobileTemplatePage::$defaults['CacheLifetime']; // set default cache lifetime is value is 0
		return (int)(time() / 60 / $cacheLifetime); // Returns a new number every x minutes
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_MOBILETEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_MOBILETEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canDelete = (Permission::check('DELETE_MOBILETEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_MOBILETEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Mobile Ausgabe" erstellen',
				'category' => 'Mobile Ausgabe',
				'sort' => 10
			),
			'DELETE_MOBILETEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Mobile Ausgabe" löschen',
				'category' => 'Mobile Ausgabe',
				'sort' => 20
			),
			'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY' => array(
				'name' => 'Kann Funktionsumfang Web-App bearbeiten',
				'category' => 'Mobile Ausgabe',
				'sort' => 110
			),
			'MOBILETEMPLATEPAGE_EDIT_GOOGLEANALYTICSID' => array(
				'name' => 'Kann Google Analytics ID bearbeiten',
				'category' => 'Mobile Ausgabe',
				'sort' => 120
			),
			'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME' => array(
				'name' => 'Kann Cache Dauer bearbeiten',
				'category' => 'Mobile Ausgabe',
				'sort' => 130
			)
		);
	}
}

class MobileTemplatePage_Controller extends TemplatePage_Controller {
	private static $allowed_actions = array(
		'ajax',
		'vote',
		'votingresults',
		'dishrating',
		'dishratingmultiple',
		'recipesubmission',
	);

	/**
	 * Render Page with given TemplateName
	 */
	public function index() {
		$siteConfig = $this->CurrentSiteConfig();
		// generate Array with suitable templates
		$templates = array(
			$this->ClassName,
			'Page'
		);
		if($this->TemplateName && is_file(Director::baseFolder().'/themes/'.$siteConfig->Theme.'/templates/'.$this->TemplateName.'.ss')) array_unshift($templates, $this->TemplateName);
		return $this->renderWith($templates);
	}

	/**
	 * Adds an ajax Controller to the current class
	 * The Ajax call renders the Page with $this->TemplateName + '_ajax'
	 *
	 * if $this->TemplateName = 'MyTemplate' ajax will use MyTemplate_ajax.ss
	 */
	public function ajax() {
		// Flush template cache
		SSViewer::flush_template_cache();

		// render with template
		return $this->renderWith(array($this->TemplateName.'_ajax'));
	}

	/**
	 * Process voting requests
	 * Add 1 vote per Request to the given DishVotingItem
	 *
	 * URL Request: /vote?id=DishVotingItem.ID
	 * e.g. /vote?id=1
	 *
	 * return an html snippet with the current voting results
	 * Use Template $this->TemplateName.'_votingresults'
	 * e.g. Mobile_WeeklyMenu_de_DE_votingresults
	 *
	 * @return String
	 */
	public function vote() {
		// get URL parameter
		$dishVotingItemID = Convert::raw2sql($_REQUEST['id']);

		// get current dishvotingitem
		$dishVotingItem = DishVotingItem::get()->byID($dishVotingItemID);
		if($dishVotingItem) {
			// count +1 vote
			$votes = $dishVotingItem->Votes;
			$votes++;
			// save updated vote to database
			$dishVotingItem->Votes = $votes;
			$dishVotingItem->write();
		}

		// render with template
		return $this->votingresults();
	}

	/**
	 * Returns HTML with current voting results
	 * Add 1 vote per Request to the given DishVotingItem
	 *
	 * @return String
	 */
	public function votingresults() {
		// render with template
		return $this->renderWith(array($this->TemplateName.'_votingresults'));
	}

	/**
	 * Process dish ratings
	 * Process form data with rating for a dish (sent via POST)
	 *
	 * URL Request: /dishrating
	 *
	 * return an html snippet with the sent rating results and inactive form
	 * Use Template $this->TemplateName.'_dishrating'
	 * e.g. Mobile_WeeklyMenu_de_DE_dishrating
	 *
	 * @return String
	 */
	public function dishrating() {
		// get form data
		$restaurantPageID = Convert::raw2sql($_POST['restaurantpageid']);
		$menuContainerID = Convert::raw2sql($_POST['menucontainerid']);
		$dishID = Convert::raw2sql($_POST['dishid']);
		$dailymenudate = Convert::raw2sql($_POST['dailymenudate']);
		$taste = Convert::raw2sql($_POST['taste']);
		$value = Convert::raw2sql($_POST['value']);
		$service = Convert::raw2sql($_POST['service']);
		$comment = $_POST['comment'];

		// check for required fields
		if($restaurantPageID && $dishID) {
			
			$dish = Dish::get()->byID($dishID);
			// get corresponding FoodCategory of Dish
			$foodCategory = $dish->getFoodCategory();			
			
			$dishRating = new DishRating();
			$dishRating->RestaurantPageID = $restaurantPageID;
			$dishRating->MenuContainerID = $menuContainerID;
			$dishRating->Date = $dailymenudate;
			$dishRating->DishDescription = $dish->Description;
			$dishRating->FoodCategoryTitle = $foodCategory->Title;
			$dishRating->DishID = $dishID;
			$dishRating->Taste = $taste;
			$dishRating->Value = $value;
			$dishRating->Service = $service;
			if(strlen($comment) >= 1) $dishRating->Comment = $comment;
			$dishRating->write();

			// add form variables to template data
			$this->customise(array(
				'Dish' => $dish,
				'DishRatingDishID' => $dishID,
				'DishRatingTaste' => $taste,
				'DishRatingValue' => $value,
				'DishRatingService' => $service,
				'DishRatingComment' => nl2br($comment),
				'MenuContainerID' => $menuContainerID,
				'Date' => $dailymenudate
			));

			// render with template
			return $this->renderWith(array($this->TemplateName.'_dishrating'));
		}
		else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Process dish ratings
	 * Process form data with rating for a dish (sent via POST)
	 *
	 * URL Request: /dishvoting
	 *
	 * returns no html template (as function above) but sends data to DB
	 *
	 * @return String
	 */
	 
	public function dishratingmultiple() {
		// get form data
		$restaurantPageID = Convert::raw2sql($_POST['restaurantpageid']);
		$menuContainerID = Convert::raw2sql($_POST['menucontainerid']);
		$dishID = Convert::raw2sql($_POST['dishid']);
		$dailymenudate = Convert::raw2sql($_POST['dailymenudate']);
		$taste = Convert::raw2sql($_POST['taste']);
		$value = Convert::raw2sql($_POST['value']);
		$service = Convert::raw2sql($_POST['service']);
		$comment = $_POST['comment'];

		// check for required fields
		if($restaurantPageID && $dishID) {
			
			$dish = Dish::get()->byID($dishID);
			// get corresponding FoodCategory of Dish
			$foodCategory = $dish->getFoodCategory();			
			
			$dishRating = new DishRating();
			$dishRating->RestaurantPageID = $restaurantPageID;
			$dishRating->MenuContainerID = $menuContainerID;
			$dishRating->Date = $dailymenudate;
			$dishRating->DishDescription = $dish->Description;
			$dishRating->FoodCategoryTitle = $foodCategory->Title;
			$dishRating->DishID = $dishID;
			$dishRating->Taste = $taste;
			$dishRating->Value = $value;
			$dishRating->Service = $service;
			if(strlen($comment) >= 1) $dishRating->Comment = $comment;
			$dishRating->write();

			// add form variables to template data
			$this->customise(array(
				'Dish' => $dish,
				'DishRatingDishID' => $dishID,
				'DishRatingTaste' => $taste,
				'DishRatingValue' => $value,
				'DishRatingService' => $service,
				'DishRatingComment' => nl2br($comment),
				'MenuContainerID' => $menuContainerID,
				'Date' => $dailymenudate
			));

			// return nothing...
			
		}
		else {
			return false;
		}
	}
	

	/**
	 * Process recipe submissions
	 * Process form data with recipe submission (sent via POST)
	 *
	 * URL Request: /recipesubmission
	 *
	 * return an html snippet with a thank you message and the sent form data
	 * Use Template $this->TemplateName.'_recipesubmission'
	 * e.g. Mobile_WeeklyMenu_de_DE_recipesubmission
	 *
	 * @return String
	 */
	public function recipesubmission() {
		// get form data
		$recipeSubmissionPageID = $_POST['recipesubmissionpageid'];
		$recipe = $_POST['recipe'];
		$name = $_POST['name'];
		$email= $_POST['email'];

		// check for required fields
		if($recipeSubmissionPageID && $recipe) {
			$recipeSubmission = new RecipeSubmissionItem();
			$recipeSubmission->RecipeSubmissionPageID = Convert::raw2sql($recipeSubmissionPageID);
			$recipeSubmission->Recipe = $recipe;
			if(strlen($name) >= 1) $recipeSubmission->Name = Convert::raw2sql($name);
			if(strlen($email) >= 1) $recipeSubmission->Email = Convert::raw2sql($email);
			$recipeSubmission->write();

			// add form variables to template data
			$this->customise(array(
				'Recipe' => nl2br($recipe),
				'Name' => $name,
				'Email' => $email
			));

			// render with template
			return $this->renderWith(array($this->TemplateName.'_recipesubmission'));
		}
		else {
			return false;
		}
	}
	
	/**
	 * Returns all child MobileInfoPages
	 * @return MobileInfoPages
	 */
	public function MobileInfoPages() {
		return MobileInfoPage::get()->filter(array('ParentID' => $this->ID));
	}
}
