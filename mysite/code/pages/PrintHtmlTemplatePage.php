<?php
class PrintHtmlTemplatePage extends TemplatePage implements PermissionProvider {

	// set icon for display in sitetree
	private static $icon = "mysite/images/treeicons/printtemplatepage";
	
	public static $db = array(
		'TemplateName' => 'Varchar(255)',
		'DocumentWidth' => 'Int',
		'DocumentHeight' => 'Int',
		'SizeUnit' => "Enum('mm,px','mm')"
	);

	public static $has_one = array(
		'HeaderImage' => 'Image'
	);
	
	private static $has_many = array(
		'InfoMessages' => 'InfoMessagePrintTemplatePage'
	);

	public function getCMSFields() {
		$f = parent::getCMSFields();

		// add fields
		$f->addFieldToTab('Root.Main', TextField::create('TemplateName', 'Template Name')->setDescription('ohne Dateiendung (z.B. Print_DinA4)'), 'StartDateType');
		$f->addFieldToTab('Root.Main', 
				FieldGroup::create(
					DropdownField::create('SizeUnit', '', singleton('PrintHtmlTemplatePage')->dbObject('SizeUnit')->enumValues())
				)
					->setTitle('Maßeinheit')
					->setName('SizeUnitFieldGroup'),
				'StartDateType' 
		);
		$f->addFieldToTab('Root.Main', NumericField::create('DocumentWidth', 'Breite Dokument'), 'StartDateType');
		$f->addFieldToTab('Root.Main', NumericField::create('DocumentHeight', 'Höhe Dokument'), 'StartDateType');
		// Image
		$f->addFieldToTab(
			'Root.Main',
			CustomUploadField::create(
				'HeaderImage',
				'Bild'
			)
				->setDescription('jpg, gif, png')
				->setAllowedExtensions(array('jpg', 'gif', 'png'))
				->setFolderName(SiteConfig::current_site_config()->DefaultFolderPrintPath(RestaurantPage::getNearestRestaurantPageID())) // set folder to default dish image folder
		);
		$f->addFieldToTab('Root.Main', TextareaField::create('FootNote', 'Text für Fußzeile'));

		// add print link if the user has permission and method exists
		if(Permission::check('PRINTHTMLTEMPLATEPAGE_VIEW_PRINTLINK')) {
			$f->addFieldToTab('Root.Main', HeaderField::create('PrintHeader', 'Druckausgabe'), 'Title');
			if($this->validController()) {
				$f->addFieldToTab('Root.Main', LiteralField::create('PrintLink', '<a href="'.$this->AbsoluteSubsiteLink($this->owner->SubsiteID).'" target="_blank" class="custom-button icon-doc-3">PDF drucken</a>'), 'Title');
			} else {
				$f->addFieldToTab('Root.Main', LiteralField::create('PrintLinkMessage','<strong>Das angegebene Template (Template Name) existiert nicht.</strong> PDF Generierung nicht möglich.'), 'Title');
			}
		}
		
		// PERMISSIONS
		
		// hide InfoMessage tab if user has no permissions
		if(
			!Permission::check('VIEW_INFOMESSAGEPRINTTEMPLATEPAGE') &&
			!Permission::check('EDIT_INFOMESSAGEPRINTTEMPLATEPAGE') &&
			!Permission::check('CREATE_INFOMESSAGEPRINTTEMPLATEPAGE') &&
			!Permission::check('DELETE_INFOMESSAGEPRINTTEMPLATEPAGE')
		) {
			$f->removeFieldFromTab('Root', 'InfoMessages');
		}
		
		// can NOT edit field -> readonly field
		if(!Permission::check('TEMPLATEPAGE_EDIT_FIELD_TEMPLATENAME')) {
			$f->replaceField('TemplateName', $f->fieldByName('Root.Main.TemplateName')->performReadonlyTransformation());
		}
		
		// can NOT view and NOT edit field -> hidden field
		if(!Permission::check('PRINTHTMLTEMPLATEPAGE_VIEW_FIELDS_DIMENSIONS') && !Permission::check('PRINTHTMLTEMPLATEPAGE_EDIT_FIELDS_DIMENSIONS')) {
			$f->replaceField('SizeUnit', HiddenField::create('SizeUnit', '', $this->SizeUnit));
			$f->removeByName('SizeUnitFieldGroup');
			$f->replaceField('DocumentWidth', HiddenField::create('DocumentWidth', '', $this->DocumentWidth));
			$f->replaceField('DocumentHeight', HiddenField::create('DocumentHeight', '', $this->DocumentHeight));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('PRINTHTMLTEMPLATEPAGE_EDIT_FIELDS_DIMENSIONS') && Permission::check('PRINTHTMLTEMPLATEPAGE_VIEW_FIELDS_DIMENSIONS')) {
			$f->replaceField('SizeUnit', ReadonlyField::create('SizeUnit', '', $this->SizeUnit));
			$f->replaceField('DocumentWidth', $f->fieldByName('Root.Main.DocumentWidth')->performReadonlyTransformation());
			$f->replaceField('DocumentHeight', $f->fieldByName('Root.Main.DocumentHeight')->performReadonlyTransformation());
		}
		
		// can NOT edit field -> remove field
		if(!Permission::check('PRINTHTMLTEMPLATEPAGE_EDIT_FIELD_HEADERIMAGE')) {
			// remove Image - otherwise ID of null will be saved - which unlinks an assigned image
			$f->removeByName('HeaderImage');
		}
		
		// Footer
		if(!Permission::check('TEMPLATEPAGE_EDIT_FOOTER')) {
			$f->replaceField('FootNote', $f->fieldByName('Root.Main.FootNote')->performReadonlyTransformation());
		}

		return $f;
	}
	
	
	/**
	 * Functions checks if $this->TemplateName is valid
	 * -> controller needs to exist
	 * -> corresponding html template needs to exist
	 * 
	 * @return boolean
	 */
	public function validController() {
		if(
			$this->TemplateName &&
			(
				is_file(Director::baseFolder().'/themes/'.$this->SiteConfig->Theme.'/templates/'.$this->TemplateName.'.ss')
			)
		) {
			return true;
		}
		return false;
	}
	
	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_PRINTHTMLTEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}
	
	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_PRINTHTMLTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Implements custom canDeleteFromLive permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDeleteFromLive($member = null) {
		$canDelete = (Permission::check('DELETE_PRINTHTMLTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	
	/**
	 * Provide permissions
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'CREATE_PRINTHTMLTEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Druck Ausgabe" erstellen',
				'category' => 'Druck HTML Ausgabe',
				'sort' => 10
			),
			'DELETE_PRINTHTMLTEMPLATEPAGE' => array(
				'name' => 'Kann Seitentyp "Druck Ausgabe" löschen',
				'category' => 'Druck HTML Ausgabe',
				'sort' => 20
			),
			// FIELDS
			'PRINTHTMLTEMPLATEPAGE_VIEW_FIELDS_DIMENSIONS' => array(
				'name' => 'Kann Felder Maßeinheit und Abmessungen Dokument betrachten',
				'category' => 'Druck HTML Ausgabe',
				'sort' => 30
			),
			'PRINTHTMLTEMPLATEPAGE_EDIT_FIELDS_DIMENSIONS' => array(
				'name' => 'Kann Felder Maßeinheit und Abmessungen Dokument bearbeiten',
				'category' => 'Druck HTML Ausgabe',
				'sort' => 40
			),
			'PRINTHTMLTEMPLATEPAGE_EDIT_FIELD_HEADERIMAGE' => array(
				'name' => 'Kann Feld Bild bearbeiten',
				'category' => 'Druck HTML Ausgabe',
				'sort' => 50
			),
			'PRINTHTMLTEMPLATEPAGE_VIEW_PRINTLINK' => array(
				'name' => 'Kann Ausdruck erstellen',
				'category' => 'Druck HTML Ausgabe',
				'sort' => 70
			)
		);
	}
}




class PrintHtmlTemplatePage_Controller extends TemplatePage_Controller {

	private static $allowed_actions = array(
		'index',
		'html'
	);

	/**
	 * include external classes and libraries
	 */
	public function init() {
		parent::init();
		
		// define theme font path for DOMPDF
		define("DOMPDF_THEME_FOLDER", Director::baseFolder().'/themes/'.$this->SiteConfig->Theme."/dompdf-fonts/");

		// include DOMPDF class
		#require_once(Director::baseFolder().'/dompdf/dompdf_config.inc.php')
		require_once(Director::baseFolder().'/dompdf-0.6.2/dompdf_config.inc.php');
		
		
	}

	
	/**
	 * Check if Member is logged in and controller is valid
	 * then return a PDF rendered from a HTML template
	 * else return a 404 error page
	 */
	public function index() {
		
	
		
		if(Member::logged_in_session_exists() && $this->validController()) {
			
			// Create URL for HTML template (calling the URL controller with 'html' action)
			// add preview date if given
			$baseURL = Director::protocolAndHost();
			$htmlTemplateURL = $baseURL.$this->Link('html');
			if($previewStartDate = $this->getPreviewStartDate()) $htmlTemplateURL .= '?showpreview='.$previewStartDate;
					
			// create DOM PDF
			// $pdf = new DOMPDF($this->DocumentWidth, $this->DocumentHeight, $this->SizeUnit, $htmlTemplateURL)
			// header('Content-Type: text/html; charset=utf-8');
			// return $pdf;
			
			$dimensions = array(
				$this->DocumentWidth,
				$this->DocumentHeight,
				$this->SizeUnit	
			);
				
			return $this->createPDF( $dimensions, $htmlTemplateURL );
			
			// control output
			#echo "DOMPDF($this->DocumentWidth, $this->DocumentHeight, $this->SizeUnit, $htmlTemplateURL)";
			#echo $this->Link('html');
			#return $this->html();
			
		}
		return $this->redirect('/error-404.html', 404);
	}

	/**
	 * HTML Controller that returns a HTML page as base for PDF rendering
	 * 
	 * Renders the Page with $this->TemplateName
	 * Called via URL mysite.com/page-urlsegment/html
	 * 
	 * :TODO: restrict public access to URL to logged in users only, while keeping URL available to DOMPDF engine
	 *
	 * if $this->TemplateName = 'Print_DinA4' it will use $ThemeDir/templates/Print_DinA4.ss
	 */
	public function html() {
		// Flush template cache of current template
		SSViewer::flush();
		// render with template
		return $this->renderWith(array($this->TemplateName));
	}
	
	
	
	/**
	 * function createPDF()
	 * Creates pdf file and returns in to browser output.
	 * 
	 * @param array $size custom size as array(w,h,unit) in mm
	 * @param string $template path/to/templatefile:
	 * @param string $filename 'somefilename' without extension
	 */
	public function createPDF($size, $template, $filename=''){
		
	
		// init dompdf
		$dompdf = new DOMPDF();
		
		/*
		 * size array map: 
		 * 
		 * $size[0]:	width
		 * $size[1]:	height
		 * $size[2]:	unit (mm)
		 *
		*/	
		
		/* convert mm to points:
		*
		* example:
		* A4 ( 297 x 210 ) results to  841 x 595 points
		* 
		*/	
		switch($size[2]){
			
			case 'mm': 
				$DocumentWidth = $this->mm_to_points($size[0]);
				$DocumentHeight = $this->mm_to_points($size[1]);
			break;
		
			case 'px': 
				$DocumentWidth = $this->px_to_points($size[0]);
				$DocumentHeight = $this->px_to_points($size[1]);
			break;
			
		}
		
		// set custom paper size
		$DocumentSize = array(0, 0, $DocumentWidth, $DocumentHeight);
	
		$dompdf->set_paper($DocumentSize);
	
		// load template file
		$dompdf->load_html_file($template);
				
		// get date range
		$dateRange = $this->getDateRangeAsArray();
		$filenameDate = $dateRange['StartDate'];
		if($dateRange['EndDate']) $filenameDate .= '-'.$dateRange['EndDate'];

		// return PDF as file download under the given filename
		$filename = $this->URLSegment.'-'.$filenameDate.'.pdf';
		
		// final output  of pdf file
		$dompdf->render();
		$dompdf->stream($filename);
				
	}
	
	/* 
	* some conversion functions.
	* 
	* converts mm to point (dompdf uses points) or px to points.
	*/
	public static function mm_to_points($mm){
		// 1mm = 2.83464567 points
		$val = $mm*2.834;
		return round($val);
	}
	
	public static function px_to_points($px){
		$val =  (($px * 72) / 96);
		return $val;
	}
}
