<?php

/**
 *  Message that is unique to a Subsite and can be assigned to a DisplayTemplatePageLandscape
 */
class InfoMessageDisplayTemplatePageLandscape extends InfoMessageDisplayTemplatePage {
	
	private static $has_one = array (
		'DisplayTemplatePageLandscape' => 'DisplayTemplatePageLandscape',
		'InfoMessageTemplateDisplayTemplatePageLandscape' => 'InfoMessageTemplateDisplayTemplatePageLandscape',
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		if($this->InfoMessageTemplateDisplayTemplatePageLandscapeID) {
			$infoMessage = $this->InfoMessageTemplateDisplayTemplatePageLandscape();
			$f->addFieldToTab('Root.Main', LiteralField::create('InfoMessageTemplateInfo', '<p class="message notice">Erstellt durch News-Template '.$infoMessage->Title.' (ID: '.$infoMessage->ID.') am '.$this->obj('Created')->FormatFromSettings().'</p>'), 'PublishGroup');
		}
		
		return $f;
	}
}
