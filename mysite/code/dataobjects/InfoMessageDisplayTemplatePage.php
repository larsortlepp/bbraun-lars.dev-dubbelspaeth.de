<?php

/**
 * Meta class for Message that is unique to a Subsite and can be assigned to a DisplayTemplatePage
 */
class InfoMessageDisplayTemplatePage extends InfoMessage implements PermissionProvider {
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News für Bildschirm betrachten',
				'category' => 'News',
				'sort' => 410
			),
			'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News für Bildschirm bearbeiten',
				'category' => 'News',
				'sort' => 420
			),
			'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News für Bildschirm erstellen',
				'category' => 'News',
				'sort' => 430
			),
			'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News für Bildschirm löschen',
				'category' => 'News',
				'sort' => 440
			)
		);
	}
}
