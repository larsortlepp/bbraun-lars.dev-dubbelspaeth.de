<?php

class InfoMessageTemplateMicrositeNewsPage extends InfoMessageTemplate implements PermissionProvider {
	
	/**
	 * ClassName of InfoMessage class that the template content will be copied to
	 * 
	 * @var string
	 */
	public static $target_classname = 'InfoMessageMicrositeNewsPage';
	
	/**
	 * ClassNames of classes that will hold the InfoMessage
	 * 
	 * @var array
	 */
	public static $target_relation_classnames = array (
		'MicrositeNewsPage'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->removeByName('InfoMessageHeader');
		$f->addFieldToTab('Root.Main', HeaderField::create('InfoMessageHeader', 'News Template Microsite News Seite'), 'PublishGroup');
		
		// Change folder of UploadField to global InfoMessage folder.
		// This needs to be hardcoded - as we are editing globally 
		// and don´t have a SiteConfig with default folders we can use.
		// wW are using the default value set in Class MySiteConfig - though this folder name can be changed per Subsite basis
		$f->fieldByName('Root.Main.Image')->setFolderName(MySiteConfigExtension::$defaults['DefaultFolderInfoMessage']);
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGETEMPLATEMICROSITENEWSPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGETEMPLATEMICROSITENEWSPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGETEMPLATEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite News Seite betrachten',
				'category' => 'News Vorlagen',
				'sort' => 310
			),
			'EDIT_INFOMESSAGETEMPLATEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite News Seite bearbeiten',
				'category' => 'News Vorlagen',
				'sort' => 320
			),
			'CREATE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite News Seite erstellen',
				'category' => 'News Vorlagen',
				'sort' => 330
			),
			'DELETE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite News Seite löschen',
				'category' => 'News Vorlagen',
				'sort' => 340
			)
		);
	}
}
