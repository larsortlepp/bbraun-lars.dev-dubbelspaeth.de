<?php

/**
 * Dish
 *
 * Base class for a dish.
 * This class can be globally accessed by all restaurants / domains.
 * It is later subclassed to add restaurant specific fields such as Price, etc.
 *
 */
class Dish extends DataObject implements PermissionProvider {

	public static $db = array (
		'Description' => 'Text',
		'Description_en_US' => 'Text',

		'Infotext' => 'Text',
		'Infotext_en_US' => 'Text',

		/* BIG-8 Nutritives */
		'Calories' => 'Float',			// Energie (kcal)
		'Kilojoule' => 'Float',			// Energie (kJ)
		'Fat' => 'Float',				// Fett
		'FattyAcids' => 'Float',		// gesättigte Fettsäuren
		'Carbohydrates' => 'Float',		// Kohlenhydrate
		'Sugar' => 'Float',				// Zucker
		'Fibres' => 'Float',			// Ballaststoffe
		'Protein' => 'Float',			// Eiweiß
		'Salt' => 'Float',				// Salz
		'ImportID' => 'Varchar(255)'
	);

	public static $has_one = array(
		'Image' => 'Image'
	);

	public static $has_many = array(
		'RestaurantDishes' => 'RestaurantDish'
	);

	public static $many_many = array(
		'GlobalAdditives' => 'GlobalAdditive',
		'SubsiteAdditives' => 'SubsiteAdditive',
		'GlobalAllergens' => 'GlobalAllergen',
		'SubsiteAllergens' => 'SubsiteAllergen',
		'GlobalDishLabels' => 'GlobalDishLabel',
		'SubsiteDishLabels' => 'SubsiteDishLabel',
		'GlobalSpecialLabels' => 'GlobalSpecialLabel',
		'SubsiteSpecialLabels' => 'SubsiteSpecialLabel'
	);

	public static $belongs_many_many = array(
		'DailyMenuPages' => 'DailyMenuPage'
	);
	
	public static $casting = array(
		'DescriptionGridField' => 'HTMLText'
	);
	
	/**
	 * Config containing all nutritives setup data
	 * Index is correspondig to databse field setup in $db
	 * 
	 * @var array 
	 */
	public static $nutritivesConfig = array(
		'Calories' => array(
			'Title'			=> 'Kalorien',
			'Title_en_US'	=> 'Calories',
			'Unit'			=> 'kcal'
		),
		'Kilojoule' => array(
			'Title'			=> 'Kilojoule',
			'Title_en_US'	=> 'Kilojoule',
			'Unit'			=> 'kJ'
		),
		'Fat' => array(
			'Title'			=> 'Fett',
			'Title_en_US'	=> 'Fat',
			'Unit'			=> 'g'
		),
		'FattyAcids' => array(
			'Title'			=> 'Ges. Fettsäuren',
			'Title_en_US'	=> 'Fatty Acids',
			'Unit'			=> 'g'
		),
		'Carbohydrates' => array(
			'Title'			=> 'Kohlenhydrate',
			'Title_en_US'	=> 'Carbohydrates',
			'Unit'			=> 'g'
		),
		'Sugar' => array(
			'Title'			=> 'Zucker',
			'Title_en_US'	=> 'Sugar',
			'Unit'			=> 'g'
		),
		'Fibres' => array(
			'Title'			=> 'Ballaststoffe',
			'Title_en_US'	=> 'Fibres',
			'Unit'			=> 'g'
		),
		'Protein' => array(
			'Title'			=> 'Eiweiß',
			'Title_en_US'	=> 'Protein',
			'Unit'			=> 'g'
		),
		'Salt' => array(
			'Title'			=> 'Salz',
			'Title_en_US'	=> 'Salt',
			'Unit'			=> 'g'
		)
	);

	public function getCMSFields()
	{

		// get siteconfig
		$showLang_en_US = $this->showLang_en_US();

		// Get all global Additives (if existing) as Array
		$globalAdditivesMap = false;
		$globalAdditives = GlobalAdditive::get();
		if ($globalAdditives->count() >= 1) {
			// return Additives as dropdownMap Array with Number and Title
			foreach ($globalAdditives as $globalAdditive) {
				$globalAdditivesMap[$globalAdditive->ID] = $globalAdditive->Number.': '.$globalAdditive->Title;
			}
		}

		// Get all subsite-specific Additives (if existing) as Array
		$subsiteAdditivesMap = false;
		$subsiteAdditives = SubsiteAdditive::get();
		if ($subsiteAdditives->count() >= 1) {
			// return Additives as dropdownMap Array with Number and Title
			foreach ($subsiteAdditives as $subsiteAdditive) {
				$subsiteAdditivesMap[$subsiteAdditive->ID] = $subsiteAdditive->Number.': '.$subsiteAdditive->Title;
			}
		}

		// Get all global Allergens (if existing) as Array
		$globalAllergensMap = false;
		$globalAllergens = GlobalAllergen::get();
		if ($globalAllergens->count() >= 1) {
			// return Allergens as dropdownMap Array with Number and Title
			foreach ($globalAllergens as $globalAllergen) {
				$globalAllergensMap[$globalAllergen->ID] = $globalAllergen->Number.': '.$globalAllergen->Title;
			}
		}

		// Get all subsite-specific Allergens (if existing) as Array
		$subsiteAllergensMap = false;
		$subsiteAllergens = SubsiteAllergen::get();
		if ($subsiteAllergens->count() >= 1) {
			// return Allergens as dropdownMap Array with Number and Title
			foreach ($subsiteAllergens as $subsiteAllergen) {
				$subsiteAllergensMap[$subsiteAllergen->ID] = $subsiteAllergen->Number.': '.$subsiteAllergen->Title;
			}
		}

		// Get all FoodCategoris
		$foodCategoriesMap = ($foodCategories = FoodCategory::get()->filter('RestaurantPageID', RestaurantPage::getNearestRestaurantPageID())) ? $foodCategories->map()->toArray() : false;

		// Get all global DishLabels
		$globalDishLabelsMap = ($globalDishLabels = GlobalDishLabel::get()) ? $globalDishLabels->map()->toArray() : false;
		$defaultGlobalDishLabels = $this->GlobalDishLabels();
		$defaultGlobalDishLabelIDs = ($defaultGlobalDishLabels && $defaultGlobalDishLabels->count()>0) ? array_keys($defaultGlobalDishLabels->map()->toArray()) : false;

		// Get all subsite-specific DishLabels
		$subsiteDishLabelsMap = ($subsiteDishLabels = SubsiteDishLabel::get()) ? $subsiteDishLabels->map()->toArray() : false;
		$defaultSubsiteDishLabels = $this->SubsiteDishLabels();
		$defaultSubsiteDishLabelIDs = ($defaultSubsiteDishLabels && $defaultSubsiteDishLabels->count()>0) ? array_keys($defaultSubsiteDishLabels->map()->toArray()) : false;

		// Get all global SpecialLabels
		$globalSpecialLabelsMap = ($globalSpecialLabels = GlobalSpecialLabel::get()) ? $globalSpecialLabels->map()->toArray() : false;
		$defaultGlobalSpecialLabels = $this->GlobalSpecialLabels();
		$defaultGlobalSpecialLabelIDs = ($defaultGlobalSpecialLabels && $defaultGlobalSpecialLabels->count()>0) ? array_keys($defaultGlobalSpecialLabels->map()->toArray()) : false;

		// Get all subsite-specific SpecialLabels
		$subsiteSpecialLabelsMap = ($subsiteSpecialLabels = SubsiteSpecialLabel::get()) ? $subsiteSpecialLabels->map()->toArray() : false;
		$defaultSubsiteSpecialLabels = $this->SubsiteSpecialLabels();
		$defaultSubsiteSpecialLabelIDs = ($defaultSubsiteSpecialLabels && $defaultSubsiteSpecialLabels->count()>0) ? array_keys($defaultSubsiteSpecialLabels->map()->toArray()) : false;

		$f = new FieldList();

		$f->push(new HeaderField('DishHeader', 'Gericht'));
		
		// Show Dish.ImportID if enabled in SiteConfig
		if(SiteConfig::current_site_config()->ShowDishImportID) {
			$f->push(ReadonlyField::create('ImportID', 'ID'));
		}

		// FoodCategory
		#$f->push(new HeaderField('DishHeader', 'Daten dieses Restaurants'));
		if($foodCategoriesMap) $f->push(new DropdownField('FoodCategoryID', 'Essenskategorie', $foodCategoriesMap, $this->getFoodCategoryID())); // RestaurantDish Value

		// Add Warning if dish is used in other restaurants
		#if($this->IsUsedInOtherSubsite()) $f->push(new LiteralField('DishWarning', '<p class="message warning">'._t('Dish.DISHWARNING', 'Caution: This Dish is used by Menus in other restaurants<br />Changing the values below, affects the Dishes in all restaurants.').'</p>'));

		// Description
		$f->push(new TextareaField('Description', 'Beschreibung'));
		if($showLang_en_US) $f->push(new TextareaField('Description_en_US', 'Beschreibung englisch'));

		$currency = $this->SiteConfig()->Currency;
		// Prices
		$f->push(new TextField('Price', "Preis ($currency)", $this->getPrice())); // RestaurantDish Value
		$f->push(new TextField('PriceExtern', "Preis extern ($currency)", $this->getPriceExtern())); // RestaurantDish Value
		$f->push(new TextField('PriceSmall', "Preis kleine Portion ($currency)", $this->getPriceSmall())); // RestaurantDish Value
		$f->push(new TextField('PriceExternSmall', "Preis kleine Portion extern ($currency)", $this->getPriceExternSmall())); // RestaurantDish Value

		// Image
		$f->push($uploadField = new CustomUploadField('Image', 'Foto von Gericht'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default dish image folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderDishImagePath(RestaurantPage::getNearestRestaurantPageID()));
		$uploadField->setSort(array('Created' => 'DESC'));
		$uploadField->setItemsPerPage(15);

		// Nutrition values
		$f->push(new HeaderField('Nutritives', 'Nährwerte '.$this->SiteConfig()->NutritivesUnit, 3));
		foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {
			$f->push(new TextField($nutritiveField, Dish::$nutritivesConfig[$nutritiveField]['Title'].' ('.Dish::$nutritivesConfig[$nutritiveField]['Unit'].')'));
		}

		// Global SpecialLabels - Dish value
		if ($globalSpecialLabelsMap) {
			$globalSpecialLabelsField = new CheckboxSetField(
				'GlobalSpecialLabels',
				'Globale Aktionen',
				$globalSpecialLabelsMap
			);
			if ($defaultGlobalSpecialLabelIDs)
				$globalSpecialLabelsField->setDefaultItems($defaultGlobalSpecialLabelIDs);
			$f->push($globalSpecialLabelsField);
		}

		// Subsite-specific SpecialLabels - Dish value
		if ($subsiteSpecialLabelsMap) {
			$subsiteSpecialLabelsField = new CheckboxSetField(
				'SubsiteSpecialLabels',
				'Mandantenspezifische Aktionen',
				$subsiteSpecialLabelsMap
			);
			if ($defaultSubsiteSpecialLabelIDs)
				$subsiteSpecialLabelsField->setDefaultItems($defaultSubsiteSpecialLabelIDs);
			$f->push($subsiteSpecialLabelsField);
		}

		// Global DishLabels - Dish value
		if ($globalDishLabelsMap) {
			$globalDishLabelsField = new CheckboxSetField(
				'GlobalDishLabels',
				'Globale Kennzeichnungen',
				$globalDishLabelsMap
			);
			if ($defaultGlobalDishLabelIDs)
				$globalDishLabelsField->setDefaultItems($defaultGlobalDishLabelIDs);
			$f->push($globalDishLabelsField);
		}

		// Subsite-specific DishLabels - Dish value
		if ($subsiteDishLabelsMap) {
			$subsiteDishLabelsField = new CheckboxSetField(
				'SubsiteDishLabels',
				'Mandantenspezifische Kennzeichnungen',
				$subsiteDishLabelsMap
			);
			if ($defaultSubsiteDishLabelIDs)
				$subsiteDishLabelsField->setDefaultItems($defaultSubsiteDishLabelIDs);
			$f->push($subsiteDishLabelsField);
		}

		// Global Additives
		if ($globalAdditivesMap)
			$f->push(
				new CheckboxSetField(
					'GlobalAdditives',
					'Globale Zusatzstoffe',
					$globalAdditivesMap
				)
			);

		// Subsite-specific Additives
		if ($subsiteAdditivesMap)
			$f->push(
				new CheckboxSetField(
					'SubsiteAdditives',
					'Mandantenspezifische Zusatzstoffe',
					$subsiteAdditivesMap
				)
			);

		// Global Allergens
		if ($globalAllergensMap)
			$f->push(
				new CheckboxSetField(
					'GlobalAllergens',
					'Globale Allergene',
					$globalAllergensMap
				)
			);

		// Subsite-specific Allergens
		if ($subsiteAllergensMap)
			$f->push(
				new CheckboxSetField(
					'SubsiteAllergens',
					'Mandantenspezifische Allergene',
					$subsiteAllergensMap
				)
			);

		// Infotext
		$f->push(new TextareaField('Infotext', 'Zusatztext (optional)'));
		if($showLang_en_US) $f->push(new TextareaField('Infotext_en_US', 'Zusatztext englisch (optional)'));

		// Side Dishes

		/*
		// SOLUTION 1: Using module AutoCompleteField (better solution)
		// @link @link: https://github.com/tractorcow/silverstripe-autocomplete/
		//
		// :TODO: Add custom "Suggest" function:
		//
		// The AutoCompleteField does only return the description, but not an ID we can store.
		// Need to enhance this function in class and js to get it working!
		//
		// @link: https://github.com/tractorcow/silverstripe-autocomplete/issues/4
		$f->push(
			ToggleCompositeField::create('SideDishes', 'Beilagen',
				array(
					new AutoCompleteField('SideDish1ID', 'Beilage 1 (optional)', $this->getSideDishID(1), null, null, 'Dish', 'Description'),
					new AutoCompleteField('SideDish2ID', 'Beilage 2 (optional)', $this->getSideDishID(2), null, null, 'Dish', 'Description'),
					new AutoCompleteField('SideDish3ID', 'Beilage 3 (optional)', $this->getSideDishID(3), null, null, 'Dish', 'Description'),
					new AutoCompleteField('SideDish4ID', 'Beilage 4 (optional)', $this->getSideDishID(4), null, null, 'Dish', 'Description'),
					new AutoCompleteField('SideDish5ID', 'Beilage 5 (optional)', $this->getSideDishID(5), null, null, 'Dish', 'Description')
				)
			)->setHeadingLevel(4)
		);
		 *
		 */

		// SOLUTION 2: using Dropdownfield with integrated search
		// downside:
		// - always loads all dishes from server (as array)
		// - searches only at the beginnig of a word, not a partialmatch filter
		$dishesForRestaurantMap = $this->SideDishesMap();
		$ddEmptyString = "-- Keine Beilage --";
		$sideDishDDFieldsArray = array();
		for($i = 1; $i <= 5; $i++) {
			${'sideDish'.$i.'Field'} = new DropdownField('SideDish'.$i.'ID', 'Beilage '.$i.' (optional)', $dishesForRestaurantMap, $this->getSideDishID($i));
			${'sideDish'.$i.'Field'}->setEmptyString($ddEmptyString);
			$sideDishDDFieldsArray[] = ${'sideDish'.$i.'Field'};
		}
		$f->push($sideDishesFieldGroup = new FieldGroup(
			'Beilagen',
			$sideDishDDFieldsArray
		));
		$sideDishesFieldGroup->addExtraClass('linear'); // add class to remove floats from formfields		 

		// Show on Template Pages
		$showOnDisplayTemplatePageField = new CheckboxField('ShowOnDisplayTemplatePage', 'Auf Bildschirm ausgeben');
		$showOnPrintTemplatePageField = new CheckboxField('ShowOnPrintTemplatePage', 'In PDF ausgeben');
		$showOnMobileTemplatePageField = new CheckboxField('ShowOnMobileTemplatePage', 'In App ausgeben');
		// set default values when new Dish is created
		if(!$this->ID) {
			$showOnDisplayTemplatePageField->setValue(1);
			$showOnPrintTemplatePageField->setValue(1);
			$showOnMobileTemplatePageField->setValue(1);
		}
		$f->push(new FieldGroup(
			'Ausgabe in Templates',
			array(
				$showOnDisplayTemplatePageField,
				$showOnPrintTemplatePageField,
				$showOnMobileTemplatePageField
			)
		));

		// Add Print Link
		/*
		$adminController = Controller::curr();
		$page = $adminController->currentPage();
		if(method_exists($adminController, 'currentPage')) {
			$f->push(new HeaderField('PrintHeader', _t('PrintTemplatePage.PRINTHEADER', 'Print PDF (click link below)')));
			$f->push(new LiteralField('PrintLinkCategeoryA4', '<a href="'.$page->Link().'print_dish_dina4/'.$this->ID.'" target="_blank" class="custom-button icon-doc">'._t('Dish.PRINTCATEGORYDINA4', '> Print DIN A4 PDF of Dish').'</a><br />'));
		}
		*/

		/**
		 * PERMISSIONS
		 */
		
		// FoodCategory
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_FOODCATEGORY') && !Permission::check('DISH_EDIT_FIELD_FOODCATEGORY')) {
			$f->replaceField('FoodCategoryID', new HiddenField('FoodCategoryID', 'Essenskategorie', $this->getFoodCategoryID()));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_FOODCATEGORY') && Permission::check('DISH_VIEW_FIELD_FOODCATEGORY')) {
			$f->replaceField('FoodCategoryID', $f->fieldByName('FoodCategoryID')->performReadonlyTransformation());
		}
		
		// Description
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_DESCRIPTION') && !Permission::check('DISH_EDIT_FIELD_DESCRIPTION')) {
			$f->replaceField('Description', new HiddenField('Description', 'Beschreibung', $this->Description));
			if($showLang_en_US) $f->replaceField('Description_en_US', new HiddenField('Description_en_US', 'Beschreibung englisch', $this->Description_en_US));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_DESCRIPTION') && Permission::check('DISH_VIEW_FIELD_DESCRIPTION')) {
			$f->replaceField('Description', $f->fieldByName('Description')->performReadonlyTransformation());
			if($showLang_en_US) $f->replaceField('Description_en_US', $f->fieldByName('Description_en_US')->performReadonlyTransformation());
		}
		
		// Prices
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_PRICES') && !Permission::check('DISH_EDIT_FIELD_PRICES')) {
			$f->replaceField('Price', new HiddenField('Price', "Preis ($currency)", $this->getPrice()));
			$f->replaceField('PriceExtern', new HiddenField('PriceExtern', "Preis extern ($currency)", $this->getPriceExtern()));
			$f->replaceField('PriceSmall', new HiddenField('PriceSmall', "Preis kleine Portion ($currency)", $this->getPriceSmall()));
			$f->replaceField('PriceExternSmall', new HiddenField('PriceExternSmall', "Preis kleine Portion extern ($currency)", $this->getPriceExternSmall()));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_PRICES') && Permission::check('DISH_VIEW_FIELD_PRICES')) {
			$f->replaceField('Price', $f->fieldByName('Price')->performReadonlyTransformation());
			$f->replaceField('PriceExtern', $f->fieldByName('PriceExtern')->performReadonlyTransformation());
			$f->replaceField('PriceSmall', $f->fieldByName('PriceSmall')->performReadonlyTransformation());
			$f->replaceField('PriceExternSmall', $f->fieldByName('PriceExternSmall')->performReadonlyTransformation());
		}
		
		// Image
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_IMAGE') && !Permission::check('DISH_EDIT_FIELD_IMAGE')) {
			$f->replaceField('Image', new HiddenField('ImageID', 'Foto von Gericht', $this->ImageID));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_IMAGE') && Permission::check('DISH_VIEW_FIELD_IMAGE')) {
			$f->replaceField('Image', $f->fieldByName('Image')->performReadonlyTransformation());
		}
		
		// Nutritives
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELDS_NUTRITIVES') && !Permission::check('DISH_EDIT_FIELDS_NUTRITIVES')) {
			$f->removeByName('Nutritives');
			foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {
				$f->replaceField($nutritiveField, new HiddenField($nutritiveField, Dish::$nutritivesConfig[$nutritiveField]['Title'].' ('.Dish::$nutritivesConfig[$nutritiveField]['Unit'].')', $this->$nutritiveField));
			}
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELDS_NUTRITIVES') && Permission::check('DISH_VIEW_FIELDS_NUTRITIVES')) {
			foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {
				$f->replaceField($nutritiveField, $f->fieldByName($nutritiveField)->performReadonlyTransformation());
			}
		}
		
		// GlobalSpecialLabels
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_GLOBALSPECIALLABELS') && !Permission::check('DISH_EDIT_FIELD_GLOBALSPECIALLABELS')) {
			$f->removeByName('GlobalSpecialLabels');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($defaultGlobalSpecialLabelIDs) {
				foreach($defaultGlobalSpecialLabels as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("GlobalSpecialLabels[$dataobject->ID]", "Globale Aktion $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_GLOBALSPECIALLABELS') && Permission::check('DISH_VIEW_FIELD_GLOBALSPECIALLABELS')) {
			if($fieldgroup = $f->fieldByName('GlobalSpecialLabels')) $f->replaceField('GlobalSpecialLabels', $fieldgroup->performDisabledTransformation());
		}
		
		// SubsiteSpecialLabels
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_SUBSITESPECIALLABELS') && !Permission::check('DISH_EDIT_FIELD_SUBSITESPECIALLABELS')) {
			$f->removeByName('SubsiteSpecialLabels');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($defaultSubsiteSpecialLabelIDs) {
				foreach($defaultSubsiteSpecialLabels as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("SubsiteSpecialLabels[$dataobject->ID]", "Mandentenspezifische Aktion $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_SUBSITESPECIALLABELS') && Permission::check('DISH_VIEW_FIELD_SUBSITESPECIALLABELS')) {
			if($fieldgroup = $f->fieldByName('SubsiteSpecialLabels')) $f->replaceField('SubsiteSpecialLabels', $fieldgroup->performDisabledTransformation());
		}
		
		// GlobalDishLabels
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_GLOBALDISHLABELS') && !Permission::check('DISH_EDIT_FIELD_GLOBALDISHLABELS')) {
			$f->removeByName('GlobalDishLabels');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($defaultGlobalDishLabelIDs) {
				foreach($defaultGlobalDishLabels as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("GlobalDishLabels[$dataobject->ID]", "Globale Kennzeichnung $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_GLOBALDISHLABELS') && Permission::check('DISH_VIEW_FIELD_GLOBALDISHLABELS')) {
			if($fieldgroup = $f->fieldByName('GlobalDishLabels')) $f->replaceField('GlobalDishLabels', $fieldgroup->performDisabledTransformation());
		}
		
		// SubsiteDishLabels
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_SUBSITEDISHLABELS') && !Permission::check('DISH_EDIT_FIELD_SUBSITEDISHLABELS')) {
			$f->removeByName('SubsiteDishLabels');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($defaultSubsiteDishLabelIDs) {
				foreach($defaultSubsiteDishLabels as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("SubsiteDishLabels[$dataobject->ID]", "Mandentenspezifische Kennzeichnung $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_SUBSITEDISHLABELS') && Permission::check('DISH_VIEW_FIELD_SUBSITEDISHLABELS')) {
			if($fieldgroup = $f->fieldByName('SubsiteDishLabels')) $f->replaceField('SubsiteDishLabels', $fieldgroup->performDisabledTransformation());
		}
		
		// GlobalAdditives
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_GLOBALADDITIVES') && !Permission::check('DISH_EDIT_FIELD_GLOBALADDITIVES')) {
			$f->removeByName('GlobalAdditives');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($selectedGlobalAdditives = $this->GlobalAdditives()) {
				foreach($selectedGlobalAdditives as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("GlobalAdditives[$dataobject->ID]", "Globale Zusatzstoffe $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_GLOBALADDITIVES') && Permission::check('DISH_VIEW_FIELD_GLOBALADDITIVES')) {
			if($fieldgroup = $f->fieldByName('GlobalAdditives')) $f->replaceField('GlobalAdditives', $fieldgroup->performDisabledTransformation());
		}
		
		// SubsiteAdditives
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_SUBSITEADDITIVES') && !Permission::check('DISH_EDIT_FIELD_SUBSITEADDITIVES')) {
			$f->removeByName('SubsiteAdditives');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($selectedSubsiteAdditives = $this->SubsiteAdditives()) {
				foreach($selectedSubsiteAdditives as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("SubsiteAdditives[$dataobject->ID]", "Mandentenspezifische Zusatzstoffe $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_SUBSITEADDITIVES') && Permission::check('DISH_VIEW_FIELD_SUBSITEADDITIVES')) {
			if($fieldgroup = $f->fieldByName('SubsiteAdditives')) $f->replaceField('SubsiteAdditives', $fieldgroup->performDisabledTransformation());
		}
		
		// GlobalAllergens
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_GLOBALALLERGENS') && !Permission::check('DISH_EDIT_FIELD_GLOBALALLERGENS')) {
			$f->removeByName('GlobalAllergens');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($selectedGlobalAllergens = $this->GlobalAllergens()) {
				foreach($selectedGlobalAllergens as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("GlobalAllergens[$dataobject->ID]", "Globale Allergene $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_GLOBALALLERGENS') && Permission::check('DISH_VIEW_FIELD_GLOBALALLERGENS')) {
			if($fieldgroup = $f->fieldByName('GlobalAllergens')) $f->replaceField('GlobalAllergens', $fieldgroup->performDisabledTransformation());
		}
		
		// SubsiteAllergens
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_SUBSITEALLERGENS') && !Permission::check('DISH_EDIT_FIELD_SUBSITEALLERGENS')) {
			$f->removeByName('SubsiteAllergens');
			// Add hidden field for every single CheckboxSetField that is assigned
			// name of ChecboxField is HAS_MANY_RELATION[ID_OF_DATAOBJECT] e.g. "GlobalSepcialLabels[1]"
			if($selectedSubsiteAllergens = $this->SubsiteAllergens()) {
				foreach($selectedSubsiteAllergens as $dataobject) {
					// add ID as value
					$f->push(new HiddenField("SubsiteAllergens[$dataobject->ID]", "Mandentenspezifische Allergene $dataobject->Title", $dataobject->ID));
				}
			}			
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_SUBSITEALLERGENS') && Permission::check('DISH_VIEW_FIELD_SUBSITEALLERGENS')) {
			if($fieldgroup = $f->fieldByName('SubsiteAllergens')) $f->replaceField('SubsiteAllergens', $fieldgroup->performDisabledTransformation());
		}
		
		// Infotext
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELD_INFOTEXT') && !Permission::check('DISH_EDIT_FIELD_INFOTEXT')) {
			$f->replaceField('Infotext', new HiddenField('Infotext', 'Zusatztext (optional)', $this->Infotext));
			if($showLang_en_US) $f->replaceField('Infotext_en_US', new HiddenField('Infotext_en_US', 'Zusatztext englisch (optional)', $this->Infotext_en_US));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELD_INFOTEXT') && Permission::check('DISH_VIEW_FIELD_INFOTEXT')) {
			$f->replaceField('Infotext', $f->fieldByName('Infotext')->performReadonlyTransformation());
			if($showLang_en_US) $f->replaceField('Infotext_en_US', $f->fieldByName('Infotext_en_US')->performReadonlyTransformation());
		}
		
		// ShowOn...TemplatePage
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES') && !Permission::check('DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES')) {
			// remove complete fieldset with labels and checkboxes
			$f->removeByName('Ausgabe in Templates');
			
			$f->push(new HiddenField('ShowOnDisplayTemplatePage', 'Ausgabe in Template Bildschirm', $this->ShowOnDisplayTemplatePage));
			$f->push(new HiddenField('ShowOnPrintTemplatePage', 'Ausgabe in Template PDF', $this->ShowOnPrintTemplatePage));
			$f->push(new HiddenField('ShowOnMobileTemplatePage', 'Ausgabe in Template App', $this->ShowOnMobileTemplatePage));
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES') && Permission::check('DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES')) {
			$f->replaceField('ShowOnDisplayTemplatePage', $showOnDisplayTemplatePageField->performDisabledTransformation());
			$f->replaceField('ShowOnPrintTemplatePage', $showOnPrintTemplatePageField->performDisabledTransformation());
			$f->replaceField('ShowOnMobileTemplatePage', $showOnMobileTemplatePageField->performDisabledTransformation());
		}
		
		// SideDishes
		// can NOT view and NOT edit field --> hidden field
		if(!Permission::check('DISH_VIEW_FIELDS_SIDEDISHES') && !Permission::check('DISH_EDIT_FIELDS_SIDEDISHES')) {
			// remove complete fieldset with all labels and formfields
			$f->removeByName('Beilagen');
			// Add hidden field if value for SideDish exists
			for($i = 1; $i <= 5; $i++) {
				if($this->getSideDishID($i)) $f->push(new HiddenField('SideDish'.$i.'ID', 'Beilage '.$i.' (optional)', $this->getSideDishID($i)));
			}
		}
		// can NOT edit but view field -> readonly field
		else if(!Permission::check('DISH_EDIT_FIELDS_SIDEDISHES') && Permission::check('DISH_VIEW_FIELDS_SIDEDISHES')) {
			for($i = 1; $i <= 5; $i++) {
				// Note: Use performReadonlyTransformation() only! 
				// performDisabledTransformation() does not submit the data to the backend (for DropDownFields)
				$f->replaceField('SideDish'.$i.'ID', ${'sideDish'.$i.'Field'}->performDisabledTransformation());
			}			
		}
		
		// Add extension hook
		// in an Extension we can modify the CMSFields via function updateCMSFields(Fields $f)
		$this->extend('updateCMSFields', $f);
		
		return $f;
	}

	/**
	 * Returns the (optional) SideDish
	 *
	 * @param integer $nr The number of the databasefield (e.g. $nr = 1, will return db field 'SideDish1')
	 * @return Dish The dish
	 */
	public function getSideDish($nr = null) {
		// if no number was given or object doesn´t exist in DB yet: return because there´s no data to get
		if (!$nr || !$this->ID)
			return false;
		$restaurantDish = $this->getRestaurantDish();
		$field = 'SideDish'.$nr;
		return $restaurantDish ? $restaurantDish->{$field}() : false;
	}

	/**
	 * Returns the (optional) SideDish ID
	 *
	 * @param integer $nr The number of the databasefield (e.g. $nr = 1, will return db field 'SideDish1')
	 * @return integer ID of the connected SideDish (= ID of the Dish)
	 */
	public function getSideDishID($nr = null) {
		$sideDish = $this->getSideDish($nr);
		return ($sideDish && $sideDish->exists()) ? $sideDish->ID : false;
	}
	
	/**
	 * Return ArrayList with all SideDish Objects that are related to the current Dish
	 * 
	 * @param String $locale locale for SideDish.Description. default: 'de_DE'
	 * @return ArrayList $sideDishes
	 */
	public function AllSideDishes() {
		$sideDishes = new ArrayList();
		for($nr = 1; $nr <= 5; $nr++) {
			$sideDish = $this->getSideDish($nr);
			if($sideDish && $sideDish->exists()) {
				$sideDishes->push($sideDish);
			}
		}
		return $sideDishes;
	}
	
	/**
	 * Return string with all SideDish Objects that are related to the current Dish
	 * comma seperated
	 * e.g. ", Beilage 1, Beilage 2, Beilage 3"
	 * 
	 * @param String $locale locale for SideDish.Description. default: 'de_DE'
	 * @param String $seperator seperator string that divides the SideDishes. default: ', '
	 * @return String $sideDishString
	 */
	public function getAllSideDishesAsString($locale = 'de_DE', $seperator = ', ') {
		$sideDishString = '';
		foreach($this->AllSideDishes() as $sideDish) {
			$description = ($locale == 'de_DE') ? $sideDish->getDescriptionSingleLine() : $sideDish->getDescription_en_USSingleLine();
			if(strlen($description) >= 1) {
				$sideDishString .= $seperator.$description;
			}
		}
		return $sideDishString;
	}

	/**
	 * Returns (Global|Subsite)Additives of the Dish
	 * and optionally of the SideDishes
	 *
	 * GlobalAdditives come first followed by SubsiteAdditives, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	  * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function getAllAdditives($includeSideDishes = false) {
		$usedGlobalAdditives = new ArrayList();
		$usedSubsiteAdditives = new ArrayList();
		
		// Get each dish's GlobalAdditives
		$globalAdditives = $this->GlobalAdditives();
		if ($globalAdditives) {
			foreach ($globalAdditives as $additive) {
				if (!$usedGlobalAdditives->find('ID', $additive->ID)) {
					$usedGlobalAdditives->push($additive);
				}
			}
		}
		
		// Get each dish's SubsiteAdditives
		$subsiteAdditives = $this->SubsiteAdditives();
		if ($subsiteAdditives) {
			foreach ($subsiteAdditives as $additive) {
				if (!$usedSubsiteAdditives->find('ID', $additive->ID)) {
					$usedSubsiteAdditives->push($additive);
				}
			}
		}

		if($includeSideDishes) {
			foreach($this->AllSideDishes() as $sideDish) {
				
				// Get each SideDish´s GlobalAdditives
				foreach ($sideDish->GlobalAdditives() as $additive) {
					if (!$usedGlobalAdditives->find('ID', $additive->ID)) {
						$usedGlobalAdditives->push($additive);
					}
				}
				
				// Get each SideDish´s SubsiteAdditives
				foreach ($sideDish->SubsiteAdditives() as $additive) {
					if (!$usedSubsiteAdditives->find('ID', $additive->ID)) {
						$usedSubsiteAdditives->push($additive);
					}
				}				
			}
		}
		$usedGlobalAdditives = $usedGlobalAdditives->sort('SortOrder');
		$usedSubsiteAdditives = $usedSubsiteAdditives->sort('SortOrder');
		
		return MyListUtil::concatenateLists($usedGlobalAdditives, $usedSubsiteAdditives);
	}
	
	/**
	 * (Global|Subsite)Allergens of the Dish
	 * and optionally of the SideDishes
	 *
	 * GlobalAllergens come first followed by SubsiteAllergens, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	  * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function getAllAllergens($includeSideDishes = false) {
		$usedGlobalAllergens = new ArrayList();
		$usedSubsiteAllergens = new ArrayList();
		
		// Get each dish's GlobalAllergens
		$globalAllergens = $this->GlobalAllergens();
		if ($globalAllergens) {
			foreach ($globalAllergens as $allergen) {
				if (!$usedGlobalAllergens->find('ID', $allergen->ID)) {
					$usedGlobalAllergens->push($allergen);
				}
			}
		}
		
		// Get each dish's SubsiteAllergens
		$subsiteAllergens = $this->SubsiteAllergens();
		if ($subsiteAllergens) {
			foreach ($subsiteAllergens as $allergen) {
				if (!$usedSubsiteAllergens->find('ID', $allergen->ID)) {
					$usedSubsiteAllergens->push($allergen);
				}
			}
		}

		if($includeSideDishes) {
			foreach($this->AllSideDishes() as $sideDish) {
				// Get each SideDish´s GlobalAllergens
				foreach ($sideDish->GlobalAllergens() as $allergen) {
					if (!$usedGlobalAllergens->find('ID', $allergen->ID)) {
						$usedGlobalAllergens->push($allergen);
					}
				}
				
				// Get each SideDish´s SubsiteAdditives
				foreach ($sideDish->SubsiteAllergens() as $allergen) {
					if (!$usedSubsiteAllergens->find('ID', $allergen->ID)) {
						$usedSubsiteAllergens->push($allergen);
					}
				}				
			}
		}
		$usedGlobalAllergens = $usedGlobalAllergens->sort('SortOrder');
		$usedSubsiteAllergens = $usedSubsiteAllergens->sort('SortOrder');
		
		return MyListUtil::concatenateLists($usedGlobalAllergens, $usedSubsiteAllergens);
	}

	/**
	 * (Global|Subsite)DishLabels of the Dish
	 * and optionally of the SideDishes
	 *
	 * GlobalDishLabels come first followed by SubsiteDishLabels, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	  * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function getAllDishLabels($includeSideDishes = false) {
		$usedGlobalDishLabels = new ArrayList();
		$usedSubsiteDishLabels = new ArrayList();
		
		// Get each dish's GlobalDishLabels
		$globalDishLabels = $this->GlobalDishLabels();
		if ($globalDishLabels) {
			foreach ($globalDishLabels as $dishLabel) {
				if (!$usedGlobalDishLabels->find('ID', $dishLabel->ID)) {
					$usedGlobalDishLabels->push($dishLabel);
				}
			}
		}
		
		// Get each dish's SubsiteDishLabels
		$subsiteDishLabels = $this->SubsiteDishLabels();
		if ($subsiteDishLabels) {
			foreach ($subsiteDishLabels as $dishLabel) {
				if (!$usedSubsiteDishLabels->find('ID', $dishLabel->ID)) {
					$usedSubsiteDishLabels->push($dishLabel);
				}
			}
		}

		if($includeSideDishes) {
			foreach($this->AllSideDishes() as $sideDish) {
				
				// Get each SideDish´s GlobalDishLabels
				foreach ($sideDish->GlobalDishLabels() as $dishLabel) {
					if (!$usedGlobalDishLabels->find('ID', $dishLabel->ID)) {
						$usedGlobalDishLabels->push($dishLabel);
					}
				}
				
				// Get each SideDish´s SubsiteDishLabels
				foreach ($sideDish->SubsiteDishLabels() as $dishLabel) {
					if (!$usedSubsiteDishLabels->find('ID', $dishLabel->ID)) {
						$usedSubsiteDishLabels->push($dishLabel);
					}
				}				
			}
		}
		$usedGlobalDishLabels = $usedGlobalDishLabels->sort('SortOrder');
		$usedSubsiteDishLabels = $usedSubsiteDishLabels->sort('SortOrder');
		
		return MyListUtil::concatenateLists($usedGlobalDishLabels, $usedSubsiteDishLabels);
	}

	/**
	 * (Global|Subsite)SpecialLabels of the Dish
	 * and optionally of the SideDishes
	 *
	 * GlobalSpecialLabels come first followed by SubsiteSpecialLabels, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	  * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @return ArrayList
	 */
	public function getAllSpecialLabels($includeSideDishes = false) {
		$usedGlobalSpecialLabels = new ArrayList();
		$usedSubsiteSpecialLabels = new ArrayList();
		
		// Get each dish's GlobalSpecialLabels
		$globalSpecialLabels = $this->GlobalSpecialLabels();
		if ($globalSpecialLabels) {
			foreach ($globalSpecialLabels as $specialLabel) {
				if (!$usedGlobalSpecialLabels->find('ID', $specialLabel->ID)) {
					$usedGlobalSpecialLabels->push($specialLabel);
				}
			}
		}
		
		// Get each dish's SubsiteSpecialLabels
		$subsiteSpecialLabels = $this->SubsiteSpecialLabels();
		if ($subsiteSpecialLabels) {
			foreach ($subsiteSpecialLabels as $specialLabel) {
				if (!$usedSubsiteSpecialLabels->find('ID', $specialLabel->ID)) {
					$usedSubsiteSpecialLabels->push($specialLabel);
				}
			}
		}

		if($includeSideDishes) {
			foreach($this->AllSideDishes() as $sideDish) {
				
				// Get each SideDish´s GlobalSpecialLabels
				foreach ($sideDish->GlobalSpecialLabels() as $specialLabel) {
					if (!$usedGlobalSpecialLabels->find('ID', $specialLabel->ID)) {
						$usedGlobalSpecialLabels->push($specialLabel);
					}
				}
				
				// Get each SideDish´s SubsiteSpecialLabels
				foreach ($sideDish->SubsiteSpecialLabels() as $specialLabel) {
					if (!$usedSubsiteSpecialLabels->find('ID', $specialLabel->ID)) {
						$usedSubsiteSpecialLabels->push($specialLabel);
					}
				}				
			}
		}
		$usedGlobalSpecialLabels = $usedGlobalSpecialLabels->sort('SortOrder');
		$usedSubsiteSpecialLabels = $usedSubsiteSpecialLabels->sort('SortOrder');
		
		return MyListUtil::concatenateLists($usedGlobalSpecialLabels, $usedSubsiteSpecialLabels);
	}

	/**
	 * returns all Additives and Allergens
	 *
	 * @return ArrayList
	 */
	public function getAllAdditivesAndAllergens() {
		return MyListUtil::ConcatenateLists(
			$this->getAllAdditives(),
			$this->getAllAllergens()
		);
	}

	/**
	 * returns all Additives and Allergens as string with separator
	 *
	 * @param String $separator separator for joining the attributes and allergens
	 * @return String $additivesAllergensString
	 */
	public function getAllAdditivesAndAllergensString($separator='/') {
		$additivesAllergensString = '';

		$additivesAndAllergens = $this->owner->getAllAdditivesAndAllergens();
		if($additivesAndAllergens) {
			$additivesAndAllergensMap = $additivesAndAllergens->map('Number', 'Number');
			$additivesAllergensString = implode($separator, $additivesAndAllergensMap);
		}

		return $additivesAllergensString;
	}
	
	/**
	 * Returns all Dishes for Dropdown of SideDishes assignment
	 * (can be extended by 'updateSideDishesQuery()' function in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateSideDishesQuery(DataList &$query) {
	 *     $query = Dishes::get();
	 * } 
	 * @return type
	 */
	public function SideDishesQuery() {
		$query = Dish::get()->leftJoin('RestaurantDish', 'Dish.ID = RestaurantDish.DishID')->where('RestaurantDish.RestaurantPageID = '.RestaurantPage::getNearestRestaurantPageID())->sort('Description ASC');
		$this->extend('updateSideDishesQuery', $query);
		return $query;
	}
	
	/**
	 * Returns all Dishes as 2-dimensional Array for Dropdown of SideDishes assignment
	 * (can be extended by 'updateSideDishesMap()' function in extensions)

	 * @return Array $array
	 */
	public function SideDishesMap() {
		$array = $this->owner->SideDishesQuery()->map('ID', 'Description');
		$this->extend('updateSideDishesMap', $array);
		return $array;
	}

	/**
	 * return Thumbnail of Image
	 *
	 * @return Image
	 */
	public function getImage_CMSThumbnail() {
		return ($Image = $this->Image()) ? $Image->croppedImage(100,100) : false;
	}

	/**
	 * Converts any given Variable (string, Integer, Float) to a float number
	 *
	 * @param	String	$string				Price as String, Integer or Float (e.g. "2", "2,5", "2.5" or "2.532")
	 * @param	Integer	$digits				Maximum number of digits after the comma
	 *
	 * @return	Float	$floatVal			Price as float: "2.5" or "2"
	 */
	public static function convertToFloat($string, $digits = 2) {
		return floatval(number_format(floatval(str_replace(',', '.', $string)), $digits, '.', ''));
	}

	/**
	 * Return String with all TemplatePages that the Dish will be showed on
	 *
	 * @return String
	 */
	public function getShowOnTemplatePages() {
		$pagesArray = null;
		if($this->ShowOnDisplayTemplatePage) $pagesArray[] = 'Bildschirm';
		if($this->ShowOnPrintTemplatePage) $pagesArray[] = 'PDF';
		if($this->ShowOnMobileTemplatePage) $pagesArray[] = 'App';
		return is_array($pagesArray) ? implode(', ', $pagesArray) : false;
	}

	/**
	 * Return true if any of the nutrition values is set
	 *
	 * @return Boolean
	 */
	public function NutritionValuesSet() {
		$nutritionValueSet = false;
		foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {
			if($this->{$nutritiveField}) {
				$nutritionValueSet = true;
				break;
			}
		}
		return $nutritionValueSet;
	}
	
	/**
	 * Return all nutritives as ArrayList
	 * Object Structure:
	 * 
	 * Object [
	 *   "Title" => TITLE OF THE NUTRITIVE (e.g. "Kohlehydrate"
	 *   "Title_en_US" => ENGLISH TITLE
	 *   "Value" => DATABASE VALUE (FLOAT)
	 *   "ValueNice" => FORMATED VALUE (runs the getXXXNice() function)
	 *   "Unit" => UNIT OF THE NUTRITIVE (e.g. "kcal", "g" etc.)
	 * ]
	 * 
	 * @return ArrayList
	 */
	public function Nutritives() {
		$nutritives = new ArrayList();
		foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {				
			if($this->{$nutritiveField}) {					
				$nutritives->push(new ArrayData(array(
					'Title'			=> $nutritiveConfig['Title'],
					'Title_en_US'	=> $nutritiveConfig['Title_en_US'],
					'Value'			=> $this->{$nutritiveField},
					'ValueNice'		=> $this->{"get".$nutritiveField."Nice"}(),
					'Unit'			=> $nutritiveConfig['Unit']
				)));
			}
		}		
		return $nutritives;
	}
	
	/**
	 * Returns all existing Nutritive values of the current dish as array
	 * including german description and unit (e.g. "Fett: 3,5 g")
	 *
	 * @return Array
	 */
	public function getNutritivesArray() {
		$nutritivesArray = false;
		$nutritives = $this->Nutritives();
		if($nutritives) {
			$nutritivesArray = array();
			foreach($nutritives as $nutritive) {
				$nutritivesArray[] = $nutritive->Title.': '.$nutritive->ValueNice.' '.$nutritive->Unit;
			}
		}

		return $nutritivesArray;
	}
	
	/**
	 * Returns the Description with HTML line breaks (<br />) 
	 * and casted as HTMLText
	 * 
	 * This is used for rendering a mulitline Description in GridField 
	 * (otherwise HTML tags are shown because Description is TextareaField)
	 * 
	 * @return String
	 */
	function DescriptionGridField(){return $this->getDescriptionGridField();}
	function getDescriptionGridField(){
	  $obj = HTMLText::create();
	  $obj->setValue(nl2br($this->Description));
	  return $obj;
	}

	/**
	 * Return Description in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Description_en_US"
	 * If no translation for the language exists, the default field "Description" is returned
	 *
	 * @return	String
	 */
	public function DescriptionLocalised() {
		$locale = i18n::get_locale();
		return ($localeDescription = $this->{'Description_'.$locale}) ? $localeDescription : $this->Description;
	}

	/**
	 * FUNCTIONS FOR ACCESS TO FIELDS AND FUNCTIONS OF RESTAURANTDISH
	 */

	/**
	 * Returns the RestaurantDish that is associated with the current dish
	 * If no RestaurantDish is available for this restaurant, it returns false
	 *
	 * @return DataList
	 */
	public function getRestaurantDish() {
		return RestaurantDish::get()->filter(array(
			'DishID' => $this->ID,
			'RestaurantPageID' => RestaurantPage::getNearestRestaurantPageID()
		))->First();
	}

	/**
	 * Returns the value of the given database field on the associated RestaurantDish
	 *
	 * @param String $fieldname Name of the database field on RestaurantDish
	 * @return String
	 */
	public function getRestaurantField($fieldname) {
		$fieldValue = false;
		$restaurantDish = $this->getRestaurantDish();
		if($restaurantDish) {
			$fieldValue = ($restaurantDish->{$fieldname}) ? $restaurantDish->{$fieldname} : null;
		}
		return $fieldValue;
	}

	/**
	 * Returns the Price value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPrice() {
		return $this->getRestaurantField('Price');
	}

	/**
	 * Returns the PriceExtern value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceExtern() {
		return $this->getRestaurantField('PriceExtern');
	}

	/**
	 * Returns the PriceSmall value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceSmall() {
		return $this->getRestaurantField('PriceSmall');
	}

	/**
	 * Returns the PriceExternSmall value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceExternSmall() {
		return $this->getRestaurantField('PriceExternSmall');
	}

	/**
	 * Returns the Pricevalue of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceNice() {
		return $this->formatPriceNice($this->getPrice());
	}

	/**
	 * Returns the PriceExtern value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceExternNice() {
		return $this->formatPriceNice($this->getPriceExtern());
	}

	/**
	 * Returns the PriceSmall value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceSmallNice() {
		return $this->formatPriceNice($this->getPriceSmall());
	}

	/**
	 * Returns the PriceExternSmall value of the associated RestaurantDish
	 *
	 * @return Float
	 */
	public function getPriceExternSmallNice() {
		return $this->formatPriceNice($this->getPriceExternSmall());
	}

	/**
	 * Returns the german Price format with comma and 2 digits after the comma
	 *
	 * @param	Float	$price	The price as float (e.g. 1.45)
	 * @return	String
	 */
	public function formatPriceNice($price = null) {
		return ($price == null) ? false : number_format($price, 2, ',', '');
	}
	
	/**
	 * Returns the ShowOnDisplayTemplatePage value of the associated RestaurantDish
	 *
	 * @return Boolean
	 */
	public function getShowOnDisplayTemplatePage() {
		return $this->getRestaurantField('ShowOnDisplayTemplatePage');
	}
	
	/**
	 * Returns the ShowOnPrintTemplatePage value of the associated RestaurantDish
	 *
	 * @return Boolean
	 */
	public function getShowOnPrintTemplatePage() {
		return $this->getRestaurantField('ShowOnPrintTemplatePage');
	}
	
	/**
	 * Returns the ShowOnMobileTemplatePage value of the associated RestaurantDish
	 *
	 * @return Boolean
	 */
	public function getShowOnMobileTemplatePage() {
		return $this->getRestaurantField('ShowOnMobileTemplatePage');
	}

	/**
	 * Returns the FoodCategory of the associated RestaurantDish
	 *
	 * @return DataObject
	 */
	public function getFoodCategory() {
		if(!$foodCategoryID = $this->getRestaurantField('FoodCategoryID')) return false;
		return FoodCategory::get()->byID($foodCategoryID);
	}

	/**
	 * Returns the FoodCategoryID value of the associated RestaurantDish
	 *
	 * @return Integer
	 */
	public function getFoodCategoryID() {
		return $this->getRestaurantField('FoodCategoryID');
	}

	/**
	 * Returns the Title of the selected FoodCategory
	 *
	 * @return String
	 */
	public function getFoodCategoryTitle() {
		return ($foodCategory = $this->getFoodCategory()) ? $foodCategory->Title() : false;
	}

	/**
	 * Returns the CategoryColor of the selected FoodCategory
	 *
	 * @return String
	 */
	public function getFoodCategoryColor() {
		return ($foodCategory = $this->getFoodCategory()) ? $foodCategory->CategoryColor : false;
	}

	/**
	 * Returns the ColorLighterRGB of the selected FoodCategory
	 *
	 * @return String
	 */
	public function getFoodCategoryColorLighterRGB($percent) {
		return ($foodCategory = $this->getFoodCategory()) ? $foodCategory->ColorLighterRGB($percent) : false;
	}

	/**
	 * Returns the ColorLighterRGB of the selected FoodCategory
	 *
	 * @return String
	 */
	public function getFoodCategoryColorBrightnessHEX($percent) {
		return ($foodCategory = $this->getFoodCategory()) ? $foodCategory->ColorBrightnessHEX($percent) : false;
	}

	/**
	 * Returns the Float value value with german comma separation
	 * e.g. 2.15 => 2,15; 2 => 2
	 *
	 * @param Float $float
	 * @param String $seperator
	 * @return String
	 */
	public function formatFloat($float = null, $seperator = ',') {
		return ($float == null || $float == 0) ? false : str_replace('.', $seperator, $float);
	}

	/**
	 * Returns the Calories value with german comma separation
	 *
	 * @return String
	 */
	public function getCaloriesNice() {
		return $this->formatFloat($this->Calories);
	}
	
	/**
	 * Returns the Kilojoule value with german comma separation
	 *
	 * @return String
	 */
	public function getKilojouleNice() {
		return $this->formatFloat($this->Kilojoule);
	}

	/**
	 * Returns the Fat value with german comma separation
	 *
	 * @return String
	 */
	public function getFatNice() {
		return $this->formatFloat($this->Fat);
	}

	/**
	 * Returns the FattyAcids value with german comma separation
	 *
	 * @return String
	 */
	public function getFattyAcidsNice() {
		return $this->formatFloat($this->FattyAcids);
	}

	/**
	 * Returns the Carbohydrates value with german comma separation
	 *
	 * @return String
	 */
	public function getCarbohydratesNice() {
		return $this->formatFloat($this->Carbohydrates);
	}

	/**
	 * Returns the Sugar value with german comma separation
	 *
	 * @return String
	 */
	public function getSugarNice() {
		return $this->formatFloat($this->Sugar);
	}

	/**
	 * Returns the Fibres value with german comma separation
	 *
	 * @return String
	 */
	public function getFibresNice() {
		return $this->formatFloat($this->Fibres);
	}

	/**
	 * Returns the Protein value with german comma separation
	 *
	 * @return String
	 */
	public function getProteinNice() {
		return $this->formatFloat($this->Protein);
	}

	/**
	 * Returns the Salt value with german comma separation
	 *
	 * @return String
	 */
	public function getSaltNice() {
		return $this->formatFloat($this->Salt);
	}

	/**
	 * Returns true if more than one DishLabel is assigned to Dish
	 *
	 * @return Boolean
	 */
	public function MoreThanOneDishLabel() {
		return $this->getAllDishLabels()->count() > 1;
	}

	/**
	 * Returns a HTMLText object with HTML code displaying the thumbnail
	 * images for the current dish's DishLabels
	 *
	 * @return HTMLText
	 */
	public function getDishLabelImagesCMS() {
		$htmlText = '';
		$dishLabels = $this->getAllDishLabels();
		if ($dishLabels) {
			foreach ($dishLabels as $dishLabel) {
				$image = ($dishLabel->Image() && $dishLabel->Image()->exists()) ? $dishLabel->Image()->SetHeight(40) : false;
				$htmlText = ($image != null) ? $image->getTag() : $dishLabel->Title;
			}
		}

		$obj = HTMLText::create();
		$obj->setValue($htmlText);
		return $obj;
	}

	/**
	 * Returns the Description as a single line (No breaks, leading or ending white space odr double spaces)
	 *
	 * @return String
	 */
	public function getDescriptionSingleLine() {
		$locale = i18n::get_locale();
		return ($this->{'Description_'.$locale}) ? $this->stringToSingleLine($this->{'Description_'.$locale}) : $this->stringToSingleLine($this->Description);
	}

	/**
	 * Returns the Description as a single line (No breaks, leading or ending white space odr double spaces)
	 *
	 * @return String
	 */
	public function getDescription_en_USSingleLine() {
		return $this->stringToSingleLine($this->Description_en_US);
	}

	/**
	 * Returns the string as a single line (No breaks, leading or ending white space odr double spaces)
	 *
	 * @param String $string	String with line breaks, multiple spaces, etc.
	 * @return String
	 */
	public function stringToSingleLine($string) {
		$string = preg_replace('/-\n/', "-", $string); // replace dash and linebreak at end of line with just a dash (remove line break)
		return preg_replace('/[ \r\n]+/', ' ', $string); // replace double spaces and line breaks with a single space
	}
	
	/**
	 * Returns the Import ID wrapped in braces e.g. '(5)' for use in GridField search
	 * 
	 * @return string The ImportID in braces, or an empty string if no ImportID exists
	 */
	public function getImportIDForSearch() {
		return $this->ImportID ? '('.$this->ImportID.') ' : '';
	}

	/**
	 * Removes multiple spaces with one single space, trims string, keep line breaks
	 *
	 * @param String $string String with multiple spaces
	 * @return String
	 */
	public function cleanUpWhiteSpace($string) {
		return trim(preg_replace("#^[ ]+|[ ]+$#m", "", preg_replace('/[^\S\n]+/', ' ', $string)));
	}

	/**
	 * Sync the Dish Image folder,
	 * to include images that were uploaded via FTP
	 * If na $foldername is given the default folder for dish images as set in 
	 * SiteConfig::DefaultFolderDishImagePath() will be used.
	 *
	 * @param String $foldername (optional) Folder name as path without 'assets/' part and ending slash. e.g. 'mandant/Fotos-Gerichte' will find Filename 'assets/mandant/Fotos-Gerichte/'
	 */
	public function syncDishImageFolder($foldername) {
		if(!$foldername) $foldername = SiteConfig::current_site_config()->DefaultFolderDishImagePath(RestaurantPage::getNearestRestaurantPageID());
		$folder = File::get()->filter(array('ClassName' => 'Folder', 'Filename' => 'assets/'.$foldername.'/'))->First();
		$folderID = $folder->ID;
		if($folderID) Filesystem::sync($folderID);
	}

	/**
	 * CleanUp values of Dish before writing to database
	 */
	public function onBeforeWrite() {
		$this->CarbohydrateUnit = $this->convertToFloat($this->CarbohydrateUnit);
		$this->Description = $this->cleanUpWhiteSpace($this->Description);
		$this->Description_en_US = $this->cleanUpWhiteSpace($this->Description_en_US);
		$this->Infotext = $this->cleanUpWhiteSpace($this->Infotext);
		$this->Infotext_en_US = $this->cleanUpWhiteSpace($this->Infotext_en_US);

		/* BIG-8 Nutritives */
		$this->Calories = $this->convertToFloat($this->Calories);
		$this->Fat = $this->convertToFloat($this->Fat);
		$this->FattyAcids = $this->convertToFloat($this->FattyAcids);
		$this->Carbohydrates = $this->convertToFloat($this->Carbohydrates);
		$this->Sugar = $this->convertToFloat($this->Sugar);
		$this->Fibres = $this->convertToFloat($this->Fibres);
		$this->Protein = $this->convertToFloat($this->Protein);
		$this->Salt = $this->convertToFloat($this->Salt);

		parent::onBeforeWrite();
	}

	/**
	 * Save values to DataObjects "Dish" and "RestaurantDish"
	 *
	 * CAUTION:
	 * Restaurant-specific values are not stored on the current "Dish" DataObject,
	 * but on the "RestaurantDish" DataObject.
	 *
	 * This way we can have globally accesible fields (on "Dish"),
	 * and locally (per Domain / Subsite) accessible fields on "RestaurantDish"
	 */
	public function onAfterWrite() {
		parent::onAfterWrite();
		
		// only assign RestaurantDish Data if we are in the admin interface
		// > only here we can be sure that the requested form fields exist
		if($this->ID && is_subclass_of(Controller::curr(), "LeftAndMain")) {

			// STORE ADDITIONAL VALUES AS SEPERATE DATAOBJECT "RestaurantDish"

			// try to retrieve associated RestaurantDish
			$restaurantDish = $this->getRestaurantDish();

			// if no RestaurantDish exists -> create a new one
			if(!$restaurantDish || !$restaurantDish->DishID) {
				$restaurantDish = new RestaurantDish();
				$restaurantDish->DishID = $this->ID;
				$restaurantDish->write(); // write RestaurantDish to database, so we can store many_many relations on it
			}

			// store values to RestaurantDish
			if(Permission::check('DISH_EDIT_FIELD_PRICES')) {
				$restaurantDish->Price = $this->convertToFloat($this->getField('Price'));
				$restaurantDish->PriceExtern = $this->convertToFloat($this->getField('PriceExtern'));
				$restaurantDish->PriceSmall = $this->convertToFloat($this->getField('PriceSmall'));
				$restaurantDish->PriceExternSmall = $this->convertToFloat($this->getField('PriceExternSmall'));
			}
			
			if(Permission::check('DISH_EDIT_FIELD_FOODCATEGORY')) {
				$restaurantDish->FoodCategoryID = $this->getField('FoodCategoryID');
			}
			
			if(Permission::check('DISH_EDIT_FIELDS_SIDEDISHES')) {
				for($i = 1; $i <= 5; $i++) {
					$restaurantDish->{'SideDish'.$i.'ID'} = $this->getField('SideDish'.$i.'ID');
				}
			}
			
			if(Permission::check('DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES')) {
				$restaurantDish->ShowOnDisplayTemplatePage = $this->getField('ShowOnDisplayTemplatePage');
				$restaurantDish->ShowOnPrintTemplatePage = $this->getField('ShowOnPrintTemplatePage');
				$restaurantDish->ShowOnMobileTemplatePage = $this->getField('ShowOnMobileTemplatePage');
			}

			// save changes on RestaurantDish
			$restaurantDish->write();
		}

		// Set relation between current Dish and the calling DailyMenuPage
		if($this->getField('SetRelationDailyMenuPage_Dishes')) {
			$adminController = Controller::curr();
			if(method_exists($adminController, 'currentPage')) {
				$page = $adminController->currentPage();
				if($page->ID) {
					// check if Dish is already connected with current DailyMenuPage
					if($page->exists() && !$page->Dishes()->find('ID', $this->ID)) {
						DB::query("
							INSERT INTO DailyMenuPage_Dishes
							(
								DailyMenuPageID,
								DishID
							)
							VALUES
							(
								".$page->ID.",
								".$this->ID."
							)
						");
					}
				}
			}
		}
	}

	public function onBeforeDelete() {
		$deleteFromTableNames = array(
			'RestaurantDish',
			'DailyMenuPage_Dishes',
			'Dish_GlobalAdditives',
			'Dish_GlobalAllergens',
			'Dish_GlobalDishLabels',
			'Dish_GlobalSpecialLabels',
			'Dish_SubsiteAdditives',
			'Dish_SubsiteAllergens',
			'Dish_SubsiteDishLabels',
			'Dish_SubsiteSpecialLabels'
		);
		
		// Delete all many_many relations for current Dish
		foreach($deleteFromTableNames as $table) {
			DB::query("DELETE FROM $table WHERE DishID = ".$this->ID);
		}

		parent::onBeforeDelete();
	}

	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		return Permission::check('VIEW_DISH', 'any', $member);
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		return Permission::check('EDIT_DISH', 'any', $member);
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_DISH', 'any', $member);
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_DISH', 'any', $member);
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_DISH' => array(
				'name' => 'Kann Gerichte betrachten',
				'category' => 'Gerichte',
				'sort' => 10
			),
			'EDIT_DISH' => array(
				'name' => 'Kann Gerichte bearbeiten',
				'category' => 'Gerichte',
				'sort' => 20
			),
			'CREATE_DISH' => array(
				'name' => 'Kann Gerichte erstellen',
				'category' => 'Gerichte',
				'sort' => 30
			),
			'DELETE_DISH' => array(
				'name' => 'Kann Gerichte löschen',
				'category' => 'Gerichte',
				'sort' => 40
			),
			// FIELDS DISH
			'DISH_VIEW_FIELD_DESCRIPTION' => array(
				'name' => 'Kann Beschreibung betrachten',
				'category' => 'Gerichte',
				'sort' => 50
			),
			'DISH_EDIT_FIELD_DESCRIPTION' => array(
				'name' => 'Kann Beschreibung bearbeiten',
				'category' => 'Gerichte',
				'sort' => 60
			),
			'DISH_VIEW_FIELD_IMAGE' => array(
				'name' => 'Kann Bild betrachten',
				'category' => 'Gerichte',
				'sort' => 70
			),
			'DISH_EDIT_FIELD_IMAGE' => array(
				'name' => 'Kann Bild bearbeiten',
				'category' => 'Gerichte',
				'sort' => 80
			),
			'DISH_VIEW_FIELDS_NUTRITIVES' => array(
				'name' => 'Kann Nährwerte betrachten',
				'category' => 'Gerichte',
				'sort' => 90
			),
			'DISH_EDIT_FIELDS_NUTRITIVES' => array(
				'name' => 'Kann Nährwerte bearbeiten',
				'category' => 'Gerichte',
				'sort' => 100
			),
			'DISH_VIEW_FIELD_GLOBALSPECIALLABELS' => array(
				'name' => 'Kann globale Aktionen betrachten',
				'category' => 'Gerichte',
				'sort' => 110
			),
			'DISH_EDIT_FIELD_GLOBALSPECIALLABELS' => array(
				'name' => 'Kann globale Aktionen bearbeiten',
				'category' => 'Gerichte',
				'sort' => 120
			),
			'DISH_VIEW_FIELD_SUBSITESPECIALLABELS' => array(
				'name' => 'Kann mandantenspezifische Aktionen betrachten',
				'category' => 'Gerichte',
				'sort' => 130
			),
			'DISH_EDIT_FIELD_SUBSITESPECIALLABELS' => array(
				'name' => 'Kann mandantenspezifische Aktionen bearbeiten',
				'category' => 'Gerichte',
				'sort' => 140
			),
			'DISH_VIEW_FIELD_GLOBALDISHLABELS' => array(
				'name' => 'Kann globale Kennzeichnungen betrachten',
				'category' => 'Gerichte',
				'sort' => 150
			),
			'DISH_EDIT_FIELD_GLOBALDISHLABELS' => array(
				'name' => 'Kann globale Kennzeichnungen bearbeiten',
				'category' => 'Gerichte',
				'sort' => 160
			),
			'DISH_VIEW_FIELD_SUBSITEDISHLABELS' => array(
				'name' => 'Kann mandantenspezifische Kennzeichnungen betrachten',
				'category' => 'Gerichte',
				'sort' => 170
			),
			'DISH_EDIT_FIELD_SUBSITEDISHLABELS' => array(
				'name' => 'Kann mandantenspezifische Kennzeichnungen bearbeiten',
				'category' => 'Gerichte',
				'sort' => 180
			),
			'DISH_VIEW_FIELD_GLOBALADDITIVES' => array(
				'name' => 'Kann globale Zusatzstoffe betrachten',
				'category' => 'Gerichte',
				'sort' => 190
			),
			'DISH_EDIT_FIELD_GLOBALADDITIVES' => array(
				'name' => 'Kann globale Zusatzstoffe bearbeiten',
				'category' => 'Gerichte',
				'sort' => 200
			),
			'DISH_VIEW_FIELD_SUBSITEADDITIVES' => array(
				'name' => 'Kann mandantenspezifische Zusatzstoffe betrachten',
				'category' => 'Gerichte',
				'sort' => 210
			),
			'DISH_EDIT_FIELD_SUBSITEADDITIVES' => array(
				'name' => 'Kann mandantenspezifische Zusatzstoffe bearbeiten',
				'category' => 'Gerichte',
				'sort' => 220
			),
			'DISH_VIEW_FIELD_GLOBALALLERGENS' => array(
				'name' => 'Kann globale Allergene betrachten',
				'category' => 'Gerichte',
				'sort' => 230
			),
			'DISH_EDIT_FIELD_GLOBALALLERGENS' => array(
				'name' => 'Kann globale Allergene bearbeiten',
				'category' => 'Gerichte',
				'sort' => 240
			),
			'DISH_VIEW_FIELD_SUBSITEALLERGENS' => array(
				'name' => 'Kann mandantenspezifische Allergene betrachten',
				'category' => 'Gerichte',
				'sort' => 250
			),
			'DISH_EDIT_FIELD_SUBSITEALLERGENS' => array(
				'name' => 'Kann mandantenspezifische Allergene bearbeiten',
				'category' => 'Gerichte',
				'sort' => 260
			),
			'DISH_VIEW_FIELD_INFOTEXT' => array(
				'name' => 'Kann Infotext betrachten',
				'category' => 'Gerichte',
				'sort' => 270
			),
			'DISH_EDIT_FIELD_INFOTEXT' => array(
				'name' => 'Kann Infotext bearbeiten',
				'category' => 'Gerichte',
				'sort' => 280
			),
			// FIELDS RESTAURANTDISH
			'DISH_VIEW_FIELD_FOODCATEGORY' => array(
				'name' => 'Kann Essenskategorie betrachten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 310
			),
			'DISH_EDIT_FIELD_FOODCATEGORY' => array(
				'name' => 'Kann Essenskategorie bearbeiten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 320
			),
			'DISH_VIEW_FIELD_PRICES' => array(
				'name' => 'Kann Preise betrachten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 330
			),
			'DISH_EDIT_FIELD_PRICES' => array(
				'name' => 'Kann Preise bearbeiten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 340
			),
			'DISH_VIEW_FIELDS_SIDEDISHES' => array(
				'name' => 'Kann Beilagen betrachten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 350
			),
			'DISH_EDIT_FIELDS_SIDEDISHES' => array(
				'name' => 'Kann Beilagen bearbeiten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 360
			),
			'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES' => array(
				'name' => 'Kann Ausgabe in Templates betrachten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 370
			),
			'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES' => array(
				'name' => 'Kann Ausgabe in Templates bearbeiten (Restaurant spezifisch)',
				'category' => 'Gerichte',
				'sort' => 380
			),
		);
	}
}
