<?php

/**
 * Display Video
 *
 * A video that is unique to a restaurant and can be assigned to any DisplayTemplatePage
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * enable Subsite support for this class:
 *
 *   DisplayVideo:
 *     extensions:
 *       - DataObjectSubsites
 */
class DisplayVideo extends DataObject implements PermissionProvider {

	private static $db = array (
		'Title' => 'Varchar(255)',
		'Title_en_US' => 'Varchar(255)',
		'VideoType' => "Enum('Datei,Youtube','Datei')",
		'YoutubeLink' => 'Varchar(255)',
		'Publish' => 'Boolean',

		// Timing
		'TimerStartDate' => 'Date',
		'TimerStartTime' => 'Time',
		'TimerEndDate' => 'Date',
		'TimerEndTime' => 'Time',

		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'VideoFileWebM' => 'File',
		'DisplayTemplatePage' => 'DisplayTemplatePage'
	);

	private static $many_many = array (
		// Timing
		'TimerRecurringDaysOfWeek' => 'RecurringDayOfWeek'
	);

	private static $defaults = array(
		'Publish' => 1,
		'VideoType' => 'Datei'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$showLang_en_US = $this->showLang_en_US();

		$f = new FieldList();

		$f->push(new HeaderField('DisplayVideo', 'Video'));

		$f->push(new CheckboxField('Publish', 'veröffentlichen / aktiv'));

		$f->push(new TextField('Title', 'Titel'));

		// English Content
		if($showLang_en_US)  {
			$f->push(new TextField('Title_en_US','Titel (english)'));
		}

		$f->push(new OptionsetField('VideoType', 'Video Wiedergabe', $this->dbObject('VideoType')->enumValues()));
		
		$f->push($uploadField = CustomUploadField::create('VideoFileWebM', 'Video Datei')->setDescription('webm Format'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default InfoMessage folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderInfoMessagePath(RestaurantPage::getNearestRestaurantPageID()));
		
		$f->push(new TextField('YoutubeLink', 'Link zu Youtube Video'));

		// Timing
		$f->push(new HeaderField('TimingHeader', 'Zeitgesteuerte Anzeige (optional)'));

		$f->push(new TimeField('TimerStartTime', 'Zeit Beginn'));
		$f->push(new TimeField('TimerEndTime', 'Zeit Ende'));

		$startDateField = new DateField('TimerStartDate', 'Datum Beginn');
		$startDateField->setLocale(i18n::get_locale());
		$startDateField->setConfig('showcalendar', true);
		$f->push($startDateField);
		$endDateField = new DateField('TimerEndDate', 'Datum Ende');
		$endDateField->setLocale(i18n::get_locale());
		$endDateField->setConfig('showcalendar', true);
		$f->push($endDateField);

		$f->push(new CheckboxSetField('TimerRecurringDaysOfWeek', 'An folgenden Tagen anzeigen', RecurringDayOfWeek::map_days_of_week()));

		$f->push(new HeaderField('EmptyHeader', '&nbsp;'));

		return $f;
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}

	// TIMING
	/**
	 * Functions returns true if InfoMessage is setup to be displayed at the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 * If no date or time is set on InfoMessage, function will return true.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return Boolean	true if InfoMessage is set to be displayed at the given date and time, else: false
	 */
	public function ShowAtDateTime($datetime = null) {
		$datetime = $datetime ? $datetime : date('Y-m-d H:i:s');
		$time = strtotime($datetime);
		$timeHIS = date('H:i:s', $time);
		$date = date('Y-m-d', $time);

		#echo "$datetime - $date zwischen".$this->TimerStartDate." und ".$this->TimerEndDate;
		#echo "$timeHIS zwischen".$this->TimerStartTime." und ".$this->TimerEndTime;

		// check if time is set and is NOT in given time span
		if(
			$this->TimerStartTime && $this->TimerEndTime &&
			($timeHIS < $this->TimerStartTime || $timeHIS > $this->TimerEndTime)
		) {
			return false;
		}

		// check if given date is NOT in between given date span
		if(
			$this->TimerStartDate && $this->TimerEndDate &&
			($date < $this->TimerStartDate || $date > $this->TimerEndDate)

		) {
			return false;
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			// get day number from given date
			$dayNum = date('w', $time);
			// check if day number is selected for current InfoMessage
			if($recurringDays->count() >= 1 && !$recurringDays->find('Value', $dayNum)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns all Date and Time Settings, collapsed as a string
	 *
	 * @return String
	 */
	public function getCollapsedDateTime() {

		// check if given date is NOT in between given date span
		if($this->TimerStartDate && $this->TimerEndDate) {
			$datetime = DateUtil::dateFormat($this->TimerStartDate).' - '.DateUtil::dateFormat($this->TimerEndDate);
		} else {
			$datetime = '';
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			if(strlen($datetime) >= 1) $datetime .= '<br />';
			$datetime .= implode(', ', $recurringDays->map('Value','Skey')->toArray());
		}

		if($this->TimerStartTime && $this->TimerEndTime) {
			if(strlen($datetime) >= 1) $datetime .= '<br />';
			$datetime .= $this->TimerStartTime.' - '.$this->TimerEndTime.' '._t('InfoMessage.OCLOCK', 'h');
		}

		return strlen($datetime) >= 1 ? $datetime : _t('InfoMessage.NODATETIMESET', 'Not set (always visible)');
	}

	/**
	 * Returns the getCollapsedDateTime() with HTML line breaks (<br />) 
	 * and casted as HTMLText
	 * 
	 * This is used for rendering a mulitline Text in GridField 
	 * (otherwise HTML tags are shown because Field is TextareaField)
	 * 
	 * @return String
	 */
	function CollapsedDateTimeGridField(){return $this->getCollapsedDateTimeGridField();}
	function getCollapsedDateTimeGridField(){
	  $obj = HTMLText::create();
	  $obj->setValue(nl2br($this->getCollapsedDateTime()));
	  return $obj;
	}

	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_DISPLAYVIDEO') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_DISPLAYVIDEO') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_DISPLAYVIDEO') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_DISPLAYVIDEO') ? true : false;
		return $canView;
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_DISPLAYVIDEO' => array(
				'name' => 'Kann Video für Bildschirmausgabe betrachten',
				'category' => 'Bildschirmausgabe - Video',
				'sort' => 10
			),
			'EDIT_DISPLAYVIDEO' => array(
				'name' => 'Kann Video für Bildschirmausgabe bearbeiten',
				'category' => 'Bildschirmausgabe - Video',
				'sort' => 20
			),
			'CREATE_DISPLAYVIDEO' => array(
				'name' => 'Kann Video für Bildschirmausgabe erstellen',
				'category' => 'Bildschirmausgabe - Video',
				'sort' => 30
			),
			'DELETE_DISPLAYVIDEO' => array(
				'name' => 'Kann Video für Bildschirmausgabe löschen',
				'category' => 'Bildschirmausgabe - Video',
				'sort' => 40
			)
		);
	}
}
