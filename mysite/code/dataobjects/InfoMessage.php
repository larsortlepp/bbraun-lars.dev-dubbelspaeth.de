<?php

/**
 * Info Message
 *
 * Meta class for News Messages that is extended to classes
 * - InfoMessageTemplate: a global template file for news messages
 * - InfoMessage{MobileTemplatePage|PrintTemplatePage|MicrositeTemplatePage|DisplayTemplatePageLandscape|DisplayTemplatePagePortrait}:
 *   a subsite aware News Message, that is uniqzue to a TemplatePage
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * enable Subsite support for this class:
 *
 *   InfoMessageMobileTemplatePage:
 *     extensions:
 *       - DataObjectSubsites
 */
class InfoMessage extends DataObject {

	private static $db = array (
		'Title' => 'Varchar(255)',
		'Title_en_US' => 'Varchar(255)',
		'Content' => 'HTMLText',
		'Content_en_US' => 'HTMLText',
		'ImageOnly' => 'Boolean',
		'Publish' => 'Boolean',

		// Timing
		'TimerStartDate' => 'Date',
		'TimerStartTime' => 'Time',
		'TimerEndDate' => 'Date',
		'TimerEndTime' => 'Time',

		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	private static $many_many = array (
		// Timing
		'TimerRecurringDaysOfWeek' => 'RecurringDayOfWeek'
	);

	private static $defaults = array(
		'Publish' => 1
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		// check if we are on a page or in ModelAdmin
		// always show english fields in ModelAdmin
		$showLang_en_US = true;
		$page = Controller::curr()->currentPage();
		if($page) {
			$restaurantPage = RestaurantPage::getNearestRestaurantPage();
			$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		}

		// create basic tab set with 'Root' tab
		// this can be extended by new tabs and be modified with 
		// $f->addFieldToTab('Root', Field::create(...));
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);

		$f->addFieldToTab('Root.Main', HeaderField::create('InfoMessageHeader', 'News'));

		$f->addFieldToTab('Root.Main', FieldGroup::create('Veröffentlichen', CheckboxField::create('Publish', 'ja'))->setName('PublishGroup'));
		$f->addFieldToTab('Root.Main', TextField::create('Title', 'Titel'));
		$f->addFieldToTab('Root.Main', $uploadField = CustomUploadField::create('Image', 'Bild (optional)'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default InfoMessage folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderInfoMessagePath(RestaurantPage::getNearestRestaurantPageID()));
		// show Fullscreen Image always and remove it on child classes if we don´ need it
		$f->addFieldToTab('Root.Main', FieldGroup::create('Vollbild', CheckboxField::create('ImageOnly', 'Bild im Vollbildmodus anzeigen'))->setName('ImageOnlyGroup'));
		$f->addFieldToTab('Root.Main', HtmlEditorField::create('Content', 'Inhalt'));

		// English Content
		if($showLang_en_US)  {
			$f->addFieldToTab('Root.Main', HeaderField::create('InfoMessageHeaderEN', 'Englisch', 2));
			$f->addFieldToTab('Root.Main', TextField::create('Title_en_US','Titel (english)'));
			$f->addFieldToTab('Root.Main', HtmlEditorField::create('Content_en_US', 'Inhalt'));
		}

		// Timing
		$f->addFieldToTab('Root.Main', HeaderField::create('TimingHeader', 'Zeitgesteuerte Anzeige (optional)'));

		$f->addFieldToTab('Root.Main', TimeField::create('TimerStartTime', 'Zeit Beginn'));
		$f->addFieldToTab('Root.Main', TimeField::create('TimerEndTime', 'Zeit Ende'));

		$f->addFieldToTab(
			'Root.Main',  
			DateField::create('TimerStartDate', 'Datum Beginn')
				->setConfig('showcalendar', true)
		);
		$f->addFieldToTab(
			'Root.Main',
			DateField::create('TimerEndDate', 'Datum Ende')
				->setConfig('showcalendar', true)
		);

		$f->addFieldToTab('Root.Main', CheckboxSetField::create('TimerRecurringDaysOfWeek', 'An folgenden Tagen anzeigen', RecurringDayOfWeek::map_days_of_week()));

		return $f;
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}

	/**
	 * Return Content in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Content() {
		$locale = i18n::get_locale();
		return ($this->{'Content_'.$locale}) ? $this->{'Content_'.$locale} : $this->Content;
	}
	
	/**
	 * Returns the Description with plain HTML code of Content
	 * 
	 * This is used for rendering in GridField 
	 * (otherwise HTML tags are shown because they are encoded)
	 * 
	 * @return String
	 */
	function ContentGridField(){return $this->getContentGridField();}
	function getContentGridField(){
	  $obj = HTMLText::create();
	  $obj->setValue($this->Content);
	  return $obj;
	}

	/**
	 * Return preview image for CMS
	 *
	 * @return Image
	 */
	public function getImage_CMSThumbnail() {
		return ($this->Image() && $this->Image()->exists()) ? $this->Image()->CMSThumbnail() : false;
	}

	/**
	 * Check if InfoMessage is the latest InfoMessage of the current TemplatePage
	 *
	 * @return Boolean
	 */
	public function LatestInfoMessage() {
		$latestMessage = InfoMessage::get()->filter('TemplatePageID', $this->TemplatePageID)->sort('LastEdited', 'DESC')->First();

		return ($latestMessage->ID == $this->ID) ? true : false;
	}

	// TIMING
	/**
	 * Functions returns true if InfoMessage is setup to be displayed at the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 * If no date or time is set on InfoMessage, function will return true.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return Boolean	true if InfoMessage is set to be displayed at the given date and time, else: false
	 */
	public function ShowAtDateTime($datetime = null) {
		$datetime = $datetime ? $datetime : date('Y-m-d H:i:s');
		$time = strtotime($datetime);
		$timeHIS = date('H:i:s', $time);
		$date = date('Y-m-d', $time);

		#echo "$datetime - $date zwischen".$this->TimerStartDate." und ".$this->TimerEndDate;
		#echo "$timeHIS zwischen".$this->TimerStartTime." und ".$this->TimerEndTime;

		// check if time is set and is NOT in given time span
		if(
			$this->TimerStartTime && $this->TimerEndTime &&
			($timeHIS < $this->TimerStartTime || $timeHIS > $this->TimerEndTime)
		) {
			return false;
		}

		// check if given date is NOT in between given date span
		if(
			$this->TimerStartDate && $this->TimerEndDate &&
			($date < $this->TimerStartDate || $date > $this->TimerEndDate)

		) {
			return false;
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			// get day number from given date
			$dayNum = date('w', $time);
			// check if day number is selected for current InfoMessage
			if($recurringDays->count() >= 1 && !$recurringDays->find('Value', $dayNum)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns all Date and Time Settings, collapsed as a string
	 *
	 * @return String
	 */
	public function getCollapsedDateTime() {

		// check if given date is NOT in between given date span
		if($this->TimerStartDate && $this->TimerEndDate) {
			$datetime = DateUtil::dateFormat($this->TimerStartDate).' - '.DateUtil::dateFormat($this->TimerEndDate);
		}
		else if($this->TimerStartDate) {
			$datetime = 'am '.DateUtil::dateFormat($this->TimerStartDate);
		}
		else if($this->TimerEndDate) {
			$datetime = 'bis '.DateUtil::dateFormat($this->TimerEndDate);
		}
		else {
			$datetime = '';
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			if(strlen($datetime) >= 1) $datetime .= '<br />';
			$datetime .= implode(', ', $recurringDays->map('Value','Skey')->toArray());
		}

		if($this->TimerStartTime && $this->TimerEndTime) {
			if(strlen($datetime) >= 1) $datetime .= '<br />';
			$datetime .= $this->TimerStartTime.' - '.$this->TimerEndTime.' '._t('InfoMessage.OCLOCK', 'h');
		}

		return strlen($datetime) >= 1 ? $datetime : _t('InfoMessage.NODATETIMESET', 'Not set (always visible)');
	}

	/**
	 * Returns the getCollapsedDateTime() with HTML line breaks (<br />) 
	 * and casted as HTMLText
	 * 
	 * This is used for rendering a mulitline Text in GridField 
	 * (otherwise HTML tags are shown because Field is TextareaField)
	 * 
	 * @return String
	 */
	function CollapsedDateTimeGridField(){return $this->getCollapsedDateTimeGridField();}
	function getCollapsedDateTimeGridField(){
	  $obj = HTMLText::create();
	  $obj->setValue(nl2br($this->getCollapsedDateTime()));
	  return $obj;
	}
	
	public function onBeforeWrite() {
		parent::onBeforeWrite();
		
		// insert new InfoMessage always at first position
		$currController = Controller::curr();
		if(method_exists($currController, 'currentPage')) {
			$currentPage = Controller::curr()->currentPage();
			// augment SortOrder of all InfoMessages of the current TemplatePage,
			// only when the InfoMessage is created and we have a parent TemplatePage
			if(!$this->ID && $currentPage && method_exists($currentPage, 'InfoMessages')) {
				// recreate SortOrder of existing InfoMessages starting with 1
				$SortOrder = 1;
				foreach($currentPage->InfoMessages()->sort('SortOrder') as $infoMessageSort) {
					$infoMessageSort->SortOrder = $SortOrder;
					$infoMessageSort->write();
					$SortOrder++;
				}
			}
		}
	}
	
	public function onBeforeDelete() {
		parent::onBeforeDelete();
		
		// delete all has_many relations
		foreach(InfoMessage::$many_many as $ClassName => $FieldName) {
			foreach($this->{$ClassName}() as $object) {
				$this->{$ClassName}()->remove($object);
			}
		}
	}
}
