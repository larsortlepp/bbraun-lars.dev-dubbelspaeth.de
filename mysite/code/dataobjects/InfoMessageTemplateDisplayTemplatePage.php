<?php

class InfoMessageTemplateDisplayTemplatePage extends InfoMessageTemplate implements PermissionProvider {
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->removeByName('InfoMessageHeader');
		$f->addFieldToTab('Root.Main', HeaderField::create('InfoMessageHeader', 'News Template Bildschirm'), 'PublishGroup');
		
		// Change folder of UploadField to global InfoMessage folder.
		// This needs to be hardcoded - as we are editing globally 
		// and don´t have a SiteConfig with default folders we can use.
		// wW are using the default value set in Class MySiteConfig - though this folder name can be changed per Subsite basis
		$f->fieldByName('Root.Main.Image')->setFolderName(MySiteConfigExtension::$defaults['DefaultFolderInfoMessage']);
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News Vorlagen für Bildschirm betrachten',
				'category' => 'News Vorlagen',
				'sort' => 410
			),
			'EDIT_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News Vorlagen für Bildschirm bearbeiten',
				'category' => 'News Vorlagen',
				'sort' => 420
			),
			'CREATE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News Vorlagen für Bildschirm erstellen',
				'category' => 'News Vorlagen',
				'sort' => 430
			),
			'DELETE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE' => array(
				'name' => 'Kann News Vorlagen für Bildschirm löschen',
				'category' => 'News Vorlagen',
				'sort' => 440
			)
		);
	}
}
