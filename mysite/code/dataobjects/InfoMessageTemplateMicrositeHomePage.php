<?php

class InfoMessageTemplateMicrositeHomePage extends InfoMessageTemplate implements PermissionProvider {
	
	/**
	 * ClassName of InfoMessage class that the template content will be copied to
	 * 
	 * @var string
	 */
	public static $target_classname = 'InfoMessageMicrositeHomePage';
	
	/**
	 * ClassNames of classes that will hold the InfoMessage
	 * 
	 * @var array
	 */
	public static $target_relation_classnames = array (
		'MicrositeHomePage'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->removeByName('InfoMessageHeader');
		$f->addFieldToTab('Root.Main', HeaderField::create('InfoMessageHeader', 'News Template Microsite Startseite'), 'PublishGroup');
		
		// Change folder of UploadField to global InfoMessage folder.
		// This needs to be hardcoded - as we are editing globally 
		// and don´t have a SiteConfig with default folders we can use.
		// wW are using the default value set in Class MySiteConfig - though this folder name can be changed per Subsite basis
		$f->fieldByName('Root.Main.Image')->setFolderName(MySiteConfigExtension::$defaults['DefaultFolderInfoMessage']);
		
		// remove fullscreen image field
		$f->removeFieldFromTab('Root.Main', 'ImageOnlyGroup');
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite Startseite betrachten',
				'category' => 'News Vorlagen',
				'sort' => 210
			),
			'EDIT_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite Startseite bearbeiten',
				'category' => 'News Vorlagen',
				'sort' => 220
			),
			'CREATE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite Startseite erstellen',
				'category' => 'News Vorlagen',
				'sort' => 230
			),
			'DELETE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News Vorlagen für Microsite Startseite löschen',
				'category' => 'News Vorlagen',
				'sort' => 240
			)
		);
	}
}
