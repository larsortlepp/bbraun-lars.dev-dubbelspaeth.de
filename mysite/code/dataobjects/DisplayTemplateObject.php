<?php

/**
 * DisplayTemplateObject
 *
 * Object with setup for a template that displays data on a display
 * Setup for: Template Name, Food Categories and Date an Time period when Template will be displayed
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * enable Subsite support for this class:
 *
 *   DisplayTemplateObject:
 *     extensions:
 *       - DataObjectSubsites
 */
class DisplayTemplateObject extends DataObject implements PermissionProvider {

	private static $db = array (
		'Title' => 'Varchar(255)',
		'DatePeriod' => "Enum('Tage,Wochen','Tage')",
		'DateRange' => 'Int',
		'StartDateType' => "Enum('Aktuellem-Datum,Anfangsdatum','Aktuellem-Datum')",
		'StartDate' => 'Date',
		'FootNote' => 'Text',

		// Timing
		'TimerStartDate' => 'Date',
		'TimerStartTime' => 'Time',
		'TimerEndDate' => 'Date',
		'TimerEndTime' => 'Time',

		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'DisplayMultiTemplatePage' => 'DisplayMultiTemplatePage',
		'MenuContainer' => 'MenuContainer'
	);

	private static $many_many = array (
		'FoodCategories' => 'FoodCategory',
		// Timing
		'TimerRecurringDaysOfWeek' => 'RecurringDayOfWeek'
	);

	public static $defaults = array(
		'DatePeriod' => 'Tage',
		'DateRange' => 1,
		'StartDateType' =>'Aktuellem_Datum',
		'StartDate' => null
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$showLang_en_US = $this->showLang_en_US();

		$f = new FieldList();

		$f->push(new HeaderField('DisplayTemplateObjectHeader', 'Bildschirm Template'));

		$f->push(new TextField('Title', 'Template Bezeichnung'));
		
		$f->push(
			new DropdownField(
				'MenuContainerID',
				'Speiseplan',
				MenuContainer::get()->filter(array(
					'ParentID' => RestaurantPage::getNearestRestaurantPageID()
				))->map('ID', 'Title')
			)
		);

		$f->push(new OptionsetField('StartDateType', 'Beginn der Ausgabe ab', $this->dbObject('StartDateType')->enumValues()));
		$dateField = new DateField('StartDate', 'Anfangsdatum (optional)');
		$dateField->setLocale(i18n::get_locale());
		$dateField->setConfig('showcalendar', true);
		$f->push($dateField);
		$f->push(new OptionsetField('DatePeriod','Einheit auszugebender Zeitraum', $this->dbObject('DatePeriod')->enumValues()));
		$f->push(new NumericField('DateRange', 'Anzahl Tage / Wochen ausgeben'));

		// add FoodCategories
		$foodCategoriesMap = ($foodCategories = FoodCategory::get()->filter('RestaurantPageID', RestaurantPage::getNearestRestaurantPageID())) ? $foodCategories->map()->toArray() : false;
		if($foodCategoriesMap) $f->push(new CheckboxSetField('FoodCategories', 'Essenskategorien', $foodCategoriesMap));

		$f->push(new TextareaField('FootNote', 'Text für Fußzeile'));

		// Timing
		$f->push(new HeaderField('TimingHeader', 'Zeitgesteuerte Anzeige (optional)'));

		$f->push(new TimeField('TimerStartTime', 'Zeit Beginn'));
		$f->push(new TimeField('TimerEndTime', 'Zeit Ende'));

		$startDateField = new DateField('TimerStartDate', 'Datum Beginn');
		$startDateField->setLocale(i18n::get_locale());
		$startDateField->setConfig('showcalendar', true);
		$f->push($startDateField);
		$endDateField = new DateField('TimerEndDate', 'Datum Ende');
		$endDateField->setLocale(i18n::get_locale());
		$endDateField->setConfig('showcalendar', true);
		$f->push($endDateField);

		$f->push(new CheckboxSetField('TimerRecurringDaysOfWeek', 'An folgenden Tagen anzeigen', RecurringDayOfWeek::map_days_of_week()));

		$f->push(new HeaderField('EmptyHeader', '&nbsp;'));

		return $f;
	}

	// TIMING
	/**
	 * Functions returns true if Object is setup to be displayed at the given $datetime
	 * If no $datetime is given, the current date and time will be used.
	 * If no date or time is set on Object, function will return true.
	 *
	 * @param String $datetime	Date and Time in format '2013-04-07 06:54:00'
	 * @return Boolean	true if Object is set to be displayed at the given date and time, else: false
	 */
	public function ShowAtDateTime($datetime = null) {
		$datetime = $datetime ? $datetime : date('Y-m-d H:i:s');
		$time = strtotime($datetime);
		$timeHIS = date('H:i:s', $time);
		$date = date('Y-m-d', $time);

		#echo "$datetime - $date zwischen".$this->TimerStartDate." und ".$this->TimerEndDate;
		#echo "$timeHIS zwischen".$this->TimerStartTime." und ".$this->TimerEndTime;

		// check if time is set and is NOT in given time span
		if(
			$this->TimerStartTime && $this->TimerEndTime &&
			($timeHIS < $this->TimerStartTime || $timeHIS > $this->TimerEndTime)
		) {
			return false;
		}

		// check if given date is NOT in between given date span
		if(
			$this->TimerStartDate && $this->TimerEndDate &&
			($date < $this->TimerStartDate || $date > $this->TimerEndDate)

		) {
			return false;
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			// get day number from given date
			$dayNum = date('w', $time);
			// check if day number is selected for current InfoMessage
			if($recurringDays->count() >= 1 && !$recurringDays->find('Value', $dayNum)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns all Date and Time Settings, collapsed as a string
	 *
	 * @return String
	 */
	public function getCollapsedDateTime() {

		// check if given date is NOT in between given date span
		if($this->TimerStartDate && $this->TimerEndDate) {
			$datetime = DateUtil::dateFormat($this->TimerStartDate).' - '.DateUtil::dateFormat($this->TimerEndDate);
		} else {
			$datetime = '';
		}

		// check if TimerRecurringDaysOfWeek are set
		if($recurringDays = $this->TimerRecurringDaysOfWeek()) {
			if(strlen($datetime) >= 1) $datetime .= '<br />';
			$datetime .= implode(', ', $recurringDays->map('Value','Skey')->toArray());
		}

		if($this->TimerStartTime && $this->TimerEndTime) {
			if(strlen($datetime) >= 1) $datetime .= '<br />';
			$datetime .= $this->TimerStartTime.' - '.$this->TimerEndTime.' '._t('InfoMessage.OCLOCK', 'h');
		}

		return strlen($datetime) >= 1 ? $datetime : _t('InfoMessage.NODATETIMESET', 'Not set (always visible)');
	}

	/**
	 * Returns the getCollapsedDateTime() with HTML line breaks (<br />) 
	 * and casted as HTMLText
	 * 
	 * This is used for rendering a mulitline Text in GridField 
	 * (otherwise HTML tags are shown because Field is TextareaField)
	 * 
	 * @return String
	 */
	function CollapsedDateTimeGridField(){return $this->getCollapsedDateTimeGridField();}
	function getCollapsedDateTimeGridField(){
	  $obj = HTMLText::create();
	  $obj->setValue(nl2br($this->getCollapsedDateTime()));
	  return $obj;
	}

	/**
	 * Returns start date for preview if it is set via URL
	 * e.g. mydomain.com/mytemplate?showpreview=2013-11-11
	 *
	 * @return String
	 */
	public function getPreviewStartDate() {
		return (Member::currentUserID() && isset($_GET['showpreview'])) ? Convert::raw2sql($_GET['showpreview']) : false;
	}

	/**
	 * RenderMenu
	 * returns true if previewStartDate or ShowAtDateTime is set
	 * --> only show at set DateTime in Live mode, always show in preview mode
	 *
	 * @return Boolean
	 */
	public function RenderMenu() {
		return ($this->ShowAtDateTime() || $this->getPreviewStartDate()) ? true : false;
	}

	/**
	 * Returns the start and end date for the TemplatePage based on the settings in the cms
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return Array $dates Array with 'StartDate' and 'EndDate'
	 */
	public function getDateRangeAsArray($datePeriod = null, $dateRange = null) {
		$dates = array();

		// if user is logged in and urlparameter 'showpreview' ist set -> use this date as start date
		$startDate = $this->getPreviewStartDate();

		if(!$startDate) {
			// set start date - depending on CMS setting: a) current date b) fix start date
			$startDate = $this->StartDateType == 'Aktuellem-Datum' ? DateUtil::currentDate() : $this->StartDate;
		}

		// get DateRange and DatePeriod from variables, otherwise from current TemplatePage
		$datePeriod = $datePeriod ? $datePeriod : $this->DatePeriod;
		$dateRange = $dateRange ? $dateRange : $this->DateRange;

		// calculate start- and end date for a) days or b) weeks
		if($datePeriod == 'Tage') {
			$dateRange = DateUtil::dayDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = ($dateRange > 1) ? $dateRange['EndDate'] :  null;
		}
		else {
			$dateRange = DateUtil::weekDateRange($startDate, $dateRange);
			$dates['StartDate'] = $dateRange['StartDate'];
			$dates['EndDate'] = $dateRange['EndDate'];
		}
		return $dates;
	}

	/**
	 * Returns the start date for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeStartDate($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		return DateUtil::dateFormat($dateRange['StartDate'], $format);
	}

	/**
	 * Returns the end date for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeEndDate($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		return DateUtil::dateFormat($dateRange['EndDate'], $format);
	}

	/**
	 * Returns the end of the week date (Friday) for the current date period,
	 * based on the stting in the cms
	 *
	 * @param String $format (optional) Date format string (for more info see: http://php.net/manual/de/function.strftime.php)
	 * @return String
	 */
	public function DateRangeEndDateWeek($format = '%d.%m.%Y') {
		$dateRange = $this->getDateRangeAsArray();
		$endOfWeek = $dateRange['EndDate'];
		while(date('l', strtotime($endOfWeek)) != 'Friday' && $endOfWeek >= $dateRange['StartDate']) {
			$endOfWeek = date('Y-m-d', strtotime($endOfWeek." -1 day"));
		}
		return $endOfWeek >= $dateRange['StartDate'] ? DateUtil::dateFormat($endOfWeek, $format) : false;
	}

	/**
	 * Returns the IDs of a DisplayMultiTemplatePage*'s selected FoodCategories as array
	 *
	 * @param Integer $templatePageID optional: ID of the *TemplatePage to use
	 *
	 * @return array
	 */
	public function FoodCategoryIDs($templatePageID = null) {
		$templatePage = $templatePageID ? call_user_func($this->DisplayMultiTemplatePage()->ClassName.'::get')->byID($templatePageID) : null;
		$foodCategories = $templatePage ? $templatePage->FoodCategories() : $this->FoodCategories();
		return $foodCategories ? array_keys($foodCategories->map()->toArray()) : false;
	}

	/**
	 * Returns the HEX color code for the first FoodCategory that is selected for this Page
	 *
	 * @return String
	 */
	public function FirstCategoryColor() {
		$foodCategories = $this->FoodCategories();
		return $foodCategories->exists() ? $foodCategories->First()->Color : false;
	}

	// CONTROLLER

	/**
	 * Returns all DailyMenuPages for the date span set in the CMS (1-n days or weeks), starting from the current day / current week
	 * Adds a String with IDs of all selected FoodCategories to every DailyMenuPage.
	 * This way we can call DailyMenuPage->FoodCategories() with the preselected FoodCategories from the CMS.
	 *
	 * If DateRange is "Wochen", a DailyMenuPage per Day will be returned, even if there is no actual Page in the SiteTree.
	 * (neccessary for consistent rendering of Data Tables)
	 *
	 * @param String $datePeriod optional: DatePeriod that will be calculated. 'Tage' oder 'Wochen'
	 * @param Integer $dateRange optional: Number of Days or Weeks to calculate the date range
	 * @return ArrayList
	 */
	public function DailyMenuPages($datePeriod = null, $dateRange = null) {
		// calculate start- and end date
		$dateRange = $this->getDateRangeAsArray($datePeriod, $dateRange);
		
		$menuContainer = $this->MenuContainer();

		// get Array of all selected FoodCategoryIDs
		$foodCategoryIDs = $this->FoodCategoryIDs();

		// return the DailyMenuPages for the date range and FoodCategories
		$dailyMenuPages = $menuContainer->DailyMenuPagesByCategoryAndDate($foodCategoryIDs, $dateRange['StartDate'], $dateRange['EndDate']);

		return $dailyMenuPages;
	}
	
	/*
	 * Returns DailyMenuPages for the current restaurant that fall within
	 * a given date range, sorted by date.
	 *
	 * @return DataList
	 */
	private function getDailyMenuPagesByDate() {
		$dateRangeArray = $this->getDateRangeAsArray();

		$menuContainer = $this->MenuContainer();

		return DailyMenuPage::get()->filter(array_merge(
			array('ParentID' => $menuContainer->ID),
			array('Date:GreaterThanOrEqual' => $dateRangeArray['StartDate']),
			$dateRangeArray['EndDate'] ? array('Date:LessThanOrEqual' => $dateRangeArray['EndDate']) : array()
		))->sort('Date');
	}

	/**
	 * Returns (Global|Subsite)Additives that are used by Dishes that
	 * - are used on DailyMenuPages of the current restaurant that fall within
	 *   a given date range
	 * - belong to FoodCategories that are selected on the current *TemplatePage
	 *
	 * GlobalAdditives come first followed by SubsiteAdditives, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedAdditives($includeSideDishes = false, $foodCategoryIDs = null) {
		$foodCategoryIDArray = $foodCategoryIDs ? explode(",", $foodCategoryIDs) : $this->FoodCategoryIDs();
		$dailyMenuPages = $this->getDailyMenuPagesByDate();

		$usedGlobalAdditives = new ArrayList();
		$usedSubsiteAdditives = new ArrayList();
		foreach ($dailyMenuPages as $dailyMenuPage) {
			/*
			 * Get dishes that are used on the current DailyMenuPages and have
			 * a food category used on this *TemplatePage
			 */
			$usedDishes = $dailyMenuPage->Dishes()
			                            ->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			                            ->filter(array('FoodCategoryID' => $foodCategoryIDArray));
			foreach ($usedDishes as $dish) {
				/* Get each dish's GlobalAdditives */
				$globalAdditives = $dish->GlobalAdditives();
				if ($globalAdditives) {
					foreach ($globalAdditives as $additive) {
						if (!$usedGlobalAdditives->find('ID', $additive->ID)) {
							$usedGlobalAdditives->push($additive);
						}
					}
				}
				// Get each SideDish´s GlobalAdditives */
				if($includeSideDishes) {
					foreach($dish->AllSideDishes() as $sideDish) {
						$sideDishGlobalAdditives = $sideDish->GlobalAdditives();
						if ($sideDishGlobalAdditives) {
							foreach ($sideDishGlobalAdditives as $additive) {
								if (!$usedGlobalAdditives->find('ID', $additive->ID)) {
									$usedGlobalAdditives->push($additive);
								}
							}
						}
					}
				}
				$usedGlobalAdditives = $usedGlobalAdditives->sort('SortOrder');

				/* Get each dish's SubsiteAdditives */
				$subsiteAdditives = $dish->SubsiteAdditives();
				if ($subsiteAdditives) {
					foreach ($subsiteAdditives as $additive) {
						if (!$usedSubsiteAdditives->find('ID', $additive->ID)) {
							$usedSubsiteAdditives->push($additive);
						}
					}
				}
				// Get each SideDish´s SubsiteAdditives */
				if($includeSideDishes) {
					foreach($dish->AllSideDishes() as $sideDish) {
						$sideDishSubsiteAdditives = $sideDish->SubsiteAdditives();
						if ($sideDishSubsiteAdditives) {
							foreach ($sideDishSubsiteAdditives as $additive) {
								if (!$usedSubsiteAdditives->find('ID', $additive->ID)) {
									$usedSubsiteAdditives->push($additive);
								}
							}
						}
					}
				}
				$usedSubsiteAdditives = $usedSubsiteAdditives->sort('SortOrder');
			}
		}

		return MyListUtil::concatenateLists($usedGlobalAdditives, $usedSubsiteAdditives);
	}

	/**
	 * Returns (Global|Subsite)Allergens that are used by Dishes that
	 * - are used on DailyMenuPages of the current restaurant that fall within
	 *   a given date range
	 * - belong to FoodCategories that are selected on the current *TemplatePage
	 *
	 * GlobalAllergens come first followed by SubsiteAllergens, each of them
	 * in turn sorted by their defined SortOrder.
	 * 
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedAllergens($includeSideDishes = false, $foodCategoryIDs = null) {
		$foodCategoryIDArray = $foodCategoryIDs ? explode(",", $foodCategoryIDs) : $this->FoodCategoryIDs();
		$dailyMenuPages = $this->getDailyMenuPagesByDate();

		$usedGlobalAllergens = new ArrayList();
		$usedSubsiteAllergens = new ArrayList();
		foreach ($dailyMenuPages as $dailyMenuPage) {
			/*
			 * Get dishes that are used on the current DailyMenuPages and have
			 * a food category used on this *TemplatePage
			 */
			$usedDishes = $dailyMenuPage->Dishes()
			                            ->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			                            ->filter(array('FoodCategoryID' => $foodCategoryIDArray));
			foreach ($usedDishes as $dish) {
				/* Get each dish's GlobalAllergens */
				$globalAllergens = $dish->GlobalAllergens();
				if ($globalAllergens) {
					foreach ($globalAllergens as $allergen) {
						if (!$usedGlobalAllergens->find('ID', $allergen->ID)) {
							$usedGlobalAllergens->push($allergen);
						}
					}
				}
				// Get each SideDish´s GlobalAllergens */
				if($includeSideDishes) {
					foreach($dish->AllSideDishes() as $sideDish) {
						$sideDishGlobalAllergens = $sideDish->GlobalAllergens();
						if ($sideDishGlobalAllergens) {
							foreach ($sideDishGlobalAllergens as $allergen) {
								if (!$usedGlobalAllergens->find('ID', $allergen->ID)) {
									$usedGlobalAllergens->push($allergen);
								}
							}
						}
					}
				}
				$usedGlobalAllergens = $usedGlobalAllergens->sort('SortOrder');

				/* Get each dish's SubsiteAllergens */
				$subsiteAllergens = $dish->SubsiteAllergens();
				if ($subsiteAllergens) {
					foreach ($subsiteAllergens as $allergen) {
						if (!$usedSubsiteAllergens->find('ID', $allergen->ID)) {
							$usedSubsiteAllergens->push($allergen);
						}
					}
				}
				// Get each SideDish´s SubsiteAdditives */
				if($includeSideDishes) {
					foreach($dish->AllSideDishes() as $sideDish) {
						$sideDishSubsiteAllergens = $sideDish->SubsiteAllergens();
						if ($sideDishSubsiteAllergens) {
							foreach ($sideDishSubsiteAllergens as $allergen) {
								if (!$usedSubsiteAllergens->find('ID', $allergen->ID)) {
									$usedSubsiteAllergens->push($allergen);
								}
							}
						}
					}
				}
				$usedSubsiteAllergens = $usedSubsiteAllergens->sort('SortOrder');
			}
		}

		return MyListUtil::concatenateLists($usedGlobalAllergens, $usedSubsiteAllergens);
	}

	/**
	 * Returns (Global|Subsite)DishLabels that are used by Dishes that
	 * - are used on DailyMenuPages of the current restaurant that fall within
	 *   a given date range
	 * - belong to FoodCategories that are selected on the current *TemplatePage
	 *
	 * GlobalDishLabels come first followed by SubsiteDishLabels, each of them
	 * in turn sorted by their defined SortOrder.
	 *
	 * @param Boolean $includeSideDishes (optional) True if Additives of SideDsihes should be included. Default: false;
	 * @param String $foodCategoryIDs (optional) Comma seperated String with FoodCategory IDs to filter results
	 * @return ArrayList
	 */
	public function UsedDishLabels($includeSideDishes = false, $foodCategoryIDs = null) {
		$foodCategoryIDArray = $foodCategoryIDs ? explode(",", $foodCategoryIDs) : $this->FoodCategoryIDs();
		$dailyMenuPages = $this->getDailyMenuPagesByDate();

		$usedGlobalDishLabels = new ArrayList();
		$usedSubsiteDishLabels = new ArrayList();
		foreach ($dailyMenuPages as $dailyMenuPage) {
			/*
			 * Get dishes that are used on the current DailyMenuPages and have
			 * a food category used on this *TemplatePage
			 */
			$usedDishes = $dailyMenuPage->Dishes()
			                            ->leftJoin('RestaurantDish', 'RestaurantDish.DishID=Dish.ID')
			                            ->filter(array('FoodCategoryID' => $foodCategoryIDArray));
			foreach ($usedDishes as $dish) {
				/* Get each dish's GlobalDishLabels */
				$globalDishLabels = $dish->GlobalDishLabels();
				if ($globalDishLabels) {
					foreach ($globalDishLabels as $dishlabel) {
						if (!$usedGlobalDishLabels->find('ID', $dishlabel->ID)) {
							$usedGlobalDishLabels->push($dishlabel);
						}
					}
				}
				// Get each SideDish´s GlobalDishLabels */
				if($includeSideDishes) {
					foreach($dish->AllSideDishes() as $sideDish) {
						$sideDishGlobalDishLabels = $sideDish->GlobalDishLabels();
						if ($sideDishGlobalDishLabels) {
							foreach ($sideDishGlobalDishLabels as $dishlabel) {
								if (!$usedGlobalDishLabels->find('ID', $dishlabel->ID)) {
									$usedGlobalDishLabels->push($dishlabel);
								}
							}
						}
					}
				}
				$usedGlobalDishLabels = $usedGlobalDishLabels->sort('SortOrder');

				/* Get each dish's SubsiteDishLabels */
				$subsiteDishLabels = $dish->SubsiteDishLabels();
				if ($subsiteDishLabels) {
					foreach ($subsiteDishLabels as $dishlabel) {
						if (!$usedSubsiteDishLabels->find('ID', $dishlabel->ID)) {
							$usedSubsiteDishLabels->push($dishlabel);
						}
					}
				}
				// Get each SideDish´s SubsiteDishLabels */
				if($includeSideDishes) {
					foreach($dish->AllSideDishes() as $sideDish) {
						$sideDishSubsiteDishLabels = $sideDish->SubsiteDishLabels();
						if ($sideDishSubsiteDishLabels) {
							foreach ($sideDishSubsiteDishLabels as $dishlabel) {
								if (!$usedSubsiteDishLabels->find('ID', $dishlabel->ID)) {
									$usedSubsiteDishLabels->push($dishlabel);
								}
							}
						}
					}
				}
				$usedSubsiteDishLabels = $usedSubsiteDishLabels->sort('SortOrder');
			}
		}

		return MyListUtil::concatenateLists($usedGlobalDishLabels, $usedSubsiteDishLabels);
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_DISPLAYTEMPLATEOBJECT') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_DISPLAYTEMPLATEOBJECT') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_DISPLAYTEMPLATEOBJECT') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_DISPLAYTEMPLATEOBJECT') ? true : false;
		return $canView;
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_DISPLAYTEMPLATEOBJECT' => array(
				'name' => 'Kann Bildschirm Speiseplan betrachten',
				'category' => 'Bildschirm Ausgabe Mehrfache Templates',
				'sort' => 10
			),
			'EDIT_DISPLAYTEMPLATEOBJECT' => array(
				'name' => 'Kann Bildschirm Speiseplan bearbeiten',
				'category' => 'Bildschirm Ausgabe Mehrfache Templates',
				'sort' => 20
			),
			'CREATE_DISPLAYTEMPLATEOBJECT' => array(
				'name' => 'Kann Bildschirm Speiseplan erstellen',
				'category' => 'Bildschirm Ausgabe Mehrfache Templates',
				'sort' => 30
			),
			'DELETE_DISPLAYTEMPLATEOBJECT' => array(
				'name' => 'Kann Bildschirm Speiseplan löschen',
				'category' => 'Bildschirm Ausgabe Mehrfache Templates',
				'sort' => 40
			)
		);
	}
}
