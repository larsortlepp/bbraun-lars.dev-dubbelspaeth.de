<?php

/**
 * SubsiteSpecialLabel
 *
 * A subsite-specific label (image / icon) for special events that is
 * assigned to Dishes (e.g. "Coa", "Curry Edition")
 */
class SubsiteSpecialLabel extends DataObject implements PermissionProvider {
	private static $db = array (
		'Title' => 'Varchar',
		'Title_en_US' => 'Varchar',
		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	private static $belongs_many_many = array(
		'Dishes' => 'Dish'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$f = new FieldList();

		$f->push(new HeaderField('SpecialLabelHeader', 'Aktion (mandantenspezifisch)'));

		$f->push(new TextField('Title', 'Titel'));
		$f->push(new TextField('Title_en_US', 'Titel (englisch)'));
		$f->push($uploadField = new CustomUploadField('Image', 'Aktionsbild'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default DishLabel folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderSpecialLabelPath());

		return $f;
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}

	/**
	 * return Thumbnail of Image
	 *
	 * @return Image
	 */
	public function getImage_CMSThumbnail() {
		return ($Image = $this->Image()) ? $Image->CMSThumbnail() : false;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_SUBSITESPECIALLABEL') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_SUBSITESPECIALLABEL') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_SUBSITESPECIALLABEL') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_SUBSITESPECIALLABEL') ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_SUBSITESPECIALLABEL' => array(
				'name' => 'Kann Aktionen Mandant betrachten',
				'category' => 'Aktionen Mandant',
				'sort' => 10
			),
			'EDIT_SUBSITESPECIALLABEL' => array(
				'name' => 'Kann Aktionen Mandant bearbeiten',
				'category' => 'Aktionen Mandant',
				'sort' => 20
			),
			'CREATE_SUBSITESPECIALLABEL' => array(
				'name' => 'Kann Aktionen Mandant erstellen',
				'category' => 'Aktionen Mandant',
				'sort' => 30
			),
			'DELETE_SUBSITESPECIALLABEL' => array(
				'name' => 'Kann Aktionen Mandant löschen',
				'category' => 'Aktionen Mandant',
				'sort' => 40
			)
		);
	}
}
