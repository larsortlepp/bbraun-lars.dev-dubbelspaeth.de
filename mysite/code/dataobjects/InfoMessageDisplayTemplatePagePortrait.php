<?php

/**
 *  Message that is unique to a Subsite and can be assigned to a DisplayTemplatePagePortrait
 */
class InfoMessageDisplayTemplatePagePortrait extends InfoMessageDisplayTemplatePage {
	
	private static $has_one = array (
		'DisplayTemplatePagePortrait' => 'DisplayTemplatePagePortrait',
		'InfoMessageTemplateDisplayTemplatePagePortrait' => 'InfoMessageTemplateDisplayTemplatePagePortrait',
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		if($this->InfoMessageTemplateDisplayTemplatePagePortraitID) {
			$infoMessage = $this->InfoMessageTemplateDisplayTemplatePagePortrait();
			$f->addFieldToTab('Root.Main', LiteralField::create('InfoMessageTemplateInfo', '<p class="message notice">Erstellt durch News-Template '.$infoMessage->Title.' (ID: '.$infoMessage->ID.') am '.$this->obj('Created')->FormatFromSettings().'</p>'), 'PublishGroup');
		}
		
		return $f;
	}
}
