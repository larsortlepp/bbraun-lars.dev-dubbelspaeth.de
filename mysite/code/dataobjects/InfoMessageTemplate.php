<?php

/**
 * Template for InfoMessages
 * Base class for template specific InfoMessageTemplates
 * 
 * InfoMessageTemplates can be created globally or per mandator
 * (as set in SiteConfig::InfoMessageTemplateType)
 * and the be copied to all TemplatePages of all Restaurants
 * 
 * @todo Implement UI to select the RestaurantPages that the InfoMessageTemplate will be copied to
 */

class InfoMessageTemplate extends InfoMessage {

	private static $field_labels = array(
		'Image.CMSThumbnail' => 'Bild',
		'Image.Name' => 'Dateiname Bild',
		'Title' => 'Titel',
		'ContentGridField' => 'Inhalt',
		'Content' => 'Inhalt',
		'CollapsedDateTimeGridField' => 'Anzeigedauer',
		'TimerStartDate' => 'Start Datum',
		'TimerStartTime' => 'Start Uhrzeit',
		'TimerEndDate' => 'End Datum',
		'TimerEndTime' => 'End Uhrzeit',
		'Publish' => 'Veröffentlicht',
		'Publish.Nice' => 'Veröffentlicht'
   );
	
	private static $summary_fields = array(
		'Image.CMSThumbnail',
		'Title',
		'ContentGridField',
		'CollapsedDateTimeGridField',
		'Publish.Nice'
   );
	
	private static $searchable_fields = array(
		'Title',
		'Content',
		'Publish',
		'Image.Name',
		'TimerStartDate',
		'TimerEndDate',
		'TimerStartTime',
		'TimerEndTime'
   );
	
	private static $default_sort = 'Created DESC';
	
	public function getCMSActions() {
		$actions = parent::getCMSActions();

		// Custom action to publish news to all TemplatePages
		$copyAction = FormAction::create('doCopyToTemplatePages', 'In alle Ausgabe Templates kopieren')
			->addExtraClass('ss-ui-action') // regular grey button. Use 'ss-ui-action-constructive' for green button.
			->setUseButtonTag(true)
			->setAttribute('data-icon', 'drive-upload'); // add icon -> show a "processing" indicator when button was clicked
		$actions->push($copyAction);

		return $actions;
	}

}
