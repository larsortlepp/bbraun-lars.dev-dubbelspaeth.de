<?php

class RecurringDayOfWeek extends DataObject {

	private static $db = array (
		'Value' => 'Int',
		'Skey' => 'Varchar(2)'
	);

	private static $belongs_many_many = array (
		'InfoMessages' => 'InfoMessage',
		'DisplayTemplatePages' => 'DisplayTemplatePages'
	);

	private static $days_initial = array ('So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa');

	public static function doDefaultRecords()
	{
		for($i = 0; $i <= 6; $i++)
		{
			$record = new RecurringDayOfWeek();
			$record->Value = $i;
			$record->Skey = self::$days_initial[$i];
			$record->write();
		}
	}
	public function requireDefaultRecords()
	{
		parent::requireDefaultRecords();
		if (!RecurringDayOfWeek::get()->count())
		{
			self::doDefaultRecords();
		}
		elseif($records = RecurringDayOfWeek::get())
		{
			if($records->Count() < 7)
			{
				foreach($records as $record)
				{
					$record->delete();
				}
				self::doDefaultRecords();
			}
		}

		//DB::alteration_message("Recurring Days of Week added.","created");
	}

	/**
	 * Returns an associative Array with ID => WeekDayName
	 * for setting RecurringDayOfWeek to formfields (DropDownField, CheckboxSetField, etc.)
	 * 
	 * @param Boolean $beginWithMonday if true (default) week days are starting with monday, if false weekdays will start with sunday
	 * @return Array Array with ID and WeekDayName
	 */
	public static function map_days_of_week($beginWithMonday = true)
	{		
		$daysOfWeek = RecurringDayOfWeek::get()->sort('Value ASC');
		$sortedDaysOfWeek = ArrayList::create();
		foreach($daysOfWeek as $dayOfWeek) {
			// Sort Days of week beginning with monday (not sunday)
			if($beginWithMonday && $dayOfWeek->Value == 0) {
				$sunday = $dayOfWeek;
			}
			else {
				$sortedDaysOfWeek->push($dayOfWeek);
			}
		}
		if($beginWithMonday)
			$sortedDaysOfWeek->push($sunday);
		
		return $sortedDaysOfWeek->map('ID', 'Skey');
	}
}
