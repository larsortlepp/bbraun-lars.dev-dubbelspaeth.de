<?php

/**
 * DishVoting
 *
 * Object for voting multiple dishes (like a poll)
 *
 */
class DishVotingItem extends DataObject implements PermissionProvider {

	private static $db = array(
		'DishDescription' => 'Text',
		'DishDescription_en_US' => 'Text',
		'Votes' => 'Int',
		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'DishVotingPage' => 'DishVotingPage'
	);

	public function getCMSFields()
	{
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		
		$f = new FieldList();
		
		$f->push(new HeaderField('DishVotingHeader',  'Voting Gericht'));
		$f->push(new TextareaField('DishDescription', 'Beschreibung Gericht'));
		if($showLang_en_US) $f->push(new TextareaField('DishDescription_en_US', 'Beschreibung Gericht englisch'));
		
		return $f;
	}

	public function getVotesPercent($postfix = '%') {
		// avoid division by zero
		$percent = ($this->Votes > 0) ? (round($this->Votes/$this->DishVotingPage()->getDishVotesTotal()*100)) : 0;
		return $percent . $postfix;
	}

	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_DISHVOTINGITEM') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_DISHVOTINGITEM') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_DISHVOTINGITEM') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_DISHVOTINGITEM') ? true : false;
		return $canView;
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_DISHVOTINGITEM' => array(
				'name' => 'Kann Voting Wunschgerichte betrachten',
				'category' => 'Voting Wunschgerichte',
				'sort' => 10
			),
			'EDIT_DISHVOTINGITEM' => array(
				'name' => 'Kann Voting Wunschgerichte bearbeiten',
				'category' => 'Voting Wunschgerichte',
				'sort' => 20
			),
			'CREATE_DISHVOTINGITEM' => array(
				'name' => 'Kann Voting Wunschgerichte erstellen',
				'category' => 'Voting Wunschgerichte',
				'sort' => 30
			),
			'DELETE_DISHVOTINGITEM' => array(
				'name' => 'Kann Voting Wunschgerichte löschen',
				'category' => 'Voting Wunschgerichte',
				'sort' => 40
			)
		);
	}
}
