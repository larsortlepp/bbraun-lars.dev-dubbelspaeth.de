<?php

/**
 * Message that is unique to a Subsite and can be assigned to a PrintTemplatePage
 */
class InfoMessagePrintTemplatePage extends InfoMessage implements PermissionProvider {
	
	private static $has_one = array (
		'InfoMessageTemplatePrintTemplatePage' => 'InfoMessageTemplatePrintTemplatePage',
		'PrintTemplatePage' => 'PrintTemplatePage',
		'PrintHtmlTemplatePage' => 'PrintHtmlTemplatePage'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		if($this->InfoMessageTemplatePrintTemplatePageID) {
			$infoMessage = $this->InfoMessageTemplatePrintTemplatePage();
			$f->addFieldToTab('Root.Main', LiteralField::create('InfoMessageTemplateInfo', '<p class="message notice">Erstellt durch News-Template '.$infoMessage->Title.' (ID: '.$infoMessage->ID.') am '.$this->obj('Created')->FormatFromSettings().'</p>'), 'PublishGroup');
		}
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGEPRINTTEMPLATEPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGEPRINTTEMPLATEPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGEPRINTTEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGEPRINTTEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE' => array(
				'name' => 'Kann News für Druck Ausgabe betrachten',
				'category' => 'News',
				'sort' => 510
			),
			'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE' => array(
				'name' => 'Kann News für Druck Ausgabe bearbeiten',
				'category' => 'News',
				'sort' => 520
			),
			'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE' => array(
				'name' => 'Kann News für Druck Ausgabe erstellen',
				'category' => 'News',
				'sort' => 530
			),
			'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE' => array(
				'name' => 'Kann News für Druck Ausgabe löschen',
				'category' => 'News',
				'sort' => 540
			)
		);
	}
}
