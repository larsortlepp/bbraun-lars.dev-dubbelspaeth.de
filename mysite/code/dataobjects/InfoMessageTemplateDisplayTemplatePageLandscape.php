<?php

class InfoMessageTemplateDisplayTemplatePageLandscape extends InfoMessageTemplateDisplayTemplatePage {
	
	/**
	 * ClassName of InfoMessage class that the template content will be copied to
	 * 
	 * @var string
	 */
	public static $target_classname = 'InfoMessageDisplayTemplatePageLandscape';
	
	/**
	 * ClassNames of classes that will hold the InfoMessage
	 * 
	 * @var array
	 */
	public static $target_relation_classnames = array (
		'DisplayTemplatePageLandscape'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$f->removeByName('InfoMessageHeader');
		$f->addFieldToTab('Root.Main', HeaderField::create('InfoMessageHeader', 'News Template Bildschirm quer'), 'PublishGroup');
		
		return $f;
	}
}
