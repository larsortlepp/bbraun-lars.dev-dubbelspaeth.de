<?php

/**
 * Message that is unique to a Subsite and can be assigned to a MicrositeHomePage
 */
class InfoMessageMicrositeHomePage extends InfoMessage implements PermissionProvider {
	
	private static $has_one = array (
		'InfoMessageTemplateMicrositeHomePage' => 'InfoMessageTemplateMicrositeHomePage',
		'MicrositeHomePage' => 'MicrositeHomePage'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		
		$this->extend('updateCMSFields', $f);
		
		if($this->InfoMessageTemplateMicrositeHomePageID) {
			$infoMessage = $this->InfoMessageTemplateMicrositeHomePage();
			$f->addFieldToTab('Root.Main', LiteralField::create('InfoMessageTemplateInfo', '<p class="message notice">Erstellt durch News-Template '.$infoMessage->Title.' (ID: '.$infoMessage->ID.') am '.$this->obj('Created')->FormatFromSettings().'</p>'), 'PublishGroup');
		}
		
		// remove fullscreen image field
		$f->removeFieldFromTab('Root.Main', 'ImageOnlyGroup');
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGEMICROSITEHOMEPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGEMICROSITEHOMEPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGEMICROSITEHOMEPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGEMICROSITEHOMEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News für Microsite Startseite betrachten',
				'category' => 'News',
				'sort' => 210
			),
			'EDIT_INFOMESSAGEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News für Microsite Startseite bearbeiten',
				'category' => 'News',
				'sort' => 220
			),
			'CREATE_INFOMESSAGEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News für Microsite Startseite erstellen',
				'category' => 'News',
				'sort' => 230
			),
			'DELETE_INFOMESSAGEMICROSITEHOMEPAGE' => array(
				'name' => 'Kann News für Microsite Startseite löschen',
				'category' => 'News',
				'sort' => 240
			)
		);
	}
}
