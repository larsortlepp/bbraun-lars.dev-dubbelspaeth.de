<?php

/**
 * SubsiteAllergen
 *
 * Subsite-specific list of food allergens.
 * Allergens can be added to Dishes.
 *
 */
class SubsiteAllergen extends DataObject implements PermissionProvider {
	private static $db = array(
		'Number' => 'Varchar(255)',
		'Title' => 'Varchar(255)',
		'Title_en_US' => 'Varchar(255)',
		'SortOrder' => 'Int'
	);

	private static $belongs_many_many = array(
		'Dishes' => 'Dish'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$f = new FieldList();

		$f->push(new HeaderField('AllergenHeader', 'Allergen (mandantenspezifisch)'));
		$f->push(new TextField('Number', 'Nummer'));
		$f->push(new TextField('Title', 'Bezeichnung'));
		$f->push(new TextField('Title_en_US', 'Bezeichnung (englisch)'));
		$f->push(new HeaderField('EmptyHeader', '&nbsp;'));

		return $f;
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}
	
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_SUBSITEALLERGEN') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_SUBSITEALLERGEN') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_SUBSITEALLERGEN') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_SUBSITEALLERGEN') ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_SUBSITEALLERGEN' => array(
				'name' => 'Kann Allergene Mandant betrachten',
				'category' => 'Allergene Mandant',
				'sort' => 10
			),
			'EDIT_SUBSITEALLERGEN' => array(
				'name' => 'Kann Allergene Mandant bearbeiten',
				'category' => 'Allergene Mandant',
				'sort' => 20
			),
			'CREATE_SUBSITEALLERGEN' => array(
				'name' => 'Kann Allergene Mandant erstellen',
				'category' => 'Allergene Mandant',
				'sort' => 30
			),
			'DELETE_SUBSITEALLERGEN' => array(
				'name' => 'Kann Allergene Mandant löschen',
				'category' => 'Allergene Mandant',
				'sort' => 40
			)
		);
	}
}
