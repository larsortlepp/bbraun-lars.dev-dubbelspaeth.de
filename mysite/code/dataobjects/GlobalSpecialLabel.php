<?php

/**
 * GlobalSpecialLabel
 *
 * A global label (image / icon) for special events that is
 * assigned to Dishes (e.g. "Coa", "Curry Edition")
 */
class GlobalSpecialLabel extends DataObject {
	private static $db = array (
		'Title' => 'Varchar',
		'Title_en_US' => 'Varchar',
		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	private static $belongs_many_many = array(
		'Dishes' => 'Dish'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$f = new FieldList();

		$f->push(new HeaderField('SpecialLabelHeader', 'Aktion (global)'));

		$f->push(new TextField('Title', 'Titel'));
		$f->push(new TextField('Title_en_US', 'Titel (englisch)'));
		$f->push($uploadField = new CustomUploadField('Image', 'Aktionsbild'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default DishLabel folder
		$uploadField->setFolderName(MySiteConfigExtension::$defaults['DefaultFolderSpecialLabel']);

		return $f;
	}

	/*
	 * Checks if the user has the permission to delete GlobalSpecialLabel records
	 *
	 * @return  Boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('ADMIN');
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}

	/**
	 * return Thumbnail of Image
	 *
	 * @return Image
	 */
	public function getImage_CMSThumbnail() {
		return ($Image = $this->Image()) ? $Image->CMSThumbnail() : false;
	}
}
