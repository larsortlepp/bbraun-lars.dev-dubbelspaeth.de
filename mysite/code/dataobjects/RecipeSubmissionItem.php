<?php

/**
 * RecipeSubmissionItem
 *
 * Object for submitting a free text recipe
 * --> parallel to the "Wunschtopf" submissions
 *
 */
class RecipeSubmissionItem extends DataObject implements PermissionProvider {

	private static $db = array(
		'Recipe' => 'Text',
		'Name' => 'Varchar(255)',
		'Email' => 'Varchar(255)'
	);

	private static $has_one = array(
		'RecipeSubmissionPage' => 'RecipeSubmissionPage'
	);

	private static $default_sort = 'Created DESC';

	public function getCMSFields()
	{
		return new FieldList(
			new HeaderField('RecipeSubmissionHeader',  'Wunschgericht', 1),
			new ReadonlyField('Recipe', 'Rezept Wunschgericht'),
			new ReadonlyField('Name', 'Name (freiwillig)'),
			new ReadonlyField('Email', 'E-mail (freiwillig)')
		);
	}

	public function getCreatedNice($format = '%d.%m.%Y') {
		return DateUtil::dateFormat($this->Created, $format);
	}

	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_RECIPESUBMISSIONITEM') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_RECIPESUBMISSIONITEM') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_RECIPESUBMISSIONITEM') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_RECIPESUBMISSIONITEM') ? true : false;
		return $canView;
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_RECIPESUBMISSIONITEM' => array(
				'name' => 'Kann Wunschtopf Einreichungen betrachten',
				'category' => 'Wunschtopf',
				'sort' => 10
			),
			'EDIT_RECIPESUBMISSIONITEM' => array(
				'name' => 'Kann Wunschtopf Einreichungen bearbeiten',
				'category' => 'Wunschtopf',
				'sort' => 20
			),
			'CREATE_RECIPESUBMISSIONITEM' => array(
				'name' => 'Kann Wunschtopf Einreichungen erstellen',
				'category' => 'Wunschtopf',
				'sort' => 30
			),
			'DELETE_RECIPESUBMISSIONITEM' => array(
				'name' => 'Kann Wunschtopf Einreichungen löschen',
				'category' => 'Wunschtopf',
				'sort' => 40
			)
		);
	}
}
