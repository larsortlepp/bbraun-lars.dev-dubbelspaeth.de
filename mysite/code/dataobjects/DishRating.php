<?php

/**
 * DishRating
 * Object stores user rating and feedback for a single dish
 */
class DishRating extends DataObject implements PermissionProvider {

	private static $db = array(
		'Date' => 'Date',
		'DishDescription' => 'Text',
		'FoodCategoryTitle' => 'Varchar(255)',
		'Taste' => 'Int',
		'Value' => 'Int',
		'Service' => 'Int',
		'Comment' => 'Text'
	);

	private static $has_one = array(
		'Dish' => 'Dish',
		'MenuContainer' => 'MenuContainer'
	);
	
	private static $defaults = array(
		'Taste' => null,
		'Value' => null,
		'Service' => null
	);

	/**
	 * Define search context to allow searching for date ranges.
	 *
	 * @link http://doc.silverstripe.org/en/developer_guides/search/searchcontext
	 */
	public function getDefaultSearchContext() {
		$fields = $this->scaffoldSearchFields(array(
			'restrictFields' => array('FoodCategoryTitle', 'DishDescription', 'RestaurantPageID')
		));

		$filters = array(
			'FoodCategoryTitle' => new PartialMatchFilter('FoodCategoryTitle'),
			'DishDescription' => new PartialMatchFilter('DishDescription'),
			'StartDate' => new GreaterThanOrEqualFilter('Date'),
			'EndDate' => new LessThanOrEqualFilter('Date'),
			'RestaurantPageID' => new ExactMatchFilter('RestaurantPageID'),
			'MenuContainerID' => new ExactMatchFilter('MenuContainerID')
		);

		return new SearchContext(
			$this->class,
			$fields,
			$filters
		);
	}

	private static $summary_fields = array(
		'Date' => 'Datum',
		'FoodCategoryTitle' => 'Essenskategorie',
		'DishDescription' => 'Gericht',
		'Taste' => 'Geschmack',
		'Value' => 'Preis-/Leistung',
		'Service' => 'Service',
		'Comment' => 'Kommentar',
		'RestaurantName' => 'Restaurant',
		'MenuContainerName' => 'Speiseplan'
	);

	public function scaffoldSearchFields($params = null) {
		$fields = parent::scaffoldSearchFields($params);

		$fields->renameField('DishDescription', 'Gericht');
		$fields->renameField('FoodCategoryTitle', 'Essenskategorie');

		$startDateField = new DateField('StartDate', 'Start Datum');
		$startDateField->setConfig('showcalendar', true);
		$fields->push($startDateField);

		$endDateField = new DateField('EndDate', 'End Datum');
		$endDateField->setConfig('showcalendar', true);
		$fields->push($endDateField);

		/*
		 * If we can't determine a subsite, we're in the "main site" and must
		 * disable the subsite filter which otherwise automatically appends
		 * a zero SubsiteID to RestaurantPage::get() below
		 */
		if (!Subsite::currentSubsite())
			Subsite::disable_subsite_filter();

		// RestaurantPage filter
		$subsiteRestaurantPages = RestaurantPage::get();
		$viewableRestaurantPages = new ArrayList();
		if ($subsiteRestaurantPages) {
			foreach ($subsiteRestaurantPages as $subsiteRestaurantPage) {
				if ($subsiteRestaurantPage->can("Edit"))
					$viewableRestaurantPages->push($subsiteRestaurantPage);
			}
		}
		$restaurantsSource = $viewableRestaurantPages->map('ID', 'Title');
		$restaurantsDD = DropdownField::create('RestaurantPageID', 'Restaurant', $restaurantsSource)->setEmptyString('-- Alle --');
		
		// MenuContainer filter (only MenuContainer of allowed restaurants)
		$menuContainers = MenuContainer::get();
		if($restaurantsSource)
			$menuContainers = $menuContainers->filter('ParentID', array_keys($restaurantsSource));
		$viewableMenuContainers = new ArrayList();
		if ($menuContainers) {
			foreach ($menuContainers as $menuContainer) {
				if ($menuContainer->can("Edit"))
					$viewableMenuContainers->push($menuContainer);
			}
		}
		$viewableMenuContainers = $viewableMenuContainers->sort(array('ParentID' => 'ASC', 'Sort' => 'ASC'));
		$menuContainerSource = array();
		// generate 2-dimensional array for grouped dropdown array by adding restaurant name 
		foreach($viewableMenuContainers as $viewableMenuContainer) {
			$restaurantPage = RestaurantPage::get()->byID($viewableMenuContainer->ParentID);
			$menuContainerSource[$restaurantPage->Title][$viewableMenuContainer->ID] = $viewableMenuContainer->Title;
		}
		$menuContainerDD = GroupedDropdownField::create('MenuContainerID', 'Speiseplan', $menuContainerSource)->setEmptyString('-- Alle --');

		/*
		 * Reenable the filter just in case we disabled it.
		 */
		Subsite::disable_subsite_filter(false);

		$fields->push($restaurantsDD);
		$fields->push($menuContainerDD);

		/*
		 * Reenable the filter just in case we disabled it.
		 */
		Subsite::disable_subsite_filter(false);

		return $fields;
	}

	public function getCMSFields() {
		$created = new DateField('Created', 'Datum');
		$created->setLocale(i18n::get_locale());
		$created = $created->performReadonlyTransformation();
		
		$foodCategoryTitle = new TextField('FoodCategoryTitle', 'Essenskategorie');
		$foodCategoryTitle = $foodCategoryTitle->performReadonlyTransformation();

		$taste = new TextField('Taste', 'Geschmack');
		$taste = $taste->performReadonlyTransformation();

		$value = new TextField('Value', 'Preis-/Leistung');
		$value = $value->performReadonlyTransformation();

		$service = new TextField('Service', 'Service');
		$service = $service->performReadonlyTransformation();

		$comment = new TextareaField('Comment', 'Kommentar');
		$comment = $comment->performReadonlyTransformation();

		return new FieldList(
			new HeaderField('DishRating', $this->DishDescription),
			$foodCategoryTitle,
			$created,
			$taste,
			$value,
			$service,
			$comment
		);
	}

	public function getRestaurantName() {
		$restaurantPage = RestaurantPage::get()->byID($this->RestaurantPageID);
		return $restaurantPage ? $restaurantPage->Title : false;
	}
	
	public function getMenuContainerName() {
		$menuContainer = MenuContainer::get()->byID($this->MenuContainerID);
		return $menuContainer ? $menuContainer->Title : false;
	}

	public function getDishDescription_en_US() {
		$dishDescription = ($this->DishID && $this->Dish()) ? $this->Dish()->Description_en_US : false;
		return $dishDescription;
	}

	public function getSubmissionDate() {
		return date('d.m.Y', strtotime($this->Date));
	}

	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_DISHRATING')) && $this->MenuContainer()->can("edit")  ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = (Permission::check('EDIT_DISHRATING') && $this->MenuContainer()->can("edit"))  ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_DISHRATING') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_DISHRATING') ? true : false;
		return $canView;
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_DISHRATING' => array(
				'name' => 'Kann Gerichtebewertungen betrachten',
				'category' => 'Gerichtebewertungen',
				'sort' => 10
			),
			'EDIT_DISHRATING' => array(
				'name' => 'Kann Gerichtebewertungen bearbeiten',
				'category' => 'Gerichtebewertungen',
				'sort' => 20
			),
			'CREATE_DISHRATING' => array(
				'name' => 'Kann Gerichtebewertungen erstellen',
				'category' => 'Gerichtebewertungen',
				'sort' => 30
			),
			'DELETE_DISHRATING' => array(
				'name' => 'Kann Gerichtebewertungen löschen',
				'category' => 'Gerichtebewertungen',
				'sort' => 40
			)
		);
	}
}
