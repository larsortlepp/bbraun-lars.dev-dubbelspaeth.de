<?php

class MicrositeLinkItem extends DataObject implements PermissionProvider {
	
	private static $db = array(
		'Title' => 'Varchar(255)',
		'Title_en_US' => 'Varchar(255)',
		"LinkType" => "Enum('Internal,External,Download','Download')",
		"ExternalURL" => "Varchar(2083)", // 2083 is the maximum length of a URL in Internet Explorer.
		'Sort' => 'Int'
	);
	
	private static $defaults = array(
		"LinkType" => "Download"
	);
	
	private static $has_one = array(
		'MicrositePage' => 'MicrositePage',
		"LinkTo" => "SiteTree",
		'Download' => 'File'
	);
	
	private static $field_labels = array(
		'Title' => 'Titel',
		'LinkType' => 'Typ',
		'LinkTypeNice' => 'Typ',
		'Link' => 'Link'
	);
	
	private static $summary_fields = array(
		'Title',
		'LinkTypeNice',
		'Link'
	);
	
	public function getCMSFields() {
		
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		
		$micrositeContainer = MicrositeContainer::get()
			->filter(array(
				'ParentID' => $restaurantPage->ID
			))
			->limit(1)
			->first();
		
		// create basic tab set with 'Root' tab
		// this can be extended by new tabs and be modified with 
		// $f->addFieldToTab('Root', Field::create(...));
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldToTab('Root.Main', Textfield::create('Title', 'Link Bezeichnung'));
		if($showLang_en_US) $f->addFieldToTab('Root.Main', Textfield::create('Title_en_US', 'Link Bezeichnung (englisch)'));
		
		$f->addFieldToTab('Root.Main', OptionsetField::create(
			"LinkType", 
			'Link Typ',
			array(
				"Internal" => _t('RedirectorPage.REDIRECTTOPAGE', "A page on your website"),
				"External" => _t('RedirectorPage.REDIRECTTOEXTERNAL', "Another website"),
				"Download" => 'Datei Download'
			)
		));
		
		// Limit SiteTree dropdown to pages of current Microsite
		$f->addFieldToTab('Root.Main', TreeDropdownField::create(	
				"LinkToID", 
				_t('RedirectorPage.YOURPAGE', "Page on your website"), 
				"SiteTree"
			)
			->setTreeBaseID($micrositeContainer->ID)
		);
		
		$f->addFieldToTab('Root.Main', TextField::create(
			"ExternalURL", 
			_t('RedirectorPage.OTHERURL', "Other website URL")
		));
		
		$f->addFieldToTab('Root.Main', CustomUploadField::create(	
				"Download", 
				"Datei Download"
			)
			->setFolderName(SiteConfig::current_site_config()->DefaultFolderMicrositePath($restaurantPage->ID))
		);
		
		return $f;
		
	}
	
	public function Link() {
		if($this->LinkType == 'External') {
			if($this->ExternalURL) {
				return $this->ExternalURL;
			}
		} 
		else if($this->LinkType == 'Internal') {
			$linkTo = $this->LinkToID ? DataObject::get_by_id("SiteTree", $this->LinkToID) : null;
			if($linkTo) return $linkTo->Link();
		}
		else {
			$download = $this->DownloadID ? $this->Download() : false;
			if($download) return $download->Link();
		}
	}
	
	public function LinkTypeNice() {
		switch($this->LinkType) {
			case 'Internal':
				return _t('RedirectorPage.REDIRECTTOPAGE', "A page on your website");
			case 'External':
				return _t('RedirectorPage.REDIRECTTOEXTERNAL', "Another website");
			default:
				return 'Datei Download';
		}
	}
	
	public function onBeforeWrite() {
		parent::onBeforeWrite();

		// Prefix the URL with "http://" if no prefix is found
		if($this->ExternalURL && (strpos($this->ExternalURL, '://') === false)) {
			$this->ExternalURL = 'http://' . $this->ExternalURL;
		}
	}
	
	// PERMISSIONS
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		return Permission::check('VIEW_MICROSITELINKITEM');
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		return Permission::check('EDIT_MICROSITELINKITEM');
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_MICROSITELINKITEM');
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_MICROSITELINKITEM');
	}

	/**
	 * Provide permission
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_MICROSITELINKITEM' => array(
				'name' => 'Kann Microsite Links betrachten',
				'category' => 'Microsite',
				'sort' => 250
			),
			'EDIT_MICROSITELINKITEM' => array(
				'name' => 'Kann Microsite Links bearbeiten',
				'category' => 'Microsite',
				'sort' => 251
			),
			'CREATE_MICROSITELINKITEM' => array(
				'name' => 'Kann Microsite Links erstellen',
				'category' => 'Microsite',
				'sort' => 252
			),
			'DELETE_MICROSITELINKITEM' => array(
				'name' => 'Kann Microsite Links löschen',
				'category' => 'Microsite',
				'sort' => 253
			)
		);
	}
}

