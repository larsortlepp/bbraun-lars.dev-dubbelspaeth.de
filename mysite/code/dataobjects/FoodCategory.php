<?php

/**
 * Food Category
 *
 * FoodCategories are RestaurantPage-specific.
 * Dishes are grouped under the FoodCategories.
 * Every Dish can be assigned to one FoodCategory.
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * enable RestaurantPage support for this class:
 *
 *   FoodCategory:
 *     extensions:
 *       - MyDataObjectRegardRestaurantPageExtension
 */
class FoodCategory extends DataObject implements PermissionProvider {

	private static $db = array(
		'Title' => 'Varchar',
		'Title_en_US' => 'Varchar',
		'Infotext' => 'Text',
		'Infotext_en_US' => 'Text',
		'Color' => 'Varchar(7)',
		'IconUnicodeCode' => 'Varchar',
		'IsMain' => 'Boolean',
		'SortOrder' => 'Int'
	);
	private static $has_one = array(
		'Image' => 'Image',
		'RestaurantPage' => 'RestaurantPage'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields() {
		$f = new FieldList();

		$f->push(new HeaderField('FoodCategoryHeader', 'Essenskategorie'));

		$f->push(new CheckboxField('IsMain', 'Ist Haupt-Essenskategorie'));
		$f->push(new TextField('Title', 'Titel'));
		if ($this->RestaurantPage()->Lang_en_US) {
			$f->push(new TextField('Title_en_US', 'Titel englisch'));
		}
		$f->push(new TextareaField('Infotext', 'Zusatztext (optional)'));
		if ($this->RestaurantPage()->Lang_en_US) {
			$f->push(new TextareaField('Infotext_en_US', 'Zusatztext englisch (optional)'));
		}

		$f->push(new TextField('Color', 'Farbe (HEX-Code: #990000)'));

		$f->push($uploadField = new CustomUploadField('Image', 'Icon Grafik'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default FoodCategory folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderFoodCategoryPath(RestaurantPage::getNearestRestaurantPageID()));

		$f->push(TextField::create('IconUnicodeCode', 'Icon-Font Unicode Code')->setDescription('z.B. 76'));

		return $f;
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_' . $locale}) ? $this->{'Title_' . $locale} : $this->Title;
	}

	/**
	 * Return Infotext in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Infotext_en_US"
	 * If no translation for the language exists, the default field "Infotext" is returned
	 *
	 * @return	String
	 */
	public function Infotext() {
		$locale = i18n::get_locale();
		return ($this->{'Infotext_' . $locale}) ? $this->{'Infotext_' . $locale} : $this->Infotext;
	}

	/**
	 * Returns the Color as Array with RGB values
	 *
	 * $rgb = array(
	 * 		'r' => 122,
	 * 		'g' => 5,
	 * 		'b'	=> 230
	 * )
	 *
	 * @param $defaultHexColor String Default color if no color is set in DB (e.g. 'ffffff')
	 * @return Array
	 */
	public function ColorRGB($defaultHexColor = 'ffffff') {
		return $this->Color ? $this->hex2rgb($this->Color) : $this->hex2rgb($defaultHexColor);
	}

	/**
	 * Make $this->Color lighter by percent
	 * @param type $percent
	 */
	public function ColorLighterRGB($percent) {
		$rgb = $this->ColorRGB();
		$colorHSL = $this->rgb2hsl($rgb['r'], $rgb['g'], $rgb['b']);

		$newS = ($colorHSL[1] - ($percent / 100) >= 0) ? ($colorHSL[1] - ($percent / 100)) : 0;
		$newL = ($colorHSL[2] + ($percent / 100) <= 1) ? ($colorHSL[2] + ($percent / 100)) : 1;

		$rgbLighter = $this->hsl2rgb($colorHSL[0], $newS, $newL);

		return array(
			'r' => $rgbLighter[0],
			'g' => $rgbLighter[1],
			'b' => $rgbLighter[2]
		);
	}

	/**
	 * Make $this->Color lighter or darker by percent
	 * @param float $percent 0.5 will lighten colour by 50%, -0.5 will darken colour by -50%
	 * @return String HEX Color e.g. #ffee44
	 */
	public function ColorBrightnessHEX($percent) {
		// Work out if hash given
		$hash = '';
		$hex = $this->Color;
		if (stristr($hex, '#')) {
			$hex = str_replace('#', '', $hex);
			$hash = '#';
		}
		/// HEX TO RGB
		$rgb = array(hexdec(substr($hex, 0, 2)), hexdec(substr($hex, 2, 2)), hexdec(substr($hex, 4, 2)));
		//// CALCULATE
		for ($i = 0; $i < 3; $i++) {
			// See if brighter or darker
			if ($percent > 0) {
				// Lighter
				$rgb[$i] = round($rgb[$i] * (1- $percent)) + round(255 * $percent);
			} else {
				// Darker
				$positivePercent = $percent - ($percent * 2);
				$rgb[$i] = round($rgb[$i] * (1 - $positivePercent)) + round(0 * $positivePercent);
			}
			// In case rounding up causes us to go to 256
			if ($rgb[$i] > 255) {
				$rgb[$i] = 255;
			}
		}
		//// RBG to Hex
		$hex = '';
		for ($i = 0; $i < 3; $i++) {
			// Convert the decimal digit to hex
			$hexDigit = dechex($rgb[$i]);
			// Add a leading zero if necessary
			if (strlen($hexDigit) == 1) {
				$hexDigit = "0" . $hexDigit;
			}
			// Append to the hex string
			$hex .= $hexDigit;
		}
		return $hash . $hex;
	}

	/**
	 * Return Color as Array with RGB Values
	 *
	 * $rgb = array(
	 * 		'r' => 122,
	 * 		'g' => 5,
	 * 		'b'	=> 230
	 * )
	 *
	 * @param String $color The HEX color code with or without leading '#' e.g. '#ffffff' or 'ffffff'
	 *
	 * @return array
	 */
	public static function hex2rgb($color) {
		if ($color[0] == '#')
			$color = substr($color, 1);

		if (strlen($color) == 6)
			list($r, $g, $b) = array($color[0] . $color[1],
				$color[2] . $color[3],
				$color[4] . $color[5]);
		elseif (strlen($color) == 3)
			list($r, $g, $b) = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
		else
			return false;

		$r = hexdec($r);
		$g = hexdec($g);
		$b = hexdec($b);

		return array(
			'r' => $r,
			'g' => $g,
			'b' => $b
		);
	}

	/**
	 * Converts RGB to HSL Colors
	 *
	 * @param type $r
	 * @param type $g
	 * @param type $b
	 * @return type
	 */
	public static function rgb2hsl($r, $g, $b) {
		$var_R = ($r / 255);
		$var_G = ($g / 255);
		$var_B = ($b / 255);

		$var_Min = min($var_R, $var_G, $var_B);
		$var_Max = max($var_R, $var_G, $var_B);
		$del_Max = $var_Max - $var_Min;

		$v = $var_Max;

		if ($del_Max == 0) {
			$h = 0;
			$s = 0;
		} else {
			$s = $del_Max / $var_Max;

			$del_R = ( ( ( $var_Max - $var_R ) / 6 ) + ( $del_Max / 2 ) ) / $del_Max;
			$del_G = ( ( ( $var_Max - $var_G ) / 6 ) + ( $del_Max / 2 ) ) / $del_Max;
			$del_B = ( ( ( $var_Max - $var_B ) / 6 ) + ( $del_Max / 2 ) ) / $del_Max;

			if ($var_R == $var_Max)
				$h = $del_B - $del_G;
			else if ($var_G == $var_Max)
				$h = ( 1 / 3 ) + $del_R - $del_B;
			else if ($var_B == $var_Max)
				$h = ( 2 / 3 ) + $del_G - $del_R;

			if ($h < 0)
				$h++;
			if ($h > 1)
				$h--;
		}

		return array($h, $s, $v);
	}

	/**
	 * Converts HSL to RGB Colors
	 *
	 * @param type $h
	 * @param type $s
	 * @param type $v
	 * @return type
	 */
	public static function hsl2rgb($h, $s, $v) {
		if ($s == 0) {
			$r = $g = $B = $v * 255;
		} else {
			$var_H = $h * 6;
			$var_i = floor($var_H);
			$var_1 = $v * ( 1 - $s );
			$var_2 = $v * ( 1 - $s * ( $var_H - $var_i ) );
			$var_3 = $v * ( 1 - $s * (1 - ( $var_H - $var_i ) ) );

			if ($var_i == 0) {
				$var_R = $v;
				$var_G = $var_3;
				$var_B = $var_1;
			} else if ($var_i == 1) {
				$var_R = $var_2;
				$var_G = $v;
				$var_B = $var_1;
			} else if ($var_i == 2) {
				$var_R = $var_1;
				$var_G = $v;
				$var_B = $var_3;
			} else if ($var_i == 3) {
				$var_R = $var_1;
				$var_G = $var_2;
				$var_B = $v;
			} else if ($var_i == 4) {
				$var_R = $var_3;
				$var_G = $var_1;
				$var_B = $v;
			} else {
				$var_R = $v;
				$var_G = $var_1;
				$var_B = $var_2;
			}

			$r = $var_R * 255;
			$g = $var_G * 255;
			$B = $var_B * 255;
		}
		return array($r, $g, $B);
	}

	/**
	 * Function returns all Dishes for the given date.
	 * This fucntion can be called in combination with DailyMenuPage->FoodCategories(), which returns all FoodCategories and sets the date of the calling DailyMenuPage to them.
	 * So calling $this->Date will return the Date of the DailyMenuPage.
	 *
	 * This is a necessary combination for use in templates to render Menus by Date -> FoodCategory -> Dishes
	 * whereas the order in DB is Date -> Dishes -> FoodCategory
	 *
	 * Example use in PrintTemplatePage to render a manu for the current day with Date -> FoodCategory -> Dishes:
	 *
	 * <% with MenuContainer %>
	 * 		<% loop DailyMenuPagesByDate %>
	 * 			Menu for: $Date
	 * 			<% loop FoodCategories %>
	 * 				Food Category: $Title
	 * 				<% loop DishesForDateAndCategory %>
	 * 					Dish: $Description
	 * 				<% end_loop %>
	 * 			<% end_loop %>
	 * 		<% end_loop %>
	 * <% end_with %>
	 *
	 * @param String $sort			Field to sort the Dishes by. (default: 'Description')
	 * @param String $direction		Direction to sort Dishes. (default: 'ASC')
	 * @param String $template		Template variable that is set on Dish (DisplayTemplatePage, MobileTemplatePage, PrintTemplatePage)
	 *
	 * @return ArrayList $dishes
	 */
	public function DishesForDateAndCategory($sort = 'SortOrder', $direction = 'ASC', $template = null) {
		$currentDate = $this->Date; // calls the Date variable from DailyMenuPage
		$dishes = new ArrayList();
		$dishesAdded = 0;

		// get DailyMenuPage of current date
		$DailyMenuPage = ($this->DailyMenuPageID) ? DailyMenuPage::get()->byID($this->DailyMenuPageID) : DailyMenuPage::get()->filter("Date", $currentDate)->First();

		if ($DailyMenuPage) {

			// get all Dishes for the requested MenuPage
			$allDishes = $DailyMenuPage->Dishes();

			if ($allDishes) {
				foreach ($allDishes as $dish) {
					// only add dishes of the requested FoodCategory and the requested Template
					if (
							$dish->FoodCategoryID == $this->ID &&
							(
							$template == null || ($dish->{'ShowOn' . $template} == 1)
							)
					) {
						$dish->Date = $currentDate;
						$dish->IsCurrentDate = $dish->Date == date('Y-m-d');

						// get the IDs of all Dishes of the current DailyMenuPage for the given FoodCategoryID
						$sqlQuery = new SQLQuery();
						$sqlQuery->selectField(
							'SortOrder'
						);
						$sqlQuery->setFrom(array(
							'DailyMenuPage_Dishes'
						));
						$sqlQuery->setWhere(array(
							'DailyMenuPageID = ' . $DailyMenuPage->ID,
							'DishID = ' . $dish->ID
						));
						// execute and return a Query-object
						$result = $sqlQuery->execute();
						$firstResult = $result->first();
						// add many_many SortOrder
						$dish->SortOrder = $firstResult['SortOrder'];
						$dish->Date = $currentDate;

						$dishes->push($dish);
						$dishesAdded++;
					}
				}
				$dishes = $dishes->sort($sort, $direction);
			}
		}

		return $dishesAdded >= 1 ? $dishes : false;
	}

	public function DishesForDateAndCategoryAndDisplayTemplatePage() {
		return $this->DishesForDateAndCategory('SortOrder', 'ASC', 'DisplayTemplatePage');
	}

	public function DishesForDateAndCategoryAndPrintTemplatePage() {
		return $this->DishesForDateAndCategory('SortOrder', 'ASC', 'PrintTemplatePage');
	}

	public function DishesForDateAndCategoryAndMobileTemplatePage() {
		return $this->DishesForDateAndCategory('SortOrder', 'ASC', 'MobileTemplatePage');
	}

	/**
	 * Returns the DailyMenuPages for the given date range (set on internal Variables)
	 *
	 * For use of Dishes that are sorted by:
	 * FoodCategory->DailyMenuPage->Dish
	 *
	 * Can be used by calling TemplatePage->FoodCategories(), FoodCategory->DailyMenuPagesForDateRange(), DailyMenuPage->DishesForCategory()
	 * Useful for creation of data tables with FoodCategories as row, and DailyMenuPages as columns.
	 *
	 * @param MenuContainer $menuContainerID	The MenuContainer object's ID to fetch the DailyMenuPages from
	 * @param Boolean $allDays		If true all days between StartDate and EndDate are returned, even if no DailyMenuPage exists for that day. (default: true)
	 * @param Boolean $onlyWeekDays	If true only the week days are returned ($allDays has to be true as well) (default: true)
	 * @return ArrayList
	 */
	public function DailyMenuPagesForDateRange($menuContainerID, $allDays = true, $onlyWeekDays = true) {
		$startdate = $this->StartDate;
		$enddate = $this->EndDate;

		$dailyMenuPages = new ArrayList();

		// get DailyMenuPage for given Date Range
		$menuContainer = MenuContainer::get()->byId($menuContainerID);
		$tmpDailyMenuPages = $menuContainer->getDailyMenuPagesByDate($startdate, $enddate);

		if ($tmpDailyMenuPages) {

			// generate ArrayList with one DailyMenuPage for each single day (even if no DailyMenuPage exists for that date)
			if ($allDays) {
				// generate Array with dates from startdate to enddate
				$allDates = DateUtil::singleDatesForDateRange($startdate, $enddate, $onlyWeekDays);

				// create
				foreach ($allDates as $weekdate) {
					if ($dailyMenuPage = $tmpDailyMenuPages->find('Date', $weekdate)) {
					} else {
						$dailyMenuPage = new DailyMenuPage();
						$dailyMenuPage->Date = $weekdate;
						$dailyMenuPage->DateFormat = strftime('%d.%m.%Y', strtotime($weekdate));
					}

					// Add ID of current FoodCategory to DailyMenuPage
					$dailyMenuPage->FoodCategoryID = $this->ID;
					$dailyMenuPages->push($dailyMenuPage);
					$dailyMenuPage->FoodCategory = FoodCategory::get()->byID($this->ID);
				}
			}
			// only return the existing DailyMenuPages
			else {
				foreach ($tmpDailyMenuPages as $dailyMenuPage) {
					// Add ID of current FoodCategory to DailyMenuPage
					$dailyMenuPage->FoodCategoryID = $this->ID;
					$dailyMenuPages->push($dailyMenuPage);
					$dailyMenuPage->FoodCategory = FoodCategory::get()->byID($this->ID);
				}
			}
		}

		return $dailyMenuPages->Count() >= 1 ? $dailyMenuPages : false;
	}

	/**
	 * Add next highest SortOrder when creating new FoodCategory
	 * If no SortOrder exists yet: Set "1" as default SortOrder
	 */
	public function onBeforeWrite() {
		parent::onBeforeWrite();
		// Get current RestaurantPage that we are on
		$adminController = Controller::curr();
		$url_params = $adminController->getURLParams();
		// only run if we are not duplicating an item
		// otherwise we would overwrite an existing SortOrder value
		if($url_params['Action'] != 'duplicate' && method_exists($adminController, 'currentPage')) {
			$restaurantPage = $adminController->currentPage();			
			if(!$this->ID && $restaurantPage) {
				$sortOrder = 1; // set initial SortOrder
				$maxSortOrder = DB::query('SELECT MAX(SortOrder) FROM "'.$this->owner->ClassName.'" WHERE RestaurantPageID = '.$restaurantPage->ID)->value();
				$this->SortOrder = ($maxSortOrder >= 1) ? $maxSortOrder+1 : $sortOrder;
			}
		}
	}

	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_FOODCATEGORY') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_FOODCATEGORY') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_FOODCATEGORY') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_FOODCATEGORY') ? true : false;
		return $canView;
	}

	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_FOODCATEGORY' => array(
				'name' => 'Kann Essenskategorien betrachten',
				'category' => 'Essenskategorien',
				'sort' => 10
			),
			'EDIT_FOODCATEGORY' => array(
				'name' => 'Kann Essenskategorien bearbeiten',
				'category' => 'Essenskategorien',
				'sort' => 20
			),
			'CREATE_FOODCATEGORY' => array(
				'name' => 'Kann Essenskategorien erstellen',
				'category' => 'Essenskategorien',
				'sort' => 30
			),
			'DELETE_FOODCATEGORY' => array(
				'name' => 'Kann Essenskategorien löschen',
				'category' => 'Essenskategorien',
				'sort' => 40
			)
		);
	}

}

class FoodCategory_Controller extends ContentController {

	public function init() {
		parent::init();
	}

}
