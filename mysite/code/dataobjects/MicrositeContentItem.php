<?php

class MicrositeContentItem extends DataObject implements PermissionProvider {
	
	private static $db = array(
		'Title' => 'Varchar(255)',
		'Title_en_US' => 'Varchar(255)',
		'Content' => 'HTMLText',
		'Content_en_US' => 'HTMLText',
		'Background' => "Enum('Color,Grey,None','Color')",
		'ImagePosition' => "Enum('Left,Right','Left')",
		'LinkTitle' => 'Varchar(255)',
		'LinkTitle_en_US' => 'Varchar(255)',
		"LinkType" => "Enum('Internal,External','Internal')",
		"ExternalURL" => "Varchar(2083)", // 2083 is the maximum length of a URL in Internet Explorer.
		'Sort' => 'Int'
	);
	
	private static $has_one = array(
		'MicrositePage' => 'MicrositePage',
		'MicrositeHomePage' => 'MicrositeHomePage',
		"LinkTo" => "SiteTree",
		'Image' => 'Image'
	);
	
	private static $defaults = array(
		'Background' => 'Color',
		'ImagePosition' => 'Left',
		'LinkType' => 'Internal'
	);
	
	private static $field_labels = array(
		'Image.CMSThumbnail' => 'Bild',
		'Title' => 'Überschrift',
		'ContentGridField' => 'Inhalt',
		'LinkTypeGridField' => 'Link',
		'BackgroundNice' => 'Hintergrund'
	);
	
	private static $summary_fields = array(
		'Image.CMSThumbnail',
		'Title',
		'ContentGridField',
		'LinkTypeGridField',
		'BackgroundNice'
	);
	
	public function getCMSFields() {
		
		$restaurantPage = RestaurantPage::getNearestRestaurantPage();
		$showLang_en_US = ($restaurantPage) ? $restaurantPage->Lang_en_US : false;
		
		$micrositeContainer = MicrositeContainer::get()
			->filterAny(array(
				'ParentID' => $restaurantPage->ID
			))
			->limit(1)
			->first();
		
		// create basic tab set with 'Root' tab
		// this can be extended by new tabs and be modified with 
		// $f->addFieldToTab('Root', Field::create(...));
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldToTab('Root.Main', Textfield::create('Title', 'Überschrift'));
		if($showLang_en_US) $f->addFieldToTab('Root.Main', Textfield::create('Title_en_US', 'Überschrift (englisch)'));
		
		$f->addFieldToTab('Root.Main', OptionsetField::create(
			'Background', 
			'Hintergrund Farbe', 
			array(
				'Color' => 'farbig',
				'Grey' => 'grau',
				'None' => 'keine'
			),
			'Color'
		));
		
		$f->addFieldToTab('Root.Main', CustomUploadField::create(	
				"Image", 
				"Bild"
			)
			->setFolderName(SiteConfig::current_site_config()->DefaultFolderMicrositePath($restaurantPage->ID))
		);
		$f->addFieldToTab('Root.Main', OptionsetField::create(
			'ImagePosition', 
			'Bild Ausrichtung', 
			array(
				'Left' => 'links',
				'Right' => 'rechts'
			),
			'Left'
		));
		
		$f->addFieldToTab('Root.Main', HtmlEditorField::create('Content', 'Inhalt'));
		if($showLang_en_US) $f->addFieldToTab('Root.Main', HtmlEditorField::create('Content_en_US', 'Inhalt (englisch)'));
		
		
		$f->addFieldToTab('Root.Main', Textfield::create('LinkTitle', 'Button Text'));
		if($showLang_en_US) $f->addFieldToTab('Root.Main', Textfield::create('LinkTitle_en_US', 'Button Text (englisch)'));
		$f->addFieldToTab('Root.Main', OptionsetField::create(
			"LinkType", 
			'Button Typ',
			array(
				"Internal" => _t('RedirectorPage.REDIRECTTOPAGE', "A page on your website"),
				"External" => _t('RedirectorPage.REDIRECTTOEXTERNAL', "Another website")
			)
		));
		
		// Limit SiteTree dropdown to pages of current Microsite
		$f->addFieldToTab('Root.Main', TreeDropdownField::create(	
				"LinkToID", 
				_t('RedirectorPage.YOURPAGE', "Page on your website"), 
				"SiteTree"
			)
			->setTreeBaseID($micrositeContainer->ID)
		);
		
		$f->addFieldToTab('Root.Main', TextField::create(
			"ExternalURL", 
			_t('RedirectorPage.OTHERURL', "Other website URL")
		));
		
		return $f;
		
	}
	
	public function Link() {
		if($this->LinkType == 'External') {
			if($this->ExternalURL) {
				return $this->ExternalURL;
			}
		} 
		else {
			$linkTo = $this->LinkToID ? DataObject::get_by_id("SiteTree", $this->LinkToID) : null;
			if($linkTo) return $linkTo->Link();
		}
	}
	
	public function LinkTypeGridField() {
		if($this->Link()) {
			return DBField::create_field('HTMLText', $this->LinkTitle.'<br /><em>('.$this->Link().')</em>');
		}
		else {
			return 'keiner';
		}
	}
	
	public function ContentGridField() {
		return DBField::create_field('HTMLText', $this->Content)->ContextSummary(200);
	}
	
	public function BackgroundNice() {
		switch($this->Background) {
			case 'Color':
				return 'farbig';
			case 'Grey':
				return 'grau';
			default:
				return 'keine';
		}
	}
	
	public function onBeforeWrite() {
		parent::onBeforeWrite();

		// Prefix the URL with "http://" if no prefix is found
		if($this->ExternalURL && (strpos($this->ExternalURL, '://') === false)) {
			$this->ExternalURL = 'http://' . $this->ExternalURL;
		}
	}
	
	// PERMISSIONS
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		return Permission::check('VIEW_MICROSITECONTENTITEM');
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		return Permission::check('EDIT_MICROSITECONTENTITEM');
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return Permission::check('CREATE_MICROSITECONTENTITEM');
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('DELETE_MICROSITECONTENTITEM');
	}

	/**
	 * Provide permission
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_MICROSITECONTENTITEM' => array(
				'name' => 'Kann Microsite Inhaltsblöcke betrachten',
				'category' => 'Microsite',
				'sort' => 260
			),
			'EDIT_MICROSITECONTENTITEM' => array(
				'name' => 'Kann Microsite Inhaltsblöcke bearbeiten',
				'category' => 'Microsite',
				'sort' => 261
			),
			'CREATE_MICROSITECONTENTITEM' => array(
				'name' => 'Kann Microsite Inhaltsblöcke erstellen',
				'category' => 'Microsite',
				'sort' => 262
			),
			'DELETE_MICROSITECONTENTITEM' => array(
				'name' => 'Kann Microsite Inhaltsblöcke löschen',
				'category' => 'Microsite',
				'sort' => 263
			)
		);
	}
}

