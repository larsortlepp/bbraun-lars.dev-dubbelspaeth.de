<?php

/**
 * GlobalAdditive
 *
 * Globally available List of food additives.
 * Additives can be added to Dishes.
 *
 */
class GlobalAdditive extends DataObject {
	private static $db = array(
		'Number' => 'Varchar(255)',
		'Title' => 'Varchar(255)',
		'Title_en_US' => 'Varchar(255)',
		'SortOrder' => 'Int'
	);

	private static $belongs_many_many = array(
		'Dishes' => 'Dish'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$f = new FieldList();

		$f->push(new HeaderField('AdditiveHeader', 'Zusatzstoff (global)'));
		$f->push(new TextField('Number', 'Nummer'));
		$f->push(new TextField('Title', 'Bezeichnung'));
		$f->push(new TextField('Title_en_US', 'Bezeichnung (englisch)'));
		$f->push(new HeaderField('EmptyHeader', '&nbsp;'));

		return $f;
	}

	/*
	 * Checks if the user has the permission to delete GlobalAdditive records
	 *
	 * @return  Boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('ADMIN');
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}
}
