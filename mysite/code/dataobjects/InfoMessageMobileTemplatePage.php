<?php

/**
 * Message that is unique to a Subsite and can be assigned to a MobileTemplatePage
 */
class InfoMessageMobileTemplatePage extends InfoMessage implements PermissionProvider {
	
	private static $has_one = array (
		'InfoMessageTemplateMobileTemplatePage' => 'InfoMessageTemplateMobileTemplatePage',
		'MobileTemplatePage' => 'MobileTemplatePage'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		if($this->InfoMessageTemplateMobileTemplatePageID) {
			$infoMessage = $this->InfoMessageTemplateMobileTemplatePage();
			$f->addFieldToTab('Root.Main', LiteralField::create('InfoMessageTemplateInfo', '<p class="message notice">Erstellt durch News-Template '.$infoMessage->Title.' (ID: '.$infoMessage->ID.') am '.$this->obj('Created')->FormatFromSettings().'</p>'), 'PublishGroup');
		}
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGEMOBILETEMPLATEPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGEMOBILETEMPLATEPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGEMOBILETEMPLATEPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGEMOBILETEMPLATEPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE' => array(
				'name' => 'Kann News für Web-App betrachten',
				'category' => 'News',
				'sort' => 110
			),
			'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE' => array(
				'name' => 'Kann News für Web-App bearbeiten',
				'category' => 'News',
				'sort' => 120
			),
			'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE' => array(
				'name' => 'Kann News für Web-App erstellen',
				'category' => 'News',
				'sort' => 130
			),
			'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE' => array(
				'name' => 'Kann News für Web-App löschen',
				'category' => 'News',
				'sort' => 140
			)
		);
	}
}
