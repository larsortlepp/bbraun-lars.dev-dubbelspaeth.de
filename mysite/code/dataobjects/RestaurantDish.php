<?php

/**
 * RestaurantDish
 *
 * This Class only exists in the Database and is managed via "Dish" DataObject.
 * RestaurantDish is only available per RestaurantPage.
 * It has additional fields that are unique to every restaurant.
 * All the other data is used from the subsite's Dish - and is accessible to all RestaurantPages
 *
 * Add the following block of code to /mysite/_config/config.yml to
 * enable Subsite support for this class:
 *
 *   RestaurantDish:
 *     extensions:
 *       - MyDataObjectRegardRestaurantPageExtension
 */
class RestaurantDish extends DataObject {

	private static $db = array(
		'Price' => 'Float',
		'PriceExtern' => 'Float',
		'PriceSmall' => 'Float',
		'PriceExternSmall' => 'Float',
		'ShowOnDisplayTemplatePage' => 'Boolean(1)',
		'ShowOnPrintTemplatePage' => 'Boolean(1)',
		'ShowOnMobileTemplatePage' => 'Boolean(1)'
	);

	private static $has_one = array(
		'FoodCategory' => 'FoodCategory',
		'Dish' => 'Dish', # n-1 connection to a dish

		# Relate to up to five side dishes
		'SideDish1' => 'Dish',
		'SideDish2' => 'Dish',
		'SideDish3' => 'Dish',
		'SideDish4' => 'Dish',
		'SideDish5' => 'Dish',
	);
	
	public static $defaults = array(
		'ShowOnDisplayTemplatePage' => 1,
		'ShowOnPrintTemplatePage' => 1,
		'ShowOnMobileTemplatePage' => 1
	);
}
