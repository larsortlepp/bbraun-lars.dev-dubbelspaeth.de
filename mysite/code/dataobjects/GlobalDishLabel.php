<?php

/**
 * GlobalDishLabel
 *
 * A Label that can be added to a Dish.
 * E.g. "Vegetarian" or "Special Offer"
 */
class GlobalDishLabel extends DataObject {

	private static $db = array (
		'Title' => 'Varchar',
		'Title_en_US' => 'Varchar',
		'IconUnicodeCode' => 'Varchar',
		'Color' => 'Varchar(7)',
		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	private static $belongs_many_many = array(
		'Dishes' => 'Dish'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$f = new FieldList();

		$f->push(new HeaderField('DishLabelHeader', 'Kennzeichnung (global)'));

		$f->push(new TextField('Title', 'Titel'));
		$f->push(new TextField('Title_en_US', 'Titel (englisch)'));
		$f->push(new TextField('Color', 'Farbe (HEX-Code #990000'));
		$f->push(new TextField('IconUnicodeCode', 'Icon-Font Unicode Code (76 oder )'));
		$f->push($uploadField = new CustomUploadField('Image', 'Icon Grafik'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default DishLabel folder
		$uploadField->setFolderName(MySiteConfigExtension::$defaults['DefaultFolderDishLabel']);

		return $f;
	}

	/*
	 * Checks if the user has the permission to delete GlobalDishLabel records
	 *
	 * @return  Boolean
	 */
	public function canDelete($member = null) {
		return Permission::check('ADMIN');
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}

	/**
	 * return Thumbnail of Image
	 *
	 * @return Image
	 */
	public function getImage_CMSThumbnail() {
		return ($Image = $this->Image()) ? $Image->CMSThumbnail() : false;
	}

	/**
	 * Returns the Color as Array with RGB values
	 *
	 * $rgb = array(
	 *		'r' => 122,
	 *		'g' => 5,
	 *		'b'	=> 230
	 * )
	 *
	 * @param $defaultHexColor String Default color if no color is set in DB (e.g. 'ffffff')
	 * @return Array
	 */
	public function ColorRGB($defaultHexColor = 'ffffff') {
		return $this->Color ? FoodCategory::hex2rgb($this->Color) : FoodCategory::hex2rgb($defaultHexColor);
	}

	/**
	 * returns the unicode character for the IconUnicodeCode
	 * e.g. 76 => 'v'
	 *
	 * @return String
	 */
	public function getIconUnicodeCharacter() {
		if(!$this->IconUnicodeCode) return false;
		return $this->unicode2utf8(hexdec($this->IconUnicodeCode));
	}

	public function unicode2utf8($c) {
		$output = "";

		if ($c < 0x80) {
			return chr($c);
		} else if ($c < 0x800) {
			return chr(0xc0 | ($c >> 6)) . chr(0x80 | ($c & 0x3f));
		} else if ($c < 0x10000) {
			return chr(0xe0 | ($c >> 12)) . chr(0x80 | (($c >> 6) & 0x3f)) . chr(0x80 | ($c & 0x3f));
		} else if ($c < 0x200000) {
			return chr(0xf0 | ($c >> 18)) . chr(0x80 | (($c >> 12) & 0x3f)) . chr(0x80 | (($c >> 6) & 0x3f)) . chr(0x80 | ($c & 0x3f));
		}
		return false;
	}
}
