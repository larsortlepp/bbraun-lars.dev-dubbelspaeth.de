<?php

/**
 * Message that is unique to a Subsite and can be assigned to a MicrositeNewsPage
 */
class InfoMessageMicrositeNewsPage extends InfoMessage implements PermissionProvider {
	
	private static $has_one = array (
		'InfoMessageTemplateMicrositeNewsPage' => 'InfoMessageTemplateMicrositeNewsPage',
		'MicrositeNewsPage' => 'MicrositeNewsPage'
	);
	
	public function getCMSFields() {
		$f = parent::getCMSFields();
		if($this->InfoMessageTemplateMicrositeNewsPageID) {
			$infoMessage = $this->InfoMessageTemplateMicrositeNewsPage();
			$f->addFieldToTab('Root.Main', LiteralField::create('InfoMessageTemplateInfo', '<p class="message notice">Erstellt durch News-Template '.$infoMessage->Title.' (ID: '.$infoMessage->ID.') am '.$this->obj('Created')->FormatFromSettings().'</p>'), 'PublishGroup');
		}
		
		// remove fullscreen image field
		$f->removeFieldFromTab('Root.Main', 'ImageOnlyGroup');
		
		return $f;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = (Permission::check('VIEW_INFOMESSAGEMICROSITENEWSPAGE')) ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canEdit = (Permission::check('EDIT_INFOMESSAGEMICROSITENEWSPAGE')) ? true : false;
		return $canEdit;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canCreate = (Permission::check('CREATE_INFOMESSAGEMICROSITENEWSPAGE')) ? true : false;
		return $canCreate;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canDelete = (Permission::check('DELETE_INFOMESSAGEMICROSITENEWSPAGE')) ? true : false;
		return $canDelete;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_INFOMESSAGEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News für Micsosite News Seite betrachten',
				'category' => 'News',
				'sort' => 310
			),
			'EDIT_INFOMESSAGEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News für Micsosite News Seite bearbeiten',
				'category' => 'News',
				'sort' => 320
			),
			'CREATE_INFOMESSAGEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News für Micsosite News Seite erstellen',
				'category' => 'News',
				'sort' => 330
			),
			'DELETE_INFOMESSAGEMICROSITENEWSPAGE' => array(
				'name' => 'Kann News für Micsosite News Seite löschen',
				'category' => 'News',
				'sort' => 340
			)
		);
	}
}
