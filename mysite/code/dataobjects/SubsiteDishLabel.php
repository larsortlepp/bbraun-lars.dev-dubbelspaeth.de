<?php

/**
 * SubsiteDishLabel
 *
 * A subsite-specific Label that can be added to a Dish.
 * E.g. "Vegetarian" or "Special Offer"
 */
class SubsiteDishLabel extends DataObject implements PermissionProvider {
	private static $db = array (
		'Title' => 'Varchar',
		'Title_en_US' => 'Varchar',
		'IconUnicodeCode' => 'Varchar',
		'Color' => 'Varchar(7)',
		'SortOrder' => 'Int'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	private static $belongs_many_many = array(
		'Dishes' => 'Dish'
	);

	private static $default_sort = 'SortOrder';

	public function getCMSFields()
	{
		$f = new FieldList();

		$f->push(new HeaderField('DishLabelHeader', 'Kennzeichnung (mandantenspezifisch)'));

		$f->push(new TextField('Title', 'Titel'));
		$f->push(new TextField('Title_en_US', 'Titel (englisch)'));
		$f->push(new TextField('Color', 'Farbe (HEX-Code #990000'));
		$f->push(new TextField('IconUnicodeCode', 'Icon-Font Unicode Code (76 oder )'));
		$f->push($uploadField = new CustomUploadField('Image', 'Icon Grafik'));
		$uploadField->setAllowedExtensions(array('jpg', 'gif', 'png'));
		// set folder to default DishLabel folder
		$uploadField->setFolderName(SiteConfig::current_site_config()->DefaultFolderDishLabelPath());

		return $f;
	}

	/**
	 * Return Title in current language (if it exists)
	 * Checks if a field with current locale exists. E.g. locale = "en_US" => checks for field "Title_en_US"
	 * If no translation for the language exists, the default field "Title" is returned
	 *
	 * @return	String
	 */
	public function Title() {
		$locale = i18n::get_locale();
		return ($this->{'Title_'.$locale}) ? $this->{'Title_'.$locale} : $this->Title;
	}

	/**
	 * return Thumbnail of Image
	 *
	 * @return Image
	 */
	public function getImage_CMSThumbnail() {
		return ($Image = $this->Image()) ? $Image->CMSThumbnail() : false;
	}

	/**
	 * Returns the Color as Array with RGB values
	 *
	 * $rgb = array(
	 *		'r' => 122,
	 *		'g' => 5,
	 *		'b'	=> 230
	 * )
	 *
	 * @param $defaultHexColor String Default color if no color is set in DB (e.g. 'ffffff')
	 * @return Array
	 */
	public function ColorRGB($defaultHexColor = 'ffffff') {
		return $this->Color ? FoodCategory::hex2rgb($this->Color) : FoodCategory::hex2rgb($defaultHexColor);
	}

	/**
	 * returns the unicode character for the IconUnicodeCode
	 * e.g. 76 => 'v'
	 *
	 * @return String
	 */
	public function getIconUnicodeCharacter() {
		if(!$this->IconUnicodeCode) return false;
		return $this->unicode2utf8(hexdec($this->IconUnicodeCode));
	}

	public function unicode2utf8($c) {
		$output = "";

		if ($c < 0x80) {
			return chr($c);
		} else if ($c < 0x800) {
			return chr(0xc0 | ($c >> 6)) . chr(0x80 | ($c & 0x3f));
		} else if ($c < 0x10000) {
			return chr(0xe0 | ($c >> 12)) . chr(0x80 | (($c >> 6) & 0x3f)) . chr(0x80 | ($c & 0x3f));
		} else if ($c < 0x200000) {
			return chr(0xf0 | ($c >> 18)) . chr(0x80 | (($c >> 12) & 0x3f)) . chr(0x80 | (($c >> 6) & 0x3f)) . chr(0x80 | ($c & 0x3f));
		}
		return false;
	}
	
	/**
	 * Implements custom canView permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canView($member = null) {
		$canView = Permission::check('VIEW_SUBSITEDISHLABEL') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canEdit permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$canView = Permission::check('EDIT_SUBSITEDISHLABEL') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canCreate permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		$canView = Permission::check('CREATE_SUBSITEDISHLABEL') ? true : false;
		return $canView;
	}

	/**
	 * Implements custom canDelete permissions
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		$canView = Permission::check('DELETE_SUBSITEDISHLABEL') ? true : false;
		return $canView;
	}
	
	/**
	 * Provide permission for Dish
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_SUBSITEDISHLABEL' => array(
				'name' => 'Kann Kennzeichnungen Mandant betrachten',
				'category' => 'Kennzeichnungen Mandant',
				'sort' => 10
			),
			'EDIT_SUBSITEDISHLABEL' => array(
				'name' => 'Kann Kennzeichnungen Mandant bearbeiten',
				'category' => 'Kennzeichnungen Mandant',
				'sort' => 20
			),
			'CREATE_SUBSITEDISHLABEL' => array(
				'name' => 'Kann Kennzeichnungen Mandant erstellen',
				'category' => 'Kennzeichnungen Mandant',
				'sort' => 30
			),
			'DELETE_SUBSITEDISHLABEL' => array(
				'name' => 'Kann Kennzeichnungen Mandant löschen',
				'category' => 'Kennzeichnungen Mandant',
				'sort' => 40
			)
		);
	}
}
