<?php
global $project;
$project = 'mysite';

// setup database credentials
global $databaseConfig;
$databaseConfig = array(
	"type" => 'MySQLDatabase',
	"server" => 'localhost',
	"username" => 'bbraun_lars_user',
	"password" => 'DSdevSecurityObscurity#1',
	"database" => 'bbraun_lars',
	"path" => '',
);

// i18n: Set timezone (required for PHP 5.3.X)
date_default_timezone_set('Europe/Berlin');

// i18n: Set the site locale
i18n::set_locale('de_DE');

// i18n: Set locale for date / time functions
DateUtil::setlocale('de_DE');

// Performance: Set new memory limit
ini_set('memory_limit', '256M');

// Set base URL to '/' to avoid index.php being added to base tag
// @link http://www.silverstripe.org/community/forums/installing-silverstripe/show/11824
Director::setBaseURL('/');

// Error handling: Enable PHP error reporting
error_reporting(E_ERROR);
ini_set('display_errors', TRUE);

// In dev mode, log errors to Apache's error log, otherwise send eMail
if (Director::isDev()) {
	SS_Log::add_writer(new SS_LogFileWriter('/var/log/apache2/error_log'), SS_Log::NOTICE, '<=');
} else {
	SS_Log::add_writer(new SS_LogEmailWriter('dubbel@dubbelspaeth.de'), SS_Log::ERR);
}

// Dev mode: Force cache to flush on page load (prevents needing ?flush=1 on the end of a URL)
if (Director::isDev()) {
	SSViewer::flush_template_cache();
}

// URLs: Force redirect to www.
#Director::forceWWW();

// TinyMCE customization (not yet possible through YAML)

// Set base URL for all relative URLs
HtmlEditorConfig::get('cms')->setOption('document_base_url', Director::absoluteBaseURL());

// Custom CSS file to use in TinyMCE's editor area
HtmlEditorConfig::get('cms')->setOption('content_css', 'themes/default-theme/css/editor.css');

// Custom CSS class to use in TinyMCE's editor area
HtmlEditorConfig::get('cms')->setOption('body_class', 'typography');

// List of formats made available via the "Formats" dropdown field
HtmlEditorConfig::get('cms')->setOption('theme_advanced_blockformats', 'p,h2,h3,h4');

// Strip all class attributes when pasting, not just MS Office's
HtmlEditorConfig::get('cms')->setOption('paste_strip_class_attributes', 'all');

// Remove all <span> elements when pasting
HtmlEditorConfig::get('cms')->setOption('paste_remove_spans', 'true');

// HTML elements and attributes that will also be accepted as valid HTML in the saved text
HtmlEditorConfig::get('cms')->setOption('extended_valid_elements', 'video[width|height|src|type|poster|controls|preload|autoplay|loop],source[src|type],iframe[width|height|src|frameborder|allowfullscreen]');

// Enable paste and searchreplace plugins
HtmlEditorConfig::get('cms')->enablePlugins(array('paste', 'searchreplace'));

// Set buttons
HtmlEditorConfig::get('cms')->setButtonsForLine(1, 'formatselect','styleselect','separator','bold','italic','separator','bullist','numlist','separator','cut','copy','paste','pastetext','separator','undo','redo','separator','search','replace','separator','media','ssimage','sslink','unlink','anchor','separator','charmap','separator','code');
HtmlEditorConfig::get('cms')->setButtonsForLine(2);
HtmlEditorConfig::get('cms')->setButtonsForLine(3);


Security::setDefaultAdmin('admin','admin');