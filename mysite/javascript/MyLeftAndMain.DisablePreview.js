/**
 * Disable preview mode in CMS by default (setting it to 'content' mode)
 * Remove the preview-mode-selector button that toggles preview mode
 * 
 * See doc for details:
 * @link https://docs.silverstripe.org/en/3.1/developer_guides/customising_the_admin_interface/preview/
 */
jQuery( function() {
	// set default mode to 'content' and disable preview
	jQuery( '.cms-preview' ).entwine( '.ss.preview' ).changeMode( 'content' );
	jQuery( '.cms-preview' ).entwine( '.ss.preview' ).disablePreview();
	// remove preview-mode-selector
	// :BUG: will only be removed on first page load
	// when loading a link from sitestree (ajax) the button will be visible on subsequent pages
	jQuery('.preview-mode-selector').remove();
} );
