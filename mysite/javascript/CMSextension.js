/**
 * Extension functions to the CMS
 */

/**
 * JQUERY INITIALISATION WHEN DOCUMENT READY
 *
 * @author Sebastian Dubbel
 *
 * @requires
 *		jQuery library v 1.4.2+: http://jquery.com
 * @encoding UTF-8
 *
 */

// load dish-image-sync function via ajax in background when button is clicked
jQuery('a.ajax-dish-image-sync').live('click', function(e) {
	e.preventDefault();
	jQuery(this).addClass('loading');
	jQuery.ajax({
		url: jQuery(this).attr('href'),
		success:function(data) {
			// remove loading class
			jQuery('a.ajax-dish-image-sync').removeClass('loading');			
		},
		error:function(data) {
			// remove loading class
			jQuery('a.ajax-dish-image-sync').removeClass('loading');		
		}
	});
});

// AJAX execution of single processtask with success / error message
jQuery('a.ajax-executeSingleProcessTask').live('click', function(e) {
	e.preventDefault();
	if(jQuery(this).hasClass('loading')) return false;
	// trigger loading animation
	jQuery(this).addClass('loading');
	// get url of current panel
	var url = jQuery.path.parseUrl(document.location.href).hrefNoSearch;
	jQuery.ajax({
		url: jQuery(this).attr('href'),
		dataType: 'text',
		success:function(data) {
			// remove error mesages
			jQuery('.executeSingleProcessTask-error, .executeSingleProcessTask-success').remove();
			
			// redirect to parent page
			if(data == 'success') {
				jQuery('<p class="message good executeSingleProcessTask-success">Aufgabe erfolgreich ausgeführt</p>').insertAfter(jQuery('.ajax-executeSingleProcessTask').first());
				// show success message and reload current panel (after short timeout)
				setTimeout(function() { jQuery('.cms-container').entwine('ss').loadPanel(url, null, null, true); }, 2000);
			}
			// show error message
			else {
				jQuery('<p class="message error executeSingleProcessTask-error">Beim Ausführen der Aufgabe ist ein Fehler aufgetreten.</p>').insertAfter(jQuery('.ajax-executeSingleProcessTask').first());
			}
			jQuery('a.ajax-executeSingleProcessTask').removeClass('loading');
		},
		error:function(data) {
			// remove error mesages
			jQuery('.executeSingleProcessTask-error, .executeSingleProcessTask-success').remove();
			
			jQuery('<p class="message error executeSingleProcessTask-error">Beim Ausführen der Aufgabe ist ein Fehler aufgetreten.<br />Womöglich liegt ein Berechtigungsproblem vor. Bitte prüfen Sie ob Sie unter der richtigen URL angemeldet sind und ausreichende Beutzerrechte haben.</p>').insertAfter(jQuery('.ajax-executeSingleProcessTask').first());
			jQuery('a.ajax-executeSingleProcessTask').removeClass('loading');
		}
	});
});