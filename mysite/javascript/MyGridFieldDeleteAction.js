(function($){
	$.entwine('ss', function($) {
		$('.ss-gridfield .col-buttons .action.gridfield-button-delete-disabled, .cms-edit-form .Actions button.action.action-delete-disabled').entwine({
			onclick: function(e){
				alert("Dieses Element kann nicht gelöscht werden, da es noch im System verwendet wird!");
				e.preventDefault();
			}
		});
	});
}(jQuery));
