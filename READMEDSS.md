# DIGITAL SIGNAGE SYSTEM (DSS) V 3.1.1

## Einführung

Framework für die Verwaltung von Gerichten in Speiseplänen.
Ausgabe der Speisepläne erfolgt über HTML (Mobile, Desktop, Monitor) und PDF.
Mit Hilfe des "Subsites" Moduls können mehrere Restaurants über das gleiche CMS und verschiedene URLs verwendet werden. Alle Restaurants können auf die gleiche Datenbank mit Gerichten zugreifen, jedoch separate Essenskategorien, Labels, etc. verwenden.

## Kontakt

 * Sebastian Dubbel <dubbel@dubbelspaeth.de>

## Systemvoraussetzungen

 * CMS: [SilverStripe 3.1.9](http://www.silverstripe.org/assets/releases/SilverStripe-cms-v3.1.9.tar.gz)
   * Patch: [#3778: Fix CheckboxSetField's getOptions() $items creation](https://github.com/silverstripe/silverstripe-framework/pull/3778)
 * Modul: [GridFieldExtensions](https://github.com/silverstripe-australia/silverstripe-gridfieldextensions) (Git rev ad55ceda)
 * Modul: [GridField-BetterButtons](https://github.com/unclecheese/silverstripe-gridfield-betterbuttons) version 1.2.6
 * Modul: [Subsites](https://github.com/silverstripe/silverstripe-subsites) (branch 1.0/Git rev 498d6e06)
 * Modul: [Version-Truncator](https://github.com/axllent/silverstripe-version-truncator) (Git rev dcce9ee1)

## Installation

### Einrichten neue Subdomain / Datenbank auf Signage Server

Für jedes neue Restaurant wird eine separate Subdomain angelegt. Auch wenn mehrere Restaurants auf das gleiche DSS zugreifen.
So ist gewährleistet dass jedes Restaurant seine eigenen Gerichte und Speisepläne konfigurieren kann.

Der Name der Subdomain setzt sich auf folgenden Bestandteilen zusammen:
BETREIBER-STADT-RESTAURANT.signage-server.de (z.B. 'db-b-osa.signage-server.de')

 * BETREIBER (z.B. 'db' = Deutsche Bank) = Kürzel des Betreibers / Kunden
 * STADT (z.B. 'b' = Berlin) = KFZ-Kennzeichen Kürzel der Stadt in der sich das Restaurant befindet
 * RESTAURANT (z.B. 'osa' = Bezeichnung des Restaurant Standorts) = Kürzel für die Restaurant Bezeichnung

#### Einrichten Subdomain

 * Im Plesk Panel unter "Websites & Domains" Button "Neue Subdomain anlegen" klicken
 * Unter "Domainname" Name der neuen Subdomain eingeben (z.B. 'db-b-osa.signage-server.de')
 * Unter "Dokumentstamm" den Namen der Subdomain mit voangstelltem 'httpdocs/' angeben (z.B. 'httpdocs/db-b-osa.signage-server.de')
 * Im Anschluss auf den neu angelegten Domainnamen klicken > Ansicht "Hostingeinstellungen" wird angezeigt
 * "SSI Unterstützung" anhaken
 * "PHP-Unterstützung (ausführen als FastCGI-Applikation)" auswählen
 * Im Reiter "PHP Einstellungen":
  * "memory_limit": "Standard" aus Dropdown wählen
  * "error_reporting": "Eigenen Wert" "E_ERROR" eintragen
  * "display_errors" auf "on" setzen

#### Einrichten Datenbank

 * Im Plesk Panel unter "Websites & Domains" Button "Datenbanken" klicken
 * Auf Button "Neue Datenbank hinzufügen" klicken
 * Unter "Datenbankname" den Namen der Subdomain mit nachfolgendem '_signage-server' angeben (z.B. 'db-b-osa_signage-server')
 * Im Anschluss auf "Neuen Datenbankbenutzer hinzufügen" klicken
 * Datenbankbenutzername und sicheres Passwort vergeben
 * In der Übersicht der Datenbankbenutzer, den neuen Benutzer anhaken und Button "Als Standard für DB Webadmin" anklicken


### Installation DSS unter Subdomain

 * Neue Silverstripe Installation per FTP auf Zielserver kopieren.
 * Silverstripe auf Zielserver installieren (Aufruf von z.B. http://db-b-osa.signage-server.de/install.php)
 * Nach Installation in Administration anmelden
 * Anschließend alle Modulverzeichnisse sowie Verzeichnis "mysite" und "themes" per FTP auf Server kopieren
 * Erforderliche Module installieren (/dev/build?flush=1)
 * Subsites Modul einrichten: URL des Zielservers als "Primary Domain" anlegen
  * Bei DB Import v. vorhandenen Seiten: In Tabelle "SubsiteDomain" das Feld "Domain" auf die verwendete URL abändern
 * /dev/build?flush=1 aufrufen
 * In SiteConfig Restaurant Name, Restaurant Untertitel, Restaurant Logo und Theke angeben
 * Startseite als "RestaurantPage" anlegen
 * In RestaurantPage die benötigen Essenskategorien und Labels für Gerichte anlegen
 * Unterhalb Startseite eine Seite als "Menu Container" anlegen
 * Unterhalb von "MenuContainer" Speisepläne als "DailyMenuPage" anlegen
 * In "DailyMenuPage" die gewünschten Gerichte eingeben
 * Unterhalb von "RestaurantPage" die gewünschten Druckausgaben als "PrintTemplatePage" anlegen, entspr. Controller zur Generierung des PDFs Programmieren
 * Unterhalb von "RestaurantPage" die gewünschten Bildschirmausgaben als "DisplayTemplatePage" anlegen. Den "Template Name" eintragen.
 * Anlegen der zugehörigen DisplayTemplates (z.B. "Screen_Counter_Daily"):
  * /themes/default-theme/templates/Screen_Counter_Daily.ss (Standard Template zur Bildschirmausgabe, mit HTML Header, Javascript, etc.)
  * /themes/default-theme/templates/Screen_Counter_Daily_ajax.ss (Template für AJAX Reload der Inhalte)
  * /themes/default-theme/templates/Includes/Screen_Counter_Daily.ss (HTML Inhalt der in "Screen_Counter_Daily.ss" und "Screen_Counter_Daily_ajax.ss" eingeladen wird)

### Anlegen Subdomain für MobileTemplatePage (Web-App)

Zur Ausgabe einer MobileTemplatePage sollte eine separate Subdomain angelegt werden.
Mit der folgenden Konfirguration wird beim Aufruf der neuen Subdomain (z.B. 'db-b-osa-mobile.signage-server.de') direkt die MobileTemplatePage angezeigt (ohne zusätzliche URLs oder Parameter).

 * Im Plesk Panel unter "Websites & Domains" Button "Neue Subdomain anlegen" klicken
 * Unter "Domainname" Name der neuen Subdomain eingeben (z.B. 'db-b-osa-mobile.signage-server.de')
 * Unter "Dokumentstamm" das Verzeichnis der bereits installierten DSS Domain angeben (z.B. 'httpdocs/db-b-osa.signage-server.de')
 * Im Anschluss auf den neu angelegten Domainnamen klicken > Ansicht "Hostingeinstellungen" wird angezeigt
 * Hier die unter obigem Punkt "Einrichten Subdomain" genannten Einstellungen für Apcahe PHP Server einstellen
 * Im DSS unter "Subsites" bei der zugehörigen Subsite die Subdomain für die MobiltTemplatePage ergänzen
 * Im DSS die MobileTemplatePage aufrufen und beim Reiter "Verhalten" unter "Domäne(n)" ebenfalls die neue Subdomain eingetragen.