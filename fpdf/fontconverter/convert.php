<?php

	/**
	 * Converts TTF Fonts to fpdf fonts
	 * 
	 * 1. Put TTF fonts in the 'fpdf/fontconverter/' folder
	 * 2. Setup the 'fpdf/fontconverter/convert.php' file:
	 *    2a. require 'makefont/makefont.php'
	 *    2b. Call Fontconverter: MakeFont('myfontname.ttf') - without encoding attribute (cp1252 handles utf-8)
	 * 3. Call the file from your webbrowser: e.g. mydomain.com/fpdf/fontconverter/convert.php
	 * 4. The resulting fpdf font will be created in 'fpdf/fontconverter'
	 * 5. Copy the fontfiles *.php and *.z to 'fpdf/font/'
	 * 
	 */
	require('makefont/makefont.php');
	
	// MakeFont($fontfile, $enc='cp1252', $embed=true)
	MakeFont('icon-dishlabels.ttf');
?>