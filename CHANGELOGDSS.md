# Version 3.1.1

## Overview

This update contains minor bugfixes and a task that can be run if we
update from version 3.0.0alpha to 3.1.0+.

Note: When running task `UpdateTask_3_1_0` we need to modify some functions,
based on the current installation.
E.g. assignment of ClassNames, ProcessTaskConfig Schedule or 
ProcessTaskConfig BlacklistConfigIDs.

## Changelog

### Features and Enhancements

 * [67f6dad](https://bitbucket.org/dubbelspaeth/digital-signage/commits/67f6dad794c568f099e58d7e546c097494c275bc) Added task `UpdateTask_3_1_0` that performs database and structural updates from version 3.0.0alpha to 3.1.0

### Bugfixes

 * [6c4e810](https://bitbucket.org/dubbelspaeth/digital-signage/commits/6c4e810080efe9b0445b26751d5fd2de692cde76) InfoMessage.php: Added check in onBeforeWrite() to only add sort value based on current TemplatePage if we have a current page
 * [0a61fa5](https://bitbucket.org/dubbelspaeth/digital-signage/commits/0a61fa53eed44994da2e4af2306968e66eec9b8d) ImportVisitorCounterTask.php: Adapted import time to variable `v` of new API export format

# Version 3.1.0

## Overview

This document describes significant changes and bugfixes to digital-signage system and related modules.

Main features are explained below. For a complete list see the Features section

### New feature: Module `dss-dishes-per-restaurant`

Dish per Restaurant module Adds RestaurantPageID to Dish, so that it can be exclusively assigned to a single RestaurantPage (instead of being globally accessible by all RestaurantPages of the current mandator).
Comes with additional permissions that can be assigned to user groups. So we can define which group can create/view/edit/delete edit global accessible Dishes and which group can only create/delete Dishes exclusively assigned to their RestaurantPage.

A global Dish can be cloned into a Dish exclusive to the current RestaurantPage. Cloning a global Dish will also exchange the global Dish with the cloned RestaurantPage Dish in the current DailyMenuPage and add a OrigDishID to have a reference to the original Dish it was cloned from.

### New feature: Module `processtask`

Process Task module allow to create and manage tasks in Silverstripe via the CMS.

Available tasks:
 * ImportMenuXml: Task importing data to DailyMenuPages and relative RestaurantPage
 * ImportRestaurant: Task importing data to selected RestaurantPage without adding it to DailyMenuPage
 * ImportVisitorCounter: Task importing data from external visitor counter servives and assign the data to DisplayTemplatePages and MobileTemplatePages
 * ExportMenuXml: Task exporting Data from DailyMenuPages to xml file 
 * ExportDisplayMenuXml: Task exporting data from DisplayTemplatePage to xml file
 * ExportMenuHtml: Task exporting selected FoodCategories for a given date range to an HTML file (including neccessary CSS, JS, etc.) storing it as .zip file on the server.

Every task has a Schedule and a timeout and it is meant to be run according to different methods:
* Single task execution with specific button in CMS (Immidiate execution witout timeout or schedule)
* All active tasks exection with cronjobs (Scheduled execution respecting specific timeout)

There is a meta task `BackgroundTask` that can be used to run all *ProcessTasks
with one single call. This aloows us do create one single cronjob, that triggers
all our *ProcessTasks.

Setup a cronjob as following (example that runs every 15 minutes):

`* /15 * * * * /your/site/folder/sake BackgroundTask`

### New feature: Module `dss-print-manager`

Adds a "Druck Manager" tab to the CMS where we can save multiple print templates with custom FoodCategory or Headline, Dishes and FootNote that can then be printed as custom PDFs.
With the print manager we are independet of the DailyMenu page and can assign any Dish from the database of the selected RestaurantPage - independet of DailyMenuPage and FoodCategory.

### New feature: `PrintHtmlTemplatePage`

The `PrintHtmlTemplatePage` is meant to replace the existing `PrintTemplatePage`.
It uses regular Silverstripe HTML templates to redner the prit view, and 
renders them as PDF using the `dompdf` library.

The advantage is, that we can now seperate the code and design:
We can create print templates based on themes, instead of hardcoding theme-specific
layouts as PHP code in the controller (as done in PrintTemplatPage).

### New feature: `ControllerRestaurantPageRelation`

ControllerRestaurantPageRelation is an interface for Controllers that are not related to SiteTree but need a relation to a RestaurantPage. We can assign them a RestaurantPageID dynamically (e.g. from request parameters) and also return the RestaurantPage dynamically for external functions.

## Changelog

### API Changes

 * [6f370a4](https://bitbucket.org/dubbelspaeth/digital-signage/commits/6f370a44598e22b8a18cef6e0436d3cd947729f0) Removed phpMySQLAutoBackup in favour of using a server side backup solution (such as Plesk backup)
 * [b3646ef](https://bitbucket.org/dubbelspaeth/digital-signage/commits/b3646efa013c0f5976082bcec37b3e0f8c791d23) Modifed XML structure of export tasks (compared to v 2.4): `ExportMenuXmlTask` and `ExportDisplayMenuXmlTask` now have `FoodCategories` nodes to group all FoodCategories of a day and `Dishes` nodes to group all Dishes of a FoodCategory

### Features and Enhancements

 * [6b1b7fb](https://bitbucket.org/dubbelspaeth/digital-signage/commits/6b1b7fb1d48b22c860696158a64ac1bcc2c4b09e) TemplatePage: Added functions to get data of DailyMenuPages in the future (e.g. the next day)
 * [f1214cd](https://bitbucket.org/dubbelspaeth/digital-signage/commits/f1214cd26fec43ffaa5919f74c4dc4211b07d77f) Moved database config from `mysite/tasks/databaseConfig.inc.php` to `mysite/_config.php`
 * [e90aa2f](https://bitbucket.org/dubbelspaeth/digital-signage/commits/e90aa2f9713216d7e8dbd48e9ed5af2bc5f2a985) Added new `CleanupTask` to remove old DailyMenupages - it replaces the old DSSTasks.php file
 * [19cd3c6](https://bitbucket.org/dubbelspaeth/digital-signage/commits/19cd3c6fd55dd869961e9d1d47776f530d4876bd) Added dss-print-manager module v 1.0.0: Adds a ModelAdmin for print templates with custom compositions of Dishes or manual title, infotext and footnote.
 * [cca33a6](https://bitbucket.org/dubbelspaeth/digital-signage/commits/cca33a6f34cb7a06c228f136a5684c6eef87d777) Added `PrintHtmlTemplatePage` with dompdf library
 * [6361a26](https://bitbucket.org/dubbelspaeth/digital-signage/commits/6361a263db426286be8bd9e3c7ce43a20eea7e56) Added module `dss-dishes-per-restaurant`
 * [622df01](https://bitbucket.org/dubbelspaeth/digital-signage/commits/622df01c3e90b1065d6b55fc1944d6b45887f460) Added feature ProcessTask
 * [e98e552](https://bitbucket.org/dubbelspaeth/digital-signage/commits/e98e5527f90ca21b054a817b6d101ab640565ede) Added interface `ControllerRestaurantPageRelation`: Interface that implements dynamic methods of getting a relation to a RestaurantPage for Controllers that are not related to SiteTree. 
 * [3a61a25](https://bitbucket.org/dubbelspaeth/digital-signage/commits/3a61a253e3ecf46242438f4cb0e600ca683ffa2b) Dish: Added `ImportID` field instead of using Extension

### Bugfixes

 * [446748b](https://bitbucket.org/dubbelspaeth/digital-signage/commits/446748bb5569a868e37dc50681243b5c3b4adcd0) Header.ss: Fixed HTML content of Header to use DSS 3.* code
 * [abaf5e6](https://bitbucket.org/dubbelspaeth/digital-signage/commits/abaf5e641ffbb317527b58e0ec428e9d9bd50717) Dish::getDishLabelImagesCMS(): Proper checking if image exists before calling getTag() function
 * [4e26a96](https://bitbucket.org/dubbelspaeth/digital-signage/commits/4e26a96766cf38aeba74c5a27f1f7ae3a18465f0) Infomessage: Fixed onBeforeWrite() to check if current page has method `InfoMessages` before accessing it in order to set an initial SortOrder value
 * [6371040](https://bitbucket.org/dubbelspaeth/digital-signage/commits/6371040f593d3526005a88a96904ac519961ee5c) FoodCategory: Do not set initial augmented SortOrder if FoodCategory is duplicated
 * [1827e45](https://bitbucket.org/dubbelspaeth/digital-signage/commits/1827e451444a3657607ef7f2768a10b4253cc707) RestaurantPage: Add info for searching import id if feature is enabled to string for DishesGridFieldAutoCompleterPlaceholderText
 * [6b07036](https://bitbucket.org/dubbelspaeth/digital-signage/commits/6b0703613bf55a1e7996f020fd29d65974db0789) PrintTemplatePage: Fixed permission checks which hide InfoMessage tab
 * [0bf3536](https://bitbucket.org/dubbelspaeth/digital-signage/commits/0bf353631a7684e7e0ae3cf6e20f667158203511) PrintTemplatePage: Fixed saving and permissions on WeeklyTableHeaderImage
 * [126540f](https://bitbucket.org/dubbelspaeth/digital-signage/commits/126540f28cc4605c7b5a9d0589158bf29e2e2880) DailyMenuPage: Fixed FoodCategories() function to return FoodCategories only once when running a SQLQuery
 * [f26f270](https://bitbucket.org/dubbelspaeth/digital-signage/commits/f26f270bec3b276e2fee7a5b38a6ea1e02e9f12a) SubsiteSpecialLabel: Fixed canCreate/View/Edit/Delete permission
 * [b6d011a](https://bitbucket.org/dubbelspaeth/digital-signage/commits/b6d011a3d9faea7a3cbbdb414ae2d73931b99d42) SubsiteDishLabel: Fixed canCreate/View/Edit/Delete permission
 * [eef4392](https://bitbucket.org/dubbelspaeth/digital-signage/commits/eef4392017764431cd40f5958d546c780df13997) SubsiteAllergen: Fixed canCreate/View/Edit/Delete permission
 * [00525b4](https://bitbucket.org/dubbelspaeth/digital-signage/commits/00525b4349e473cd9508a9f021439d5e7053c1e3) SubsiteAdditive: Fixed canCreate/View/Edit/Delete permission
 * [748af40](https://bitbucket.org/dubbelspaeth/digital-signage/commits/748af4079590e23479d2d878307d943b1bf80874) RecipeSubmissionItem: Fixed canCreate/View/Edit/Delete permission
 * [1d418f4](https://bitbucket.org/dubbelspaeth/digital-signage/commits/1d418f445bb0306c12dfe45e571ddc84e0ce30b6) FoodCategory: Fixed canCreate/View/Edit/Delete permission
 * [ba7f971](https://bitbucket.org/dubbelspaeth/digital-signage/commits/ba7f97163d25482462d3dbb765d7ccd73e5c1f0d) DisplayVideo: Fixed canCreate/View/Edit/Delete permission
 * [7a98474](https://bitbucket.org/dubbelspaeth/digital-signage/commits/7a9847476cb6fd9f113ac3fdbc59340a3353cd17) DisplayTemplateObject: Fixed canCreate/View/Edit/Delete permission
 * [f78a396](https://bitbucket.org/dubbelspaeth/digital-signage/commits/f78a396a95ef729ed67c8378cdb31cfcf4d7cc84) DishVotingItem: Fixed canCreate/View/Edit/Delete permission
 * [61e8131](https://bitbucket.org/dubbelspaeth/digital-signage/commits/61e81316c8ec32c1408ad82a5fdd77d8ca1d508c) DishRating: Fixed canCreate/View/Edit/Delete permission
 * [e17391c](https://bitbucket.org/dubbelspaeth/digital-signage/commits/e17391c5335ffdad8f872895e91e401f3218e9d1) Dish: Fixed canCreate/View/Edit/Delete permission Permissions only based on the permission setting
 * [3817e1f](https://bitbucket.org/dubbelspaeth/digital-signage/commits/3817e1f9a6d10c6ae6a990ad854c1e3b60954e89) Dish: Fixed AllSideDishes() to check if $sideDish exists before trying to get a value from it
 * [4cd894f](https://bitbucket.org/dubbelspaeth/digital-signage/commits/4cd894f32c27557af68185458f0637e8dbd214ea) RecipeSubmission: Added proper wording "Wunschgericht" for all occurrences 
