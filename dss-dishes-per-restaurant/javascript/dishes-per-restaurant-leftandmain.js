// import excel data and remove preview
jQuery('a.ajax-duplicateAsDishPerRestaurant').live('click', function(e) {
	e.preventDefault();
	if(jQuery(this).hasClass('loading')) return false;
	// trigger loading animation
	jQuery(this).addClass('loading');
	// set backlink url to variable to redirect there on success
	var backlink = jQuery(this).attr('data-backlink');
	jQuery.ajax({
		url: jQuery(this).attr('href'),
		dataType: 'text',
		success:function(data) {
			// remove error mesages
			jQuery('.duplicateAsDishPerRestaurant-error').remove();
			
			// redirect to parent page
			if(data == 'success') {
				jQuery('<p class="message good">Gericht erfolgreich kopiert</p>').insertAfter(jQuery('.ajax-duplicateAsDishPerRestaurant').first().parent());
				// go back to DailyMenuPage
				// :TODO: 
				// This currently breaks back button behaviour, because we remove the Edit Dish from history and replace it with the DailyMenuPage
				// Wen need to add 'forceReferrer' attribute (loadPanel(url, title, data, forceReload, forceReferer)) 
				// with url of copied Dish (we need to get the ID of the copeied Dish as AJAX response) e.g. admin/pages/edit/EditForm/field/Dishes/item/DISHID/edit
				jQuery('.cms-container').entwine('ss').loadPanel(backlink, null, null, true, backlink);
			}
			// show error message
			else {
				jQuery('<p class="message error duplicateAsDishPerRestaurant-error">Beim Kopieren des Gerichts ist ein Fehler aufgetreten.</p>').insertAfter(jQuery('.ajax-duplicateAsDishPerRestaurant').first().parent());
			}
			jQuery('a.ajax-duplicateAsDishPerRestaurant').removeClass('loading');
		},
		error:function(data) {
			// remove error mesages
			jQuery('.duplicateAsDishPerRestaurant-error').remove();
			
			jQuery('<p class="message error duplicateAsDishPerRestaurant-error">Beim Kopieren des Gerichts ist ein Fehler aufgetreten.<br />Womöglich liegt ein Berechtigungsproblem vor. Bitte prüfen Sie ob Sie unter der richtigen URL angemeldet sind und ausreichende Beutzerrechte haben.</p>').insertAfter(jQuery('.ajax-duplicateAsDishPerRestaurant').first().parent());
			jQuery('a.ajax-duplicateAsDishPerRestaurant').removeClass('loading');
		}
	});
});
