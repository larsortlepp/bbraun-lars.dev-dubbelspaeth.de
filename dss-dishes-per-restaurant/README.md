## DSS Module dishes-per-restaurant

## Version ##
 * Module Version: 1.0.1

## Requirements ##
 * DSS: 3.1.0
 * Silverstripe: 3.1

## Description ##
Adds RestaurantPageID to Dish, so that it can be exclusively assigned to a single RestaurantPage (instead of being globally accessible by all RestaurantPages of the current mandator).
Comes with additional permissions that can be assigned to user groups. So we can define which group can create/view/edit/delete edit global accessible Dishes and which group can only create/delete Dishes exclusively assigned to their RestaurantPage.

When creating a new Dish we can decide whether it is global or by RestaurantPage.
A global Dish can be cloned into a Dish exclusive to the current RestaurantPage. Cloning a global Dish will also exchange the global Dish with the cloned RestaurantPage Dish in the current DailyMenuPage and add a OrigDishID to have a reference to the original Dish it was cloned from.

## Changelogs

### 1.0.1

#### Bugfixes

 * [7b6d97b](https://bitbucket.org/dubbelspaeth/dss-dishes-per-restaurant/commits/7b6d97b430d5d9e08b562ccf462bcf41230a1803) DishesPerRestaurantDishExtension: Don´t duplicate relation DailyMenuPages, just set one relation to the current DailyMenuPage