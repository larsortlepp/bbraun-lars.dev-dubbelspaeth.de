<?php

/**
 * Adds RestaurantPageID to Dish
 * to be able to restrict a Dish (which is originally stored by Subsite) 
 * to a RestaurantPage
 *
 * Add the following block of code to /dss-dishes-per-restaurant/_config/config.yml to
 * activate this class:
 *
 *   Dish:
 *     extensions:
 *       - DishesPerRestaurantDishExtension
 * 
 * @package dss-dishes-per-restaurant
 * 
 */
class DishesPerRestaurantDishExtension extends DataExtension {

	static $has_one = array(
		'RestaurantPage' => 'RestaurantPage',
		'OrigDish' => 'Dish'
	);
	
	// set RestaurantPageID to null by default
	// RestaurantPageID = null means it is a subsite specific Dish
	static $defaults = array(
		'RestaurantPageID' => null
	);
	
	/**
	 * Array with title of $many_many or $belongs_many_many relations
	 * that should not be duplicated, when function duplicateAsDishPerRestaurant() is called.
	 * The given relations will be removed after Dish was duplicated.
	 * 
	 * @return array
	 */
	private static function duplicate_per_restaurant_many_many_exceptions() {
		return array(
			'DailyMenuPages', //exclude DailyMenuPages, we will set the relation to current page manually in duplicateAsDishPerRestaurant()
			'PrintManagerConfigs'
		);
	}

	public function updateCMSFields(FieldList $f) {

		$siteConfig = SiteConfig::current_site_config();
		$showLang_en_US = $this->owner->showLang_en_US();
		
		// If DishesPerRestaurant module is enabled
		if($siteConfig->EnableIndividualDishesPerRestaurant) {			
			
			// get restaurant page
			$restaurantPage = RestaurantPage::getNearestRestaurantPage();
			$restaurantString = 'Nur für dieses Restaurant ('.$restaurantPage->Title.')';
			$mandatorString = 'Für alle Restaurants von '.$siteConfig->Title;
			
			// If new Dish is added (ID does not exist yet):
			// add selection if Dish should be per Subsite or per Restaurant
			if(!$this->owner->ID) {
				// Add options for Dish depending on permissions
				$dishTypes = array();
				if(Permission::check('CREATE_SUBSITE_DISH_PER_RESTAURANT')) {
					$dishTypes['0'] = $mandatorString;
					$dishTypeDefault = '0';
				}
				if(Permission::check('CREATE_RESTAURANT_DISH_PER_RESTAURANT')) {
					$dishTypes[$restaurantPage->ID] = $restaurantString;
					$dishTypeDefault = $restaurantPage->ID;
				}
				$f->insertAfter(
					new OptionsetField(
						'RestaurantPageID',
						'Gericht Zuordnung',
						$dishTypes,
						$dishTypeDefault
					), 
					'DishHeader'
				);
			}
			// show message if Dish is per Subsite or per Restaurant
			else {
				
				// only show message if we are in SiteTree and can access a current page
				$page = Controller::curr()->currentPage();
				if($page && $page->exists()) {
					if($this->owner->RestaurantPageID == 0) {
						$dishType = '<strong>'.$mandatorString.'</strong>';
						if(!Permission::check('EDIT_SUBSITE_DISH_PER_RESTAURANT')) $dishType .= '<br /><em>Einige Felder sind wegen fehlender Berechtigung für Sie gesperrt.</em>';
						$dishType .= '<br /><br /><a href="'.Controller::curr()->currentPage()->AbsoluteSubsiteLink($this->owner->SubsiteID, 'duplicateasdishperrestaurant/'.$this->owner->ID).'" class="custom-button ajax-duplicateAsDishPerRestaurant icon-forward-3" data-backlink="admin/pages/edit/show/'.Controller::curr()->currentPage()->ID.'">Kopie für dieses Restaurant erstellen</a>';
					}
					else {
						$dishType = '<strong>'.$restaurantString.'</strong>';
					}
					$f->insertAfter(
						new LiteralField(
							'DishType',
							'<div class="message '.$this->owner->getDishTypeClassname().'">'.$dishType.'</div>'
						),
						'DishHeader'
					);
				}
			}
			
			// Augment basic permission restrictions for SubsiteDishes (every Dish that has RestaurantPageID == 0)
			// based on Dish permissions of current user
			// no need to change default permissions on DishesPerRestaurantDish (standard permissions apply)
			// only apply if Dish is already created (not if we create a new one - then $this->owner->ID will be null)
			if($this->owner->ID && $this->owner->RestaurantPageID == 0) {
				// restrict permissions of Dish variables (Subsite specific)
				// but keep RestaurantDish permissions untouched (as they apply per Restaurant)
		
				// can NOT view and NOT edit subsite dishes
				if(!Permission::check('VIEW_SUBSITE_DISH_PER_RESTAURANT') && !Permission::check('EDIT_SUBSITE_DISH_PER_RESTAURANT')) {
					
					// Description
					$f->replaceField('Description', new HiddenField('Description', 'Beschreibung', $this->owner->Description));
					if($showLang_en_US) $f->replaceField('Description_en_US', new HiddenField('Description_en_US', 'Beschreibung englisch', $this->owner->Description_en_US));
					
					// Image
					$f->replaceField('Image', new HiddenField('ImageID', 'Foto von Gericht', $this->owner->ImageID));
					
					// Nutritives
					$f->removeByName('Nutritives');
					foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {
						$f->replaceField($nutritiveField, new HiddenField($nutritiveField, Dish::$nutritivesConfig[$nutritiveField]['Title'].' ('.Dish::$nutritivesConfig[$nutritiveField]['Unit'].')', $this->owner->$nutritiveField));
					}
					
					// GlobalSpecialLabels
					$f->removeByName('GlobalSpecialLabels');
					
					// SubsiteSpecialLabels
					$f->removeByName('SubsiteSpecialLabels');
					
					// GlobalDishLabels
					$f->removeByName('GlobalDishLabels');
					
					// SubsiteDishLabels
					$f->removeByName('SubsiteDishLabels');
					
					// GlobalAdditives
					$f->removeByName('GlobalAdditives');
					
					// SubsiteAdditives
					$f->removeByName('SubsiteAdditives');
					
					// GlobalAllergens
					$f->removeByName('GlobalAllergens');
					
					// SubsiteAllergens
					$f->removeByName('SubsiteAllergens');
					
					// Infotext
					$f->replaceField('Infotext', new HiddenField('Infotext', 'Zusatztext (optional)', $this->owner->Infotext));
					if($showLang_en_US) $f->replaceField('Infotext_en_US', new HiddenField('Infotext_en_US', 'Zusatztext englisch (optional)', $this->owner->Infotext_en_US));
				}
				// can NOT edit but view subsite dishes
				else if(!Permission::check('EDIT_SUBSITE_DISH_PER_RESTAURANT') && Permission::check('VIEW_SUBSITE_DISH_PER_RESTAURANT')) {
					
					// Description
					$f->replaceField('Description', $f->fieldByName('Description')->performReadonlyTransformation());
					if($showLang_en_US) $f->replaceField('Description_en_US', $f->fieldByName('Description_en_US')->performReadonlyTransformation());
					
					// Image
					$f->replaceField('Image', $f->fieldByName('Image')->performReadonlyTransformation());
					
					// Nutritives
					foreach(Dish::$nutritivesConfig as $nutritiveField => $nutritiveConfig) {
						$f->replaceField($nutritiveField, $f->fieldByName($nutritiveField)->performReadonlyTransformation());
					}
					
					// GlobalSpecialLabels
					if($fieldgroup = $f->fieldByName('GlobalSpecialLabels')) $f->replaceField('GlobalSpecialLabels', $fieldgroup->performDisabledTransformation());
					
					// SubsiteSpecialLabels
					if($fieldgroup = $f->fieldByName('SubsiteSpecialLabels')) $f->replaceField('SubsiteSpecialLabels', $fieldgroup->performDisabledTransformation());
					
					// GlobalDishLabels
					if($fieldgroup = $f->fieldByName('GlobalDishLabels')) $f->replaceField('GlobalDishLabels', $fieldgroup->performDisabledTransformation());
					
					// SubsiteDishLabels
					if($fieldgroup = $f->fieldByName('SubsiteDishLabels')) $f->replaceField('SubsiteDishLabels', $fieldgroup->performDisabledTransformation());
					
					// GlobalAdditives
					if($fieldgroup = $f->fieldByName('GlobalAdditives')) $f->replaceField('GlobalAdditives', $fieldgroup->performDisabledTransformation());
					
					// SubsiteAdditives
					if($fieldgroup = $f->fieldByName('SubsiteAdditives')) $f->replaceField('SubsiteAdditives', $fieldgroup->performDisabledTransformation());
					
					// GlobalAllergens
					if($fieldgroup = $f->fieldByName('GlobalAllergens')) $f->replaceField('GlobalAllergens', $fieldgroup->performDisabledTransformation());
					
					// SubsiteAllergens
					if($fieldgroup = $f->fieldByName('SubsiteAllergens')) $f->replaceField('SubsiteAllergens', $fieldgroup->performDisabledTransformation());
					
					// Infotext
					$f->replaceField('Infotext', $f->fieldByName('Infotext')->performReadonlyTransformation());
					if($showLang_en_US) $f->replaceField('Infotext_en_US', $f->fieldByName('Infotext_en_US')->performReadonlyTransformation());
					
				}
			}			
		}
	}
	
	/**
	 * Modify query for SideDishes to only return global and Dishes assigned explicitely for this RestaurantPage
	 * @param DataList $query
	 */
	public function updateSideDishesQuery(&$query) {
		$query = $query->filter('Dish.RestaurantPageID', array(0, RestaurantPage::getNearestRestaurantPageID()));
	}
	
	/**
	 * Create array for dropdown of SideDish selection: 
	 * Add the DishTypeShortcode to the label to distinguish global from RestaurantOnly Dishes
	 * Mapping: "ID" -> "DishTypeShortCode Description"
	 * @param SS_Map $array
	 */
	public function updateSideDishesMap(&$array) {
		$query = $this->owner->SideDishesQuery();
		$array = array();
		// Add DishShort Code to Dish.Description
		foreach($query as $object) {
			$array[$object->ID] = $object->owner->getDishTypeShortCode().' '.$object->Description;
		}
	}
	
	/**
	 * Returns a classname for current Dish type
	 * if RestaurantPageID is 0: 'dish-type-subsite'
	 * else: 'dish-type-restaurant'
	 * 
	 * Can be used for rendering in CMS
	 * 
	 * @return String
	 */
	public function getDishTypeClassname() {
		return ($this->owner->RestaurantPageID == 0) ? 'dish-type-subsite' : 'dish-type-restaurant';
	}
	
	public function getDishTypeHTMLCode() {
		$obj = HTMLText::create();
		$obj->setValue('<div class="dish-type-block '.$this->owner->DishTypeClassname.'"></div>');
		return $obj;
	}
	
	public function getDishTypeShortCode() {
		return ($this->owner->RestaurantPageID == 0) ? '' : '[+] ';
	}
	
	
	/**
	 * Makes a duplicate of the current Dish and RestaurantDish
	 * and adds the current RestaurantPageID to the Dish,
	 * so that the Dish will be only accessible by the current Restaurant
	 * 
	 * @return Dish $duplicateDish The duplicate of the current Dish
	 */
	public function duplicateAsDishPerRestaurant() {
		// stop if dish is not a Subsite Dish
		if($this->owner->RestaurantPageID != 0) return false;
		
		$currRestaurantPageID = RestaurantPage::getNearestRestaurantPageID();
		
		// duplicate Dish
		$origDish = $this->owner;
		$duplicateDish = $this->owner->duplicate(); // do not write
		$duplicateDish->RestaurantPageID = $currRestaurantPageID;
		$duplicateDish->OrigDishID = $origDish->ID; // add reference to original Dish
		
		// remove many_many relations as given by duplicate_per_restaurant_many_many_exceptions()
		// Dish->duplicate() will duplicate all many_many and belongs_many_many relations
		// here we can remove all relations that we do not want to be duplicated
		if($many_many_exceptions = $this->duplicate_per_restaurant_many_many_exceptions()) {
			// Remove all other relations from classes other than the current class
			// to the duplicated dish
			if ($duplicateDish->many_many()) {
				foreach($duplicateDish->many_many() as $name => $type) {
					if (in_array($name,  $many_many_exceptions)) {
						//many_many include belongs_many_many
						$relations = $duplicateDish->$name();
						if ($relations) {
							if ($relations instanceOf RelationList) {   //many-to-something relation
								if ($relations->Count() > 0) {  //with more than one thing it is related to
									foreach($relations as $relation) {
										$duplicateDish->$name()->remove($relation);
									}
								}
							}
						}
					}
				}
			}
		}
		
		// Add Relation to current DailyMenuPage
		// Initially a DishPerRestaurant will only be related to the 
		// current DailyMenuPage where the duplicateAsDishPerRestaurant() function was called
		// it can then be added manually to all DailyMenuPages of the Restaurant
		$page = Controller::curr();
		if($page && $page->ID) {
			$dailyMenuPage = DailyMenuPage::get()->byID($page->ID);
			$duplicateDish->DailyMenuPages()->add($dailyMenuPage);
		}
		
		$duplicateDish->write();
		
		// duplicate or create new RestaurantDish
		$origRestaurantDish = RestaurantDish::get()->filter(array(
				'DishID' => $origDish->ID,
				'RestaurantPageID' => $currRestaurantPageID
			))->limit(1)->first();
		if($origRestaurantDish && $origRestaurantDish->exists()) {
			$duplicateRestaurantDish = $origRestaurantDish->duplicate();
		}
		else {
			$duplicateRestaurantDish = RestaurantDish::create();
			$duplicateRestaurantDish->RestaurantPageID = $currRestaurantPageID;
		}
		// connect Dish to RestaurantDish
		$duplicateRestaurantDish->DishID = $duplicateDish->ID;
		$duplicateRestaurantDish->write();
		
		return $duplicateDish;
	}
	
	public function canCreate($member = null) {
		// If DishesPerRestaurant module is enabled
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) {
			return (Permission::check('CREATE_SUBSITE_DISH_PER_RESTAURANT') || Permission::check('CREATE_RESTAURANT_DISH_PER_RESTAURANT')) ? true : false;
		}

	}
	
	public function canDelete($member = null) {
		
		// If DishesPerRestaurant module is enabled
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) {		
			if($this->owner->RestaurantPageID == 0 && Permission::check('DELETE_SUBSITE_DISH_PER_RESTAURANT')) {
				return true;
			}
			else if($this->owner->RestaurantPageID == RestaurantPage::getNearestRestaurantPageID() && Permission::check('DELETE_RESTAURANT_DISH_PER_RESTAURANT')) {
				return true;
			}
			else {
				return false;
			}
		}

	}
}

/**
 * Helper Class to add permissions to DishesPerRestaurantExtension
 * 
 * Needed as we can´t implement PermissionProvider on an Extension or DataExtension
 * This class only declares permissions and has no further functionality
 * 
 * @package dss-dishes-per-restaurant
 * @requires Dish.php
 *
 */
class DishesPerRestaurantDishExtensionPermissions extends DataObject implements PermissionProvider {
	
	/**
	 * Provide permissions for DishesPerRestaurantDishExtension
	 * @return type
	 */
	public function providePermissions(){
		return array(
			'VIEW_SUBSITE_DISH_PER_RESTAURANT' => array(
				'name' => 'Kann Gerichte für alle Restaurants betrachten',
				'category' => 'Modul "Individuelle Gerichte pro Restaurant"',
				'sort' => 10
			),
			'EDIT_SUBSITE_DISH_PER_RESTAURANT' => array(
				'name' => 'Kann Gerichte für alle Restaurants bearbeiten',
				'category' => 'Modul "Individuelle Gerichte pro Restaurant"',
				'sort' => 20
			),
			'CREATE_SUBSITE_DISH_PER_RESTAURANT' => array(
				'name' => 'Kann Gerichte für alle Restaurants erstellen',
				'category' => 'Modul "Individuelle Gerichte pro Restaurant"',
				'sort' => 30
			),
			'DELETE_SUBSITE_DISH_PER_RESTAURANT' => array(
				'name' => 'Kann Gerichte für alle Restaurants löschen',
				'category' => 'Modul "Individuelle Gerichte pro Restaurant"',
				'sort' => 40
			),
			'CREATE_RESTAURANT_DISH_PER_RESTAURANT' => array(
				'name' => 'Kann Gericht für eigenes Restaurant erstellen',
				'category' => 'Modul "Individuelle Gerichte pro Restaurant"',
				'sort' => 50
			),
			'DELETE_RESTAURANT_DISH_PER_RESTAURANT' => array(
				'name' => 'Kann Gericht für eigenes Restaurant löschen',
				'category' => 'Modul "Individuelle Gerichte pro Restaurant"',
				'sort' => 60
			)
		);
	}
}
