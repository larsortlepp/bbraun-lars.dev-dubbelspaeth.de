<?php

/**
 * Extend PrintManagerConfig for additional functionality to work with DishesPerRestaurant
 *
 * Add the following block of code to /dss-dishes-per-restaurant/_config/config.yml to
 * activate this class:
 *
 *   PrintManagerConfig:
 *     extensions:
 *       - DishesPerRestaurantPrintManagerConfigExtension
 * 
 * @package dss-dishes-per-restaurant
 * @requires dss-print-manager/code/dataobjects/PrintManagerConfig.php
 * 
 */
class DishesPerRestaurantPrintManagerConfigExtension extends DataExtension {
	
	/**
	 * Functions that modify 'Dishes' GridField behaviour
	 * (can be extended by 'updateDishesGridField...()' functions in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateDishesGridFieldAutoCompleterResultFormat(String &$string) {
	 *     $string = '$Description.RAW ($ID)';
	 * }
	 * 
	 */
	public function updateDishesGridFieldQuery(&$query) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) {
			$query = $query->filter(array('Dish.RestaurantPageID' => array(0, $this->owner->RestaurantPageID)));
		}
	}
	
	public function updateDishesGridFieldCols(&$cols) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) {
			$tmp_cols = $cols;
			// add new field that color marks if Subsite or Restaurant Dish at beginning of exisiting array
			$cols = array_merge(array('DishTypeHTMLCode' => ''), $tmp_cols);
		}
	}
	
	public function updateDishesGridFieldAutoCompleterSearchList(&$list) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) $list = $list->filter(array('Dish.RestaurantPageID' => array(0, $this->owner->RestaurantPageID)));
	}
	
	public function updateDishesGridFieldAutoCompleterResultFormat(&$string) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) $string = '{$DishTypeShortCode}'.$string;
	}
}
