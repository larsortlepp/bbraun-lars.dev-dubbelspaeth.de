<?php

/**
 * Extend DailyMenuPage for additional functionality to work with DishesPerRestaurant
 *
 * Add the following block of code to /dss-dishes-per-restaurant/_config/config.yml to
 * activate this class:
 *
 *   DailyMenuPage:
 *     extensions:
 *       - DishesPerRestaurantDailyMenuPageExtension
 * 
 * @package dss-dishes-per-restaurant
 * @requires DailyMenuPage.php
 * 
 */
class DishesPerRestaurantDailyMenuPageExtension extends DataExtension {
	
	/**
	 * Functions that modify 'Dishes' GridField behaviour
	 * (can be extended by 'updateDishesGridField...()' functions in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateDishesGridFieldAutoCompleterResultFormat(String &$string) {
	 *     $string = '$Description.RAW ($ID)';
	 * }
	 * 
	 */
	public function updateDishesGridFieldQuery(&$query) {
	}
	
	public function updateDishesGridFieldTitle(&$string) {
	}
	
	public function updateDishesGridFieldCols(&$cols) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) {
			$tmp_cols = $cols;
			// add new field that color marks if Subsite or Restaurant Dish at beginning of exisiting array
			$cols = array_merge(array('DishTypeHTMLCode' => ''), $tmp_cols);
		}
	}
	
	public function updateDishesGridFieldRowsPerPage(&$integer) {
	}
	
	public function updateDishesGridFieldAutoCompleterResultFormat(&$string) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) $string = '{$DishTypeShortCode} '.$string;
	}
	
	public function updateDishesGridFieldAutoCompleterPlaceholderText(&$string) {
	}
	
	public function updateDishesGridFieldAutoCompleterSearchList(&$list) {
		// Add filter to only return global Dishes or Dishes that are exclusively assigned to the current RestaurantPage
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) $list = $list->filter('Dish.RestaurantPageID', array(0, $this->owner->RestaurantPageID()));
	}
	
	public function updateDishesGridFieldAutoCompleterSearchCols(&$cols) {
	}
	
	public function updateDishesGridFieldAutoCompleterOnAfterAdd(&$func) {
	}
	
	public function updateDishesGridFieldAllowCustomSort(&$boolean) {
	}
	
	public function updateDishesGridFieldExtraSortCols(&$string) {
	}
	
	public function updateDishesGridFieldReorderColumnNumber(&$integer) {
		// place reorder handle after FoodCategory (FoddCategory SortOrder can´ be changed in GridField)
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) $integer = 2;
	}
}

class DishesPerRestaurantDailyMenuPage_ControllerExtension extends Extension {
	private static $allowed_actions = array (
		'duplicateasdishperrestaurant'
	);
	
	/**
	 * Duplicates the Dish for the given ID and adds the ID of the current
	 * RestaurantPage to it, making it a DishPerRestaurant
	 * (only accessible by the current Restaurant)
	 * 
	 * Call this by URL e.g.
	 * mydomain.com/RESTAURANTPAGE/MENUCONTAINER/DAILYMENUPAGE/duplicateasdishperrestaurant/dish-id
	 * 
	 * @param SS_HTTPRequest $request
	 * @return String $status 'success' or 'error'
	 */
	public function duplicateasdishperrestaurant(SS_HTTPRequest $request) {
		
		$status = 'error';
		
		if(
			!Member::logged_in_session_exists() || 
			!Permission::check('CMS_ACCESS_CMSMain') ||
			!SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant
		) {
			return $this->owner->httpError(403, 'Sie haben keine Berechtigung diese Seite aufzurufen');
		}
		
		// get given Dish
		$origDish = Dish::get()->filter('ID', $request->param('ID'))->limit(1)->first();
		if($origDish && $origDish->exists()) {
			$duplicateDish = $origDish->duplicateAsDishPerRestaurant();
			
			if($duplicateDish) {
				
				// get SortOrder of relation
				$sqlQuery = new SQLQuery();
				$sqlQuery->setFrom('DailyMenuPage_Dishes')->selectField('SortOrder')->addWhere('DailyMenuPageID = '.$this->owner->ID.' AND DishID = '.$origDish->ID)->setLimit(1);
				$result = $sqlQuery->execute();
				$resultRecord = $result->nextRecord();
				$origSortOrder = $resultRecord['SortOrder'];
				
				// delete original Dish from DailyMenuPage
				DB::query("
					DELETE FROM ".$this->owner->ClassName."_".$origDish->ClassName."es
					WHERE ".$this->owner->ClassName."ID = ".$this->owner->ID."
					AND ".$origDish->ClassName."ID = ".$origDish->ID."
				");
				
				// Relation between Dish and DailyMenuPage is automatically set
				// in Dish->onAfterWrite()
				// check if it was set correctly
				$manymanyExists = DB::query("
					SELECT COUNT(*) 
					FROM ".$this->owner->ClassName."_".$duplicateDish->ClassName."es
					WHERE ".$this->owner->ClassName."ID = ".$this->owner->ID."
					AND ".$duplicateDish->ClassName."ID = ".$duplicateDish->ID."
				")->value();
				
				if($manymanyExists) {
					// Update original SortOrder for DailyMenuPage_Dishes relation
					DB::query("
						UPDATE DailyMenuPage_Dishes
						SET SortOrder = $origSortOrder
						WHERE ".$this->owner->ClassName."ID = ".$this->owner->ID."
						AND ".$duplicateDish->ClassName."ID = ".$duplicateDish->ID."
					");
				}
				$status = 'success';
			}
		}
		return $status;
	}
}