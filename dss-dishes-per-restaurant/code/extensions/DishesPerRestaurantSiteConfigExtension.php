<?php

/**
 * Adds site wide configuration options for this module.
 *
 * Add the following block of code to /dss-dishes-per-restaurant/_config/config.yml to
 * activate this class:
 *
 *   SiteConfig:
 *     extensions:
 *       - DishesPerRestaurantSiteConfigExtension
 * 
 * @package dss-dishes-per-restaurant
 * @requires MySiteConfigExtension.php, MySiteConfigExtensionPermissions.php
 * 
 */
class DishesPerRestaurantSiteConfigExtension extends DataExtension {

	static $db = array(
		'EnableIndividualDishesPerRestaurant' => 'Boolean'
	);
	static $defaults = array(
		'EnableIndividualDishesPerRestaurant' => 0
	);

	public function updateCMSFields(FieldList $f) {

		// Are we in the "main site" or a subsite?
		if (Subsite::CurrentSubsite() && Permission::check('VIEW_SITECONFIG_TAB_MODULES')) {
			// Add subsite-specific fields to "Modules" tab
			$f->addFieldToTab('Root.Modules', new HeaderField('HeaderModuleDishesPerRestaurant', 'Modul "Individuelle Gerichte pro Restaurant"'));
			$f->addFieldToTab('Root.Modules', new LabelField('DescriptionModuleDishesPerRestaurant', 'Führt 2 Typen von Gerichten ein: 1. Globale Gerichte (für alle Restaurants) 2. Restaurantspezifische Gerichte<br /> Die Gerichte Typen werden über Farbkodierungen bzw. Textkennungen im User Interface gekennzeichnet.<br />Wenn aktiviert werden Berechtigungen zum Bearbeiten für globale Gerichte eingeschränkt, nur Felder des "RestaurantDish" können bearbeitet werden wenn eine entsprechende Berechtigung vergeben wurde.<br /> Für restaurantspezifische Gerichte werden die Standard Berechtigungen der "Gerichte" übernommen.'));
			$f->addFieldToTab('Root.Modules', new FieldGroup('Modul "Individuelle Gerichte pro Restaurant"', array(new CheckboxField('EnableIndividualDishesPerRestaurant',  'aktiviert'))));
			$f->fieldByName('Root.Modules')->setTitle('Module');
		}
	}
}