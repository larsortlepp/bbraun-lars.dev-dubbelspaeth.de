<?php

/**
 * Extend RestaurantPage for additional functionality to work with DishesPerRestaurant
 *
 * Add the following block of code to /dss-dishes-per-restaurant/_config/config.yml to
 * activate this class:
 *
 *   RestaurantPage:
 *     extensions:
 *       - DishesPerRestaurantDailyMenuPageExtension
 * 
 * @package dss-dishes-per-restaurant
 * @requires RestaurantPage.php
 * 
 */
class DishesPerRestaurantRestaurantPageExtension extends DataExtension {
	
	/**
	 * Functions that modify 'Dishes' GridField behaviour
	 * (can be extended by 'updateDishesGridField...()' functions in extensions)
	 * 
	 * create a function in extension object with REFERENCE to variable, to modify the result
	 * e.g. 
	 * function updateDishesGridFieldAutoCompleterResultFormat(String &$string) {
	 *     $string = '$Description.RAW ($ID)';
	 * }
	 * 
	 */
	public function updateDishesGridFieldQuery(&$query) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) $query = $query->filter('Dish.RestaurantPageID', array(0, $this->owner->ID));
	}
	
	public function updateDishesGridFieldCols(&$cols) {
		if(SiteConfig::current_site_config()->EnableIndividualDishesPerRestaurant) {
			$tmp_cols = $cols;
			// add new field that color marks if Subsite or Restaurant Dish at beginning of exisiting array
			$cols = array_merge(array('DishTypeHTMLCode' => ''), $tmp_cols);
		}
	}
}
