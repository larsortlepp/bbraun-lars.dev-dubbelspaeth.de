-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Erstellungszeit: 26. Nov 2016 um 13:26
-- Server-Version: 5.5.53-0ubuntu0.14.04.1
-- PHP-Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `dss-3-1-0`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AdditivesBlacklistConfig`
--

DROP TABLE IF EXISTS `AdditivesBlacklistConfig`;
CREATE TABLE `AdditivesBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AdditivesBlacklistConfig') DEFAULT 'AdditivesBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AdditivesBlacklistObject`
--

DROP TABLE IF EXISTS `AdditivesBlacklistObject`;
CREATE TABLE `AdditivesBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AdditivesBlacklistObject') DEFAULT 'AdditivesBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `AdditivesBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AllergensBlacklistConfig`
--

DROP TABLE IF EXISTS `AllergensBlacklistConfig`;
CREATE TABLE `AllergensBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AllergensBlacklistConfig') DEFAULT 'AllergensBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AllergensBlacklistObject`
--

DROP TABLE IF EXISTS `AllergensBlacklistObject`;
CREATE TABLE `AllergensBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AllergensBlacklistObject') DEFAULT 'AllergensBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `AllergensBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage`
--

DROP TABLE IF EXISTS `DailyMenuPage`;
CREATE TABLE `DailyMenuPage` (
  `ID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Message` mediumtext,
  `Message_en_US` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage`
--

INSERT INTO `DailyMenuPage` (`ID`, `Date`, `Message`, `Message_en_US`) VALUES(12, '2016-01-01', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage_Dishes`
--

DROP TABLE IF EXISTS `DailyMenuPage_Dishes`;
CREATE TABLE `DailyMenuPage_Dishes` (
  `ID` int(11) NOT NULL,
  `DailyMenuPageID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage_Dishes`
--

INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(1, 12, 1, 1);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(2, 12, 2, 2);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(3, 12, 3, 4);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(4, 12, 4, 5);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(5, 12, 6, 3);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(6, 12, 5, 6);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(7, 12, 7, 8);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(8, 12, 8, 9);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(9, 12, 9, 7);
INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES(10, 12, 10, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage_Live`
--

DROP TABLE IF EXISTS `DailyMenuPage_Live`;
CREATE TABLE `DailyMenuPage_Live` (
  `ID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Message` mediumtext,
  `Message_en_US` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage_Live`
--

INSERT INTO `DailyMenuPage_Live` (`ID`, `Date`, `Message`, `Message_en_US`) VALUES(12, '2016-01-01', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage_versions`
--

DROP TABLE IF EXISTS `DailyMenuPage_versions`;
CREATE TABLE `DailyMenuPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` date DEFAULT NULL,
  `Message` mediumtext,
  `Message_en_US` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage_versions`
--

INSERT INTO `DailyMenuPage_versions` (`ID`, `RecordID`, `Version`, `Date`, `Message`, `Message_en_US`) VALUES(1, 12, 1, NULL, NULL, NULL);
INSERT INTO `DailyMenuPage_versions` (`ID`, `RecordID`, `Version`, `Date`, `Message`, `Message_en_US`) VALUES(2, 12, 2, NULL, NULL, NULL);
INSERT INTO `DailyMenuPage_versions` (`ID`, `RecordID`, `Version`, `Date`, `Message`, `Message_en_US`) VALUES(3, 12, 3, '2016-01-01', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish`
--

DROP TABLE IF EXISTS `Dish`;
CREATE TABLE `Dish` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Dish') DEFAULT 'Dish',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Description` mediumtext,
  `Description_en_US` mediumtext,
  `Infotext` mediumtext,
  `Infotext_en_US` mediumtext,
  `Calories` float NOT NULL DEFAULT '0',
  `Kilojoule` float NOT NULL DEFAULT '0',
  `Fat` float NOT NULL DEFAULT '0',
  `FattyAcids` float NOT NULL DEFAULT '0',
  `Carbohydrates` float NOT NULL DEFAULT '0',
  `Sugar` float NOT NULL DEFAULT '0',
  `Fibres` float NOT NULL DEFAULT '0',
  `Protein` float NOT NULL DEFAULT '0',
  `Salt` float NOT NULL DEFAULT '0',
  `ImportID` varchar(255) DEFAULT NULL,
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `OrigDishID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish`
--

INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(1, 'Dish', '2016-10-27 11:43:03', '2016-10-27 11:43:03', 'Hühnerbrühe mit Gemüse', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(2, 'Dish', '2016-10-27 11:43:29', '2016-10-27 11:43:29', 'Camembert mit Preiselbeeren und Baugette', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(3, 'Dish', '2016-10-27 11:44:43', '2016-10-27 11:48:04', 'Gebratenes Schweinefilet auf Pimentoschaum mit Penne Rigate und Brokkoli', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 30, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(4, 'Dish', '2016-10-27 11:46:34', '2016-10-27 11:46:34', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 18, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(5, 'Dish', '2016-10-27 11:49:05', '2016-10-27 11:50:09', 'Gebackenes Seelachsfilet mit Remouladensoße und Kartoffelsalat', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 32, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(6, 'Dish', '2016-10-27 11:49:53', '2016-10-27 17:07:41', 'Geflügelstreifen in pikanter asiatischer Soße mit Reis', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 42, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(7, 'Dish', '2016-10-27 11:50:37', '2016-10-27 11:50:37', '1/2 Flammkuchen Elsässer Art', '1/2 tarte flambée Alsatian style', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(8, 'Dish', '2016-10-27 11:51:04', '2016-10-27 11:51:04', '1/2 Flammkuchen Florentiner Art', '1/2 Tarte flambée Florentine style', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(9, 'Dish', '2016-10-27 17:31:25', '2016-10-27 17:31:25', 'Kartoffepüree', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);
INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES(10, 'Dish', '2016-10-27 17:32:11', '2016-10-27 17:32:11', 'Waldfruchtjoghurt', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishesPerRestaurantDishExtensionPermissions`
--

DROP TABLE IF EXISTS `DishesPerRestaurantDishExtensionPermissions`;
CREATE TABLE `DishesPerRestaurantDishExtensionPermissions` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishesPerRestaurantDishExtensionPermissions') DEFAULT 'DishesPerRestaurantDishExtensionPermissions',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishLabelsBlacklistConfig`
--

DROP TABLE IF EXISTS `DishLabelsBlacklistConfig`;
CREATE TABLE `DishLabelsBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishLabelsBlacklistConfig') DEFAULT 'DishLabelsBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishLabelsBlacklistObject`
--

DROP TABLE IF EXISTS `DishLabelsBlacklistObject`;
CREATE TABLE `DishLabelsBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishLabelsBlacklistObject') DEFAULT 'DishLabelsBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `DishLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishRating`
--

DROP TABLE IF EXISTS `DishRating`;
CREATE TABLE `DishRating` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishRating') DEFAULT 'DishRating',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `DishDescription` mediumtext,
  `FoodCategoryTitle` varchar(255) DEFAULT NULL,
  `Taste` int(11) NOT NULL DEFAULT '0',
  `Value` int(11) NOT NULL DEFAULT '0',
  `Service` int(11) NOT NULL DEFAULT '0',
  `Comment` mediumtext,
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingItem`
--

DROP TABLE IF EXISTS `DishVotingItem`;
CREATE TABLE `DishVotingItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishVotingItem') DEFAULT 'DishVotingItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `DishDescription` mediumtext,
  `DishDescription_en_US` mediumtext,
  `Votes` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `DishVotingPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingItem`
--

INSERT INTO `DishVotingItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `DishDescription`, `DishDescription_en_US`, `Votes`, `SortOrder`, `DishVotingPageID`) VALUES(1, 'DishVotingItem', '2016-10-27 12:29:31', '2016-10-27 12:29:31', 'Glasnudeln mit Rindfleisch und knackigem Gemüse', NULL, 0, 0, 30);
INSERT INTO `DishVotingItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `DishDescription`, `DishDescription_en_US`, `Votes`, `SortOrder`, `DishVotingPageID`) VALUES(2, 'DishVotingItem', '2016-10-27 12:29:45', '2016-10-27 12:29:45', 'Bratwurst mit Sauerkraut und Püree', NULL, 0, 0, 30);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingPage`
--

DROP TABLE IF EXISTS `DishVotingPage`;
CREATE TABLE `DishVotingPage` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingPage`
--

INSERT INTO `DishVotingPage` (`ID`, `VotingDate`, `Publish`) VALUES(30, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingPage_Live`
--

DROP TABLE IF EXISTS `DishVotingPage_Live`;
CREATE TABLE `DishVotingPage_Live` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingPage_Live`
--

INSERT INTO `DishVotingPage_Live` (`ID`, `VotingDate`, `Publish`) VALUES(30, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingPage_versions`
--

DROP TABLE IF EXISTS `DishVotingPage_versions`;
CREATE TABLE `DishVotingPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingPage_versions`
--

INSERT INTO `DishVotingPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(1, 30, 1, NULL, 0);
INSERT INTO `DishVotingPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(2, 30, 2, NULL, 0);
INSERT INTO `DishVotingPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(3, 30, 3, '2016-10-27', 0);
INSERT INTO `DishVotingPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(4, 30, 4, '2016-10-28', 0);
INSERT INTO `DishVotingPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(5, 30, 5, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalAdditives`
--

DROP TABLE IF EXISTS `Dish_GlobalAdditives`;
CREATE TABLE `Dish_GlobalAdditives` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalAdditiveID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalAllergens`
--

DROP TABLE IF EXISTS `Dish_GlobalAllergens`;
CREATE TABLE `Dish_GlobalAllergens` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalAllergenID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalDishLabels`
--

DROP TABLE IF EXISTS `Dish_GlobalDishLabels`;
CREATE TABLE `Dish_GlobalDishLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalDishLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalSpecialLabels`
--

DROP TABLE IF EXISTS `Dish_GlobalSpecialLabels`;
CREATE TABLE `Dish_GlobalSpecialLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalSpecialLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteAdditives`
--

DROP TABLE IF EXISTS `Dish_SubsiteAdditives`;
CREATE TABLE `Dish_SubsiteAdditives` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteAdditiveID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteAllergens`
--

DROP TABLE IF EXISTS `Dish_SubsiteAllergens`;
CREATE TABLE `Dish_SubsiteAllergens` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteAllergenID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteDishLabels`
--

DROP TABLE IF EXISTS `Dish_SubsiteDishLabels`;
CREATE TABLE `Dish_SubsiteDishLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteDishLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteSpecialLabels`
--

DROP TABLE IF EXISTS `Dish_SubsiteSpecialLabels`;
CREATE TABLE `Dish_SubsiteSpecialLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteSpecialLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplateObject`
--

DROP TABLE IF EXISTS `DisplayTemplateObject`;
CREATE TABLE `DisplayTemplateObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DisplayTemplateObject') DEFAULT 'DisplayTemplateObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `DisplayMultiTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplateObject_FoodCategories`
--

DROP TABLE IF EXISTS `DisplayTemplateObject_FoodCategories`;
CREATE TABLE `DisplayTemplateObject_FoodCategories` (
  `ID` int(11) NOT NULL,
  `DisplayTemplateObjectID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplateObject_TimerRecurringDaysOfWeek`
--

DROP TABLE IF EXISTS `DisplayTemplateObject_TimerRecurringDaysOfWeek`;
CREATE TABLE `DisplayTemplateObject_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `DisplayTemplateObjectID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage`
--

DROP TABLE IF EXISTS `DisplayTemplatePage`;
CREATE TABLE `DisplayTemplatePage` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `InfoMessagesOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReloadInterval` int(11) NOT NULL DEFAULT '0',
  `TransitionDuration` int(11) NOT NULL DEFAULT '0',
  `ShowScreenDuration` int(11) NOT NULL DEFAULT '0',
  `TransitionEffect` enum('fade','slide') DEFAULT 'fade',
  `ScreenRotation` enum('keine','90° im Uhrzeigersinn','90° gegen den Uhrzeigersinn') DEFAULT 'keine',
  `DisplayTemplatePage_WeeklyMenuTableTemplate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DisplayTemplatePage`
--

INSERT INTO `DisplayTemplatePage` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(21, 'Screen_DailyMenu_Photos', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(22, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(23, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(24, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(25, 'Screen_Counter_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage_Live`
--

DROP TABLE IF EXISTS `DisplayTemplatePage_Live`;
CREATE TABLE `DisplayTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `InfoMessagesOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReloadInterval` int(11) NOT NULL DEFAULT '0',
  `TransitionDuration` int(11) NOT NULL DEFAULT '0',
  `ShowScreenDuration` int(11) NOT NULL DEFAULT '0',
  `TransitionEffect` enum('fade','slide') DEFAULT 'fade',
  `ScreenRotation` enum('keine','90° im Uhrzeigersinn','90° gegen den Uhrzeigersinn') DEFAULT 'keine',
  `DisplayTemplatePage_WeeklyMenuTableTemplate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DisplayTemplatePage_Live`
--

INSERT INTO `DisplayTemplatePage_Live` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(21, 'Screen_DailyMenu_Photos', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_Live` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(22, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_Live` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(23, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_Live` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(24, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_Live` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(25, 'Screen_Counter_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage_TimerRecurringDaysOfWeek`
--

DROP TABLE IF EXISTS `DisplayTemplatePage_TimerRecurringDaysOfWeek`;
CREATE TABLE `DisplayTemplatePage_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `DisplayTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage_versions`
--

DROP TABLE IF EXISTS `DisplayTemplatePage_versions`;
CREATE TABLE `DisplayTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `TemplateName` varchar(255) DEFAULT NULL,
  `InfoMessagesOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReloadInterval` int(11) NOT NULL DEFAULT '0',
  `TransitionDuration` int(11) NOT NULL DEFAULT '0',
  `ShowScreenDuration` int(11) NOT NULL DEFAULT '0',
  `TransitionEffect` enum('fade','slide') DEFAULT 'fade',
  `ScreenRotation` enum('keine','90° im Uhrzeigersinn','90° gegen den Uhrzeigersinn') DEFAULT 'keine',
  `DisplayTemplatePage_WeeklyMenuTableTemplate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DisplayTemplatePage_versions`
--

INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(1, 21, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(2, 21, 2, 'Screen_DailyMenu_Photos', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(3, 21, 3, 'Screen_DailyMenu_Photos', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(4, 22, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(5, 22, 2, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(6, 22, 3, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(7, 23, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(8, 23, 2, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(9, 24, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(10, 24, 2, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(11, 25, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(12, 25, 2, 'Screen_Counter_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES(13, 23, 3, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayVideo`
--

DROP TABLE IF EXISTS `DisplayVideo`;
CREATE TABLE `DisplayVideo` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DisplayVideo') DEFAULT 'DisplayVideo',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `VideoType` enum('Datei','Youtube') DEFAULT 'Datei',
  `YoutubeLink` varchar(255) DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `VideoFileWebMID` int(11) NOT NULL DEFAULT '0',
  `DisplayTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayVideo_TimerRecurringDaysOfWeek`
--

DROP TABLE IF EXISTS `DisplayVideo_TimerRecurringDaysOfWeek`;
CREATE TABLE `DisplayVideo_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `DisplayVideoID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ErrorPage`
--

DROP TABLE IF EXISTS `ErrorPage`;
CREATE TABLE `ErrorPage` (
  `ID` int(11) NOT NULL,
  `ErrorCode` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ErrorPage`
--

INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES(4, 404);
INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES(5, 500);
INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES(9, 404);
INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES(10, 500);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ErrorPage_Live`
--

DROP TABLE IF EXISTS `ErrorPage_Live`;
CREATE TABLE `ErrorPage_Live` (
  `ID` int(11) NOT NULL,
  `ErrorCode` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ErrorPage_Live`
--

INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES(4, 404);
INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES(5, 500);
INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES(9, 404);
INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES(10, 500);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ErrorPage_versions`
--

DROP TABLE IF EXISTS `ErrorPage_versions`;
CREATE TABLE `ErrorPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ErrorPage_versions`
--

INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES(1, 4, 1, 404);
INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES(2, 5, 1, 500);
INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES(3, 9, 1, 404);
INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES(4, 10, 1, 500);
INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES(5, 9, 2, 404);
INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES(6, 10, 2, 500);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportConfig`
--

DROP TABLE IF EXISTS `ExportConfig`;
CREATE TABLE `ExportConfig` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Path` varchar(255) DEFAULT NULL,
  `AllowApiAccess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ApiPublicKey` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportDisplayMenuXmlConfig`
--

DROP TABLE IF EXISTS `ExportDisplayMenuXmlConfig`;
CREATE TABLE `ExportDisplayMenuXmlConfig` (
  `ID` int(11) NOT NULL,
  `ExportFunction` enum('export_default') DEFAULT 'export_default',
  `DisplayTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportMenuHtmlConfig`
--

DROP TABLE IF EXISTS `ExportMenuHtmlConfig`;
CREATE TABLE `ExportMenuHtmlConfig` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportMenuHtmlConfig_FoodCategories`
--

DROP TABLE IF EXISTS `ExportMenuHtmlConfig_FoodCategories`;
CREATE TABLE `ExportMenuHtmlConfig_FoodCategories` (
  `ID` int(11) NOT NULL,
  `ExportMenuHtmlConfigID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportMenuXmlConfig`
--

DROP TABLE IF EXISTS `ExportMenuXmlConfig`;
CREATE TABLE `ExportMenuXmlConfig` (
  `ID` int(11) NOT NULL,
  `ExportFunction` enum('export_default') DEFAULT 'export_default',
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File`
--

DROP TABLE IF EXISTS `File`;
CREATE TABLE `File` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('File','Folder','Image','Image_Cached') DEFAULT 'File',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Filename` mediumtext,
  `Content` mediumtext,
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditFolderType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanDeleteType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanUploadType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File`
--

INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(1, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'Aktionslabels', 'Aktionslabels', 'assets/Aktionslabels/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(2, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'classic-edition.png', 'classic-edition.png', 'assets/Aktionslabels/classic-edition.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(3, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'coa.png', 'coa.png', 'assets/Aktionslabels/coa.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(4, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'curry-edition.png', 'curry-edition.png', 'assets/Aktionslabels/curry-edition.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(5, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'dean-and-david.png', 'dean-and-david.png', 'assets/Aktionslabels/dean-and-david.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(6, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'fitforfun.png', 'fitforfun.png', 'assets/Aktionslabels/fitforfun.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(7, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'fitforfunvitalien.png', 'fitforfunvitalien.png', 'assets/Aktionslabels/fitforfunvitalien.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(8, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'sushi-circle.png', 'sushi-circle.png', 'assets/Aktionslabels/sushi-circle.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(9, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'vebu.png', 'vebu.png', 'assets/Aktionslabels/vebu.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(10, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'vitalien-2015.png', 'vitalien-2015.png', 'assets/Aktionslabels/vitalien-2015.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(11, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:03:17', 'Druck', 'Druck', 'assets/Druck/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(12, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Fotos-Gerichte', 'Fotos-Gerichte', 'assets/Fotos-Gerichte/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(13, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', '1.jpg', '1.jpg', 'assets/Fotos-Gerichte/1.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(14, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', '2.jpg', '2.jpg', 'assets/Fotos-Gerichte/2.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(15, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'CanneloniSpinat.jpg', 'CanneloniSpinat.jpg', 'assets/Fotos-Gerichte/CanneloniSpinat.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(16, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Eurestcurrywurst.jpg', 'Eurestcurrywurst.jpg', 'assets/Fotos-Gerichte/Eurestcurrywurst.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(17, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Feuergriller.jpg', 'Feuergriller.jpg', 'assets/Fotos-Gerichte/Feuergriller.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(18, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Frikka.jpg', 'Frikka.jpg', 'assets/Fotos-Gerichte/Frikka.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(19, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Gemsecurry.jpg', 'Gemsecurry.jpg', 'assets/Fotos-Gerichte/Gemsecurry.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(20, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'GeschnetzeltesPolenta.jpg', 'GeschnetzeltesPolenta.jpg', 'assets/Fotos-Gerichte/GeschnetzeltesPolenta.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(21, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Hahnchen-Himmel-und-Erd.jpg', 'Hahnchen-Himmel-und-Erd.jpg', 'assets/Fotos-Gerichte/Hahnchen-Himmel-und-Erd.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(22, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Maispoularde.jpg', 'Maispoularde.jpg', 'assets/Fotos-Gerichte/Maispoularde.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(23, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Nackensteak.jpg', 'Nackensteak.jpg', 'assets/Fotos-Gerichte/Nackensteak.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(24, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Napoli.jpg', 'Napoli.jpg', 'assets/Fotos-Gerichte/Napoli.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(25, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Penne-Mandariene.jpg', 'Penne-Mandariene.jpg', 'assets/Fotos-Gerichte/Penne-Mandariene.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(26, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Pizza-Gemse.jpg', 'Pizza-Gemse.jpg', 'assets/Fotos-Gerichte/Pizza-Gemse.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(27, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Putenbrust-aus-dem-wok.jpg', 'Putenbrust-aus-dem-wok.jpg', 'assets/Fotos-Gerichte/Putenbrust-aus-dem-wok.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(28, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'SaltinBocca.jpg', 'SaltinBocca.jpg', 'assets/Fotos-Gerichte/SaltinBocca.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(29, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'SaltinBocca2.jpg', 'SaltinBocca2.jpg', 'assets/Fotos-Gerichte/SaltinBocca2.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(30, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'SchwSchulter-kartoffelsalat.jpg', 'SchwSchulter-kartoffelsalat.jpg', 'assets/Fotos-Gerichte/SchwSchulter-kartoffelsalat.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(31, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Schweinepfeffer.jpg', 'Schweinepfeffer.jpg', 'assets/Fotos-Gerichte/Schweinepfeffer.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(32, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Seelachs-Senfsauce.jpg', 'Seelachs-Senfsauce.jpg', 'assets/Fotos-Gerichte/Seelachs-Senfsauce.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(33, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Spirelli-Paprika.jpg', 'Spirelli-Paprika.jpg', 'assets/Fotos-Gerichte/Spirelli-Paprika.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(34, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Tellerrsti.jpg', 'Tellerrsti.jpg', 'assets/Fotos-Gerichte/Tellerrsti.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(35, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'cheeseburger.jpg', 'cheeseburger.jpg', 'assets/Fotos-Gerichte/cheeseburger.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(36, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'fischragout.jpg', 'fischragout.jpg', 'assets/Fotos-Gerichte/fischragout.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(37, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'glasierteHhnchen.jpg', 'glasierteHhnchen.jpg', 'assets/Fotos-Gerichte/glasierteHhnchen.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(38, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'italSchnitzel.jpg', 'italSchnitzel.jpg', 'assets/Fotos-Gerichte/italSchnitzel.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(39, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'karotteerbse.jpg', 'karotteerbse.jpg', 'assets/Fotos-Gerichte/karotteerbse.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(40, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'ofenkartoffel.jpg', 'ofenkartoffel.jpg', 'assets/Fotos-Gerichte/ofenkartoffel.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(41, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'piccata.jpg', 'piccata.jpg', 'assets/Fotos-Gerichte/piccata.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(42, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'salat.jpg', 'salat.jpg', 'assets/Fotos-Gerichte/salat.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(43, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'schweinesteak.jpg', 'schweinesteak.jpg', 'assets/Fotos-Gerichte/schweinesteak.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(44, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'Kennzeichnungen', 'Kennzeichnungen', 'assets/Kennzeichnungen/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(45, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'chili.png', 'chili.png', 'assets/Kennzeichnungen/chili.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(46, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'fisch.png', 'fisch.png', 'assets/Kennzeichnungen/fisch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(47, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'huhn.png', 'huhn.png', 'assets/Kennzeichnungen/huhn.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(48, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'knoblauch.png', 'knoblauch.png', 'assets/Kennzeichnungen/knoblauch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(49, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'laktosefrei.png', 'laktosefrei.png', 'assets/Kennzeichnungen/laktosefrei.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(50, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'rind.png', 'rind.png', 'assets/Kennzeichnungen/rind.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(51, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'schwein.png', 'schwein.png', 'assets/Kennzeichnungen/schwein.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(52, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'tagesmenue.png', 'tagesmenue.png', 'assets/Kennzeichnungen/tagesmenue.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(53, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'vegan.png', 'vegan.png', 'assets/Kennzeichnungen/vegan.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(54, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'vegetarisch.png', 'vegetarisch.png', 'assets/Kennzeichnungen/vegetarisch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(55, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'zwiebel.png', 'zwiebel.png', 'assets/Kennzeichnungen/zwiebel.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(56, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:06:18', 'Logos', 'Logos', 'assets/Logos/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(57, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:06:18', 'logo-eurest.gif', 'logo-eurest.gif', 'assets/Logos/logo-eurest.gif', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 56, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(58, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:06:45', 'Microsite', 'Microsite', 'assets/Microsite/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(59, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'News', 'News', 'assets/News/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(60, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', '2014-aktion-dean-david-1920x1080.jpg', '2014-aktion-dean-david-1920x1080.jpg', 'assets/News/2014-aktion-dean-david-1920x1080.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(61, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', '2014-app-market-1980x1080.jpg', '2014-app-market-1980x1080.jpg', 'assets/News/2014-app-market-1980x1080.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(62, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'coa-info.png', 'coa-info.png', 'assets/News/coa-info.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(63, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-snacking-bagel.jpg', 'microsite-snacking-bagel.jpg', 'assets/News/microsite-snacking-bagel.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(64, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-snacking-kaffeemaschine.jpg', 'microsite-snacking-kaffeemaschine.jpg', 'assets/News/microsite-snacking-kaffeemaschine.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(65, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-snacking-kaffeetasse.jpg', 'microsite-snacking-kaffeetasse.jpg', 'assets/News/microsite-snacking-kaffeetasse.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(66, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-speiseplan.jpg', 'microsite-speiseplan.jpg', 'assets/News/microsite-speiseplan.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(67, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-startseite-slider-1.jpg', 'microsite-startseite-slider-1.jpg', 'assets/News/microsite-startseite-slider-1.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(68, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-startseite-slider-2.jpg', 'microsite-startseite-slider-2.jpg', 'assets/News/microsite-startseite-slider-2.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(69, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-startseite-slider-3.jpg', 'microsite-startseite-slider-3.jpg', 'assets/News/microsite-startseite-slider-3.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(70, 'File', '2016-10-27 09:53:20', '2016-10-27 09:53:20', 'error-404.html', 'error-404.html', 'assets/error-404.html', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(71, 'File', '2016-10-27 09:53:20', '2016-10-27 09:53:20', 'error-500.html', 'error-500.html', 'assets/error-500.html', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(72, 'Folder', '2016-10-27 11:44:09', '2016-10-27 16:50:06', 'vorlage', 'vorlage', 'assets/vorlage/', NULL, 1, 'OnlyTheseUsers', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(73, 'Folder', '2016-10-27 11:44:09', '2016-10-27 16:50:06', 'restaurant-vorlage', 'restaurant-vorlage', 'assets/vorlage/restaurant-vorlage/', NULL, 1, 'Inherit', 'OnlyTheseUsers', 'Inherit', 'OnlyTheseUsers', 72, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(74, 'Folder', '2016-10-27 11:44:09', '2016-10-27 16:50:06', 'Fotos-Gerichte', 'Fotos-Gerichte', 'assets/vorlage/restaurant-vorlage/Fotos-Gerichte/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(75, 'Folder', '2016-10-27 12:18:20', '2016-10-27 16:50:06', 'News', 'News', 'assets/vorlage/restaurant-vorlage/News/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(76, 'Folder', '2016-10-27 14:27:15', '2016-10-27 16:50:06', 'Microsite', 'Microsite', 'assets/vorlage/restaurant-vorlage/Microsite/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(77, 'File', '2016-10-27 14:27:16', '2016-10-27 16:50:06', 'Wochenspeiseplan.pdf', 'Wochenspeiseplan', 'assets/vorlage/restaurant-vorlage/Microsite/Wochenspeiseplan.pdf', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 76, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(78, 'Folder', '2016-10-27 14:57:03', '2016-10-27 16:50:06', 'Aktionslabels', 'Aktionslabels', 'assets/vorlage/Aktionslabels/', NULL, 1, 'Inherit', 'OnlyTheseUsers', 'Inherit', 'Inherit', 72, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(79, 'Folder', '2016-10-27 14:57:49', '2016-10-27 16:50:06', 'Kennzeichnungen', 'Kennzeichnungen', 'assets/vorlage/Kennzeichnungen/', NULL, 1, 'Inherit', 'OnlyTheseUsers', 'OnlyTheseUsers', 'OnlyTheseUsers', 72, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(80, 'Folder', '2016-10-27 15:00:22', '2016-10-27 16:50:06', 'Essenskategorien', 'Essenskategorien', 'assets/vorlage/restaurant-vorlage/Essenskategorien/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(81, 'Folder', '2016-10-27 15:04:32', '2016-10-27 16:50:06', 'Logos', 'Logos', 'assets/vorlage/restaurant-vorlage/Logos/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0);
INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES(82, 'Folder', '2016-10-27 15:17:23', '2016-10-27 16:50:06', 'Druck', 'Druck', 'assets/vorlage/restaurant-vorlage/Druck/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_DeleterGroups`
--

DROP TABLE IF EXISTS `File_DeleterGroups`;
CREATE TABLE `File_DeleterGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_DeleterGroups`
--

INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(1, 78, 2);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(2, 78, 3);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(3, 78, 1);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(4, 79, 2);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(5, 79, 3);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(6, 79, 1);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(9, 73, 2);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(10, 73, 3);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(11, 73, 4);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(12, 73, 5);
INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES(13, 73, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_EditorFolderGroups`
--

DROP TABLE IF EXISTS `File_EditorFolderGroups`;
CREATE TABLE `File_EditorFolderGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_EditorFolderGroups`
--

INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(1, 78, 2);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(2, 78, 3);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(3, 78, 1);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(4, 79, 2);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(5, 79, 3);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(6, 79, 1);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(7, 73, 2);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(8, 73, 3);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(9, 73, 4);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(10, 73, 5);
INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES(11, 73, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_UploaderGroups`
--

DROP TABLE IF EXISTS `File_UploaderGroups`;
CREATE TABLE `File_UploaderGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_UploaderGroups`
--

INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(1, 73, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(2, 73, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(3, 78, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(4, 78, 3);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(5, 78, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(6, 79, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(7, 79, 3);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(8, 79, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(9, 73, 3);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(10, 73, 4);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(11, 73, 5);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(12, 1, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(13, 1, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(14, 11, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(15, 11, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(16, 12, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(17, 12, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(18, 44, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(19, 44, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(20, 56, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(21, 56, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(22, 58, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(23, 58, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(24, 59, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(25, 59, 1);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(26, 72, 2);
INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES(27, 72, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_ViewerGroups`
--

DROP TABLE IF EXISTS `File_ViewerGroups`;
CREATE TABLE `File_ViewerGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_ViewerGroups`
--

INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(1, 73, 2);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(2, 73, 3);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(3, 73, 4);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(4, 73, 5);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(5, 73, 1);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(6, 72, 2);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(7, 72, 3);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(8, 72, 4);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(9, 72, 5);
INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES(10, 72, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FoodCategory`
--

DROP TABLE IF EXISTS `FoodCategory`;
CREATE TABLE `FoodCategory` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('FoodCategory') DEFAULT 'FoodCategory',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `Infotext` mediumtext,
  `Infotext_en_US` mediumtext,
  `Color` varchar(7) DEFAULT NULL,
  `IconUnicodeCode` varchar(50) DEFAULT NULL,
  `IsMain` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `FoodCategory`
--

INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(1, 'FoodCategory', '2016-10-27 11:35:13', '2016-10-27 11:35:24', 'Suppe', 'Soup', NULL, NULL, NULL, NULL, 0, 1, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(2, 'FoodCategory', '2016-10-27 11:35:48', '2016-10-27 11:36:05', 'Vorspeise', 'Appetizer', NULL, NULL, NULL, NULL, 0, 2, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(3, 'FoodCategory', '2016-10-27 11:36:27', '2016-10-27 11:38:03', 'Aktion', 'Aktion', NULL, NULL, '#c00303', NULL, 1, 3, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(4, 'FoodCategory', '2016-10-27 11:36:58', '2016-10-27 11:38:25', 'Vitalien', 'Vitalien', NULL, NULL, '#9cc42b', NULL, 1, 4, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(5, 'FoodCategory', '2016-10-27 11:37:28', '2016-10-27 17:09:18', 'Regional', 'Regional', NULL, NULL, '#00a2e2', NULL, 1, 5, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(6, 'FoodCategory', '2016-10-27 11:38:44', '2016-10-27 11:38:56', 'Market', 'Market', NULL, NULL, '#ee7311', NULL, 1, 6, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(7, 'FoodCategory', '2016-10-27 11:39:24', '2016-10-27 11:39:52', 'Beilagen', 'Sides', NULL, NULL, NULL, NULL, 0, 7, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(8, 'FoodCategory', '2016-10-27 11:39:59', '2016-10-27 11:40:06', 'Salat', 'Salad', NULL, NULL, NULL, NULL, 0, 8, 0, 7);
INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES(9, 'FoodCategory', '2016-10-27 11:40:31', '2016-10-27 11:40:37', 'Dessert', 'Dessert', NULL, NULL, NULL, NULL, 0, 9, 0, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FoodCategoryBlacklistConfig`
--

DROP TABLE IF EXISTS `FoodCategoryBlacklistConfig`;
CREATE TABLE `FoodCategoryBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('FoodCategoryBlacklistConfig') DEFAULT 'FoodCategoryBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FoodCategoryBlacklistObject`
--

DROP TABLE IF EXISTS `FoodCategoryBlacklistObject`;
CREATE TABLE `FoodCategoryBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('FoodCategoryBlacklistObject') DEFAULT 'FoodCategoryBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `FoodCategoryBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalAdditive`
--

DROP TABLE IF EXISTS `GlobalAdditive`;
CREATE TABLE `GlobalAdditive` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalAdditive') DEFAULT 'GlobalAdditive',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalAllergen`
--

DROP TABLE IF EXISTS `GlobalAllergen`;
CREATE TABLE `GlobalAllergen` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalAllergen') DEFAULT 'GlobalAllergen',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalDishLabel`
--

DROP TABLE IF EXISTS `GlobalDishLabel`;
CREATE TABLE `GlobalDishLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalDishLabel') DEFAULT 'GlobalDishLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `IconUnicodeCode` varchar(50) DEFAULT NULL,
  `Color` varchar(7) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalSpecialLabel`
--

DROP TABLE IF EXISTS `GlobalSpecialLabel`;
CREATE TABLE `GlobalSpecialLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalSpecialLabel') DEFAULT 'GlobalSpecialLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group`
--

DROP TABLE IF EXISTS `Group`;
CREATE TABLE `Group` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Group') DEFAULT 'Group',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` mediumtext,
  `Code` varchar(255) DEFAULT NULL,
  `Locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext,
  `AccessAllSubsites` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Group`
--

INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(1, 'Group', '2016-10-17 09:06:31', '2016-10-17 13:56:18', 'Admin System', NULL, 'content-authors', 0, 1, NULL, 1, 0);
INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(2, 'Group', '2016-10-17 09:06:31', '2016-10-17 11:44:25', 'Admin Global', NULL, 'administrators', 0, 0, NULL, 1, 0);
INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(3, 'Group', '2016-10-17 10:24:06', '2016-10-17 11:44:34', 'Admin Mandant', NULL, 'admin-system', 0, 0, NULL, 1, 0);
INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(4, 'Group', '2016-10-17 10:24:16', '2016-10-17 11:44:51', 'Admin Restaurant', NULL, 'admin-mandant', 0, 0, NULL, 1, 0);
INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(5, 'Group', '2016-10-17 10:24:26', '2016-11-26 13:25:24', 'Editor Restaurant', NULL, 'admin-restaurant', 0, 0, NULL, 1, 0);
INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(6, 'Group', '2016-10-17 10:24:55', '2016-10-17 11:57:21', 'Admin Microsite', NULL, 'admin-microsite', 0, 0, NULL, 1, 0);
INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES(7, 'Group', '2016-10-17 10:25:07', '2016-10-17 11:45:38', 'Editor Microsite', NULL, 'editor-restaurant', 0, 0, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group_Members`
--

DROP TABLE IF EXISTS `Group_Members`;
CREATE TABLE `Group_Members` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Group_Members`
--

INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES(2, 1, 1);
INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES(3, 5, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group_Roles`
--

DROP TABLE IF EXISTS `Group_Roles`;
CREATE TABLE `Group_Roles` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Group_Roles`
--

INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(1, 1, 2);
INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(2, 2, 1);
INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(3, 3, 3);
INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(5, 4, 4);
INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(6, 5, 5);
INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(7, 7, 6);
INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES(8, 6, 8);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group_Subsites`
--

DROP TABLE IF EXISTS `Group_Subsites`;
CREATE TABLE `Group_Subsites` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportConfig`
--

DROP TABLE IF EXISTS `ImportConfig`;
CREATE TABLE `ImportConfig` (
  `ID` int(11) NOT NULL,
  `URL` varchar(2083) DEFAULT NULL,
  `NutritivesMapConfigID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `AdditivesBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `AllergensBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `DishLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `SpecialLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportFilterConfig`
--

DROP TABLE IF EXISTS `ImportFilterConfig`;
CREATE TABLE `ImportFilterConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('ImportFilterConfig') DEFAULT 'ImportFilterConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportMenuXmlConfig`
--

DROP TABLE IF EXISTS `ImportMenuXmlConfig`;
CREATE TABLE `ImportMenuXmlConfig` (
  `ID` int(11) NOT NULL,
  `ImportFunction` enum('import_lz_catering','import_roche_rotkreuz','import_roche_rotkreuz_productinfo') DEFAULT 'import_lz_catering',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportRestaurantConfig`
--

DROP TABLE IF EXISTS `ImportRestaurantConfig`;
CREATE TABLE `ImportRestaurantConfig` (
  `ID` int(11) NOT NULL,
  `ImportFunction` enum('import_lz_catering') DEFAULT 'import_lz_catering',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportVisitorCounterConfig`
--

DROP TABLE IF EXISTS `ImportVisitorCounterConfig`;
CREATE TABLE `ImportVisitorCounterConfig` (
  `ID` int(11) NOT NULL,
  `URL` varchar(2083) DEFAULT NULL,
  `ImportFunction` enum('import_sensalytics') DEFAULT 'import_sensalytics',
  `PredictionBase` enum('LastWeek','Continous','SameMonthLastYear') DEFAULT 'Continous',
  `Capacity` int(11) NOT NULL DEFAULT '0',
  `CounterDataStart` time DEFAULT NULL,
  `CounterDataEnd` time DEFAULT NULL,
  `CounterDataIntervall` int(11) NOT NULL DEFAULT '0',
  `VisitorThresholdMedium` int(11) NOT NULL DEFAULT '0',
  `VisitorThresholdHigh` int(11) NOT NULL DEFAULT '0',
  `PredicitionDataLastCalculated` datetime DEFAULT NULL,
  `ChartMinY` int(11) NOT NULL DEFAULT '0',
  `ChartMaxY` int(11) NOT NULL DEFAULT '0',
  `ApiHttpMethod` enum('GET','POST','PUT') DEFAULT 'PUT',
  `ApiRequestBody` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessage`
--

DROP TABLE IF EXISTS `InfoMessage`;
CREATE TABLE `InfoMessage` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('InfoMessage','InfoMessageDisplayTemplatePage','InfoMessageDisplayTemplatePageLandscape','InfoMessageDisplayTemplatePagePortrait','InfoMessageMicrositeHomePage','InfoMessageMicrositeNewsPage','InfoMessageMobileTemplatePage','InfoMessagePrintTemplatePage','InfoMessageTemplate','InfoMessageTemplateDisplayTemplatePage','InfoMessageTemplateDisplayTemplatePageLandscape','InfoMessageTemplateDisplayTemplatePagePortrait','InfoMessageTemplateMicrositeHomePage','InfoMessageTemplateMicrositeNewsPage','InfoMessageTemplateMobileTemplatePage','InfoMessageTemplatePrintTemplatePage') DEFAULT 'InfoMessage',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content` mediumtext,
  `Content_en_US` mediumtext,
  `ImageOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessage`
--

INSERT INTO `InfoMessage` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `ImageOnly`, `Publish`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES(1, 'InfoMessageDisplayTemplatePageLandscape', '2016-10-27 12:19:02', '2016-10-27 17:20:08', 'dean & david', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, 1, 60);
INSERT INTO `InfoMessage` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `ImageOnly`, `Publish`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES(2, 'InfoMessageMicrositeHomePage', '2016-10-27 12:40:32', '2016-10-27 12:43:13', 'Vielseitige Kulinarik', NULL, '<p>Ob leicht oder deftig – hier findet jeder etwas für seinen Geschmack</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 1, 1, 67);
INSERT INTO `InfoMessage` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `ImageOnly`, `Publish`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES(3, 'InfoMessageMicrositeHomePage', '2016-10-27 12:41:23', '2016-10-27 12:43:13', 'Mit viel Energie durch den Tag', NULL, '<p>Das passende Mittagsangebot hilft Ihnen dabei, mit neuer Energie frisch durchzustarten</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 2, 1, 66);
INSERT INTO `InfoMessage` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `ImageOnly`, `Publish`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES(4, 'InfoMessageMicrositeHomePage', '2016-10-27 12:43:13', '2016-10-27 12:43:13', 'Foodtrends und Lieblingsrezepte im Fokus', NULL, '<p>Kantine war gestern – in Ihrem Betriebsrestaurant möchten wir Ihnen viel Abwechslung uns spannende Neuheiten bieten.</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 3, 1, 63);
INSERT INTO `InfoMessage` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `ImageOnly`, `Publish`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES(5, 'InfoMessageMicrositeNewsPage', '2016-10-27 13:22:41', '2016-10-27 13:22:41', 'Französisches Flair mit Vitalien-Special „Vive le football“ bei der Compass Group Deutschland', NULL, '<p><strong>Pünktlich zur Fußball-Europameisterschaft bringt die Compass Group Deutschland vom 10. Juni bis zum 10. Juli 2016 mit sommerlich leichten und sportlichen Gerichten französisches Flair in ihre Eurest-Restaurants.</strong></p>\n<div>\n<p>„Vive le football“ heißt das Special, das an fünf Tagen, auf mehrere Wochen verteilt, mit jeweils einem speziell zubereiteten Gericht der Marke „Vitalien“ die Gäste in die Restaurants lockt. Die Menüs spiegeln eine Auswahl typischer Speisen aus der französischen Landesküche, gemischt mit moderner Küche, wider. Das Besondere: Die kulinarischen Highlights sollen die Leistungsfähigkeit fördern und nicht belasten.</p>\n<p><strong>Unvergesslicher Genuss aus Frankreich</strong><br>Gutes Essen gehört in der Grande Nation zur Lebensart dazu. Je nach Region reicht die Spannbreite von mediterranen bis hin zu deftigen Spezialitäten. „Dass französische Küche sommerlich leicht sein kann, zeigen unsere Vitalien-Menüs“, sagt Attila Wandrey, Head of Marketing &amp; Communications. „Unsere kalorienreduzierten und fettarmen Rezepturen wirken nicht nur der Müdigkeit und dem Mittagstief entgegen, sondern sind auch optimale Fitmacher für einen langen Arbeitstag.“</p>\n<p><strong>Exquisite Rundreise durch Frankreich</strong><br>Um in diesem Zusammenhang der Vielfalt an Köstlichkeiten gerecht zu werden, variieren die Gerichte an den Special-Tagen je nach Region, in der die Spiele stattfinden. So finden sich neben klassischen Produkten wie frischen Kräutern und Gewürzen auch Hülsenfrüchte und fettreduzierte Öle wie Olivenöl unter den Zutaten wieder. Liebhaber der traditionellen Küche dürfen sich vorab auf delikate Hochgenüsse wie „Bretonische Bouillabaisse mit Meeresfrüchten und Croutons“ sowie „Französische Entenbrust auf Ragout von roten Linsen mit Dijon-Senf-Kräuter-Hollandaise“ freuen.</p>\n</div>', NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageDisplayTemplatePageLandscape`
--

DROP TABLE IF EXISTS `InfoMessageDisplayTemplatePageLandscape`;
CREATE TABLE `InfoMessageDisplayTemplatePageLandscape` (
  `ID` int(11) NOT NULL,
  `DisplayTemplatePageLandscapeID` int(11) NOT NULL DEFAULT '0',
  `InfoMessageTemplateDisplayTemplatePageLandscapeID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessageDisplayTemplatePageLandscape`
--

INSERT INTO `InfoMessageDisplayTemplatePageLandscape` (`ID`, `DisplayTemplatePageLandscapeID`, `InfoMessageTemplateDisplayTemplatePageLandscapeID`) VALUES(1, 23, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageDisplayTemplatePagePortrait`
--

DROP TABLE IF EXISTS `InfoMessageDisplayTemplatePagePortrait`;
CREATE TABLE `InfoMessageDisplayTemplatePagePortrait` (
  `ID` int(11) NOT NULL,
  `DisplayTemplatePagePortraitID` int(11) NOT NULL DEFAULT '0',
  `InfoMessageTemplateDisplayTemplatePagePortraitID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageMicrositeHomePage`
--

DROP TABLE IF EXISTS `InfoMessageMicrositeHomePage`;
CREATE TABLE `InfoMessageMicrositeHomePage` (
  `ID` int(11) NOT NULL,
  `LinkType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `InfoMessageTemplateMicrositeHomePageID` int(11) NOT NULL DEFAULT '0',
  `MicrositeHomePageID` int(11) NOT NULL DEFAULT '0',
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  `DownloadID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessageMicrositeHomePage`
--

INSERT INTO `InfoMessageMicrositeHomePage` (`ID`, `LinkType`, `ExternalURL`, `InfoMessageTemplateMicrositeHomePageID`, `MicrositeHomePageID`, `LinkToID`, `DownloadID`) VALUES(2, 'Internal', NULL, 0, 35, 0, 0);
INSERT INTO `InfoMessageMicrositeHomePage` (`ID`, `LinkType`, `ExternalURL`, `InfoMessageTemplateMicrositeHomePageID`, `MicrositeHomePageID`, `LinkToID`, `DownloadID`) VALUES(3, 'Internal', NULL, 0, 35, 0, 0);
INSERT INTO `InfoMessageMicrositeHomePage` (`ID`, `LinkType`, `ExternalURL`, `InfoMessageTemplateMicrositeHomePageID`, `MicrositeHomePageID`, `LinkToID`, `DownloadID`) VALUES(4, 'Internal', NULL, 0, 35, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageMicrositeNewsPage`
--

DROP TABLE IF EXISTS `InfoMessageMicrositeNewsPage`;
CREATE TABLE `InfoMessageMicrositeNewsPage` (
  `ID` int(11) NOT NULL,
  `InfoMessageTemplateMicrositeNewsPageID` int(11) NOT NULL DEFAULT '0',
  `MicrositeNewsPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessageMicrositeNewsPage`
--

INSERT INTO `InfoMessageMicrositeNewsPage` (`ID`, `InfoMessageTemplateMicrositeNewsPageID`, `MicrositeNewsPageID`) VALUES(5, 0, 36);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageMobileTemplatePage`
--

DROP TABLE IF EXISTS `InfoMessageMobileTemplatePage`;
CREATE TABLE `InfoMessageMobileTemplatePage` (
  `ID` int(11) NOT NULL,
  `InfoMessageTemplateMobileTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `MobileTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessagePrintTemplatePage`
--

DROP TABLE IF EXISTS `InfoMessagePrintTemplatePage`;
CREATE TABLE `InfoMessagePrintTemplatePage` (
  `ID` int(11) NOT NULL,
  `InfoMessageTemplatePrintTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `PrintTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `PrintHtmlTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessage_TimerRecurringDaysOfWeek`
--

DROP TABLE IF EXISTS `InfoMessage_TimerRecurringDaysOfWeek`;
CREATE TABLE `InfoMessage_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `InfoMessageID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `LoginAttempt`
--

DROP TABLE IF EXISTS `LoginAttempt`;
CREATE TABLE `LoginAttempt` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('LoginAttempt') DEFAULT 'LoginAttempt',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Status` enum('Success','Failure') DEFAULT 'Success',
  `IP` varchar(255) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Member`
--

DROP TABLE IF EXISTS `Member`;
CREATE TABLE `Member` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Member') DEFAULT 'Member',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Email` varchar(254) DEFAULT NULL,
  `TempIDHash` varchar(160) DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `RememberLoginToken` varchar(160) DEFAULT NULL,
  `NumVisit` int(11) NOT NULL DEFAULT '0',
  `LastVisited` datetime DEFAULT NULL,
  `AutoLoginHash` varchar(160) DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `DateFormat` varchar(30) DEFAULT NULL,
  `TimeFormat` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Member`
--

INSERT INTO `Member` (`ID`, `ClassName`, `Created`, `LastEdited`, `FirstName`, `Surname`, `Email`, `TempIDHash`, `TempIDExpired`, `Password`, `RememberLoginToken`, `NumVisit`, `LastVisited`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `DateFormat`, `TimeFormat`) VALUES(1, 'Member', '2016-10-17 09:06:31', '2016-10-31 10:36:00', 'Standard Admin', NULL, 'admin', '07978804e5355460d496d7585e53b2a155b6cf98', '2016-11-03 10:36:00', '$2y$10$03513e6846e5d7b990218uZbYrQn4ocPeKrNElHcYxDidyKKZNsd2', NULL, 14, '2016-11-26 13:26:42', NULL, NULL, 'blowfish', '10$03513e6846e5d7b9902181', NULL, NULL, 'de_DE', 0, 'dd.MM.yyyy', 'HH:mm:ss');
INSERT INTO `Member` (`ID`, `ClassName`, `Created`, `LastEdited`, `FirstName`, `Surname`, `Email`, `TempIDHash`, `TempIDExpired`, `Password`, `RememberLoginToken`, `NumVisit`, `LastVisited`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `DateFormat`, `TimeFormat`) VALUES(2, 'Member', '2016-10-17 13:56:53', '2016-10-27 17:16:58', 'Standard Editor', NULL, 'editor', '5280f363ca35886ebe3c6d210d26e02fa0ffc9c5', '2016-10-30 16:16:58', '$2y$10$6e00eebfea3f79820ae81OLeMeQxA0PRUXuyPAiu/FTzmpB3TyI/a', NULL, 8, '2016-10-27 18:22:01', NULL, NULL, 'blowfish', '10$6e00eebfea3f79820ae81c', NULL, NULL, 'de_DE', 0, 'dd.MM.yyyy', 'HH:mm:ss');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MemberPassword`
--

DROP TABLE IF EXISTS `MemberPassword`;
CREATE TABLE `MemberPassword` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MemberPassword') DEFAULT 'MemberPassword',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MemberPassword`
--

INSERT INTO `MemberPassword` (`ID`, `ClassName`, `Created`, `LastEdited`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES(1, 'MemberPassword', '2016-10-17 13:56:53', '2016-10-17 13:56:53', '$2y$10$6e00eebfea3f79820ae81OLeMeQxA0PRUXuyPAiu/FTzmpB3TyI/a', '10$6e00eebfea3f79820ae81c', 'blowfish', 2);
INSERT INTO `MemberPassword` (`ID`, `ClassName`, `Created`, `LastEdited`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES(2, 'MemberPassword', '2016-10-18 08:07:06', '2016-10-18 08:07:06', '$2y$10$03513e6846e5d7b990218uZbYrQn4ocPeKrNElHcYxDidyKKZNsd2', '10$03513e6846e5d7b9902181', 'blowfish', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MenuContainer`
--

DROP TABLE IF EXISTS `MenuContainer`;
CREATE TABLE `MenuContainer` (
  `ID` int(11) NOT NULL,
  `EmptyField` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MenuContainer`
--

INSERT INTO `MenuContainer` (`ID`, `EmptyField`) VALUES(11, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MenuContainer_Live`
--

DROP TABLE IF EXISTS `MenuContainer_Live`;
CREATE TABLE `MenuContainer_Live` (
  `ID` int(11) NOT NULL,
  `EmptyField` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MenuContainer_Live`
--

INSERT INTO `MenuContainer_Live` (`ID`, `EmptyField`) VALUES(11, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MenuContainer_versions`
--

DROP TABLE IF EXISTS `MenuContainer_versions`;
CREATE TABLE `MenuContainer_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `EmptyField` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MenuContainer_versions`
--

INSERT INTO `MenuContainer_versions` (`ID`, `RecordID`, `Version`, `EmptyField`) VALUES(1, 11, 1, 0);
INSERT INTO `MenuContainer_versions` (`ID`, `RecordID`, `Version`, `EmptyField`) VALUES(2, 11, 2, 0);
INSERT INTO `MenuContainer_versions` (`ID`, `RecordID`, `Version`, `EmptyField`) VALUES(3, 11, 3, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContainer`
--

DROP TABLE IF EXISTS `MicrositeContainer`;
CREATE TABLE `MicrositeContainer` (
  `ID` int(11) NOT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContainer`
--

INSERT INTO `MicrositeContainer` (`ID`, `GoogleAnalyticsID`) VALUES(34, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContainer_Live`
--

DROP TABLE IF EXISTS `MicrositeContainer_Live`;
CREATE TABLE `MicrositeContainer_Live` (
  `ID` int(11) NOT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContainer_Live`
--

INSERT INTO `MicrositeContainer_Live` (`ID`, `GoogleAnalyticsID`) VALUES(34, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContainer_versions`
--

DROP TABLE IF EXISTS `MicrositeContainer_versions`;
CREATE TABLE `MicrositeContainer_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContainer_versions`
--

INSERT INTO `MicrositeContainer_versions` (`ID`, `RecordID`, `Version`, `GoogleAnalyticsID`) VALUES(1, 34, 1, NULL);
INSERT INTO `MicrositeContainer_versions` (`ID`, `RecordID`, `Version`, `GoogleAnalyticsID`) VALUES(2, 34, 2, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContentItem`
--

DROP TABLE IF EXISTS `MicrositeContentItem`;
CREATE TABLE `MicrositeContentItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MicrositeContentItem') DEFAULT 'MicrositeContentItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content` mediumtext,
  `Content_en_US` mediumtext,
  `Background` enum('Color','Grey','None') DEFAULT 'Color',
  `ImagePosition` enum('Left','Right') DEFAULT 'Left',
  `LinkTitle` varchar(255) DEFAULT NULL,
  `LinkTitle_en_US` varchar(255) DEFAULT NULL,
  `LinkType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `MicrositePageID` int(11) NOT NULL DEFAULT '0',
  `MicrositeHomePageID` int(11) NOT NULL DEFAULT '0',
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContentItem`
--

INSERT INTO `MicrositeContentItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `Background`, `ImagePosition`, `LinkTitle`, `LinkTitle_en_US`, `LinkType`, `ExternalURL`, `Sort`, `MicrositePageID`, `MicrositeHomePageID`, `LinkToID`, `ImageID`) VALUES(1, 'MicrositeContentItem', '2016-10-27 12:44:15', '2016-10-27 17:01:04', 'Und für zwischendurch…', NULL, '<h4>Den kleinen Hunger oder Durst zwischendurch können Sie mit unserer Auswahl an leckeren Snacks und Getränken in unserem Caffè Dallucci stillen.</h4>\n<p>Ob der Espresso nach dem Mittagsessen oder ein leckeres belegtes Brötchen am Nachmittag – mit unserem Snacking-Angebot im Caffè Dallucci kommen Sie gut durch den Tag. Sie treffen unsere freundlichen Kollegen hier täglich von 08:00 bis 17:00 Uhr an.</p>', NULL, 'Color', 'Left', 'Snacking', NULL, 'Internal', NULL, 0, 0, 35, 38, 0);
INSERT INTO `MicrositeContentItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `Background`, `ImagePosition`, `LinkTitle`, `LinkTitle_en_US`, `LinkType`, `ExternalURL`, `Sort`, `MicrositePageID`, `MicrositeHomePageID`, `LinkToID`, `ImageID`) VALUES(2, 'MicrositeContentItem', '2016-10-27 14:30:35', '2016-10-27 17:01:49', NULL, NULL, '<p>In unseren Betriebsrestaurants "Name1 und "Name2" stehen Service und Genuss an erster Stelle! Wir bieten Ihnen täglich ein leckeres wechselndes Aktionsgericht an, lassen Sie sich von unserem Küchenleiter überraschen. Welches Gericht das sein wird, können Sie morgens direkt in der Cafeteria an der jeweiligen Tafel am Eingang entnehmen.<br><br><strong>Wir freuen uns auf Sie!</strong></p>', NULL, 'Color', 'Left', NULL, NULL, 'Internal', NULL, 0, 37, 0, 0, 66);
INSERT INTO `MicrositeContentItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `Background`, `ImagePosition`, `LinkTitle`, `LinkTitle_en_US`, `LinkType`, `ExternalURL`, `Sort`, `MicrositePageID`, `MicrositeHomePageID`, `LinkToID`, `ImageID`) VALUES(3, 'MicrositeContentItem', '2016-10-27 14:32:41', '2016-10-27 14:32:41', 'Caffé Dallucci', NULL, '<p>... steht für Genuss und Entspannung. <br>Nehmen Sie sich eine Auszeit vom Alltag!</p>', NULL, 'Color', 'Left', NULL, NULL, 'Internal', NULL, 0, 38, 0, 0, 65);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeHomePage`
--

DROP TABLE IF EXISTS `MicrositeHomePage`;
CREATE TABLE `MicrositeHomePage` (
  `ID` int(11) NOT NULL,
  `ShowMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeHomePage`
--

INSERT INTO `MicrositeHomePage` (`ID`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(35, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeHomePage_Live`
--

DROP TABLE IF EXISTS `MicrositeHomePage_Live`;
CREATE TABLE `MicrositeHomePage_Live` (
  `ID` int(11) NOT NULL,
  `ShowMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeHomePage_Live`
--

INSERT INTO `MicrositeHomePage_Live` (`ID`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(35, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeHomePage_versions`
--

DROP TABLE IF EXISTS `MicrositeHomePage_versions`;
CREATE TABLE `MicrositeHomePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ShowMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeHomePage_versions`
--

INSERT INTO `MicrositeHomePage_versions` (`ID`, `RecordID`, `Version`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(1, 33, 1, 0, 0, NULL, NULL, NULL);
INSERT INTO `MicrositeHomePage_versions` (`ID`, `RecordID`, `Version`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(2, 35, 1, 0, 0, NULL, NULL, NULL);
INSERT INTO `MicrositeHomePage_versions` (`ID`, `RecordID`, `Version`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(3, 35, 2, 0, 0, NULL, NULL, NULL);
INSERT INTO `MicrositeHomePage_versions` (`ID`, `RecordID`, `Version`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(4, 35, 3, 1, 1, NULL, NULL, NULL);
INSERT INTO `MicrositeHomePage_versions` (`ID`, `RecordID`, `Version`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(5, 35, 4, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeLinkItem`
--

DROP TABLE IF EXISTS `MicrositeLinkItem`;
CREATE TABLE `MicrositeLinkItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MicrositeLinkItem') DEFAULT 'MicrositeLinkItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `LinkType` enum('Internal','External','Download') DEFAULT 'Download',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `MicrositePageID` int(11) NOT NULL DEFAULT '0',
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  `DownloadID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeLinkItem`
--

INSERT INTO `MicrositeLinkItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `LinkType`, `ExternalURL`, `Sort`, `MicrositePageID`, `LinkToID`, `DownloadID`) VALUES(1, 'MicrositeLinkItem', '2016-10-27 14:27:20', '2016-10-27 14:28:48', 'Speiseplan KW XX', NULL, 'Download', NULL, 0, 37, 0, 77);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositePage`
--

DROP TABLE IF EXISTS `MicrositePage`;
CREATE TABLE `MicrositePage` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositePage`
--

INSERT INTO `MicrositePage` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(36, NULL, NULL, NULL);
INSERT INTO `MicrositePage` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(37, NULL, 'Aktuelle Speisepläne', NULL);
INSERT INTO `MicrositePage` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(38, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositePage_Live`
--

DROP TABLE IF EXISTS `MicrositePage_Live`;
CREATE TABLE `MicrositePage_Live` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositePage_Live`
--

INSERT INTO `MicrositePage_Live` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(36, NULL, NULL, NULL);
INSERT INTO `MicrositePage_Live` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(37, NULL, 'Aktuelle Speisepläne', NULL);
INSERT INTO `MicrositePage_Live` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(38, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositePage_versions`
--

DROP TABLE IF EXISTS `MicrositePage_versions`;
CREATE TABLE `MicrositePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositePage_versions`
--

INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(1, 36, 1, NULL, NULL, NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(2, 36, 2, NULL, NULL, NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(3, 37, 1, NULL, NULL, NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(4, 37, 2, NULL, NULL, NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(5, 37, 3, NULL, NULL, NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(6, 37, 4, NULL, 'Aktuelle Speisepläne', NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(7, 38, 1, NULL, NULL, NULL);
INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES(8, 38, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeRedirectorPage`
--

DROP TABLE IF EXISTS `MicrositeRedirectorPage`;
CREATE TABLE `MicrositeRedirectorPage` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeRedirectorPage_Live`
--

DROP TABLE IF EXISTS `MicrositeRedirectorPage_Live`;
CREATE TABLE `MicrositeRedirectorPage_Live` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeRedirectorPage_versions`
--

DROP TABLE IF EXISTS `MicrositeRedirectorPage_versions`;
CREATE TABLE `MicrositeRedirectorPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileInfoPage`
--

DROP TABLE IF EXISTS `MobileInfoPage`;
CREATE TABLE `MobileInfoPage` (
  `ID` int(11) NOT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content_en_US` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileInfoPage`
--

INSERT INTO `MobileInfoPage` (`ID`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES(28, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileInfoPage_Live`
--

DROP TABLE IF EXISTS `MobileInfoPage_Live`;
CREATE TABLE `MobileInfoPage_Live` (
  `ID` int(11) NOT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content_en_US` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileInfoPage_Live`
--

INSERT INTO `MobileInfoPage_Live` (`ID`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES(28, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileInfoPage_versions`
--

DROP TABLE IF EXISTS `MobileInfoPage_versions`;
CREATE TABLE `MobileInfoPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content_en_US` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileInfoPage_versions`
--

INSERT INTO `MobileInfoPage_versions` (`ID`, `RecordID`, `Version`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES(1, 28, 1, NULL, NULL, 0);
INSERT INTO `MobileInfoPage_versions` (`ID`, `RecordID`, `Version`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES(2, 28, 2, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage`
--

DROP TABLE IF EXISTS `MobileTemplatePage`;
CREATE TABLE `MobileTemplatePage` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL,
  `EnableDishRatings` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowLegend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `CacheLifetime` int(11) NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage`
--

INSERT INTO `MobileTemplatePage` (`ID`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES(26, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 10, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage_Live`
--

DROP TABLE IF EXISTS `MobileTemplatePage_Live`;
CREATE TABLE `MobileTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL,
  `EnableDishRatings` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowLegend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `CacheLifetime` int(11) NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage_Live`
--

INSERT INTO `MobileTemplatePage_Live` (`ID`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES(26, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 10, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage_MenuContainers`
--

DROP TABLE IF EXISTS `MobileTemplatePage_MenuContainers`;
CREATE TABLE `MobileTemplatePage_MenuContainers` (
  `ID` int(11) NOT NULL,
  `MobileTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage_MenuContainers`
--

INSERT INTO `MobileTemplatePage_MenuContainers` (`ID`, `MobileTemplatePageID`, `MenuContainerID`) VALUES(1, 26, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage_versions`
--

DROP TABLE IF EXISTS `MobileTemplatePage_versions`;
CREATE TABLE `MobileTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `TemplateName` varchar(255) DEFAULT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL,
  `EnableDishRatings` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowLegend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `CacheLifetime` int(11) NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage_versions`
--

INSERT INTO `MobileTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES(1, 26, 1, NULL, NULL, 1, 1, 1, 10, 0, 0);
INSERT INTO `MobileTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES(2, 26, 2, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 10, 0, 0);
INSERT INTO `MobileTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES(3, 26, 3, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 10, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MySiteConfigExtensionPermissions`
--

DROP TABLE IF EXISTS `MySiteConfigExtensionPermissions`;
CREATE TABLE `MySiteConfigExtensionPermissions` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MySiteConfigExtensionPermissions') DEFAULT 'MySiteConfigExtensionPermissions',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `NutritivesMapConfig`
--

DROP TABLE IF EXISTS `NutritivesMapConfig`;
CREATE TABLE `NutritivesMapConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('NutritivesMapConfig') DEFAULT 'NutritivesMapConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `NutritivesMapObject`
--

DROP TABLE IF EXISTS `NutritivesMapObject`;
CREATE TABLE `NutritivesMapObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('NutritivesMapObject') DEFAULT 'NutritivesMapObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `ExternalKey` varchar(255) DEFAULT NULL,
  `InternalKey` varchar(255) DEFAULT NULL,
  `NutritivesMapConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Permission`
--

DROP TABLE IF EXISTS `Permission`;
CREATE TABLE `Permission` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Permission') DEFAULT 'Permission',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Permission`
--

INSERT INTO `Permission` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `Arg`, `Type`, `GroupID`) VALUES(19, 'Permission', '2016-10-17 11:44:25', '2016-10-17 11:44:25', 'ADMIN', 0, 1, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PermissionRole`
--

DROP TABLE IF EXISTS `PermissionRole`;
CREATE TABLE `PermissionRole` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PermissionRole') DEFAULT 'PermissionRole',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PermissionRole`
--

INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(1, 'PermissionRole', '2016-10-17 10:58:13', '2016-10-17 11:41:25', 'Admin Global', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(2, 'PermissionRole', '2016-10-17 10:59:38', '2016-10-17 10:59:38', 'Admin System', 1);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(3, 'PermissionRole', '2016-10-17 11:08:47', '2016-10-17 11:41:46', 'Admin Mandant', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(4, 'PermissionRole', '2016-10-17 11:11:52', '2016-10-17 11:42:09', 'Admin Restaurant', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(5, 'PermissionRole', '2016-10-17 11:38:49', '2016-10-17 11:49:25', 'Editor Restaurant Gerichte Global', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(6, 'PermissionRole', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'Editor Microsite', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(7, 'PermissionRole', '2016-10-17 11:44:00', '2016-11-26 13:26:08', 'Aufgabenplanung "Import Speiseplan" Ausführung', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(8, 'PermissionRole', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'Admin Microsite', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(9, 'PermissionRole', '2016-10-17 11:53:57', '2016-10-17 11:57:08', 'Editor Restaurant Gerichte Restaurant', 0);
INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES(10, 'PermissionRole', '2016-11-26 13:26:41', '2016-11-26 13:26:41', 'Aufgabenplanung "Import Restaurant" Ausführung', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PermissionRoleCode`
--

DROP TABLE IF EXISTS `PermissionRoleCode`;
CREATE TABLE `PermissionRoleCode` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PermissionRoleCode') DEFAULT 'PermissionRoleCode',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PermissionRoleCode`
--

INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(155, 'PermissionRoleCode', '2016-10-17 10:59:38', '2016-10-17 10:59:38', 'ADMIN', 2);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1356, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1357, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1358, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CMS_ACCESS_CMSMain', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1359, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CMS_ACCESS_AssetAdmin', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1360, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_FILE_ROOT', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1361, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_FOODCATEGORY', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1362, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'SITETREE_REORGANISE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1363, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITEPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1364, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_MICROSITELINKITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1365, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_MICROSITELINKITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1366, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITELINKITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1367, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'DELETE_MICROSITELINKITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1368, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_MICROSITECONTENTITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1369, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_MICROSITECONTENTITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1370, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITECONTENTITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1371, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'DELETE_MICROSITECONTENTITEM', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1372, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITEREDIRECTORPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1373, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1374, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1375, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1376, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1377, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1378, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 6);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1379, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SUBSITESPECIALLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1380, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_SUBSITESPECIALLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1381, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_SUBSITESPECIALLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1382, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_SUBSITESPECIALLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1383, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SUBSITEALLERGEN', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1384, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_SUBSITEALLERGEN', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1385, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_SUBSITEALLERGEN', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1386, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_SUBSITEALLERGEN', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1387, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1388, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1389, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'TEMPLATEPAGE_EDIT_FOOTER', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1390, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1391, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1392, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1393, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISPLAYTEMPLATEOBJECT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1394, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_DISPLAYTEMPLATEOBJECT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1395, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_DISPLAYTEMPLATEOBJECT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1396, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISPLAYTEMPLATEOBJECT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1397, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISPLAYVIDEO', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1398, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_DISPLAYVIDEO', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1399, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_DISPLAYVIDEO', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1400, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISPLAYVIDEO', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1401, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_CMSMain', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1402, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_AssetAdmin', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1403, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_DishRatingAdmin', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1404, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_InfoMessageTemplateAdmin', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1405, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_FILE_ROOT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1406, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_SPECIALLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1407, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_DISHLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1408, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_ADDITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1409, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_ALLERGENS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1410, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_FOODCATEGORY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1411, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_FOODCATEGORY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1412, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_FOODCATEGORY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1413, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_FOODCATEGORY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1414, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISH', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1415, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_DISH', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1416, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_DISH', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1417, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISH', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1418, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_DESCRIPTION', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1419, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_DESCRIPTION', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1420, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_IMAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1421, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_IMAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1422, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELDS_NUTRITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1423, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELDS_NUTRITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1424, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1425, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1426, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1427, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1428, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1429, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1430, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1431, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1432, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1433, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1434, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1435, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1436, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1437, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1438, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1439, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1440, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_INFOTEXT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1441, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_INFOTEXT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1442, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_FOODCATEGORY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1443, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_FOODCATEGORY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1444, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_PRICES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1445, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_PRICES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1446, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELDS_SIDEDISHES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1447, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELDS_SIDEDISHES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1448, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1449, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1450, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISHRATING', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1451, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISHRATING', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1452, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'SITETREE_REORGANISE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1453, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SUBSITEDISHLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1454, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_SUBSITEDISHLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1455, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_SUBSITEDISHLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1456, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_SUBSITEDISHLABEL', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1457, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'MICROSITECONTAINER_VIEW_FIELD_GOOGLEANALYTICSID', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1458, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_MICROSITEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1459, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1460, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_MICROSITELINKITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1461, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_MICROSITELINKITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1462, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_MICROSITELINKITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1463, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITELINKITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1464, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_MICROSITECONTENTITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1465, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_MICROSITECONTENTITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1466, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_MICROSITECONTENTITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1467, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITECONTENTITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1468, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_MICROSITEREDIRECTORPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1469, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITEREDIRECTORPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1470, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1471, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1472, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_SUBSITE_DISH_PER_RESTAURANT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1473, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_SUBSITE_DISH_PER_RESTAURANT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1474, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_SUBSITE_DISH_PER_RESTAURANT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1475, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_SUBSITE_DISH_PER_RESTAURANT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1476, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_RESTAURANT_DISH_PER_RESTAURANT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1477, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_RESTAURANT_DISH_PER_RESTAURANT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1478, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1479, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1480, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1481, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1482, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1483, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1484, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1485, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1486, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1487, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1488, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1489, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1490, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1491, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1492, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1493, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1494, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1495, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1496, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1497, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1498, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1499, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1500, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1501, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1502, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1503, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1504, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1505, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1506, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1507, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1508, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1509, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1510, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1511, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1512, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1513, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1514, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1515, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1516, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1517, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1518, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1519, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_DISHES', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1520, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1521, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_IMPRINT', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1522, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1523, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1524, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_DISHVOTINGPAGE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1525, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_DISHVOTINGITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1526, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_DISHVOTINGITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1527, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_DISHVOTINGITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1528, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_DISHVOTINGITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1529, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_RECIPESUBMISSIONITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1530, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_RECIPESUBMISSIONITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1531, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_RECIPESUBMISSIONITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1532, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_RECIPESUBMISSIONITEM', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1533, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_SUBSITEADDITIVE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1534, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_SUBSITEADDITIVE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1535, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_SUBSITEADDITIVE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1536, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_SUBSITEADDITIVE', 1);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1537, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITESPECIALLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1538, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITESPECIALLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1539, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITESPECIALLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1540, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITESPECIALLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1541, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITEALLERGEN', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1542, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITEALLERGEN', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1543, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITEALLERGEN', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1544, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITEALLERGEN', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1545, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1546, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1547, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'TEMPLATEPAGE_EDIT_FOOTER', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1548, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1549, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1550, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1551, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISPLAYTEMPLATEOBJECT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1552, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISPLAYTEMPLATEOBJECT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1553, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISPLAYTEMPLATEOBJECT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1554, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISPLAYTEMPLATEOBJECT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1555, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISPLAYVIDEO', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1556, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISPLAYVIDEO', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1557, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISPLAYVIDEO', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1558, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISPLAYVIDEO', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1559, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_CMSMain', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1560, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_AssetAdmin', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1561, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_DishRatingAdmin', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1562, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_InfoMessageTemplateAdmin', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1563, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_FILE_ROOT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1564, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_SPECIALLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1565, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_DISHLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1566, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_ADDITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1567, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_ALLERGENS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1568, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_FOODCATEGORY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1569, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_FOODCATEGORY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1570, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_FOODCATEGORY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1571, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_FOODCATEGORY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1572, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISH', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1573, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISH', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1574, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISH', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1575, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISH', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1576, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_DESCRIPTION', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1577, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_DESCRIPTION', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1578, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_IMAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1579, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_IMAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1580, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELDS_NUTRITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1581, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELDS_NUTRITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1582, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1583, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1584, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1585, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1586, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1587, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1588, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1589, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1590, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1591, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1592, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1593, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1594, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1595, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1596, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1597, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1598, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_INFOTEXT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1599, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_INFOTEXT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1600, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_FOODCATEGORY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1601, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_FOODCATEGORY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1602, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_PRICES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1603, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_PRICES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1604, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELDS_SIDEDISHES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1605, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELDS_SIDEDISHES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1606, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1607, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1608, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISHRATING', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1609, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISHRATING', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1610, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'SITETREE_REORGANISE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1611, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITEDISHLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1612, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITEDISHLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1613, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITEDISHLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1614, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITEDISHLABEL', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1615, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1616, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1617, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_MICROSITELINKITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1618, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_MICROSITELINKITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1619, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITELINKITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1620, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITELINKITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1621, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_MICROSITECONTENTITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1622, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_MICROSITECONTENTITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1623, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITECONTENTITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1624, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITECONTENTITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1625, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITEREDIRECTORPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1626, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITEREDIRECTORPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1627, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1628, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1629, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITE_DISH_PER_RESTAURANT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1630, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITE_DISH_PER_RESTAURANT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1631, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITE_DISH_PER_RESTAURANT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1632, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITE_DISH_PER_RESTAURANT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1633, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_RESTAURANT_DISH_PER_RESTAURANT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1634, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_RESTAURANT_DISH_PER_RESTAURANT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1635, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1636, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1637, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1638, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1639, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1640, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1641, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1642, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1643, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1644, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1645, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1646, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1647, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1648, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1649, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1650, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1651, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1652, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1653, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1654, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1655, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1656, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_DISHES', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1657, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1658, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_IMPRINT', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1659, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1660, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1661, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SITECONFIG', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1662, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISHVOTINGPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1663, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISHVOTINGITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1664, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISHVOTINGITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1665, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISHVOTINGITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1666, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISHVOTINGITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1667, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_RECIPESUBMISSIONPAGE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1668, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_RECIPESUBMISSIONITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1669, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_RECIPESUBMISSIONITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1670, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_RECIPESUBMISSIONITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1671, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_RECIPESUBMISSIONITEM', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1672, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITEADDITIVE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1673, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITEADDITIVE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1674, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITEADDITIVE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1675, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITEADDITIVE', 3);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1676, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1677, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1678, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'TEMPLATEPAGE_EDIT_FOOTER', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1679, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1680, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1681, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1682, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISPLAYTEMPLATEOBJECT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1683, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISPLAYTEMPLATEOBJECT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1684, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISPLAYTEMPLATEOBJECT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1685, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISPLAYTEMPLATEOBJECT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1686, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISPLAYVIDEO', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1687, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISPLAYVIDEO', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1688, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISPLAYVIDEO', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1689, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISPLAYVIDEO', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1690, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CMS_ACCESS_CMSMain', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1691, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CMS_ACCESS_AssetAdmin', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1692, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CMS_ACCESS_DishRatingAdmin', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1693, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_FILE_ROOT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1694, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_FOODCATEGORY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1695, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_FOODCATEGORY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1696, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_FOODCATEGORY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1697, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_FOODCATEGORY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1698, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISH', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1699, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISH', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1700, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISH', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1701, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISH', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1702, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_DESCRIPTION', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1703, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_DESCRIPTION', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1704, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_IMAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1705, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_IMAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1706, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELDS_NUTRITIVES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1707, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELDS_NUTRITIVES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1708, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1709, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1710, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1711, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1712, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1713, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1714, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1715, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1716, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1717, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1718, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1719, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1720, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1721, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1722, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1723, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1724, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_INFOTEXT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1725, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_INFOTEXT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1726, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_FOODCATEGORY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1727, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_FOODCATEGORY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1728, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_PRICES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1729, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_PRICES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1730, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELDS_SIDEDISHES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1731, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELDS_SIDEDISHES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1732, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1733, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1734, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISHRATING', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1735, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISHRATING', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1736, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'SITETREE_REORGANISE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1737, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1738, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1739, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_MICROSITELINKITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1740, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_MICROSITELINKITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1741, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITELINKITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1742, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITELINKITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1743, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_MICROSITECONTENTITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1744, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_MICROSITECONTENTITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1745, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITECONTENTITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1746, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITECONTENTITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1747, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITEREDIRECTORPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1748, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITEREDIRECTORPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1749, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1750, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1751, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_SUBSITE_DISH_PER_RESTAURANT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1752, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_SUBSITE_DISH_PER_RESTAURANT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1753, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_SUBSITE_DISH_PER_RESTAURANT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1754, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_SUBSITE_DISH_PER_RESTAURANT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1755, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_RESTAURANT_DISH_PER_RESTAURANT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1756, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_RESTAURANT_DISH_PER_RESTAURANT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1757, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1758, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1759, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1760, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1761, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1762, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1763, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1764, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1765, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1766, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1767, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1768, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1769, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1770, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1771, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1772, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1773, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1774, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1775, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1776, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1777, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1778, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_DISHES', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1779, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1780, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_IMPRINT', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1781, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1782, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1783, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISHVOTINGPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1784, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISHVOTINGITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1785, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISHVOTINGITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1786, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISHVOTINGITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1787, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISHVOTINGITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1788, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_RECIPESUBMISSIONPAGE', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1789, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_RECIPESUBMISSIONITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1790, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_RECIPESUBMISSIONITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1791, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_RECIPESUBMISSIONITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1792, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_RECIPESUBMISSIONITEM', 4);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1804, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1805, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1806, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CMS_ACCESS_CMSMain', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1807, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CMS_ACCESS_AssetAdmin', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1808, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_FILE_ROOT', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1809, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_FOODCATEGORY', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1810, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_DISH', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1811, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_DISH', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1812, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_DISH', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1813, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_DESCRIPTION', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1814, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_DESCRIPTION', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1815, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_IMAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1816, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_IMAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1817, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELDS_NUTRITIVES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1818, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELDS_NUTRITIVES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1819, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1820, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1821, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1822, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1823, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1824, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1825, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1826, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1827, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1828, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1829, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1830, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1831, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1832, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1833, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1834, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1835, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_INFOTEXT', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1836, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_INFOTEXT', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1837, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_FOODCATEGORY', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1838, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_FOODCATEGORY', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1839, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_PRICES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1840, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_PRICES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1841, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELDS_SIDEDISHES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1842, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELDS_SIDEDISHES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1843, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1844, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1845, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'SITETREE_REORGANISE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1846, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITEPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1847, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_MICROSITEPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1848, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_MICROSITELINKITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1849, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_MICROSITELINKITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1850, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITELINKITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1851, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_MICROSITELINKITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1852, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_MICROSITECONTENTITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1853, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_MICROSITECONTENTITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1854, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITECONTENTITEM', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1855, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITEREDIRECTORPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1856, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_MICROSITEREDIRECTORPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1857, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1858, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1859, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1860, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1861, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1862, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1863, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1864, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1865, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1866, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1867, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1868, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1869, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1870, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1871, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'TEMPLATEPAGE_EDIT_FOOTER', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1872, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1873, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1874, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_DISPLAYTEMPLATEOBJECT', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1875, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_DISPLAYTEMPLATEOBJECT', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1876, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_DISPLAYVIDEO', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1877, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_DISPLAYVIDEO', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1878, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CREATE_DISPLAYVIDEO', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1879, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DELETE_DISPLAYVIDEO', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1880, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CMS_ACCESS_CMSMain', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1881, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CMS_ACCESS_AssetAdmin', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1882, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CMS_ACCESS_DishRatingAdmin', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1883, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_FILE_ROOT', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1884, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_FOODCATEGORY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1885, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_FOODCATEGORY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1886, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CREATE_FOODCATEGORY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1887, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_DISH', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1888, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_DISH', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1889, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CREATE_DISH', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1890, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELD_DESCRIPTION', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1891, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELD_DESCRIPTION', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1892, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELD_IMAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1893, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELD_IMAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1894, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELDS_NUTRITIVES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1895, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELDS_NUTRITIVES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1896, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1897, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1898, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1899, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1900, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1901, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1902, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1903, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1904, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1905, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1906, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1907, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1908, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1909, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1910, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1911, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1912, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_INFOTEXT', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1913, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_INFOTEXT', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1914, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_FOODCATEGORY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1915, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_FOODCATEGORY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1916, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_PRICES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1917, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_PRICES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1918, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELDS_SIDEDISHES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1919, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELDS_SIDEDISHES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1920, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1921, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1922, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_DISHRATING', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1923, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'SITETREE_REORGANISE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1924, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1925, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1926, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_MICROSITELINKITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1927, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_MICROSITELINKITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1928, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITELINKITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1929, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITELINKITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1930, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_MICROSITECONTENTITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1931, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_MICROSITECONTENTITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1932, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITECONTENTITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1933, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITECONTENTITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1934, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITEREDIRECTORPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1935, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITEREDIRECTORPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1936, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1937, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1938, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1939, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1940, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1941, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1942, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1943, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1944, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1945, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1946, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1947, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1948, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1949, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1950, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1951, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1952, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1953, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1954, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1955, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1956, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1957, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1958, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1959, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1960, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1961, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_DAILYMENUPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1962, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_DISHVOTINGITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1963, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_DISHVOTINGITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1964, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_DISHVOTINGITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1965, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_DISHVOTINGITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1966, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_RECIPESUBMISSIONPAGE', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1967, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_RECIPESUBMISSIONITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1968, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_RECIPESUBMISSIONITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1969, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_RECIPESUBMISSIONITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(1970, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_RECIPESUBMISSIONITEM', 5);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2059, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2060, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2061, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'TEMPLATEPAGE_EDIT_FOOTER', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2062, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2063, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2064, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISPLAYTEMPLATEOBJECT', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2065, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_DISPLAYTEMPLATEOBJECT', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2066, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISPLAYVIDEO', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2067, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_DISPLAYVIDEO', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2068, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_DISPLAYVIDEO', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2069, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_DISPLAYVIDEO', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2070, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CMS_ACCESS_CMSMain', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2071, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CMS_ACCESS_AssetAdmin', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2072, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CMS_ACCESS_DishRatingAdmin', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2073, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_FILE_ROOT', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2074, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_FOODCATEGORY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2075, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_FOODCATEGORY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2076, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_FOODCATEGORY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2077, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISH', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2078, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_DESCRIPTION', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2079, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_IMAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2080, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELDS_NUTRITIVES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2081, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2082, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2083, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2084, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2085, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2086, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2087, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2088, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2089, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_INFOTEXT', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2090, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_FOODCATEGORY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2091, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELD_FOODCATEGORY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2092, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_PRICES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2093, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELD_PRICES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2094, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELDS_SIDEDISHES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2095, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELDS_SIDEDISHES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2096, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2097, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2098, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISHRATING', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2099, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'SITETREE_REORGANISE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2100, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2101, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2102, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_MICROSITELINKITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2103, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_MICROSITELINKITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2104, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITELINKITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2105, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITELINKITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2106, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_MICROSITECONTENTITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2107, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_MICROSITECONTENTITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2108, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITECONTENTITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2109, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITECONTENTITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2110, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITEREDIRECTORPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2111, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITEREDIRECTORPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2112, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2113, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2114, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2115, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2116, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2117, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2118, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2119, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2120, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2121, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2122, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2123, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2124, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2125, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2126, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2127, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2128, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2129, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2130, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2131, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2132, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2133, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2134, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2135, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2136, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2137, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_DISHVOTINGPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2138, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISHVOTINGITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2139, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_DISHVOTINGITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2140, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_DISHVOTINGITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2141, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_DISHVOTINGITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2142, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_RECIPESUBMISSIONPAGE', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2143, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_RECIPESUBMISSIONITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2144, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_RECIPESUBMISSIONITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2145, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_RECIPESUBMISSIONITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2146, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_RECIPESUBMISSIONITEM', 9);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2147, 'PermissionRoleCode', '2016-11-26 13:26:08', '2016-11-26 13:26:08', 'VIEW_IMPORTMENUXMLCONFIG', 7);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2148, 'PermissionRoleCode', '2016-11-26 13:26:08', '2016-11-26 13:26:08', 'RUN_SINGLE_IMPORTMENUXMLTASK', 7);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2149, 'PermissionRoleCode', '2016-11-26 13:26:41', '2016-11-26 13:26:41', 'VIEW_IMPORTRESTAURANTCONFIG', 10);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2150, 'PermissionRoleCode', '2016-11-26 13:26:41', '2016-11-26 13:26:41', 'RUN_SINGLE_IMPORTRESTAURANTTASK', 10);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES(2151, 'PermissionRoleCode', '2016-11-26 13:26:41', '2016-11-26 13:26:41', 'CMS_ACCESS_ProcessTaskAdmin', 10);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintHtmlTemplatePage`
--

DROP TABLE IF EXISTS `PrintHtmlTemplatePage`;
CREATE TABLE `PrintHtmlTemplatePage` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `HeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintHtmlTemplatePage_Live`
--

DROP TABLE IF EXISTS `PrintHtmlTemplatePage_Live`;
CREATE TABLE `PrintHtmlTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `HeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintHtmlTemplatePage_versions`
--

DROP TABLE IF EXISTS `PrintHtmlTemplatePage_versions`;
CREATE TABLE `PrintHtmlTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `TemplateName` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `HeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintManagerConfig`
--

DROP TABLE IF EXISTS `PrintManagerConfig`;
CREATE TABLE `PrintManagerConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PrintManagerConfig') DEFAULT 'PrintManagerConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Headline` mediumtext,
  `Infotext` mediumtext,
  `FootNote` mediumtext,
  `AuthKey` varchar(25) DEFAULT NULL,
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0',
  `PrintManagerTemplateID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintManagerConfig_Dishes`
--

DROP TABLE IF EXISTS `PrintManagerConfig_Dishes`;
CREATE TABLE `PrintManagerConfig_Dishes` (
  `ID` int(11) NOT NULL,
  `PrintManagerConfigID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintManagerTemplate`
--

DROP TABLE IF EXISTS `PrintManagerTemplate`;
CREATE TABLE `PrintManagerTemplate` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PrintManagerTemplate') DEFAULT 'PrintManagerTemplate',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `Sort` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintTemplatePage`
--

DROP TABLE IF EXISTS `PrintTemplatePage`;
CREATE TABLE `PrintTemplatePage` (
  `ID` int(11) NOT NULL,
  `ControllerName` varchar(255) DEFAULT NULL,
  `WeeklyTableHeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintTemplatePage`
--

INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(13, 'print_weekly_table_detail_de_de', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(14, 'print_weekly_table_detail_en_us', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(15, 'print_category_dina4_portrait', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(16, 'print_category_dina4_landscape', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(17, 'print_category_dina5', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(18, 'print_category_dina7', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(19, 'print_temperature_table', 0);
INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(20, 'print_production_table', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintTemplatePage_Live`
--

DROP TABLE IF EXISTS `PrintTemplatePage_Live`;
CREATE TABLE `PrintTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `ControllerName` varchar(255) DEFAULT NULL,
  `WeeklyTableHeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintTemplatePage_Live`
--

INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(13, 'print_weekly_table_detail_de_de', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(14, 'print_weekly_table_detail_en_us', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(15, 'print_category_dina4_portrait', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(16, 'print_category_dina4_landscape', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(17, 'print_category_dina5', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(18, 'print_category_dina7', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(19, 'print_temperature_table', 0);
INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(20, 'print_production_table', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintTemplatePage_versions`
--

DROP TABLE IF EXISTS `PrintTemplatePage_versions`;
CREATE TABLE `PrintTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ControllerName` varchar(255) DEFAULT NULL,
  `WeeklyTableHeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintTemplatePage_versions`
--

INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(1, 13, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(2, 13, 2, 'page_13', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(3, 13, 3, 'print_weekly_table_detail_de_de', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(4, 14, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(5, 14, 2, 'page_14', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(6, 14, 3, 'print_weekly_table_detail_en_us', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(7, 15, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(8, 15, 2, 'page_15', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(9, 15, 3, 'print_category_dina4_portrait', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(10, 16, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(11, 16, 2, 'page_16', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(12, 16, 3, 'print_category_dina4_landscape', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(13, 17, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(14, 17, 2, 'page_17', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(15, 17, 3, 'print_category_dina5', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(16, 18, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(17, 18, 2, 'page_18', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(18, 18, 3, 'print_category_dina7', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(19, 19, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(20, 19, 2, 'page_19', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(21, 19, 3, 'print_temperature_table', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(22, 13, 4, 'print_weekly_table_detail_de_de', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(23, 14, 4, 'print_weekly_table_detail_en_us', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(24, 19, 4, 'print_temperature_table', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(25, 20, 1, NULL, 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(26, 20, 2, 'page_20', 0);
INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES(27, 20, 3, 'print_production_table', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ProcessTaskObject`
--

DROP TABLE IF EXISTS `ProcessTaskObject`;
CREATE TABLE `ProcessTaskObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('ProcessTaskObject','ExportConfig','ExportDisplayMenuXmlConfig','ExportMenuHtmlConfig','ExportMenuXmlConfig','ImportConfig','ImportMenuXmlConfig','ImportRestaurantConfig','ImportVisitorCounterConfig') DEFAULT 'ProcessTaskObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Status` enum('Running','Error','Finished') DEFAULT 'Finished',
  `LastStarted` datetime DEFAULT NULL,
  `LastRun` datetime DEFAULT NULL,
  `Timeout` int(11) NOT NULL DEFAULT '0',
  `Schedule` varchar(255) DEFAULT NULL,
  `Published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionItem`
--

DROP TABLE IF EXISTS `RecipeSubmissionItem`;
CREATE TABLE `RecipeSubmissionItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('RecipeSubmissionItem') DEFAULT 'RecipeSubmissionItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Recipe` mediumtext,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `RecipeSubmissionPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionPage`
--

DROP TABLE IF EXISTS `RecipeSubmissionPage`;
CREATE TABLE `RecipeSubmissionPage` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecipeSubmissionPage`
--

INSERT INTO `RecipeSubmissionPage` (`ID`, `VotingDate`, `Publish`) VALUES(32, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionPage_Live`
--

DROP TABLE IF EXISTS `RecipeSubmissionPage_Live`;
CREATE TABLE `RecipeSubmissionPage_Live` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecipeSubmissionPage_Live`
--

INSERT INTO `RecipeSubmissionPage_Live` (`ID`, `VotingDate`, `Publish`) VALUES(32, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionPage_versions`
--

DROP TABLE IF EXISTS `RecipeSubmissionPage_versions`;
CREATE TABLE `RecipeSubmissionPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecipeSubmissionPage_versions`
--

INSERT INTO `RecipeSubmissionPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(1, 32, 1, NULL, 0);
INSERT INTO `RecipeSubmissionPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(2, 32, 2, NULL, 0);
INSERT INTO `RecipeSubmissionPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(3, 32, 3, '2016-10-27', 0);
INSERT INTO `RecipeSubmissionPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(4, 32, 4, '2016-10-27', 1);
INSERT INTO `RecipeSubmissionPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES(5, 32, 5, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecurringDayOfWeek`
--

DROP TABLE IF EXISTS `RecurringDayOfWeek`;
CREATE TABLE `RecurringDayOfWeek` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('RecurringDayOfWeek') DEFAULT 'RecurringDayOfWeek',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Value` int(11) NOT NULL DEFAULT '0',
  `Skey` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecurringDayOfWeek`
--

INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(1, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 0, 'So');
INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(2, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 1, 'Mo');
INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(3, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 2, 'Di');
INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(4, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 3, 'Mi');
INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(5, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 4, 'Do');
INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(6, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 5, 'Fr');
INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES(7, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 6, 'Sa');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RedirectorPage`
--

DROP TABLE IF EXISTS `RedirectorPage`;
CREATE TABLE `RedirectorPage` (
  `ID` int(11) NOT NULL,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RedirectorPage`
--

INSERT INTO `RedirectorPage` (`ID`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(6, 'Internal', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RedirectorPage_Live`
--

DROP TABLE IF EXISTS `RedirectorPage_Live`;
CREATE TABLE `RedirectorPage_Live` (
  `ID` int(11) NOT NULL,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RedirectorPage_Live`
--

INSERT INTO `RedirectorPage_Live` (`ID`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(6, 'Internal', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RedirectorPage_versions`
--

DROP TABLE IF EXISTS `RedirectorPage_versions`;
CREATE TABLE `RedirectorPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RedirectorPage_versions`
--

INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(1, 6, 1, 'Internal', NULL, 0);
INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(2, 6, 2, 'Internal', NULL, 0);
INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(3, 6, 3, 'Internal', NULL, 0);
INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(4, 6, 4, 'Internal', NULL, 7);
INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(5, 6, 5, 'Internal', NULL, 7);
INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES(6, 6, 6, 'Internal', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantDish`
--

DROP TABLE IF EXISTS `RestaurantDish`;
CREATE TABLE `RestaurantDish` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('RestaurantDish') DEFAULT 'RestaurantDish',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Price` float NOT NULL DEFAULT '0',
  `PriceExtern` float NOT NULL DEFAULT '0',
  `PriceSmall` float NOT NULL DEFAULT '0',
  `PriceExternSmall` float NOT NULL DEFAULT '0',
  `ShowOnDisplayTemplatePage` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `ShowOnPrintTemplatePage` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `ShowOnMobileTemplatePage` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SideDish1ID` int(11) NOT NULL DEFAULT '0',
  `SideDish2ID` int(11) NOT NULL DEFAULT '0',
  `SideDish3ID` int(11) NOT NULL DEFAULT '0',
  `SideDish4ID` int(11) NOT NULL DEFAULT '0',
  `SideDish5ID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantDish`
--

INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(1, 'RestaurantDish', '2016-10-27 11:43:03', '2016-10-27 11:43:03', 0.6, 1.4, 0, 0, 1, 1, 1, 7, 1, 1, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(2, 'RestaurantDish', '2016-10-27 11:43:29', '2016-10-27 11:43:29', 5.6, 7.2, 0, 0, 1, 1, 1, 7, 2, 2, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(3, 'RestaurantDish', '2016-10-27 11:44:43', '2016-10-27 11:48:05', 3.2, 5.1, 0, 0, 1, 1, 1, 7, 4, 3, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(4, 'RestaurantDish', '2016-10-27 11:46:34', '2016-10-27 11:46:34', 2.7, 3.9, 0, 0, 1, 1, 1, 7, 5, 4, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(5, 'RestaurantDish', '2016-10-27 11:49:05', '2016-10-27 11:50:09', 4.5, 7.2, 0, 0, 1, 1, 1, 7, 6, 5, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(6, 'RestaurantDish', '2016-10-27 11:49:53', '2016-10-27 17:07:41', 4.2, 6.4, 0, 0, 1, 1, 1, 7, 3, 6, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(7, 'RestaurantDish', '2016-10-27 11:50:37', '2016-10-27 11:50:37', 1.6, 0, 0, 0, 1, 1, 1, 7, 7, 7, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(8, 'RestaurantDish', '2016-10-27 11:51:04', '2016-10-27 11:51:04', 1.7, 0, 0, 0, 1, 1, 1, 7, 7, 8, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(9, 'RestaurantDish', '2016-10-27 17:31:25', '2016-10-27 17:31:25', 0.6, 0.6, 0, 0, 1, 1, 1, 7, 7, 9, 0, 0, 0, 0, 0);
INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES(10, 'RestaurantDish', '2016-10-27 17:32:11', '2016-10-27 17:32:11', 1.1, 1.8, 0, 0, 1, 1, 1, 7, 9, 10, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantPage`
--

DROP TABLE IF EXISTS `RestaurantPage`;
CREATE TABLE `RestaurantPage` (
  `ID` int(11) NOT NULL,
  `Lang_en_US` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `OpeningHours` mediumtext,
  `Imprint` mediumtext,
  `ContactCompanyName` mediumtext,
  `ContactPersonName` varchar(50) DEFAULT NULL,
  `ContactStreetAddress` mediumtext,
  `ContactPhone` varchar(50) DEFAULT NULL,
  `ContactFax` varchar(50) DEFAULT NULL,
  `ContactEmail` varchar(50) DEFAULT NULL,
  `ContactCopyrightCompany` varchar(255) DEFAULT NULL,
  `RestaurantLogoID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantPage`
--

INSERT INTO `RestaurantPage` (`ID`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(7, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href="http://www.dubbelspaeth.de" target="_blank">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href="http://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href="http://www.google.com/analytics/terms/de.html" target="_blank">hier</a> bzw. <a href="http://www.google.com/analytics/terms/de.html" target="_blank">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantPage_Live`
--

DROP TABLE IF EXISTS `RestaurantPage_Live`;
CREATE TABLE `RestaurantPage_Live` (
  `ID` int(11) NOT NULL,
  `Lang_en_US` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `OpeningHours` mediumtext,
  `Imprint` mediumtext,
  `ContactCompanyName` mediumtext,
  `ContactPersonName` varchar(50) DEFAULT NULL,
  `ContactStreetAddress` mediumtext,
  `ContactPhone` varchar(50) DEFAULT NULL,
  `ContactFax` varchar(50) DEFAULT NULL,
  `ContactEmail` varchar(50) DEFAULT NULL,
  `ContactCopyrightCompany` varchar(255) DEFAULT NULL,
  `RestaurantLogoID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantPage_Live`
--

INSERT INTO `RestaurantPage_Live` (`ID`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(7, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href="http://www.dubbelspaeth.de" target="_blank">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href="http://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href="http://www.google.com/analytics/terms/de.html" target="_blank">hier</a> bzw. <a href="http://www.google.com/analytics/terms/de.html" target="_blank">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantPage_versions`
--

DROP TABLE IF EXISTS `RestaurantPage_versions`;
CREATE TABLE `RestaurantPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Lang_en_US` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `OpeningHours` mediumtext,
  `Imprint` mediumtext,
  `ContactCompanyName` mediumtext,
  `ContactPersonName` varchar(50) DEFAULT NULL,
  `ContactStreetAddress` mediumtext,
  `ContactPhone` varchar(50) DEFAULT NULL,
  `ContactFax` varchar(50) DEFAULT NULL,
  `ContactEmail` varchar(50) DEFAULT NULL,
  `ContactCopyrightCompany` varchar(255) DEFAULT NULL,
  `RestaurantLogoID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantPage_versions`
--

INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(1, 7, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(2, 7, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(3, 7, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(4, 7, 4, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(5, 7, 5, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', NULL, 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);
INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES(6, 7, 6, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href="http://www.dubbelspaeth.de" target="_blank">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href="http://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href="http://www.google.com/analytics/terms/de.html" target="_blank">hier</a> bzw. <a href="http://www.google.com/analytics/terms/de.html" target="_blank">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig`
--

DROP TABLE IF EXISTS `SiteConfig`;
CREATE TABLE `SiteConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SiteConfig') DEFAULT 'SiteConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Tagline` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `EnableIndividualDishesPerRestaurant` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `KeepDaysDailyMenuPages` int(11) NOT NULL DEFAULT '0',
  `AllergenDisclaimer` mediumtext,
  `AllergenDisclaimer_en_US` mediumtext,
  `NutritivesUnit` varchar(255) DEFAULT NULL,
  `NutritivesUnit_en_US` varchar(255) DEFAULT NULL,
  `Currency` varchar(20) DEFAULT NULL,
  `DefaultFolderType` enum('Restaurant','Mandant','Global') DEFAULT 'Restaurant',
  `DefaultFolderRestaurantLogo` varchar(50) DEFAULT NULL,
  `DefaultFolderDishImage` varchar(50) DEFAULT NULL,
  `DefaultFolderInfoMessage` varchar(50) DEFAULT NULL,
  `DefaultFolderPrint` varchar(50) DEFAULT NULL,
  `DefaultFolderMicrosite` varchar(50) DEFAULT NULL,
  `DefaultFolderFoodCategory` varchar(50) DEFAULT NULL,
  `DefaultFolderDishLabel` varchar(50) DEFAULT NULL,
  `DefaultFolderSpecialLabel` varchar(50) DEFAULT NULL,
  `DishesAutoCompleterResultsLimit` int(11) NOT NULL DEFAULT '0',
  `EnableDishSearchForImportID` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowDishImportID` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `EnablePrintManager` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteConfig`
--

INSERT INTO `SiteConfig` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Tagline`, `Theme`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `EnableIndividualDishesPerRestaurant`, `KeepDaysDailyMenuPages`, `AllergenDisclaimer`, `AllergenDisclaimer_en_US`, `NutritivesUnit`, `NutritivesUnit_en_US`, `Currency`, `DefaultFolderType`, `DefaultFolderRestaurantLogo`, `DefaultFolderDishImage`, `DefaultFolderInfoMessage`, `DefaultFolderPrint`, `DefaultFolderMicrosite`, `DefaultFolderFoodCategory`, `DefaultFolderDishLabel`, `DefaultFolderSpecialLabel`, `DishesAutoCompleterResultsLimit`, `EnableDishSearchForImportID`, `ShowDishImportID`, `SubsiteID`, `EnablePrintManager`) VALUES(1, 'SiteConfig', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'DSS Konfiguration', '', NULL, 'Anyone', 'LoggedInUsers', 'LoggedInUsers', 0, 14, NULL, NULL, 'pro Portion', 'per serving', 'EUR', 'Restaurant', 'Logos', 'Fotos-Gerichte', 'News', 'Druck', 'Microsite', 'Essenskategorien', 'Kennzeichnungen', 'Aktionslabels', 25, 0, 0, 0, 0);
INSERT INTO `SiteConfig` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Tagline`, `Theme`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `EnableIndividualDishesPerRestaurant`, `KeepDaysDailyMenuPages`, `AllergenDisclaimer`, `AllergenDisclaimer_en_US`, `NutritivesUnit`, `NutritivesUnit_en_US`, `Currency`, `DefaultFolderType`, `DefaultFolderRestaurantLogo`, `DefaultFolderDishImage`, `DefaultFolderInfoMessage`, `DefaultFolderPrint`, `DefaultFolderMicrosite`, `DefaultFolderFoodCategory`, `DefaultFolderDishLabel`, `DefaultFolderSpecialLabel`, `DishesAutoCompleterResultsLimit`, `EnableDishSearchForImportID`, `ShowDishImportID`, `SubsiteID`, `EnablePrintManager`) VALUES(2, 'SiteConfig', '2016-10-27 09:54:24', '2016-10-27 09:55:18', 'VORLAGE', 'Ihr Websiteslogan', 'default-theme', 'Anyone', 'OnlyTheseUsers', 'OnlyTheseUsers', 0, 0, NULL, NULL, 'pro Portion', 'per serving', 'EUR', 'Restaurant', 'Logos', 'Fotos-Gerichte', 'News', 'Druck', 'Microsite', 'Essenskategorien', 'Kennzeichnungen', 'Aktionslabels', 25, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig_CreateTopLevelGroups`
--

DROP TABLE IF EXISTS `SiteConfig_CreateTopLevelGroups`;
CREATE TABLE `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteConfig_CreateTopLevelGroups`
--

INSERT INTO `SiteConfig_CreateTopLevelGroups` (`ID`, `SiteConfigID`, `GroupID`) VALUES(1, 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig_EditorGroups`
--

DROP TABLE IF EXISTS `SiteConfig_EditorGroups`;
CREATE TABLE `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteConfig_EditorGroups`
--

INSERT INTO `SiteConfig_EditorGroups` (`ID`, `SiteConfigID`, `GroupID`) VALUES(1, 2, 2);
INSERT INTO `SiteConfig_EditorGroups` (`ID`, `SiteConfigID`, `GroupID`) VALUES(2, 2, 3);
INSERT INTO `SiteConfig_EditorGroups` (`ID`, `SiteConfigID`, `GroupID`) VALUES(3, 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig_ViewerGroups`
--

DROP TABLE IF EXISTS `SiteConfig_ViewerGroups`;
CREATE TABLE `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree`
--

DROP TABLE IF EXISTS `SiteTree`;
CREATE TABLE `SiteTree` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SiteTree','Page','ErrorPage','RedirectorPage','MicrositeRedirectorPage','VirtualPage','SubsitesVirtualPage','DailyMenuPage','DishVotingContainer','DishVotingPage','MenuContainer','MicrositeContainer','MicrositePage','MicrositeNewsPage','MobileInfoPage','RecipeSubmissionContainer','RecipeSubmissionPage','RestaurantPage','TemplatePage','DisplayTemplatePage','DisplayMultiTemplatePage','DisplayTemplatePageLandscape','DisplayTemplatePagePortrait','MicrositeHomePage','MobileTemplatePage','DishRatingTemplatePage','PrintHtmlTemplatePage','PrintTemplatePage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `HomepageForDomain` varchar(100) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree`
--

INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(1, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:52:27', 'home', 'Digital Signage System', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 0, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(4, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(5, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(6, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 14:50:59', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(7, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 17:07:53', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(9, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:52:06', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 2, 1, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(10, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:53:03', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 2, 1, 0);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(11, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:29:33', 'speiseplaene', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(12, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:29', '2016-01-01', 'Freitag, 01.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(13, 'PrintTemplatePage', '2016-10-27 11:56:08', '2016-10-27 12:07:42', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(14, 'PrintTemplatePage', '2016-10-27 12:01:27', '2016-10-27 12:07:57', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(15, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:04:00', 'druck-a4-hoch', 'Druck A4 hoch', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(16, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:05:01', 'druck-a4-quer', 'Druck A4 quer', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(17, 'PrintTemplatePage', '2016-10-27 12:05:25', '2016-10-27 12:06:03', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(18, 'PrintTemplatePage', '2016-10-27 12:06:20', '2016-10-27 12:06:46', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(19, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 17:16:19', 'druck-temperaturprotokoll', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(20, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'druck-produktionsblatt', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(21, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:14:13', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(22, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:57', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(23, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 17:19:11', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(24, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2016-10-27 12:21:03', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(25, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2016-10-27 12:22:47', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(26, 'MobileTemplatePage', '2016-10-27 12:23:27', '2016-10-27 12:48:17', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(28, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:34', 'app-auf-dem-home-bilschirm-speichern', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt "Zum Startbildschirm hinzufügen" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 26);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(29, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:33', 'voting-gerichte', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(30, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:11', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 29);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(31, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:56', 'wunschgericht', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(32, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 17:23:20', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 31);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(34, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:53', 'microsite', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(35, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 17:29:35', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(36, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 13:15:17', 'aktuelles', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(37, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:24', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34);
INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(38, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:55', 'snacking', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_CrossSubsiteLinkTracking`
--

DROP TABLE IF EXISTS `SiteTree_CrossSubsiteLinkTracking`;
CREATE TABLE `SiteTree_CrossSubsiteLinkTracking` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_EditorGroups`
--

DROP TABLE IF EXISTS `SiteTree_EditorGroups`;
CREATE TABLE `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree_EditorGroups`
--

INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(1, 6, 1);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(2, 7, 2);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(3, 7, 3);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(4, 7, 4);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(5, 7, 1);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(6, 7, 5);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(7, 9, 1);
INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES(8, 10, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_ImageTracking`
--

DROP TABLE IF EXISTS `SiteTree_ImageTracking`;
CREATE TABLE `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_LinkTracking`
--

DROP TABLE IF EXISTS `SiteTree_LinkTracking`;
CREATE TABLE `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_Live`
--

DROP TABLE IF EXISTS `SiteTree_Live`;
CREATE TABLE `SiteTree_Live` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SiteTree','Page','ErrorPage','RedirectorPage','MicrositeRedirectorPage','VirtualPage','SubsitesVirtualPage','DailyMenuPage','DishVotingContainer','DishVotingPage','MenuContainer','MicrositeContainer','MicrositePage','MicrositeNewsPage','MobileInfoPage','RecipeSubmissionContainer','RecipeSubmissionPage','RestaurantPage','TemplatePage','DisplayTemplatePage','DisplayMultiTemplatePage','DisplayTemplatePageLandscape','DisplayTemplatePagePortrait','MicrositeHomePage','MobileTemplatePage','DishRatingTemplatePage','PrintHtmlTemplatePage','PrintTemplatePage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `HomepageForDomain` varchar(100) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree_Live`
--

INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(1, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:53:20', 'home', 'Digital Signage System', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 0, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(4, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:53:20', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(5, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:53:20', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(6, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 17:07:53', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(7, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 17:07:53', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(9, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:52:06', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 2, 1, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(10, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:53:03', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 2, 1, 0);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(11, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:29:34', 'speiseplaene', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(12, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:29', '2016-01-01', 'Freitag, 01.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(13, 'PrintTemplatePage', '2016-10-27 11:56:08', '2016-10-27 12:07:42', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(14, 'PrintTemplatePage', '2016-10-27 12:01:27', '2016-10-27 12:07:57', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(15, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:04:00', 'druck-a4-hoch', 'Druck A4 hoch', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(16, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:05:01', 'druck-a4-quer', 'Druck A4 quer', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(17, 'PrintTemplatePage', '2016-10-27 12:05:25', '2016-10-27 12:06:03', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(18, 'PrintTemplatePage', '2016-10-27 12:06:20', '2016-10-27 12:06:46', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(19, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 17:16:19', 'druck-temperaturprotokoll', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(20, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'druck-produktionsblatt', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(21, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:14:13', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(22, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:57', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(23, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 17:19:11', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(24, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2016-10-27 12:21:03', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(25, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2016-10-27 12:22:47', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(26, 'MobileTemplatePage', '2016-10-27 12:23:27', '2016-10-27 12:48:17', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(28, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:34', 'app-auf-dem-home-bilschirm-speichern', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt "Zum Startbildschirm hinzufügen" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 26);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(29, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:33', 'voting-gerichte', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(30, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:11', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 29);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(31, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:56', 'wunschgericht', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(32, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 17:23:20', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 31);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(34, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:53', 'microsite', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(35, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 17:29:35', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(36, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 13:15:17', 'aktuelles', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(37, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:24', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34);
INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES(38, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:55', 'snacking', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_versions`
--

DROP TABLE IF EXISTS `SiteTree_versions`;
CREATE TABLE `SiteTree_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SiteTree','Page','ErrorPage','RedirectorPage','MicrositeRedirectorPage','VirtualPage','SubsitesVirtualPage','DailyMenuPage','DishVotingContainer','DishVotingPage','MenuContainer','MicrositeContainer','MicrositePage','MicrositeNewsPage','MobileInfoPage','RecipeSubmissionContainer','RecipeSubmissionPage','RestaurantPage','TemplatePage','DisplayTemplatePage','DisplayMultiTemplatePage','DisplayTemplatePageLandscape','DisplayTemplatePagePortrait','MicrositeHomePage','MobileTemplatePage','DishRatingTemplatePage','PrintHtmlTemplatePage','PrintTemplatePage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `HomepageForDomain` varchar(100) DEFAULT NULL,
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree_versions`
--

INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(1, 1, 1, 1, 0, 0, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'home', 'Startseite', NULL, '<p>Willkommen bei SilverStripe! Dies ist die Standard-Startseite. Sie können diese Seite bearbeiten, indem Sie <a href="admin/">das CMS</a> öffnen. Sie können außerdem die  <a href="http://doc.silverstripe.com">Entwicker-Dokumentation</a> oder die <a href="http://doc.silverstripe.com/doku.php?id=tutorials">Einführungskurse und Tutorien</a> aufrufen.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(2, 2, 1, 1, 0, 0, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'ueber-uns', 'Über uns', NULL, '<p>Sie können diese Seite mit Ihren eigenen Inhalten füllen, oder sie löschen und Ihre eigenen Seiten erstellen.<br /></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(3, 3, 1, 1, 0, 0, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'kontakt', 'Kontakt', NULL, '<p>Sie können diese Seite mit Ihren eigenen Inhalten füllen, oder sie löschen und Ihre eigenen Seiten erstellen.<br /></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(4, 4, 1, 1, 0, 0, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(5, 5, 1, 1, 0, 0, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(6, 1, 2, 1, 1, 1, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:52:27', 'home', 'Digital Signage System', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(7, 6, 1, 0, 1, 0, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:55:54', 'neue-weiterleitungsseite', 'Neue Weiterleitungsseite', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 1, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(8, 6, 2, 0, 1, 0, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:56:22', 'neue-weiterleitungsseite', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 1, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(9, 6, 3, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:56:35', 'neue-weiterleitungsseite', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 1, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(10, 7, 1, 0, 1, 0, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 09:56:53', 'neue-restaurant-startseite', 'Neue Restaurant Startseite', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(11, 7, 2, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 09:57:15', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(12, 7, 3, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 09:58:16', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(13, 6, 4, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:58:25', 'neue-weiterleitungsseite', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(14, 8, 1, 1, 1, 1, 'Page', '2016-10-27 09:58:42', '2016-10-27 09:58:42', 'home', 'Startseite', NULL, '<p>Willkommen bei SilverStripe! Dies ist die Standard-Startseite. Sie können diese Seite bearbeiten, indem Sie <a href="admin/">das CMS</a> öffnen. Sie können außerdem die  <a href="http://doc.silverstripe.com">Entwicker-Dokumentation</a> oder die <a href="http://doc.silverstripe.com/doku.php?id=tutorials">Einführungskurse und Tutorien</a> aufrufen.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(15, 9, 1, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 09:58:42', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(16, 10, 1, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 09:58:42', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(17, 6, 5, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:59:24', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(18, 11, 1, 0, 1, 0, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:28:11', 'neue-speiseplan-datenbank', 'Neue Speiseplan Datenbank', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(19, 11, 2, 1, 1, 1, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:28:50', 'restaurant', 'Restaurant', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(20, 11, 3, 1, 1, 1, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:29:33', 'speiseplaene', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(21, 12, 1, 0, 1, 0, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:09', 'neue-speiseplan-tag', 'Neue Speiseplan (Tag)', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(22, 12, 2, 0, 1, 0, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:29', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(23, 12, 3, 1, 1, 1, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:29', '2016-01-01', 'Freitag, 01.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(24, 7, 4, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 11:41:28', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(25, 7, 5, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 11:42:04', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(26, 7, 6, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 11:42:19', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(27, 13, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 11:56:08', '2016-10-27 11:56:08', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(28, 13, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 11:56:08', '2016-10-27 11:57:02', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(29, 13, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 11:56:08', '2016-10-27 11:57:02', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(30, 14, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:01:27', '2016-10-27 12:01:27', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(31, 14, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:01:27', '2016-10-27 12:02:06', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(32, 14, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:01:27', '2016-10-27 12:02:06', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(33, 15, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:03:02', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(34, 15, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:04:00', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(35, 15, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:04:00', 'druck-a4-hoch', 'Druck A4 hoch', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(36, 16, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:04:18', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(37, 16, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:05:01', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(38, 16, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:05:01', 'druck-a4-quer', 'Druck A4 quer', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(39, 17, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:05:25', '2016-10-27 12:05:25', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(40, 17, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:05:25', '2016-10-27 12:06:03', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(41, 17, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:05:25', '2016-10-27 12:06:03', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(42, 18, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:06:20', '2016-10-27 12:06:20', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(43, 18, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:06:20', '2016-10-27 12:06:46', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(44, 18, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:06:20', '2016-10-27 12:06:46', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(45, 19, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:07:03', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(46, 19, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:07:27', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(47, 19, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:07:28', 'print-category-dina7', 'print_category_dina7', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(48, 13, 4, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 11:56:08', '2016-10-27 12:07:42', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(49, 14, 4, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:01:27', '2016-10-27 12:07:57', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(50, 19, 4, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:09:04', 'druck-temperaturprotokoll', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(51, 20, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:09:23', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(52, 20, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(53, 20, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'druck-produktionsblatt', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(54, 21, 1, 0, 1, 0, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:12:17', 'neue-bildschirm-ausgabe-hoch', 'Neue Bildschirm Ausgabe hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(55, 21, 2, 1, 1, 1, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:13:43', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(56, 21, 3, 1, 1, 1, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:14:13', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(57, 22, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:14:50', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(58, 22, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:37', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(59, 22, 3, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:57', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(60, 23, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 12:17:23', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(61, 23, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 12:17:55', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(62, 24, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2016-10-27 12:20:02', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(63, 24, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2016-10-27 12:21:03', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(64, 25, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2016-10-27 12:22:15', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(65, 25, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2016-10-27 12:22:47', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(66, 26, 1, 0, 1, 0, 'MobileTemplatePage', '2016-10-27 12:23:27', '2016-10-27 12:23:27', 'neue-mobile-ausgabe', 'Neue Mobile Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(67, 26, 2, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2016-10-27 12:24:38', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(68, 27, 1, 0, 1, 0, 'Page', '2016-10-27 12:25:35', '2016-10-27 12:25:35', 'neue-seite', 'Neue Seite', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(69, 28, 1, 0, 1, 0, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:08', 'neue-mobile-infoseite', 'Neue Mobile Infoseite', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 26);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(70, 28, 2, 1, 1, 1, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:34', 'app-auf-dem-home-bilschirm-speichern', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt "Zum Startbildschirm hinzufügen" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 26);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(71, 29, 1, 0, 1, 0, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:17', 'neue-voting-gerichte-datenbank', 'Neue Voting Gerichte Datenbank', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(72, 29, 2, 1, 1, 1, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:33', 'voting-gerichte', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(73, 30, 1, 0, 1, 0, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 12:28:55', 'neue-voting-gerichte', 'Neue Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(74, 30, 2, 0, 1, 0, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 12:29:05', 'neu', 'Neu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(75, 30, 3, 1, 1, 1, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 12:29:05', '2016-10-27', '27.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(76, 31, 1, 0, 1, 0, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:45', 'neue-wunschgerichte-datenbank', 'Neue Wunschgerichte Datenbank', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(77, 31, 2, 1, 1, 1, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:56', 'wunschgericht', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(78, 32, 1, 0, 1, 0, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:04', 'neue-wunschgericht-uebermittlung', 'Neue Wunschgericht Übermittlung', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(79, 32, 2, 0, 1, 0, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:09', 'neu', 'Wunschgerichte Neu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(80, 32, 3, 1, 1, 1, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:09', '2016-10-27', 'Wunschgerichte 27.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(81, 32, 4, 1, 1, 1, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:32', '2016-10-27', 'Wunschgerichte 27.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(82, 33, 1, 0, 1, 0, 'MicrositeHomePage', '2016-10-27 12:33:06', '2016-10-27 12:33:06', 'neue-microsite-startseite', 'Neue Microsite Startseite', NULL, NULL, NULL, NULL, 1, 0, 18, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(83, 34, 1, 0, 1, 0, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:42', 'neue-microsite', 'Neue Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(84, 34, 2, 1, 1, 1, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:53', 'microsite', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(85, 35, 1, 0, 1, 0, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 12:35:42', 'neue-microsite-startseite', 'Neue Microsite Startseite', NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(86, 35, 2, 1, 1, 1, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 12:36:05', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(87, 36, 1, 1, 1, 1, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 12:46:56', 'neue-microsite-news-seite', 'Neue Microsite News Seite', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(88, 26, 3, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2016-10-27 12:48:17', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(89, 36, 2, 1, 1, 1, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 13:15:17', 'aktuelles', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(90, 35, 3, 1, 1, 1, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 13:24:02', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(91, 37, 1, 0, 1, 0, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:26:05', 'neue-microsite-seite', 'Neue Microsite Seite', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(92, 37, 2, 1, 1, 1, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:27:56', 'speiseplan', 'Speiseplan', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(93, 37, 3, 1, 1, 1, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:02', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(94, 37, 4, 1, 1, 1, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:24', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(95, 38, 1, 0, 1, 0, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:37', 'neue-microsite-seite', 'Neue Microsite Seite', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(96, 38, 2, 1, 1, 1, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:55', 'snacking', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(97, 6, 6, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 14:50:59', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(98, 9, 2, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:52:06', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(99, 10, 2, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:53:03', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(100, 23, 3, 1, 2, 2, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 17:19:11', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(101, 30, 4, 1, 2, 2, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:03', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(102, 30, 5, 1, 2, 2, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:11', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(103, 32, 5, 1, 2, 2, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 17:23:20', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31);
INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES(104, 35, 4, 1, 2, 2, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 17:29:35', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_ViewerGroups`
--

DROP TABLE IF EXISTS `SiteTree_ViewerGroups`;
CREATE TABLE `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SpecialLabelsBlacklistConfig`
--

DROP TABLE IF EXISTS `SpecialLabelsBlacklistConfig`;
CREATE TABLE `SpecialLabelsBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SpecialLabelsBlacklistConfig') DEFAULT 'SpecialLabelsBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SpecialLabelsBlacklistObject`
--

DROP TABLE IF EXISTS `SpecialLabelsBlacklistObject`;
CREATE TABLE `SpecialLabelsBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SpecialLabelsBlacklistObject') DEFAULT 'SpecialLabelsBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SpecialLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Subsite`
--

DROP TABLE IF EXISTS `Subsite`;
CREATE TABLE `Subsite` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Subsite') DEFAULT 'Subsite',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `RedirectURL` varchar(255) DEFAULT NULL,
  `DefaultSite` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Theme` varchar(50) DEFAULT NULL,
  `Language` varchar(6) DEFAULT NULL,
  `IsPublic` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `PageTypeBlacklist` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Subsite`
--

INSERT INTO `Subsite` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `RedirectURL`, `DefaultSite`, `Theme`, `Language`, `IsPublic`, `PageTypeBlacklist`) VALUES(1, 'Subsite', '2016-10-27 09:54:02', '2016-10-27 09:54:02', 'VORLAGE', NULL, 0, 'default-theme', 'de_DE', 1, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteAdditive`
--

DROP TABLE IF EXISTS `SubsiteAdditive`;
CREATE TABLE `SubsiteAdditive` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteAdditive') DEFAULT 'SubsiteAdditive',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteAllergen`
--

DROP TABLE IF EXISTS `SubsiteAllergen`;
CREATE TABLE `SubsiteAllergen` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteAllergen') DEFAULT 'SubsiteAllergen',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteDishLabel`
--

DROP TABLE IF EXISTS `SubsiteDishLabel`;
CREATE TABLE `SubsiteDishLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteDishLabel') DEFAULT 'SubsiteDishLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `IconUnicodeCode` varchar(50) DEFAULT NULL,
  `Color` varchar(7) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteDomain`
--

DROP TABLE IF EXISTS `SubsiteDomain`;
CREATE TABLE `SubsiteDomain` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteDomain') DEFAULT 'SubsiteDomain',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `IsPrimary` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SubsiteDomain`
--

INSERT INTO `SubsiteDomain` (`ID`, `ClassName`, `Created`, `LastEdited`, `Domain`, `IsPrimary`, `SubsiteID`) VALUES(1, 'SubsiteDomain', '2016-10-27 09:54:17', '2016-11-26 13:06:56', 'enter-domain-here.com', 1, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteSpecialLabel`
--

DROP TABLE IF EXISTS `SubsiteSpecialLabel`;
CREATE TABLE `SubsiteSpecialLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteSpecialLabel') DEFAULT 'SubsiteSpecialLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsitesVirtualPage`
--

DROP TABLE IF EXISTS `SubsitesVirtualPage`;
CREATE TABLE `SubsitesVirtualPage` (
  `ID` int(11) NOT NULL,
  `CustomMetaTitle` varchar(255) DEFAULT NULL,
  `CustomMetaKeywords` varchar(255) DEFAULT NULL,
  `CustomMetaDescription` mediumtext,
  `CustomExtraMeta` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsitesVirtualPage_Live`
--

DROP TABLE IF EXISTS `SubsitesVirtualPage_Live`;
CREATE TABLE `SubsitesVirtualPage_Live` (
  `ID` int(11) NOT NULL,
  `CustomMetaTitle` varchar(255) DEFAULT NULL,
  `CustomMetaKeywords` varchar(255) DEFAULT NULL,
  `CustomMetaDescription` mediumtext,
  `CustomExtraMeta` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsitesVirtualPage_versions`
--

DROP TABLE IF EXISTS `SubsitesVirtualPage_versions`;
CREATE TABLE `SubsitesVirtualPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `CustomMetaTitle` varchar(255) DEFAULT NULL,
  `CustomMetaKeywords` varchar(255) DEFAULT NULL,
  `CustomMetaDescription` mediumtext,
  `CustomExtraMeta` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage`
--

DROP TABLE IF EXISTS `TemplatePage`;
CREATE TABLE `TemplatePage` (
  `ID` int(11) NOT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage`
--

INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(13, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(14, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(15, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(16, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(17, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(18, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(19, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(20, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(21, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(22, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(23, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(24, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(25, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(26, 'Wochen', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0);
INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(35, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage_FoodCategories`
--

DROP TABLE IF EXISTS `TemplatePage_FoodCategories`;
CREATE TABLE `TemplatePage_FoodCategories` (
  `ID` int(11) NOT NULL,
  `TemplatePageID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage_FoodCategories`
--

INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(1, 13, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(2, 13, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(3, 13, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(4, 13, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(5, 13, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(6, 13, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(7, 13, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(8, 13, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(9, 13, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(10, 14, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(11, 14, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(12, 14, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(13, 14, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(14, 14, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(15, 14, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(16, 14, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(17, 14, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(18, 14, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(19, 15, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(20, 15, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(21, 15, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(22, 15, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(23, 15, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(24, 15, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(25, 15, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(26, 15, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(27, 15, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(28, 16, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(29, 16, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(30, 16, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(31, 16, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(32, 16, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(33, 16, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(34, 16, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(35, 16, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(36, 16, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(37, 17, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(38, 17, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(39, 17, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(40, 17, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(41, 17, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(42, 17, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(43, 17, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(44, 17, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(45, 17, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(46, 18, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(47, 18, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(48, 18, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(49, 18, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(50, 18, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(51, 18, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(52, 18, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(53, 18, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(54, 18, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(55, 19, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(56, 19, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(57, 19, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(58, 19, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(59, 19, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(60, 19, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(61, 19, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(62, 19, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(63, 19, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(64, 20, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(65, 20, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(66, 20, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(67, 20, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(68, 20, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(69, 20, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(70, 20, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(71, 20, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(72, 20, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(73, 21, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(74, 21, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(75, 21, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(76, 21, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(77, 22, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(78, 22, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(79, 22, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(80, 22, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(81, 24, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(82, 24, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(83, 24, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(84, 24, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(85, 24, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(86, 24, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(87, 25, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(88, 26, 1);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(89, 26, 2);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(90, 26, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(91, 26, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(92, 26, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(93, 26, 6);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(94, 26, 7);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(95, 26, 8);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(96, 26, 9);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(97, 35, 3);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(98, 35, 4);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(99, 35, 5);
INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES(100, 35, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage_Live`
--

DROP TABLE IF EXISTS `TemplatePage_Live`;
CREATE TABLE `TemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage_Live`
--

INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(13, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(14, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(15, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(16, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(17, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(18, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(19, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(20, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(21, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(22, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(23, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(24, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(25, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(26, 'Wochen', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0);
INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(35, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage_versions`
--

DROP TABLE IF EXISTS `TemplatePage_versions`;
CREATE TABLE `TemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage_versions`
--

INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(1, 13, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(2, 13, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(3, 13, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(4, 14, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(5, 14, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(6, 14, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(7, 15, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(8, 15, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(9, 15, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(10, 16, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(11, 16, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(12, 16, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(13, 17, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(14, 17, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(15, 17, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(16, 18, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(17, 18, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(18, 18, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(19, 19, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(20, 19, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(21, 19, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(22, 13, 4, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(23, 14, 4, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(24, 19, 4, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(25, 20, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(26, 20, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(27, 20, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(28, 21, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(29, 21, 2, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(30, 21, 3, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(31, 22, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(32, 22, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(33, 22, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(34, 23, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(35, 23, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(36, 24, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(37, 24, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(38, 25, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(39, 25, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(40, 26, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(41, 26, 2, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(42, 33, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(43, 35, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(44, 35, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(45, 26, 3, 'Wochen', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(46, 35, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(47, 23, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11);
INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES(48, 35, 4, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VirtualPage`
--

DROP TABLE IF EXISTS `VirtualPage`;
CREATE TABLE `VirtualPage` (
  `ID` int(11) NOT NULL,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VirtualPage_Live`
--

DROP TABLE IF EXISTS `VirtualPage_Live`;
CREATE TABLE `VirtualPage_Live` (
  `ID` int(11) NOT NULL,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VirtualPage_versions`
--

DROP TABLE IF EXISTS `VirtualPage_versions`;
CREATE TABLE `VirtualPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VisitorCounterData`
--

DROP TABLE IF EXISTS `VisitorCounterData`;
CREATE TABLE `VisitorCounterData` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('VisitorCounterData') DEFAULT 'VisitorCounterData',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Time` time DEFAULT NULL,
  `MaxVisitors` int(11) NOT NULL DEFAULT '0',
  `MaxVisitorsPercent` int(11) NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VisitorCounterPredictionData`
--

DROP TABLE IF EXISTS `VisitorCounterPredictionData`;
CREATE TABLE `VisitorCounterPredictionData` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('VisitorCounterPredictionData') DEFAULT 'VisitorCounterPredictionData',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Time` time DEFAULT NULL,
  `MaxVisitors` int(11) NOT NULL DEFAULT '0',
  `MaxVisitorsPercent` int(11) NOT NULL DEFAULT '0',
  `WeekdayNum` int(11) NOT NULL DEFAULT '0',
  `Month` int(11) NOT NULL DEFAULT '0',
  `Year` int(11) NOT NULL DEFAULT '0',
  `PredictionBase` enum('LastWeek','Continous','SameMonthLastYear') DEFAULT 'Continous',
  `PredictionDataItemCount` int(11) NOT NULL DEFAULT '0',
  `LastAddedDateTime` datetime DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `AdditivesBlacklistConfig`
--
ALTER TABLE `AdditivesBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `AdditivesBlacklistObject`
--
ALTER TABLE `AdditivesBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `AdditivesBlacklistConfigID` (`AdditivesBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `AllergensBlacklistConfig`
--
ALTER TABLE `AllergensBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `AllergensBlacklistObject`
--
ALTER TABLE `AllergensBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `AllergensBlacklistConfigID` (`AllergensBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DailyMenuPage`
--
ALTER TABLE `DailyMenuPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DailyMenuPage_Dishes`
--
ALTER TABLE `DailyMenuPage_Dishes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DailyMenuPageID` (`DailyMenuPageID`),
  ADD KEY `DishID` (`DishID`);

--
-- Indizes für die Tabelle `DailyMenuPage_Live`
--
ALTER TABLE `DailyMenuPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DailyMenuPage_versions`
--
ALTER TABLE `DailyMenuPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `Dish`
--
ALTER TABLE `Dish`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `OrigDishID` (`OrigDishID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishesPerRestaurantDishExtensionPermissions`
--
ALTER TABLE `DishesPerRestaurantDishExtensionPermissions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishLabelsBlacklistConfig`
--
ALTER TABLE `DishLabelsBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishLabelsBlacklistObject`
--
ALTER TABLE `DishLabelsBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishLabelsBlacklistConfigID` (`DishLabelsBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishRating`
--
ALTER TABLE `DishRating`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishVotingItem`
--
ALTER TABLE `DishVotingItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishVotingPageID` (`DishVotingPageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishVotingPage`
--
ALTER TABLE `DishVotingPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DishVotingPage_Live`
--
ALTER TABLE `DishVotingPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DishVotingPage_versions`
--
ALTER TABLE `DishVotingPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `Dish_GlobalAdditives`
--
ALTER TABLE `Dish_GlobalAdditives`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalAdditiveID` (`GlobalAdditiveID`);

--
-- Indizes für die Tabelle `Dish_GlobalAllergens`
--
ALTER TABLE `Dish_GlobalAllergens`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalAllergenID` (`GlobalAllergenID`);

--
-- Indizes für die Tabelle `Dish_GlobalDishLabels`
--
ALTER TABLE `Dish_GlobalDishLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalDishLabelID` (`GlobalDishLabelID`);

--
-- Indizes für die Tabelle `Dish_GlobalSpecialLabels`
--
ALTER TABLE `Dish_GlobalSpecialLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalSpecialLabelID` (`GlobalSpecialLabelID`);

--
-- Indizes für die Tabelle `Dish_SubsiteAdditives`
--
ALTER TABLE `Dish_SubsiteAdditives`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteAdditiveID` (`SubsiteAdditiveID`);

--
-- Indizes für die Tabelle `Dish_SubsiteAllergens`
--
ALTER TABLE `Dish_SubsiteAllergens`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteAllergenID` (`SubsiteAllergenID`);

--
-- Indizes für die Tabelle `Dish_SubsiteDishLabels`
--
ALTER TABLE `Dish_SubsiteDishLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteDishLabelID` (`SubsiteDishLabelID`);

--
-- Indizes für die Tabelle `Dish_SubsiteSpecialLabels`
--
ALTER TABLE `Dish_SubsiteSpecialLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteSpecialLabelID` (`SubsiteSpecialLabelID`);

--
-- Indizes für die Tabelle `DisplayTemplateObject`
--
ALTER TABLE `DisplayTemplateObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `DisplayMultiTemplatePageID` (`DisplayMultiTemplatePageID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DisplayTemplateObject_FoodCategories`
--
ALTER TABLE `DisplayTemplateObject_FoodCategories`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplateObjectID` (`DisplayTemplateObjectID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`);

--
-- Indizes für die Tabelle `DisplayTemplateObject_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplateObject_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplateObjectID` (`DisplayTemplateObjectID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage`
--
ALTER TABLE `DisplayTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage_Live`
--
ALTER TABLE `DisplayTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplatePage_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePageID` (`DisplayTemplatePageID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage_versions`
--
ALTER TABLE `DisplayTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `DisplayVideo`
--
ALTER TABLE `DisplayVideo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `VideoFileWebMID` (`VideoFileWebMID`),
  ADD KEY `DisplayTemplatePageID` (`DisplayTemplatePageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DisplayVideo_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayVideo_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayVideoID` (`DisplayVideoID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `ErrorPage`
--
ALTER TABLE `ErrorPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `ErrorPage_Live`
--
ALTER TABLE `ErrorPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `ErrorPage_versions`
--
ALTER TABLE `ErrorPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `ExportConfig`
--
ALTER TABLE `ExportConfig`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `ExportDisplayMenuXmlConfig`
--
ALTER TABLE `ExportDisplayMenuXmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePageID` (`DisplayTemplatePageID`);

--
-- Indizes für die Tabelle `ExportMenuHtmlConfig`
--
ALTER TABLE `ExportMenuHtmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `ExportMenuHtmlConfig_FoodCategories`
--
ALTER TABLE `ExportMenuHtmlConfig_FoodCategories`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ExportMenuHtmlConfigID` (`ExportMenuHtmlConfigID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`);

--
-- Indizes für die Tabelle `ExportMenuXmlConfig`
--
ALTER TABLE `ExportMenuXmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `File`
--
ALTER TABLE `File`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `OwnerID` (`OwnerID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `File_DeleterGroups`
--
ALTER TABLE `File_DeleterGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `File_EditorFolderGroups`
--
ALTER TABLE `File_EditorFolderGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `File_UploaderGroups`
--
ALTER TABLE `File_UploaderGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `File_ViewerGroups`
--
ALTER TABLE `File_ViewerGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `FoodCategory`
--
ALTER TABLE `FoodCategory`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `FoodCategoryBlacklistConfig`
--
ALTER TABLE `FoodCategoryBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `FoodCategoryBlacklistObject`
--
ALTER TABLE `FoodCategoryBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FoodCategoryBlacklistConfigID` (`FoodCategoryBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalAdditive`
--
ALTER TABLE `GlobalAdditive`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalAllergen`
--
ALTER TABLE `GlobalAllergen`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalDishLabel`
--
ALTER TABLE `GlobalDishLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalSpecialLabel`
--
ALTER TABLE `GlobalSpecialLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Group`
--
ALTER TABLE `Group`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Group_Members`
--
ALTER TABLE `Group_Members`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Indizes für die Tabelle `Group_Roles`
--
ALTER TABLE `Group_Roles`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `PermissionRoleID` (`PermissionRoleID`);

--
-- Indizes für die Tabelle `Group_Subsites`
--
ALTER TABLE `Group_Subsites`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `SubsiteID` (`SubsiteID`);

--
-- Indizes für die Tabelle `ImportConfig`
--
ALTER TABLE `ImportConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NutritivesMapConfigID` (`NutritivesMapConfigID`),
  ADD KEY `FoodCategoryBlacklistConfigID` (`FoodCategoryBlacklistConfigID`),
  ADD KEY `AdditivesBlacklistConfigID` (`AdditivesBlacklistConfigID`),
  ADD KEY `AllergensBlacklistConfigID` (`AllergensBlacklistConfigID`),
  ADD KEY `DishLabelsBlacklistConfigID` (`DishLabelsBlacklistConfigID`),
  ADD KEY `SpecialLabelsBlacklistConfigID` (`SpecialLabelsBlacklistConfigID`);

--
-- Indizes für die Tabelle `ImportFilterConfig`
--
ALTER TABLE `ImportFilterConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `ImportMenuXmlConfig`
--
ALTER TABLE `ImportMenuXmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `ImportRestaurantConfig`
--
ALTER TABLE `ImportRestaurantConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`);

--
-- Indizes für die Tabelle `ImportVisitorCounterConfig`
--
ALTER TABLE `ImportVisitorCounterConfig`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `InfoMessage`
--
ALTER TABLE `InfoMessage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `InfoMessageDisplayTemplatePageLandscape`
--
ALTER TABLE `InfoMessageDisplayTemplatePageLandscape`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePageLandscapeID` (`DisplayTemplatePageLandscapeID`),
  ADD KEY `InfoMessageTemplateDisplayTemplatePageLandscapeID` (`InfoMessageTemplateDisplayTemplatePageLandscapeID`);

--
-- Indizes für die Tabelle `InfoMessageDisplayTemplatePagePortrait`
--
ALTER TABLE `InfoMessageDisplayTemplatePagePortrait`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePagePortraitID` (`DisplayTemplatePagePortraitID`),
  ADD KEY `InfoMessageTemplateDisplayTemplatePagePortraitID` (`InfoMessageTemplateDisplayTemplatePagePortraitID`);

--
-- Indizes für die Tabelle `InfoMessageMicrositeHomePage`
--
ALTER TABLE `InfoMessageMicrositeHomePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplateMicrositeHomePageID` (`InfoMessageTemplateMicrositeHomePageID`),
  ADD KEY `MicrositeHomePageID` (`MicrositeHomePageID`),
  ADD KEY `LinkToID` (`LinkToID`),
  ADD KEY `DownloadID` (`DownloadID`);

--
-- Indizes für die Tabelle `InfoMessageMicrositeNewsPage`
--
ALTER TABLE `InfoMessageMicrositeNewsPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplateMicrositeNewsPageID` (`InfoMessageTemplateMicrositeNewsPageID`),
  ADD KEY `MicrositeNewsPageID` (`MicrositeNewsPageID`);

--
-- Indizes für die Tabelle `InfoMessageMobileTemplatePage`
--
ALTER TABLE `InfoMessageMobileTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplateMobileTemplatePageID` (`InfoMessageTemplateMobileTemplatePageID`),
  ADD KEY `MobileTemplatePageID` (`MobileTemplatePageID`);

--
-- Indizes für die Tabelle `InfoMessagePrintTemplatePage`
--
ALTER TABLE `InfoMessagePrintTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplatePrintTemplatePageID` (`InfoMessageTemplatePrintTemplatePageID`),
  ADD KEY `PrintTemplatePageID` (`PrintTemplatePageID`),
  ADD KEY `PrintHtmlTemplatePageID` (`PrintHtmlTemplatePageID`);

--
-- Indizes für die Tabelle `InfoMessage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `InfoMessage_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageID` (`InfoMessageID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `LoginAttempt`
--
ALTER TABLE `LoginAttempt`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Member`
--
ALTER TABLE `Member`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Email` (`Email`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MemberPassword`
--
ALTER TABLE `MemberPassword`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MenuContainer`
--
ALTER TABLE `MenuContainer`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MenuContainer_Live`
--
ALTER TABLE `MenuContainer_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MenuContainer_versions`
--
ALTER TABLE `MenuContainer_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeContainer`
--
ALTER TABLE `MicrositeContainer`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeContainer_Live`
--
ALTER TABLE `MicrositeContainer_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeContainer_versions`
--
ALTER TABLE `MicrositeContainer_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeContentItem`
--
ALTER TABLE `MicrositeContentItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MicrositePageID` (`MicrositePageID`),
  ADD KEY `MicrositeHomePageID` (`MicrositeHomePageID`),
  ADD KEY `LinkToID` (`LinkToID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MicrositeHomePage`
--
ALTER TABLE `MicrositeHomePage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeHomePage_Live`
--
ALTER TABLE `MicrositeHomePage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeHomePage_versions`
--
ALTER TABLE `MicrositeHomePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeLinkItem`
--
ALTER TABLE `MicrositeLinkItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MicrositePageID` (`MicrositePageID`),
  ADD KEY `LinkToID` (`LinkToID`),
  ADD KEY `DownloadID` (`DownloadID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MicrositePage`
--
ALTER TABLE `MicrositePage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositePage_Live`
--
ALTER TABLE `MicrositePage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositePage_versions`
--
ALTER TABLE `MicrositePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeRedirectorPage`
--
ALTER TABLE `MicrositeRedirectorPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeRedirectorPage_Live`
--
ALTER TABLE `MicrositeRedirectorPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeRedirectorPage_versions`
--
ALTER TABLE `MicrositeRedirectorPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MobileInfoPage`
--
ALTER TABLE `MobileInfoPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`);

--
-- Indizes für die Tabelle `MobileInfoPage_Live`
--
ALTER TABLE `MobileInfoPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`);

--
-- Indizes für die Tabelle `MobileInfoPage_versions`
--
ALTER TABLE `MobileInfoPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `ImageID` (`ImageID`);

--
-- Indizes für die Tabelle `MobileTemplatePage`
--
ALTER TABLE `MobileTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `MobileTemplatePage_Live`
--
ALTER TABLE `MobileTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `MobileTemplatePage_MenuContainers`
--
ALTER TABLE `MobileTemplatePage_MenuContainers`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MobileTemplatePageID` (`MobileTemplatePageID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `MobileTemplatePage_versions`
--
ALTER TABLE `MobileTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `MySiteConfigExtensionPermissions`
--
ALTER TABLE `MySiteConfigExtensionPermissions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `NutritivesMapConfig`
--
ALTER TABLE `NutritivesMapConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `NutritivesMapObject`
--
ALTER TABLE `NutritivesMapObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NutritivesMapConfigID` (`NutritivesMapConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Permission`
--
ALTER TABLE `Permission`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `Code` (`Code`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PermissionRole`
--
ALTER TABLE `PermissionRole`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PermissionRoleCode`
--
ALTER TABLE `PermissionRoleCode`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RoleID` (`RoleID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PrintHtmlTemplatePage`
--
ALTER TABLE `PrintHtmlTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `HeaderImageID` (`HeaderImageID`);

--
-- Indizes für die Tabelle `PrintHtmlTemplatePage_Live`
--
ALTER TABLE `PrintHtmlTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `HeaderImageID` (`HeaderImageID`);

--
-- Indizes für die Tabelle `PrintHtmlTemplatePage_versions`
--
ALTER TABLE `PrintHtmlTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `HeaderImageID` (`HeaderImageID`);

--
-- Indizes für die Tabelle `PrintManagerConfig`
--
ALTER TABLE `PrintManagerConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`),
  ADD KEY `PrintManagerTemplateID` (`PrintManagerTemplateID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PrintManagerConfig_Dishes`
--
ALTER TABLE `PrintManagerConfig_Dishes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `PrintManagerConfigID` (`PrintManagerConfigID`),
  ADD KEY `DishID` (`DishID`);

--
-- Indizes für die Tabelle `PrintManagerTemplate`
--
ALTER TABLE `PrintManagerTemplate`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PrintTemplatePage`
--
ALTER TABLE `PrintTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `WeeklyTableHeaderImageID` (`WeeklyTableHeaderImageID`);

--
-- Indizes für die Tabelle `PrintTemplatePage_Live`
--
ALTER TABLE `PrintTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `WeeklyTableHeaderImageID` (`WeeklyTableHeaderImageID`);

--
-- Indizes für die Tabelle `PrintTemplatePage_versions`
--
ALTER TABLE `PrintTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `WeeklyTableHeaderImageID` (`WeeklyTableHeaderImageID`);

--
-- Indizes für die Tabelle `ProcessTaskObject`
--
ALTER TABLE `ProcessTaskObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RecipeSubmissionItem`
--
ALTER TABLE `RecipeSubmissionItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RecipeSubmissionPageID` (`RecipeSubmissionPageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RecipeSubmissionPage`
--
ALTER TABLE `RecipeSubmissionPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `RecipeSubmissionPage_Live`
--
ALTER TABLE `RecipeSubmissionPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `RecipeSubmissionPage_versions`
--
ALTER TABLE `RecipeSubmissionPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `RecurringDayOfWeek`
--
ALTER TABLE `RecurringDayOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RedirectorPage`
--
ALTER TABLE `RedirectorPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `LinkToID` (`LinkToID`);

--
-- Indizes für die Tabelle `RedirectorPage_Live`
--
ALTER TABLE `RedirectorPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `LinkToID` (`LinkToID`);

--
-- Indizes für die Tabelle `RedirectorPage_versions`
--
ALTER TABLE `RedirectorPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `LinkToID` (`LinkToID`);

--
-- Indizes für die Tabelle `RestaurantDish`
--
ALTER TABLE `RestaurantDish`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SideDish1ID` (`SideDish1ID`),
  ADD KEY `SideDish2ID` (`SideDish2ID`),
  ADD KEY `SideDish3ID` (`SideDish3ID`),
  ADD KEY `SideDish4ID` (`SideDish4ID`),
  ADD KEY `SideDish5ID` (`SideDish5ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RestaurantPage`
--
ALTER TABLE `RestaurantPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantLogoID` (`RestaurantLogoID`);

--
-- Indizes für die Tabelle `RestaurantPage_Live`
--
ALTER TABLE `RestaurantPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantLogoID` (`RestaurantLogoID`);

--
-- Indizes für die Tabelle `RestaurantPage_versions`
--
ALTER TABLE `RestaurantPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `RestaurantLogoID` (`RestaurantLogoID`);

--
-- Indizes für die Tabelle `SiteConfig`
--
ALTER TABLE `SiteConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteConfig_CreateTopLevelGroups`
--
ALTER TABLE `SiteConfig_CreateTopLevelGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteConfigID` (`SiteConfigID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteConfig_EditorGroups`
--
ALTER TABLE `SiteConfig_EditorGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteConfigID` (`SiteConfigID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteConfig_ViewerGroups`
--
ALTER TABLE `SiteConfig_ViewerGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteConfigID` (`SiteConfigID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteTree`
--
ALTER TABLE `SiteTree`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `URLSegment` (`URLSegment`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteTree_CrossSubsiteLinkTracking`
--
ALTER TABLE `SiteTree_CrossSubsiteLinkTracking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `ChildID` (`ChildID`);

--
-- Indizes für die Tabelle `SiteTree_EditorGroups`
--
ALTER TABLE `SiteTree_EditorGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteTree_ImageTracking`
--
ALTER TABLE `SiteTree_ImageTracking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `FileID` (`FileID`);

--
-- Indizes für die Tabelle `SiteTree_LinkTracking`
--
ALTER TABLE `SiteTree_LinkTracking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `ChildID` (`ChildID`);

--
-- Indizes für die Tabelle `SiteTree_Live`
--
ALTER TABLE `SiteTree_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `URLSegment` (`URLSegment`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteTree_versions`
--
ALTER TABLE `SiteTree_versions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `AuthorID` (`AuthorID`),
  ADD KEY `PublisherID` (`PublisherID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `URLSegment` (`URLSegment`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteTree_ViewerGroups`
--
ALTER TABLE `SiteTree_ViewerGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SpecialLabelsBlacklistConfig`
--
ALTER TABLE `SpecialLabelsBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SpecialLabelsBlacklistObject`
--
ALTER TABLE `SpecialLabelsBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SpecialLabelsBlacklistConfigID` (`SpecialLabelsBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Subsite`
--
ALTER TABLE `Subsite`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteAdditive`
--
ALTER TABLE `SubsiteAdditive`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteAllergen`
--
ALTER TABLE `SubsiteAllergen`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteDishLabel`
--
ALTER TABLE `SubsiteDishLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteDomain`
--
ALTER TABLE `SubsiteDomain`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteSpecialLabel`
--
ALTER TABLE `SubsiteSpecialLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsitesVirtualPage`
--
ALTER TABLE `SubsitesVirtualPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `SubsitesVirtualPage_Live`
--
ALTER TABLE `SubsitesVirtualPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `SubsitesVirtualPage_versions`
--
ALTER TABLE `SubsitesVirtualPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `TemplatePage`
--
ALTER TABLE `TemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `TemplatePage_FoodCategories`
--
ALTER TABLE `TemplatePage_FoodCategories`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TemplatePageID` (`TemplatePageID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`);

--
-- Indizes für die Tabelle `TemplatePage_Live`
--
ALTER TABLE `TemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `TemplatePage_versions`
--
ALTER TABLE `TemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `VirtualPage`
--
ALTER TABLE `VirtualPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CopyContentFromID` (`CopyContentFromID`);

--
-- Indizes für die Tabelle `VirtualPage_Live`
--
ALTER TABLE `VirtualPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CopyContentFromID` (`CopyContentFromID`);

--
-- Indizes für die Tabelle `VirtualPage_versions`
--
ALTER TABLE `VirtualPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `CopyContentFromID` (`CopyContentFromID`);

--
-- Indizes für die Tabelle `VisitorCounterData`
--
ALTER TABLE `VisitorCounterData`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `VisitorCounterPredictionData`
--
ALTER TABLE `VisitorCounterPredictionData`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `AdditivesBlacklistConfig`
--
ALTER TABLE `AdditivesBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `AdditivesBlacklistObject`
--
ALTER TABLE `AdditivesBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `AllergensBlacklistConfig`
--
ALTER TABLE `AllergensBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `AllergensBlacklistObject`
--
ALTER TABLE `AllergensBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage`
--
ALTER TABLE `DailyMenuPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage_Dishes`
--
ALTER TABLE `DailyMenuPage_Dishes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage_Live`
--
ALTER TABLE `DailyMenuPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage_versions`
--
ALTER TABLE `DailyMenuPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `Dish`
--
ALTER TABLE `Dish`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `DishesPerRestaurantDishExtensionPermissions`
--
ALTER TABLE `DishesPerRestaurantDishExtensionPermissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishLabelsBlacklistConfig`
--
ALTER TABLE `DishLabelsBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishLabelsBlacklistObject`
--
ALTER TABLE `DishLabelsBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishRating`
--
ALTER TABLE `DishRating`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishVotingItem`
--
ALTER TABLE `DishVotingItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `DishVotingPage`
--
ALTER TABLE `DishVotingPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT für Tabelle `DishVotingPage_Live`
--
ALTER TABLE `DishVotingPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT für Tabelle `DishVotingPage_versions`
--
ALTER TABLE `DishVotingPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalAdditives`
--
ALTER TABLE `Dish_GlobalAdditives`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalAllergens`
--
ALTER TABLE `Dish_GlobalAllergens`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalDishLabels`
--
ALTER TABLE `Dish_GlobalDishLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalSpecialLabels`
--
ALTER TABLE `Dish_GlobalSpecialLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteAdditives`
--
ALTER TABLE `Dish_SubsiteAdditives`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteAllergens`
--
ALTER TABLE `Dish_SubsiteAllergens`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteDishLabels`
--
ALTER TABLE `Dish_SubsiteDishLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteSpecialLabels`
--
ALTER TABLE `Dish_SubsiteSpecialLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplateObject`
--
ALTER TABLE `DisplayTemplateObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplateObject_FoodCategories`
--
ALTER TABLE `DisplayTemplateObject_FoodCategories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplateObject_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplateObject_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage`
--
ALTER TABLE `DisplayTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage_Live`
--
ALTER TABLE `DisplayTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplatePage_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage_versions`
--
ALTER TABLE `DisplayTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT für Tabelle `DisplayVideo`
--
ALTER TABLE `DisplayVideo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayVideo_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayVideo_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ErrorPage`
--
ALTER TABLE `ErrorPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `ErrorPage_Live`
--
ALTER TABLE `ErrorPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `ErrorPage_versions`
--
ALTER TABLE `ErrorPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `ExportConfig`
--
ALTER TABLE `ExportConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportDisplayMenuXmlConfig`
--
ALTER TABLE `ExportDisplayMenuXmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportMenuHtmlConfig`
--
ALTER TABLE `ExportMenuHtmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportMenuHtmlConfig_FoodCategories`
--
ALTER TABLE `ExportMenuHtmlConfig_FoodCategories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportMenuXmlConfig`
--
ALTER TABLE `ExportMenuXmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `File`
--
ALTER TABLE `File`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT für Tabelle `File_DeleterGroups`
--
ALTER TABLE `File_DeleterGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT für Tabelle `File_EditorFolderGroups`
--
ALTER TABLE `File_EditorFolderGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `File_UploaderGroups`
--
ALTER TABLE `File_UploaderGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT für Tabelle `File_ViewerGroups`
--
ALTER TABLE `File_ViewerGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `FoodCategory`
--
ALTER TABLE `FoodCategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT für Tabelle `FoodCategoryBlacklistConfig`
--
ALTER TABLE `FoodCategoryBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `FoodCategoryBlacklistObject`
--
ALTER TABLE `FoodCategoryBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `GlobalAdditive`
--
ALTER TABLE `GlobalAdditive`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `GlobalAllergen`
--
ALTER TABLE `GlobalAllergen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `GlobalDishLabel`
--
ALTER TABLE `GlobalDishLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `GlobalSpecialLabel`
--
ALTER TABLE `GlobalSpecialLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Group`
--
ALTER TABLE `Group`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `Group_Members`
--
ALTER TABLE `Group_Members`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `Group_Roles`
--
ALTER TABLE `Group_Roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `Group_Subsites`
--
ALTER TABLE `Group_Subsites`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportConfig`
--
ALTER TABLE `ImportConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportFilterConfig`
--
ALTER TABLE `ImportFilterConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportMenuXmlConfig`
--
ALTER TABLE `ImportMenuXmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportRestaurantConfig`
--
ALTER TABLE `ImportRestaurantConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportVisitorCounterConfig`
--
ALTER TABLE `ImportVisitorCounterConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessage`
--
ALTER TABLE `InfoMessage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageDisplayTemplatePageLandscape`
--
ALTER TABLE `InfoMessageDisplayTemplatePageLandscape`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageDisplayTemplatePagePortrait`
--
ALTER TABLE `InfoMessageDisplayTemplatePagePortrait`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageMicrositeHomePage`
--
ALTER TABLE `InfoMessageMicrositeHomePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageMicrositeNewsPage`
--
ALTER TABLE `InfoMessageMicrositeNewsPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageMobileTemplatePage`
--
ALTER TABLE `InfoMessageMobileTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessagePrintTemplatePage`
--
ALTER TABLE `InfoMessagePrintTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `InfoMessage_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `LoginAttempt`
--
ALTER TABLE `LoginAttempt`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Member`
--
ALTER TABLE `Member`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MemberPassword`
--
ALTER TABLE `MemberPassword`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MenuContainer`
--
ALTER TABLE `MenuContainer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `MenuContainer_Live`
--
ALTER TABLE `MenuContainer_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `MenuContainer_versions`
--
ALTER TABLE `MenuContainer_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContainer`
--
ALTER TABLE `MicrositeContainer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContainer_Live`
--
ALTER TABLE `MicrositeContainer_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContainer_versions`
--
ALTER TABLE `MicrositeContainer_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContentItem`
--
ALTER TABLE `MicrositeContentItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `MicrositeHomePage`
--
ALTER TABLE `MicrositeHomePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT für Tabelle `MicrositeHomePage_Live`
--
ALTER TABLE `MicrositeHomePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT für Tabelle `MicrositeHomePage_versions`
--
ALTER TABLE `MicrositeHomePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `MicrositeLinkItem`
--
ALTER TABLE `MicrositeLinkItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `MicrositePage`
--
ALTER TABLE `MicrositePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT für Tabelle `MicrositePage_Live`
--
ALTER TABLE `MicrositePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT für Tabelle `MicrositePage_versions`
--
ALTER TABLE `MicrositePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `MicrositeRedirectorPage`
--
ALTER TABLE `MicrositeRedirectorPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `MicrositeRedirectorPage_Live`
--
ALTER TABLE `MicrositeRedirectorPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `MicrositeRedirectorPage_versions`
--
ALTER TABLE `MicrositeRedirectorPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `MobileInfoPage`
--
ALTER TABLE `MobileInfoPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT für Tabelle `MobileInfoPage_Live`
--
ALTER TABLE `MobileInfoPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT für Tabelle `MobileInfoPage_versions`
--
ALTER TABLE `MobileInfoPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage`
--
ALTER TABLE `MobileTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage_Live`
--
ALTER TABLE `MobileTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage_MenuContainers`
--
ALTER TABLE `MobileTemplatePage_MenuContainers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage_versions`
--
ALTER TABLE `MobileTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `MySiteConfigExtensionPermissions`
--
ALTER TABLE `MySiteConfigExtensionPermissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `NutritivesMapConfig`
--
ALTER TABLE `NutritivesMapConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `NutritivesMapObject`
--
ALTER TABLE `NutritivesMapObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Permission`
--
ALTER TABLE `Permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1609;
--
-- AUTO_INCREMENT für Tabelle `PermissionRole`
--
ALTER TABLE `PermissionRole`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `PermissionRoleCode`
--
ALTER TABLE `PermissionRoleCode`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2152;
--
-- AUTO_INCREMENT für Tabelle `PrintHtmlTemplatePage`
--
ALTER TABLE `PrintHtmlTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintHtmlTemplatePage_Live`
--
ALTER TABLE `PrintHtmlTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintHtmlTemplatePage_versions`
--
ALTER TABLE `PrintHtmlTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintManagerConfig`
--
ALTER TABLE `PrintManagerConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintManagerConfig_Dishes`
--
ALTER TABLE `PrintManagerConfig_Dishes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintManagerTemplate`
--
ALTER TABLE `PrintManagerTemplate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintTemplatePage`
--
ALTER TABLE `PrintTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT für Tabelle `PrintTemplatePage_Live`
--
ALTER TABLE `PrintTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT für Tabelle `PrintTemplatePage_versions`
--
ALTER TABLE `PrintTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT für Tabelle `ProcessTaskObject`
--
ALTER TABLE `ProcessTaskObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionItem`
--
ALTER TABLE `RecipeSubmissionItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionPage`
--
ALTER TABLE `RecipeSubmissionPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionPage_Live`
--
ALTER TABLE `RecipeSubmissionPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionPage_versions`
--
ALTER TABLE `RecipeSubmissionPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `RecurringDayOfWeek`
--
ALTER TABLE `RecurringDayOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `RedirectorPage`
--
ALTER TABLE `RedirectorPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RedirectorPage_Live`
--
ALTER TABLE `RedirectorPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RedirectorPage_versions`
--
ALTER TABLE `RedirectorPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RestaurantDish`
--
ALTER TABLE `RestaurantDish`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `RestaurantPage`
--
ALTER TABLE `RestaurantPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `RestaurantPage_Live`
--
ALTER TABLE `RestaurantPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `RestaurantPage_versions`
--
ALTER TABLE `RestaurantPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig`
--
ALTER TABLE `SiteConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig_CreateTopLevelGroups`
--
ALTER TABLE `SiteConfig_CreateTopLevelGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig_EditorGroups`
--
ALTER TABLE `SiteConfig_EditorGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig_ViewerGroups`
--
ALTER TABLE `SiteConfig_ViewerGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree`
--
ALTER TABLE `SiteTree`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_CrossSubsiteLinkTracking`
--
ALTER TABLE `SiteTree_CrossSubsiteLinkTracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_EditorGroups`
--
ALTER TABLE `SiteTree_EditorGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_ImageTracking`
--
ALTER TABLE `SiteTree_ImageTracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_LinkTracking`
--
ALTER TABLE `SiteTree_LinkTracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_Live`
--
ALTER TABLE `SiteTree_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_versions`
--
ALTER TABLE `SiteTree_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_ViewerGroups`
--
ALTER TABLE `SiteTree_ViewerGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SpecialLabelsBlacklistConfig`
--
ALTER TABLE `SpecialLabelsBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SpecialLabelsBlacklistObject`
--
ALTER TABLE `SpecialLabelsBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Subsite`
--
ALTER TABLE `Subsite`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `SubsiteAdditive`
--
ALTER TABLE `SubsiteAdditive`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsiteAllergen`
--
ALTER TABLE `SubsiteAllergen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsiteDishLabel`
--
ALTER TABLE `SubsiteDishLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsiteDomain`
--
ALTER TABLE `SubsiteDomain`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `SubsiteSpecialLabel`
--
ALTER TABLE `SubsiteSpecialLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsitesVirtualPage`
--
ALTER TABLE `SubsitesVirtualPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsitesVirtualPage_Live`
--
ALTER TABLE `SubsitesVirtualPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsitesVirtualPage_versions`
--
ALTER TABLE `SubsitesVirtualPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage`
--
ALTER TABLE `TemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage_FoodCategories`
--
ALTER TABLE `TemplatePage_FoodCategories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage_Live`
--
ALTER TABLE `TemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage_versions`
--
ALTER TABLE `TemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT für Tabelle `VirtualPage`
--
ALTER TABLE `VirtualPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VirtualPage_Live`
--
ALTER TABLE `VirtualPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VirtualPage_versions`
--
ALTER TABLE `VirtualPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VisitorCounterData`
--
ALTER TABLE `VisitorCounterData`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VisitorCounterPredictionData`
--
ALTER TABLE `VisitorCounterPredictionData`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
