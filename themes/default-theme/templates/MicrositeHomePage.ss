<% include MicrositeHtmlHead %>

	<% include Favicons_include %>
	<% include MicrositeCSS %>
	<link rel="stylesheet" href="$ThemeDir/javascript/libs/nivoslider/themes/default/default.css" />
	<link rel="stylesheet" href="$ThemeDir/javascript/libs/nivoslider/nivo-slider.css" type="text/css" media="screen" />
	<% include MicrositeJavascriptHead %>

</head>

<body class="$ClassName">
	
	<% include MicrositeHeader %>

	<main id="content">
		
		<% if $InfoMessagesAtDateTime %>
			<div class="container">
				<div class="slider-wrapper theme-default">

					<div class="nivoSlider slider clearfix" id="slider">
						<% loop $InfoMessagesAtDateTime %>
						<img src="$Image.CroppedImage(1170,450).Link"<% if $Title || $Content %> title="#news-slider-item-$ID"<% end_if %> alt="" />
						<% end_loop %>
					</div>

					<% loop $InfoMessagesAtDateTime %>
						<% if $Title || $Content %>
							<div class="nivo-caption" id="news-slider-item-$ID">
								<% if $Title %><h3><% if $Link %><a href="$Link"<% if $LinkType = External %> target="_blank"<% end_if %>><% end_if %>$Title<% if $Link %></a><% end_if %></h3><% end_if %>
								<p>$Content.NoHTML</p>
							</div>
						<% end_if %>
					<% end_loop %>

				</div><!-- slider -->
			</div>
		<% end_if %>

		<% if $Content %>
			<div class="container-fluid bg-grey-light-1 bx-padt-l bx-padb-l">
				<div class="container">
					<div class="row">
						
						<div class="col-lg-8 col-md-6 col-xs-12">
							$Content
						</div>
						
						<div class="col-lg-4 col-md-6 col-xs-12">
						
							<% if $ShowMenu %>
								<h1>Heute im Restaurant</h1>
								<% with $MenuContainer %>
									<% if $Up.DailyMenuPagesForMenuContainerID($ID) %>
										<% loop $Up.DailyMenuPagesForMenuContainerID($ID) %>
											<% if $FoodCategories %>

												<% loop $FoodCategories %>
													<% if $DishesForDateAndCategoryAndMobileTemplatePage %>
														<div class="FoodCategoryContainer<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $Color;"<% end_if %>>
															<h4 class="FoodCategoryTitle">$Title</h4>
															<ul class="dishes">
																<% loop $DishesForDateAndCategoryAndMobileTemplatePage %>
																	<li>
																		<div class="clearfix">
																			<% include Mobile_WeeklyMenu_DishData %>
																		</div>
																	</li>
																<% end_loop %>
															</ul>
														</div>
													<% end_if %>
												<% end_loop %>

											<% end_if %>
										<% end_loop %>
									<% end_if %>
								<% end_with %>
							<% end_if %>
							
						</div>
						
					</div>
				</div>
			</div>
		<% end_if %>
		
		<% if $ShowOpeningHours %>
			<div class="container-fluid bg-grey-light-2">
				<div class="container">
					<div class="row">

						<div class="col-lg-6 col-md-8 col-xs-12 has-image">
							<img src="/$ThemeDir/images/microsite/opening-hours.jpg" alt="" />
						</div>

						<div class="col-lg-6 col-md-4 col-xs-12 bx-padt-l bx-padb-l">
							<h1 class="bx-marb-xs">Öffnungszeiten</h1>
							<p>$RestaurantPage.OpeningHours</p>
						</div>
					</div>
				</div>
			</div>
		<% end_if %>

		<% include MicrositeContentItems %>

	</main><!-- main -->

	<% include MicrositeFooter %>

	<% include MicrositeJavascriptBody %>
</body>
</html>
