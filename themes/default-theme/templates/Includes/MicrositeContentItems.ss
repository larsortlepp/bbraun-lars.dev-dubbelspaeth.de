<% if $MicrositeContentItems %>
	<% loop $MicrositeContentItems.Sort('Sort') %>
		<div class="container-fluid<% if $Background = Color %> bg-green-apple<% else_if $Background = Grey %> bg-grey-light-2<% end_if %>">
			<div class="container">
				<div class="row">

					<% if $Image && $ImagePosition = Left %>
						<div class="col-lg-6 col-md-8 col-xs-12 has-image">
							$Image.SetRatioSize(555,555)
						</div>
					<% end_if %>

					<div class="<% if $Image %>col-lg-6 col-md-4 <% end_if %>col-xs-12 bx-padt-l bx-padb-l">
						<% if $Title %><h1 class="bx-marb-xs">$Title</h1><% end_if %>
						$Content
						<% if $Link %><a href="$Link" class="btn"<% if $LinkType = External %> target="_blank"<% end_if %>>$LinkTitle <i class="icon-right-open-big"></i></a><% end_if %>
					</div>

					<% if $Image && $ImagePosition = Right %>
						<div class="col-lg-6 col-md-8 col-xs-12 has-image">
							$Image.SetRatioSize(555,555)
						</div>
					<% end_if %>
				</div>
			</div>
		</div>
	<% end_loop %>
<% end_if %>