
	<article class="InfoMessage<% if $Image %> hasImage<% end_if %><% if $ImageOnly && $Image %> FullScreenImage<% end_if %>">
		<div class="inner-article">

			<% if $ImageOnly && $Image %>
				$Image.SetRatioSize(1920,1080)
			<% else %>

			<% include Screen_DailyMenu_InfoMessage_Header %>
			<% if $Image %><div class="info-message-image">$Image.SetRatioSize(735,1040)</div><% end_if %>
			<div id="content">
				<div class="inner-content">
					$Content
					<% if $RestaurantPage.Lang_en_US %><% if $Content_en_US %>
					<div class="lang-en-US">
						$Content_en_US
					</div>
					<% end_if %><% end_if %>
				</div>
			</div>
			<% end_if %>
		</div>
	</article>
