													<% if $NutritionValuesSet %>
														<h3>
															Nährwerte $SiteConfig.NutritivesUnit
															<% if $RestaurantPage.Lang_en_US %><br /><span class="lang-en-US">Nutritives $SiteConfig.NutritivesUnit_en_US</span><% end_if %>
														</h3>
														<table class="nutrition-values kein-abstand">
															<tbody>
																<% loop $Nutritives %>
																	<tr>
																		<th>$Title<% if $RestaurantPage.Lang_en_US %> <span class="lang-en-US">$Title_en_US</span><% end_if %></th>
																		<td class="table-align-right">$ValueNice $Unit</td>
																	</tr>
																<% end_loop %>
															</tbody>
														</table>
													<% end_if %>