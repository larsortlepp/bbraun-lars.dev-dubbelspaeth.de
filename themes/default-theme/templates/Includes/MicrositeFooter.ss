<footer>
	<div class="container-fluid bg-grey-mid bx-padt-l bx-padb-l">
		<div class="container">

				<div class="row ">
					<% with $RestaurantPage %>
						<div class="col-lg-4 col-md-12 col-xs-12 bx-marb-s">

							<% if $ContactCompanyName || $ContactStreetAddress %>
								<h3>Kontakt</h3>
								$ContactCompanyName<br />
								$ContactStreetAddress
							<% end_if %>
						</div>

						<div class="col-lg-4 col-md-12 col-xs-12 bx-marb-s">
							<% if $ContactPersonName || $ContactPhone || $ContactFax || $ContactEmail %>
								<% if $ContactPersonName %>$ContactPersonName<br /><% end_if %>
								<% if $ContactPhone %>Tel.: $ContactPhone<br /><% end_if %>
								<% if $ContactFax %>Fax: $ContactFax<br /><% end_if %>
								<% if $ContactEmail %><a href="mailto:$ContactEmail">$ContactEmail</a><% end_if %>
							<% end_if %>
						</div>

						<div class="col-lg-4 col-md-12 col-xs-12 bx-marb-s">
							<a href="http://www.eurest.de/impressum" target="_blank">Impressum</a>
							<% if $ContactCopyrightCompany %><br />&copy; {$Top.Now.Year} – $ContactCopyrightCompany<% end_if %>
						</div>
					<% end_with %>

				</div>

		</div>
	</div>
</footer><!-- footer -->

<a href="#top"></a>