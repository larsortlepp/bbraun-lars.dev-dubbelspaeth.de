<!-- loaded -->
<div class="mt-page-inner">
	<h2 class="outerHeader">
		Danke, dass Sie uns Ihr WUNSCHGERICHT mitgeteilt haben!
		<% if $RestaurantPage.Lang_en_US %><br /> <span class="lang-en-US">Thank you for telling us your favourite dish!</span><% end_if %>
	</h2>
	<div class="content-container">
		<ul class="mt-form-result-list">
			<li>
				<h4 class="label">
					Ihr WUNSCHGERICHT
					<% if $RestaurantPage.Lang_en_US %><br /><span class="lang-en-US">Your favourite recipe</span><% end_if %>
				</h4>
				<p class="field">
					$Recipe
				</p>
			</li>
			<li>
				<h4 class="label">
					Name (freiwillig)<% if $RestaurantPage.Lang_en_US %> <span class="lang-en-US">Name (optional)</span><% end_if %>
				</h4>
				<p class="field">
					<% if $Name %>$Name<% else %>–<% end_if %>
				</p>
			</li>
			<li>
				<h4 class="label">
					E-Mail (freiwillig)<% if $RestaurantPage.Lang_en_US %> <span class="lang-en-US">Email (optional)</span><% end_if %>
				</h4>
				<p class="field">
					<% if $Email %>$Email<% else %>–<% end_if %>
				</p>
			</li>
		</ul>
	</div>
</div>
