<% if $UsedDishLabels %>
<div class="sideInfosDishLabels">
	<h2>Kennzeichnungen<% if $RestaurantPage.Lang_en_US %> / <span class="lang-en-US">Labels</span><% end_if %>:</h2>
	<ul class="icon-list">
		<% loop $UsedDishLabels %>
		<li><% if $IconUnicodeCode %>
			<span class="dishLabelIcon dishlabel-icon-$IconUnicodeCode"<% if $Color %> style="color: $Color;"<% end_if %>></span>
			<% end_if %>
			<span class="dishLabelDescription">$Title<% if $RestaurantPage.Lang_en_US %><% if $Title_en_US %> / <span class="lang-en-US">$Title_en_US</span><% end_if %><% end_if %></span>
		</li>
		<% end_loop %>
	</ul>
</div>
<% end_if %>
