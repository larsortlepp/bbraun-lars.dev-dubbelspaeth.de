											<% if $Image %>
												<div class="dishImage lazyload-container">
													<img class="lazy" src="/$ThemeDir/images/lazyload.gif" data-src="<% with $Image %><% with $CroppedImage(400,200) %>$URL<% end_with %><% end_with %>"  data-src-retina="<% with $Image %><% with $CroppedImage(800,400) %>$URL<% end_with %><% end_with %>" />
												</div>
											<% end_if %>
