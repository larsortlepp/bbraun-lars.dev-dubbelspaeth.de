		<article class="video">
			<div class="inner-article">
				<video width="1080" height="607" id="videoplayer" preload="none" autoplay="autoplay">
					<% if $VideoType == "Youtube" %>
						<source type="video/youtube" src="$YoutubeLink" />
					<% else_if $VideoFileWebM %>
						<!-- WebM/VP8 for Firefox4, Opera, and Chrome -->
						<source type="video/webm" src="$VideoFileWebM.URL" />
					<% end_if %>
				</video>
			</div>
		</article>
