		<header class="title-container">
			<% if $RestaurantPage %>
				<% with $RestaurantPage %>
					<% if $RestaurantLogo %>$RestaurantLogo.SetRatioSize(80,200)<% end_if %>
					<h2 class="title">$Title</h2>
				<% end_with %>
			<% else %>
				<h2 class="title">$SiteConfig.Title</h2>
			<% end_if %>
		</header>
