/**
 * Links in neuem Fenster/Tab öffnen
 *
 * Anwendung: dem <a>-Tag wird die unten angegebene Klasse zugewiesen, damit der Link in neuem Fenster geöffnet wird
 * <a href="http://www.google.de/" class="external_link">Google (öffnet in neuem Fenster)</a>
 */
jQuery.external_links = {

	// VARIABLEN
	triggerClass: 'external_link', // Klassenname für Links, die in neuem Fenster geöffnet werden sollen
	addTitle: '(öffnet in neuem Fenster)', // Hinweis der dem title Attribut des Links angefügt wird

	init: function() {
		var links = jQuery('.'+jQuery.external_links.triggerClass);
		if(links) {
			links.each(function() {
				jQuery(this).attr({target: "_blank"});
				jQuery(this).attr({title: jQuery(this).attr('title')+" "+jQuery.external_links.addTitle});
			});
		}
	}
}

/**
 * Allen Popup Links ein Overlay hinzufügen, damit sichtbar ist, dass bei Klick ein Popup geöffnet wird
 *
 * Anwendung: dem <a>-Tag wird die unten angegebene Klasse zugewiesen, damit der Link in Popup geöffnet wird
 * <a href="http://www.google.de/" class="popup">Google (öffnet in Popup)</a>
 */
jQuery.popup = {

	// VARIABLEN
	triggerClass: 'popup', // Klassenname für Links, die in Popup geöffnet werden sollen
	imgClass: 'image', // Zusätzlicher Klassenname der Links die Bild beinhalten angefügt wird
	overlayImgClass: 'popup_overlay', // Klassenname Overlay, dass über Link (Bild) gelegt wird

	init: function() {
		var links = jQuery('.'+jQuery.popup.triggerClass);
		if(links) {
			for(var i = 0; i < links.length; i++) {
				jQuery(links[i]).addClass(jQuery.popup.imgClass);

				// overlay for images
				if(jQuery(links[i]).children("img").first().val() != null) {
					jQuery("<span/>", {
						"class": jQuery.popup.overlayImgClass
					}).appendTo(links[i]);
				}
			}
		}
	}
}

/**
 * Default-Werte der Formularfelder leeren, wenn Nutzer ins Feld klickt
 */
jQuery.clearDefaultsOnFields = {

	// VARIABLEN
	fieldIDs: new Array('SearchForm_SearchForm_Search'), // IDs der Formularfelder die geleert werden sollen
	defaultValues: new Array(), // Array that stores the default values of the given

	init: function() {

		// loop throug all IDs
		var IDCount = jQuery.clearDefaultsOnFields.fieldIDs.length;
		for(var i=0; i < IDCount; i++ ) {
			// if field exists
			if(jQuery('#'+jQuery.clearDefaultsOnFields.fieldIDs[i]).val() != null) {
				var formField = jQuery('#'+jQuery.clearDefaultsOnFields.fieldIDs[i]);
				// store default value
				jQuery.clearDefaultsOnFields.defaultValues[i] = jQuery(formField).attr('value');
				// store internal id to element to be able to read out the default value later on
				jQuery(formField).data('ID', i);

				// add focusin event handler to each field
				jQuery(formField).focusin(function(){
					// if value is default value -> clear it
					if(jQuery(this).attr('value') == jQuery.clearDefaultsOnFields.defaultValues[jQuery(this).data('ID')]) {
						jQuery(this).attr('value', '');
					}
				});

				// add focusout event handler to each field
				jQuery(formField).focusout(function(){
					// if value is empty -> restore it
					if(jQuery(this).attr('value') == '') {
						jQuery(this).attr('value', jQuery.clearDefaultsOnFields.defaultValues[jQuery(this).data('ID')]);
					}
				});

			}
		}
	}
}

/**
 * JQUERY INITIALISATION WHEN DOCUMENT READY
 *
 * @author Sebastian Dubbel
 *
 * @requires
 *		jQuery library v 1.2.6+: http://jquery.com
 * @encoding UTF-8
 *
 */
// start required scripts when DOM is loaded
jQuery(document).ready(function(){


});
