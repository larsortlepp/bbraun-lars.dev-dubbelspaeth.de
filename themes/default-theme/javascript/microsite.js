/*
*	functions v1.2 for demo-v3.signage-server.de
*	version of 10-05-2016
************************************************
*/

	jQuery(function($) {
	
		$('body').addClass('js');
						
		// nav hover
		$('.menu-item-has-children').hover(
			function(){
				$(this).addClass('current');
			},
			function(){
				$(this).removeClass('current');
			}
		);
		

		/***********************************************************
		*	DROPDOWN MENU (Selection only - menu is based on css)
		***********************************************************/
		$('.dropdown .first').click(function(e){
			e.preventDefault();
			$(this).next('ul.children').toggleClass('show');
		});		
		
		
		/***********************************************************
		*	ACCORDION
		***********************************************************/
		function closeOpenSections(){
			$('.accordion .accordion-trigger').removeClass('open');
			$('.accordion .accordion-content').slideUp(500);
		}
		
		$('.accordion .accordion-trigger').click(function(e){

			if( $(e.target).is('.open')){
				closeOpenSections();
			} else {
				closeOpenSections();
				$(this).addClass('open');
				$(this).next('.accordion-content:hidden').slideToggle();
			}
					
		});
		
		
		
		/***********************************************************
		*	TOGGLE MOBILE MENU
		***********************************************************/
		$('.menu-toggle').click(function(e){
			e.preventDefault();
			$('body').toggleClass('menu-active');			
		});
	
	
		
		/***********************************************************
		*	SCROLL TO TOP BTN
		***********************************************************/
		$(window).scroll(function(){
			var sc = $(window).scrollTop();
			if(sc>1500){
				$('a[href="#top"]').show();
			} else {
				$('a[href="#top"]').hide();
			}
		});
		
		$('a[href="#top"]').click(function(e){
			e.preventDefault();
			$('html, body').animate({scrollTop:0},1000);
		});
			
});





/***********************************************************
*	NIVO SLIDER
***********************************************************/

// nivo slider uses .load( ... ) instead of .ready( ... )
jQuery(window).load(function() {
   
   /* params: */
   /*
   		effect: 'fade',                // Specify sets like: 'fold,fade,sliceDown'
	    slices: 15,                     // For slice animations
	    boxCols: 8,                     // For box animations
	    boxRows: 4,                     // For box animations
	    animSpeed: 1000,                 // Slide transition speed
	    pauseTime: 60000,                 // How long each slide will show
	    startSlide: 0,                     // Set starting Slide (0 index)
	    directionNav: true,             // Next & Prev navigation
	    controlNav: true,                 // 1,2,3... navigation
	    controlNavThumbs: false,         // Use thumbnails for Control Nav
	    pauseOnHover: true,             // Stop animation while hovering
	    manualAdvance: false,             // Force manual transitions
	    prevText: '',                 // Prev directionNav text
	    nextText: '',                 // Next directionNav text
	    randomStart: false,             // Start on a random slide
	    beforeChange: function(){},     // Triggers before a slide transition
	    afterChange: function(){},         // Triggers after a slide transition
	    slideshowEnd: function(){},     // Triggers after all slides have been shown
	    lastSlide: function(){},         // Triggers when last slide is shown
	    afterLoad: function(){}         // Triggers when slider has loaded
   */
   
   jQuery('#slider').nivoSlider({
	   effect: 'fade',   
	   animSpeed: 400,
	   pauseTime: 4000
   });	
  
});

