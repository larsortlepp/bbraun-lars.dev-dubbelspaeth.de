<?php
return array(
 'times' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'times-roman' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'courier' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'helvetica' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
  ),
  'symbol' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Symbol',
    'bold' => DOMPDF_FONT_DIR . 'Symbol',
    'italic' => DOMPDF_FONT_DIR . 'Symbol',
    'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
  ),
  'serif' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'monospace' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'fixed' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'sans-serif' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
'icons' => 
	array (
	  'normal' => DOMPDF_FONT_DIR . 'icons',
	  'bold' => DOMPDF_FONT_DIR . 'icons',
	  'italic' => DOMPDF_FONT_DIR . 'icons',
	  'bold_italic' => DOMPDF_FONT_DIR . 'icons',
	),
 'icon-dishlabels' => 
	array (
	  'normal' => DOMPDF_FONT_DIR . 'icon-dishlabels',
	  'bold' => DOMPDF_FONT_DIR . 'icon-dishlabels',
	  'italic' => DOMPDF_FONT_DIR . 'icon-dishlabels',
	  'bold_italic' => DOMPDF_FONT_DIR . 'icon-dishlabels',
	),
 'univers-light' => 
	array (
	  'normal' => DOMPDF_FONT_DIR . 'universnextw1g_light',
	  'bold' => DOMPDF_FONT_DIR . 'universnextw1g_light',
	  'italic' => DOMPDF_FONT_DIR . 'universnextw1g_light',
	  'bold_italic' => DOMPDF_FONT_DIR . 'universnextw1g_light',
	),
 'bebasneue' => 
	array (
	  'normal' => DOMPDF_FONT_DIR . 'bebasneue',
	  'bold' => DOMPDF_FONT_DIR . 'bebasneue',
	  'italic' => DOMPDF_FONT_DIR . 'bebasneue',
	  'bold_italic' => DOMPDF_FONT_DIR . 'bebasneue',
	),
 'a-gentle-touch' => 
	array (
	  'normal' => DOMPDF_FONT_DIR . 'a_gentle_touch',
	  'bold' => DOMPDF_FONT_DIR . 'a_gentle_touch',
	  'italic' => DOMPDF_FONT_DIR . 'a_gentle_touch',
	  'bold_italic' => DOMPDF_FONT_DIR . 'a_gentle_touch',
	),
);


?>