/* REMOVE THIS FROM LIVE SCRIPT - CAUSES ERRORS IN IE */
window.log = function f(){log.history = log.history || [];log.history.push(arguments);if(this.console) {var args = arguments, newarr;args.callee = args.callee.caller;newarr = [].slice.call(args);if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

/**
 * spin.js
 * AAJAX Spinner in JavaScript
 * @link http://fgnass.github.com/spin.js/
 */

//fgnass.github.com/spin.js#v1.2.8
!function(window, document, undefined) {

  /**
   * Copyright (c) 2011 Felix Gnass [fgnass at neteye dot de]
   * Licensed under the MIT license
   */

  var prefixes = ['webkit', 'Moz', 'ms', 'O'] /* Vendor prefixes */
    , animations = {} /* Animation rules keyed by their name */
    , useCssAnimations

  /**
   * Utility function to create elements. If no tag name is given,
   * a DIV is created. Optionally properties can be passed.
   */
  function createEl(tag, prop) {
    var el = document.createElement(tag || 'div')
      , n

    for(n in prop) el[n] = prop[n]
    return el
  }

  /**
   * Appends children and returns the parent.
   */
  function ins(parent /* child1, child2, ...*/) {
    for (var i=1, n=arguments.length; i<n; i++)
      parent.appendChild(arguments[i])

    return parent
  }

  /**
   * Insert a new stylesheet to hold the @keyframe or VML rules.
   */
  var sheet = function() {
    var el = createEl('style', {type : 'text/css'})
    ins(document.getElementsByTagName('head')[0], el)
    return el.sheet || el.styleSheet
  }()

  /**
   * Creates an opacity keyframe animation rule and returns its name.
   * Since most mobile Webkits have timing issues with animation-delay,
   * we create separate rules for each line/segment.
   */
  function addAnimation(alpha, trail, i, lines) {
    var name = ['opacity', trail, ~~(alpha*100), i, lines].join('-')
      , start = 0.01 + i/lines*100
      , z = Math.max(1 - (1-alpha) / trail * (100-start), alpha)
      , prefix = useCssAnimations.substring(0, useCssAnimations.indexOf('Animation')).toLowerCase()
      , pre = prefix && '-'+prefix+'-' || ''

    if (!animations[name]) {
      sheet.insertRule(
        '@' + pre + 'keyframes ' + name + '{' +
        '0%{opacity:' + z + '}' +
        start + '%{opacity:' + alpha + '}' +
        (start+0.01) + '%{opacity:1}' +
        (start+trail) % 100 + '%{opacity:' + alpha + '}' +
        '100%{opacity:' + z + '}' +
        '}', sheet.cssRules.length)

      animations[name] = 1
    }
    return name
  }

  /**
   * Tries various vendor prefixes and returns the first supported property.
   **/
  function vendor(el, prop) {
    var s = el.style
      , pp
      , i

    if(s[prop] !== undefined) return prop
    prop = prop.charAt(0).toUpperCase() + prop.slice(1)
    for(i=0; i<prefixes.length; i++) {
      pp = prefixes[i]+prop
      if(s[pp] !== undefined) return pp
    }
  }

  /**
   * Sets multiple style properties at once.
   */
  function css(el, prop) {
    for (var n in prop)
      el.style[vendor(el, n)||n] = prop[n]

    return el
  }

  /**
   * Fills in default values.
   */
  function merge(obj) {
    for (var i=1; i < arguments.length; i++) {
      var def = arguments[i]
      for (var n in def)
        if (obj[n] === undefined) obj[n] = def[n]
    }
    return obj
  }

  /**
   * Returns the absolute page-offset of the given element.
   */
  function pos(el) {
    var o = {x:el.offsetLeft, y:el.offsetTop}
    while((el = el.offsetParent))
      o.x+=el.offsetLeft, o.y+=el.offsetTop

    return o
  }

  var defaults = {
    lines: 12,            // The number of lines to draw
    length: 7,            // The length of each line
    width: 5,             // The line thickness
    radius: 10,           // The radius of the inner circle
    rotate: 0,            // Rotation offset
    corners: 1,           // Roundness (0..1)
    color: '#000',        // #rgb or #rrggbb
    speed: 1,             // Rounds per second
    trail: 100,           // Afterglow percentage
    opacity: 1/4,         // Opacity of the lines
    fps: 20,              // Frames per second when using setTimeout()
    zIndex: 2e9,          // Use a high z-index by default
    className: 'spinner', // CSS class to assign to the element
    top: 'auto',          // center vertically
    left: 'auto',         // center horizontally
    position: 'relative'  // element position
  }

  /** The constructor */
  function Spinner(o) {
    if (!this.spin) return new Spinner(o)
    this.opts = merge(o || {}, Spinner.defaults, defaults)
  }

  Spinner.defaults = {}

  merge(Spinner.prototype, {
    spin: function(target) {
      this.stop()
      var self = this
        , o = self.opts
        , el = self.el = css(createEl(0, {className: o.className}), {position: o.position, width: 0, zIndex: o.zIndex})
        , mid = o.radius+o.length+o.width
        , ep // element position
        , tp // target position

      if (target) {
        target.insertBefore(el, target.firstChild||null)
        tp = pos(target)
        ep = pos(el)
        css(el, {
          left: (o.left == 'auto' ? tp.x-ep.x + (target.offsetWidth >> 1) : parseInt(o.left, 10) + mid) + 'px',
          top: (o.top == 'auto' ? tp.y-ep.y + (target.offsetHeight >> 1) : parseInt(o.top, 10) + mid)  + 'px'
        })
      }

      el.setAttribute('aria-role', 'progressbar')
      self.lines(el, self.opts)

      if (!useCssAnimations) {
        // No CSS animation support, use setTimeout() instead
        var i = 0
          , fps = o.fps
          , f = fps/o.speed
          , ostep = (1-o.opacity) / (f*o.trail / 100)
          , astep = f/o.lines

        ;(function anim() {
          i++;
          for (var s=o.lines; s; s--) {
            var alpha = Math.max(1-(i+s*astep)%f * ostep, o.opacity)
            self.opacity(el, o.lines-s, alpha, o)
          }
          self.timeout = self.el && setTimeout(anim, ~~(1000/fps))
        })()
      }
      return self
    },

    stop: function() {
      var el = this.el
      if (el) {
        clearTimeout(this.timeout)
        if (el.parentNode) el.parentNode.removeChild(el)
        this.el = undefined
      }
      return this
    },

    lines: function(el, o) {
      var i = 0
        , seg

      function fill(color, shadow) {
        return css(createEl(), {
          position: 'absolute',
          width: (o.length+o.width) + 'px',
          height: o.width + 'px',
          background: color,
          boxShadow: shadow,
          transformOrigin: 'left',
          transform: 'rotate(' + ~~(360/o.lines*i+o.rotate) + 'deg) translate(' + o.radius+'px' +',0)',
          borderRadius: (o.corners * o.width>>1) + 'px'
        })
      }

      for (; i < o.lines; i++) {
        seg = css(createEl(), {
          position: 'absolute',
          top: 1+~(o.width/2) + 'px',
          transform: o.hwaccel ? 'translate3d(0,0,0)' : '',
          opacity: o.opacity,
          animation: useCssAnimations && addAnimation(o.opacity, o.trail, i, o.lines) + ' ' + 1/o.speed + 's linear infinite'
        })

        if (o.shadow) ins(seg, css(fill('#000', '0 0 4px ' + '#000'), {top: 2+'px'}))

        ins(el, ins(seg, fill(o.color, '0 0 1px rgba(0,0,0,.1)')))
      }
      return el
    },

    opacity: function(el, i, val) {
      if (i < el.childNodes.length) el.childNodes[i].style.opacity = val
    }

  })

  /////////////////////////////////////////////////////////////////////////
  // VML rendering for IE
  /////////////////////////////////////////////////////////////////////////

  /**
   * Check and init VML support
   */
  ;(function() {

    function vml(tag, attr) {
      return createEl('<' + tag + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', attr)
    }

    var s = css(createEl('group'), {behavior: 'url(#default#VML)'})

    if (!vendor(s, 'transform') && s.adj) {

      // VML support detected. Insert CSS rule ...
      sheet.addRule('.spin-vml', 'behavior:url(#default#VML)')

      Spinner.prototype.lines = function(el, o) {
        var r = o.length+o.width
          , s = 2*r

        function grp() {
          return css(
            vml('group', {
              coordsize: s + ' ' + s,
              coordorigin: -r + ' ' + -r
            }),
            {width: s, height: s}
          )
        }

        var margin = -(o.width+o.length)*2 + 'px'
          , g = css(grp(), {position: 'absolute', top: margin, left: margin})
          , i

        function seg(i, dx, filter) {
          ins(g,
            ins(css(grp(), {rotation: 360 / o.lines * i + 'deg', left: ~~dx}),
              ins(css(vml('roundrect', {arcsize: o.corners}), {
                  width: r,
                  height: o.width,
                  left: o.radius,
                  top: -o.width>>1,
                  filter: filter
                }),
                vml('fill', {color: o.color, opacity: o.opacity}),
                vml('stroke', {opacity: 0}) // transparent stroke to fix color bleeding upon opacity change
              )
            )
          )
        }

        if (o.shadow)
          for (i = 1; i <= o.lines; i++)
            seg(i, -2, 'progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)')

        for (i = 1; i <= o.lines; i++) seg(i)
        return ins(el, g)
      }

      Spinner.prototype.opacity = function(el, i, val, o) {
        var c = el.firstChild
        o = o.shadow && o.lines || 0
        if (c && i+o < c.childNodes.length) {
          c = c.childNodes[i+o];c = c && c.firstChild;c = c && c.firstChild
          if (c) c.opacity = val
        }
      }
    }
    else
      useCssAnimations = vendor(s, 'animation')
  })()

  if (typeof define == 'function' && define.amd)
    define(function() {return Spinner})
  else
    window.Spinner = Spinner

}(window, document);


/**
 * Links in neuem Fenster/Tab öffnen
 * 
 * Anwendung: dem <a>-Tag wird die unten angegebene Klasse zugewiesen, damit der Link in neuem Fenster geöffnet wird
 * <a href="http://www.google.de/" class="external_link">Google (öffnet in neuem Fenster)</a>
 */
jQuery.external_links = {
	
	// VARIABLEN
	triggerClass: 'external_link', // Klassenname für Links, die in neuem Fenster geöffnet werden sollen
	addTitle: '(öffnet in neuem Fenster)', // Hinweis der dem title Attribut des Links angefügt wird
	
	init: function() {
		var links = jQuery('.'+jQuery.external_links.triggerClass);
		if(links) {
			links.each(function() {
				jQuery(this).attr({target: "_blank"});
				jQuery(this).attr({title: jQuery(this).attr('title')+" "+jQuery.external_links.addTitle});
			});
		}
	}
}

/*
 * ACCORDION: Toggle content via link
 * slide container in/out
 * trigger link needs class "toggle" and href="#idOfTargetElement"
 *
 * Example HTML:
 * <h4><a href="#konditionen-1" title="" class="trigger">Konditionen</a> { optional: <span class="mt-toggle-icon">+</span> } </h4>
 * <div id="konditionen-1">
 *     ...
 * </div>
 *
 * @param String	selector	Selector string for initialisation (e.g. '#myContainer' -> will init all trigger links inside this container)
 * @param Integer	openElem	Index for elemtn that will be opened (Default: 0 = first item, -1 = Close all items at init)
 *
 */
jQuery.accordion = {

	showHideTriggerClass: 'mt-accordion-trigger-link',
	accordionContainerClass: 'mt-accordion',
	initFlagClass: 'mt-accordion-initialised', // will be added to element when initialised
	openClass: 'active',
	slideSpeed: 300,
	dataOpenAttr: 'data-date', // attribute on accordion trigger link which indicates the date of the item (items of the current date will be opened). Date Format: data-date="2013-01-14"
	defaultOpenElem: 0, // index of element that is opened by default
	
	init: function(selector, openElem, currDate) {
		
		selector = selector ? selector : 'body';

		if(jQuery(selector+" a."+jQuery.accordion.showHideTriggerClass)) {

			openElem = (openElem) ? openElem : jQuery.accordion.defaultOpenElem;
			currDate = (currDate) ? currDate : false;
			
			//console.log('trying to open elemnt with: '+currDate);

			// hide all target elements when page loads, show only first one
			jQuery(selector+" a."+jQuery.accordion.showHideTriggerClass).each(function(i) {
				
				// only run if item wasn´t initialised
				if(jQuery(this).hasClass(jQuery.accordion.initFlagClass)) {
					return true;
				}
				// otherwise add initialised class
				else {
					jQuery(this).addClass(jQuery.accordion.initFlagClass);
				}
				
				// get target id of link
				var target = jQuery.accordion.getHashFromURL(jQuery(this).attr("href"));
				jQuery("#"+target).css({
					overflow: 'hidden'
				});
				// hide target item with given ID
				if(target) {
					// hide target <div>
					jQuery("#"+target).hide();
				}
				// add "open" class to given target item (defaultOpenElem)
				if(
					(!currDate && i == openElem) ||
					(currDate && jQuery(this).attr(jQuery.accordion.dataOpenAttr) == currDate)
				) {
					jQuery(this).addClass(jQuery.accordion.openClass);
					jQuery("#"+target).css({
						display: 'block'
					});
				}
				
				//console.log('data-date: '+jQuery(this).attr(jQuery.accordion.dataOpenAttr));

				// add tap event
				// trigger accordion or show/hide element function
				if(jQuery(this).parents('.'+jQuery.accordion.accordionContainerClass).length >= 1) {
					// accordion
					jQuery(this).on('tap', function(e){jQuery.accordion.showAccordionItem(e);});
				}
				else {
					// show / hide element
					jQuery(this).on('tap', function(e){jQuery.accordion.showHideItem(e)});
				}
			});

			// check if element was opened, if not open default element
			if( (jQuery(selector+" a."+jQuery.accordion.showHideTriggerClass+"."+jQuery.accordion.openClass).length == 0) && (openElem !=-1) ) {
				var firstElem = jQuery(selector+" a."+jQuery.accordion.showHideTriggerClass).first();
				var target = jQuery.accordion.getHashFromURL(jQuery(firstElem).attr("href"));

				if(!jQuery(firstElem).hasClass(jQuery.accordion.initFlagClass)) {
					jQuery(firstElem).addClass(jQuery.accordion.openClass);
					jQuery("#"+target).css({
						display: 'block'
					});
				}
			}
		}
	},
	
	destroy: function(selector) {
		
		selector = selector ? selector : 'body';
		
		if(jQuery(selector+" a."+jQuery.accordion.showHideTriggerClass)) {
			
			//console.log('destroying '+selector);

			// hide all target elements when page loads, show only first one
			jQuery(selector+" a."+jQuery.accordion.showHideTriggerClass).each(function(i) {
				// get target id of link
				var target = jQuery.accordion.getHashFromURL(jQuery(this).attr("href"));
				jQuery("#"+target).css({
					overflow: 'visible'
				});
				// remove "open" class
				jQuery(this).removeClass(jQuery.accordion.openClass);
				// show all target items
				jQuery("#"+target).show();
				// remove click event
				jQuery(this).off('click');
			});
		}
	},

	showAccordionItem: function(e) {			
		e.preventDefault();
		var triggerLink = jQuery.accordion.getTriggerLink(e);
		// check if current element is NOT open -> close all elements, open the current one
		if(!jQuery(triggerLink).hasClass(jQuery.accordion.openClass)) {
			// hide current open element
			jQuery("a."+jQuery.accordion.showHideTriggerClass).each(function(i) {

				if(jQuery(this).hasClass(jQuery.accordion.openClass)) {
					// hide target <div>
					var closeTarget = jQuery.accordion.getHashFromURL(jQuery(this).attr("href"));
					jQuery("#"+closeTarget).slideToggle(jQuery.accordion.slideSpeed);
					// remove "open" class
					jQuery(this).removeClass(jQuery.accordion.openClass);
				}
				
			});
			var target = jQuery.accordion.getHashFromURL(jQuery(triggerLink).attr("href"));
			// open target item with given ID
			if(target) {
				// add "open" class to clicked element
				jQuery(triggerLink).addClass(jQuery.accordion.openClass);
				jQuery("#"+target).slideToggle(jQuery.accordion.slideSpeed);
			}
		}
	},
	
	showHideItem: function(e) {
		e.preventDefault();
		
		var triggerLink = jQuery.accordion.getTriggerLink(e);
		
		var target = jQuery.accordion.getHashFromURL(jQuery(triggerLink).attr("href"));
		// show/hide target item with given ID
		if(target) {
			jQuery(triggerLink).toggleClass(jQuery.accordion.openClass);
			jQuery("#"+target).slideToggle(jQuery.accordion.slideSpeed);
		}			
	},
	
	/**
	 * Return the a element of the event (if e.target is an a element: return that, else look up the DOM for the next parent a element)
	 *
	 * @return DOM-object
	 */
	getTriggerLink: function(e) {
		return (e.target.tagName == 'A') ? e.target : jQuery(e.target).parents('a').first();
	},
	
	getHashFromURL: function(url) {
		if(!url) return false;
		var hashPos = url.indexOf('#');
		return url.substring(hashPos+1, url.length);
	}

}

/**
 * mobileTouch
 * 
 * Adds ability to use a HTML document like an app.
 * It creates virtual "pages" from HTML elements that can be slided in an out.
 * A history and back button provide provide App-typical "back" functionality
 * 
 * Example HTML:
 * 
 * <header id="mt-header">
 *		<h1 id="mt-page-title"></h2>
 *		<a href="#back" id="mt-nav-back">Zurück</a>
 * </header>
 * 
 * <div id="mt-content" role="main">
 *		
 *		<section id="view-main" class="mt-view">
 *		
 *			<section class="mt-page" id="home" data-mt-title="Home Screen">
 *				<div class="mt-page-inner">
 *					<ul class="mt-page-link-list">
 *						<li><a href="#page1" class="mt-page-link">Show page 1</a></li>
 *						<li><a href="#page2" class="mt-page-link">Show page 2</a></li>
 *						<li><a href="/url-to-external-resource" class="mt-page-link">Show page 3 with AJAX request</a></li>
 *					</ul>
 *				</div>
 *			</section>
 *		
 *			<section class="mt-page" id="page1" data-mt-title="Page 1">
 *				<div class="mt-page-inner">
 *					<h2>Hello I´m page 1</h2>
 *				</div>
 *			</section>
 *		
 *			<section class="mt-page weekly-view" id="page2" data-mt-title="Page 2 with Accordion">
 *				<div class="mt-page-inner">
 *					<h2>I´m page 2 with accordion</h2>
 *					<div class="mt-accordion">
 *						<h3><a href="#accordion-container1" class="mt-accordion-trigger-link" data-date="2013-01-14">Show item 1</a></h3>
 *						<div id="accordion-container1">
 *							<p>Hello I´m the content of item 1</p>
 *						</div>
 *					</div>
 *				</div>
 *			</section>
 *			
 *		</section>
 *		
 *		<section id="view-messages" class="mt-view">
 *			
 *			<section class="mt-page" id="page-messages" data-mt-title="Messages">
 *				<div class="mt-page-inner">
 *					<h2>Content here</h2>
 *				</div>
 *		
 *		</section>
 *		
 *		...
 *		
 * </div>
 * 
 * <nav role="navigation" class="mt-nav mt-nav-item-count-3" id="custom-nav-id">
 *		<ul class="clearfix">
 *			<li class="current"><a href="#view-main" class="mt-view-link icon-food">Speiseplan</a></li>
 *			<li><a href="#view-messages" class="mt-view-link icon-comment">Nachrichten</a></li>
 *			...
 *		</ul>
 * </nav>
 * 
 */
jQuery.mobileTouch = {
	
	// VARIABLES
	appActiveClass: 'mt-active', // class that is added to body when app is active
	appStandaloneClass: 'mt-standalone', // class that is added to body when browser is in standalone mode
	appContainerID: 'mt-content', // ID of the container that will contain the app
	appHeaderID: 'mt-header', // ID of app header
	appHeaderMultiTitleClass: 'mt-multiheading', // class that indicates that there are multiple headings in the header
	appNavID: 'mt-nav', // ID of app navigation (triggers the different views)
	appNavItemCount: 'mt-nav-item-count', // Class for nav item count (if 3 items in nav: mt-nav-item-count-3)
	appBackBtnID: 'mt-nav-back', // ID of link that is used as back button
	appNavHideClass: 'mt-nav-hide', // Class that triggers hiding of navigation (class name must be added to mt-page container)
	appViewClass: 'mt-view', // class name for container with 'views' which can be triggered via the navigation
	appViewCurrentClass: 'mt-current-view', // class name for current view (visible)
	appViewLinkClass: 'mt-view-link', // class name for all links that trigger changes of views
	appPageClass: 'mt-page', // class name for container that contains virtual pages
	appPageCurrentClass: 'mt-current-page', // class name for current page (visible)
	appPageLinkClass: 'mt-page-link', // class name for all links that trigger changes of pages
	appCurrDateLink: 'mt-current-date-link', // link to page with menu of current day
	appVotingLinkClass: 'mt-voting-link', // class name for all links that trigger changes of pages
	appRatingPageClass: 'mt-page-dish-rating', // class name for dish rating pages
	appRatingFormClass: 'mt-dish-rating-form', // class name for form to submit dish ratings
	appVotingPageClass: 'mt-page-voting', // class name for dish rating pages
	appVotingResultsURL: 'votingresults', // url for votingresults - relative to current app URL
	appRecipeSubmissionFormClass: 'mt-recipe-submission-form', // class name for form to submit recipes (Wunschtopf)
	appTitleID: 'mt-page-title', // title of container for app title
	appSubtitleID: 'mt-page-subtitle', // title of container for app subtitle
	appLoaderID: 'mt-loading-indicator', // ID for the element that will show up when ajax requests are loaded
	appLoaderTextID: 'mt-loading-text', // ID for loading text
	appHistoryVar: 'mt-history', // variable name of the history (stores the page history on each view)
	appCurrPageIDVar: 'mt-current-page-id', // variable name for the current page id (stored on each view)
	appStartupImageID: 'mt-startup-image', // ID for startup image container
	appStartupActiveClass: 'mt-startup-image-active', // Class that is added to body tag while startup image is shown
	appCurrClass: 'current', // class for selected / active elements
	appStarRatingClass: 'mt-star-rating', // class for <select> elements that are turned into star-ratings
	appOverlayLinkClass: 'mt-overlay-link', // class for links that point to overlays
	appOverlayContainerPrefix: 'mt-overlay', // prefix for dynamically generated overlay IDs
	appOverlayClass: 'mt-overlay', // class for overlay container
	appInfoMessageOverlay: 'mt-page-info-message-overlay', // class for info message page, that will be rendered as overlay on startup
	appDataAttrPageTitle: 'data-mt-title', // attribute of page container, which contains the page title
	appDataAttrPageSubtitle: 'data-mt-subtitle', // attribute of page container, which contains the page subtitle
	appDataAttrMenuTitle: 'data-mt-menu-title', // attribute that contains the menu title
	appDataAttrIcon: 'data-mt-icon-class', // attribute name that contains icon class names (used on views)
	appDataAttrNavID: 'data-mt-nav-id', // attribute name that contains the nav id for a mt-page-link
	
	// RESTAURANT OVERVIEW
	appRestContainerID: 'view-restaurants', // ID of mt-view container with restaurant selection
	appRestDataContainerClass: 'restaurant-container', // Class of container with data of selected restaurant (when multiple restaurants are available)
	appRestLinkClass: 'mt-restaurant-load-link', // Class for links that load content of a restaurant
	appRestSelectedIDVar: 'selected-restaurant-id', // Variable Name for local Storage where Subsite ID of selected restaurant will be stored
	appRestDataAttrRestID: 'data-mt-restaurant-id', // attribute name on link with restaurant id
	appRestDataAttrRestBaseURL: 'data-mt-restaurant-base-url', // attribute name on link with restaurant base URL
	appRestMenuViewID: 'view-main', // ID of the view with menu
	
	// loader
	loaderFadeSpeed: 100, // fade out speed in ms for loading idicator
	loaderText: '', // text that is shown under loading animation
	
	// startup images
	startupImageTimeout: 3000, // time to show the startup image on first load (milliseconds)
	iosMenuCurrentDayDelay: 300, // delay before page with menu is shown on ios devices (after startup image disappears)
	
	// page transitions
	ptIsAnimating: false,
	ptEndCurrPage: false,
	ptEndNextPage: false,
	ptAnimEndEventNames: {
		'WebkitAnimation' : 'webkitAnimationEnd',
		'OAnimation' : 'oAnimationEnd',
		'msAnimation' : 'MSAnimationEnd',
		'animation' : 'animationend'
	},
	ptAnimEndEventName: null, // animation end event name
	ptSupport: Modernizr.cssanimations, // support css animations
	ptAnimationViewNext: 99, // animation number for animations to "next" views (see /css/sass/partials/_animations.css for details),
	ptAnimationViewPrev: 99, // animation number for animations to "previous" views (see /css/sass/partials/_animations.css for details)
	ptAnimationPageNext: 1, // animation number for animations to "next" pages (see /css/sass/partials/_animations.css for details),
	ptAnimationPagePrev: 2, // animation number for animations to "previous" pages (see /css/sass/partials/_animations.css for details)
	ptDataClassVar: 'originalClassList', // data variable to store the current class names
	
	// accordion setup
	weekViewClass: 'weekly-view', // class name for container that has accordion elements with week day view
	weekDayStart: 6, // First day of week: 0 = sunday, 1 = saturday, ... 6 = monday
	accScrollToCurrDay: false, // true or false: If true page is scrolled to current day accordion item
	
	// lazy loading for images
	lazyLoadClass: 'lazy', // class in <img> tag to triger lazy loading in pages
	lazyLoadOffset: 200, // scroll offset for loading images
	lazyLoadLoadedClass: 'lazy-loaded', // Class that is added when image is loaded
	
	// internal variables
	currentView: null, // DOM object of the current view
	menuCurrentDayPage: null, // DOM Object of the page with menu of the current day
	standalone: false, // true if browser is in standalone mode
	currRestaurantBaseURL: null, // the base URL of the currently active restaurant (e.g. http://myrestaurant.signage-server.de)
	
	init: function() {
		
		// add app active class
		jQuery('body').addClass(jQuery.mobileTouch.appActiveClass);
		
		// add app standalone class if browser is in standalone mode
		if(jQuery.mobileTouch.isStandalone()) {
			jQuery('body').addClass(jQuery.mobileTouch.appStandaloneClass);
			jQuery.mobileTouch.standalone = true;
		}
		
		// get current animation event names
		jQuery.mobileTouch.ptAnimEndEventName = jQuery.mobileTouch.ptAnimEndEventNames[ Modernizr.prefixed( 'animation' ) ];
		
		// setup address bar hack
		//jQuery.mobileTouch.addressBarHack();
		
		// add ajax loading container
		jQuery.mobileTouch.addAJAXLoader();	
		
		
		//console.log('CurrentRestaurantID in localStorage is:'+jQuery.Storage.get(jQuery.mobileTouch.appRestSelectedIDVar));
		
		// load content of selected restaurant
		if(jQuery.Storage.get(jQuery.mobileTouch.appRestSelectedIDVar)) {
			var currRestaurantLink = jQuery("["+jQuery.mobileTouch.appRestDataAttrRestID+"='"+jQuery.Storage.get(jQuery.mobileTouch.appRestSelectedIDVar)+"']").first();
			// check if URL for given Restaurant ID exists in current restaurant overview
			if(currRestaurantLink.length >= 1) {
				// set current base URL of restaurant
				jQuery.mobileTouch.currRestaurantBaseURL = jQuery(currRestaurantLink).attr(jQuery.mobileTouch.appRestDataAttrRestBaseURL);
				// load data from restaurant URL
				jQuery.mobileTouch.loadRestaurantContent(jQuery(currRestaurantLink).attr('href'), jQuery.Storage.get(jQuery.mobileTouch.appRestSelectedIDVar));
			}
			// set link of current restaurant as selected
			jQuery(currRestaurantLink).addClass(jQuery.mobileTouch.appCurrClass);
		}
		// setup views
		jQuery('.'+jQuery.mobileTouch.appViewClass).each(function(index){
			
			//console.log('init view: '+jQuery(this).attr('id'));
			
			// init all views, store the id of the first page in view
			jQuery.mobileTouch.initView(this);
			
			// show first view
			if(index == 0) {
				// save ID of current view
				jQuery.mobileTouch.currentView = this;
				// display first view
				jQuery(this).addClass(jQuery.mobileTouch.appViewCurrentClass);
				// save page with menu of current day if it exists in first view
				if(jQuery('#'+jQuery(this).data(jQuery.mobileTouch.appCurrPageIDVar)).find('a.'+jQuery.mobileTouch.appCurrDateLink)) {
					var menuPageHref, menuPageID = null;
					menuPageHref = jQuery('#'+jQuery(this).data(jQuery.mobileTouch.appCurrPageIDVar)).find('a.'+jQuery.mobileTouch.appCurrDateLink).first().attr('href');
					if(menuPageHref) menuPageID = jQuery.mobileTouch.getHashFromURL(menuPageHref);
					if(menuPageID) jQuery.mobileTouch.menuCurrentDayPage = jQuery('#'+menuPageID);
				}
			}
		});
		
		// show startup image
		jQuery.mobileTouch.showStartupImage();
		
		// init nav here (reusable function, because nav needs to be re- initiated when views change)
		jQuery.mobileTouch.initNav();
		
		// set app title
		jQuery.mobileTouch.setAppTitle(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar));
		
		// init Accordion links on current page in current view
		//console.log('init ACCORDION: '+jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar));
		jQuery.mobileTouch.initAccordionOnPage(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar));
		
		// init star rating
		jQuery.mobileTouch.initStarRating();
		
		// add event listener to back button
		jQuery('#'+jQuery.mobileTouch.appBackBtnID).off('tap'); // Unbind all previous event handlers
		jQuery('#'+jQuery.mobileTouch.appBackBtnID).on('tap', function(e){
			e.preventDefault();			
			jQuery.mobileTouch.showPage(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar), jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appHistoryVar)[jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appHistoryVar).length - 1].ID, 'ltr');
		});
		
		// setup lazy loading script for images (requires unveil plug-in: http://luis-almeida.github.com/unveil)
		jQuery.mobileTouch.initLazyLoad();
		
		// add Info Message as overlay
		jQuery.mobileTouch.addInfoMessageOverlay();
		
		// add Overlays
		jQuery.mobileTouch.initOverlays();
		
		// add event listener to hide address bar on mobile browsers
		jQuery(window).on("load", function(){if(!window.pageYOffset){jQuery.mobileTouch.hideAddressBar();}} );
		jQuery(window).on("orientationchange", jQuery.mobileTouch.hideAddressBar );
	},
	
	/**
	 * Init the restaurant container (appRestDataContainerClass)
	 * with its externally (ajax) loaded contents
	 */
	initRestaurantContainer: function() {
		
		// get restaurant Container
		var restaurantContainer = jQuery('.'+jQuery.mobileTouch.appRestDataContainerClass);
		
		// remove all Overlay Containers (to avoid generation of nav-elements for stray overlays)
		jQuery.mobileTouch.removeOverlayContainers();
		
		// setup views of restaurant container
		jQuery(restaurantContainer).find('.'+jQuery.mobileTouch.appViewClass).each(function(index){
			
			//console.log('init view: '+jQuery(this).attr('id'));
			
			// init all views, store the id of the first page in view
			jQuery.mobileTouch.initView(this);
			
			// show restaurant menu view
			if(jQuery(this).attr('id') == jQuery.mobileTouch.appRestMenuViewID) {
				// save ID of current view
				jQuery.mobileTouch.currentView = this;
				// display menu view
				jQuery(this).addClass(jQuery.mobileTouch.appViewCurrentClass);
				// save page with menu of current day if it exists in first view
				if(jQuery('#'+jQuery(this).data(jQuery.mobileTouch.appCurrPageIDVar)).find('a.'+jQuery.mobileTouch.appCurrDateLink)) {
					var menuPageHref, menuPageID = null;
					menuPageHref = jQuery('#'+jQuery(this).data(jQuery.mobileTouch.appCurrPageIDVar)).find('a.'+jQuery.mobileTouch.appCurrDateLink).first().attr('href');
					if(menuPageHref) menuPageID = jQuery.mobileTouch.getHashFromURL(menuPageHref);
					if(menuPageID) jQuery.mobileTouch.menuCurrentDayPage = jQuery('#'+menuPageID);
				}
			}
		});
		
		// init nav here (reusable function, because nav needs to be re- initiated when views change)
		jQuery.mobileTouch.initNav();
		
		// set app title
		jQuery.mobileTouch.setAppTitle(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar));
		
		// init Accordion links on current page in current view
		//console.log('init ACCORDION: '+jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar));
		jQuery.mobileTouch.initAccordionOnPage(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar));
		
		// init star rating
		jQuery.mobileTouch.initStarRating();
		
		// setup lazy loading script for images (requires unveil plug-in: http://luis-almeida.github.com/unveil)
		jQuery.mobileTouch.initLazyLoad();
		
		// init info message overlay
		jQuery.mobileTouch.addInfoMessageOverlay();
		
		// add Overlays
		jQuery.mobileTouch.initOverlays();
	},
	
	/**
	 * Generate HTML Code for externally loaded Content, and wrap it inside the neccessary view container
	 * Add new view to end of app container
	 * 
	 * @param	string	ID		id for the view container element
	 * @param	string	content	(optional) String with HTML content
	 * @param	String	historyPageID	(optional) ID of the page that will be added to the history of the newly created view
	 * @return	DOM-Object		the DOM object of the newly created view container
	 */
	createView: function(id, content, historyPageID) {
		var content = content ? content : false;
		
		// check if view container with ID already exists -> overwrite it
		if(jQuery('#'+id).length >= 1) jQuery('#'+id).remove();
		
		// create new container for view
		var view= jQuery('<section id="'+id+'" class="'+jQuery.mobileTouch.appViewClass+'" />');
		
		// append new view to end of app container
		jQuery(jQuery('#'+jQuery.mobileTouch.appContainerID)).append(view);
		
		// add HTML content to view
		if(content) jQuery(view).html(content);
		
		// init new view
		jQuery.mobileTouch.initView(view, historyPageID);
		
		return view;
	},
	
	/**
	 * init a view in the document:
	 * - add a history object to the view
	 * - add a currentPageID variable (to store the current page of a view)
	 * - init all contained pages
	 * 
	 * @param	DOM-Object	view	DOM Object of a view
	 * @param	String	historyPageID	(optional) ID of the page that will be added to the history of the newly created view
	 */
	initView: function(view, historyPageID) {		
		// add history object
		jQuery(view).data(jQuery.mobileTouch.appHistoryVar, []);
		
		// add given page ID to the history (as first element)
		if(historyPageID) jQuery(view).data(jQuery.mobileTouch.appHistoryVar).push({
			'ID': historyPageID, 
			'title': jQuery('#'+historyPageID).attr(jQuery.mobileTouch.appDataAttrPageTitle)
		});
		
		// add variable for current page ID
		jQuery(view).data(jQuery.mobileTouch.appCurrPageIDVar, null);
		
		// store current class names - to restore them after page transitions
		jQuery(view).data(jQuery.mobileTouch.ptDataClassVar, jQuery(view).attr('class'));
		
		// init all contained pages
		jQuery(view).find('.'+jQuery.mobileTouch.appPageClass).each(function(index){
			
			//console.log('init page: '+jQuery(this).attr('id'));
			
			// init all pages
			jQuery.mobileTouch.initPage(this);
			
			// show first page
			if(index == 0) {
				//console.log('first page is: '+jQuery(this).attr('id'));
				// save ID of current page
				jQuery(view).data(jQuery.mobileTouch.appCurrPageIDVar, jQuery(this).attr('id'));
				// display first page
				jQuery(this).addClass(jQuery.mobileTouch.appPageCurrentClass);
			}
		});
		
		// return ID of first page in this view
		return jQuery(view).data(jQuery.mobileTouch.appCurrPageIDVar);
	},
	
	/**
	 * Shows hides the given view and shows the target view
	 * 
	 * @param	String	currView		DOM Object of currently shown view
	 * @param	String	targetView		DOM Object of target view that will be shown
	 */
	showView: function(currView, targetView) {
		
		//console.log('showView');
		
		// stop if page transition is running
		if(jQuery.mobileTouch.ptIsAnimating) return;
		
		// stop if current or target ID is missing
		if(!currView || !targetView) return;
		
		var $currentView = jQuery(currView);
		var $targetView = jQuery(targetView);
		
		// check if target and current page exist and they are not the same
		if( ($currentView.length == 0 || $targetView.length == 0) || ($currentView.attr('id') == $targetView.attr('id'))) return;
		
		// lazy load contained images on first page
		$targetView.find('.'+jQuery.mobileTouch.appPageClass).first().find('img.lazy').trigger('unveil');
	   	   
		// start page transition for view
		jQuery.mobileTouch.pageTransition($currentView, $targetView, jQuery.mobileTouch.ptAnimationViewNext);
		
		// set current view
		jQuery.mobileTouch.currentView = targetView;
		
		// init nav
		jQuery.mobileTouch.showHideNavForCurrentPage();
		
		// set app Title
		jQuery.mobileTouch.setAppTitle($targetView.data(jQuery.mobileTouch.appCurrPageIDVar));
		
		// show / hide nav button
		jQuery.mobileTouch.showHideBackBtn();
		
	},
	
	/**
	 * returns the DOM object of the view the current page is in
	 * 
	 * @param	DOM-Object	page	DOM Object of a page
	 * @return	DOM-Object	view	DOM Object of the parent view
	 */
	getViewForPage: function(page) {
		return jQuery(page).parents('.'+jQuery.mobileTouch.appViewClass).first();
	},
	
	/**
	 * Generate HTML Code for externally loaded Content, and wrap it inside the neccessary page container
	 * Add new page to end of given view
	 * 
	 * @param	String	content	HTML content
	 * @param	String	id		id for the page container element
	 * @param	DOM-object view	(optional) DOM object of the view that the page will be appended to
	 */
	createPage: function(content, id, view) {
		var view = view ? view : jQuery.mobileTouch.currentView;
		// append new page to end of view container
		jQuery(view).append(content);
		var page = jQuery(view).find('.'+jQuery.mobileTouch.appPageClass+':last');
		// add id to new page
		jQuery(page).attr('id', id);
		// init new page
		jQuery.mobileTouch.initPage(page);
	},
	
	/**
	 * Init page: setup user-interface and events
	 * 
	 * @param	DOM-Object	page	DOM Object of a page
	 */
	initPage: function(page) {
		
		var $page = jQuery(page);
		
		// store current class names - to restore them after page transitions
		$page.data(jQuery.mobileTouch.ptDataClassVar, $page.attr('class'));
		
		// setup all page links
		$page.find('a.'+jQuery.mobileTouch.appPageLinkClass).each(function(index){
			jQuery(this).off('tap'); // Unbind all previous event handlers
			jQuery(this).on('tap', function(e){
				e.preventDefault();
				var target = jQuery.mobileTouch.getURLTarget(jQuery(this).attr('href'));
				if(target) {
					jQuery.mobileTouch.showPage(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar), jQuery(target).attr('id'));
				}
			});
		});
		
		// setup links that load restaurant content
		$page.find('a.'+jQuery.mobileTouch.appRestLinkClass).each(function(index){
			
			//console.log('Init Restaurant Link: '+jQuery(this).attr('href'));
			jQuery(this).off('tap'); // Unbind all previous event handlers
			jQuery(this).on('tap', function(e){
				e.preventDefault();
				// set selected restaurant to active
				jQuery('a.'+jQuery.mobileTouch.appRestLinkClass).each(function(index){
					jQuery(this).removeClass(jQuery.mobileTouch.appCurrClass);
				});
				jQuery(this).addClass(jQuery.mobileTouch.appCurrClass);
				// set current base URL of restaurant
				jQuery.mobileTouch.currRestaurantBaseURL = jQuery(this).attr(jQuery.mobileTouch.appRestDataAttrRestBaseURL);
				// load content
				var target = jQuery(this).attr('href');
				var id = jQuery(this).attr(jQuery.mobileTouch.appRestDataAttrRestID);
				//console.log('Restaurant with ID '+id+' tapped');
				if(target && id) {
					jQuery.mobileTouch.loadRestaurantContent(target, id);
				}
			});
		});
		
		// load finished dish ratings from cache
		if(jQuery(page).hasClass(jQuery.mobileTouch.appRatingPageClass)){
			
			// if dish rating with same id exists in local storage -> replace form with rating
			var pageID = jQuery(page).attr('id');
			var ratingData = jQuery.Storage.get(pageID);
			if(ratingData) {
				jQuery(page).first().html(ratingData);	
			}
		}
		
		// load voting results for current voting (if user has already submitted voting data)
		if(jQuery(page).hasClass(jQuery.mobileTouch.appVotingPageClass)){
			
			// if dish rating with same id exists in local storage -> replace form with rating
			var pageID = jQuery(page).attr('id');
			
			//console.log('Replacing existing content from Voting with pageID: '+pageID);
			
			var votingFlag = jQuery.Storage.get(pageID);
			
			if(votingFlag) {
				// generate url based on base url of current restaurant
				var restaurantURL = jQuery.mobileTouch.currRestaurantBaseURL ? jQuery.mobileTouch.currRestaurantBaseURL +  jQuery(page).attr('data-mt-content-url') : jQuery(page).attr('data-mt-content-url');
				// load voting results
				jQuery.when(jQuery.mobileTouch.ajaxRequest(restaurantURL, null, null, false)).done(function(data, status) {
				// succesfull request
					if(status == 'success') {
						// replace form with voting results
						jQuery(page).first().html(data);
						// return null (to prevent synchronus script execution before content is loaded)
						return null;
					}
				});
			}
		}		
		
		// setup all voting links
		jQuery(page).find('.'+jQuery.mobileTouch.appVotingLinkClass).each(function(index){
			jQuery(this).live('tap', function(e){
				if(!jQuery(this).data('clicked')) {
					jQuery(this).data('clicked', '1'); // set attribute to prevent multiple sendings
					jQuery.mobileTouch.dishVoting(jQuery(this).attr('href'));
				}
				e.preventDefault();
				e.stopPropagation();
				e.stopImmediatePropagation();
				return false;
			});
		});
		
		// setup all rating forms
		jQuery(page).find('.'+jQuery.mobileTouch.appRatingFormClass).each(function(index){
			jQuery(this).off('submit'); // Unbind all previous event handlers
			jQuery(this).on('submit', function(e){
				e.preventDefault();
				jQuery.mobileTouch.postDishRatingForm(jQuery(this).attr('action'), this);
			});
		});
		
		// setup all recipe submission forms
		jQuery(page).find('.'+jQuery.mobileTouch.appRecipeSubmissionFormClass).each(function(index){
			jQuery(this).off('submit'); // Unbind all previous event handlers
			jQuery(this).on('submit', function(e){
				e.preventDefault();
				jQuery.mobileTouch.postRecipeSubmissionForm(jQuery(this).attr('action'), this);
			});
		});
	},
	
	/**
	 *	Slides in a container with 'currID', slides out the container with 'currID'.
	 *	Direction of sliding is set via 'dir' (default: left to right)
	 *	
	 *	@param	String	currID		ID of currently shown container
	 *	@param	String	targetID	ID of target container that will be shown
	 *	@param	Int		animation	Direction of animation: 'ltr' = left to right, 'rtl' = right to left. Default: 'rtl'
	 */
	showPage: function(currID, targetID, dir) {
		
		// stop if page transition is running
		if(jQuery.mobileTouch.ptIsAnimating) return;
		
		// stop if current or target ID is missing
		if(!currID || !targetID) return;
		
		// set default animation
		var animation = jQuery.mobileTouch.ptAnimationPageNext;
		
		// set direction
		dir = dir ? dir : 'rtl';
		
		var $currentPage = jQuery(jQuery('#'+currID));
		var $targetPage = jQuery(jQuery('#'+targetID));
		
		// init accordion on page
		//console.log(targetID);
		jQuery.mobileTouch.initAccordionOnPage(targetID);
		
		// lazy load contained images
		$targetPage.find('img.lazy').trigger('unveil');
		
		// check if target and current page exist
		if($currentPage.length == 0 || $targetPage.length == 0) return;
		
		// setup animation values, depending on animation direction
		// set history and current item
		if(dir == 'rtl') {
			// :TODO: scrollTop is not working
			// reset top position to and scroll position to 0 => target page is viewd from its upper top
			$targetPage.scrollTop(0);
			// Add current page ID to history
			jQuery.mobileTouch.updateHistory(currID);
		}
		else {
			// set animation to page prev
			animation = jQuery.mobileTouch.ptAnimationPagePrev;
			// Remove current page ID from history (always keep the first item)
			jQuery.mobileTouch.updateHistory(null);
		}
		
		// Set App Title
		jQuery.mobileTouch.setAppTitle(targetID);
		
		// start page transition for view
		jQuery.mobileTouch.pageTransition($currentPage, $targetPage, animation);
	},
	
	/**
	 * Add class names for CSS page transition to inpage and outpage
	 * Animations will be done with CSS3 only
	 */	
	pageTransition: function($outpage, $inpage, animation) {
		
		if(jQuery.mobileTouch.ptIsAnimating) return;

		jQuery.mobileTouch.ptIsAnimating = true;
		
		var currClass = $inpage.hasClass(jQuery.mobileTouch.appViewCurrentClass) ? jQuery.mobileTouch.appViewCurrentClass : jQuery.mobileTouch.appPageCurrentClass;
		
		var outClass = '', inClass = '';

		switch( animation ) {
			
			case 1:
				outClass = 'mt-page-moveToLeft';
				inClass = 'mt-page-moveFromRight';
				break;
			case 2:
				outClass = 'mt-page-moveToRight';
				inClass = 'mt-page-moveFromLeft';
				break;
			case 3:
				outClass = 'mt-page-moveToTop';
				inClass = 'mt-page-moveFromBottom';
				break;
			case 4:
				outClass = 'mt-page-moveToBottom';
				inClass = 'mt-page-moveFromTop';
				break;
			case 5:
				outClass = 'mt-page-fade';
				inClass = 'mt-page-moveFromRight mt-page-ontop';
				break;
			case 6:
				outClass = 'mt-page-fade';
				inClass = 'mt-page-moveFromLeft mt-page-ontop';
				break;
			case 7:
				outClass = 'mt-page-fade';
				inClass = 'mt-page-moveFromBottom mt-page-ontop';
				break;
			case 8:
				outClass = 'mt-page-fade';
				inClass = 'mt-page-moveFromTop mt-page-ontop';
				break;
			case 9:
				outClass = 'mt-page-moveToLeftFade';
				inClass = 'mt-page-moveFromRightFade';
				break;
			case 10:
				outClass = 'mt-page-moveToRightFade';
				inClass = 'mt-page-moveFromLeftFade';
				break;
			case 11:
				outClass = 'mt-page-moveToTopFade';
				inClass = 'mt-page-moveFromBottomFade';
				break;
			case 12:
				outClass = 'mt-page-moveToBottomFade';
				inClass = 'mt-page-moveFromTopFade';
				break;
			case 13:
				outClass = 'mt-page-moveToLeftEasing mt-page-ontop';
				inClass = 'mt-page-moveFromRight';
				break;
			case 14:  
				outClass = 'mt-page-moveToRightEasing mt-page-ontop';
				inClass = 'mt-page-moveFromLeft';
				break;
			case 15:
				outClass = 'mt-page-moveToTopEasing mt-page-ontop';
				inClass = 'mt-page-moveFromBottom';
				break;
			case 16:
				outClass = 'mt-page-moveToBottomEasing mt-page-ontop';
				inClass = 'mt-page-moveFromTop';
				break;
			case 17:
				outClass = 'mt-page-scaleDown';
				inClass = 'mt-page-moveFromRight mt-page-ontop';
				break;
			case 18:
				outClass = 'mt-page-scaleDown';
				inClass = 'mt-page-moveFromLeft mt-page-ontop';
				break;
			case 19:
				outClass = 'mt-page-scaleDown';
				inClass = 'mt-page-moveFromBottom mt-page-ontop';
				break;
			case 20:
				outClass = 'mt-page-scaleDown';
				inClass = 'mt-page-moveFromTop mt-page-ontop';
				break;
			case 21:
				outClass = 'mt-page-scaleDown';
				inClass = 'mt-page-scaleUpDown mt-page-delay300';
				break;
			case 22:
				outClass = 'mt-page-scaleDownUp';
				inClass = 'mt-page-scaleUp mt-page-delay300';
				break;
			case 23:
				outClass = 'mt-page-moveToLeft mt-page-ontop';
				inClass = 'mt-page-scaleUp';
				break;
			case 24:
				outClass = 'mt-page-moveToRight mt-page-ontop';
				inClass = 'mt-page-scaleUp';
				break;
			case 25:
				outClass = 'mt-page-moveToTop mt-page-ontop';
				inClass = 'mt-page-scaleUp';
				break;
			case 26:
				outClass = 'mt-page-moveToBottom mt-page-ontop';
				inClass = 'mt-page-scaleUp';
				break;
			case 27:
				outClass = 'mt-page-scaleDownCenter';
				inClass = 'mt-page-scaleUpCenter mt-page-delay400';
				break;
			case 28:
				outClass = 'mt-page-rotateRightSideFirst';
				inClass = 'mt-page-moveFromRight mt-page-delay200 mt-page-ontop';
				break;
			case 29:
				outClass = 'mt-page-rotateLeftSideFirst';
				inClass = 'mt-page-moveFromLeft mt-page-delay200 mt-page-ontop';
				break;
			case 30:
				outClass = 'mt-page-rotateTopSideFirst';
				inClass = 'mt-page-moveFromTop mt-page-delay200 mt-page-ontop';
				break;
			case 31:
				outClass = 'mt-page-rotateBottomSideFirst';
				inClass = 'mt-page-moveFromBottom mt-page-delay200 mt-page-ontop';
				break;
			case 32:
				outClass = 'mt-page-flipOutRight';
				inClass = 'mt-page-flipInLeft mt-page-delay500';
				break;
			case 33:
				outClass = 'mt-page-flipOutLeft';
				inClass = 'mt-page-flipInRight mt-page-delay500';
				break;
			case 34:
				outClass = 'mt-page-flipOutTop';
				inClass = 'mt-page-flipInBottom mt-page-delay500';
				break;
			case 35:
				outClass = 'mt-page-flipOutBottom';
				inClass = 'mt-page-flipInTop mt-page-delay500';
				break;
			case 36:
				outClass = 'mt-page-rotateFall mt-page-ontop';
				inClass = 'mt-page-scaleUp';
				break;
			case 37:
				outClass = 'mt-page-rotateOutNewspaper';
				inClass = 'mt-page-rotateInNewspaper mt-page-delay500';
				break;
			case 38:
				outClass = 'mt-page-rotatePushLeft';
				inClass = 'mt-page-moveFromRight';
				break;
			case 39:
				outClass = 'mt-page-rotatePushRight';
				inClass = 'mt-page-moveFromLeft';
				break;
			case 40:
				outClass = 'mt-page-rotatePushTop';
				inClass = 'mt-page-moveFromBottom';
				break;
			case 41:
				outClass = 'mt-page-rotatePushBottom';
				inClass = 'mt-page-moveFromTop';
				break;
			case 42:
				outClass = 'mt-page-rotatePushLeft';
				inClass = 'mt-page-rotatePullRight mt-page-delay180';
				break;
			case 43:
				outClass = 'mt-page-rotatePushRight';
				inClass = 'mt-page-rotatePullLeft mt-page-delay180';
				break;
			case 44:
				outClass = 'mt-page-rotatePushTop';
				inClass = 'mt-page-rotatePullBottom mt-page-delay180';
				break;
			case 45:
				outClass = 'mt-page-rotatePushBottom';
				inClass = 'mt-page-rotatePullTop mt-page-delay180';
				break;
			case 46:
				outClass = 'mt-page-rotateFoldLeft';
				inClass = 'mt-page-moveFromRightFade';
				break;
			case 47:
				outClass = 'mt-page-rotateFoldRight';
				inClass = 'mt-page-moveFromLeftFade';
				break;
			case 48:
				outClass = 'mt-page-rotateFoldTop';
				inClass = 'mt-page-moveFromBottomFade';
				break;
			case 49:
				outClass = 'mt-page-rotateFoldBottom';
				inClass = 'mt-page-moveFromTopFade';
				break;
			case 50:
				outClass = 'mt-page-moveToRightFade';
				inClass = 'mt-page-rotateUnfoldLeft';
				break;
			case 51:
				outClass = 'mt-page-moveToLeftFade';
				inClass = 'mt-page-rotateUnfoldRight';
				break;
			case 52:
				outClass = 'mt-page-moveToBottomFade';
				inClass = 'mt-page-rotateUnfoldTop';
				break;
			case 53:
				outClass = 'mt-page-moveToTopFade';
				inClass = 'mt-page-rotateUnfoldBottom';
				break;
			case 54:
				outClass = 'mt-page-rotateRoomLeftOut mt-page-ontop';
				inClass = 'mt-page-rotateRoomLeftIn';
				break;
			case 55:
				outClass = 'mt-page-rotateRoomRightOut mt-page-ontop';
				inClass = 'mt-page-rotateRoomRightIn';
				break;
			case 56:
				outClass = 'mt-page-rotateRoomTopOut mt-page-ontop';
				inClass = 'mt-page-rotateRoomTopIn';
				break;
			case 57:
				outClass = 'mt-page-rotateRoomBottomOut mt-page-ontop';
				inClass = 'mt-page-rotateRoomBottomIn';
				break;
			case 58:
				outClass = 'mt-page-rotateCubeLeftOut mt-page-ontop';
				inClass = 'mt-page-rotateCubeLeftIn';
				break;
			case 59:
				outClass = 'mt-page-rotateCubeRightOut mt-page-ontop';
				inClass = 'mt-page-rotateCubeRightIn';
				break;
			case 60:
				outClass = 'mt-page-rotateCubeTopOut mt-page-ontop';
				inClass = 'mt-page-rotateCubeTopIn';
				break;
			case 61:
				outClass = 'mt-page-rotateCubeBottomOut mt-page-ontop';
				inClass = 'mt-page-rotateCubeBottomIn';
				break;
			case 62:
				outClass = 'mt-page-rotateCarouselLeftOut mt-page-ontop';
				inClass = 'mt-page-rotateCarouselLeftIn';
				break;
			case 63:
				outClass = 'mt-page-rotateCarouselRightOut mt-page-ontop';
				inClass = 'mt-page-rotateCarouselRightIn';
				break;
			case 64:
				outClass = 'mt-page-rotateCarouselTopOut mt-page-ontop';
				inClass = 'mt-page-rotateCarouselTopIn';
				break;
			case 65:
				outClass = 'mt-page-rotateCarouselBottomOut mt-page-ontop';
				inClass = 'mt-page-rotateCarouselBottomIn';
				break;
			case 66:
				outClass = 'mt-page-rotateSidesOut';
				inClass = 'mt-page-rotateSidesIn mt-page-delay200';
				break;
			case 67:
				outClass = 'mt-page-rotateSlideOut';
				inClass = 'mt-page-rotateSlideIn';
				break;
			case 99:
				outClass = 'mt-page-nothing';
				inClass = 'mt-page-fadeIn mt-page-ontop';
				break;

		}

		$outpage.addClass( outClass ).on( jQuery.mobileTouch.ptAnimEndEventName, function() {
			$outpage.off( jQuery.mobileTouch.ptAnimEndEventName );
			jQuery.mobileTouch.ptEndCurrPage = true;
			if( jQuery.mobileTouch.ptEndNextPage ) {
				jQuery.mobileTouch.onEndPageAnimation( $outpage, $inpage );
			}
		} );

		$inpage.addClass( inClass + ' ' + currClass ).on( jQuery.mobileTouch.ptAnimEndEventName, function() {
			$inpage.off( jQuery.mobileTouch.ptAnimEndEventName );
			jQuery.mobileTouch.ptEndNextPage = true;
			if( jQuery.mobileTouch.ptEndCurrPage ) {
				jQuery.mobileTouch.onEndPageAnimation( $outpage, $inpage );
			}
		} );

		if( !jQuery.mobileTouch.ptSupport ) {
			jQuery.mobileTouch.onEndPageAnimation( $outpage, $inpage );
		}
	},
	
	/**
	 * Reset animation variables after page animation
	 */
	onEndPageAnimation: function( $outpage, $inpage ) {
		jQuery.mobileTouch.ptEndCurrPage = false;
		jQuery.mobileTouch.ptEndNextPage = false;
		jQuery.mobileTouch.resetPage( $outpage, $inpage );
		jQuery.mobileTouch.ptIsAnimating = false;
	},

	/**
	 * Reset classnames after animation
	 */
	resetPage: function( $outpage, $inpage ) {
		
		// check if original class name of $inpage is a view or a page
		var currClass = ($inpage.data(jQuery.mobileTouch.ptDataClassVar).indexOf(jQuery.mobileTouch.appViewClass) >= 0) ? jQuery.mobileTouch.appViewCurrentClass : jQuery.mobileTouch.appPageCurrentClass;
		
		$outpage.attr( 'class', $outpage.data( jQuery.mobileTouch.ptDataClassVar ) );
		$inpage.attr( 'class', $inpage.data( jQuery.mobileTouch.ptDataClassVar ) + ' ' + currClass );
		
		// setup page
		if(currClass == jQuery.mobileTouch.appPageCurrentClass) {
			
			// Set target page as current page
			jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar, $inpage.attr('id'));

			// init nav
			jQuery.mobileTouch.showHideNavForCurrentPage();
			
			// destroy accordion of outpage
			jQuery.accordion.destroy('#'+$outpage.attr('id'));
			
			// scroll to current accordion item if it exists
			if(jQuery.mobileTouch.accScrollToCurrDay && jQuery(this).find("a."+jQuery.accordion.showHideTriggerClass+"."+jQuery.accordion.openClass).length >= 1) {
				jQuery('html, body').animate({scrollTop: jQuery(this).find("a."+jQuery.accordion.showHideTriggerClass+"."+jQuery.accordion.openClass).offset().top - jQuery('#'+jQuery.mobileTouch.appHeaderID).outerHeight()});
			}
		}
	},
	
	/**
	 * init accordion items
	 */
	initAccordionOnPage: function(pageID) {
		// if page has weekly view -> open the accordion item corresponding to the current day
		// needs attribute jQuery.mobileTouch.weekDateAttr to be set to the current date
		
		//console.log('init accordion on page: '+pageID);
		var currDate = (jQuery('#'+pageID).hasClass(jQuery.mobileTouch.weekViewClass)) ? jQuery.mobileTouch.dateFormat() : false;
		jQuery.accordion.init('#'+pageID, -1, currDate); // init accordion on given page
	},
	
	/**
	 * creates dom structure for navigation with the given navID and the links to the given viewIDs:
	 * - creates a mt-view-link for each view
	 * 
	 * @param	string	navID	(optional) id for the navigation. Default: jQuery.mobileTouch.appNavID
	 * @param	array	viewIDs	(optional) array of ids of views that are linked in navigation. Default: All existing views in the html document
	 * @return	DOM-Object	nav	DOM object of the newly created navigation
	 */ 
	createNav: function(navID, viewIDs) {
		var nav = false;
		
		// set default values for parameter
		navID = navID ? navID : jQuery.mobileTouch.appNavID;
		if(viewIDs && viewIDs instanceof Array) {
			viewIDs = viewIDs;
		}
		else {
			// get IDS of all existing views in html document
			viewIDs = new Array();
			jQuery('.'+jQuery.mobileTouch.appViewClass).each(function() {
				viewIDs.push(jQuery(this).attr('id'));
			});
		}
		
		// remove nav if it already exists
		if(jQuery('#'+navID).length >= 1) jQuery('#'+navID).remove();
		
		//console.log('typ von viewID: '+typeof(viewIDs));
		
		
		// get all views
		var viewSelectorString = '#'+viewIDs.join(', #');
		//console.log('selector String: '+viewSelectorString);
		var views = jQuery(viewSelectorString);
		if(views.length > 1) {
			
			// create new nav
			nav = jQuery('<nav role="navigation" class="'+jQuery.mobileTouch.appNavID+' '+jQuery.mobileTouch.appNavItemCount+'-'+viewIDs.length+'" id="'+navID+'" />');
			var navList = jQuery('<ul class="clearfix" />');
			// create list item for each view
			jQuery(views).each(function(index) {
				var $this = jQuery(this);
				//console.log('creating nav item for '+$this.attr('id'));
				// get icon class from view container (data-icon-class="icon-xyz")
				var iconClass = $this.attr(jQuery.mobileTouch.appDataAttrIcon);
				var menuTitle = $this.attr(jQuery.mobileTouch.appDataAttrMenuTitle);
				var navListItem = jQuery('<li><a href="#'+$this.attr('id')+'" class="'+jQuery.mobileTouch.appViewLinkClass+' '+iconClass+'">'+menuTitle+'</a></li>');
				jQuery(navList).append(navListItem);
			});
			jQuery(nav).append(navList);
			jQuery('body').append(nav);
		}
		
		return nav;
	},
	
	/**
	 * init navigation (bottom nav)
	 * - add event handler to links
	 * - show / hide nav, depending on current view and page (some pages may not support nav)
	 */
	initNav: function() {
		var $navContainer = jQuery(jQuery('#'+jQuery.mobileTouch.appNavID));		
		
		// create nav
		// if nav with same ID already exists it will be removed and created new
		var nav = jQuery.mobileTouch.createNav();
		$navContainer = jQuery(nav);
		
		// add event handler to each link
		$navContainer.find('.'+jQuery.mobileTouch.appViewLinkClass).each(function(index){
			
			// set current view 
			//console.log('Current View: '+jQuery(jQuery.mobileTouch.currentView).attr('id')+' Current Nav Link ID: '+jQuery.mobileTouch.getHashFromURL(jQuery(this).attr('href')));
			if(jQuery(jQuery.mobileTouch.currentView).attr('id') == jQuery.mobileTouch.getHashFromURL(jQuery(this).attr('href'))) jQuery(this).parents('li').first().addClass(jQuery.mobileTouch.appCurrClass);
			jQuery(this).on('tap', function(e){
				e.preventDefault();
				
				// stop if page transition is running
				if(jQuery.mobileTouch.ptIsAnimating) return;
				
				var targetView = jQuery.mobileTouch.getHashFromURL(jQuery(this).attr('href'));
				// remove current class from previously selected element
				$navContainer.find('.'+jQuery.mobileTouch.appViewLinkClass).each(function(){
					jQuery(this).parents('li').first().removeClass(jQuery.mobileTouch.appCurrClass);
				});
				// set clicked item as current
				jQuery(this).parents('li').first().addClass(jQuery.mobileTouch.appCurrClass);
				
				// show view
				jQuery.mobileTouch.showView(jQuery.mobileTouch.currentView, jQuery('#'+targetView));
			});
		});
		
		// if current page has no nav -> hide nav
		jQuery.mobileTouch.showHideNavForCurrentPage();
	},
	
	
	/**
	 * shows or hides the nav depending on current page
	 * If current page has appNavHideClass -> nav will be hidden
	 */
	showHideNavForCurrentPage: function() {
		//console.log('show or hide nav: '+jQuery('#'+jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar)).attr('class'));
		var show = true;
		if(jQuery('#'+jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar)).hasClass(jQuery.mobileTouch.appNavHideClass)) {
			show = false;
			//console.log('current page has hidden nav');
		}
		jQuery.mobileTouch.showHideNav(show);
	},
	
	
	/**
	 * Show or hide navigation
	 * 
	 * @param boolean	show	if true: navigation is shown, if false, navigation is hidden
	 */
	showHideNav: function(show) {
		var $navContainer = jQuery(jQuery('#'+jQuery.mobileTouch.appNavID));
		if(show) {
			$navContainer.fadeIn(jQuery.mobileTouch.loaderFadeSpeed);
		}
		else {
			$navContainer.fadeOut(jQuery.mobileTouch.loaderFadeSpeed);
		}
	},
	
	/**
	 * show or hide nav button, depending on history state of current view
	 */
	showHideBackBtn: function() {
		(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appHistoryVar).length >= 1) ? jQuery('#'+jQuery.mobileTouch.appBackBtnID).show() : jQuery('#'+jQuery.mobileTouch.appBackBtnID).hide();
	},
	
	/**
	 * set width and height of viewport to app container and other app elements
	 */
	setAppDimensions: function() {
		jQuery.mobileTouch.windowWidth = jQuery(window).width();
		jQuery.mobileTouch.windowHeight = jQuery(window).height();		
		// set width and height of app container
		jQuery('#'+jQuery.mobileTouch.appContainerID).width(jQuery.mobileTouch.windowWidth).height(jQuery.mobileTouch.windowHeight);
	},
	
	/**
	 * sets the app title for the given page ID
	 * -> reads the attribute from page container that is set via jQuery.mobileTouch.appDataAttrPageTitle
	 * 
	 * @param	String	pageID	
	 */
	setAppTitle: function(pageID) {
		var title = jQuery('#'+pageID).attr(jQuery.mobileTouch.appDataAttrPageTitle);
		var subtitle = jQuery('#'+pageID).attr(jQuery.mobileTouch.appDataAttrPageSubtitle);
		
		if(typeof subtitle !== 'undefined' && subtitle !== false) {
			// set class to indicate if subtitle exists
			jQuery('#'+jQuery.mobileTouch.appHeaderID).addClass(jQuery.mobileTouch.appHeaderMultiTitleClass);
		}
		else {
			subtitle = '';
			jQuery('#'+jQuery.mobileTouch.appHeaderID).removeClass(jQuery.mobileTouch.appHeaderMultiTitleClass);
		}
		//console.log(title);
		jQuery('#'+jQuery.mobileTouch.appTitleID).text(title);
		jQuery('#'+jQuery.mobileTouch.appSubtitleID).text(subtitle);
	},
	
	/**
	 * Add element for Ajax loading indicator and spinner animation
	 */
	addAJAXLoader: function(message) {
		// add loading container
		var loader = jQuery('<div/>').attr('id', jQuery.mobileTouch.appLoaderID).hide();
		jQuery('#'+jQuery.mobileTouch.appContainerID).append(loader);
		// add spinner to current element
		var opts = {
			lines: 13, // The number of lines to draw
			length: 6, // The length of each line
			width: 2, // The line thickness
			radius: 9, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			color: '#fff', // #rgb or #rrggbb
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2e9, // The z-index (defaults to 2000000000)
			top: 'auto', // Top position relative to parent in px
			left: 'auto' // Left position relative to parent in px
		};
		var spinner = new Spinner(opts).spin();
		jQuery(loader).append(spinner.el);
		jQuery(loader).off('tap'); // Unbind all previous event handlers
		jQuery(loader).on('tap', function(e) {
			e.preventDefault();
			jQuery(this).fadeOut(jQuery.mobileTouch.loaderFadeSpeed);
		});
		// add loading text
		jQuery(loader).append(jQuery('<p id="'+jQuery.mobileTouch.appLoaderTextID+'">'+jQuery.mobileTouch.loaderText+'</p>'));
	},
	
	/**
	 * Creates overlay with title and content
	 * Overlay has close button, if an ID is given and showOnce is set
	 * an overlay that was was closed once, will not show up again
	 * (ID of Overlay is stored in local storage)
	 * 
	 * @param String title Title of the overlay
	 * @param String content HTML Content of the overlay
	 * @paran id String (optional) data ID that will be used to determine if overlay was shown before
	 * @param showOnce Boolean (optional) if true the overlay will not be shown again, after it was closed
	 * @return overlayContainer Object Returns the object of the overlay container
	 */
	createOverlay: function(title, content, id, showOnce) {
		
		// init variables
		title = title ? title : '';
		content = content ? content : '';
		id = id ? id : false;
		showOnce = showOnce ? true : false;
		
		//console.log('title: '+title, 'content: '+content, 'id: '+id);
		
		// do not create overlays that were already shown and closed
		// or that already exist in DOM
		if(!id || (id && !jQuery.Storage.get(id))) {
			
			// add overlay container
			var overlayContainer = jQuery('<section/>').attr({'class': jQuery.mobileTouch.appViewClass + '  ' + jQuery.mobileTouch.appOverlayClass, 'data-id': id, 'id': jQuery.mobileTouch.appOverlayContainerPrefix+id});
			jQuery('body').append(overlayContainer);
			
			// hide overlay initially
			jQuery(overlayContainer).css({'display': 'none'});
				
			var overlay = jQuery('<div/>').attr({'class': 'mt-page'});
			jQuery(overlayContainer).append(overlay);

			// add title container
			jQuery(overlayContainer).append('<div class="mt-header"><h2 class="mt-page-title">'+title+'</h2></div>');

			// add footer container
			var footer = jQuery('<div class="mt-nav"></div>');
			var closeBtn = jQuery('<a href="#" class="button icon-cancel">Schließen</a>');
			jQuery(footer).append(closeBtn);
			jQuery(overlayContainer).append(footer);

			jQuery(closeBtn).off('tap'); // Unbind all previous event handlers
			jQuery(closeBtn).on('tap', function(e) {
				e.preventDefault();
					
				// get overlay container
				var overlayContainer = jQuery(this).parents().find('.'+jQuery.mobileTouch.appOverlayClass).first();
				
				// if showOnce is set - store current id, fade out and remove container
				if(showOnce) {
					// save info message ID of info message -> do not show again on next startup
					jQuery.Storage.set(jQuery(overlayContainer).attr('data-id'), '1');
					// fadeOut and remove DOM Object
					jQuery.mobileTouch.hideRemoveOverlay(overlayContainer);
				}
				// else fade out container
				else {
					// fadeOut DOM Object
					jQuery.mobileTouch.hideOverlay(overlayContainer);
				}
				
			});
			
			// add content
			jQuery(overlay).append(content);
				
			// trigger lazy load of contained images
			jQuery.mobileTouch.initLazyLoad(overlay);
			
			return overlayContainer;
		}	
	},
	
	
	/**
	 * Removes all existing overlay containers in the current app
	 */
	removeOverlayContainers: function() {
		jQuery('section.'+jQuery.mobileTouch.appOverlayClass).remove();
	},
	
	/**
	 * Show overlay
	 * 
	 * @param {Object} overlay Object of the overlaycontainer
	 */
	showOverlay: function(overlay) {
		jQuery(overlay).fadeIn(jQuery.mobileTouch.loaderFadeSpeed);
		// trigger lazy load of contained images (only works when images are not set to display: none)
		jQuery.mobileTouch.initLazyLoad(overlay);
	},
	
	/**
	 * Hide overlay
	 * Fade out overlayontainer
	 * 
	 * @param {Object} overlay Object of the overlaycontainer
	 */
	hideOverlay: function(overlay) {
		jQuery(overlay).fadeOut(jQuery.mobileTouch.loaderFadeSpeed);
	},
	
	/**
	 * Hide overlay
	 * Fade out overlayontainer then remove DOM object
	 * 
	 * @param {Object} overlay Object of the overlaycontainer
	 */
	hideRemoveOverlay: function(overlay) {
		jQuery(overlay).fadeOut(jQuery.mobileTouch.loaderFadeSpeed, function() { jQuery(this).remove(); });
	},
	
	/**
	 * Init all Overlays
	 * Find Links with 'mt-overlay-link' class and add their targets as Overlays (hidden)
	 * A click on the link shows the corresponding overlay
	 */
	initOverlays: function() {
		
		// get overlay links
		var overlayLinks = jQuery('a.'+jQuery.mobileTouch.appOverlayLinkClass);
		// add overlays for all existing overlay links
		if(jQuery(overlayLinks).length) {
			
			jQuery(overlayLinks).each(function(index) {
				var overlayObject = jQuery('#'+jQuery.mobileTouch.getHashFromURL(jQuery(this).attr('href')));
				var overlayContainer = jQuery('#'+jQuery.mobileTouch.appOverlayContainerPrefix+jQuery(overlayObject).attr('id'));
				
				//console.log('current: object: '+jQuery.mobileTouch.getHashFromURL(jQuery(this).attr('href')));
				
				
				// if target exists
				if(jQuery(overlayObject).length) {
					
					//console.log('target object exists');
					
					// if overlay container does not exist yet -> create it
					if(!jQuery(overlayContainer).length) {
						
						//console.log('overlay container does not exist yet. CREATE: '+jQuery(overlayObject).attr('id'));
						
						var infoMessageTitle = jQuery(overlayObject).attr(jQuery.mobileTouch.appDataAttrPageTitle);
						var infoMessageContent = jQuery(overlayObject).html();

						// create InfoMessage as Overlay
						var overlay = jQuery.mobileTouch.createOverlay(infoMessageTitle, infoMessageContent, jQuery(overlayObject).attr('id'), false);
					}

					// set link to trigger showing overlay
					jQuery(this).off('tap');
					jQuery(this).on('tap', function(e) {
						e.preventDefault();
						jQuery.mobileTouch.showOverlay(jQuery('#'+jQuery.mobileTouch.appOverlayContainerPrefix+jQuery(overlayObject).attr('id')));
					});
				}
			});
		}		
	},
	
	/**
	 * Add info message as overlay on top of app UI
	 * InfoMessage with class 'mt-page-info-message-overlay' will be cloned as overlay
	 * Overlay has close button
	 * If a InfoMessage was closed once, it will not show up again
	 * (ID of Infomessage is stored in local storage)
	 */
	addInfoMessageOverlay: function() {
		
		// get info mesage content
		var infoMessage = jQuery('#view-messages').find('.'+jQuery.mobileTouch.appInfoMessageOverlay).first();
		// only show infoMessage if it exists
		if(infoMessage.length == 1) {
			var infoMessageTitle = jQuery(infoMessage).attr(jQuery.mobileTouch.appDataAttrPageTitle);
			var infoMessageContent = jQuery(infoMessage).html();

			// create InfoMessage as Overlay
			var overlay = jQuery.mobileTouch.createOverlay(infoMessageTitle, infoMessageContent, jQuery(infoMessage).attr('id'), true);
			jQuery.mobileTouch.showOverlay(overlay);
		}		
	},
	
	/**
	 * shows startup image on first initialisation
	 * do not show startup image if app runs in web-app mode
	 * 
	 * @param boolean iphoneOnly if true the startup image will only be shown on iphones. default: false
	 */
	showStartupImage: function(iphoneOnly) {
		
		// don´t show startup image if page runs in web-app mode (startup image will be shown by os)
		if(jQuery.mobileTouch.standalone) {
			// show menu of current day
			jQuery.mobileTouch.showMenuCurrentDay(jQuery.mobileTouch.iosMenuCurrentDayDelay);
			return;
		}
		
		iphoneOnly = iphoneOnly ? true : false;
		
		if(!iphoneOnly || (iphoneOnly && navigator.userAgent.indexOf('iPhone') != -1)) {
			// add startup image class to body
			jQuery('body').addClass(jQuery.mobileTouch.appStartupActiveClass);
			
			// generate startup image container
			var imgContainer = jQuery('<div/>').attr('id', jQuery.mobileTouch.appStartupImageID);
			jQuery('#'+jQuery.mobileTouch.appContainerID).append(imgContainer);

			// fadeout and remove startup image container after timeout
			window.setTimeout(function(){
				jQuery('body').removeClass(jQuery.mobileTouch.appStartupActiveClass);
				jQuery('#'+jQuery.mobileTouch.appStartupImageID).fadeOut(300, function(){
					jQuery(this).remove();
					// show menu of current day
					jQuery.mobileTouch.showMenuCurrentDay();
				});
			}, jQuery.mobileTouch.startupImageTimeout);
		}
	},
	
	/**
	 * shows page with menu of current day
	 */
	showMenuCurrentDay: function(delay) {
		delay = delay ? delay : 0;
		// show menu of current day
		if(jQuery.mobileTouch.menuCurrentDayPage) {
			window.setTimeout(function()
				{
					jQuery.mobileTouch.showPage(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar), jQuery(jQuery.mobileTouch.menuCurrentDayPage).attr('id'));
				}, 
				delay
			);			
		}
	},
	
	/**
	 * Add or remove IDs of pages to history Array
	 * 
	 * @param	String	ID	ID of the page, if ID = null the last element from history will be removed
	 */
	updateHistory: function(ID) {
		// add or remove ID to/from history
		if(ID) {
			jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appHistoryVar).push({
				'ID': ID, 
				'title': jQuery('#'+ID).attr(jQuery.mobileTouch.appDataAttrPageTitle)
			});
		}
		else {
			jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appHistoryVar).pop();
		}
		
		// Show / hide back button
		jQuery.mobileTouch.showHideBackBtn();
	},
	
	/**
	 * get integer value for current day (0 = Monday, etc.)
	 * 
	 * @return Integer
	 */
	getCurrDayNum: function() {
		var d = new Date();
		return (d.getDay() + jQuery.mobileTouch.weekDayStart) %7; 
	},
	
	/**
	 * get date as String in format 'yyyy-mm-dd' eg. '2013-01-15'
	 */
	dateFormat: function() {
		var d = new Date();
		var day = d.getDate();
		var month = d.getMonth()+1;
		var year = d.getFullYear();
		
		// make 2 digits day and month eg. '01' instead of '1'
		day = (day < 10 ? '0' : '') + day
		month = (month < 10 ? '0' : '') + month
		
		return year+'-'+month+'-'+day;
	},
	
	/**
	 * get current scroll position of html document
	 * --> respects differences in Webkit and Firefox
	 * 
	 * @return Integer
	 */
	getScrollTop: function() {
		var htmlScrollTop = jQuery('html').scrollTop();
		var bodyScrollTop = jQuery('body').scrollTop();
		
		return (htmlScrollTop >= bodyScrollTop) ? htmlScrollTop : bodyScrollTop;
	},
	
	/**
	 * Checks if given string is a valid URL or a hash that points to an existing element on the current page
	 * 
	 * @param	String	url		String with a aurl or a hashtag (e.g. 'myurl/my-subfolder/file.html' or '#myID' or 'myinternalurl#myID')
	 * @return	Object	target	Object with the following content: 
	 *							target[{
	 *								'type':		null if the url does not point to a valid element or url, 'url' if external url, 'id' if element on current page,
	 *								'id':		ID of the target element. If type = 'url' an ID with the url-name is generated, if type = id the id
	 *								'html':		[optional] - if type = 'url' => the html content of the ajax request
	 */ 
	getURLTarget: function(url) {
		//setup target object
		var target = {
			'type': null,
			'id':	null,
			'html':	null
		};
		var hash = jQuery.mobileTouch.getHashFromURL(url);
		var urlToID = jQuery.mobileTouch.urlToID(url);
		
		// check if hash exists and is an existing id on the page
		if(hash && jQuery('#'+hash).length >= 1) {
			target.type = 'id';
			target.id = hash ;
			return target;
		}
		// if an id with the content of an url already exists
		else if(urlToID && jQuery('#'+urlToID).length >= 1) {
			target.type = 'id';
			target.id = urlToID ;
			return target;
		}
		// check if URL points to an existing URL
		// -> check that an url exists to avoid loading the page itself (href="#")
		else if(urlToID && !hash) {
			
			jQuery.when(jQuery.mobileTouch.ajaxRequest(url)).done(function(data, status) {
				// succesfull request
				if(status == 'success') {
					// set target object
					target.type = 'url';
					target.id = urlToID;
					target.html = data;
					
					// create new page for loaded content, add ID of current page to history of loaded page
					jQuery.mobileTouch.createPage(data, urlToID);
					
					// show page and return null (to prevent synchronus script execution before content is loaded)
					jQuery.mobileTouch.showPage(jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar), target.id);
					return null;
				}
			});
		}
	},
	
	/**
	 * Sends a dish voting URL request and inserts the reults in the current page
	 * 
	 * @param	String	url		String with a url (e.g. 'vote/id=1')
	 * @return	null		
	 */ 
	dishVoting: function(url) {
		
		// generate url for dish voting based on base url of current restaurant
		var restaurantURL = jQuery.mobileTouch.currRestaurantBaseURL ? jQuery.mobileTouch.currRestaurantBaseURL +  url: url;
		
		jQuery.when(jQuery.mobileTouch.ajaxRequest(restaurantURL)).done(function(data, status) {
			
			// succesfull request
			if(status == 'success') {
				var currPageID = jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar);
				// replace current page content with results of current voting
				jQuery(jQuery('#'+currPageID)).first().html(data);
				
				// setup lazy loading script for images in the inserted html
				jQuery.mobileTouch.initLazyLoad('#'+currPageID);
				
				// set flag to localStorage, that user voted on current dish voting (via id)
				jQuery.Storage.set(currPageID, 'voted');
				
				// reset top position to and scroll position to 0 => target page is viewd from its upper top
				jQuery('#'+currPageID).css('top', 0);
				jQuery('html, body').scrollTop(0);
			 
				// return null (to prevent synchronus script execution before content is loaded)
				return null;
			}
		});
	},
	
	/**
	 * Checks if given string is a valid URL or a hash that points to an existing element on the current page
	 *
	 * @param	Object	form	Form Object that will be sent via ajax
	 * @param	String	url		String with a url or a hashtag (e.g. 'myurl/my-subfolder/file.html' or '#myID' or 'myinternalurl#myID')
	 * @return	Boolean	
	 */ 
	postDishRatingForm: function(url, form) {
		
		var formdata = jQuery(form).serialize();
		
		// generate url for dish rating based on base url of current restaurant
		var restaurantURL = jQuery.mobileTouch.currRestaurantBaseURL ? jQuery.mobileTouch.currRestaurantBaseURL +  url: url;
		
		jQuery.when(jQuery.mobileTouch.ajaxRequest(restaurantURL, 'post', formdata)).done(function(data, status) {
			// succesfull request
			if(status == 'success') {
				
				var currPageID = jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar);
				
				// replace current page content with results of current voting
				jQuery(jQuery('#'+currPageID)).first().html(data);
				
				// setup lazy loading script for images in the inserted html
				jQuery.mobileTouch.initLazyLoad('#'+currPageID);
				
				// trigger lazy load of contained images
				jQuery('#'+currPageID).find('img.lazy').trigger('unveil');
				
				// reset top position to and scroll position to 0 => target page is viewd from its upper top
				jQuery('#'+currPageID).css('top', 0);
				jQuery('html, body').scrollTop(0);
				
				// store rating in localStorage to retrieve it later on
				jQuery.Storage.set(currPageID, data);
				
				// return null (to prevent synchronus script execution before content is loaded)
				return null;
			}
		});
		
	},
	
	/**
	 * Init Star Rating (via RateYo jQuery plugin)
	 * selects all elements with the mobileTouch.appStarRatingClass 
	 * 
	 * Needs the following HTML structure:
	 * 
	 * <div class="mt-star-rating"></div>
	 * <input type="hidden" name="FORMFIELDNAME" value="0" />
	 * 
	 * @link http://prrashi.github.io/rateYo
	 */
	initStarRating: function() {
		jQuery('.'+jQuery.mobileTouch.appStarRatingClass).rateYo({
			starWidth: "35px",
			normalFill: "#cccccc",
			ratedFill: "#00a2e2",
			fullStar: true
		}).on("rateyo.set", function (e, data) {
			var target = jQuery(e.target);
			while(!jQuery(target).hasClass(jQuery.mobileTouch.appStarRatingClass)) {
				target = jQuery(target).parent();
			}
			jQuery(target).next('input').val(data.rating);
        });
	},
	
	/**
	 * Checks if given string is a valid URL or a hash that points to an existing element on the current page
	 *
	 * @param	Object	form	Form Object that will be sent via ajax
	 * @param	String	url		String with a aurl or a hashtag (e.g. 'myurl/my-subfolder/file.html' or '#myID' or 'myinternalurl#myID')
	 * @return	Boolean	
	 */ 
	postRecipeSubmissionForm: function(url, form) {
		
		var formdata = jQuery(form).serialize();
		
		// generate url for recipe submission based on base url of current restaurant
		var restaurantURL = jQuery.mobileTouch.currRestaurantBaseURL ? jQuery.mobileTouch.currRestaurantBaseURL +  url: url;
		
		jQuery.when(jQuery.mobileTouch.ajaxRequest(restaurantURL, 'post', formdata)).done(function(data, status) {
			// succesfull request
			if(status == 'success') {
				
				var currPageID = jQuery(jQuery.mobileTouch.currentView).data(jQuery.mobileTouch.appCurrPageIDVar);
				
				// replace current page content with results of current voting
				jQuery(jQuery('#'+currPageID)).first().html(data);
				
				// setup lazy loading script for images in the inserted html
				jQuery.mobileTouch.initLazyLoad('#'+currPageID);
				
				// reset top position to and scroll position to 0 => target page is viewd from its upper top
				jQuery('#'+currPageID).css('top', 0);
				jQuery('html, body').scrollTop(0);
				
				// return null (to prevent synchronus script execution before content is loaded)
				return null;
			}
		});
		
	},
	
	/**
	 * init lazy loading for images in the given container
	 * 
	 * @param String container CSS selector for the container (e.g. #mycontainer), default: 'body'
	 * @return Boolean false
	 */
	initLazyLoad: function(container) {
		container = container ? container : 'body';
		
		jQuery(container).find('img.'+jQuery.mobileTouch.lazyLoadClass).unveil(jQuery.mobileTouch.lazyLoadOffset, function() {
			jQuery(this).load(function() {
			  jQuery(this).addClass(jQuery.mobileTouch.lazyLoadLoadedClass);
			});
		});
		
	},
	
	
	/**
	 * Loads Restaurant content (HTML) from a given URL and inserts the reults in the appRestaurantPageClass container
	 * 
	 * @param	String	url		String with a url (e.g. 'http://myrestaurant.de/home/web-app/ajax')
	 * @param {Integer} id		ID of the selected restaurant (Subsite ID)
	 * @return	null		
	 */ 
	loadRestaurantContent: function(url, id) {
		
		//console.log('loadRestaurantContent: '+id);
		
		jQuery.when(jQuery.mobileTouch.ajaxRequest(url)).done(function(data, status) {
			
			// succesfull request
			if(status == 'success') {

				// store ID of selected restaurant to local storage
				// -> initially load selected restaurant content on next app start
				jQuery.Storage.set(jQuery.mobileTouch.appRestSelectedIDVar, id);
					
				// clear all contents and their events from restaurant container
				jQuery('.'+jQuery.mobileTouch.appRestDataContainerClass).remove();
					
				// create new restaurant container
				var $restaurantContainer = jQuery('<div>', {class: jQuery.mobileTouch.appRestDataContainerClass});
				$restaurantContainer.insertAfter('#'+jQuery.mobileTouch.appRestContainerID);
					
				// add content to restaurant container
				$restaurantContainer.html(data);
					
				// init app with new loaded restaurant data
				jQuery.mobileTouch.initRestaurantContainer();
			 
				// return null (to prevent synchronus script execution before content is loaded)
				return null;
			}
		});
	},
	
	
	/** 
	 * sends an asynchronus ajax request to the given url and returns the result of the ajax request
	 * fades the ajax loading indicator in and out
	 * 
	 * @param	String	url		URL to send the data to
	 * @param	String	type	(optional) "get" or "post". default: "get"
	 * @param	Object	data	(optional) form data to be send
	 * @param	Boolean	showLoader	(optional) true to show loader animation, false if not. default: true
	 */
	ajaxRequest: function(url, type, data, showLoader) {
		type = type ? type : 'get';
		data = data ? data : null;
		showLoader = (showLoader === false) ? false : true;
		
		return jQuery.ajax({
			url: url,
			async: true,
			type: type,
			data: data,
			dataType: "html",
			timeout: 20000,
			beforeSend: function() {
				// show loading animation
				if(showLoader) jQuery('#'+jQuery.mobileTouch.appLoaderID).show();
			},
			error: function(){
				//console.log('ajax error');
			},
			success: function(data) {
				//console.log('ajax success');
			},
			complete: function(){
				//console.log('ajax complete');
				// hide loading animation
				if(showLoader) jQuery('#'+jQuery.mobileTouch.appLoaderID).fadeOut(jQuery.mobileTouch.loaderFadeSpeed);
			}
		});
	},
	
	/**
	 * Hides the address bar in mobile browsers
	 * @link http://davidwalsh.name/hide-address-bar
	 */
	hideAddressBar: function() {
		setTimeout( function(){window.scrollTo(0, 1);}, 50 );		
	},	
	
	/**
	 * Return height that is at least as high as window.outerHeight
	 * 
	 * @param integer	height	height in pixel
	 * @return	integer			height that is at least as tall as the viewport height
	 */
	getMinHeightForPage: function(height) {
		var minHeight = window.innerHeight + 60;
		// iphone 5, portrait
		if(window.screen.height == 568 && window.orientation == 0) {
			minHeight = 508;
		}
		// iphone 5, landscape
		else if(window.screen.height == 568) {
			minHeight = 290;
		}
		return (height < minHeight) ? (minHeight) : height;
	},
	
	/**
	 * Returns true if device is touch device
	 * 
	 * @return boolean	true if touch device, else false
	 */
	isTouchDevice: function() {
		var el = document.createElement('div');
		el.setAttribute('ongesturestart', 'return;');
		return typeof el.ongesturestart === "function";
	},
	
	/**
	 * Returns true if website is run in standalone mode
	 * @return Boolean	true if browser is in standalone mode, else false
	 */
	isStandalone: function() {
		return (window.navigator.standalone == true) ? true : false;
	},
	
	/**
	 * Prevents dropping down of address bar in mobile safari when links are clicked
	 * 
	 * @link http://blog.josemanuelperez.es/2011/06/prevent-iphone-navigation-bar-ajax-link-click/
	 */
	addressBarHack: function() {
		
		// only for devices with touch events
		//if(!jQuery.mobileTouch.isTouchDevice()) return;
		
		// remove href attribute on touchend
		jQuery('a').on('touchend', function(e){
			var $this = jQuery(this);
			var href = $this.attr('href');
			$this(this).data('href', href);
			$this.removeAttr('href');
		});
		// reset href on click
		jQuery('a').on('tap', function(e){
			var $this = jQuery(this);
			if($this.data('href')) $this.attr('href', $this.data('href'));
		});
	},
	
	/**
	 * makes a given url save to use as an ID in a html document
	 *
	 * @param	String	url			URL
	 * @param	String	IDPrefix	String that is added at the beginnig of the ID (default: 'page')
	 * @return	String
	 */
	urlToID: function(url, IDPrefix) {
		IDPrefix = IDPrefix ? IDPrefix : 'page';
		// convert url to ID (strip protocol, hashtags and get parameters, convert slashes to '-')
		var urlClean = url.replace(/^https?:\/\//, "").replace(/#[^#]*$/, "").replace(/\?[^\?]*$/, "");
		var urlToID = urlClean.replace(/\//g, '-').replace(/\./g, '-');
		return (urlToID.length >= 1) ? IDPrefix+'-'+urlToID : null;
	},
	
	/**
	 * Returns th part after '#' from a given URL
	 * e.g. 'my-url/with-sub-folder#myhashtag' => 'myhashtag'
	 * 
	 * @return String
	 */
	getHashFromURL: function(url) {
		if(!url) return null;
		var hashPos = url.indexOf('#');
		return (hashPos != -1) ? url.substring(hashPos+1, url.length) : null;
	}
};

/**
 * JQUERY INITIALISATION WHEN DOCUMENT READY
 *
 * @author Sebastian Dubbel
 *
 * @requires 
 *		jQuery library v 1.2.6+: http://jquery.com
 * @encoding UTF-8
 *
 */ 
// start required scripts when DOM is loaded
jQuery(document).ready(function(){
		
	// open external links in seperate window
	jQuery.external_links.init();
	
	// init mobileTouch app
	jQuery.mobileTouch.init();


});
