<% if $FoodCategories && DailyMenuPages %>

	<div class="weeklymenu-table-top">
		<div class="date-week">Wochenspeiseplan $DateRangeStartDate - $DateRangeEndDateWeek</div>
		<div class="header-text">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		<div class="logo">
			<img src="$ThemeDir/images/print-weeklytable-logo-casino.gif">
		</div>
	</div>

	<div class="weeklymenu weeklymenu-dishinfo">
				
		<div class="weeklymenu-header">
				<div class="border-right col-1st">&nbsp;</div>
				<% loop $DailyMenuPages %>
					<div class="date border-right col-header-date">$DateFormat("%d.%m.%Y")</div>
					<div class="price border-right col-header-kcal">kcal</div>
					<div class="kh border-right col-header-kh">KH</div>
					<div class="fp border-right col-header-fp">FP</div>
					<div class="ew col-header-ew<% if $Last %><% else %> border-right<% end_if %>">EW</div>
				<% end_loop %>
		</div>
		
		<div class="weeklymenu-wrap">		
			<% loop $FoodCategories %>
				<article class="row border-top">
					
						<div class="col-1st foodcategory<% if $Color %><% else %> foodcategory-blank<% end_if %>"<% if $Color %> style="background-color: $Color;"<% end_if %>>
							<span>$Title</span>
						</div>
						
						<% loop $DailyMenuPagesForDateRange($Up.MenuContainer.ID) %>
						<div class="dish<% if $First %> first-dish<% end_if %>">
							<% loop $DishesForCategory %>	
								<div class="dishContainer<% if $DescriptionSingleLine %><% else %> empty-cell<% end_if %>">
									<div class="dishDescription col-description">
										<span class="dishDescriptionInner">$DescriptionSingleLine</span>
									</div>
									<div class="col-kcal">$Calories</div>
									<div class="col-kh">$Carbohydrates</div>
									<div class="col-fp">$Fat</div>
									<div class="col-ew">$Protein</div>
								</div>
							<% end_loop %>
						</div>
						<% end_loop %>
				</article>
			<% end_loop %><!-- loop fc -->
		</div>
	
	</div>
	
<% end_if %>