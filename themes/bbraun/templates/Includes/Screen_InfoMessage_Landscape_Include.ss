<article class="InfoMessage<% if ImageOnly && Image %> FullScreenImage<% end_if %>" data-show-screen-duration="<% if $ShowScreenDuration > 0 %>$ShowScreenDuration<% else %>$TemplateShowScreenDuration<% end_if %>">
	<div class="inner-article">

		<% if $ImageOnly && $Image %>
			$Image.SetRatioSize(1920,1080)
		<% else %>

		<% if $Image %><div class="info-message-image">$Image.SetRatioSize(655,1080)</div><% end_if %>
			<div class="content">

				<div class="inner-content">

					<% include Screen_DailyMenu_InfoMessage_Header_Include %>

					$Content
					<% if $Top.SiteConfig.Lang_en_US %>
						<% if $Title_en_US %><h2>$Title_en_US</h2><% end_if %>
						<% if $Content_en_US %>
						<div class="lang-en-US">
							$Content_en_US
						</div>
						<% end_if %>
					<% end_if %>
				</div>
			</div>

		<% end_if %>
	</div>
</article>