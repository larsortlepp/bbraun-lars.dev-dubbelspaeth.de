<% if $AllAdditives || $AllAllergens %>
	<span class="additives">
		<% loop $AllAdditives %><% if $First %><% else %> | <% end_if %>$Number<% end_loop %><% if $AllAdditives && $AllAllergens %> | <% end_if %><% loop $AllAllergens %><% if $First %><% else %> | <% end_if %>$Number<% end_loop %>
	</span>
<% end_if %>