<% if $HasDailyMenuPagesForWeeklyTable %>
	<article class="Screen_WeeklyMenu_Table_Landscape">
		<div class="inner-article">
			<header class="title-container logo">
				<div id="inner-title-container">
					<h1>$RestaurantPage.Title</h1>
				</div>
				<% if $RestaurantPage.OpeningHours %>
				<div class="opening-hours-container">
					<p>
						Wir haben für Sie geöffnet<br />
						$RestaurantPage.OpeningHours
					</p>
				</div>
				<% end_if %>
			</header>

			<table class="weeklymenu-table">
				<thead>
					<tr>
						<th class="unstyled">&nbsp;</th>
						<th>&nbsp;</th>
						<% loop $DailyMenuPages %>
							<th class="date">$DateDayName, $DateFormat("%d.%m.%Y")</th>
						<% end_loop %>
						<th class="unstyled">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<% loop $FoodCategories %>

							<tr>
								<td class="foodcategory-pre-color<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $ColorBrightnessHEX(-0.7);"<% end_if %>></td>
								<th class="foodcategory<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $Color;"<% end_if %>>$Title</th>
								<% loop $DailyMenuPages %>
									<td class="dish"<% with $FoodCategory %><% if $Color %> style="background-color: $ColorBrightnessHEX(-0.7);"<% end_if %><% end_with %>>
										<% if $Message %>
											$Message
										<% else %>
											<% loop $DishesForCategory %>
												<div class="dishContainer clearfix">
														<span class="dishDescription">
															<span class="dishDescriptionInner">
																$DescriptionSingleLine <% include Additives_Include %>
															</span>
														</span>
														<div class="dishInfoRight">
															<% if $AllSpecialLabels %>
																<ul class="specialLabels clearfix">
																	<% loop $AllSpecialLabels %>
																		<li><% with $Image %><% with $SetRatioSize(30,30) %><img src="$URL" width="$Width" height="$Height" alt="$Title" class="specialLabelImage" /><% end_with %><% end_with %></li>
																	<% end_loop %>
																</ul>
															<% end_if %>
															<% if $Price || $PriceExtern %>
															<span class="dishPrice">
																<span class="dishPriceInner">€&nbsp;$PriceNice</span>
																<% if $PriceExtern %><span class="dishPriceSmallInner"><%t RestaurantDish.PRICEEXTERN 'Extern' %> €&nbsp;$PriceExternNice</span><% end_if %>
															</span>
															<% end_if %>
															<% if $AllDishLabels %>
															<ul class="dishLabels clearfix">
																<% loop $AllDishLabels %>
																<li class="dishLabel"><span class="dishLabelTitle">$Title<% if $RestaurantPage.Lang_en_US %><% if $Title_en_US %>/$Title_en_US<% end_if %><% end_if %></span><% if $IconUnicodeCode %> <span class="dishLabelIcon dishlabel-icon-$IconUnicodeCode"<% if $Color %> style="color: $Color;"<% end_if %>></span><% end_if %></li>
																<% end_loop %>
															</ul>
															<% end_if %>
														</div>
												</div>
											<% end_loop %>
										<% end_if %>
									</td>
								<% end_loop %>
								<td class="foodcategory-post-color<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $ColorBrightnessHEX(-0.7);"<% end_if %>></td>
							</tr>
					<% end_loop %>
				</tbody>
			</table>

			<% include WeeklyMenu_Table_Landscape_Footer_Include %>

		</div>
	</article>

	<% if $RestaurantPage.Lang_en_US %>
		<article class="Screen_WeeklyMenu_Table_Landscape">
			<div class="inner-article">
				<header class="title-container logo">
					<div id="inner-title-container">
						<h1>$RestaurantPage.Title</h1>
					</div>
					<% if $RestaurantPage.OpeningHours %>
					<div class="opening-hours-container">
						<p>
							Our opening hours<br />
							$RestaurantPage.OpeningHours
						</p>
					</div>
					<% end_if %>
				</header>

				<table class="weeklymenu-table">
					<thead>
						<tr>
							<th class="unstyled">&nbsp;</th>
							<th>&nbsp;</th>
							<% loop $DailyMenuPagesForWeeklyTable("Wochen", 1) %>
								<th class="date">$DateDayName_en_US, $DateFormat("%d.%m.%Y")</th>
							<% end_loop %>
							<th class="unstyled">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<% loop $FoodCategoriesForWeeklyTable("Wochen", 1) %>

								<tr>
									<td class="foodcategory-pre-color<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $ColorBrightnessHEX(-0.7);"<% end_if %>></td>
									<th class="foodcategory<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $Color;"<% end_if %>>$Title</th>
									<% loop $DailyMenuPagesForDateRange($Up.MenuContainer.ID) %>
										<td class="dish"<% with $FoodCategory %><% if $Color %> style="background-color: $ColorBrightnessHEX(-0.7);"<% end_if %><% end_with %>>
											<% if $Message %>
												$Message
											<% else %>
												<% loop $DishesForCategory %>
													<div class="dishContainer clearfix">
															<span class="dishDescription">
																<span class="dishDescriptionInner">
																	<% if $Description_en_US %>
																		$Description_en_USSingleLine
																	<% else %>
																		$DescriptionSingleLine
																	<% end_if %>
																	<% include Additives_Include %>
																</span>
															</span>
															<div class="dishInfoRight">
																<% if $AllSpecialLabels %>
																	<ul class="specialLabels clearfix">
																		<% loop $AllSpecialLabels %>
																			<li><% with $Image %><% with $SetRatioSize(30,30) %><img src="$URL" width="$Width" height="$Height" alt="$Title" class="specialLabelImage" /><% end_with %><% end_with %></li>
																		<% end_loop %>
																	</ul>
																<% end_if %>
																<% if $Price || $PriceExtern %>
																<span class="dishPrice">
																	<span class="dishPriceInner">€&nbsp;$PriceNice</span>
																	<% if $PriceExtern %><span class="dishPriceSmallInner"><%t RestaurantDish.PRICEEXTERN 'Extern' %> €&nbsp;$PriceExternNice</span><% end_if %>
																</span>
																<% end_if %>
																<% if $AllDishLabels %>
																<ul class="dishLabels clearfix">
																	<% loop $AllDishLabels %>
																	<li class="dishLabel"><span class="dishLabelTitle">$Title<% if $RestaurantPage.Lang_en_US %><% if $Title_en_US %>/$Title_en_US<% end_if %><% end_if %></span><% if $IconUnicodeCode %> <span class="dishLabelIcon dishlabel-icon-$IconUnicodeCode"<% if $Color %> style="color: $Color;"<% end_if %>></span><% end_if %></li>
																	<% end_loop %>
																</ul>
																<% end_if %>
															</div>
													</div>
												<% end_loop %>
											<% end_if %>
										</td>
									<% end_loop %>
									<td class="foodcategory-post-color<% if $Color %> FoodCategoryColor<% end_if %>"<% if $Color %> style="background-color: $ColorBrightnessHEX(-0.7);"<% end_if %>></td>
								</tr>
						<% end_loop %>
					</tbody>
				</table>

				<% include WeeklyMenu_Table_Landscape_Footer_Include %>

			</div>
		</article>
	<% end_if %>
<% else %>
	<article>
		<div class="inner-article">

			<% include Screen_DailyMenu_Header_Include %>

			<div id="content">
				<div class="inner-content">
					<h3 class="message">
						Kein Speiseplan verfügbar
						<% if $RestaurantPage.Lang_en_US %><br /> <span class="lang-en-US">No menu available</span><% end_if %>
					</h3>
				</div>
			</div>
		</div>
	</article>
<% end_if %>
