<?php
	
// 1. via silverstripe: constant DOMPDF_THEME_FOLDER is defined in /mysite/code/PrintTemplatePage.php
if(defined('DOMPDF_THEME_FOLDER')){
	define("DOMPDF_FONT_DIR", DOMPDF_THEME_FOLDER);
} else {
/*
 *  2. via font generator: constant DOMPDF_THEME_FOLDER is defined in /dompdf/fontgenerator/load_font_web.php
 *  we use DOMPDF_SERVER_ROOT_DIR with server doc root, complete workpath w/ theme name is build in script 
 */
	define("DOMPDF_SERVER_ROOT_DIR", $_SERVER['DOCUMENT_ROOT']);
} 
	
	
// Please refer to dompdf_config.inc.php for details on each configuration option.

//define("DOMPDF_TEMP_DIR", "/tmp");
//define("DOMPDF_FONT_DIR", DOMPDF_DIR."/lib/fonts/");
define("DOMPDF_TEMP_DIR", "/tmp");
define("DOMPDF_CHROOT", DOMPDF_DIR);

//define("DOMPDF_FONT_CACHE", DOMPDF_DIR."/lib/fonts/");
define("DOMPDF_FONT_CACHE", DOMPDF_FONT_DIR);

//define("DOMPDF_UNICODE_ENABLED", true);
//define("DOMPDF_PDF_BACKEND", "PDFLib");
define("DOMPDF_PDF_BACKEND", "PDFLib");
//define("DOMPDF_DEFAULT_MEDIA_TYPE", "print");
//define("DOMPDF_DEFAULT_PAPER_SIZE", "letter");
//define("DOMPDF_DEFAULT_FONT", "serif");
//define("DOMPDF_DPI", 72);
define("DOMPDF_ENABLE_CSS_FLOAT", true);
//define("DOMPDF_ENABLE_JAVASCRIPT", false);
define("DOMPDF_ENABLE_REMOTE", true);
//define("DEBUGPNG", true);
//define("DEBUGKEEPTEMP", true);
//define("DEBUGCSS", true);
//define("DEBUG_LAYOUT", true);
//define("DEBUG_LAYOUT_LINES", false);
//define("DEBUG_LAYOUT_BLOCKS", false);
//define("DEBUG_LAYOUT_INLINE", false);
//define("DOMPDF_FONT_HEIGHT_RATIO", 1.0);
//define("DEBUG_LAYOUT_PADDINGBOX", false);
//define("DOMPDF_LOG_OUTPUT_FILE", DOMPDF_FONT_DIR."log.htm");
//define("DOMPDF_ENABLE_HTML5PARSER", true);
//define("DOMPDF_ENABLE_FONTSUBSETTING", true);

// Authentication for the dompdf/www
//define("DOMPDF_ADMIN_USERNAME", "user");
//define("DOMPDF_ADMIN_PASSWORD", "password");

?>