<?php
/**
 * @package dompdf
 * @link    http://dompdf.github.com/
 * @author  Benj Carson <benjcarson@digitaljunkies.ca>
 * @author  Fabien Ménager <fabien.menager@gmail.com>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * 
 * Rebuild:	Command Line Interface to Web interface. by LOrtlepp / Dubbel Späth Darmstadt 09/2015
 * 
 */

// set to 1 for reporting all errors
$error_reporting = 0;

// set to 1 for deleting temp files after generating fonts
$cleanup_after_success = 1;




if($error_reporting==1){
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
} else {
	error_reporting(0);
	ini_set('display_errors', FALSE);
}

require_once "../dompdf_config.inc.php";

// collect messages...
$msg ='';


/*
* function get_available_themes()
*  - looks for existing themes and returns select menu 
******************************************************/
function get_available_themes(){
	
	$themes = array();
	$select = '<select id="themeselect" name="theme">';
	$foldercheck = '';

	// does directory "themes" exist?
	if( is_dir( DOMPDF_SERVER_ROOT_DIR.'/themes' ) ){
		
		$themes = scandir( DOMPDF_SERVER_ROOT_DIR.'/themes' );
		
		for( $i = 0; $i < count($themes); $i++ ) {
		
			// list folders only
			if($themes[$i]!=".DS_Store" && $themes[$i]!='.' && $themes[$i]!='..'){
				// options
				$selected = ($_POST['theme']==$themes[$i]) ? ' selected="selected"' : '';
				$select .= '<option value="'.$themes[$i].'"'.$selected.'>'.$themes[$i].'</option>'."\n";
			}
		}
		
		$select .= '</select>';
		
		
		// check: folder "/dompdf-fonts" vorhanden?
		for($a=0;$a<count($themes); $a++){
			
			$themefolder = scandir(DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themes[$a]);
			
			  if($themes[$a]!=".DS_Store"  && $themes[$a]!='.' && $themes[$a]!='..'){
				  
					# var_dump(DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themes[$a].'/dompdf-fonts');
				  
				if(is_dir( DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themes[$a]."/dompdf-fonts") ){
					$existing_folders[] = '<span class="green">Ordner "'.$themes[$a].'/dompdf-fonts": ok</span>';		
				} else {
					
					mkdir(DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themes[$a].'/dompdf-fonts',0755);
					$existing_folders[] = '<span class="red">Ordner "'.$themes[$a].'/dompdf-fonts": nicht gefunden.</ span><br>
					<span class="green">Ordner wurde automatisch erzeugt.</span>';
				}
			  }
		}
				
		for($b=0;$b<count($existing_folders); $b++){
			$foldercheck .= $existing_folders[$b].'<br>';
		}
		
	} else {
		echo 'Ordner "/themes" wurde nicht gefunden.';
		die();
	}
	
	return $select.'<p class="foldercheck">Suche Themeverzeichnisse:<br />'.$foldercheck.'</p>';
}



/*
* output of converter form
***********************************/
function form(){
		
	?>
	<!doctype html>
	<html>
	<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/style.css" />
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/jquery-migrate-1.2.1.min.js"></script>
	<script>
	$(function(){
		
		$('.msg').delay(3000).fadeOut(300);
		
		// initial value
		$('#themedir').val($('#themeselect').val());
		
		// get value on change
		$('#themeselect').change(function(){
			var index = $(this).prop('selectedIndex');
			
			if(index!=0 && index!=''){
				// initial index is 0, add value to hidden field only if not 0
				var v = $(this).val();
				$('#themedir').val(v);
			} else {
				$('#themedir').val('');
			}
		});
		
		
		
		
		// check if theme is selected
		$('form').submit(function(){
			
			var v = $('#themedir').val();
			
			if(v!=''){ 
				
				$('.msg-error').hide();
				return true;
				$('#form').submit();
			} else {
				$('.msg-error').text('Bitte ein Theme auswählen!').show();
				$('#themeselect').focus();
				return false;
			}
		});
	});
</script>
</head>
	
<body>
<div class="wrap">
<div class="formwrap">		
	<form enctype="multipart/form-data" method="post" id="form">
		<h1>.ttf converter for dompdf</h1>
		<small>Version of Feb 08th 2017</small>
		<ul class="form">
			<li><label>Font File (.ttf):</label> <input type="file" name="fontfile"></li>
			<li>
				<label>Theme Target Folder</label> <?php echo get_available_themes(); ?> <span class="msg-error"></span>
			</li>
			<li><label><button type="submit" id="submit">Konvertieren</button></label></li>
			<li>
				<input type="hidden" name="themedir" id="themedir" value="">
				<input type="hidden" name="send" value="1">
			</li>
		</ul>
	</form>
</div>
<pre>
Beispiel zur Namenskonvetion der CSS-Definitionen:
<strong>File: /[THEME_DIR]/dompdf-fonts/dompdf_font_family_cache.php</strong>
Zuweisung des Font-Namens im Array:
<span class="code">
&lt;?php
 '<span class="red">sourcesans</span>'<sup class="red">1)</sup> => 
	array (
	  'normal' => DOMPDF_FONT_DIR . '<span class="blue">SourceSansPro-Regular</span>'<sup class="blue">2)</sup>,
	  'bold' => DOMPDF_FONT_DIR . 'SourceSansPro-Regular',
	  'italic' => DOMPDF_FONT_DIR . 'SourceSansPro-Regular',
	  'bold_italic' => DOMPDF_FONT_DIR . 'SourceSansPro-Regular',
	),
?&gt;
</span>
Diese Anweisung ebenfalls in folgende Datei kopieren:
<strong>File: /[THEME_DIR]/dompdf-fonts/dompdf_font_family_cache.dist.php</strong>
</pre>

		

<pre>
Legende:
<span class="code">
<span class="red">
1)</span>Name der CSS-Klasse:
$heading-font: '<span class="red">sourcesans</span>', sans-serif;
$heading-line-height: 1;
</span>

<span class="code">
<span class="blue">
2)</span>Name der erzeugten Font-Datei in "/dompdf-fonts/":
<span class="blue">SourceSansPro-Regular</span>.ttf
<span class="blue">SourceSansPro-Regular</span>.ufm
<span class="blue">SourceSansPro-Regular</span>.ufm.php
</span>
</pre>
</p>

</div>
</body>
</html>




<?php
}

/*
*  execute: POST data conversion
***********************************/
$send = isset($_POST['send']) ? $_POST['send'] : '';
 
if(($send==1) && ($_FILES['fontfile']['size']>0) && (!empty($_POST['themedir']))){

	// get FILE data
	$tmpname = (isset($_FILES['fontfile']['tmp_name'])) ? $_FILES['fontfile']['tmp_name'] : '';
	$name = $_FILES['fontfile']['name'];
	
	$themedir = basename($_POST['themedir']);
	$temp_dir = DOMPDF_DIR.'/fonttemp/';
	
	
	// create tmp dir if not found
	if(!is_dir($temp_dir)){
		// $msg .=  'Directory "'.$temp_dir.'" not found, creating folder...<br>';
		mkdir($temp_dir,0755);
	}
	
	$path_parts = pathinfo($name);
	$fontname = strtolower($path_parts['filename']); // font file name in lower case
	
	$file_dest = $temp_dir.$name;
	
	//  rewrite DOMPDF_FONT_DIR for FontMetrics Class.
	define(DOMPDF_FONT_DIR, DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themedir."/dompdf-fonts/");
	
	// does the temp dir exist
	if(is_dir($temp_dir)){
		
		
		/*
			...check for files and create them if not found
			dompdf_font_family_cache.php
			dompdf_font_family_cache.dist.php
		*/
		$handle = fopen(DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themedir.'/dompdf-fonts/dompdf_font_family_cache.php', "w");
		fclose($handle);
		$handle = fopen(DOMPDF_SERVER_ROOT_DIR.'/themes/'.$themedir.'/dompdf-fonts/dompdf_font_family_cache.dist.php', "w");
		fclose($handle);
	
		
		if(is_writable($temp_dir)){
		
			// copy created font file
			if(move_uploaded_file($tmpname, $file_dest)){
				$msg .=  "Copy font file...<br>";
				install_font_family($fontname, $file_dest, $bold = null, $italic = null, $bold_italic = null, $cleanup_after_success);
			} else {
				$msg .=  'error...';
			}
		
		} else {
			echo 'Das Verzeichnis "/dompdf/<b>fonttemp</b>/" ist nicht beschreibbar.';
		}
	
	}
	
} else {
	form();
	
}






/**
 * Installs a new font family
 * This function maps a font-family name to a font.  It tries to locate the
 * bold, italic, and bold italic versions of the font as well.  Once the
 * files are located, ttf versions of the font are copied to the fonts
 * directory.  Changes to the font lookup table are saved to the cache.
 *
 * @param string $fontname    the font-family name
 * @param string $normal      the filename of the normal face font subtype
 * @param string $bold        the filename of the bold face font subtype
 * @param string $italic      the filename of the italic face font subtype
 * @param string $bold_italic the filename of the bold italic face font subtype
 *
 * @throws DOMPDF_Exception
 */
function install_font_family($fontname, $normal, $bold = null, $italic = null, $bold_italic = null, $cleanup = null) {
  
  Font_Metrics::init();
  
  
  // Check if the base filename is readable
  if ( !is_readable($normal) )
    throw new DOMPDF_Exception("Unable to read '$normal'.");

  $dir = dirname($normal);
  $basename = basename($normal);
  $last_dot = strrpos($basename, '.');
  if ($last_dot !== false) {
    $file = substr($basename, 0, $last_dot);
    $ext = strtolower(substr($basename, $last_dot));
  } else {
    $file = $basename;
    $ext = '';
  }
 
  if ( !in_array($ext, array(".ttf", ".otf")) ) {
    throw new DOMPDF_Exception("Unable to process fonts of type '$ext'.");
  }

  // Try $file_Bold.$ext etc.
  $path = "$dir/$file";
  
  $patterns = array(
    "bold"        => array("_Bold", "b", "B", "bd", "BD"),
    "italic"      => array("_Italic", "i", "I"),
    "bold_italic" => array("_Bold_Italic", "bi", "BI", "ib", "IB"),
  );
  
  foreach ($patterns as $type => $_patterns) {
    if ( !isset($$type) || !is_readable($$type) ) {
      foreach($_patterns as $_pattern) {
        if ( is_readable("$path$_pattern$ext") ) {
          $$type = "$path$_pattern$ext";
          break;
        }
      }
      
      if ( is_null($$type) )
        $msg .= "Notice: Unable to find $type font.<br>";
    }
  }

  $fonts = compact("normal", "bold", "italic", "bold_italic");
  $entry = array();

  // Copy the files to the font directory.
  foreach ($fonts as $var => $src) {
    if ( is_null($src) ) {
      $entry[$var] = DOMPDF_FONT_DIR . mb_substr(basename($normal), 0, -4);
      continue;
    }
	
	

    // Verify that the fonts exist and are readable
    if ( !is_readable($src) )
      throw new DOMPDF_Exception("Requested font '$src' is not readable");

    $dest = DOMPDF_FONT_DIR . basename($src);

    if ( !is_writeable(dirname($dest)) )
      throw new DOMPDF_Exception("Unable to write to destination '$dest'.");

    $msg .=  "<br>Copying:<br> $src <br><br>to destination:<br> $dest...<br><br>";

    if ( !copy($src, $dest) )
      throw new DOMPDF_Exception("Unable to copy '$src' to '$dest'");
    
    $entry_name = mb_substr($dest, 0, -4);
    
    $msg .=  "Generating Adobe Font Metrics for $entry_name ...<br>";
	

    $font_obj = Font::load($dest);
    $font_obj->saveAdobeFontMetrics("$entry_name.ufm");

    $entry[$var] = $entry_name;
	
  }

  	
  
	
	
  // Store the fonts in the lookup table
  Font_Metrics::set_font_family($fontname, $entry);

  // Save the changes
  Font_Metrics::save_font_families();
  
  $msg .= "<br>Done! :)<br>";
  
  if($cleanup==1) { 
	  clear_upload_dir(); 
  }
  
  form();
  echo '<p class="msg">'.$msg.'</p>';
  
  
}

/*
* clean up temporary files in DOMPDF_DIR/fonttemp/ after generating fonts
*************************************************************************/
function clear_upload_dir(){

	$temp_files = scandir( DOMPDF_DIR.'/fonttemp/' );
	for( $i = 0; $i < count($temp_files); $i++ ){
		if($temp_files[$i]!='.DS_Store' && $temp_files[$i]!='.' && $temp_files[$i]!='..' && $temp_files[$i]!='log.htm'){
			// clean up temp files
			unlink(DOMPDF_DIR.'/fonttemp/'.$temp_files[$i]);
		}
	}
	
}