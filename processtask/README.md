# Module ProcessTask

## Version
1.0.5

## Requirements
* Silverstripe 3.1
* [PHP Cron Expression Parser](https://github.com/mtdowling/cron-expression) 

## Overview

Process Task module allow to create and manage tasks in Silverstripe via the CMS.
It contains wireframe classes that can be extended in order to add the
needed functionality.

The ProcessTask will call multiple scheduled tasks whose settings 
are stored in a related ProcessTaskObject.
Each single task will only be run in the scheduled intervall.
This way we can manage multiple tasks which have similar functionality 
from within the CMS (e.g. importing or exporting data at a certain intervall).

A ProcessTask is best called with a cronjob, 
using the silverstripe [CLI or sake interface] (https://docs.silverstripe.org/en/3.1/developer_guides/cli)
(which will run the process as a background task with all
silverstripe functions and methaods available, 
but without blocking the frontend of the CMS)
A ProcessTask can also be called via URL over the web frontend.

### Permissions
If the task is called via CLI or sake script it always has the permissions
to run the task.

If we call the task via URL over the web frontend, we provide permissions 
to execute the task via the `can_run_task()` function.
This function can be subclassed and implement any custom permission checks.

## Installation

* Add module to your project directory (name the folder `processtask`) 
* Add `ProcessTaskAdmin` to `mysite/code` (see example file under `code/admin/ProcessTaskAdmin.php`)
* add `/_ss_environment.php` file to website root directory to match the absolute path of the project folder to the URL that the site is assigned to (see example)
* Define custom `<Taskname>Task` and `<Taskname>Config` extending ProcessTask and ProcessTaskObject
* Add `routes.yml` to `mysite/_config/` directory to route a URL to `<Taskname>Task` (see example)

### Setting up `_ss_environment.php` file

When running the tasks with [CLI or sake interface] (https://docs.silverstripe.org/en/3.1/developer_guides/cli)
the server needs to know which URL to use for the environment.
We need to assign the current folder on the server (absolute path) with the
URL that is assigned to our project, by creating a `_ss_environment.php` file 
and placing it in the project root:

```php
<?php

global $_FILE_TO_URL_MAPPING;
// map folder to URL
$_FILE_TO_URL_MAPPING['/var/www/vhosts/MYVHOST/httpdocs/myprojectfolder'] = 'http://mydomain.com';
```

### Setting up URL routes via `routes.yml`

Each ProcessTask needs to set a unique URL route via `_config/routes.yml` file
to map a URL (that is used to execute that task) to the ProcessTask class.
If we want to access the task via `mydomain.com/tasks/mytask` we need to point 
the URL `tasks/mytask` to our custom `MyTaskController`

#### Example `routes.yml`

```yml
---
Name: processtaskroutes
After: framework/routes#coreroutes
---
Director:
  rules:
    'tasks/mytask': 'MyTaskController'
```

## Changelog

### 1.0.5
* Added german translation and proper formatting for Schedule description
  (Cron Expression examples)

### 1.0.4
* [FIX]: Removed empty `_config/routes.yml`, because this empty file was causing
  errors when running `dev/build`. 
  Added the example code from this file to documentation instead.

### 1.0.3
* Added documentation on creating a `_ss_environment.php` file that is required
  for running the tasks via CLI or sake script  

### 1.0.2
* Added return value to ProcessTask::execute_task() function
* Set ProcessTaskObject->Published to true by default
* Format 'LastStarted' and 'LastRun' date based on user settings by default
