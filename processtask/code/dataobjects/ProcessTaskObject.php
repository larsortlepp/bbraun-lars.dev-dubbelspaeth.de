<?php

/**
 * Object that stores settings of a single task
 * The Schedule (intervall) in that the task should be run can be set
 * via `Schedule` variable using [cron expression notation] (https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)
 * The datetime the task was startet (`LastStarted`), 
 * the current status (`Status`) and the datetime the task was 
 * successfully finished (`LastRun`) is stored in the task object.
 */
class ProcessTaskObject extends DataObject  {

//  static $singular_name = "ProcessTaskObject";

//	static $plural_name = "ProcessTaskObject";
	
	private static $db = array (
        'Title' => 'Varchar(255)',
		"Status" => "Enum('Running,Error,Finished','Finished')",
		'LastStarted' => 'SS_Datetime',
		'LastRun' => 'SS_Datetime',
		'Timeout' => 'Int', // timeout in minutes (task will be started again after this timeout)
		'Schedule' => 'Varchar(255)',
		'Published' => 'Boolean'
    );
	
	private static $defaults = array(
		'Published' => 1
	);
	
	/**
	 * Redefine $field_labels, $summary_fields and $searchable_fields 
	 * on all subclasses
	 */
	private static $field_labels = array(
		'Title' => 'Titel',
		'Status' => 'Status',
		'LastStarted' => 'Zuletzt gestartet',
		'LastStarted.FormatFromSettings' => 'Zuletzt gestartet',
		'LastRun' => 'Zuletzt ausgeführt',
		'LastRun.FormatFromSettings' => 'Zuletzt ausgeführt',
		'Timeout' => 'Timeout',
		'Schedule' => 'Zeitplan',
		'Subsite.Title' => 'Mandant',
		'Published.Nice' => 'aktiv'
	);
	
	private static $summary_fields = array(
		'Subsite.Title',
		'Title',
		'Schedule',
		'Status',
		'LastStarted.FormatFromSettings',
		'LastRun.FormatFromSettings',
		'Timeout',
		'Published.Nice'
	);
	
	private static $searchable_fields = array(
		'Status',
		'Published',
		'Subsite.Title',
		'Title',
		'LastStarted',
		'LastRun',
		'Timeout',
		'Schedule'
	);
	
	public function isTaskDue($datetime) {
		$cron = Cron\CronExpression::factory($this->Schedule);
		
		// run if it is due at $datetime 
		// or if it was due in the past - based on LastRun
		Debug::message("Status: $this->Status, LastRun: $this->LastRun - Current DateTime: $datetime - Previous Run Date ".$cron->getPreviousRunDate()->format('Y-m-d H:i:s'));
		if ($cron->isDue($datetime) || ($cron->getPreviousRunDate()->format('Y-m-d H:i:s') > $this->LastRun )) {
			return true;
		} 
		else {
			Debug::message("Process ".$this->Title." will run at ".$cron->getNextRunDate()->format('Y-m-d H:i:s'));
			return false;
		}
	}

    public function getCMSFields() {
		
		// create basic tab structure
		$f = new FieldList(
			new TabSet('Root',
				Tab::create('Main', 'Hauptteil')
			)
		);
		
		$f->addFieldsToTab('Root.Main',
			array(
				FieldGroup::create(CheckboxField::create('Published', 'ja'))->setTitle('Aktiv')->setName('GroupField'),
				FieldGroup::create(
					'Mandant',
					LiteralField::create('SubsiteTitle', $this->Subsite()->Title)
				),
				TextField::create('Title', 'Titel'),
				TextField::create('Schedule', 'Zeitplan')->setDescription('
Parameter:<br />
@daily: täglich<br />
@monthly: monatlich<br />
<br />
oder Cron Expression:<br />
<pre><code>
*    *    *    *    *    *
-    -    -    -    -    -
|    |    |    |    |    |
|    |    |    |    |    + Jahr [optional]
|    |    |    |    +----- Wochentag (0 - 7) (Sonntag = 0 oder 7)
|    |    |    +---------- Monat (1 - 12)
|    |    +--------------- Tag (1 - 31)
|    +-------------------- Stunde (0 - 23)
+------------------------- Minute (0 - 59)
</code></pre>
<br />
Beispiele:
<br />
<pre style="display: inline;"><code>30	13	*	*	*	</code></pre>Wird täglich um 13:30 Uhr ausgeführt<br />
<pre style="display: inline;"><code>30	13	*	*	1-5	</code></pre>Wird an allen Wochentagen (Montag - Freitag) um 13:30 Uhr ausgeführt<br />
<pre style="display: inline;"><code>*/5	*	*	*	*	</code></pre>Wird alle 5 Minuten ausgeführt
				'),
				NumericField::create('Timeout', 'Timeout')->setDescription('in Minuten'),
				ReadonlyField::create('LastStarted', 'Zuletzt gestartet'),
				ReadonlyField::create('Status', 'Status'),
				ReadonlyField::create('LastRun', 'Zuletzt ausgeführt')
			)
		);
		
		return $f;
    }
	
	/**
	 * Define permission per DataObject in subclasses
	 * Subclasses must implement PermissionProvider
	 * 
	public function canCreate($member = null) {
        return Permission::check('CREATE_PROCESSTASKOBJECT', 'any', $member);
    }
	
	public function canView($member = null) {
        return Permission::check('VIEW_PROCESSTASKOBJECT', 'any', $member);
    }
	
	public function canEdit($member = null) {
        return Permission::check('EDIT_PROCESSTASKOBJECT', 'any', $member); // call this without a function and have providePeröissions funtion
    }
	
	public function canDelete($member = null) {
        return Permission::check('DELETE_PROCESSTASKOBJECT', 'any', $member);
    }
	
	public function providePermissions() {
		// Doc: http://www.balbuss.com/adding-new-group-permissions/
		return array(
			"CREATE_PROCESSTASKOBJECT" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" erstellen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 10
			),
			"VIEW_PROCESSTASKOBJECT" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" betrachten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 20
			),
			"EDIT_PROCESSTASKOBJECT" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" bearbeiten',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 30
			),
			"DELETE_PROCESSTASKOBJECT" => array(
				'name' => 'Kann Aufgabe "'.$this::$singular_name.'" löschen',
				'category' => 'Aufgabenplanung',
				'help' => '',
				'sort' => 40
			)
		);
	}
	*/
}

