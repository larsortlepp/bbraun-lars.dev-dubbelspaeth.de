<?php

/**
 * The ProcessTask will call multiple scheduled tasks whose settings 
 * are stored in a related ProcessTaskObject.
 * Each single task will only be run in the scheduled intervall.
 * This way we can manage multiple tasks which have similar functionality 
 * from within the CMS (e.g. importing or exporting data at a certain intervall).
 * 
 * Each ProcessTask needs to set a unique URL route via _config/routes.yml file
 * to map a URL (that is used to execute that task) to the ProcessTask class.
 * For example code see `processtask/_config/routes.yml
 * 
 * A ProcessTask is best called with a cronjob, 
 * using the silverstripe [CLI or sake interface] (https://docs.silverstripe.org/en/3.1/developer_guides/cli)
 * (which will run the process as a background task with all
 * silverstripe functions and methaods available, 
 * but without blocking the frontend of the CMS)
 * A ProcessTask can also be called via URL over the web frontend.
 * 
 * **Permissions**
 * If the task is called via CLI or sake script it always has the permissions
 * to run the task.
 * If we call the task via URL over the web frontend, we provide permissions 
 * to execute the task via the `can_run_task()` function.
 * This function can be subclassed and implement any custom permission checks.
 * 
 * @requires [PHP Cron Expression Parser](https://github.com/mtdowling/cron-expression)
 */
class ProcessTask extends Controller {
	
	public  $taskstartdatetime; // date time when the task was started (used to check the schedue of taskobjects)
	/**
	 * ClassName of the DataObject that defines the configuration for the task
	 * Modifiy this name in subclasses
	 * @var string
	 */
	public static $task_object_classname = 'ProcessTaskObject';

	private static $allowed_actions = array(
		'index'
	);
	
	/**
	 * Provide permissions for current member to run the task
	 * This function can be subclassed an implement class-specific permissions
	 *  
	 * @return Boolean
	 */
	public function can_run_task() {
		return Permission::check("ADMIN");
	}
	
	/**
	 * Calls all DataObjects with $task_object_classname
	 * Checks if isTaskDue() (if the Schedule of the DataObject is set to be run)
	 * Calls the $this->process() to perform the task with the configuration
	 * of the DataObject
	 * 
	 * @param SS_HTTPRequest $request
	 */
	public function index($request) {
		// Check access permission
		// Allow access from command line and from can_run_task() function
		if(!Director::is_cli() && !$this->can_run_task()) {
			return Security::permissionFailure();
		}
		
		$this->taskstartdatetime = date('Y-m-d H:i:s');
		$taskObjectClassName = $this->getTaskObjectClassName();
		Subsite::disable_subsite_filter($disabled = true);
		foreach($taskObjectClassName::get()->filter('Published', 1) as $taskconfig){
			
			// is the task scheduled to run now?
			if(
				$taskconfig->isTaskDue($this->taskstartdatetime) && 
				date('Y-m-d H:i', strtotime($this->taskstartdatetime)) > date('Y-m-d H:i', strtotime($taskconfig->LastRun))
			){
				$this->execute_task($taskconfig, $request);
			}
			else {
				Debug::message("Task ".$taskconfig->Title." was just run in this minute: $taskconfig->LastRun");

			}
		}
		Subsite::disable_subsite_filter($disabled = false);
	}
	
	/**
	 * Execute task by running process and update Status, LastStarted and LastRun
	 * 
	 * @param ProcessTaskObject $taskconfig
	 * @return Boolean $success true if task execution was successfull, false if error occured
	 */
	public function execute_task($taskconfig, $request){
		// start processing if task is finished OR task is running (or has an error) for more than the set Timeout
		// hint: does not stop the task if it would really be running. We assume that there was an error if the Timeout is reached.
		$minutesSinceLastStarted = round((time() - strtotime($taskconfig->LastStarted)) / 60);
		$success = false;
		if (
				(
				$taskconfig->Status == 'Finished' ||
				$taskconfig->Status == 'Error'
				) ||
				(
				$taskconfig->Status == 'Running' && isset($taskconfig->Timeout) && $minutesSinceLastStarted > $taskconfig->Timeout
				)
		) {

			$taskconfig->LastStarted = date('Y-m-d H:i:s');
			if ($taskconfig->Status == 'Error')
				Debug::message("There was an error last runtime");
			$taskconfig->Status = 'Running';
			$taskconfig->write();
			$success = $this->process($taskconfig, $request);

			// store current date time as LastRun value if processing was successfull
			if ($success) {
				$taskconfig->LastRun = date('Y-m-d H:i:s');
				$taskconfig->Status = 'Finished';
			} else {
				$taskconfig->Status = 'Error';
			}
			$taskconfig->write();
			Debug::message("Ending Process with Status: $taskconfig->Status");
		} else {
			Debug::message("Process $taskconfig->Title has Status $taskconfig->Status");
		}
		
		return $success;
	}
	
	/**
	 * Defines the actual processing of the task
	 * Define the functionality in subclasses
	 * 
	 * @param DataObject $taskconfig
	 * @param SS_HTTPRequest $request
	 * @return boolean true if successfull, false if error occured
	 */
	public function process($taskconfig,$request) {
	}

	public function getTaskObjectClassName() {
		return $this::$task_object_classname;
	}
	
}
