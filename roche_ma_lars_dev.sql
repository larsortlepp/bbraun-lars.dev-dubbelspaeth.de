-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 28. Feb 2017 um 10:05
-- Server-Version: 5.5.54-0ubuntu0.14.04.1
-- PHP-Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `roche_ma_lars_dev`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AdditivesBlacklistConfig`
--

CREATE TABLE `AdditivesBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AdditivesBlacklistConfig') DEFAULT 'AdditivesBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AdditivesBlacklistObject`
--

CREATE TABLE `AdditivesBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AdditivesBlacklistObject') DEFAULT 'AdditivesBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `AdditivesBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AllergensBlacklistConfig`
--

CREATE TABLE `AllergensBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AllergensBlacklistConfig') DEFAULT 'AllergensBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `AllergensBlacklistObject`
--

CREATE TABLE `AllergensBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('AllergensBlacklistObject') DEFAULT 'AllergensBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `AllergensBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage`
--

CREATE TABLE `DailyMenuPage` (
  `ID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Message` mediumtext,
  `Message_en_US` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage`
--

INSERT INTO `DailyMenuPage` (`ID`, `Date`, `Message`, `Message_en_US`) VALUES
(12, '2016-01-04', NULL, NULL),
(41, '2016-01-04', NULL, NULL),
(66, '2016-01-05', NULL, NULL),
(67, '2016-01-06', NULL, NULL),
(68, '2016-01-07', NULL, NULL),
(69, '2016-01-08', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage_Dishes`
--

CREATE TABLE `DailyMenuPage_Dishes` (
  `ID` int(11) NOT NULL,
  `DailyMenuPageID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage_Dishes`
--

INSERT INTO `DailyMenuPage_Dishes` (`ID`, `DailyMenuPageID`, `DishID`, `SortOrder`) VALUES
(1, 12, 1, 1),
(2, 12, 2, 2),
(3, 12, 3, 4),
(4, 12, 4, 5),
(5, 12, 6, 3),
(6, 12, 5, 6),
(7, 12, 7, 8),
(8, 12, 8, 9),
(9, 12, 9, 7),
(10, 12, 10, 0),
(11, 41, 1, 0),
(12, 41, 2, 0),
(13, 41, 3, 0),
(14, 41, 4, 0),
(15, 41, 5, 0),
(16, 41, 6, 0),
(17, 41, 7, 0),
(18, 41, 8, 0),
(19, 41, 9, 0),
(20, 41, 10, 0),
(21, 12, 11, 0),
(22, 66, 6, 0),
(23, 66, 2, 0),
(24, 66, 7, 0),
(25, 66, 4, 0),
(26, 66, 3, 0),
(27, 66, 10, 0),
(28, 67, 5, 0),
(29, 67, 3, 0),
(30, 67, 1, 0),
(31, 67, 4, 0),
(32, 67, 9, 0),
(33, 67, 2, 0),
(34, 68, 3, 0),
(35, 68, 4, 0),
(36, 68, 2, 0),
(37, 68, 5, 0),
(38, 68, 6, 0),
(39, 68, 1, 0),
(40, 69, 4, 0),
(41, 69, 3, 0),
(42, 69, 6, 0),
(43, 69, 2, 0),
(44, 69, 5, 0),
(45, 69, 7, 0),
(46, 69, 9, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage_Live`
--

CREATE TABLE `DailyMenuPage_Live` (
  `ID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Message` mediumtext,
  `Message_en_US` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage_Live`
--

INSERT INTO `DailyMenuPage_Live` (`ID`, `Date`, `Message`, `Message_en_US`) VALUES
(12, '2016-01-04', NULL, NULL),
(41, '2016-01-04', NULL, NULL),
(66, '2016-01-05', NULL, NULL),
(67, '2016-01-06', NULL, NULL),
(68, '2016-01-07', NULL, NULL),
(69, '2016-01-08', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DailyMenuPage_versions`
--

CREATE TABLE `DailyMenuPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` date DEFAULT NULL,
  `Message` mediumtext,
  `Message_en_US` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DailyMenuPage_versions`
--

INSERT INTO `DailyMenuPage_versions` (`ID`, `RecordID`, `Version`, `Date`, `Message`, `Message_en_US`) VALUES
(1, 12, 1, NULL, NULL, NULL),
(2, 12, 2, NULL, NULL, NULL),
(3, 12, 3, '2016-01-01', NULL, NULL),
(4, 12, 4, '2016-01-04', NULL, NULL),
(5, 41, 1, NULL, NULL, NULL),
(6, 41, 2, '2016-01-04', NULL, NULL),
(7, 66, 1, NULL, NULL, NULL),
(8, 66, 2, NULL, NULL, NULL),
(9, 66, 3, '2016-01-05', NULL, NULL),
(10, 67, 1, NULL, NULL, NULL),
(11, 67, 2, NULL, NULL, NULL),
(12, 67, 3, '2016-01-06', NULL, NULL),
(13, 68, 1, NULL, NULL, NULL),
(14, 68, 2, NULL, NULL, NULL),
(15, 68, 3, '2016-01-07', NULL, NULL),
(16, 69, 1, NULL, NULL, NULL),
(17, 69, 2, NULL, NULL, NULL),
(18, 69, 3, '2016-01-08', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish`
--

CREATE TABLE `Dish` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Dish') DEFAULT 'Dish',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Description` mediumtext,
  `Description_en_US` mediumtext,
  `Infotext` mediumtext,
  `Infotext_en_US` mediumtext,
  `Calories` float NOT NULL DEFAULT '0',
  `Kilojoule` float NOT NULL DEFAULT '0',
  `Fat` float NOT NULL DEFAULT '0',
  `FattyAcids` float NOT NULL DEFAULT '0',
  `Carbohydrates` float NOT NULL DEFAULT '0',
  `Sugar` float NOT NULL DEFAULT '0',
  `Fibres` float NOT NULL DEFAULT '0',
  `Protein` float NOT NULL DEFAULT '0',
  `Salt` float NOT NULL DEFAULT '0',
  `ImportID` varchar(255) DEFAULT NULL,
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `OrigDishID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish`
--

INSERT INTO `Dish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Description`, `Description_en_US`, `Infotext`, `Infotext_en_US`, `Calories`, `Kilojoule`, `Fat`, `FattyAcids`, `Carbohydrates`, `Sugar`, `Fibres`, `Protein`, `Salt`, `ImportID`, `SubsiteID`, `ImageID`, `RestaurantPageID`, `OrigDishID`) VALUES
(1, 'Dish', '2016-10-27 11:43:03', '2017-02-07 11:27:37', 'Hühnerbrühe mit Gemüse', 'English Description', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0),
(2, 'Dish', '2016-10-27 11:43:29', '2017-02-27 15:03:18', 'Camembert mit Preiselbeeren und Baugette', 'Descr EN', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0),
(3, 'Dish', '2016-10-27 11:44:43', '2017-02-07 17:30:26', 'Gebratenes Schweinefilet auf Pimentoschaum mit Penne Rigate und Brokkoli', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 30, 0, 0),
(4, 'Dish', '2016-10-27 11:46:34', '2017-02-27 16:44:05', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'English Description', NULL, NULL, 4, 5, 6, 7, 8, 9, 7, 5, 4, NULL, 1, 18, 0, 0),
(5, 'Dish', '2016-10-27 11:49:05', '2017-02-07 11:50:27', 'Gebackenes Seelachsfilet mit Remouladensoße und Kartoffelsalat', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 32, 0, 0),
(6, 'Dish', '2016-10-27 11:49:53', '2017-02-01 15:52:14', 'Geflügelstreifen in pikanter asiatischer Soße mit Reis', NULL, 'Lorem Ipsum', NULL, 1, 2, 3, 4, 5, 6, 7, 8, 9, NULL, 1, 42, 0, 0),
(7, 'Dish', '2016-10-27 11:50:37', '2017-02-27 12:25:15', '1/2 Flammkuchen Elsässer Art', '1/2 tarte flambée Alsatian style', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0),
(8, 'Dish', '2016-10-27 11:51:04', '2016-10-27 11:51:04', '1/2 Flammkuchen Florentiner Art', '1/2 Tarte flambée Florentine style', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0),
(9, 'Dish', '2016-10-27 17:31:25', '2017-02-07 11:30:43', 'Kartoffepüree', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0),
(10, 'Dish', '2016-10-27 17:32:11', '2016-10-27 17:32:11', 'Waldfruchtjoghurt', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0),
(11, 'Dish', '2017-02-07 11:29:51', '2017-02-07 17:19:49', 'Maiscremesuppe', 'Corn cream soup', NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishesPerRestaurantDishExtensionPermissions`
--

CREATE TABLE `DishesPerRestaurantDishExtensionPermissions` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishesPerRestaurantDishExtensionPermissions') DEFAULT 'DishesPerRestaurantDishExtensionPermissions',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishLabelsBlacklistConfig`
--

CREATE TABLE `DishLabelsBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishLabelsBlacklistConfig') DEFAULT 'DishLabelsBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishLabelsBlacklistObject`
--

CREATE TABLE `DishLabelsBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishLabelsBlacklistObject') DEFAULT 'DishLabelsBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `DishLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishRating`
--

CREATE TABLE `DishRating` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishRating') DEFAULT 'DishRating',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `DishDescription` mediumtext,
  `FoodCategoryTitle` varchar(255) DEFAULT NULL,
  `Taste` int(11) NOT NULL DEFAULT '0',
  `Value` int(11) NOT NULL DEFAULT '0',
  `Service` int(11) NOT NULL DEFAULT '0',
  `Comment` mediumtext,
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishRating`
--

INSERT INTO `DishRating` (`ID`, `ClassName`, `Created`, `LastEdited`, `Date`, `DishDescription`, `FoodCategoryTitle`, `Taste`, `Value`, `Service`, `Comment`, `RestaurantPageID`, `DishID`, `MenuContainerID`) VALUES
(1, 'DishRating', '2017-02-06 15:33:22', '2017-02-06 15:33:22', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 4, 3, 0, 'Hello World!', 7, 4, 11),
(2, 'DishRating', '2017-02-06 15:48:40', '2017-02-06 15:48:40', '2016-01-04', 'Gebackenes Seelachsfilet mit Remouladensoße und Kartoffelsalat', 'Weltweit', 4, 3, 4, 'Hallo Welt!', 7, 5, 11),
(3, 'DishRating', '2017-02-07 14:15:59', '2017-02-07 14:15:59', '2016-01-04', 'Maiscremesuppe', 'Suppe', 3, 4, 3, 'Hello World!', 7, 11, 11),
(4, 'DishRating', '2017-02-07 14:36:44', '2017-02-07 14:36:44', '2016-01-04', 'Hühnerbrühe mit Gemüse', 'Suppe', 3, 4, 3, 'Hallo Welt!', 7, 1, 11),
(5, 'DishRating', '2017-02-07 16:00:42', '2017-02-07 16:00:42', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 4, 3, 5, 'Lorem Ipsum', 7, 4, 11),
(6, 'DishRating', '2017-02-07 16:01:31', '2017-02-07 16:01:31', '2016-01-04', 'Gebackenes Seelachsfilet mit Remouladensoße und Kartoffelsalat', 'Weltweit', 4, 3, 3, 'Lorem Ipsum', 7, 5, 11),
(7, 'DishRating', '2017-02-07 16:03:49', '2017-02-07 16:03:49', '2016-01-04', 'Gebratenes Schweinefilet auf Pimentoschaum mit Penne Rigate und Brokkoli', 'Vital', 3, 3, 4, 'Test 123', 7, 3, 11),
(8, 'DishRating', '2017-02-07 16:04:48', '2017-02-07 16:04:48', '2016-01-04', 'Waldfruchtjoghurt', 'Dessert', 3, 3, 0, 'Lorem Ipsum', 7, 10, 11),
(9, 'DishRating', '2017-02-07 16:06:15', '2017-02-07 16:06:15', '2016-01-04', 'Waldfruchtjoghurt', 'Dessert', 3, 3, 0, 'Lorem Ipsum', 7, 10, 11),
(10, 'DishRating', '2017-02-07 16:06:35', '2017-02-07 16:06:35', '2016-01-04', '1/2 Flammkuchen Elsässer Art', 'Beilagen', 4, 3, 5, NULL, 7, 7, 11),
(11, 'DishRating', '2017-02-07 16:12:46', '2017-02-07 16:12:46', '2016-01-04', 'Kartoffepüree', 'Beilagen', 4, 3, 3, NULL, 7, 9, 11),
(12, 'DishRating', '2017-02-07 16:15:17', '2017-02-07 16:15:17', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 3, 4, 3, NULL, 7, 4, 11),
(13, 'DishRating', '2017-02-07 16:17:45', '2017-02-07 16:17:45', '2016-01-04', 'Camembert mit Preiselbeeren und Baugette', 'Vorspeise', 3, 4, 2, NULL, 7, 2, 11),
(14, 'DishRating', '2017-02-07 16:18:21', '2017-02-07 16:18:21', '2016-01-04', 'Camembert mit Preiselbeeren und Baugette', 'Vorspeise', 3, 4, 2, NULL, 7, 2, 11),
(15, 'DishRating', '2017-02-07 16:19:14', '2017-02-07 16:19:14', '2016-01-04', 'Geflügelstreifen in pikanter asiatischer Soße mit Reis', 'Aktion', 3, 3, 5, NULL, 7, 6, 11),
(16, 'DishRating', '2017-02-07 16:20:34', '2017-02-07 16:20:34', '2016-01-04', '1/2 Flammkuchen Florentiner Art', 'Beilagen', 3, 4, 2, NULL, 7, 8, 11),
(17, 'DishRating', '2017-02-07 16:21:11', '2017-02-07 16:21:11', '2016-01-04', '1/2 Flammkuchen Elsässer Art', 'Beilagen', 3, 4, 3, NULL, 7, 7, 11),
(18, 'DishRating', '2017-02-07 16:29:59', '2017-02-07 16:29:59', '2016-01-04', 'Maiscremesuppe', 'Suppe', 3, 4, 3, NULL, 7, 11, 11),
(19, 'DishRating', '2017-02-07 16:30:38', '2017-02-07 16:30:38', '2016-01-04', 'Gebackenes Seelachsfilet mit Remouladensoße und Kartoffelsalat', 'Weltweit', 4, 5, 4, NULL, 7, 5, 11),
(20, 'DishRating', '2017-02-07 16:35:25', '2017-02-07 16:35:25', '2016-01-04', 'Gebackenes Seelachsfilet mit Remouladensoße und Kartoffelsalat', 'Weltweit', 3, 4, 2, NULL, 7, 5, 11),
(21, 'DishRating', '2017-02-07 17:28:20', '2017-02-07 17:28:20', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 3, 4, 5, 'Lorem Ipsum 123', 7, 4, 11),
(22, 'DishRating', '2017-02-07 17:30:46', '2017-02-07 17:30:46', '2016-01-04', 'Gebratenes Schweinefilet auf Pimentoschaum mit Penne Rigate und Brokkoli', 'Vital', 4, 3, 5, 'asdf', 7, 3, 11),
(23, 'DishRating', '2017-02-08 10:08:41', '2017-02-08 10:08:41', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 4, 3, 5, NULL, 7, 4, 11),
(24, 'DishRating', '2017-02-08 10:11:19', '2017-02-08 10:11:19', '2016-01-04', 'Waldfruchtjoghurt', 'Dessert', 4, 3, 5, 'asdf', 7, 10, 11),
(25, 'DishRating', '2017-02-08 10:21:28', '2017-02-08 10:21:28', '2016-01-04', 'Waldfruchtjoghurt', 'Dessert', 5, 4, 5, NULL, 7, 10, 11),
(26, 'DishRating', '2017-02-08 10:22:06', '2017-02-08 10:22:06', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 5, 5, 5, NULL, 7, 4, 11),
(27, 'DishRating', '2017-02-08 10:23:48', '2017-02-08 10:23:48', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 5, 5, 5, NULL, 7, 4, 11),
(28, 'DishRating', '2017-02-08 10:29:44', '2017-02-08 10:29:44', '2016-01-04', 'Maiscremesuppe', 'Suppe', 4, 5, 4, 'asdf', 7, 11, 11),
(29, 'DishRating', '2017-02-08 10:37:13', '2017-02-08 10:37:13', '2016-01-04', 'Gebratene Frikadelle vom Rind und Schwein mit Pilzsoße und Kartoffelpüree', 'Heimat', 5, 0, 5, 'sdf', 7, 4, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingItem`
--

CREATE TABLE `DishVotingItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DishVotingItem') DEFAULT 'DishVotingItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `DishDescription` mediumtext,
  `DishDescription_en_US` mediumtext,
  `Votes` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `DishVotingPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingItem`
--

INSERT INTO `DishVotingItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `DishDescription`, `DishDescription_en_US`, `Votes`, `SortOrder`, `DishVotingPageID`) VALUES
(1, 'DishVotingItem', '2016-10-27 12:29:31', '2016-10-27 12:29:31', 'Glasnudeln mit Rindfleisch und knackigem Gemüse', NULL, 0, 0, 30),
(2, 'DishVotingItem', '2016-10-27 12:29:45', '2016-10-27 12:29:45', 'Bratwurst mit Sauerkraut und Püree', NULL, 0, 0, 30);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingPage`
--

CREATE TABLE `DishVotingPage` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingPage`
--

INSERT INTO `DishVotingPage` (`ID`, `VotingDate`, `Publish`) VALUES
(30, '2016-10-28', 1),
(57, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingPage_Live`
--

CREATE TABLE `DishVotingPage_Live` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingPage_Live`
--

INSERT INTO `DishVotingPage_Live` (`ID`, `VotingDate`, `Publish`) VALUES
(30, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DishVotingPage_versions`
--

CREATE TABLE `DishVotingPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DishVotingPage_versions`
--

INSERT INTO `DishVotingPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES
(1, 30, 1, NULL, 0),
(2, 30, 2, NULL, 0),
(3, 30, 3, '2016-10-27', 0),
(4, 30, 4, '2016-10-28', 0),
(5, 30, 5, '2016-10-28', 1),
(6, 57, 1, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalAdditives`
--

CREATE TABLE `Dish_GlobalAdditives` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalAdditiveID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_GlobalAdditives`
--

INSERT INTO `Dish_GlobalAdditives` (`ID`, `DishID`, `GlobalAdditiveID`) VALUES
(1, 6, 6),
(2, 6, 8),
(3, 6, 11),
(7, 3, 3),
(8, 3, 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalAllergens`
--

CREATE TABLE `Dish_GlobalAllergens` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalAllergenID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_GlobalAllergens`
--

INSERT INTO `Dish_GlobalAllergens` (`ID`, `DishID`, `GlobalAllergenID`) VALUES
(1, 6, 9),
(2, 6, 12),
(3, 6, 13),
(4, 9, 8),
(5, 9, 11),
(9, 11, 9),
(10, 11, 19);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalDishLabels`
--

CREATE TABLE `Dish_GlobalDishLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalDishLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_GlobalDishLabels`
--

INSERT INTO `Dish_GlobalDishLabels` (`ID`, `DishID`, `GlobalDishLabelID`) VALUES
(1, 6, 6),
(2, 11, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_GlobalSpecialLabels`
--

CREATE TABLE `Dish_GlobalSpecialLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `GlobalSpecialLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_GlobalSpecialLabels`
--

INSERT INTO `Dish_GlobalSpecialLabels` (`ID`, `DishID`, `GlobalSpecialLabelID`) VALUES
(1, 6, 1),
(2, 4, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteAdditives`
--

CREATE TABLE `Dish_SubsiteAdditives` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteAdditiveID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_SubsiteAdditives`
--

INSERT INTO `Dish_SubsiteAdditives` (`ID`, `DishID`, `SubsiteAdditiveID`) VALUES
(1, 9, 2),
(2, 5, 2),
(3, 5, 3),
(4, 5, 4),
(5, 5, 5),
(6, 4, 2),
(7, 4, 6),
(8, 4, 8),
(9, 3, 7),
(10, 3, 8),
(11, 7, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteAllergens`
--

CREATE TABLE `Dish_SubsiteAllergens` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteAllergenID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_SubsiteAllergens`
--

INSERT INTO `Dish_SubsiteAllergens` (`ID`, `DishID`, `SubsiteAllergenID`) VALUES
(1, 5, 2),
(2, 5, 3),
(3, 5, 9),
(4, 5, 10),
(5, 5, 7),
(6, 4, 4),
(7, 4, 6),
(8, 4, 7),
(9, 11, 6),
(10, 11, 7),
(11, 3, 2),
(12, 3, 6),
(13, 3, 8);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteDishLabels`
--

CREATE TABLE `Dish_SubsiteDishLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteDishLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Dish_SubsiteDishLabels`
--

INSERT INTO `Dish_SubsiteDishLabels` (`ID`, `DishID`, `SubsiteDishLabelID`) VALUES
(1, 4, 5),
(2, 4, 4),
(3, 4, 9),
(4, 5, 3),
(5, 3, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Dish_SubsiteSpecialLabels`
--

CREATE TABLE `Dish_SubsiteSpecialLabels` (
  `ID` int(11) NOT NULL,
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SubsiteSpecialLabelID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplateObject`
--

CREATE TABLE `DisplayTemplateObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DisplayTemplateObject') DEFAULT 'DisplayTemplateObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `DisplayMultiTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplateObject_FoodCategories`
--

CREATE TABLE `DisplayTemplateObject_FoodCategories` (
  `ID` int(11) NOT NULL,
  `DisplayTemplateObjectID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplateObject_TimerRecurringDaysOfWeek`
--

CREATE TABLE `DisplayTemplateObject_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `DisplayTemplateObjectID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage`
--

CREATE TABLE `DisplayTemplatePage` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `InfoMessagesOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReloadInterval` int(11) NOT NULL DEFAULT '0',
  `TransitionDuration` int(11) NOT NULL DEFAULT '0',
  `ShowScreenDuration` int(11) NOT NULL DEFAULT '0',
  `TransitionEffect` enum('fade','slide') DEFAULT 'fade',
  `ScreenRotation` enum('keine','90° im Uhrzeigersinn','90° gegen den Uhrzeigersinn') DEFAULT 'keine',
  `DisplayTemplatePage_WeeklyMenuTableTemplate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DisplayTemplatePage`
--

INSERT INTO `DisplayTemplatePage` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES
(22, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(23, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(24, 'Screen_WeeklyMenu_Table_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(25, 'Screen_Counter', 0, 60000, 1000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(50, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(51, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(52, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(53, 'Screen_Counter', 0, 60000, 1000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage_Live`
--

CREATE TABLE `DisplayTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `InfoMessagesOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReloadInterval` int(11) NOT NULL DEFAULT '0',
  `TransitionDuration` int(11) NOT NULL DEFAULT '0',
  `ShowScreenDuration` int(11) NOT NULL DEFAULT '0',
  `TransitionEffect` enum('fade','slide') DEFAULT 'fade',
  `ScreenRotation` enum('keine','90° im Uhrzeigersinn','90° gegen den Uhrzeigersinn') DEFAULT 'keine',
  `DisplayTemplatePage_WeeklyMenuTableTemplate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DisplayTemplatePage_Live`
--

INSERT INTO `DisplayTemplatePage_Live` (`ID`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES
(22, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(23, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(24, 'Screen_WeeklyMenu_Table_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(25, 'Screen_Counter', 0, 60000, 1000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage_TimerRecurringDaysOfWeek`
--

CREATE TABLE `DisplayTemplatePage_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `DisplayTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayTemplatePage_versions`
--

CREATE TABLE `DisplayTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `TemplateName` varchar(255) DEFAULT NULL,
  `InfoMessagesOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReloadInterval` int(11) NOT NULL DEFAULT '0',
  `TransitionDuration` int(11) NOT NULL DEFAULT '0',
  `ShowScreenDuration` int(11) NOT NULL DEFAULT '0',
  `TransitionEffect` enum('fade','slide') DEFAULT 'fade',
  `ScreenRotation` enum('keine','90° im Uhrzeigersinn','90° gegen den Uhrzeigersinn') DEFAULT 'keine',
  `DisplayTemplatePage_WeeklyMenuTableTemplate` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `DisplayTemplatePage_versions`
--

INSERT INTO `DisplayTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `InfoMessagesOnly`, `ReloadInterval`, `TransitionDuration`, `ShowScreenDuration`, `TransitionEffect`, `ScreenRotation`, `DisplayTemplatePage_WeeklyMenuTableTemplate`, `DisplayVisitorCounterData`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `ImportVisitorCounterConfigID`) VALUES
(1, 21, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(2, 21, 2, 'Screen_DailyMenu_Photos', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(3, 21, 3, 'Screen_DailyMenu_Photos', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(4, 22, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(5, 22, 2, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(6, 22, 3, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(7, 23, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(8, 23, 2, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(9, 24, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(10, 24, 2, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(11, 25, 1, NULL, 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(12, 25, 2, 'Screen_Counter_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(13, 23, 3, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(14, 25, 3, 'Screen_Counter', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(15, 21, 4, 'Screen_DailyMenu', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(16, 25, 4, 'Screen_DailyMenu', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(17, 25, 5, 'Screen_Counter', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(18, 25, 6, 'Screen_Counter', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(19, 25, 7, 'Screen_Counter', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(20, 25, 8, 'Screen_Counter', 0, 300000, 2000, 5000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(21, 25, 9, 'Screen_Counter', 0, 60000, 2000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(22, 25, 10, 'Screen_Counter', 0, 60000, 1000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(23, 25, 11, 'Screen_Counter', 0, 60000, 1000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(24, 50, 1, 'Screen_DailyMenu_Photos_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(25, 51, 1, 'Screen_DailyMenu_Photos_Landscape', 1, 300000, 2000, 12000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(26, 52, 1, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(27, 53, 1, 'Screen_Counter', 0, 60000, 1000, 4000, 'fade', 'keine', 0, 0, NULL, NULL, NULL, NULL, 0),
(28, 24, 3, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(29, 24, 4, 'Screen_WeeklyMenu_Table_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(30, 24, 5, 'Screen_WeeklyMenu_Table_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0),
(31, 24, 6, 'Screen_WeeklyMenu_Table_Landscape', 0, 300000, 2000, 12000, 'fade', 'keine', 1, 0, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayVideo`
--

CREATE TABLE `DisplayVideo` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('DisplayVideo') DEFAULT 'DisplayVideo',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `VideoType` enum('Datei','Youtube') DEFAULT 'Datei',
  `YoutubeLink` varchar(255) DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `VideoFileWebMID` int(11) NOT NULL DEFAULT '0',
  `DisplayTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `DisplayVideo_TimerRecurringDaysOfWeek`
--

CREATE TABLE `DisplayVideo_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `DisplayVideoID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ErrorPage`
--

CREATE TABLE `ErrorPage` (
  `ID` int(11) NOT NULL,
  `ErrorCode` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ErrorPage`
--

INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500),
(9, 404),
(65, 500);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ErrorPage_Live`
--

CREATE TABLE `ErrorPage_Live` (
  `ID` int(11) NOT NULL,
  `ErrorCode` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ErrorPage_Live`
--

INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500),
(9, 404),
(65, 500);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ErrorPage_versions`
--

CREATE TABLE `ErrorPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ErrorPage_versions`
--

INSERT INTO `ErrorPage_versions` (`ID`, `RecordID`, `Version`, `ErrorCode`) VALUES
(1, 4, 1, 404),
(2, 5, 1, 500),
(3, 9, 1, 404),
(4, 10, 1, 500),
(5, 9, 2, 404),
(6, 10, 2, 500),
(7, 65, 1, 500);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportConfig`
--

CREATE TABLE `ExportConfig` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Path` varchar(255) DEFAULT NULL,
  `AllowApiAccess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ApiPublicKey` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportDisplayMenuXmlConfig`
--

CREATE TABLE `ExportDisplayMenuXmlConfig` (
  `ID` int(11) NOT NULL,
  `ExportFunction` enum('export_default') DEFAULT 'export_default',
  `DisplayTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportMenuHtmlConfig`
--

CREATE TABLE `ExportMenuHtmlConfig` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportMenuHtmlConfig_FoodCategories`
--

CREATE TABLE `ExportMenuHtmlConfig_FoodCategories` (
  `ID` int(11) NOT NULL,
  `ExportMenuHtmlConfigID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ExportMenuXmlConfig`
--

CREATE TABLE `ExportMenuXmlConfig` (
  `ID` int(11) NOT NULL,
  `ExportFunction` enum('export_default') DEFAULT 'export_default',
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File`
--

CREATE TABLE `File` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('File','Folder','Image','Image_Cached') DEFAULT 'File',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Filename` mediumtext,
  `Content` mediumtext,
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditFolderType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanDeleteType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanUploadType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File`
--

INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `CanViewType`, `CanEditFolderType`, `CanDeleteType`, `CanUploadType`, `ParentID`, `OwnerID`, `SubsiteID`) VALUES
(1, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'Aktionslabels', 'Aktionslabels', 'assets/Aktionslabels/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(2, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'classic-edition.png', 'classic-edition.png', 'assets/Aktionslabels/classic-edition.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(3, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'coa.png', 'coa.png', 'assets/Aktionslabels/coa.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(4, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'curry-edition.png', 'curry-edition.png', 'assets/Aktionslabels/curry-edition.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(5, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'dean-and-david.png', 'dean-and-david.png', 'assets/Aktionslabels/dean-and-david.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(6, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'fitforfun.png', 'fitforfun.png', 'assets/Aktionslabels/fitforfun.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(7, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'fitforfunvitalien.png', 'fitforfunvitalien.png', 'assets/Aktionslabels/fitforfunvitalien.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(8, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'sushi-circle.png', 'sushi-circle.png', 'assets/Aktionslabels/sushi-circle.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(9, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'vebu.png', 'vebu.png', 'assets/Aktionslabels/vebu.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(10, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:01', 'vitalien-2015.png', 'vitalien-2015.png', 'assets/Aktionslabels/vitalien-2015.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 1, 1, 0),
(11, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:03:17', 'Druck', 'Druck', 'assets/Druck/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(12, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Fotos-Gerichte', 'Fotos-Gerichte', 'assets/Fotos-Gerichte/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(13, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', '1.jpg', '1.jpg', 'assets/Fotos-Gerichte/1.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(14, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', '2.jpg', '2.jpg', 'assets/Fotos-Gerichte/2.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(15, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'CanneloniSpinat.jpg', 'CanneloniSpinat.jpg', 'assets/Fotos-Gerichte/CanneloniSpinat.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(16, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Eurestcurrywurst.jpg', 'Eurestcurrywurst.jpg', 'assets/Fotos-Gerichte/Eurestcurrywurst.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(17, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Feuergriller.jpg', 'Feuergriller.jpg', 'assets/Fotos-Gerichte/Feuergriller.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(18, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Frikka.jpg', 'Frikka.jpg', 'assets/Fotos-Gerichte/Frikka.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(19, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Gemsecurry.jpg', 'Gemsecurry.jpg', 'assets/Fotos-Gerichte/Gemsecurry.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(20, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'GeschnetzeltesPolenta.jpg', 'GeschnetzeltesPolenta.jpg', 'assets/Fotos-Gerichte/GeschnetzeltesPolenta.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(21, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Hahnchen-Himmel-und-Erd.jpg', 'Hahnchen-Himmel-und-Erd.jpg', 'assets/Fotos-Gerichte/Hahnchen-Himmel-und-Erd.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(22, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Maispoularde.jpg', 'Maispoularde.jpg', 'assets/Fotos-Gerichte/Maispoularde.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(23, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Nackensteak.jpg', 'Nackensteak.jpg', 'assets/Fotos-Gerichte/Nackensteak.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(24, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Napoli.jpg', 'Napoli.jpg', 'assets/Fotos-Gerichte/Napoli.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(25, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Penne-Mandariene.jpg', 'Penne-Mandariene.jpg', 'assets/Fotos-Gerichte/Penne-Mandariene.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(26, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Pizza-Gemse.jpg', 'Pizza-Gemse.jpg', 'assets/Fotos-Gerichte/Pizza-Gemse.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(27, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Putenbrust-aus-dem-wok.jpg', 'Putenbrust-aus-dem-wok.jpg', 'assets/Fotos-Gerichte/Putenbrust-aus-dem-wok.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(28, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'SaltinBocca.jpg', 'SaltinBocca.jpg', 'assets/Fotos-Gerichte/SaltinBocca.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(29, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'SaltinBocca2.jpg', 'SaltinBocca2.jpg', 'assets/Fotos-Gerichte/SaltinBocca2.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(30, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'SchwSchulter-kartoffelsalat.jpg', 'SchwSchulter-kartoffelsalat.jpg', 'assets/Fotos-Gerichte/SchwSchulter-kartoffelsalat.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(31, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Schweinepfeffer.jpg', 'Schweinepfeffer.jpg', 'assets/Fotos-Gerichte/Schweinepfeffer.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(32, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Seelachs-Senfsauce.jpg', 'Seelachs-Senfsauce.jpg', 'assets/Fotos-Gerichte/Seelachs-Senfsauce.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(33, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Spirelli-Paprika.jpg', 'Spirelli-Paprika.jpg', 'assets/Fotos-Gerichte/Spirelli-Paprika.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(34, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'Tellerrsti.jpg', 'Tellerrsti.jpg', 'assets/Fotos-Gerichte/Tellerrsti.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(35, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'cheeseburger.jpg', 'cheeseburger.jpg', 'assets/Fotos-Gerichte/cheeseburger.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(36, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'fischragout.jpg', 'fischragout.jpg', 'assets/Fotos-Gerichte/fischragout.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(37, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'glasierteHhnchen.jpg', 'glasierteHhnchen.jpg', 'assets/Fotos-Gerichte/glasierteHhnchen.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(38, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'italSchnitzel.jpg', 'italSchnitzel.jpg', 'assets/Fotos-Gerichte/italSchnitzel.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(39, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'karotteerbse.jpg', 'karotteerbse.jpg', 'assets/Fotos-Gerichte/karotteerbse.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(40, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'ofenkartoffel.jpg', 'ofenkartoffel.jpg', 'assets/Fotos-Gerichte/ofenkartoffel.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(41, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'piccata.jpg', 'piccata.jpg', 'assets/Fotos-Gerichte/piccata.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(42, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'salat.jpg', 'salat.jpg', 'assets/Fotos-Gerichte/salat.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(43, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:03:30', 'schweinesteak.jpg', 'schweinesteak.jpg', 'assets/Fotos-Gerichte/schweinesteak.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 12, 1, 0),
(44, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'Kennzeichnungen', 'Kennzeichnungen', 'assets/Kennzeichnungen/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(45, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'chili.png', 'chili.png', 'assets/Kennzeichnungen/chili.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(46, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'fisch.png', 'fisch.png', 'assets/Kennzeichnungen/fisch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(47, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'huhn.png', 'huhn.png', 'assets/Kennzeichnungen/huhn.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(48, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'knoblauch.png', 'knoblauch.png', 'assets/Kennzeichnungen/knoblauch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(49, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'laktosefrei.png', 'laktosefrei.png', 'assets/Kennzeichnungen/laktosefrei.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(50, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'rind.png', 'rind.png', 'assets/Kennzeichnungen/rind.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(51, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'schwein.png', 'schwein.png', 'assets/Kennzeichnungen/schwein.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(52, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'tagesmenue.png', 'tagesmenue.png', 'assets/Kennzeichnungen/tagesmenue.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(53, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'vegan.png', 'vegan.png', 'assets/Kennzeichnungen/vegan.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(54, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'vegetarisch.png', 'vegetarisch.png', 'assets/Kennzeichnungen/vegetarisch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(55, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:05:12', 'zwiebel.png', 'zwiebel.png', 'assets/Kennzeichnungen/zwiebel.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 44, 1, 0),
(56, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:06:18', 'Logos', 'Logos', 'assets/Logos/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(57, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:06:18', 'logo-eurest.gif', 'logo-eurest.gif', 'assets/Logos/logo-eurest.gif', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 56, 1, 0),
(58, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:06:45', 'Microsite', 'Microsite', 'assets/Microsite/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(59, 'Folder', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'News', 'News', 'assets/News/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(60, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', '2014-aktion-dean-david-1920x1080.jpg', '2014-aktion-dean-david-1920x1080.jpg', 'assets/News/2014-aktion-dean-david-1920x1080.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(61, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', '2014-app-market-1980x1080.jpg', '2014-app-market-1980x1080.jpg', 'assets/News/2014-app-market-1980x1080.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(62, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'coa-info.png', 'coa-info.png', 'assets/News/coa-info.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(63, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-snacking-bagel.jpg', 'microsite-snacking-bagel.jpg', 'assets/News/microsite-snacking-bagel.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(64, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-snacking-kaffeemaschine.jpg', 'microsite-snacking-kaffeemaschine.jpg', 'assets/News/microsite-snacking-kaffeemaschine.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(65, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-snacking-kaffeetasse.jpg', 'microsite-snacking-kaffeetasse.jpg', 'assets/News/microsite-snacking-kaffeetasse.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(66, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-speiseplan.jpg', 'microsite-speiseplan.jpg', 'assets/News/microsite-speiseplan.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(67, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-startseite-slider-1.jpg', 'microsite-startseite-slider-1.jpg', 'assets/News/microsite-startseite-slider-1.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(68, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-startseite-slider-2.jpg', 'microsite-startseite-slider-2.jpg', 'assets/News/microsite-startseite-slider-2.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(69, 'Image', '2016-10-27 09:53:20', '2016-10-27 16:07:06', 'microsite-startseite-slider-3.jpg', 'microsite-startseite-slider-3.jpg', 'assets/News/microsite-startseite-slider-3.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 59, 1, 0),
(70, 'File', '2016-10-27 09:53:20', '2016-10-27 09:53:20', 'error-404.html', 'error-404.html', 'assets/error-404.html', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 0, 1, 0),
(71, 'File', '2016-10-27 09:53:20', '2016-10-27 09:53:20', 'error-500.html', 'error-500.html', 'assets/error-500.html', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 0, 1, 0),
(72, 'Folder', '2016-10-27 11:44:09', '2016-10-27 16:50:06', 'vorlage', 'vorlage', 'assets/vorlage/', NULL, 1, 'OnlyTheseUsers', 'Inherit', 'Inherit', 'OnlyTheseUsers', 0, 1, 0),
(73, 'Folder', '2016-10-27 11:44:09', '2016-10-27 16:50:06', 'restaurant-vorlage', 'restaurant-vorlage', 'assets/vorlage/restaurant-vorlage/', NULL, 1, 'Inherit', 'OnlyTheseUsers', 'Inherit', 'OnlyTheseUsers', 72, 1, 0),
(74, 'Folder', '2016-10-27 11:44:09', '2016-10-27 16:50:06', 'Fotos-Gerichte', 'Fotos-Gerichte', 'assets/vorlage/restaurant-vorlage/Fotos-Gerichte/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0),
(75, 'Folder', '2016-10-27 12:18:20', '2016-10-27 16:50:06', 'News', 'News', 'assets/vorlage/restaurant-vorlage/News/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0),
(76, 'Folder', '2016-10-27 14:27:15', '2016-10-27 16:50:06', 'Microsite', 'Microsite', 'assets/vorlage/restaurant-vorlage/Microsite/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0),
(77, 'File', '2016-10-27 14:27:16', '2016-10-27 16:50:06', 'Wochenspeiseplan.pdf', 'Wochenspeiseplan', 'assets/vorlage/restaurant-vorlage/Microsite/Wochenspeiseplan.pdf', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 76, 1, 0),
(78, 'Folder', '2016-10-27 14:57:03', '2016-10-27 16:50:06', 'Aktionslabels', 'Aktionslabels', 'assets/vorlage/Aktionslabels/', NULL, 1, 'Inherit', 'OnlyTheseUsers', 'Inherit', 'Inherit', 72, 1, 0),
(79, 'Folder', '2016-10-27 14:57:49', '2016-10-27 16:50:06', 'Kennzeichnungen', 'Kennzeichnungen', 'assets/vorlage/Kennzeichnungen/', NULL, 1, 'Inherit', 'OnlyTheseUsers', 'OnlyTheseUsers', 'OnlyTheseUsers', 72, 1, 0),
(80, 'Folder', '2016-10-27 15:00:22', '2016-10-27 16:50:06', 'Essenskategorien', 'Essenskategorien', 'assets/vorlage/restaurant-vorlage/Essenskategorien/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0),
(81, 'Folder', '2016-10-27 15:04:32', '2016-10-27 16:50:06', 'Logos', 'Logos', 'assets/vorlage/restaurant-vorlage/Logos/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0),
(82, 'Folder', '2016-10-27 15:17:23', '2016-10-27 16:50:06', 'Druck', 'Druck', 'assets/vorlage/restaurant-vorlage/Druck/', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 73, 1, 0),
(83, 'Image', '2017-02-01 17:03:08', '2017-02-20 15:25:19', 'suppe.png', 'suppe', 'assets/vorlage/restaurant-vorlage/Essenskategorien/suppe.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(84, 'Image', '2017-02-01 17:05:51', '2017-02-20 15:26:59', 'vital.png', 'vital', 'assets/vorlage/restaurant-vorlage/Essenskategorien/vital.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(85, 'Image', '2017-02-01 17:06:22', '2017-02-20 15:27:13', 'kalter-genuss.png', 'kalter genuss', 'assets/vorlage/restaurant-vorlage/Essenskategorien/kalter-genuss.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(86, 'Image', '2017-02-01 17:06:49', '2017-02-20 15:27:28', 'beilagen.png', 'beilagen', 'assets/vorlage/restaurant-vorlage/Essenskategorien/beilagen.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(87, 'Image', '2017-02-01 17:07:08', '2017-02-20 15:28:19', 'salat.png', 'salat', 'assets/vorlage/restaurant-vorlage/Essenskategorien/salat.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(88, 'Image', '2017-02-01 17:07:26', '2017-02-20 15:27:59', 'dessert.png', 'dessert', 'assets/vorlage/restaurant-vorlage/Essenskategorien/dessert.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(90, 'Image', '2017-02-01 17:08:58', '2017-02-20 15:26:44', 'weltweit.png', 'weltweit', 'assets/vorlage/restaurant-vorlage/Essenskategorien/weltweit.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(91, 'Image', '2017-02-01 17:10:20', '2017-02-20 15:26:24', 'szene.png', 'szene', 'assets/vorlage/restaurant-vorlage/Essenskategorien/szene.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0),
(92, 'Image', '2017-02-01 17:25:53', '2017-02-01 17:25:53', 'vegetarisch.png', 'vegetarisch', 'assets/vorlage/Kennzeichnungen/vegetarisch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(93, 'Image', '2017-02-01 17:26:22', '2017-02-01 17:26:22', 'vegan.png', 'vegan', 'assets/vorlage/Kennzeichnungen/vegan.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(94, 'Image', '2017-02-01 17:26:45', '2017-02-01 17:26:45', 'fisch.png', 'fisch', 'assets/vorlage/Kennzeichnungen/fisch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(95, 'Image', '2017-02-01 17:27:12', '2017-02-01 17:27:12', 'schwein.png', 'schwein', 'assets/vorlage/Kennzeichnungen/schwein.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(96, 'Image', '2017-02-01 17:27:32', '2017-02-01 17:27:32', 'rind.png', 'rind', 'assets/vorlage/Kennzeichnungen/rind.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(97, 'Image', '2017-02-01 17:28:12', '2017-02-01 17:28:12', 'chili.png', 'chili', 'assets/vorlage/Kennzeichnungen/chili.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(98, 'Image', '2017-02-01 17:28:33', '2017-02-01 17:28:33', 'zwiebel.png', 'zwiebel', 'assets/vorlage/Kennzeichnungen/zwiebel.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(99, 'Image', '2017-02-01 17:28:56', '2017-02-01 17:28:56', 'knoblauch.png', 'knoblauch', 'assets/vorlage/Kennzeichnungen/knoblauch.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(100, 'Image', '2017-02-01 17:29:12', '2017-02-01 17:29:12', 'bio-siegel.png', 'bio siegel', 'assets/vorlage/Kennzeichnungen/bio-siegel.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(101, 'Image', '2017-02-01 17:29:52', '2017-02-01 17:29:52', 'tagesmenue.png', 'tagesmenue', 'assets/vorlage/Kennzeichnungen/tagesmenue.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(102, 'Image', '2017-02-06 10:22:36', '2017-02-06 10:22:36', 'microsite-startseite-slider-1.jpg', 'microsite startseite slider 1', 'assets/vorlage/restaurant-vorlage/News/microsite-startseite-slider-1.jpg', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 75, 1, 0),
(103, 'Image', '2017-02-06 17:18:22', '2017-02-06 17:18:22', 'laktosefrei.png', 'laktosefrei', 'assets/vorlage/Kennzeichnungen/laktosefrei.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(104, 'Image', '2017-02-06 17:19:24', '2017-02-06 17:19:24', 'huhn.png', 'huhn', 'assets/vorlage/Kennzeichnungen/huhn.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 79, 1, 0),
(105, 'Image', '2017-02-20 15:26:04', '2017-02-20 15:26:04', 'heimat.png', 'heimat', 'assets/vorlage/restaurant-vorlage/Essenskategorien/heimat.png', NULL, 1, 'Inherit', 'Inherit', 'Inherit', 'Inherit', 80, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_DeleterGroups`
--

CREATE TABLE `File_DeleterGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_DeleterGroups`
--

INSERT INTO `File_DeleterGroups` (`ID`, `FileID`, `GroupID`) VALUES
(1, 78, 2),
(2, 78, 3),
(3, 78, 1),
(4, 79, 2),
(5, 79, 3),
(6, 79, 1),
(9, 73, 2),
(10, 73, 3),
(11, 73, 4),
(12, 73, 5),
(13, 73, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_EditorFolderGroups`
--

CREATE TABLE `File_EditorFolderGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_EditorFolderGroups`
--

INSERT INTO `File_EditorFolderGroups` (`ID`, `FileID`, `GroupID`) VALUES
(1, 78, 2),
(2, 78, 3),
(3, 78, 1),
(4, 79, 2),
(5, 79, 3),
(6, 79, 1),
(7, 73, 2),
(8, 73, 3),
(9, 73, 4),
(10, 73, 5),
(11, 73, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_UploaderGroups`
--

CREATE TABLE `File_UploaderGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_UploaderGroups`
--

INSERT INTO `File_UploaderGroups` (`ID`, `FileID`, `GroupID`) VALUES
(1, 73, 2),
(2, 73, 1),
(3, 78, 2),
(4, 78, 3),
(5, 78, 1),
(6, 79, 2),
(7, 79, 3),
(8, 79, 1),
(9, 73, 3),
(10, 73, 4),
(11, 73, 5),
(12, 1, 2),
(13, 1, 1),
(14, 11, 2),
(15, 11, 1),
(16, 12, 2),
(17, 12, 1),
(18, 44, 2),
(19, 44, 1),
(20, 56, 2),
(21, 56, 1),
(22, 58, 2),
(23, 58, 1),
(24, 59, 2),
(25, 59, 1),
(26, 72, 2),
(27, 72, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `File_ViewerGroups`
--

CREATE TABLE `File_ViewerGroups` (
  `ID` int(11) NOT NULL,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `File_ViewerGroups`
--

INSERT INTO `File_ViewerGroups` (`ID`, `FileID`, `GroupID`) VALUES
(1, 73, 2),
(2, 73, 3),
(3, 73, 4),
(4, 73, 5),
(5, 73, 1),
(6, 72, 2),
(7, 72, 3),
(8, 72, 4),
(9, 72, 5),
(10, 72, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FoodCategory`
--

CREATE TABLE `FoodCategory` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('FoodCategory') DEFAULT 'FoodCategory',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `Infotext` mediumtext,
  `Infotext_en_US` mediumtext,
  `Color` varchar(7) DEFAULT NULL,
  `IconUnicodeCode` varchar(50) DEFAULT NULL,
  `IsMain` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `FoodCategory`
--

INSERT INTO `FoodCategory` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Infotext`, `Infotext_en_US`, `Color`, `IconUnicodeCode`, `IsMain`, `SortOrder`, `ImageID`, `RestaurantPageID`) VALUES
(1, 'FoodCategory', '2016-10-27 11:35:13', '2017-02-20 15:25:23', 'Suppe', 'Soup', NULL, NULL, '#977c6b', NULL, 0, 1, 83, 7),
(2, 'FoodCategory', '2016-10-27 11:35:48', '2016-10-27 11:36:05', 'Vorspeise', 'Appetizer', NULL, NULL, NULL, NULL, 0, 10, 0, 7),
(3, 'FoodCategory', '2016-10-27 11:36:27', '2017-02-01 17:12:36', 'Aktion', 'Aktion', NULL, NULL, '#c00303', NULL, 1, 11, 0, 7),
(4, 'FoodCategory', '2016-10-27 11:36:58', '2017-02-20 15:27:00', 'Vital', 'Vital', NULL, NULL, '#6b9480', NULL, 1, 5, 84, 7),
(5, 'FoodCategory', '2016-10-27 11:37:28', '2017-02-20 15:26:06', 'Heimat', 'Heimat', NULL, NULL, '#5680a2', NULL, 1, 2, 105, 7),
(6, 'FoodCategory', '2016-10-27 11:38:44', '2017-02-20 15:26:45', 'Weltweit', 'Weltweit', NULL, NULL, '#dc9244', NULL, 1, 4, 90, 7),
(7, 'FoodCategory', '2016-10-27 11:39:24', '2017-02-20 15:27:29', 'Beilagen', 'Sides', NULL, NULL, '#b84d1b', NULL, 0, 7, 86, 7),
(8, 'FoodCategory', '2016-10-27 11:39:59', '2017-02-20 15:28:21', 'Salat', 'Salad', NULL, NULL, '#93ae40', NULL, 0, 8, 87, 7),
(9, 'FoodCategory', '2016-10-27 11:40:31', '2017-02-20 15:28:00', 'Dessert', 'Dessert', NULL, NULL, '#d57575', NULL, 0, 9, 88, 7),
(10, 'FoodCategory', '2017-02-01 17:06:26', '2017-02-20 15:27:15', 'Kalter Genuss', 'Kalter Genuss', NULL, NULL, '#6da3ae', NULL, 1, 6, 85, 7),
(11, 'FoodCategory', '2017-02-01 17:10:22', '2017-02-20 15:26:26', 'Szene', 'Szene', NULL, NULL, '#a2709a', NULL, 1, 3, 91, 7),
(12, 'FoodCategory', '2016-10-27 11:35:13', '2017-02-06 18:01:57', 'Suppe', 'Soup', NULL, NULL, '#977c6b', NULL, 0, 12, 83, 39),
(13, 'FoodCategory', '2016-10-27 11:37:28', '2017-02-06 18:01:57', 'Heimat', 'Heimat', NULL, NULL, '#5680a2', NULL, 1, 12, 89, 39),
(14, 'FoodCategory', '2017-02-01 17:10:22', '2017-02-06 18:01:57', 'Szene', 'Szene', NULL, NULL, '#a2709a', NULL, 1, 12, 91, 39),
(15, 'FoodCategory', '2016-10-27 11:38:44', '2017-02-06 18:01:57', 'Weltweit', 'Weltweit', NULL, NULL, '#dc9244', NULL, 1, 12, 90, 39),
(16, 'FoodCategory', '2016-10-27 11:36:58', '2017-02-06 18:01:57', 'Vital', 'Vital', NULL, NULL, '#6b9480', NULL, 1, 12, 84, 39),
(17, 'FoodCategory', '2017-02-01 17:06:26', '2017-02-06 18:01:57', 'Kalter Genuss', 'Kalter Genuss', NULL, NULL, '#6da3ae', NULL, 1, 12, 85, 39),
(18, 'FoodCategory', '2016-10-27 11:39:24', '2017-02-06 18:01:57', 'Beilagen', 'Sides', NULL, NULL, '#b84d1b', NULL, 0, 12, 86, 39),
(19, 'FoodCategory', '2016-10-27 11:39:59', '2017-02-06 18:01:57', 'Salat', 'Salad', NULL, NULL, '#93ae40', NULL, 0, 12, 87, 39),
(20, 'FoodCategory', '2016-10-27 11:40:31', '2017-02-06 18:01:57', 'Dessert', 'Dessert', NULL, NULL, '#d57575', NULL, 0, 12, 88, 39),
(21, 'FoodCategory', '2016-10-27 11:35:48', '2017-02-06 18:01:57', 'Vorspeise', 'Appetizer', NULL, NULL, NULL, NULL, 0, 12, 0, 39),
(22, 'FoodCategory', '2016-10-27 11:36:27', '2017-02-06 18:01:57', 'Aktion', 'Aktion', NULL, NULL, '#c00303', NULL, 1, 12, 0, 39);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FoodCategoryBlacklistConfig`
--

CREATE TABLE `FoodCategoryBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('FoodCategoryBlacklistConfig') DEFAULT 'FoodCategoryBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `FoodCategoryBlacklistObject`
--

CREATE TABLE `FoodCategoryBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('FoodCategoryBlacklistObject') DEFAULT 'FoodCategoryBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `FoodCategoryBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalAdditive`
--

CREATE TABLE `GlobalAdditive` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalAdditive') DEFAULT 'GlobalAdditive',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `GlobalAdditive`
--

INSERT INTO `GlobalAdditive` (`ID`, `ClassName`, `Created`, `LastEdited`, `Number`, `Title`, `Title_en_US`, `SortOrder`) VALUES
(1, 'GlobalAdditive', '2016-11-17 17:01:03', '2016-11-17 17:02:59', '1', 'mit Konservierungsstoff', 'Contains preservatives', 0),
(2, 'GlobalAdditive', '2016-11-17 17:03:41', '2016-11-17 17:03:41', '2', 'mit Farbstoff', 'Contains food colouring', 0),
(3, 'GlobalAdditive', '2016-11-17 17:04:26', '2016-11-17 17:04:26', '3', 'mit Antioxidationsmittel', 'Contains antioxidants', 0),
(4, 'GlobalAdditive', '2016-11-17 17:04:58', '2016-11-17 17:04:58', '4', 'mit Geschmacksverstärker', 'Contains flavour enhancers', 0),
(5, 'GlobalAdditive', '2016-11-17 17:05:41', '2016-11-17 17:05:41', '5', 'mit Phosphat', 'Contains phosphate', 0),
(6, 'GlobalAdditive', '2016-11-17 17:06:29', '2016-11-17 17:06:29', '6', 'Contains phosphate', 'Contains sweetener', 0),
(7, 'GlobalAdditive', '2016-11-17 17:07:21', '2016-11-17 17:07:21', '7', 'enthält eine Phenylalaninquelle', 'Contains a source of phenylalanine', 0),
(8, 'GlobalAdditive', '2016-11-17 17:08:57', '2016-11-17 17:08:57', '8', 'geschwefelt', 'Sulfurised', 0),
(9, 'GlobalAdditive', '2016-11-17 17:09:19', '2016-11-17 17:09:19', '9', 'geschwärzt', 'Blackened', 0),
(10, 'GlobalAdditive', '2016-11-17 17:10:28', '2016-11-17 17:10:28', '10', 'gewachst', 'Waxed', 0),
(11, 'GlobalAdditive', '2016-11-17 17:11:18', '2016-11-17 17:11:18', '11', 'koffeinhaltig', 'Contains caffeine', 0),
(12, 'GlobalAdditive', '2016-11-17 17:11:53', '2016-11-17 17:11:53', '12', 'chininhaltig', 'Contains quinine', 0),
(13, 'GlobalAdditive', '2016-11-17 17:15:05', '2016-11-17 17:15:05', '13', 'enthält Alkohol', 'Contains alcohol', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalAllergen`
--

CREATE TABLE `GlobalAllergen` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalAllergen') DEFAULT 'GlobalAllergen',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `GlobalAllergen`
--

INSERT INTO `GlobalAllergen` (`ID`, `ClassName`, `Created`, `LastEdited`, `Number`, `Title`, `Title_en_US`, `SortOrder`) VALUES
(1, 'GlobalAllergen', '2016-11-17 17:16:10', '2016-11-17 17:16:10', 'Aa', 'Weizen', 'Wheat', 0),
(2, 'GlobalAllergen', '2016-11-17 17:16:54', '2016-11-17 17:16:54', 'Ab', 'Roggen', 'Rye', 0),
(3, 'GlobalAllergen', '2016-11-17 17:18:01', '2016-11-17 17:18:01', 'Ac', 'Gerste', 'Barley', 0),
(4, 'GlobalAllergen', '2016-11-17 17:18:42', '2016-11-17 17:18:42', 'Ad', 'Hafer', 'Oats', 0),
(5, 'GlobalAllergen', '2016-11-17 17:19:21', '2016-11-17 17:19:21', 'B', 'Krebstiere', 'Crustaceans', 0),
(6, 'GlobalAllergen', '2016-11-17 17:20:29', '2016-11-17 17:20:29', 'C', 'Eier', 'Eggs', 0),
(7, 'GlobalAllergen', '2016-11-17 17:20:59', '2016-11-17 17:20:59', 'D', 'Fisch', 'Fish', 0),
(8, 'GlobalAllergen', '2016-11-17 17:21:53', '2016-11-17 17:21:53', 'E', 'Erdnüsse', 'Peanuts', 0),
(9, 'GlobalAllergen', '2016-11-17 17:22:39', '2016-11-17 17:22:39', 'F', 'Soja', 'Soybeans', 0),
(10, 'GlobalAllergen', '2016-11-17 17:23:03', '2016-11-17 17:23:03', 'G', 'Milch (einschließlich Lactose)', 'Milk (including lactose)', 0),
(11, 'GlobalAllergen', '2016-11-17 17:24:11', '2016-11-17 17:24:11', 'Ha', 'Mandeln', 'Almonds', 0),
(12, 'GlobalAllergen', '2016-11-17 17:24:41', '2016-11-17 17:24:41', 'Hb', 'Haselnüsse', 'Almonds', 0),
(13, 'GlobalAllergen', '2016-11-17 17:25:44', '2016-11-17 17:25:44', 'Hc', 'Walnüsse', 'Walnuts', 0),
(14, 'GlobalAllergen', '2016-11-17 17:26:46', '2016-11-17 17:26:46', 'Hd', 'Cashewnüsse', 'Cashews', 0),
(15, 'GlobalAllergen', '2016-11-17 17:27:32', '2016-11-17 17:27:32', 'He', 'Pekannüsse', 'Pecan nuts', 0),
(16, 'GlobalAllergen', '2016-11-17 17:28:01', '2016-11-17 17:28:01', 'Hf', 'Paranüsse', 'Brazil nuts', 0),
(17, 'GlobalAllergen', '2016-11-17 17:29:22', '2016-11-17 17:29:22', 'Hg', 'Pistazien', 'Pistachio', 0),
(18, 'GlobalAllergen', '2016-11-17 17:30:02', '2016-11-17 17:30:02', 'Hh', 'Macadamianüsse', 'Macadamia', 0),
(19, 'GlobalAllergen', '2016-11-17 17:31:16', '2016-11-17 17:31:43', 'I', 'Sellerie', 'Celery', 0),
(20, 'GlobalAllergen', '2016-11-17 17:32:14', '2016-11-17 17:32:14', 'J', 'Senf', 'Mustard', 0),
(21, 'GlobalAllergen', '2016-11-17 17:33:12', '2016-11-17 17:33:12', 'K', 'Sesamsamen', 'Sesame', 0),
(22, 'GlobalAllergen', '2016-11-17 17:35:41', '2016-11-17 17:35:41', 'L', 'Schwefeldioxid und Sulphite', 'Sulphor dioxide and sulphites', 0),
(23, 'GlobalAllergen', '2016-11-17 17:37:06', '2016-11-17 17:37:06', 'M', 'Lupinen', 'Lupin', 0),
(24, 'GlobalAllergen', '2016-11-17 17:38:07', '2016-11-17 17:38:07', 'N', 'Weichtiere', 'Molluscs', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalDishLabel`
--

CREATE TABLE `GlobalDishLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalDishLabel') DEFAULT 'GlobalDishLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `IconUnicodeCode` varchar(50) DEFAULT NULL,
  `Color` varchar(7) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `GlobalDishLabel`
--

INSERT INTO `GlobalDishLabel` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `IconUnicodeCode`, `Color`, `SortOrder`, `ImageID`) VALUES
(1, 'GlobalDishLabel', '2016-11-17 16:38:42', '2016-11-17 16:38:42', 'vegetarisch', 'vegetarian', '76', NULL, 1, 54),
(2, 'GlobalDishLabel', '2016-11-17 16:40:00', '2016-11-17 16:59:15', 'vegan', 'vegan', '56', NULL, 2, 53),
(3, 'GlobalDishLabel', '2016-11-17 16:41:39', '2016-11-17 16:41:39', 'Fisch', 'fish', '66', NULL, 3, 46),
(4, 'GlobalDishLabel', '2016-11-17 16:42:23', '2016-11-17 16:42:23', 'Schwein', 'pork', '73', NULL, 4, 51),
(5, 'GlobalDishLabel', '2016-11-17 16:44:15', '2016-11-17 16:44:15', 'Rind', 'beef', '72', NULL, 5, 50),
(6, 'GlobalDishLabel', '2016-11-17 16:44:48', '2016-11-17 16:44:48', 'Geflügel', 'chicken', '68', NULL, 6, 47),
(7, 'GlobalDishLabel', '2016-11-17 16:46:35', '2016-11-17 16:46:35', 'scharf', 'spicy', '63', NULL, 7, 45),
(8, 'GlobalDishLabel', '2016-11-17 16:47:31', '2016-11-17 16:47:31', 'Knoblauch', 'garlic', '6b', NULL, 8, 48),
(9, 'GlobalDishLabel', '2016-11-17 16:49:15', '2016-11-17 16:49:15', 'Zwiebel', 'onion', '7a', NULL, 9, 55),
(10, 'GlobalDishLabel', '2016-11-17 16:49:54', '2016-11-17 16:50:16', 'laktosefrei', 'lactose free', '6c', NULL, 10, 49),
(11, 'GlobalDishLabel', '2016-11-17 16:54:33', '2016-11-17 16:56:00', 'Aktion', 'special', '74', '#bd0f1f', 11, 52);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GlobalSpecialLabel`
--

CREATE TABLE `GlobalSpecialLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('GlobalSpecialLabel') DEFAULT 'GlobalSpecialLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `GlobalSpecialLabel`
--

INSERT INTO `GlobalSpecialLabel` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `SortOrder`, `ImageID`) VALUES
(1, 'GlobalSpecialLabel', '2016-11-17 16:28:44', '2016-11-17 16:28:44', 'Classic Edition', 'Classic Edition', 0, 2),
(2, 'GlobalSpecialLabel', '2016-11-17 16:29:11', '2016-11-17 16:29:11', 'Coa', 'Coa', 0, 3),
(3, 'GlobalSpecialLabel', '2016-11-17 16:29:37', '2016-11-17 16:29:37', 'Curry Edition', 'Curry Edition', 0, 4),
(4, 'GlobalSpecialLabel', '2016-11-17 16:30:05', '2016-11-17 16:30:05', 'dean & david', 'dean & david', 0, 5),
(5, 'GlobalSpecialLabel', '2016-11-17 16:30:37', '2016-11-17 16:30:37', 'fit for fun', 'fit for fun', 0, 6),
(6, 'GlobalSpecialLabel', '2016-11-17 16:31:23', '2016-11-17 16:31:23', 'fit for fun & vitalien', 'fit for fun & vitalien', 0, 7),
(7, 'GlobalSpecialLabel', '2016-11-17 16:31:49', '2016-11-17 16:31:49', 'Sushi Circle', 'Sushi Circle', 0, 8),
(8, 'GlobalSpecialLabel', '2016-11-17 16:32:18', '2016-11-17 16:32:18', 'vitalien', 'vitalien', 0, 9);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group`
--

CREATE TABLE `Group` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Group') DEFAULT 'Group',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` mediumtext,
  `Code` varchar(255) DEFAULT NULL,
  `Locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext,
  `AccessAllSubsites` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Group`
--

INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `AccessAllSubsites`, `ParentID`) VALUES
(1, 'Group', '2016-10-17 09:06:31', '2016-10-17 13:56:18', 'Admin System', NULL, 'content-authors', 0, 1, NULL, 1, 0),
(2, 'Group', '2016-10-17 09:06:31', '2016-10-17 11:44:25', 'Admin Global', NULL, 'administrators', 0, 0, NULL, 1, 0),
(3, 'Group', '2016-10-17 10:24:06', '2016-11-26 13:13:59', 'Admin Mandant', NULL, 'admin-system', 0, 0, NULL, 1, 0),
(4, 'Group', '2016-10-17 10:24:16', '2016-10-17 11:44:51', 'Admin Restaurant', NULL, 'admin-mandant', 0, 0, NULL, 1, 0),
(5, 'Group', '2016-10-17 10:24:26', '2016-11-26 13:16:38', 'Editor Restaurant', NULL, 'admin-restaurant', 0, 0, NULL, 1, 0),
(6, 'Group', '2016-10-17 10:24:55', '2016-10-17 11:57:21', 'Admin Microsite', NULL, 'admin-microsite', 0, 0, NULL, 1, 0),
(7, 'Group', '2016-10-17 10:25:07', '2016-10-17 11:45:38', 'Editor Microsite', NULL, 'editor-restaurant', 0, 0, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group_Members`
--

CREATE TABLE `Group_Members` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Group_Members`
--

INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES
(2, 1, 1),
(3, 5, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group_Roles`
--

CREATE TABLE `Group_Roles` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Group_Roles`
--

INSERT INTO `Group_Roles` (`ID`, `GroupID`, `PermissionRoleID`) VALUES
(1, 1, 2),
(2, 2, 1),
(3, 3, 3),
(5, 4, 4),
(6, 5, 5),
(7, 7, 6),
(8, 6, 8);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Group_Subsites`
--

CREATE TABLE `Group_Subsites` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportConfig`
--

CREATE TABLE `ImportConfig` (
  `ID` int(11) NOT NULL,
  `URL` varchar(2083) DEFAULT NULL,
  `NutritivesMapConfigID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `AdditivesBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `AllergensBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `DishLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0',
  `SpecialLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportFilterConfig`
--

CREATE TABLE `ImportFilterConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('ImportFilterConfig') DEFAULT 'ImportFilterConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportMenuXmlConfig`
--

CREATE TABLE `ImportMenuXmlConfig` (
  `ID` int(11) NOT NULL,
  `ImportFunction` enum('import_lz_catering','import_roche_rotkreuz','import_roche_rotkreuz_productinfo') DEFAULT 'import_lz_catering',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportRestaurantConfig`
--

CREATE TABLE `ImportRestaurantConfig` (
  `ID` int(11) NOT NULL,
  `ImportFunction` enum('import_lz_catering') DEFAULT 'import_lz_catering',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ImportVisitorCounterConfig`
--

CREATE TABLE `ImportVisitorCounterConfig` (
  `ID` int(11) NOT NULL,
  `URL` varchar(2083) DEFAULT NULL,
  `ImportFunction` enum('import_sensalytics') DEFAULT 'import_sensalytics',
  `PredictionBase` enum('LastWeek','Continous','SameMonthLastYear') DEFAULT 'Continous',
  `Capacity` int(11) NOT NULL DEFAULT '0',
  `CounterDataStart` time DEFAULT NULL,
  `CounterDataEnd` time DEFAULT NULL,
  `CounterDataIntervall` int(11) NOT NULL DEFAULT '0',
  `VisitorThresholdMedium` int(11) NOT NULL DEFAULT '0',
  `VisitorThresholdHigh` int(11) NOT NULL DEFAULT '0',
  `PredicitionDataLastCalculated` datetime DEFAULT NULL,
  `ChartMinY` int(11) NOT NULL DEFAULT '0',
  `ChartMaxY` int(11) NOT NULL DEFAULT '0',
  `ApiHttpMethod` enum('GET','POST','PUT') DEFAULT 'PUT',
  `ApiRequestBody` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessage`
--

CREATE TABLE `InfoMessage` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('InfoMessage','InfoMessageDisplayTemplatePage','InfoMessageDisplayTemplatePageLandscape','InfoMessageDisplayTemplatePagePortrait','InfoMessageMicrositeHomePage','InfoMessageMicrositeNewsPage','InfoMessageMobileTemplatePage','InfoMessagePrintTemplatePage','InfoMessageTemplate','InfoMessageTemplateDisplayTemplatePage','InfoMessageTemplateDisplayTemplatePageLandscape','InfoMessageTemplateDisplayTemplatePagePortrait','InfoMessageTemplateMicrositeHomePage','InfoMessageTemplateMicrositeNewsPage','InfoMessageTemplateMobileTemplatePage','InfoMessageTemplatePrintTemplatePage') DEFAULT 'InfoMessage',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content` mediumtext,
  `Content_en_US` mediumtext,
  `ImageOnly` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `TimerStartDate` date DEFAULT NULL,
  `TimerStartTime` time DEFAULT NULL,
  `TimerEndDate` date DEFAULT NULL,
  `TimerEndTime` time DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessage`
--

INSERT INTO `InfoMessage` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `ImageOnly`, `Publish`, `TimerStartDate`, `TimerStartTime`, `TimerEndDate`, `TimerEndTime`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES
(1, 'InfoMessageDisplayTemplatePageLandscape', '2016-10-27 12:19:02', '2016-10-27 17:20:08', 'dean & david', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, 1, 60),
(2, 'InfoMessageMicrositeHomePage', '2016-10-27 12:40:32', '2016-10-27 12:43:13', 'Vielseitige Kulinarik', NULL, '<p>Ob leicht oder deftig – hier findet jeder etwas für seinen Geschmack</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 1, 1, 67),
(3, 'InfoMessageMicrositeHomePage', '2016-10-27 12:41:23', '2016-10-27 12:43:13', 'Mit viel Energie durch den Tag', NULL, '<p>Das passende Mittagsangebot hilft Ihnen dabei, mit neuer Energie frisch durchzustarten</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 2, 1, 66),
(4, 'InfoMessageMicrositeHomePage', '2016-10-27 12:43:13', '2016-10-27 12:43:13', 'Foodtrends und Lieblingsrezepte im Fokus', NULL, '<p>Kantine war gestern – in Ihrem Betriebsrestaurant möchten wir Ihnen viel Abwechslung uns spannende Neuheiten bieten.</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 3, 1, 63),
(5, 'InfoMessageMicrositeNewsPage', '2016-10-27 13:22:41', '2016-11-18 09:20:44', 'Französisches Flair mit Vitalien-Special „Vive le football“ bei der Compass Group Deutschland', NULL, '<p><strong>Pünktlich zur Fußball-Europameisterschaft bringt die Compass Group Deutschland vom 10. Juni bis zum 10. Juli 2016 mit sommerlich leichten und sportlichen Gerichten französisches Flair in ihre Eurest-Restaurants.</strong></p>\n<div>\n<p>„Vive le football“ heißt das Special, das an fünf Tagen, auf mehrere Wochen verteilt, mit jeweils einem speziell zubereiteten Gericht der Marke „Vitalien“ die Gäste in die Restaurants lockt. Die Menüs spiegeln eine Auswahl typischer Speisen aus der französischen Landesküche, gemischt mit moderner Küche, wider. Das Besondere: Die kulinarischen Highlights sollen die Leistungsfähigkeit fördern und nicht belasten.</p>\n<p><strong>Unvergesslicher Genuss aus Frankreich</strong><br>Gutes Essen gehört in der Grande Nation zur Lebensart dazu. Je nach Region reicht die Spannbreite von mediterranen bis hin zu deftigen Spezialitäten. „Dass französische Küche sommerlich leicht sein kann, zeigen unsere Vitalien-Menüs“, sagt Attila Wandrey, Head of Marketing &amp; Communications. „Unsere kalorienreduzierten und fettarmen Rezepturen wirken nicht nur der Müdigkeit und dem Mittagstief entgegen, sondern sind auch optimale Fitmacher für einen langen Arbeitstag.“</p>\n<p><strong>Exquisite Rundreise durch Frankreich</strong><br>Um in diesem Zusammenhang der Vielfalt an Köstlichkeiten gerecht zu werden, variieren die Gerichte an den Special-Tagen je nach Region, in der die Spiele stattfinden. So finden sich neben klassischen Produkten wie frischen Kräutern und Gewürzen auch Hülsenfrüchte und fettreduzierte Öle wie Olivenöl unter den Zutaten wieder. Liebhaber der traditionellen Küche dürfen sich vorab auf delikate Hochgenüsse wie „Bretonische Bouillabaisse mit Meeresfrüchten und Croutons“ sowie „Französische Entenbrust auf Ragout von roten Linsen mit Dijon-Senf-Kräuter-Hollandaise“ freuen.</p>\n</div>', NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 1, 67),
(6, 'InfoMessageDisplayTemplatePageLandscape', '2017-02-06 10:22:43', '2017-02-06 10:22:43', NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 1, 102),
(7, 'InfoMessageDisplayTemplatePageLandscape', '2016-10-27 12:19:02', '2017-02-06 18:01:59', 'dean & david', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, 1, 60),
(8, 'InfoMessageDisplayTemplatePageLandscape', '2017-02-06 10:22:43', '2017-02-06 18:01:59', NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 1, 102),
(9, 'InfoMessageMicrositeHomePage', '2016-10-27 12:40:32', '2017-02-06 18:02:00', 'Vielseitige Kulinarik', NULL, '<p>Ob leicht oder deftig – hier findet jeder etwas für seinen Geschmack</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 1, 1, 67),
(10, 'InfoMessageMicrositeHomePage', '2016-10-27 12:41:23', '2017-02-06 18:02:00', 'Mit viel Energie durch den Tag', NULL, '<p>Das passende Mittagsangebot hilft Ihnen dabei, mit neuer Energie frisch durchzustarten</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 2, 1, 66),
(11, 'InfoMessageMicrositeHomePage', '2016-10-27 12:43:13', '2017-02-06 18:02:00', 'Foodtrends und Lieblingsrezepte im Fokus', NULL, '<p>Kantine war gestern – in Ihrem Betriebsrestaurant möchten wir Ihnen viel Abwechslung uns spannende Neuheiten bieten.</p>', NULL, 0, 1, NULL, NULL, NULL, NULL, 3, 1, 63),
(12, 'InfoMessageMicrositeNewsPage', '2016-10-27 13:22:41', '2017-02-06 18:02:00', 'Französisches Flair mit Vitalien-Special „Vive le football“ bei der Compass Group Deutschland', NULL, '<p><strong>Pünktlich zur Fußball-Europameisterschaft bringt die Compass Group Deutschland vom 10. Juni bis zum 10. Juli 2016 mit sommerlich leichten und sportlichen Gerichten französisches Flair in ihre Eurest-Restaurants.</strong></p>\n<div>\n<p>„Vive le football“ heißt das Special, das an fünf Tagen, auf mehrere Wochen verteilt, mit jeweils einem speziell zubereiteten Gericht der Marke „Vitalien“ die Gäste in die Restaurants lockt. Die Menüs spiegeln eine Auswahl typischer Speisen aus der französischen Landesküche, gemischt mit moderner Küche, wider. Das Besondere: Die kulinarischen Highlights sollen die Leistungsfähigkeit fördern und nicht belasten.</p>\n<p><strong>Unvergesslicher Genuss aus Frankreich</strong><br>Gutes Essen gehört in der Grande Nation zur Lebensart dazu. Je nach Region reicht die Spannbreite von mediterranen bis hin zu deftigen Spezialitäten. „Dass französische Küche sommerlich leicht sein kann, zeigen unsere Vitalien-Menüs“, sagt Attila Wandrey, Head of Marketing &amp; Communications. „Unsere kalorienreduzierten und fettarmen Rezepturen wirken nicht nur der Müdigkeit und dem Mittagstief entgegen, sondern sind auch optimale Fitmacher für einen langen Arbeitstag.“</p>\n<p><strong>Exquisite Rundreise durch Frankreich</strong><br>Um in diesem Zusammenhang der Vielfalt an Köstlichkeiten gerecht zu werden, variieren die Gerichte an den Special-Tagen je nach Region, in der die Spiele stattfinden. So finden sich neben klassischen Produkten wie frischen Kräutern und Gewürzen auch Hülsenfrüchte und fettreduzierte Öle wie Olivenöl unter den Zutaten wieder. Liebhaber der traditionellen Küche dürfen sich vorab auf delikate Hochgenüsse wie „Bretonische Bouillabaisse mit Meeresfrüchten und Croutons“ sowie „Französische Entenbrust auf Ragout von roten Linsen mit Dijon-Senf-Kräuter-Hollandaise“ freuen.</p>\n</div>', NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 1, 67);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageDisplayTemplatePageLandscape`
--

CREATE TABLE `InfoMessageDisplayTemplatePageLandscape` (
  `ID` int(11) NOT NULL,
  `DisplayTemplatePageLandscapeID` int(11) NOT NULL DEFAULT '0',
  `InfoMessageTemplateDisplayTemplatePageLandscapeID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessageDisplayTemplatePageLandscape`
--

INSERT INTO `InfoMessageDisplayTemplatePageLandscape` (`ID`, `DisplayTemplatePageLandscapeID`, `InfoMessageTemplateDisplayTemplatePageLandscapeID`) VALUES
(1, 23, 0),
(6, 25, 0),
(7, 51, 0),
(8, 53, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageDisplayTemplatePagePortrait`
--

CREATE TABLE `InfoMessageDisplayTemplatePagePortrait` (
  `ID` int(11) NOT NULL,
  `DisplayTemplatePagePortraitID` int(11) NOT NULL DEFAULT '0',
  `InfoMessageTemplateDisplayTemplatePagePortraitID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageMicrositeHomePage`
--

CREATE TABLE `InfoMessageMicrositeHomePage` (
  `ID` int(11) NOT NULL,
  `LinkType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `InfoMessageTemplateMicrositeHomePageID` int(11) NOT NULL DEFAULT '0',
  `MicrositeHomePageID` int(11) NOT NULL DEFAULT '0',
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  `DownloadID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessageMicrositeHomePage`
--

INSERT INTO `InfoMessageMicrositeHomePage` (`ID`, `LinkType`, `ExternalURL`, `InfoMessageTemplateMicrositeHomePageID`, `MicrositeHomePageID`, `LinkToID`, `DownloadID`) VALUES
(2, 'Internal', NULL, 0, 35, 0, 0),
(3, 'Internal', NULL, 0, 35, 0, 0),
(4, 'Internal', NULL, 0, 35, 0, 0),
(9, 'Internal', NULL, 0, 61, 0, 0),
(10, 'Internal', NULL, 0, 61, 0, 0),
(11, 'Internal', NULL, 0, 61, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageMicrositeNewsPage`
--

CREATE TABLE `InfoMessageMicrositeNewsPage` (
  `ID` int(11) NOT NULL,
  `InfoMessageTemplateMicrositeNewsPageID` int(11) NOT NULL DEFAULT '0',
  `MicrositeNewsPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `InfoMessageMicrositeNewsPage`
--

INSERT INTO `InfoMessageMicrositeNewsPage` (`ID`, `InfoMessageTemplateMicrositeNewsPageID`, `MicrositeNewsPageID`) VALUES
(5, 0, 36),
(12, 0, 62);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessageMobileTemplatePage`
--

CREATE TABLE `InfoMessageMobileTemplatePage` (
  `ID` int(11) NOT NULL,
  `InfoMessageTemplateMobileTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `MobileTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessagePrintTemplatePage`
--

CREATE TABLE `InfoMessagePrintTemplatePage` (
  `ID` int(11) NOT NULL,
  `InfoMessageTemplatePrintTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `PrintTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `PrintHtmlTemplatePageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `InfoMessage_TimerRecurringDaysOfWeek`
--

CREATE TABLE `InfoMessage_TimerRecurringDaysOfWeek` (
  `ID` int(11) NOT NULL,
  `InfoMessageID` int(11) NOT NULL DEFAULT '0',
  `RecurringDayOfWeekID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `LoginAttempt`
--

CREATE TABLE `LoginAttempt` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('LoginAttempt') DEFAULT 'LoginAttempt',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Status` enum('Success','Failure') DEFAULT 'Success',
  `IP` varchar(255) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Member`
--

CREATE TABLE `Member` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Member') DEFAULT 'Member',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Email` varchar(254) DEFAULT NULL,
  `TempIDHash` varchar(160) DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `RememberLoginToken` varchar(160) DEFAULT NULL,
  `NumVisit` int(11) NOT NULL DEFAULT '0',
  `LastVisited` datetime DEFAULT NULL,
  `AutoLoginHash` varchar(160) DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `DateFormat` varchar(30) DEFAULT NULL,
  `TimeFormat` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Member`
--

INSERT INTO `Member` (`ID`, `ClassName`, `Created`, `LastEdited`, `FirstName`, `Surname`, `Email`, `TempIDHash`, `TempIDExpired`, `Password`, `RememberLoginToken`, `NumVisit`, `LastVisited`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `DateFormat`, `TimeFormat`) VALUES
(1, 'Member', '2016-10-17 09:06:31', '2017-02-27 18:06:20', 'Standard Admin', NULL, 'admin', '2be22feb65c1307a00b4b7ef5e6af9b2ea4da884', '2017-03-02 09:22:26', '$2y$10$03513e6846e5d7b990218uZbYrQn4ocPeKrNElHcYxDidyKKZNsd2', NULL, 38, '2017-02-27 18:06:20', NULL, NULL, 'blowfish', '10$03513e6846e5d7b9902181', NULL, NULL, 'de_DE', 0, 'dd.MM.yyyy', 'HH:mm:ss'),
(2, 'Member', '2016-10-17 13:56:53', '2016-10-27 17:16:58', 'Standard Editor', NULL, 'editor', '5280f363ca35886ebe3c6d210d26e02fa0ffc9c5', '2016-10-30 16:16:58', '$2y$10$6e00eebfea3f79820ae81OLeMeQxA0PRUXuyPAiu/FTzmpB3TyI/a', NULL, 8, '2016-10-27 18:22:01', NULL, NULL, 'blowfish', '10$6e00eebfea3f79820ae81c', NULL, NULL, 'de_DE', 0, 'dd.MM.yyyy', 'HH:mm:ss');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MemberPassword`
--

CREATE TABLE `MemberPassword` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MemberPassword') DEFAULT 'MemberPassword',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MemberPassword`
--

INSERT INTO `MemberPassword` (`ID`, `ClassName`, `Created`, `LastEdited`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES
(1, 'MemberPassword', '2016-10-17 13:56:53', '2016-10-17 13:56:53', '$2y$10$6e00eebfea3f79820ae81OLeMeQxA0PRUXuyPAiu/FTzmpB3TyI/a', '10$6e00eebfea3f79820ae81c', 'blowfish', 2),
(2, 'MemberPassword', '2016-10-18 08:07:06', '2016-10-18 08:07:06', '$2y$10$03513e6846e5d7b990218uZbYrQn4ocPeKrNElHcYxDidyKKZNsd2', '10$03513e6846e5d7b9902181', 'blowfish', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MenuContainer`
--

CREATE TABLE `MenuContainer` (
  `ID` int(11) NOT NULL,
  `EmptyField` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MenuContainer`
--

INSERT INTO `MenuContainer` (`ID`, `EmptyField`) VALUES
(11, 0),
(40, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MenuContainer_Live`
--

CREATE TABLE `MenuContainer_Live` (
  `ID` int(11) NOT NULL,
  `EmptyField` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MenuContainer_Live`
--

INSERT INTO `MenuContainer_Live` (`ID`, `EmptyField`) VALUES
(11, 0),
(40, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MenuContainer_versions`
--

CREATE TABLE `MenuContainer_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `EmptyField` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MenuContainer_versions`
--

INSERT INTO `MenuContainer_versions` (`ID`, `RecordID`, `Version`, `EmptyField`) VALUES
(1, 11, 1, 0),
(2, 11, 2, 0),
(3, 11, 3, 0),
(4, 40, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContainer`
--

CREATE TABLE `MicrositeContainer` (
  `ID` int(11) NOT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContainer`
--

INSERT INTO `MicrositeContainer` (`ID`, `GoogleAnalyticsID`) VALUES
(34, NULL),
(60, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContainer_Live`
--

CREATE TABLE `MicrositeContainer_Live` (
  `ID` int(11) NOT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContainer_Live`
--

INSERT INTO `MicrositeContainer_Live` (`ID`, `GoogleAnalyticsID`) VALUES
(34, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContainer_versions`
--

CREATE TABLE `MicrositeContainer_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContainer_versions`
--

INSERT INTO `MicrositeContainer_versions` (`ID`, `RecordID`, `Version`, `GoogleAnalyticsID`) VALUES
(1, 34, 1, NULL),
(2, 34, 2, NULL),
(3, 60, 1, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeContentItem`
--

CREATE TABLE `MicrositeContentItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MicrositeContentItem') DEFAULT 'MicrositeContentItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content` mediumtext,
  `Content_en_US` mediumtext,
  `Background` enum('Color','Grey','None') DEFAULT 'Color',
  `ImagePosition` enum('Left','Right') DEFAULT 'Left',
  `LinkTitle` varchar(255) DEFAULT NULL,
  `LinkTitle_en_US` varchar(255) DEFAULT NULL,
  `LinkType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `MicrositePageID` int(11) NOT NULL DEFAULT '0',
  `MicrositeHomePageID` int(11) NOT NULL DEFAULT '0',
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeContentItem`
--

INSERT INTO `MicrositeContentItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `Content`, `Content_en_US`, `Background`, `ImagePosition`, `LinkTitle`, `LinkTitle_en_US`, `LinkType`, `ExternalURL`, `Sort`, `MicrositePageID`, `MicrositeHomePageID`, `LinkToID`, `ImageID`) VALUES
(1, 'MicrositeContentItem', '2016-10-27 12:44:15', '2016-10-27 17:01:04', 'Und für zwischendurch…', NULL, '<h4>Den kleinen Hunger oder Durst zwischendurch können Sie mit unserer Auswahl an leckeren Snacks und Getränken in unserem Caffè Dallucci stillen.</h4>\n<p>Ob der Espresso nach dem Mittagsessen oder ein leckeres belegtes Brötchen am Nachmittag – mit unserem Snacking-Angebot im Caffè Dallucci kommen Sie gut durch den Tag. Sie treffen unsere freundlichen Kollegen hier täglich von 08:00 bis 17:00 Uhr an.</p>', NULL, 'Color', 'Left', 'Snacking', NULL, 'Internal', NULL, 0, 0, 35, 38, 0),
(2, 'MicrositeContentItem', '2016-10-27 14:30:35', '2016-10-27 17:01:49', NULL, NULL, '<p>In unseren Betriebsrestaurants \"Name1 und \"Name2\" stehen Service und Genuss an erster Stelle! Wir bieten Ihnen täglich ein leckeres wechselndes Aktionsgericht an, lassen Sie sich von unserem Küchenleiter überraschen. Welches Gericht das sein wird, können Sie morgens direkt in der Cafeteria an der jeweiligen Tafel am Eingang entnehmen.<br><br><strong>Wir freuen uns auf Sie!</strong></p>', NULL, 'Color', 'Left', NULL, NULL, 'Internal', NULL, 0, 37, 0, 0, 66),
(3, 'MicrositeContentItem', '2016-10-27 14:32:41', '2016-10-27 14:32:41', 'Caffé Dallucci', NULL, '<p>... steht für Genuss und Entspannung. <br>Nehmen Sie sich eine Auszeit vom Alltag!</p>', NULL, 'Color', 'Left', NULL, NULL, 'Internal', NULL, 0, 38, 0, 0, 65),
(4, 'MicrositeContentItem', '2016-10-27 12:44:15', '2017-02-06 18:02:00', 'Und für zwischendurch…', NULL, '<h4>Den kleinen Hunger oder Durst zwischendurch können Sie mit unserer Auswahl an leckeren Snacks und Getränken in unserem Caffè Dallucci stillen.</h4>\n<p>Ob der Espresso nach dem Mittagsessen oder ein leckeres belegtes Brötchen am Nachmittag – mit unserem Snacking-Angebot im Caffè Dallucci kommen Sie gut durch den Tag. Sie treffen unsere freundlichen Kollegen hier täglich von 08:00 bis 17:00 Uhr an.</p>', NULL, 'Color', 'Left', 'Snacking', NULL, 'Internal', NULL, 0, 0, 61, 38, 0),
(5, 'MicrositeContentItem', '2016-10-27 14:30:35', '2017-02-06 18:02:00', NULL, NULL, '<p>In unseren Betriebsrestaurants \"Name1 und \"Name2\" stehen Service und Genuss an erster Stelle! Wir bieten Ihnen täglich ein leckeres wechselndes Aktionsgericht an, lassen Sie sich von unserem Küchenleiter überraschen. Welches Gericht das sein wird, können Sie morgens direkt in der Cafeteria an der jeweiligen Tafel am Eingang entnehmen.<br><br><strong>Wir freuen uns auf Sie!</strong></p>', NULL, 'Color', 'Left', NULL, NULL, 'Internal', NULL, 0, 63, 0, 0, 66),
(6, 'MicrositeContentItem', '2016-10-27 14:32:41', '2017-02-06 18:02:00', 'Caffé Dallucci', NULL, '<p>... steht für Genuss und Entspannung. <br>Nehmen Sie sich eine Auszeit vom Alltag!</p>', NULL, 'Color', 'Left', NULL, NULL, 'Internal', NULL, 0, 64, 0, 0, 65);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeHomePage`
--

CREATE TABLE `MicrositeHomePage` (
  `ID` int(11) NOT NULL,
  `ShowMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeHomePage`
--

INSERT INTO `MicrositeHomePage` (`ID`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES
(35, 1, 1, NULL, NULL, NULL),
(61, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeHomePage_Live`
--

CREATE TABLE `MicrositeHomePage_Live` (
  `ID` int(11) NOT NULL,
  `ShowMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeHomePage_Live`
--

INSERT INTO `MicrositeHomePage_Live` (`ID`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES
(35, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeHomePage_versions`
--

CREATE TABLE `MicrositeHomePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ShowMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeHomePage_versions`
--

INSERT INTO `MicrositeHomePage_versions` (`ID`, `RecordID`, `Version`, `ShowMenu`, `ShowOpeningHours`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES
(1, 33, 1, 0, 0, NULL, NULL, NULL),
(2, 35, 1, 0, 0, NULL, NULL, NULL),
(3, 35, 2, 0, 0, NULL, NULL, NULL),
(4, 35, 3, 1, 1, NULL, NULL, NULL),
(5, 35, 4, 1, 1, NULL, NULL, NULL),
(6, 61, 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeLinkItem`
--

CREATE TABLE `MicrositeLinkItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MicrositeLinkItem') DEFAULT 'MicrositeLinkItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `LinkType` enum('Internal','External','Download') DEFAULT 'Download',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `MicrositePageID` int(11) NOT NULL DEFAULT '0',
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  `DownloadID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositeLinkItem`
--

INSERT INTO `MicrositeLinkItem` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `LinkType`, `ExternalURL`, `Sort`, `MicrositePageID`, `LinkToID`, `DownloadID`) VALUES
(1, 'MicrositeLinkItem', '2016-10-27 14:27:20', '2016-10-27 14:28:48', 'Speiseplan KW XX', NULL, 'Download', NULL, 0, 37, 0, 77),
(2, 'MicrositeLinkItem', '2016-10-27 14:27:20', '2017-02-06 18:02:00', 'Speiseplan KW XX', NULL, 'Download', NULL, 0, 63, 0, 77);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositePage`
--

CREATE TABLE `MicrositePage` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositePage`
--

INSERT INTO `MicrositePage` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES
(36, NULL, NULL, NULL),
(37, NULL, 'Aktuelle Speisepläne', NULL),
(38, NULL, NULL, NULL),
(62, NULL, NULL, NULL),
(63, NULL, 'Aktuelle Speisepläne', NULL),
(64, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositePage_Live`
--

CREATE TABLE `MicrositePage_Live` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositePage_Live`
--

INSERT INTO `MicrositePage_Live` (`ID`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES
(36, NULL, NULL, NULL),
(37, NULL, 'Aktuelle Speisepläne', NULL),
(38, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositePage_versions`
--

CREATE TABLE `MicrositePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MicrositePage_versions`
--

INSERT INTO `MicrositePage_versions` (`ID`, `RecordID`, `Version`, `Content_en_US`, `MicrositeLinkItemsTitle`, `MicrositeLinkItemsTitle_en_US`) VALUES
(1, 36, 1, NULL, NULL, NULL),
(2, 36, 2, NULL, NULL, NULL),
(3, 37, 1, NULL, NULL, NULL),
(4, 37, 2, NULL, NULL, NULL),
(5, 37, 3, NULL, NULL, NULL),
(6, 37, 4, NULL, 'Aktuelle Speisepläne', NULL),
(7, 38, 1, NULL, NULL, NULL),
(8, 38, 2, NULL, NULL, NULL),
(9, 62, 1, NULL, NULL, NULL),
(10, 63, 1, NULL, 'Aktuelle Speisepläne', NULL),
(11, 64, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeRedirectorPage`
--

CREATE TABLE `MicrositeRedirectorPage` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeRedirectorPage_Live`
--

CREATE TABLE `MicrositeRedirectorPage_Live` (
  `ID` int(11) NOT NULL,
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MicrositeRedirectorPage_versions`
--

CREATE TABLE `MicrositeRedirectorPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content_en_US` mediumtext,
  `MicrositeLinkItemsTitle` varchar(255) DEFAULT NULL,
  `MicrositeLinkItemsTitle_en_US` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileInfoPage`
--

CREATE TABLE `MobileInfoPage` (
  `ID` int(11) NOT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content_en_US` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileInfoPage`
--

INSERT INTO `MobileInfoPage` (`ID`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES
(28, NULL, NULL, 0),
(55, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileInfoPage_Live`
--

CREATE TABLE `MobileInfoPage_Live` (
  `ID` int(11) NOT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content_en_US` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileInfoPage_Live`
--

INSERT INTO `MobileInfoPage_Live` (`ID`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES
(28, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileInfoPage_versions`
--

CREATE TABLE `MobileInfoPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Title_en_US` varchar(255) DEFAULT NULL,
  `Content_en_US` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileInfoPage_versions`
--

INSERT INTO `MobileInfoPage_versions` (`ID`, `RecordID`, `Version`, `Title_en_US`, `Content_en_US`, `ImageID`) VALUES
(1, 28, 1, NULL, NULL, 0),
(2, 28, 2, NULL, NULL, 0),
(3, 55, 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage`
--

CREATE TABLE `MobileTemplatePage` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL,
  `EnableDishRatings` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowLegend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `CacheLifetime` int(11) NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage`
--

INSERT INTO `MobileTemplatePage` (`ID`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES
(26, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(54, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage_Live`
--

CREATE TABLE `MobileTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL,
  `EnableDishRatings` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowLegend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `CacheLifetime` int(11) NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage_Live`
--

INSERT INTO `MobileTemplatePage_Live` (`ID`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES
(26, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(54, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage_MenuContainers`
--

CREATE TABLE `MobileTemplatePage_MenuContainers` (
  `ID` int(11) NOT NULL,
  `MobileTemplatePageID` int(11) NOT NULL DEFAULT '0',
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage_MenuContainers`
--

INSERT INTO `MobileTemplatePage_MenuContainers` (`ID`, `MobileTemplatePageID`, `MenuContainerID`) VALUES
(1, 26, 11),
(4, 54, 40);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MobileTemplatePage_versions`
--

CREATE TABLE `MobileTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `TemplateName` varchar(255) DEFAULT NULL,
  `GoogleAnalyticsID` varchar(20) DEFAULT NULL,
  `EnableDishRatings` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowOpeningHours` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowLegend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `CacheLifetime` int(11) NOT NULL DEFAULT '0',
  `DisplayVisitorCounterData` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `MobileTemplatePage_versions`
--

INSERT INTO `MobileTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `GoogleAnalyticsID`, `EnableDishRatings`, `ShowOpeningHours`, `ShowLegend`, `CacheLifetime`, `DisplayVisitorCounterData`, `ImportVisitorCounterConfigID`) VALUES
(1, 26, 1, NULL, NULL, 1, 1, 1, 10, 0, 0),
(7, 26, 7, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(8, 26, 8, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(9, 26, 9, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(10, 26, 10, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(11, 26, 11, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(12, 54, 1, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(13, 54, 2, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(14, 54, 3, 'Mobile_WeeklyMenu_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(15, 54, 4, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(16, 26, 12, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(17, 26, 13, 'Mobile_DailyMenu_DishRating_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(18, 26, 14, 'Mobile_DailyMenu_DishRating_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(19, 26, 15, 'Mobile_DailyMenu_DishRating_de_DE', NULL, 1, 1, 1, 0, 0, 0),
(20, 26, 16, 'Mobile_RestaurantOverview_de_DE', NULL, 1, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `MySiteConfigExtensionPermissions`
--

CREATE TABLE `MySiteConfigExtensionPermissions` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('MySiteConfigExtensionPermissions') DEFAULT 'MySiteConfigExtensionPermissions',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `NutritivesMapConfig`
--

CREATE TABLE `NutritivesMapConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('NutritivesMapConfig') DEFAULT 'NutritivesMapConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `NutritivesMapObject`
--

CREATE TABLE `NutritivesMapObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('NutritivesMapObject') DEFAULT 'NutritivesMapObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `ExternalKey` varchar(255) DEFAULT NULL,
  `InternalKey` varchar(255) DEFAULT NULL,
  `NutritivesMapConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Permission`
--

CREATE TABLE `Permission` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Permission') DEFAULT 'Permission',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Permission`
--

INSERT INTO `Permission` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `Arg`, `Type`, `GroupID`) VALUES
(19, 'Permission', '2016-10-17 11:44:25', '2016-10-17 11:44:25', 'ADMIN', 0, 1, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PermissionRole`
--

CREATE TABLE `PermissionRole` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PermissionRole') DEFAULT 'PermissionRole',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PermissionRole`
--

INSERT INTO `PermissionRole` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `OnlyAdminCanApply`) VALUES
(1, 'PermissionRole', '2016-10-17 10:58:13', '2016-10-17 11:41:25', 'Admin Global', 0),
(2, 'PermissionRole', '2016-10-17 10:59:38', '2016-10-17 10:59:38', 'Admin System', 1),
(3, 'PermissionRole', '2016-10-17 11:08:47', '2016-10-17 11:41:46', 'Admin Mandant', 0),
(4, 'PermissionRole', '2016-10-17 11:11:52', '2016-10-17 11:42:09', 'Admin Restaurant', 0),
(5, 'PermissionRole', '2016-10-17 11:38:49', '2016-10-17 11:49:25', 'Editor Restaurant Gerichte Global', 0),
(6, 'PermissionRole', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'Editor Microsite', 0),
(7, 'PermissionRole', '2016-10-17 11:44:00', '2016-11-26 13:20:25', 'Aufgabenplanung \"Import Speiseplan\" Ausführung', 0),
(8, 'PermissionRole', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'Admin Microsite', 0),
(9, 'PermissionRole', '2016-10-17 11:53:57', '2016-10-17 11:57:08', 'Editor Restaurant Gerichte Restaurant', 0),
(10, 'PermissionRole', '2016-11-26 13:21:56', '2016-11-26 13:21:56', 'Aufgabenplanung \"Import Restaurant\" Ausführung', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PermissionRoleCode`
--

CREATE TABLE `PermissionRoleCode` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PermissionRoleCode') DEFAULT 'PermissionRoleCode',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PermissionRoleCode`
--

INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES
(155, 'PermissionRoleCode', '2016-10-17 10:59:38', '2016-10-17 10:59:38', 'ADMIN', 2),
(1356, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 6),
(1357, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 6),
(1358, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CMS_ACCESS_CMSMain', 6),
(1359, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CMS_ACCESS_AssetAdmin', 6),
(1360, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_FILE_ROOT', 6),
(1361, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_FOODCATEGORY', 6),
(1362, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'SITETREE_REORGANISE', 6),
(1363, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITEPAGE', 6),
(1364, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_MICROSITELINKITEM', 6),
(1365, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_MICROSITELINKITEM', 6),
(1366, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITELINKITEM', 6),
(1367, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'DELETE_MICROSITELINKITEM', 6),
(1368, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_MICROSITECONTENTITEM', 6),
(1369, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_MICROSITECONTENTITEM', 6),
(1370, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITECONTENTITEM', 6),
(1371, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'DELETE_MICROSITECONTENTITEM', 6),
(1372, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_MICROSITEREDIRECTORPAGE', 6),
(1373, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 6),
(1374, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 6),
(1375, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 6),
(1376, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 6),
(1377, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 6),
(1378, 'PermissionRoleCode', '2016-10-17 11:41:02', '2016-10-17 11:41:02', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 6),
(1379, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SUBSITESPECIALLABEL', 1),
(1380, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_SUBSITESPECIALLABEL', 1),
(1381, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_SUBSITESPECIALLABEL', 1),
(1382, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_SUBSITESPECIALLABEL', 1),
(1383, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SUBSITEALLERGEN', 1),
(1384, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_SUBSITEALLERGEN', 1),
(1385, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_SUBSITEALLERGEN', 1),
(1386, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_SUBSITEALLERGEN', 1),
(1387, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 1),
(1388, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 1),
(1389, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'TEMPLATEPAGE_EDIT_FOOTER', 1),
(1390, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 1),
(1391, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 1),
(1392, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS', 1),
(1393, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISPLAYTEMPLATEOBJECT', 1),
(1394, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_DISPLAYTEMPLATEOBJECT', 1),
(1395, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_DISPLAYTEMPLATEOBJECT', 1),
(1396, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISPLAYTEMPLATEOBJECT', 1),
(1397, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISPLAYVIDEO', 1),
(1398, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_DISPLAYVIDEO', 1),
(1399, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_DISPLAYVIDEO', 1),
(1400, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISPLAYVIDEO', 1),
(1401, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_CMSMain', 1),
(1402, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_AssetAdmin', 1),
(1403, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_DishRatingAdmin', 1),
(1404, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CMS_ACCESS_InfoMessageTemplateAdmin', 1),
(1405, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_FILE_ROOT', 1),
(1406, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_SPECIALLABELS', 1),
(1407, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_DISHLABELS', 1),
(1408, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_ADDITIVES', 1),
(1409, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SITECONFIG_TAB_ALLERGENS', 1),
(1410, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_FOODCATEGORY', 1),
(1411, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_FOODCATEGORY', 1),
(1412, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_FOODCATEGORY', 1),
(1413, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_FOODCATEGORY', 1),
(1414, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISH', 1),
(1415, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_DISH', 1),
(1416, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_DISH', 1),
(1417, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISH', 1),
(1418, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_DESCRIPTION', 1),
(1419, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_DESCRIPTION', 1),
(1420, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_IMAGE', 1),
(1421, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_IMAGE', 1),
(1422, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELDS_NUTRITIVES', 1),
(1423, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELDS_NUTRITIVES', 1),
(1424, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 1),
(1425, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 1),
(1426, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 1),
(1427, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 1),
(1428, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 1),
(1429, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 1),
(1430, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 1),
(1431, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 1),
(1432, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 1),
(1433, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 1),
(1434, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 1),
(1435, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 1),
(1436, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 1),
(1437, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 1),
(1438, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 1),
(1439, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 1),
(1440, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_INFOTEXT', 1),
(1441, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_INFOTEXT', 1),
(1442, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_FOODCATEGORY', 1),
(1443, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_FOODCATEGORY', 1),
(1444, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELD_PRICES', 1),
(1445, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELD_PRICES', 1),
(1446, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELDS_SIDEDISHES', 1),
(1447, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELDS_SIDEDISHES', 1),
(1448, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 1),
(1449, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 1),
(1450, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_DISHRATING', 1),
(1451, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_DISHRATING', 1),
(1452, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'SITETREE_REORGANISE', 1),
(1453, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'VIEW_SUBSITEDISHLABEL', 1),
(1454, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'EDIT_SUBSITEDISHLABEL', 1),
(1455, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_SUBSITEDISHLABEL', 1),
(1456, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'DELETE_SUBSITEDISHLABEL', 1),
(1457, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'MICROSITECONTAINER_VIEW_FIELD_GOOGLEANALYTICSID', 1),
(1458, 'PermissionRoleCode', '2016-10-17 11:41:24', '2016-10-17 11:41:24', 'CREATE_MICROSITEPAGE', 1),
(1459, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITEPAGE', 1),
(1460, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_MICROSITELINKITEM', 1),
(1461, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_MICROSITELINKITEM', 1),
(1462, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_MICROSITELINKITEM', 1),
(1463, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITELINKITEM', 1),
(1464, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_MICROSITECONTENTITEM', 1),
(1465, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_MICROSITECONTENTITEM', 1),
(1466, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_MICROSITECONTENTITEM', 1),
(1467, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITECONTENTITEM', 1),
(1468, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_MICROSITEREDIRECTORPAGE', 1),
(1469, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_MICROSITEREDIRECTORPAGE', 1),
(1470, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 1),
(1471, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME', 1),
(1472, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_SUBSITE_DISH_PER_RESTAURANT', 1),
(1473, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_SUBSITE_DISH_PER_RESTAURANT', 1),
(1474, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_SUBSITE_DISH_PER_RESTAURANT', 1),
(1475, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_SUBSITE_DISH_PER_RESTAURANT', 1),
(1476, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_RESTAURANT_DISH_PER_RESTAURANT', 1),
(1477, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_RESTAURANT_DISH_PER_RESTAURANT', 1),
(1478, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 1),
(1479, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 1),
(1480, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 1),
(1481, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 1),
(1482, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 1),
(1483, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 1),
(1484, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 1),
(1485, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 1),
(1486, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 1),
(1487, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 1),
(1488, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 1),
(1489, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 1),
(1490, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1),
(1491, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1),
(1492, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1),
(1493, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 1),
(1494, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 1),
(1495, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 1),
(1496, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 1),
(1497, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 1),
(1498, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1),
(1499, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1),
(1500, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1),
(1501, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEMOBILETEMPLATEPAGE', 1),
(1502, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1),
(1503, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1),
(1504, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1),
(1505, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEMICROSITEHOMEPAGE', 1),
(1506, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1),
(1507, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1),
(1508, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1),
(1509, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEMICROSITENEWSPAGE', 1),
(1510, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1),
(1511, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1),
(1512, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1),
(1513, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEDISPLAYTEMPLATEPAGE', 1),
(1514, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1),
(1515, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1),
(1516, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1),
(1517, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_INFOMESSAGETEMPLATEPRINTTEMPLATEPAGE', 1),
(1518, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 1),
(1519, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_DISHES', 1),
(1520, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 1),
(1521, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_TAB_IMPRINT', 1),
(1522, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 1),
(1523, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 1),
(1524, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_DISHVOTINGPAGE', 1),
(1525, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_DISHVOTINGITEM', 1),
(1526, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_DISHVOTINGITEM', 1),
(1527, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_DISHVOTINGITEM', 1),
(1528, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_DISHVOTINGITEM', 1),
(1529, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_RECIPESUBMISSIONITEM', 1),
(1530, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_RECIPESUBMISSIONITEM', 1),
(1531, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_RECIPESUBMISSIONITEM', 1),
(1532, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_RECIPESUBMISSIONITEM', 1),
(1533, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'VIEW_SUBSITEADDITIVE', 1),
(1534, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'EDIT_SUBSITEADDITIVE', 1),
(1535, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'CREATE_SUBSITEADDITIVE', 1),
(1536, 'PermissionRoleCode', '2016-10-17 11:41:25', '2016-10-17 11:41:25', 'DELETE_SUBSITEADDITIVE', 1),
(1537, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITESPECIALLABEL', 3),
(1538, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITESPECIALLABEL', 3),
(1539, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITESPECIALLABEL', 3),
(1540, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITESPECIALLABEL', 3),
(1541, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITEALLERGEN', 3),
(1542, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITEALLERGEN', 3),
(1543, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITEALLERGEN', 3),
(1544, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITEALLERGEN', 3),
(1545, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 3),
(1546, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 3),
(1547, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'TEMPLATEPAGE_EDIT_FOOTER', 3),
(1548, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 3),
(1549, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 3),
(1550, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS', 3),
(1551, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISPLAYTEMPLATEOBJECT', 3),
(1552, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISPLAYTEMPLATEOBJECT', 3),
(1553, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISPLAYTEMPLATEOBJECT', 3),
(1554, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISPLAYTEMPLATEOBJECT', 3),
(1555, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISPLAYVIDEO', 3),
(1556, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISPLAYVIDEO', 3),
(1557, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISPLAYVIDEO', 3),
(1558, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISPLAYVIDEO', 3),
(1559, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_CMSMain', 3),
(1560, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_AssetAdmin', 3),
(1561, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_DishRatingAdmin', 3),
(1562, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CMS_ACCESS_InfoMessageTemplateAdmin', 3),
(1563, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_FILE_ROOT', 3),
(1564, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_SPECIALLABELS', 3),
(1565, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_DISHLABELS', 3),
(1566, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_ADDITIVES', 3),
(1567, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SITECONFIG_TAB_ALLERGENS', 3),
(1568, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_FOODCATEGORY', 3),
(1569, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_FOODCATEGORY', 3),
(1570, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_FOODCATEGORY', 3),
(1571, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_FOODCATEGORY', 3),
(1572, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISH', 3),
(1573, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISH', 3),
(1574, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISH', 3),
(1575, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISH', 3),
(1576, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_DESCRIPTION', 3),
(1577, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_DESCRIPTION', 3),
(1578, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_IMAGE', 3),
(1579, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_IMAGE', 3),
(1580, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELDS_NUTRITIVES', 3),
(1581, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELDS_NUTRITIVES', 3),
(1582, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 3),
(1583, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 3),
(1584, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 3),
(1585, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 3),
(1586, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 3),
(1587, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 3),
(1588, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 3),
(1589, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 3),
(1590, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 3),
(1591, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 3),
(1592, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 3),
(1593, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 3),
(1594, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 3),
(1595, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 3),
(1596, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 3),
(1597, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 3),
(1598, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_INFOTEXT', 3),
(1599, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_INFOTEXT', 3),
(1600, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_FOODCATEGORY', 3),
(1601, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_FOODCATEGORY', 3),
(1602, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELD_PRICES', 3),
(1603, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELD_PRICES', 3),
(1604, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELDS_SIDEDISHES', 3),
(1605, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELDS_SIDEDISHES', 3),
(1606, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 3),
(1607, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 3),
(1608, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISHRATING', 3),
(1609, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISHRATING', 3),
(1610, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'SITETREE_REORGANISE', 3),
(1611, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITEDISHLABEL', 3),
(1612, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITEDISHLABEL', 3),
(1613, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITEDISHLABEL', 3),
(1614, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITEDISHLABEL', 3),
(1615, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITEPAGE', 3),
(1616, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITEPAGE', 3),
(1617, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_MICROSITELINKITEM', 3),
(1618, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_MICROSITELINKITEM', 3),
(1619, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITELINKITEM', 3),
(1620, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITELINKITEM', 3),
(1621, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_MICROSITECONTENTITEM', 3),
(1622, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_MICROSITECONTENTITEM', 3),
(1623, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITECONTENTITEM', 3),
(1624, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITECONTENTITEM', 3),
(1625, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_MICROSITEREDIRECTORPAGE', 3),
(1626, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_MICROSITEREDIRECTORPAGE', 3),
(1627, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 3),
(1628, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME', 3),
(1629, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITE_DISH_PER_RESTAURANT', 3),
(1630, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITE_DISH_PER_RESTAURANT', 3),
(1631, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITE_DISH_PER_RESTAURANT', 3),
(1632, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITE_DISH_PER_RESTAURANT', 3),
(1633, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_RESTAURANT_DISH_PER_RESTAURANT', 3),
(1634, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_RESTAURANT_DISH_PER_RESTAURANT', 3),
(1635, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 3),
(1636, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 3),
(1637, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 3),
(1638, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 3),
(1639, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 3),
(1640, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 3),
(1641, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 3),
(1642, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 3),
(1643, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 3),
(1644, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 3),
(1645, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 3),
(1646, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 3),
(1647, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3),
(1648, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3),
(1649, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3),
(1650, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 3),
(1651, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 3),
(1652, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 3),
(1653, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 3),
(1654, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 3),
(1655, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 3),
(1656, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_DISHES', 3),
(1657, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 3),
(1658, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_TAB_IMPRINT', 3),
(1659, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 3),
(1660, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 3),
(1661, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SITECONFIG', 3),
(1662, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISHVOTINGPAGE', 3),
(1663, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_DISHVOTINGITEM', 3),
(1664, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_DISHVOTINGITEM', 3),
(1665, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_DISHVOTINGITEM', 3),
(1666, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_DISHVOTINGITEM', 3),
(1667, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_RECIPESUBMISSIONPAGE', 3),
(1668, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_RECIPESUBMISSIONITEM', 3),
(1669, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_RECIPESUBMISSIONITEM', 3),
(1670, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_RECIPESUBMISSIONITEM', 3),
(1671, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_RECIPESUBMISSIONITEM', 3),
(1672, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'VIEW_SUBSITEADDITIVE', 3),
(1673, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'EDIT_SUBSITEADDITIVE', 3),
(1674, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'CREATE_SUBSITEADDITIVE', 3),
(1675, 'PermissionRoleCode', '2016-10-17 11:41:46', '2016-10-17 11:41:46', 'DELETE_SUBSITEADDITIVE', 3),
(1676, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 4),
(1677, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 4),
(1678, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'TEMPLATEPAGE_EDIT_FOOTER', 4),
(1679, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 4),
(1680, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 4),
(1681, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISPLAYTEMPLATEPAGE_EDIT_DISPLAYSETTINGS', 4),
(1682, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISPLAYTEMPLATEOBJECT', 4),
(1683, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISPLAYTEMPLATEOBJECT', 4),
(1684, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISPLAYTEMPLATEOBJECT', 4),
(1685, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISPLAYTEMPLATEOBJECT', 4),
(1686, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISPLAYVIDEO', 4),
(1687, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISPLAYVIDEO', 4),
(1688, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISPLAYVIDEO', 4),
(1689, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISPLAYVIDEO', 4),
(1690, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CMS_ACCESS_CMSMain', 4),
(1691, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CMS_ACCESS_AssetAdmin', 4),
(1692, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CMS_ACCESS_DishRatingAdmin', 4),
(1693, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_FILE_ROOT', 4),
(1694, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_FOODCATEGORY', 4),
(1695, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_FOODCATEGORY', 4),
(1696, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_FOODCATEGORY', 4),
(1697, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_FOODCATEGORY', 4),
(1698, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISH', 4),
(1699, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISH', 4),
(1700, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISH', 4),
(1701, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISH', 4),
(1702, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_DESCRIPTION', 4),
(1703, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_DESCRIPTION', 4),
(1704, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_IMAGE', 4),
(1705, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_IMAGE', 4),
(1706, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELDS_NUTRITIVES', 4),
(1707, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELDS_NUTRITIVES', 4),
(1708, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 4),
(1709, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 4),
(1710, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 4),
(1711, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 4),
(1712, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 4),
(1713, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 4),
(1714, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 4),
(1715, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 4),
(1716, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 4),
(1717, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 4),
(1718, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 4),
(1719, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 4),
(1720, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 4),
(1721, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 4),
(1722, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 4),
(1723, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 4),
(1724, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_INFOTEXT', 4),
(1725, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_INFOTEXT', 4),
(1726, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_FOODCATEGORY', 4),
(1727, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_FOODCATEGORY', 4),
(1728, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELD_PRICES', 4),
(1729, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELD_PRICES', 4),
(1730, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELDS_SIDEDISHES', 4),
(1731, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELDS_SIDEDISHES', 4),
(1732, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 4),
(1733, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 4),
(1734, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISHRATING', 4),
(1735, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISHRATING', 4),
(1736, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'SITETREE_REORGANISE', 4),
(1737, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITEPAGE', 4),
(1738, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITEPAGE', 4),
(1739, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_MICROSITELINKITEM', 4),
(1740, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_MICROSITELINKITEM', 4),
(1741, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITELINKITEM', 4),
(1742, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITELINKITEM', 4),
(1743, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_MICROSITECONTENTITEM', 4),
(1744, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_MICROSITECONTENTITEM', 4),
(1745, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITECONTENTITEM', 4),
(1746, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITECONTENTITEM', 4),
(1747, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_MICROSITEREDIRECTORPAGE', 4),
(1748, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_MICROSITEREDIRECTORPAGE', 4),
(1749, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 4),
(1750, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'MOBILETEMPLATEPAGE_EDIT_CACHELIFETIME', 4),
(1751, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_SUBSITE_DISH_PER_RESTAURANT', 4),
(1752, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_SUBSITE_DISH_PER_RESTAURANT', 4),
(1753, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_SUBSITE_DISH_PER_RESTAURANT', 4),
(1754, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_SUBSITE_DISH_PER_RESTAURANT', 4),
(1755, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_RESTAURANT_DISH_PER_RESTAURANT', 4),
(1756, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_RESTAURANT_DISH_PER_RESTAURANT', 4),
(1757, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 4),
(1758, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 4),
(1759, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 4),
(1760, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 4),
(1761, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 4),
(1762, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 4),
(1763, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 4),
(1764, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 4),
(1765, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 4),
(1766, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 4),
(1767, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 4),
(1768, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 4),
(1769, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4),
(1770, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4),
(1771, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4),
(1772, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 4),
(1773, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 4),
(1774, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 4),
(1775, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 4),
(1776, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 4),
(1777, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 4),
(1778, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_DISHES', 4),
(1779, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 4),
(1780, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_TAB_IMPRINT', 4),
(1781, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 4),
(1782, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 4),
(1783, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISHVOTINGPAGE', 4),
(1784, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_DISHVOTINGITEM', 4),
(1785, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_DISHVOTINGITEM', 4),
(1786, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_DISHVOTINGITEM', 4),
(1787, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_DISHVOTINGITEM', 4),
(1788, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_RECIPESUBMISSIONPAGE', 4),
(1789, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'VIEW_RECIPESUBMISSIONITEM', 4),
(1790, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'EDIT_RECIPESUBMISSIONITEM', 4),
(1791, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'CREATE_RECIPESUBMISSIONITEM', 4),
(1792, 'PermissionRoleCode', '2016-10-17 11:42:09', '2016-10-17 11:42:09', 'DELETE_RECIPESUBMISSIONITEM', 4),
(1804, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 8),
(1805, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 8),
(1806, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CMS_ACCESS_CMSMain', 8),
(1807, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CMS_ACCESS_AssetAdmin', 8),
(1808, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_FILE_ROOT', 8),
(1809, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_FOODCATEGORY', 8),
(1810, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_DISH', 8),
(1811, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_DISH', 8),
(1812, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_DISH', 8),
(1813, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_DESCRIPTION', 8),
(1814, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_DESCRIPTION', 8),
(1815, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_IMAGE', 8),
(1816, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_IMAGE', 8),
(1817, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELDS_NUTRITIVES', 8),
(1818, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELDS_NUTRITIVES', 8),
(1819, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 8),
(1820, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 8),
(1821, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 8),
(1822, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 8);
INSERT INTO `PermissionRoleCode` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `RoleID`) VALUES
(1823, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 8),
(1824, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 8),
(1825, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 8),
(1826, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 8),
(1827, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 8),
(1828, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 8),
(1829, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 8),
(1830, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 8),
(1831, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 8),
(1832, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 8),
(1833, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 8),
(1834, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 8),
(1835, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_INFOTEXT', 8),
(1836, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_INFOTEXT', 8),
(1837, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_FOODCATEGORY', 8),
(1838, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_FOODCATEGORY', 8),
(1839, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELD_PRICES', 8),
(1840, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELD_PRICES', 8),
(1841, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELDS_SIDEDISHES', 8),
(1842, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELDS_SIDEDISHES', 8),
(1843, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 8),
(1844, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 8),
(1845, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'SITETREE_REORGANISE', 8),
(1846, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITEPAGE', 8),
(1847, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_MICROSITEPAGE', 8),
(1848, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_MICROSITELINKITEM', 8),
(1849, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_MICROSITELINKITEM', 8),
(1850, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITELINKITEM', 8),
(1851, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_MICROSITELINKITEM', 8),
(1852, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_MICROSITECONTENTITEM', 8),
(1853, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_MICROSITECONTENTITEM', 8),
(1854, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITECONTENTITEM', 8),
(1855, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_MICROSITEREDIRECTORPAGE', 8),
(1856, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_MICROSITEREDIRECTORPAGE', 8),
(1857, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 8),
(1858, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 8),
(1859, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 8),
(1860, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 8),
(1861, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 8),
(1862, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 8),
(1863, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 8),
(1864, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 8),
(1865, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 8),
(1866, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 8),
(1867, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 8),
(1868, 'PermissionRoleCode', '2016-10-17 11:49:01', '2016-10-17 11:49:01', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 8),
(1869, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 5),
(1870, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 5),
(1871, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'TEMPLATEPAGE_EDIT_FOOTER', 5),
(1872, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 5),
(1873, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 5),
(1874, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_DISPLAYTEMPLATEOBJECT', 5),
(1875, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_DISPLAYTEMPLATEOBJECT', 5),
(1876, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_DISPLAYVIDEO', 5),
(1877, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_DISPLAYVIDEO', 5),
(1878, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CREATE_DISPLAYVIDEO', 5),
(1879, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DELETE_DISPLAYVIDEO', 5),
(1880, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CMS_ACCESS_CMSMain', 5),
(1881, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CMS_ACCESS_AssetAdmin', 5),
(1882, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CMS_ACCESS_DishRatingAdmin', 5),
(1883, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_FILE_ROOT', 5),
(1884, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_FOODCATEGORY', 5),
(1885, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_FOODCATEGORY', 5),
(1886, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CREATE_FOODCATEGORY', 5),
(1887, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'VIEW_DISH', 5),
(1888, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'EDIT_DISH', 5),
(1889, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'CREATE_DISH', 5),
(1890, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELD_DESCRIPTION', 5),
(1891, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELD_DESCRIPTION', 5),
(1892, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELD_IMAGE', 5),
(1893, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELD_IMAGE', 5),
(1894, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELDS_NUTRITIVES', 5),
(1895, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELDS_NUTRITIVES', 5),
(1896, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 5),
(1897, 'PermissionRoleCode', '2016-10-17 11:49:24', '2016-10-17 11:49:24', 'DISH_EDIT_FIELD_GLOBALSPECIALLABELS', 5),
(1898, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 5),
(1899, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITESPECIALLABELS', 5),
(1900, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 5),
(1901, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_GLOBALDISHLABELS', 5),
(1902, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 5),
(1903, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITEDISHLABELS', 5),
(1904, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 5),
(1905, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_GLOBALADDITIVES', 5),
(1906, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 5),
(1907, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITEADDITIVES', 5),
(1908, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 5),
(1909, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_GLOBALALLERGENS', 5),
(1910, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 5),
(1911, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_SUBSITEALLERGENS', 5),
(1912, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_INFOTEXT', 5),
(1913, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_INFOTEXT', 5),
(1914, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_FOODCATEGORY', 5),
(1915, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_FOODCATEGORY', 5),
(1916, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELD_PRICES', 5),
(1917, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELD_PRICES', 5),
(1918, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELDS_SIDEDISHES', 5),
(1919, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELDS_SIDEDISHES', 5),
(1920, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 5),
(1921, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 5),
(1922, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_DISHRATING', 5),
(1923, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'SITETREE_REORGANISE', 5),
(1924, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITEPAGE', 5),
(1925, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITEPAGE', 5),
(1926, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_MICROSITELINKITEM', 5),
(1927, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_MICROSITELINKITEM', 5),
(1928, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITELINKITEM', 5),
(1929, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITELINKITEM', 5),
(1930, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_MICROSITECONTENTITEM', 5),
(1931, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_MICROSITECONTENTITEM', 5),
(1932, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITECONTENTITEM', 5),
(1933, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITECONTENTITEM', 5),
(1934, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_MICROSITEREDIRECTORPAGE', 5),
(1935, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_MICROSITEREDIRECTORPAGE', 5),
(1936, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 5),
(1937, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 5),
(1938, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 5),
(1939, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 5),
(1940, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 5),
(1941, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 5),
(1942, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 5),
(1943, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 5),
(1944, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 5),
(1945, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 5),
(1946, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 5),
(1947, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 5),
(1948, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 5),
(1949, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5),
(1950, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5),
(1951, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5),
(1952, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 5),
(1953, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 5),
(1954, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 5),
(1955, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 5),
(1956, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 5),
(1957, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 5),
(1958, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 5),
(1959, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 5),
(1960, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 5),
(1961, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_DAILYMENUPAGE', 5),
(1962, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_DISHVOTINGITEM', 5),
(1963, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_DISHVOTINGITEM', 5),
(1964, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_DISHVOTINGITEM', 5),
(1965, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_DISHVOTINGITEM', 5),
(1966, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_RECIPESUBMISSIONPAGE', 5),
(1967, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'VIEW_RECIPESUBMISSIONITEM', 5),
(1968, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'EDIT_RECIPESUBMISSIONITEM', 5),
(1969, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'CREATE_RECIPESUBMISSIONITEM', 5),
(1970, 'PermissionRoleCode', '2016-10-17 11:49:25', '2016-10-17 11:49:25', 'DELETE_RECIPESUBMISSIONITEM', 5),
(2059, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'TEMPLATEPAGE_EDIT_DATETIMEPERIOD', 9),
(2060, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'TEMPLATEPAGE_EDIT_FOODCATEGORIES', 9),
(2061, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'TEMPLATEPAGE_EDIT_FOOTER', 9),
(2062, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISPLAYTEMPLATEPAGE_EDIT_TIMERSETTINGS', 9),
(2063, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISPLAYTEMPLATEPAGE_EDIT_SHOWINFOMESSAGESONLY', 9),
(2064, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISPLAYTEMPLATEOBJECT', 9),
(2065, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_DISPLAYTEMPLATEOBJECT', 9),
(2066, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISPLAYVIDEO', 9),
(2067, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_DISPLAYVIDEO', 9),
(2068, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_DISPLAYVIDEO', 9),
(2069, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_DISPLAYVIDEO', 9),
(2070, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CMS_ACCESS_CMSMain', 9),
(2071, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CMS_ACCESS_AssetAdmin', 9),
(2072, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CMS_ACCESS_DishRatingAdmin', 9),
(2073, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_FILE_ROOT', 9),
(2074, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_FOODCATEGORY', 9),
(2075, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_FOODCATEGORY', 9),
(2076, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_FOODCATEGORY', 9),
(2077, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISH', 9),
(2078, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_DESCRIPTION', 9),
(2079, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_IMAGE', 9),
(2080, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELDS_NUTRITIVES', 9),
(2081, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALSPECIALLABELS', 9),
(2082, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITESPECIALLABELS', 9),
(2083, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALDISHLABELS', 9),
(2084, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITEDISHLABELS', 9),
(2085, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALADDITIVES', 9),
(2086, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITEADDITIVES', 9),
(2087, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_GLOBALALLERGENS', 9),
(2088, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_SUBSITEALLERGENS', 9),
(2089, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_INFOTEXT', 9),
(2090, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_FOODCATEGORY', 9),
(2091, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELD_FOODCATEGORY', 9),
(2092, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELD_PRICES', 9),
(2093, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELD_PRICES', 9),
(2094, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELDS_SIDEDISHES', 9),
(2095, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELDS_SIDEDISHES', 9),
(2096, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_VIEW_FIELDS_SHOWONTEMPLATEPAGES', 9),
(2097, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DISH_EDIT_FIELDS_SHOWONTEMPLATEPAGES', 9),
(2098, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISHRATING', 9),
(2099, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'SITETREE_REORGANISE', 9),
(2100, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITEPAGE', 9),
(2101, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITEPAGE', 9),
(2102, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_MICROSITELINKITEM', 9),
(2103, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_MICROSITELINKITEM', 9),
(2104, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITELINKITEM', 9),
(2105, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITELINKITEM', 9),
(2106, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_MICROSITECONTENTITEM', 9),
(2107, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_MICROSITECONTENTITEM', 9),
(2108, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITECONTENTITEM', 9),
(2109, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITECONTENTITEM', 9),
(2110, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_MICROSITEREDIRECTORPAGE', 9),
(2111, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_MICROSITEREDIRECTORPAGE', 9),
(2112, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'MOBILETEMPLATEPAGE_EDIT_ENABLE_FUNCTIONALITY', 9),
(2113, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEMOBILETEMPLATEPAGE', 9),
(2114, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEMOBILETEMPLATEPAGE', 9),
(2115, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEMOBILETEMPLATEPAGE', 9),
(2116, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEMOBILETEMPLATEPAGE', 9),
(2117, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEMICROSITEHOMEPAGE', 9),
(2118, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEMICROSITEHOMEPAGE', 9),
(2119, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEMICROSITEHOMEPAGE', 9),
(2120, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEMICROSITEHOMEPAGE', 9),
(2121, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEMICROSITENEWSPAGE', 9),
(2122, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEMICROSITENEWSPAGE', 9),
(2123, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEMICROSITENEWSPAGE', 9),
(2124, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEMICROSITENEWSPAGE', 9),
(2125, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9),
(2126, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9),
(2127, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9),
(2128, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEDISPLAYTEMPLATEPAGE', 9),
(2129, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_INFOMESSAGEPRINTTEMPLATEPAGE', 9),
(2130, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_INFOMESSAGEPRINTTEMPLATEPAGE', 9),
(2131, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_INFOMESSAGEPRINTTEMPLATEPAGE', 9),
(2132, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_INFOMESSAGEPRINTTEMPLATEPAGE', 9),
(2133, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_VIEW_TAB_FOODCATEGORIES', 9),
(2134, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_VIEW_TAB_OPENINGHOURS', 9),
(2135, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_VIEW_CONTACTDATA', 9),
(2136, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'RESTAURANTPAGE_EDIT_CONTACTDATA', 9),
(2137, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_DISHVOTINGPAGE', 9),
(2138, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_DISHVOTINGITEM', 9),
(2139, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_DISHVOTINGITEM', 9),
(2140, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_DISHVOTINGITEM', 9),
(2141, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_DISHVOTINGITEM', 9),
(2142, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_RECIPESUBMISSIONPAGE', 9),
(2143, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'VIEW_RECIPESUBMISSIONITEM', 9),
(2144, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'EDIT_RECIPESUBMISSIONITEM', 9),
(2145, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'CREATE_RECIPESUBMISSIONITEM', 9),
(2146, 'PermissionRoleCode', '2016-10-17 11:57:08', '2016-10-17 11:57:08', 'DELETE_RECIPESUBMISSIONITEM', 9),
(2147, 'PermissionRoleCode', '2016-11-26 13:20:25', '2016-11-26 13:20:25', 'VIEW_IMPORTMENUXMLCONFIG', 7),
(2148, 'PermissionRoleCode', '2016-11-26 13:20:25', '2016-11-26 13:20:25', 'RUN_SINGLE_IMPORTMENUXMLTASK', 7),
(2149, 'PermissionRoleCode', '2016-11-26 13:20:25', '2016-11-26 13:20:25', 'CMS_ACCESS_ProcessTaskAdmin', 7),
(2150, 'PermissionRoleCode', '2016-11-26 13:21:56', '2016-11-26 13:21:56', 'VIEW_IMPORTRESTAURANTCONFIG', 10),
(2151, 'PermissionRoleCode', '2016-11-26 13:21:56', '2016-11-26 13:21:56', 'RUN_SINGLE_IMPORTRESTAURANTTASK', 10),
(2152, 'PermissionRoleCode', '2016-11-26 13:21:56', '2016-11-26 13:21:56', 'CMS_ACCESS_ProcessTaskAdmin', 10);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintHtmlTemplatePage`
--

CREATE TABLE `PrintHtmlTemplatePage` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `HeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintHtmlTemplatePage`
--

INSERT INTO `PrintHtmlTemplatePage` (`ID`, `TemplateName`, `DocumentWidth`, `DocumentHeight`, `SizeUnit`, `HeaderImageID`) VALUES
(13, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(14, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(17, 'Print_Category_DinA5', 297, 210, 'mm', 0),
(18, 'Print_DailyMenu_DinA7', 210, 297, 'mm', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintHtmlTemplatePage_Live`
--

CREATE TABLE `PrintHtmlTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `HeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintHtmlTemplatePage_Live`
--

INSERT INTO `PrintHtmlTemplatePage_Live` (`ID`, `TemplateName`, `DocumentWidth`, `DocumentHeight`, `SizeUnit`, `HeaderImageID`) VALUES
(13, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(14, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(17, 'Print_Category_DinA5', 297, 210, 'mm', 0),
(18, 'Print_DailyMenu_DinA7', 210, 297, 'mm', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintHtmlTemplatePage_versions`
--

CREATE TABLE `PrintHtmlTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `TemplateName` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `HeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintHtmlTemplatePage_versions`
--

INSERT INTO `PrintHtmlTemplatePage_versions` (`ID`, `RecordID`, `Version`, `TemplateName`, `DocumentWidth`, `DocumentHeight`, `SizeUnit`, `HeaderImageID`) VALUES
(3, 14, 5, NULL, 0, 0, 'mm', 0),
(4, 14, 6, NULL, 297, 210, 'mm', 0),
(7, 17, 5, NULL, 297, 210, 'mm', 0),
(8, 18, 4, NULL, 0, 0, 'mm', 0),
(9, 18, 5, NULL, 210, 297, 'mm', 0),
(10, 13, 8, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(11, 13, 9, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(12, 17, 6, 'Print_DailyMenu_DinA5', 297, 210, 'mm', 0),
(13, 17, 7, 'Screen_DailyMenu', 297, 210, 'mm', 0),
(14, 17, 8, 'Screen_DailyMenu', 297, 210, 'mm', 0),
(15, 17, 9, 'Print_DailyMenu_DinA5', 297, 210, 'mm', 0),
(16, 18, 6, 'Print_DailyMenu_DinA7', 210, 297, 'mm', 0),
(17, 18, 7, 'Print_DailyMenu_DinA7', 210, 297, 'mm', 0),
(18, 13, 10, 'Screen_WeeklyMenu_Table_Landscape', 297, 210, 'mm', 0),
(19, 13, 11, 'Screen_WeeklyMenu_Table_Separate_Lang_Tables_Landscape', 297, 210, 'mm', 0),
(20, 13, 12, 'Screen_WeeklyMenu_Table_Landscape', 297, 210, 'mm', 0),
(21, 13, 13, 'Screen_WeeklyMenu_Table_Landscape', 297, 210, 'mm', 0),
(22, 13, 14, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(23, 13, 15, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(24, 13, 16, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(25, 13, 17, 'Print_WeeklyMenu_DinA4_Landscape', 297, 210, 'mm', 0),
(26, 17, 10, 'Print_DailyMenu_DinA5', 295, 209, 'mm', 0),
(27, 17, 11, 'Print_DailyMenu_DinA5', 297, 210, 'mm', 0),
(28, 14, 7, 'Print_WeeklyMenu_DinA4_Landscape_EN', 297, 210, 'mm', 0),
(29, 14, 8, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(30, 14, 9, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(31, 14, 10, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(32, 14, 11, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(33, 14, 12, 'Print_WeeklyMenu_DinA4_Landscape_en_US', 297, 210, 'mm', 0),
(34, 17, 12, 'Print_Category_DinA5', 297, 210, 'mm', 0),
(35, 17, 13, 'Print_Category_DinA5', 297, 209, 'mm', 0),
(36, 17, 14, 'Print_Category_DinA5', 297, 210, 'mm', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintManagerConfig`
--

CREATE TABLE `PrintManagerConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PrintManagerConfig') DEFAULT 'PrintManagerConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Headline` mediumtext,
  `Infotext` mediumtext,
  `FootNote` mediumtext,
  `AuthKey` varchar(25) DEFAULT NULL,
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0',
  `PrintManagerTemplateID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintManagerConfig_Dishes`
--

CREATE TABLE `PrintManagerConfig_Dishes` (
  `ID` int(11) NOT NULL,
  `PrintManagerConfigID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SortOrder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintManagerTemplate`
--

CREATE TABLE `PrintManagerTemplate` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('PrintManagerTemplate') DEFAULT 'PrintManagerTemplate',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `DocumentWidth` int(11) NOT NULL DEFAULT '0',
  `DocumentHeight` int(11) NOT NULL DEFAULT '0',
  `SizeUnit` enum('mm','px') DEFAULT 'mm',
  `Sort` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintTemplatePage`
--

CREATE TABLE `PrintTemplatePage` (
  `ID` int(11) NOT NULL,
  `ControllerName` varchar(255) DEFAULT NULL,
  `WeeklyTableHeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintTemplatePage`
--

INSERT INTO `PrintTemplatePage` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES
(13, 'print_weekly_table_detail_de_de', 0),
(14, 'print_weekly_table_detail_en_us', 0),
(17, 'print_category_dina5', 0),
(18, 'print_category_dina7', 0),
(19, 'print_temperature_table', 0),
(20, 'print_production_table', 0),
(42, 'print_weekly_table_detail_de_de', 0),
(43, 'print_weekly_table_detail_en_us', 0),
(44, 'print_category_dina4_portrait', 0),
(45, 'print_category_dina4_landscape', 0),
(46, 'print_category_dina5', 0),
(47, 'print_category_dina7', 0),
(48, 'print_temperature_table', 0),
(49, 'print_production_table', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintTemplatePage_Live`
--

CREATE TABLE `PrintTemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `ControllerName` varchar(255) DEFAULT NULL,
  `WeeklyTableHeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintTemplatePage_Live`
--

INSERT INTO `PrintTemplatePage_Live` (`ID`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES
(13, 'print_weekly_table_detail_de_de', 0),
(14, 'print_weekly_table_detail_en_us', 0),
(17, 'print_category_dina5', 0),
(18, 'print_category_dina7', 0),
(19, 'print_temperature_table', 0),
(20, 'print_production_table', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PrintTemplatePage_versions`
--

CREATE TABLE `PrintTemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ControllerName` varchar(255) DEFAULT NULL,
  `WeeklyTableHeaderImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `PrintTemplatePage_versions`
--

INSERT INTO `PrintTemplatePage_versions` (`ID`, `RecordID`, `Version`, `ControllerName`, `WeeklyTableHeaderImageID`) VALUES
(7, 15, 1, NULL, 0),
(8, 15, 2, 'page_15', 0),
(9, 15, 3, 'print_category_dina4_portrait', 0),
(10, 16, 1, NULL, 0),
(11, 16, 2, 'page_16', 0),
(12, 16, 3, 'print_category_dina4_landscape', 0),
(19, 19, 1, NULL, 0),
(20, 19, 2, 'page_19', 0),
(21, 19, 3, 'print_temperature_table', 0),
(24, 19, 4, 'print_temperature_table', 0),
(25, 20, 1, NULL, 0),
(26, 20, 2, 'page_20', 0),
(27, 20, 3, 'print_production_table', 0),
(28, 42, 1, 'print_weekly_table_detail_de_de', 0),
(29, 43, 1, 'print_weekly_table_detail_en_us', 0),
(30, 44, 1, 'print_category_dina4_portrait', 0),
(31, 45, 1, 'print_category_dina4_landscape', 0),
(32, 46, 1, 'print_category_dina5', 0),
(33, 47, 1, 'print_category_dina7', 0),
(34, 48, 1, 'print_temperature_table', 0),
(35, 49, 1, 'print_production_table', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ProcessTaskObject`
--

CREATE TABLE `ProcessTaskObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('ProcessTaskObject','ExportConfig','ExportDisplayMenuXmlConfig','ExportMenuHtmlConfig','ExportMenuXmlConfig','ImportConfig','ImportMenuXmlConfig','ImportRestaurantConfig','ImportVisitorCounterConfig') DEFAULT 'ProcessTaskObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Status` enum('Running','Error','Finished') DEFAULT 'Finished',
  `LastStarted` datetime DEFAULT NULL,
  `LastRun` datetime DEFAULT NULL,
  `Timeout` int(11) NOT NULL DEFAULT '0',
  `Schedule` varchar(255) DEFAULT NULL,
  `Published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionItem`
--

CREATE TABLE `RecipeSubmissionItem` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('RecipeSubmissionItem') DEFAULT 'RecipeSubmissionItem',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Recipe` mediumtext,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `RecipeSubmissionPageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionPage`
--

CREATE TABLE `RecipeSubmissionPage` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecipeSubmissionPage`
--

INSERT INTO `RecipeSubmissionPage` (`ID`, `VotingDate`, `Publish`) VALUES
(32, '2016-10-28', 1),
(59, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionPage_Live`
--

CREATE TABLE `RecipeSubmissionPage_Live` (
  `ID` int(11) NOT NULL,
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecipeSubmissionPage_Live`
--

INSERT INTO `RecipeSubmissionPage_Live` (`ID`, `VotingDate`, `Publish`) VALUES
(32, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecipeSubmissionPage_versions`
--

CREATE TABLE `RecipeSubmissionPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VotingDate` date DEFAULT NULL,
  `Publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecipeSubmissionPage_versions`
--

INSERT INTO `RecipeSubmissionPage_versions` (`ID`, `RecordID`, `Version`, `VotingDate`, `Publish`) VALUES
(1, 32, 1, NULL, 0),
(2, 32, 2, NULL, 0),
(3, 32, 3, '2016-10-27', 0),
(4, 32, 4, '2016-10-27', 1),
(5, 32, 5, '2016-10-28', 1),
(6, 59, 1, '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RecurringDayOfWeek`
--

CREATE TABLE `RecurringDayOfWeek` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('RecurringDayOfWeek') DEFAULT 'RecurringDayOfWeek',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Value` int(11) NOT NULL DEFAULT '0',
  `Skey` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RecurringDayOfWeek`
--

INSERT INTO `RecurringDayOfWeek` (`ID`, `ClassName`, `Created`, `LastEdited`, `Value`, `Skey`) VALUES
(1, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 0, 'So'),
(2, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 1, 'Mo'),
(3, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 2, 'Di'),
(4, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 3, 'Mi'),
(5, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 4, 'Do'),
(6, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 5, 'Fr'),
(7, 'RecurringDayOfWeek', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 6, 'Sa');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RedirectorPage`
--

CREATE TABLE `RedirectorPage` (
  `ID` int(11) NOT NULL,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RedirectorPage`
--

INSERT INTO `RedirectorPage` (`ID`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES
(6, 'Internal', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RedirectorPage_Live`
--

CREATE TABLE `RedirectorPage_Live` (
  `ID` int(11) NOT NULL,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RedirectorPage_Live`
--

INSERT INTO `RedirectorPage_Live` (`ID`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES
(6, 'Internal', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RedirectorPage_versions`
--

CREATE TABLE `RedirectorPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RedirectorPage_versions`
--

INSERT INTO `RedirectorPage_versions` (`ID`, `RecordID`, `Version`, `RedirectionType`, `ExternalURL`, `LinkToID`) VALUES
(1, 6, 1, 'Internal', NULL, 0),
(2, 6, 2, 'Internal', NULL, 0),
(3, 6, 3, 'Internal', NULL, 0),
(4, 6, 4, 'Internal', NULL, 7),
(5, 6, 5, 'Internal', NULL, 7),
(6, 6, 6, 'Internal', NULL, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantDish`
--

CREATE TABLE `RestaurantDish` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('RestaurantDish') DEFAULT 'RestaurantDish',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Price` float NOT NULL DEFAULT '0',
  `PriceExtern` float NOT NULL DEFAULT '0',
  `PriceSmall` float NOT NULL DEFAULT '0',
  `PriceExternSmall` float NOT NULL DEFAULT '0',
  `ShowOnDisplayTemplatePage` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `ShowOnPrintTemplatePage` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `ShowOnMobileTemplatePage` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `RestaurantPageID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0',
  `DishID` int(11) NOT NULL DEFAULT '0',
  `SideDish1ID` int(11) NOT NULL DEFAULT '0',
  `SideDish2ID` int(11) NOT NULL DEFAULT '0',
  `SideDish3ID` int(11) NOT NULL DEFAULT '0',
  `SideDish4ID` int(11) NOT NULL DEFAULT '0',
  `SideDish5ID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantDish`
--

INSERT INTO `RestaurantDish` (`ID`, `ClassName`, `Created`, `LastEdited`, `Price`, `PriceExtern`, `PriceSmall`, `PriceExternSmall`, `ShowOnDisplayTemplatePage`, `ShowOnPrintTemplatePage`, `ShowOnMobileTemplatePage`, `RestaurantPageID`, `FoodCategoryID`, `DishID`, `SideDish1ID`, `SideDish2ID`, `SideDish3ID`, `SideDish4ID`, `SideDish5ID`) VALUES
(1, 'RestaurantDish', '2016-10-27 11:43:03', '2017-02-07 11:27:37', 0.6, 1.4, 0, 0, 1, 1, 1, 7, 1, 1, 0, 0, 0, 0, 0),
(2, 'RestaurantDish', '2016-10-27 11:43:29', '2017-02-27 15:03:18', 5.6, 7.2, 0, 0, 1, 1, 1, 7, 2, 2, 0, 0, 0, 0, 0),
(3, 'RestaurantDish', '2016-10-27 11:44:43', '2017-02-07 17:30:26', 3.2, 5.1, 0, 0, 1, 1, 1, 7, 4, 3, 9, 11, 0, 0, 0),
(4, 'RestaurantDish', '2016-10-27 11:46:34', '2017-02-27 16:44:05', 2.7, 3.9, 0, 0, 1, 1, 1, 7, 5, 4, 11, 7, 0, 0, 0),
(5, 'RestaurantDish', '2016-10-27 11:49:05', '2017-02-07 11:50:27', 4.5, 7.2, 0, 0, 1, 1, 1, 7, 6, 5, 0, 0, 0, 0, 0),
(6, 'RestaurantDish', '2016-10-27 11:49:53', '2017-02-01 15:52:14', 4.2, 6.4, 0, 0, 1, 1, 1, 7, 3, 6, 7, 0, 0, 0, 0),
(7, 'RestaurantDish', '2016-10-27 11:50:37', '2017-02-27 12:25:15', 1.6, 0, 0, 0, 1, 1, 1, 7, 7, 7, 0, 0, 0, 0, 0),
(8, 'RestaurantDish', '2016-10-27 11:51:04', '2016-10-27 11:51:04', 1.7, 0, 0, 0, 1, 1, 1, 7, 7, 8, 0, 0, 0, 0, 0),
(9, 'RestaurantDish', '2016-10-27 17:31:25', '2017-02-07 11:30:43', 0.6, 0.6, 0, 0, 1, 1, 1, 7, 7, 9, 0, 0, 0, 0, 0),
(10, 'RestaurantDish', '2016-10-27 17:32:11', '2016-10-27 17:32:11', 1.1, 1.8, 0, 0, 1, 1, 1, 7, 9, 10, 0, 0, 0, 0, 0),
(11, 'RestaurantDish', '2017-02-06 18:04:09', '2017-02-06 18:04:17', 0, 0, 0, 0, 1, 1, 1, 39, 13, 3, 0, 0, 0, 0, 0),
(12, 'RestaurantDish', '2017-02-07 11:29:51', '2017-02-07 17:19:49', 0.34, 0.72, 0, 0, 1, 1, 1, 7, 1, 11, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantPage`
--

CREATE TABLE `RestaurantPage` (
  `ID` int(11) NOT NULL,
  `Lang_en_US` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `OpeningHours` mediumtext,
  `Imprint` mediumtext,
  `ContactCompanyName` mediumtext,
  `ContactPersonName` varchar(50) DEFAULT NULL,
  `ContactStreetAddress` mediumtext,
  `ContactPhone` varchar(50) DEFAULT NULL,
  `ContactFax` varchar(50) DEFAULT NULL,
  `ContactEmail` varchar(50) DEFAULT NULL,
  `ContactCopyrightCompany` varchar(255) DEFAULT NULL,
  `RestaurantLogoID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantPage`
--

INSERT INTO `RestaurantPage` (`ID`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES
(7, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href=\"http://www.dubbelspaeth.de\" target=\"_blank\">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a> bzw. <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0),
(39, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href=\"http://www.dubbelspaeth.de\" target=\"_blank\">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a> bzw. <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantPage_Live`
--

CREATE TABLE `RestaurantPage_Live` (
  `ID` int(11) NOT NULL,
  `Lang_en_US` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `OpeningHours` mediumtext,
  `Imprint` mediumtext,
  `ContactCompanyName` mediumtext,
  `ContactPersonName` varchar(50) DEFAULT NULL,
  `ContactStreetAddress` mediumtext,
  `ContactPhone` varchar(50) DEFAULT NULL,
  `ContactFax` varchar(50) DEFAULT NULL,
  `ContactEmail` varchar(50) DEFAULT NULL,
  `ContactCopyrightCompany` varchar(255) DEFAULT NULL,
  `RestaurantLogoID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantPage_Live`
--

INSERT INTO `RestaurantPage_Live` (`ID`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES
(7, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href=\"http://www.dubbelspaeth.de\" target=\"_blank\">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a> bzw. <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0),
(39, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href=\"http://www.dubbelspaeth.de\" target=\"_blank\">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a> bzw. <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `RestaurantPage_versions`
--

CREATE TABLE `RestaurantPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Lang_en_US` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `OpeningHours` mediumtext,
  `Imprint` mediumtext,
  `ContactCompanyName` mediumtext,
  `ContactPersonName` varchar(50) DEFAULT NULL,
  `ContactStreetAddress` mediumtext,
  `ContactPhone` varchar(50) DEFAULT NULL,
  `ContactFax` varchar(50) DEFAULT NULL,
  `ContactEmail` varchar(50) DEFAULT NULL,
  `ContactCopyrightCompany` varchar(255) DEFAULT NULL,
  `RestaurantLogoID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `RestaurantPage_versions`
--

INSERT INTO `RestaurantPage_versions` (`ID`, `RecordID`, `Version`, `Lang_en_US`, `OpeningHours`, `Imprint`, `ContactCompanyName`, `ContactPersonName`, `ContactStreetAddress`, `ContactPhone`, `ContactFax`, `ContactEmail`, `ContactCopyrightCompany`, `RestaurantLogoID`) VALUES
(1, 7, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 7, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 7, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 7, 4, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, 7, 5, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', NULL, 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0),
(6, 7, 6, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href=\"http://www.dubbelspaeth.de\" target=\"_blank\">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a> bzw. <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0),
(7, 39, 1, 1, 'Montag bis Freitag\r\n11:30 - 13:30 Uhr', '<h2>Impressum</h2>\n<h3>Herausgeber</h3>\n<p>EUREST DEUTSCHLAND GmbH<br>Helfmann-Park 2<br>65760 Eschborn</p>\n<h3>Geschäftsführung</h3>\n<p>Jürgen Thamm, Vorsitzender der Geschäftsführung<br>Rainer Baumgärtner, Geschäftsführer Finanzen<br>Carsten Bick, Geschäftsführer Einkauf<br>Karen Hoyndorf, Geschäftsführerin Personal</p>\n<h3>Aufsichtsrat</h3>\n<p>Sven Kado, Vorsitzender</p>\n<p>Telefon: 06196/478-500<br>Fax: 06196/ 478-569<br>E-Mail: info.eurest [@] compass-group.de</p>\n<p>Handelsregister:<br>Amtsgericht Frankfurt am Main · HRB 44457<br>USt.-IdNr.: DE 812489838 </p>\n<h3>Konzept, Design, Umsetzung</h3>\n<p>DUBBEL SPÄTH<br>Konzept Kreation Medien</p>\n<p>Frankfurter Str. 44<br>64293 Darmstadt<br>Telefon: +49 6151 9719840<br><a href=\"http://www.dubbelspaeth.de\" target=\"_blank\">www.dubbelspaeth.de</a></p>\n<h3>Datenschutzhinweis</h3>\n<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link (<a href=\"http://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin herunterladen und installieren.</p>\n<p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a> bzw. <a href=\"http://www.google.com/analytics/terms/de.html\" target=\"_blank\">hier</a>.</p>', 'EUREST DEUTSCHLAND GmbH', 'Max Mustermann', 'Musterstraße 1\r\n12345 Musterstadt', '0123 - 456789', '0123 - 456789', 'max@mustermann.de', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig`
--

CREATE TABLE `SiteConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SiteConfig') DEFAULT 'SiteConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Tagline` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `EnableIndividualDishesPerRestaurant` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `KeepDaysDailyMenuPages` int(11) NOT NULL DEFAULT '0',
  `AllergenDisclaimer` mediumtext,
  `AllergenDisclaimer_en_US` mediumtext,
  `NutritivesUnit` varchar(255) DEFAULT NULL,
  `NutritivesUnit_en_US` varchar(255) DEFAULT NULL,
  `Currency` varchar(20) DEFAULT NULL,
  `DefaultFolderType` enum('Restaurant','Mandant','Global') DEFAULT 'Restaurant',
  `DefaultFolderRestaurantLogo` varchar(50) DEFAULT NULL,
  `DefaultFolderDishImage` varchar(50) DEFAULT NULL,
  `DefaultFolderInfoMessage` varchar(50) DEFAULT NULL,
  `DefaultFolderPrint` varchar(50) DEFAULT NULL,
  `DefaultFolderMicrosite` varchar(50) DEFAULT NULL,
  `DefaultFolderFoodCategory` varchar(50) DEFAULT NULL,
  `DefaultFolderDishLabel` varchar(50) DEFAULT NULL,
  `DefaultFolderSpecialLabel` varchar(50) DEFAULT NULL,
  `DishesAutoCompleterResultsLimit` int(11) NOT NULL DEFAULT '0',
  `EnableDishSearchForImportID` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowDishImportID` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `EnablePrintManager` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteConfig`
--

INSERT INTO `SiteConfig` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Tagline`, `Theme`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `EnableIndividualDishesPerRestaurant`, `KeepDaysDailyMenuPages`, `AllergenDisclaimer`, `AllergenDisclaimer_en_US`, `NutritivesUnit`, `NutritivesUnit_en_US`, `Currency`, `DefaultFolderType`, `DefaultFolderRestaurantLogo`, `DefaultFolderDishImage`, `DefaultFolderInfoMessage`, `DefaultFolderPrint`, `DefaultFolderMicrosite`, `DefaultFolderFoodCategory`, `DefaultFolderDishLabel`, `DefaultFolderSpecialLabel`, `DishesAutoCompleterResultsLimit`, `EnableDishSearchForImportID`, `ShowDishImportID`, `SubsiteID`, `EnablePrintManager`) VALUES
(1, 'SiteConfig', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'DSS Konfiguration', '', NULL, 'Anyone', 'LoggedInUsers', 'LoggedInUsers', 0, 14, NULL, NULL, 'pro Portion', 'per serving', 'EUR', 'Restaurant', 'Logos', 'Fotos-Gerichte', 'News', 'Druck', 'Microsite', 'Essenskategorien', 'Kennzeichnungen', 'Aktionslabels', 25, 0, 0, 0, 0),
(2, 'SiteConfig', '2016-10-27 09:54:24', '2017-02-15 12:29:35', 'VORLAGE', 'Ihr Websiteslogan', 'roche-mannheim', 'Anyone', 'OnlyTheseUsers', 'OnlyTheseUsers', 0, 0, NULL, NULL, 'pro Portion', 'per serving', 'EUR', 'Restaurant', 'Logos', 'Fotos-Gerichte', 'News', 'Druck', 'Microsite', 'Essenskategorien', 'Kennzeichnungen', 'Aktionslabels', 25, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig_CreateTopLevelGroups`
--

CREATE TABLE `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteConfig_CreateTopLevelGroups`
--

INSERT INTO `SiteConfig_CreateTopLevelGroups` (`ID`, `SiteConfigID`, `GroupID`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig_EditorGroups`
--

CREATE TABLE `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteConfig_EditorGroups`
--

INSERT INTO `SiteConfig_EditorGroups` (`ID`, `SiteConfigID`, `GroupID`) VALUES
(1, 2, 2),
(2, 2, 3),
(3, 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteConfig_ViewerGroups`
--

CREATE TABLE `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree`
--

CREATE TABLE `SiteTree` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SiteTree','Page','ErrorPage','RedirectorPage','MicrositeRedirectorPage','VirtualPage','SubsitesVirtualPage','DailyMenuPage','DishVotingContainer','DishVotingPage','MenuContainer','MicrositeContainer','MicrositePage','MicrositeNewsPage','MobileInfoPage','RecipeSubmissionContainer','RecipeSubmissionPage','RestaurantPage','TemplatePage','DisplayTemplatePage','DisplayMultiTemplatePage','DisplayTemplatePageLandscape','DisplayTemplatePagePortrait','MicrositeHomePage','MobileTemplatePage','DishRatingTemplatePage','PrintHtmlTemplatePage','PrintTemplatePage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `HomepageForDomain` varchar(100) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree`
--

INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES
(1, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:52:27', 'home', 'Digital Signage System', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 0, 0),
(4, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0),
(5, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0),
(6, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 14:50:59', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0),
(7, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 17:07:53', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0),
(9, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:52:06', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 2, 1, 0),
(11, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:29:33', 'speiseplaene', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7),
(12, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 12:45:03', '2016-01-04', 'Montag, 04.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 11),
(13, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 12:33:49', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 17, 1, 7),
(14, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-27 12:08:24', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 12, 1, 7),
(17, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-27 17:53:04', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 14, 1, 7),
(18, 'PrintHtmlTemplatePage', '2016-10-27 12:06:20', '2017-02-14 13:37:09', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 7, 1, 7),
(19, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 17:16:19', 'druck-temperaturprotokoll', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7),
(20, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'druck-produktionsblatt', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7),
(22, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:57', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7),
(23, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 17:19:11', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7),
(24, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-15 12:53:48', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 6, 1, 7),
(25, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 12:45:15', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 11, 1, 7),
(26, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-08 10:27:15', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 16, 1, 7),
(28, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:34', 'app-auf-dem-home-bilschirm-speichern', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt \"Zum Startbildschirm hinzufügen\" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 26),
(29, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:33', 'voting-gerichte', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7),
(30, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:11', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 29),
(31, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:56', 'wunschgericht', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7),
(32, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 17:23:20', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 31),
(34, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:53', 'microsite', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7),
(35, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 17:29:35', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34),
(36, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 13:15:17', 'aktuelles', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34),
(37, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:24', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34),
(38, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:55', 'snacking', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34),
(39, 'RestaurantPage', '2016-10-27 09:56:53', '2017-02-06 18:05:44', 'restaurant-2', 'Restaurant 2', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 1, 0),
(40, 'MenuContainer', '2016-10-27 11:28:11', '2017-02-06 18:04:39', 'speiseplaene-2', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(41, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 18:03:56', '2016-01-04', 'Montag, 04.01.2016', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 40),
(42, 'PrintTemplatePage', '2016-10-27 11:56:08', '2017-02-06 18:01:58', 'druck-woche-deutsch-2', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(43, 'PrintTemplatePage', '2016-10-27 12:01:27', '2017-02-06 18:01:58', 'druck-woche-englisch-2', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(44, 'PrintTemplatePage', '2016-10-27 12:03:02', '2017-02-06 18:01:58', 'druck-a4-hoch-2', 'Druck A4 hoch', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(45, 'PrintTemplatePage', '2016-10-27 12:04:18', '2017-02-06 18:01:58', 'druck-a4-quer-2', 'Druck A4 quer', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(46, 'PrintTemplatePage', '2016-10-27 12:05:25', '2017-02-06 18:01:58', 'druck-a5-2', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(47, 'PrintTemplatePage', '2016-10-27 12:06:20', '2017-02-06 18:01:58', 'druck-a7-2', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(48, 'PrintTemplatePage', '2016-10-27 12:07:03', '2017-02-06 18:01:59', 'druck-temperaturprotokoll-2', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(49, 'PrintTemplatePage', '2016-10-27 12:09:23', '2017-02-06 18:01:59', 'druck-produktionsblatt-2', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(50, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2017-02-06 18:01:59', 'bildschirm-speiseplan-quer-2', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 1, 39),
(51, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2017-02-06 18:01:59', 'bildschirm-info-2', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 1, 39),
(52, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-06 18:01:59', 'bildschirm-wochenspeiseplan-2', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 1, 39),
(53, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 18:01:59', 'bildschirm-counter-2', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 1, 39),
(54, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:06:16', 'web-app-2', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 4, 1, 39),
(55, 'MobileInfoPage', '2016-10-27 12:26:08', '2017-02-06 18:02:00', 'app-auf-dem-home-bilschirm-speichern-2', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt \"Zum Startbildschirm hinzufügen\" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 54),
(56, 'DishVotingContainer', '2016-10-27 12:27:17', '2017-02-06 18:02:00', 'voting-gerichte-2', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 1, 39),
(57, 'DishVotingPage', '2016-10-27 12:28:55', '2017-02-06 18:02:00', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 56),
(58, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2017-02-06 18:02:00', 'wunschgericht-2', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 1, 39),
(59, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2017-02-06 18:02:00', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 58),
(60, 'MicrositeContainer', '2016-10-27 12:34:42', '2017-02-06 18:02:01', 'microsite-2', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 1, 39),
(61, 'MicrositeHomePage', '2016-10-27 12:35:42', '2017-02-06 18:02:00', 'neue-microsite-startseite-2', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 60),
(62, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2017-02-06 18:02:00', 'aktuelles-2', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 60),
(63, 'MicrositePage', '2016-10-27 14:26:05', '2017-02-06 18:02:00', 'speiseplan-2', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 60),
(64, 'MicrositePage', '2016-10-27 14:31:37', '2017-02-06 18:02:00', 'snacking-2', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 60),
(65, 'ErrorPage', '2017-02-14 12:21:22', '2017-02-14 12:21:22', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 0),
(66, 'DailyMenuPage', '2017-02-15 10:57:02', '2017-02-15 10:58:58', '2016-01-05', 'Dienstag, 05.01.2016', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11),
(67, 'DailyMenuPage', '2017-02-15 12:01:52', '2017-02-15 12:03:42', '2016-01-06', 'Mittwoch, 06.01.2016', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11),
(68, 'DailyMenuPage', '2017-02-15 12:03:51', '2017-02-15 12:04:43', '2016-01-07', 'Donnerstag, 07.01.2016', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11),
(69, 'DailyMenuPage', '2017-02-15 12:05:12', '2017-02-15 12:05:57', '2016-01-08', 'Freitag, 08.01.2016', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_CrossSubsiteLinkTracking`
--

CREATE TABLE `SiteTree_CrossSubsiteLinkTracking` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_EditorGroups`
--

CREATE TABLE `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree_EditorGroups`
--

INSERT INTO `SiteTree_EditorGroups` (`ID`, `SiteTreeID`, `GroupID`) VALUES
(1, 6, 1),
(2, 7, 2),
(3, 7, 3),
(4, 7, 4),
(5, 7, 1),
(6, 7, 5),
(7, 9, 1),
(8, 10, 1),
(9, 39, 2),
(10, 39, 3),
(11, 39, 4),
(12, 39, 1),
(13, 39, 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_ImageTracking`
--

CREATE TABLE `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_LinkTracking`
--

CREATE TABLE `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_Live`
--

CREATE TABLE `SiteTree_Live` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SiteTree','Page','ErrorPage','RedirectorPage','MicrositeRedirectorPage','VirtualPage','SubsitesVirtualPage','DailyMenuPage','DishVotingContainer','DishVotingPage','MenuContainer','MicrositeContainer','MicrositePage','MicrositeNewsPage','MobileInfoPage','RecipeSubmissionContainer','RecipeSubmissionPage','RestaurantPage','TemplatePage','DisplayTemplatePage','DisplayMultiTemplatePage','DisplayTemplatePageLandscape','DisplayTemplatePagePortrait','MicrositeHomePage','MobileTemplatePage','DishRatingTemplatePage','PrintHtmlTemplatePage','PrintTemplatePage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `HomepageForDomain` varchar(100) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree_Live`
--

INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `Version`, `SubsiteID`, `ParentID`) VALUES
(1, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:53:20', 'home', 'Digital Signage System', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 0, 0),
(4, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:53:20', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0),
(5, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:53:20', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0, 0),
(6, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 17:07:53', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0),
(7, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 17:07:53', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 6, 1, 0),
(9, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:52:06', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 2, 1, 0),
(11, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:29:34', 'speiseplaene', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7),
(12, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 12:45:03', '2016-01-04', 'Montag, 04.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 11),
(13, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 12:33:49', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 17, 1, 7),
(14, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-27 12:08:24', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 12, 1, 7),
(17, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-27 17:53:04', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 14, 1, 7),
(18, 'PrintHtmlTemplatePage', '2016-10-27 12:06:20', '2017-02-14 13:37:09', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 7, 1, 7),
(19, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 17:16:19', 'druck-temperaturprotokoll', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 7),
(20, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'druck-produktionsblatt', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 7),
(22, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:57', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7),
(23, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 17:19:11', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 3, 1, 7),
(24, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-15 12:53:48', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 6, 1, 7),
(25, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 12:45:15', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 11, 1, 7),
(26, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-08 10:27:15', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 16, 1, 7),
(28, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:34', 'app-auf-dem-home-bilschirm-speichern', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt \"Zum Startbildschirm hinzufügen\" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 26),
(29, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:33', 'voting-gerichte', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7),
(30, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:11', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 29),
(31, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:56', 'wunschgericht', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 2, 1, 7),
(32, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 17:23:20', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 5, 1, 31),
(34, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:53', 'microsite', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 2, 1, 7),
(35, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 17:29:35', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34),
(36, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 13:15:17', 'aktuelles', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34),
(37, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:24', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 4, 1, 34),
(38, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:55', 'snacking', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 34),
(39, 'RestaurantPage', '2016-10-27 09:56:53', '2017-02-06 18:05:44', 'restaurant-2', 'Restaurant 2', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 1, 0),
(40, 'MenuContainer', '2016-10-27 11:28:11', '2017-02-06 18:04:39', 'speiseplaene-2', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 39),
(41, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 18:03:56', '2016-01-04', 'Montag, 04.01.2016', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 2, 1, 40),
(54, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:06:16', 'web-app-2', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 4, 1, 39),
(65, 'ErrorPage', '2017-02-14 12:21:22', '2017-02-14 12:21:23', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 1, 0),
(66, 'DailyMenuPage', '2017-02-15 10:57:02', '2017-02-15 10:58:58', '2016-01-05', 'Dienstag, 05.01.2016', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11),
(67, 'DailyMenuPage', '2017-02-15 12:01:52', '2017-02-15 12:03:42', '2016-01-06', 'Mittwoch, 06.01.2016', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11),
(68, 'DailyMenuPage', '2017-02-15 12:03:51', '2017-02-15 12:04:43', '2016-01-07', 'Donnerstag, 07.01.2016', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11),
(69, 'DailyMenuPage', '2017-02-15 12:05:12', '2017-02-15 12:05:57', '2016-01-08', 'Freitag, 08.01.2016', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 3, 1, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_versions`
--

CREATE TABLE `SiteTree_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SiteTree','Page','ErrorPage','RedirectorPage','MicrositeRedirectorPage','VirtualPage','SubsitesVirtualPage','DailyMenuPage','DishVotingContainer','DishVotingPage','MenuContainer','MicrositeContainer','MicrositePage','MicrositeNewsPage','MobileInfoPage','RecipeSubmissionContainer','RecipeSubmissionPage','RestaurantPage','TemplatePage','DisplayTemplatePage','DisplayMultiTemplatePage','DisplayTemplatePageLandscape','DisplayTemplatePagePortrait','MicrositeHomePage','MobileTemplatePage','DishRatingTemplatePage','PrintHtmlTemplatePage','PrintTemplatePage') DEFAULT 'SiteTree',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaDescription` mediumtext,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `HomepageForDomain` varchar(100) DEFAULT NULL,
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SiteTree_versions`
--

INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaDescription`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `HomepageForDomain`, `SubsiteID`, `ParentID`) VALUES
(1, 1, 1, 1, 0, 0, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'home', 'Startseite', NULL, '<p>Willkommen bei SilverStripe! Dies ist die Standard-Startseite. Sie können diese Seite bearbeiten, indem Sie <a href=\"admin/\">das CMS</a> öffnen. Sie können außerdem die  <a href=\"http://doc.silverstripe.com\">Entwicker-Dokumentation</a> oder die <a href=\"http://doc.silverstripe.com/doku.php?id=tutorials\">Einführungskurse und Tutorien</a> aufrufen.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0),
(2, 2, 1, 1, 0, 0, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'ueber-uns', 'Über uns', NULL, '<p>Sie können diese Seite mit Ihren eigenen Inhalten füllen, oder sie löschen und Ihre eigenen Seiten erstellen.<br /></p>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0),
(3, 3, 1, 1, 0, 0, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'kontakt', 'Kontakt', NULL, '<p>Sie können diese Seite mit Ihren eigenen Inhalten füllen, oder sie löschen und Ihre eigenen Seiten erstellen.<br /></p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0),
(4, 4, 1, 1, 0, 0, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0),
(5, 5, 1, 1, 0, 0, 'ErrorPage', '2016-10-27 09:41:40', '2016-10-27 09:41:40', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0),
(6, 1, 2, 1, 1, 1, 'Page', '2016-10-27 09:41:40', '2016-10-27 09:52:27', 'home', 'Digital Signage System', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 0, 0),
(7, 6, 1, 0, 1, 0, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:55:54', 'neue-weiterleitungsseite', 'Neue Weiterleitungsseite', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 1, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(8, 6, 2, 0, 1, 0, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:56:22', 'neue-weiterleitungsseite', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 1, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(9, 6, 3, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:56:35', 'neue-weiterleitungsseite', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 1, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(10, 7, 1, 0, 1, 0, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 09:56:53', 'neue-restaurant-startseite', 'Neue Restaurant Startseite', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(11, 7, 2, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 09:57:15', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(12, 7, 3, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 09:58:16', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(13, 6, 4, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:58:25', 'neue-weiterleitungsseite', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(14, 8, 1, 1, 1, 1, 'Page', '2016-10-27 09:58:42', '2016-10-27 09:58:42', 'home', 'Startseite', NULL, '<p>Willkommen bei SilverStripe! Dies ist die Standard-Startseite. Sie können diese Seite bearbeiten, indem Sie <a href=\"admin/\">das CMS</a> öffnen. Sie können außerdem die  <a href=\"http://doc.silverstripe.com\">Entwicker-Dokumentation</a> oder die <a href=\"http://doc.silverstripe.com/doku.php?id=tutorials\">Einführungskurse und Tutorien</a> aufrufen.</p>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(15, 9, 1, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 09:58:42', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(16, 10, 1, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 09:58:42', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(17, 6, 5, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 09:59:24', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(18, 11, 1, 0, 1, 0, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:28:11', 'neue-speiseplan-datenbank', 'Neue Speiseplan Datenbank', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(19, 11, 2, 1, 1, 1, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:28:50', 'restaurant', 'Restaurant', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(20, 11, 3, 1, 1, 1, 'MenuContainer', '2016-10-27 11:28:11', '2016-10-27 11:29:33', 'speiseplaene', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(21, 12, 1, 0, 1, 0, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:09', 'neue-speiseplan-tag', 'Neue Speiseplan (Tag)', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(22, 12, 2, 0, 1, 0, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:29', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(23, 12, 3, 1, 1, 1, 'DailyMenuPage', '2016-10-27 11:31:09', '2016-10-27 11:31:29', '2016-01-01', 'Freitag, 01.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(24, 7, 4, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 11:41:28', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(25, 7, 5, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 11:42:04', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(26, 7, 6, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2016-10-27 11:42:19', 'restaurant-vorlage', 'Restaurant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(33, 15, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:03:02', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(34, 15, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:04:00', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(35, 15, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:03:02', '2016-10-27 12:04:00', 'druck-a4-hoch', 'Druck A4 hoch', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(36, 16, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:04:18', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(37, 16, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:05:01', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(38, 16, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:04:18', '2016-10-27 12:05:01', 'druck-a4-quer', 'Druck A4 quer', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(45, 19, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:07:03', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(46, 19, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:07:27', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(47, 19, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:07:28', 'print-category-dina7', 'print_category_dina7', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(50, 19, 4, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:07:03', '2016-10-27 12:09:04', 'druck-temperaturprotokoll', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(51, 20, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:09:23', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(52, 20, 2, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'neue-druck-ausgabe', 'Neue Druck Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(53, 20, 3, 1, 1, 1, 'PrintTemplatePage', '2016-10-27 12:09:23', '2016-10-27 12:10:12', 'druck-produktionsblatt', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(54, 21, 1, 0, 1, 0, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:12:17', 'neue-bildschirm-ausgabe-hoch', 'Neue Bildschirm Ausgabe hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(55, 21, 2, 1, 1, 1, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:13:43', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(56, 21, 3, 1, 1, 1, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2016-10-27 12:14:13', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(57, 22, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:14:50', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(58, 22, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:37', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(59, 22, 3, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2016-10-27 12:15:57', 'bildschirm-speiseplan-quer', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(60, 23, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 12:17:23', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(61, 23, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 12:17:55', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(62, 24, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2016-10-27 12:20:02', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(63, 24, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2016-10-27 12:21:03', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(64, 25, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2016-10-27 12:22:15', 'neue-bildschirm-ausgabe-quer', 'Neue Bildschirm Ausgabe quer', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(65, 25, 2, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2016-10-27 12:22:47', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(66, 26, 1, 0, 1, 0, 'MobileTemplatePage', '2016-10-27 12:23:27', '2016-10-27 12:23:27', 'neue-mobile-ausgabe', 'Neue Mobile Ausgabe', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(68, 27, 1, 0, 1, 0, 'Page', '2016-10-27 12:25:35', '2016-10-27 12:25:35', 'neue-seite', 'Neue Seite', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(69, 28, 1, 0, 1, 0, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:08', 'neue-mobile-infoseite', 'Neue Mobile Infoseite', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 26),
(70, 28, 2, 1, 1, 1, 'MobileInfoPage', '2016-10-27 12:26:08', '2016-10-27 12:26:34', 'app-auf-dem-home-bilschirm-speichern', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt \"Zum Startbildschirm hinzufügen\" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 26),
(71, 29, 1, 0, 1, 0, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:17', 'neue-voting-gerichte-datenbank', 'Neue Voting Gerichte Datenbank', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7),
(72, 29, 2, 1, 1, 1, 'DishVotingContainer', '2016-10-27 12:27:17', '2016-10-27 12:27:33', 'voting-gerichte', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 16, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7),
(73, 30, 1, 0, 1, 0, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 12:28:55', 'neue-voting-gerichte', 'Neue Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29),
(74, 30, 2, 0, 1, 0, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 12:29:05', 'neu', 'Neu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29),
(75, 30, 3, 1, 1, 1, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 12:29:05', '2016-10-27', '27.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29),
(76, 31, 1, 0, 1, 0, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:45', 'neue-wunschgerichte-datenbank', 'Neue Wunschgerichte Datenbank', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7),
(77, 31, 2, 1, 1, 1, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2016-10-27 12:30:56', 'wunschgericht', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 17, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 7),
(78, 32, 1, 0, 1, 0, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:04', 'neue-wunschgericht-uebermittlung', 'Neue Wunschgericht Übermittlung', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31),
(79, 32, 2, 0, 1, 0, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:09', 'neu', 'Wunschgerichte Neu', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31),
(80, 32, 3, 1, 1, 1, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:09', '2016-10-27', 'Wunschgerichte 27.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31),
(81, 32, 4, 1, 1, 1, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 12:31:32', '2016-10-27', 'Wunschgerichte 27.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31),
(82, 33, 1, 0, 1, 0, 'MicrositeHomePage', '2016-10-27 12:33:06', '2016-10-27 12:33:06', 'neue-microsite-startseite', 'Neue Microsite Startseite', NULL, NULL, NULL, NULL, 1, 0, 18, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(83, 34, 1, 0, 1, 0, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:42', 'neue-microsite', 'Neue Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(84, 34, 2, 1, 1, 1, 'MicrositeContainer', '2016-10-27 12:34:42', '2016-10-27 12:34:53', 'microsite', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 18, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(85, 35, 1, 0, 1, 0, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 12:35:42', 'neue-microsite-startseite', 'Neue Microsite Startseite', NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(86, 35, 2, 1, 1, 1, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 12:36:05', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(87, 36, 1, 1, 1, 1, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 12:46:56', 'neue-microsite-news-seite', 'Neue Microsite News Seite', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(89, 36, 2, 1, 1, 1, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2016-10-27 13:15:17', 'aktuelles', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(90, 35, 3, 1, 1, 1, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 13:24:02', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(91, 37, 1, 0, 1, 0, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:26:05', 'neue-microsite-seite', 'Neue Microsite Seite', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(92, 37, 2, 1, 1, 1, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:27:56', 'speiseplan', 'Speiseplan', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(93, 37, 3, 1, 1, 1, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:02', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(94, 37, 4, 1, 1, 1, 'MicrositePage', '2016-10-27 14:26:05', '2016-10-27 14:28:24', 'speiseplan', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(95, 38, 1, 0, 1, 0, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:37', 'neue-microsite-seite', 'Neue Microsite Seite', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(96, 38, 2, 1, 1, 1, 'MicrositePage', '2016-10-27 14:31:37', '2016-10-27 14:31:55', 'snacking', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(97, 6, 6, 1, 1, 1, 'RedirectorPage', '2016-10-27 09:55:54', '2016-10-27 14:50:59', 'home', 'Mandant Vorlage', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(98, 9, 2, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:52:06', 'seite-nicht-gefunden', 'Seite nicht gefunden', NULL, '<p>Entschuldigung, möglicherweise versuchen Sie eine Seite zu erreichen die nicht existiert.</p><p>Bitte überprüfen Sie die Schreibweise der URL die Sie versucht haben zu erreichen und versuchen Sie es noch einmal.</p>', NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(99, 10, 2, 1, 1, 1, 'ErrorPage', '2016-10-27 09:58:42', '2016-10-27 14:53:03', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 9, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(100, 23, 3, 1, 2, 2, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2016-10-27 17:19:11', 'bildschirm-info', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 12, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(101, 30, 4, 1, 2, 2, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:03', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29),
(102, 30, 5, 1, 2, 2, 'DishVotingPage', '2016-10-27 12:28:55', '2016-10-27 17:23:11', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 29),
(103, 32, 5, 1, 2, 2, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2016-10-27 17:23:20', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 31),
(104, 35, 4, 1, 2, 2, 'MicrositeHomePage', '2016-10-27 12:35:42', '2016-10-27 17:29:35', 'neue-microsite-startseite', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 34),
(105, 25, 3, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-01 13:02:32', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(106, 21, 4, 1, 1, 1, 'DisplayTemplatePagePortrait', '2016-10-27 12:12:17', '2017-02-01 14:34:27', 'bildschirm-speiseplan-hoch', 'Bildschirm Speiseplan hoch', NULL, NULL, NULL, NULL, 0, 0, 10, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(107, 25, 4, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-01 14:39:20', 'bildschirm-speiseplan-hoch-2', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(108, 25, 5, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-01 14:40:07', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(109, 25, 6, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-01 14:40:40', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(110, 25, 7, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-01 15:04:07', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(111, 25, 8, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-01 18:24:13', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(112, 25, 9, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 12:00:09', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(113, 25, 10, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 12:00:24', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(117, 26, 7, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 12:23:33', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(118, 26, 8, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 12:25:31', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(119, 26, 9, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 12:25:51', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(120, 26, 10, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 12:26:11', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(121, 12, 4, 1, 1, 1, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 12:45:03', '2016-01-04', 'Montag, 04.01.2016', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(122, 25, 11, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 12:45:15', 'bildschirm-counter', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 14, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(123, 26, 11, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 12:45:38', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(124, 41, 1, 0, 1, 0, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 18:01:58', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 40),
(125, 40, 1, 1, 1, 1, 'MenuContainer', '2016-10-27 11:28:11', '2017-02-06 18:01:58', 'speiseplaene-2', 'Speisepläne', NULL, NULL, NULL, NULL, 1, 1, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(126, 42, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 11:56:08', '2017-02-06 18:01:58', 'druck-woche-deutsch-2', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(127, 43, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:01:27', '2017-02-06 18:01:58', 'druck-woche-englisch-2', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(128, 44, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:03:02', '2017-02-06 18:01:58', 'druck-a4-hoch-2', 'Druck A4 hoch', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(129, 45, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:04:18', '2017-02-06 18:01:58', 'druck-a4-quer-2', 'Druck A4 quer', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(130, 46, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:05:25', '2017-02-06 18:01:58', 'druck-a5-2', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(131, 47, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:06:20', '2017-02-06 18:01:58', 'druck-a7-2', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(132, 48, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:07:03', '2017-02-06 18:01:59', 'druck-temperaturprotokoll-2', 'Druck Temperaturprotokoll', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(133, 49, 1, 0, 1, 0, 'PrintTemplatePage', '2016-10-27 12:09:23', '2017-02-06 18:01:59', 'druck-produktionsblatt-2', 'Druck Produktionsblatt', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 39),
(134, 50, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:14:50', '2017-02-06 18:01:59', 'bildschirm-speiseplan-quer-2', 'Bildschirm Speiseplan quer', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(135, 51, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:17:23', '2017-02-06 18:01:59', 'bildschirm-info-2', 'Bildschirm Info', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(136, 52, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-06 18:01:59', 'bildschirm-wochenspeiseplan-2', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(137, 53, 1, 0, 1, 0, 'DisplayTemplatePageLandscape', '2016-10-27 12:22:15', '2017-02-06 18:01:59', 'bildschirm-counter-2', 'Bildschirm Counter', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(138, 55, 1, 0, 1, 0, 'MobileInfoPage', '2016-10-27 12:26:08', '2017-02-06 18:02:00', 'app-auf-dem-home-bilschirm-speichern-2', 'App auf dem Home-Bilschirm speichern', NULL, '<p>Starten Sie die Speiseplan Web-App noch schneller direkt von Ihrem Home-Bildschirm!</p>\n<p>HINWEIS:<br>Wenn Sie einen QR-Code Scanner verwenden, öffnen Sie die Speiseplan Web-App nach dem Scannen in Ihrem Standard Browser (Safari, Firefox, Chrome).</p>\n<h3>Anleitung für iPhone / iPad</h3>\n<ul><li>In der Browser Leiste am unteren Ende des Bildschirms auf das Pfeil-Symbol in der Mitte tippen</li>\n<li>„Zum Home-Bildschirm“ antippen</li>\n<li>Name für die Web-App eintippen</li>\n<li>Mit „Hinzufügen“ bestätigen</li>\n</ul><h3>Anleitung für Android</h3>\n<ul><li>In der Browser Leiste oben rechts auf das Menü-Symbol (drei kleine Punkte) tippen</li>\n<li>Das Stern-Symbol antippen um die Web-App als Lesezeichen zu speichern</li>\n<li>Die Lesezeichen-Übersicht über das Menü öffnen</li>\n<li>Einmal lang auf das neue Web-App Lesezeichen tippen</li>\n<li>Den Punkt \"Zum Startbildschirm hinzufügen\" antippen (letzter Punkt)</li>\n</ul>', NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 54),
(139, 54, 1, 0, 1, 0, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:02:00', 'web-app-2', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(140, 57, 1, 0, 1, 0, 'DishVotingPage', '2016-10-27 12:28:55', '2017-02-06 18:02:00', '2016-10-28', '28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 56),
(141, 56, 1, 0, 1, 0, 'DishVotingContainer', '2016-10-27 12:27:17', '2017-02-06 18:02:00', 'voting-gerichte-2', 'Voting Gerichte', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 39),
(142, 59, 1, 0, 1, 0, 'RecipeSubmissionPage', '2016-10-27 12:31:04', '2017-02-06 18:02:00', '2016-10-28', 'Wunschgerichte 28.10.2016', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 58),
(143, 58, 1, 0, 1, 0, 'RecipeSubmissionContainer', '2016-10-27 12:30:45', '2017-02-06 18:02:00', 'wunschgericht-2', 'Wunschgericht', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'LoggedInUsers', 'Inherit', NULL, 1, 39),
(144, 61, 1, 0, 1, 0, 'MicrositeHomePage', '2016-10-27 12:35:42', '2017-02-06 18:02:00', 'neue-microsite-startseite-2', 'Restaurant', NULL, '<h2>Willkommen in Ihrem Eurest Betriebsrestaurant!</h2>\n<p>Liebe Gäste,<br><strong>Catering ist mehr als nur eine warme Mahlzeit.</strong> Wir von Eurest liefern kulinarische Qualität. Wir setzen alles daran, Sie jeden Tag aufs Neue mit einem vielfältigen und ständig wechselnden Speisenangebot zufrieden zu stellen. Erfahren Sie mehr über unser Angebot und unsere Aktionen.</p>\n<p>Wir freuen uns auf Sie!<br>Ihr Eurest Team</p>', NULL, NULL, 1, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 60),
(145, 62, 1, 0, 1, 0, 'MicrositeNewsPage', '2016-10-27 12:46:56', '2017-02-06 18:02:00', 'aktuelles-2', 'Aktuelles', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 60),
(146, 63, 1, 0, 1, 0, 'MicrositePage', '2016-10-27 14:26:05', '2017-02-06 18:02:00', 'speiseplan-2', 'Speiseplan', NULL, '<h4>„Wir wollen, dass sich unsere Gäste jeden Tag auf die Mittagspause freuen. Dass sie neue Kräfte schöpfen und wieder motiviert durchstarten können.“</h4>\n<p>Kantine war gestern, Betriebsrestaurant ist heute: ein Ort der Entspannung, in dem unsere Gäste zu Mitgenießern werden. Denn Betriebsrestaurant bedeutet durchkomponierte Menüfolgen, ausgewählte Zutaten, herzliche Servicekräfte und ein Ambiente, in dem man sich einfach wohl fühlt.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 60),
(147, 64, 1, 0, 1, 0, 'MicrositePage', '2016-10-27 14:31:37', '2017-02-06 18:02:00', 'snacking-2', 'Snacking', NULL, '<h4>Snacking &amp; Caffè Dallucci</h4>\n<p>Unser Caffè Dallucci ist ein modernes Kaffeebar Konzept, das durch sein italienisches Flair und sein vielfältiges Angebot besticht. Neben leckeren Kaffeespezialitäten, Tees und Kaltgetränken bietet die Menükarte eine reiche Auswahl an süßen und herzhaften Snacks.</p>', NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 60),
(148, 60, 1, 0, 1, 0, 'MicrositeContainer', '2016-10-27 12:34:42', '2017-02-06 18:02:01', 'microsite-2', 'Microsite', NULL, NULL, NULL, NULL, 1, 1, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(149, 39, 1, 1, 1, 1, 'RestaurantPage', '2016-10-27 09:56:53', '2017-02-06 18:02:16', 'restaurant-2', 'Restaurant 2', NULL, NULL, NULL, NULL, 1, 1, 10, 0, 0, NULL, 'Inherit', 'OnlyTheseUsers', NULL, 1, 0),
(150, 54, 2, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:03:13', 'web-app-2', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(151, 41, 2, 1, 1, 1, 'DailyMenuPage', '2016-10-27 11:31:09', '2017-02-06 18:03:56', '2016-01-04', 'Montag, 04.01.2016', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 40),
(152, 54, 3, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:04:30', 'web-app-2', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(153, 54, 4, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:06:16', 'web-app-2', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 19, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 39),
(154, 26, 12, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-06 18:06:28', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(155, 26, 13, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-08 09:58:30', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(156, 26, 14, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-08 09:58:57', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(157, 26, 15, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-08 10:02:30', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(158, 26, 16, 1, 1, 1, 'MobileTemplatePage', '2016-10-27 12:23:27', '2017-02-08 10:27:15', 'web-app', 'Web-App', NULL, NULL, NULL, NULL, 0, 0, 15, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(161, 14, 5, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-08 14:39:00', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(162, 14, 6, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-08 14:39:11', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(165, 17, 5, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-08 14:40:09', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(166, 18, 4, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:06:20', '2017-02-08 14:40:17', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(167, 18, 5, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:06:20', '2017-02-08 14:40:24', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(168, 13, 8, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-08 15:00:47', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(169, 13, 9, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-08 15:10:12', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(170, 17, 6, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-13 10:52:49', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(171, 17, 7, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-13 11:02:09', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(172, 17, 8, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-13 11:02:41', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(173, 17, 9, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-13 11:05:10', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(174, 18, 6, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:06:20', '2017-02-14 12:20:57', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(175, 65, 1, 1, 1, 1, 'ErrorPage', '2017-02-14 12:21:22', '2017-02-14 12:21:22', 'serverfehler', 'Serverfehler', NULL, '<p>Entschuldigung, bei der Bearbeitung ihrer Anfrage ist ein Problem aufgetreten.</p>', NULL, NULL, 0, 0, 11, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 0),
(176, 18, 7, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:06:20', '2017-02-14 13:37:09', 'druck-a7', 'Druck A7', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(177, 13, 10, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 09:34:27', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(178, 13, 11, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 09:43:47', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(179, 24, 3, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-15 09:47:06', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(180, 13, 12, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 10:08:37', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(181, 24, 4, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-15 10:11:19', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(182, 13, 13, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 10:20:29', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(183, 13, 14, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 10:41:46', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(184, 66, 1, 0, 1, 0, 'DailyMenuPage', '2017-02-15 10:57:02', '2017-02-15 10:57:02', 'neue-speiseplan-tag', 'Neue Speiseplan (Tag)', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(185, 66, 2, 0, 1, 0, 'DailyMenuPage', '2017-02-15 10:57:02', '2017-02-15 10:57:24', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(186, 66, 3, 1, 1, 1, 'DailyMenuPage', '2017-02-15 10:57:02', '2017-02-15 10:57:24', '2016-01-05', 'Dienstag, 05.01.2016', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(187, 13, 15, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 11:00:39', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(188, 67, 1, 0, 1, 0, 'DailyMenuPage', '2017-02-15 12:01:52', '2017-02-15 12:01:52', 'neue-speiseplan-tag', 'Neue Speiseplan (Tag)', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(189, 67, 2, 0, 1, 0, 'DailyMenuPage', '2017-02-15 12:01:52', '2017-02-15 12:03:41', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(190, 67, 3, 1, 1, 1, 'DailyMenuPage', '2017-02-15 12:01:52', '2017-02-15 12:03:42', '2016-01-06', 'Mittwoch, 06.01.2016', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(191, 68, 1, 0, 1, 0, 'DailyMenuPage', '2017-02-15 12:03:51', '2017-02-15 12:03:51', 'neue-speiseplan-tag', 'Neue Speiseplan (Tag)', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(192, 68, 2, 0, 1, 0, 'DailyMenuPage', '2017-02-15 12:03:51', '2017-02-15 12:04:43', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(193, 68, 3, 1, 1, 1, 'DailyMenuPage', '2017-02-15 12:03:51', '2017-02-15 12:04:43', '2016-01-07', 'Donnerstag, 07.01.2016', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(194, 69, 1, 0, 1, 0, 'DailyMenuPage', '2017-02-15 12:05:12', '2017-02-15 12:05:12', 'neue-speiseplan-tag', 'Neue Speiseplan (Tag)', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(195, 69, 2, 0, 1, 0, 'DailyMenuPage', '2017-02-15 12:05:12', '2017-02-15 12:05:57', 'neu', 'Neu', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(196, 69, 3, 1, 1, 1, 'DailyMenuPage', '2017-02-15 12:05:12', '2017-02-15 12:05:57', '2016-01-08', 'Freitag, 08.01.2016', NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 11),
(197, 24, 5, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-15 12:08:21', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(198, 13, 16, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 12:32:56', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(199, 13, 17, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 11:56:08', '2017-02-15 12:33:04', 'druck-woche-deutsch', 'Druck Woche deutsch', NULL, NULL, NULL, NULL, 0, 0, 2, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(200, 24, 6, 1, 1, 1, 'DisplayTemplatePageLandscape', '2016-10-27 12:20:02', '2017-02-15 12:53:48', 'bildschirm-wochenspeiseplan', 'Bildschirm Wochenspeiseplan', NULL, NULL, NULL, NULL, 0, 0, 13, 0, 0, NULL, 'Anyone', 'Inherit', NULL, 1, 7),
(201, 17, 10, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-20 14:13:58', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(202, 17, 11, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-20 14:15:36', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(203, 14, 7, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-22 16:52:27', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(204, 14, 8, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-22 16:52:33', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(205, 14, 9, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-22 16:59:00', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(206, 14, 10, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-27 12:07:15', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(207, 14, 11, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-27 12:07:40', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(208, 14, 12, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:01:27', '2017-02-27 12:08:24', 'druck-woche-englisch', 'Druck Woche englisch', NULL, NULL, NULL, NULL, 0, 0, 3, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(209, 17, 12, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-27 16:53:46', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(210, 17, 13, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-27 17:52:25', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7),
(211, 17, 14, 1, 1, 1, 'PrintHtmlTemplatePage', '2016-10-27 12:05:25', '2017-02-27 17:53:04', 'druck-a5', 'Druck A5', NULL, NULL, NULL, NULL, 0, 0, 6, 0, 0, NULL, 'Inherit', 'Inherit', NULL, 1, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SiteTree_ViewerGroups`
--

CREATE TABLE `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SpecialLabelsBlacklistConfig`
--

CREATE TABLE `SpecialLabelsBlacklistConfig` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SpecialLabelsBlacklistConfig') DEFAULT 'SpecialLabelsBlacklistConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SpecialLabelsBlacklistObject`
--

CREATE TABLE `SpecialLabelsBlacklistObject` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SpecialLabelsBlacklistObject') DEFAULT 'SpecialLabelsBlacklistObject',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `SpecialLabelsBlacklistConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Subsite`
--

CREATE TABLE `Subsite` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('Subsite') DEFAULT 'Subsite',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `RedirectURL` varchar(255) DEFAULT NULL,
  `DefaultSite` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Theme` varchar(50) DEFAULT NULL,
  `Language` varchar(6) DEFAULT NULL,
  `IsPublic` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `PageTypeBlacklist` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `Subsite`
--

INSERT INTO `Subsite` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `RedirectURL`, `DefaultSite`, `Theme`, `Language`, `IsPublic`, `PageTypeBlacklist`) VALUES
(1, 'Subsite', '2016-10-27 09:54:02', '2017-02-01 12:38:02', 'ROCHE MANNHEIM', NULL, 0, 'default-theme', 'de_DE', 1, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteAdditive`
--

CREATE TABLE `SubsiteAdditive` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteAdditive') DEFAULT 'SubsiteAdditive',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SubsiteAdditive`
--

INSERT INTO `SubsiteAdditive` (`ID`, `ClassName`, `Created`, `LastEdited`, `Number`, `Title`, `Title_en_US`, `SortOrder`, `SubsiteID`) VALUES
(1, 'SubsiteAdditive', '2017-02-01 16:46:12', '2017-02-01 16:47:09', '1', 'Farbstoff', 'dye', 0, 1),
(2, 'SubsiteAdditive', '2017-02-01 16:46:24', '2017-02-01 16:47:14', '2', 'Konservierungsstoff', 'preservatives', 0, 1),
(3, 'SubsiteAdditive', '2017-02-01 16:46:45', '2017-02-01 16:46:55', '3', 'Antioxidationsmittel', 'anti-oxidant', 0, 1),
(4, 'SubsiteAdditive', '2017-02-01 16:47:33', '2017-02-01 16:47:33', '4', 'Geschmacksverstärker', 'flavor enhancers', 0, 1),
(5, 'SubsiteAdditive', '2017-02-01 16:47:46', '2017-02-01 16:47:46', '5', 'Geschwefelt', 'sulfurized', 0, 1),
(6, 'SubsiteAdditive', '2017-02-01 16:48:06', '2017-02-01 16:48:06', '6', 'Geschwärzt', 'blackened', 0, 1),
(7, 'SubsiteAdditive', '2017-02-01 16:48:22', '2017-02-01 16:48:22', '7', 'Gewachst', 'waxed', 0, 1),
(8, 'SubsiteAdditive', '2017-02-01 16:48:37', '2017-02-01 16:48:37', '8', 'Phosphat', 'phosphate', 0, 1),
(9, 'SubsiteAdditive', '2017-02-01 16:48:56', '2017-02-01 16:48:56', '9', 'Süßungsmittel', 'sweetener', 0, 1),
(10, 'SubsiteAdditive', '2017-02-01 16:49:10', '2017-02-01 16:49:16', '10', 'Phenylalaninquelle', 'source of phenylalanine', 0, 1),
(11, 'SubsiteAdditive', '2017-02-01 16:49:31', '2017-02-01 16:49:31', '11', 'Genetisch verändert', 'genetically modified', 0, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteAllergen`
--

CREATE TABLE `SubsiteAllergen` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteAllergen') DEFAULT 'SubsiteAllergen',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Title_en_US` varchar(255) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SubsiteAllergen`
--

INSERT INTO `SubsiteAllergen` (`ID`, `ClassName`, `Created`, `LastEdited`, `Number`, `Title`, `Title_en_US`, `SortOrder`, `SubsiteID`) VALUES
(1, 'SubsiteAllergen', '2017-02-01 16:50:51', '2017-02-01 16:50:51', '21', 'Glutenhaltiges Getreide', 'Cereals containing gluten', 0, 1),
(2, 'SubsiteAllergen', '2017-02-01 16:51:03', '2017-02-01 16:51:03', '22', 'Fisch und Fischerzeugnisse', 'Fish and fish products', 0, 1),
(3, 'SubsiteAllergen', '2017-02-01 16:51:17', '2017-02-01 16:51:17', '23', 'Krebstiere und Krebstiererzeugnisse', 'Crustaceans and crustacean products', 0, 1),
(4, 'SubsiteAllergen', '2017-02-01 16:51:41', '2017-02-01 16:51:54', '24', 'Eier und Eiererzeugnisse', 'Eggs and egg products', 0, 1),
(5, 'SubsiteAllergen', '2017-02-01 16:52:23', '2017-02-01 16:52:23', '25', 'Erdnüsse und Erdnusserzeugnisse', 'Peanuts and products thereof', 0, 1),
(6, 'SubsiteAllergen', '2017-02-01 16:52:38', '2017-02-01 16:52:38', '26', 'Soja und Sojaerzeugnisse', 'Soybeans and products thereof', 0, 1),
(7, 'SubsiteAllergen', '2017-02-01 16:52:51', '2017-02-01 16:52:51', '27', 'Milch und Milcherzeugnisse', 'Milk and milk products', 0, 1),
(8, 'SubsiteAllergen', '2017-02-01 16:53:05', '2017-02-01 16:53:05', '28', 'Schalenfrüchte', 'nuts', 0, 1),
(9, 'SubsiteAllergen', '2017-02-01 16:53:22', '2017-02-01 16:53:22', '29', 'Sellerie und Sellerieerzeugnisse', 'Celery and products thereof', 0, 1),
(10, 'SubsiteAllergen', '2017-02-01 16:53:37', '2017-02-01 16:53:37', '30', 'Senf und Senferzeugnisse', 'Mustard and products thereof', 0, 1),
(11, 'SubsiteAllergen', '2017-02-01 16:53:50', '2017-02-01 16:53:50', '31', 'Sesamsamen und Sesamsamenerzeugnisse', 'Sesame seeds and sesame seed products', 0, 1),
(12, 'SubsiteAllergen', '2017-02-01 16:54:07', '2017-02-01 16:54:07', '32', 'Schwefeldioxid und Sulfite', 'Sulphur dioxide and sulphites', 0, 1),
(13, 'SubsiteAllergen', '2017-02-01 16:54:24', '2017-02-01 16:54:24', '33', 'Lupine', 'Lupine', 0, 1),
(14, 'SubsiteAllergen', '2017-02-01 16:54:42', '2017-02-01 16:54:42', '34', 'Weichtiere und Weichtiererzeugnisse', 'Molluscs and products thereof', 0, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteDishLabel`
--

CREATE TABLE `SubsiteDishLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteDishLabel') DEFAULT 'SubsiteDishLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `IconUnicodeCode` varchar(50) DEFAULT NULL,
  `Color` varchar(7) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SubsiteDishLabel`
--

INSERT INTO `SubsiteDishLabel` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Title_en_US`, `IconUnicodeCode`, `Color`, `SortOrder`, `SubsiteID`, `ImageID`) VALUES
(1, 'SubsiteDishLabel', '2017-02-01 17:25:56', '2017-02-01 17:25:56', 'vegetarisch', 'vegetarian', '76', NULL, 0, 1, 92),
(2, 'SubsiteDishLabel', '2017-02-01 17:26:23', '2017-02-01 17:26:26', 'vegan', 'vegan', '56', NULL, 0, 1, 93),
(3, 'SubsiteDishLabel', '2017-02-01 17:26:47', '2017-02-01 17:26:47', 'Fisch', 'fish', '66', NULL, 0, 1, 94),
(4, 'SubsiteDishLabel', '2017-02-01 17:27:14', '2017-02-01 17:27:14', 'Schwein', 'pork', '73', NULL, 0, 1, 95),
(5, 'SubsiteDishLabel', '2017-02-01 17:27:33', '2017-02-01 17:27:33', 'Rind', 'beef', '72', NULL, 0, 1, 96),
(6, 'SubsiteDishLabel', '2017-02-01 17:28:14', '2017-02-01 17:28:14', 'Chili', 'Chili', '63', NULL, 0, 1, 97),
(7, 'SubsiteDishLabel', '2017-02-01 17:28:35', '2017-02-01 17:28:35', 'Zwiebel', 'Onion', '7a', NULL, 0, 1, 98),
(8, 'SubsiteDishLabel', '2017-02-01 17:28:58', '2017-02-06 17:16:54', 'Knoblauch', 'Garlic', '6b', NULL, 0, 1, 99),
(9, 'SubsiteDishLabel', '2017-02-01 17:29:27', '2017-02-06 17:16:44', 'Bio Siegel', 'Bio Siegel', '62', NULL, 0, 1, 100),
(10, 'SubsiteDishLabel', '2017-02-01 17:29:54', '2017-02-01 17:29:54', 'Tagesmenü', 'menu', '74', '#bd0f1f', 0, 1, 101),
(11, 'SubsiteDishLabel', '2017-02-06 17:17:43', '2017-02-06 17:18:24', 'Laktosefrei', 'Lactose-free', '6c', NULL, 0, 1, 103),
(12, 'SubsiteDishLabel', '2017-02-06 17:19:26', '2017-02-06 17:19:26', 'Geflügel', 'Chicken', '68', NULL, 0, 1, 104);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteDomain`
--

CREATE TABLE `SubsiteDomain` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteDomain') DEFAULT 'SubsiteDomain',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Domain` varchar(255) DEFAULT NULL,
  `IsPrimary` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `SubsiteDomain`
--

INSERT INTO `SubsiteDomain` (`ID`, `ClassName`, `Created`, `LastEdited`, `Domain`, `IsPrimary`, `SubsiteID`) VALUES
(1, 'SubsiteDomain', '2016-10-27 09:54:17', '2017-02-01 12:38:10', 'roche-ma-lars.dev-dubbelspaeth.de', 1, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsiteSpecialLabel`
--

CREATE TABLE `SubsiteSpecialLabel` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('SubsiteSpecialLabel') DEFAULT 'SubsiteSpecialLabel',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Title_en_US` varchar(50) DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `SubsiteID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsitesVirtualPage`
--

CREATE TABLE `SubsitesVirtualPage` (
  `ID` int(11) NOT NULL,
  `CustomMetaTitle` varchar(255) DEFAULT NULL,
  `CustomMetaKeywords` varchar(255) DEFAULT NULL,
  `CustomMetaDescription` mediumtext,
  `CustomExtraMeta` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsitesVirtualPage_Live`
--

CREATE TABLE `SubsitesVirtualPage_Live` (
  `ID` int(11) NOT NULL,
  `CustomMetaTitle` varchar(255) DEFAULT NULL,
  `CustomMetaKeywords` varchar(255) DEFAULT NULL,
  `CustomMetaDescription` mediumtext,
  `CustomExtraMeta` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SubsitesVirtualPage_versions`
--

CREATE TABLE `SubsitesVirtualPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `CustomMetaTitle` varchar(255) DEFAULT NULL,
  `CustomMetaKeywords` varchar(255) DEFAULT NULL,
  `CustomMetaDescription` mediumtext,
  `CustomExtraMeta` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage`
--

CREATE TABLE `TemplatePage` (
  `ID` int(11) NOT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage`
--

INSERT INTO `TemplatePage` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES
(13, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(14, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'This is the english translated footnote placeholder text.', 11),
(17, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(18, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(19, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11),
(20, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(22, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(23, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(24, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(25, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(26, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(35, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(42, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(43, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(44, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(45, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(46, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(47, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(48, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11),
(49, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(50, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(51, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(52, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(53, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(54, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(61, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage_FoodCategories`
--

CREATE TABLE `TemplatePage_FoodCategories` (
  `ID` int(11) NOT NULL,
  `TemplatePageID` int(11) NOT NULL DEFAULT '0',
  `FoodCategoryID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage_FoodCategories`
--

INSERT INTO `TemplatePage_FoodCategories` (`ID`, `TemplatePageID`, `FoodCategoryID`) VALUES
(1, 13, 1),
(2, 13, 2),
(3, 13, 3),
(4, 13, 4),
(5, 13, 5),
(6, 13, 6),
(7, 13, 7),
(8, 13, 8),
(9, 13, 9),
(10, 14, 1),
(11, 14, 2),
(12, 14, 3),
(13, 14, 4),
(14, 14, 5),
(15, 14, 6),
(16, 14, 7),
(17, 14, 8),
(18, 14, 9),
(19, 15, 1),
(20, 15, 2),
(21, 15, 3),
(22, 15, 4),
(23, 15, 5),
(24, 15, 6),
(25, 15, 7),
(26, 15, 8),
(27, 15, 9),
(28, 16, 1),
(29, 16, 2),
(30, 16, 3),
(31, 16, 4),
(32, 16, 5),
(33, 16, 6),
(34, 16, 7),
(35, 16, 8),
(36, 16, 9),
(37, 17, 1),
(38, 17, 2),
(39, 17, 3),
(40, 17, 4),
(41, 17, 5),
(42, 17, 6),
(43, 17, 7),
(44, 17, 8),
(45, 17, 9),
(46, 18, 1),
(47, 18, 2),
(48, 18, 3),
(49, 18, 4),
(50, 18, 5),
(51, 18, 6),
(52, 18, 7),
(53, 18, 8),
(54, 18, 9),
(55, 19, 1),
(56, 19, 2),
(57, 19, 3),
(58, 19, 4),
(59, 19, 5),
(60, 19, 6),
(61, 19, 7),
(62, 19, 8),
(63, 19, 9),
(64, 20, 1),
(65, 20, 2),
(66, 20, 3),
(67, 20, 4),
(68, 20, 5),
(69, 20, 6),
(70, 20, 7),
(71, 20, 8),
(72, 20, 9),
(73, 21, 3),
(74, 21, 4),
(75, 21, 5),
(76, 21, 6),
(77, 22, 3),
(78, 22, 4),
(79, 22, 5),
(80, 22, 6),
(81, 24, 1),
(82, 24, 2),
(83, 24, 3),
(84, 24, 4),
(85, 24, 5),
(86, 24, 6),
(88, 26, 1),
(89, 26, 2),
(90, 26, 3),
(91, 26, 4),
(92, 26, 5),
(93, 26, 6),
(94, 26, 7),
(95, 26, 8),
(96, 26, 9),
(97, 35, 3),
(98, 35, 4),
(99, 35, 5),
(100, 35, 6),
(101, 25, 3),
(102, 25, 5),
(103, 42, 1),
(104, 42, 5),
(105, 42, 6),
(106, 42, 4),
(107, 42, 7),
(108, 42, 8),
(109, 42, 9),
(110, 42, 2),
(111, 42, 3),
(112, 43, 1),
(113, 43, 5),
(114, 43, 6),
(115, 43, 4),
(116, 43, 7),
(117, 43, 8),
(118, 43, 9),
(119, 43, 2),
(120, 43, 3),
(121, 44, 1),
(122, 44, 5),
(123, 44, 6),
(124, 44, 4),
(125, 44, 7),
(126, 44, 8),
(127, 44, 9),
(128, 44, 2),
(129, 44, 3),
(130, 45, 1),
(131, 45, 5),
(132, 45, 6),
(133, 45, 4),
(134, 45, 7),
(135, 45, 8),
(136, 45, 9),
(137, 45, 2),
(138, 45, 3),
(139, 46, 1),
(140, 46, 5),
(141, 46, 6),
(142, 46, 4),
(143, 46, 7),
(144, 46, 8),
(145, 46, 9),
(146, 46, 2),
(147, 46, 3),
(148, 47, 1),
(149, 47, 5),
(150, 47, 6),
(151, 47, 4),
(152, 47, 7),
(153, 47, 8),
(154, 47, 9),
(155, 47, 2),
(156, 47, 3),
(157, 48, 1),
(158, 48, 5),
(159, 48, 6),
(160, 48, 4),
(161, 48, 7),
(162, 48, 8),
(163, 48, 9),
(164, 48, 2),
(165, 48, 3),
(166, 49, 1),
(167, 49, 5),
(168, 49, 6),
(169, 49, 4),
(170, 49, 7),
(171, 49, 8),
(172, 49, 9),
(173, 49, 2),
(174, 49, 3),
(175, 50, 5),
(176, 50, 6),
(177, 50, 4),
(178, 50, 3),
(179, 52, 1),
(180, 52, 5),
(181, 52, 6),
(182, 52, 4),
(183, 52, 2),
(184, 52, 3),
(185, 53, 5),
(186, 53, 3),
(196, 61, 5),
(197, 61, 6),
(198, 61, 4),
(199, 61, 3),
(200, 54, 12),
(201, 54, 13),
(202, 54, 14),
(203, 54, 15),
(204, 54, 16),
(205, 54, 17),
(206, 54, 18),
(207, 54, 19),
(208, 54, 20),
(209, 54, 21),
(210, 54, 22),
(211, 24, 11),
(212, 24, 10),
(213, 24, 7),
(214, 24, 8),
(215, 24, 9);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage_Live`
--

CREATE TABLE `TemplatePage_Live` (
  `ID` int(11) NOT NULL,
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage_Live`
--

INSERT INTO `TemplatePage_Live` (`ID`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES
(13, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(14, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'This is the english translated footnote placeholder text.', 11),
(17, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(18, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(19, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11),
(20, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(22, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(23, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(24, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(25, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(26, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(35, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(54, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `TemplatePage_versions`
--

CREATE TABLE `TemplatePage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `DatePeriod` enum('Tage','Wochen') DEFAULT 'Tage',
  `DateRange` int(11) NOT NULL DEFAULT '0',
  `StartDateType` enum('Aktuellem-Datum','Anfangsdatum') DEFAULT 'Aktuellem-Datum',
  `StartDate` date DEFAULT NULL,
  `FootNote` mediumtext,
  `MenuContainerID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `TemplatePage_versions`
--

INSERT INTO `TemplatePage_versions` (`ID`, `RecordID`, `Version`, `DatePeriod`, `DateRange`, `StartDateType`, `StartDate`, `FootNote`, `MenuContainerID`) VALUES
(7, 15, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(8, 15, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(9, 15, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(10, 16, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(11, 16, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(12, 16, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(19, 19, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(20, 19, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(21, 19, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(24, 19, 4, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11),
(25, 20, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(26, 20, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(27, 20, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(28, 21, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(29, 21, 2, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz', 11),
(30, 21, 3, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(31, 22, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(32, 22, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz', 11),
(33, 22, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(34, 23, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(35, 23, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(36, 24, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(37, 24, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(38, 25, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(39, 25, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(40, 26, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(42, 33, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(43, 35, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 0),
(44, 35, 2, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(46, 35, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(47, 23, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(48, 35, 4, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(49, 25, 3, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(50, 21, 4, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(51, 25, 4, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(52, 25, 5, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(53, 25, 6, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(54, 25, 7, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(55, 25, 8, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(56, 25, 9, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(57, 25, 10, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(61, 26, 7, 'Wochen', 1, 'Anfangsdatum', '2015-12-28', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(62, 26, 8, 'Tage', 1, 'Anfangsdatum', '2016-01-01', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(63, 26, 9, 'Tage', 1, 'Anfangsdatum', '2015-12-28', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(64, 26, 10, 'Wochen', 1, 'Anfangsdatum', '2015-12-28', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(65, 25, 11, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(66, 26, 11, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(67, 42, 1, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(68, 43, 1, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(69, 44, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(70, 45, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(71, 46, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(72, 47, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(73, 48, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Kennziffern für Korrekturmaßnahmen (K.M.)\r\n1: Temperatur erhöht bzw. reduziert\r\n2: Garzeit bzw. Kühlzeit verlängert\r\n3: kleine Menge in ausgabe bereitgestellt\r\n4: Produkt vernichtet', 11),
(74, 49, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(75, 50, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(76, 51, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(77, 52, 1, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(78, 53, 1, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(79, 54, 1, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(80, 61, 1, 'Tage', 1, 'Anfangsdatum', '2016-01-01', NULL, 11),
(81, 54, 2, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(82, 54, 3, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(83, 54, 4, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(84, 26, 12, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(85, 26, 13, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(86, 26, 14, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(87, 26, 15, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(88, 26, 16, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 0),
(91, 14, 5, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(92, 14, 6, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(95, 17, 5, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(96, 18, 4, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(97, 18, 5, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(98, 13, 8, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(99, 13, 9, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(100, 17, 6, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(101, 17, 7, 'Tage', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(102, 17, 8, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(103, 17, 9, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(104, 18, 6, 'Tage', 1, 'Aktuellem-Datum', NULL, NULL, 11),
(105, 18, 7, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(106, 13, 10, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(107, 13, 11, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(108, 24, 3, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(109, 13, 12, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(110, 24, 4, 'Tage', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(111, 13, 13, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(112, 13, 14, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(113, 13, 15, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(114, 24, 5, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(115, 13, 16, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(116, 13, 17, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(117, 24, 6, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', NULL, 11),
(118, 17, 10, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(119, 17, 11, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(120, 14, 7, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(121, 14, 8, 'Wochen', 1, 'Aktuellem-Datum', NULL, 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(122, 14, 9, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(123, 14, 10, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'This is the english translated Footnote.', 11),
(124, 14, 11, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'This is the english translated Footnote placeholder Text.', 11),
(125, 14, 12, 'Wochen', 1, 'Anfangsdatum', '2016-01-04', 'This is the english translated footnote placeholder text.', 11),
(126, 17, 12, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(127, 17, 13, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11),
(128, 17, 14, 'Tage', 1, 'Anfangsdatum', '2016-01-04', 'Alle Preise inkl. 19% MwSt. | Wir verwenden jodiertes Speisesalz\r\nÄnderungen vorbehalten', 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VirtualPage`
--

CREATE TABLE `VirtualPage` (
  `ID` int(11) NOT NULL,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VirtualPage_Live`
--

CREATE TABLE `VirtualPage_Live` (
  `ID` int(11) NOT NULL,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VirtualPage_versions`
--

CREATE TABLE `VirtualPage_versions` (
  `ID` int(11) NOT NULL,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VisitorCounterData`
--

CREATE TABLE `VisitorCounterData` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('VisitorCounterData') DEFAULT 'VisitorCounterData',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `Time` time DEFAULT NULL,
  `MaxVisitors` int(11) NOT NULL DEFAULT '0',
  `MaxVisitorsPercent` int(11) NOT NULL DEFAULT '0',
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `VisitorCounterPredictionData`
--

CREATE TABLE `VisitorCounterPredictionData` (
  `ID` int(11) NOT NULL,
  `ClassName` enum('VisitorCounterPredictionData') DEFAULT 'VisitorCounterPredictionData',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Time` time DEFAULT NULL,
  `MaxVisitors` int(11) NOT NULL DEFAULT '0',
  `MaxVisitorsPercent` int(11) NOT NULL DEFAULT '0',
  `WeekdayNum` int(11) NOT NULL DEFAULT '0',
  `Month` int(11) NOT NULL DEFAULT '0',
  `Year` int(11) NOT NULL DEFAULT '0',
  `PredictionBase` enum('LastWeek','Continous','SameMonthLastYear') DEFAULT 'Continous',
  `PredictionDataItemCount` int(11) NOT NULL DEFAULT '0',
  `LastAddedDateTime` datetime DEFAULT NULL,
  `ImportVisitorCounterConfigID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `AdditivesBlacklistConfig`
--
ALTER TABLE `AdditivesBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `AdditivesBlacklistObject`
--
ALTER TABLE `AdditivesBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `AdditivesBlacklistConfigID` (`AdditivesBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `AllergensBlacklistConfig`
--
ALTER TABLE `AllergensBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `AllergensBlacklistObject`
--
ALTER TABLE `AllergensBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `AllergensBlacklistConfigID` (`AllergensBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DailyMenuPage`
--
ALTER TABLE `DailyMenuPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DailyMenuPage_Dishes`
--
ALTER TABLE `DailyMenuPage_Dishes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DailyMenuPageID` (`DailyMenuPageID`),
  ADD KEY `DishID` (`DishID`);

--
-- Indizes für die Tabelle `DailyMenuPage_Live`
--
ALTER TABLE `DailyMenuPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DailyMenuPage_versions`
--
ALTER TABLE `DailyMenuPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `Dish`
--
ALTER TABLE `Dish`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `OrigDishID` (`OrigDishID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishesPerRestaurantDishExtensionPermissions`
--
ALTER TABLE `DishesPerRestaurantDishExtensionPermissions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishLabelsBlacklistConfig`
--
ALTER TABLE `DishLabelsBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishLabelsBlacklistObject`
--
ALTER TABLE `DishLabelsBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishLabelsBlacklistConfigID` (`DishLabelsBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishRating`
--
ALTER TABLE `DishRating`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishVotingItem`
--
ALTER TABLE `DishVotingItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishVotingPageID` (`DishVotingPageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DishVotingPage`
--
ALTER TABLE `DishVotingPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DishVotingPage_Live`
--
ALTER TABLE `DishVotingPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `DishVotingPage_versions`
--
ALTER TABLE `DishVotingPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `Dish_GlobalAdditives`
--
ALTER TABLE `Dish_GlobalAdditives`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalAdditiveID` (`GlobalAdditiveID`);

--
-- Indizes für die Tabelle `Dish_GlobalAllergens`
--
ALTER TABLE `Dish_GlobalAllergens`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalAllergenID` (`GlobalAllergenID`);

--
-- Indizes für die Tabelle `Dish_GlobalDishLabels`
--
ALTER TABLE `Dish_GlobalDishLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalDishLabelID` (`GlobalDishLabelID`);

--
-- Indizes für die Tabelle `Dish_GlobalSpecialLabels`
--
ALTER TABLE `Dish_GlobalSpecialLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `GlobalSpecialLabelID` (`GlobalSpecialLabelID`);

--
-- Indizes für die Tabelle `Dish_SubsiteAdditives`
--
ALTER TABLE `Dish_SubsiteAdditives`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteAdditiveID` (`SubsiteAdditiveID`);

--
-- Indizes für die Tabelle `Dish_SubsiteAllergens`
--
ALTER TABLE `Dish_SubsiteAllergens`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteAllergenID` (`SubsiteAllergenID`);

--
-- Indizes für die Tabelle `Dish_SubsiteDishLabels`
--
ALTER TABLE `Dish_SubsiteDishLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteDishLabelID` (`SubsiteDishLabelID`);

--
-- Indizes für die Tabelle `Dish_SubsiteSpecialLabels`
--
ALTER TABLE `Dish_SubsiteSpecialLabels`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SubsiteSpecialLabelID` (`SubsiteSpecialLabelID`);

--
-- Indizes für die Tabelle `DisplayTemplateObject`
--
ALTER TABLE `DisplayTemplateObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `DisplayMultiTemplatePageID` (`DisplayMultiTemplatePageID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DisplayTemplateObject_FoodCategories`
--
ALTER TABLE `DisplayTemplateObject_FoodCategories`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplateObjectID` (`DisplayTemplateObjectID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`);

--
-- Indizes für die Tabelle `DisplayTemplateObject_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplateObject_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplateObjectID` (`DisplayTemplateObjectID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage`
--
ALTER TABLE `DisplayTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage_Live`
--
ALTER TABLE `DisplayTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplatePage_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePageID` (`DisplayTemplatePageID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `DisplayTemplatePage_versions`
--
ALTER TABLE `DisplayTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `DisplayVideo`
--
ALTER TABLE `DisplayVideo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `VideoFileWebMID` (`VideoFileWebMID`),
  ADD KEY `DisplayTemplatePageID` (`DisplayTemplatePageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `DisplayVideo_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayVideo_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayVideoID` (`DisplayVideoID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `ErrorPage`
--
ALTER TABLE `ErrorPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `ErrorPage_Live`
--
ALTER TABLE `ErrorPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `ErrorPage_versions`
--
ALTER TABLE `ErrorPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `ExportConfig`
--
ALTER TABLE `ExportConfig`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `ExportDisplayMenuXmlConfig`
--
ALTER TABLE `ExportDisplayMenuXmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePageID` (`DisplayTemplatePageID`);

--
-- Indizes für die Tabelle `ExportMenuHtmlConfig`
--
ALTER TABLE `ExportMenuHtmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `ExportMenuHtmlConfig_FoodCategories`
--
ALTER TABLE `ExportMenuHtmlConfig_FoodCategories`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ExportMenuHtmlConfigID` (`ExportMenuHtmlConfigID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`);

--
-- Indizes für die Tabelle `ExportMenuXmlConfig`
--
ALTER TABLE `ExportMenuXmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `File`
--
ALTER TABLE `File`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `OwnerID` (`OwnerID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `File_DeleterGroups`
--
ALTER TABLE `File_DeleterGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `File_EditorFolderGroups`
--
ALTER TABLE `File_EditorFolderGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `File_UploaderGroups`
--
ALTER TABLE `File_UploaderGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `File_ViewerGroups`
--
ALTER TABLE `File_ViewerGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FileID` (`FileID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `FoodCategory`
--
ALTER TABLE `FoodCategory`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `FoodCategoryBlacklistConfig`
--
ALTER TABLE `FoodCategoryBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `FoodCategoryBlacklistObject`
--
ALTER TABLE `FoodCategoryBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FoodCategoryBlacklistConfigID` (`FoodCategoryBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalAdditive`
--
ALTER TABLE `GlobalAdditive`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalAllergen`
--
ALTER TABLE `GlobalAllergen`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalDishLabel`
--
ALTER TABLE `GlobalDishLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `GlobalSpecialLabel`
--
ALTER TABLE `GlobalSpecialLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Group`
--
ALTER TABLE `Group`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Group_Members`
--
ALTER TABLE `Group_Members`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Indizes für die Tabelle `Group_Roles`
--
ALTER TABLE `Group_Roles`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `PermissionRoleID` (`PermissionRoleID`);

--
-- Indizes für die Tabelle `Group_Subsites`
--
ALTER TABLE `Group_Subsites`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `SubsiteID` (`SubsiteID`);

--
-- Indizes für die Tabelle `ImportConfig`
--
ALTER TABLE `ImportConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NutritivesMapConfigID` (`NutritivesMapConfigID`),
  ADD KEY `FoodCategoryBlacklistConfigID` (`FoodCategoryBlacklistConfigID`),
  ADD KEY `AdditivesBlacklistConfigID` (`AdditivesBlacklistConfigID`),
  ADD KEY `AllergensBlacklistConfigID` (`AllergensBlacklistConfigID`),
  ADD KEY `DishLabelsBlacklistConfigID` (`DishLabelsBlacklistConfigID`),
  ADD KEY `SpecialLabelsBlacklistConfigID` (`SpecialLabelsBlacklistConfigID`);

--
-- Indizes für die Tabelle `ImportFilterConfig`
--
ALTER TABLE `ImportFilterConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `ImportMenuXmlConfig`
--
ALTER TABLE `ImportMenuXmlConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `ImportRestaurantConfig`
--
ALTER TABLE `ImportRestaurantConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`);

--
-- Indizes für die Tabelle `ImportVisitorCounterConfig`
--
ALTER TABLE `ImportVisitorCounterConfig`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `InfoMessage`
--
ALTER TABLE `InfoMessage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `InfoMessageDisplayTemplatePageLandscape`
--
ALTER TABLE `InfoMessageDisplayTemplatePageLandscape`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePageLandscapeID` (`DisplayTemplatePageLandscapeID`),
  ADD KEY `InfoMessageTemplateDisplayTemplatePageLandscapeID` (`InfoMessageTemplateDisplayTemplatePageLandscapeID`);

--
-- Indizes für die Tabelle `InfoMessageDisplayTemplatePagePortrait`
--
ALTER TABLE `InfoMessageDisplayTemplatePagePortrait`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `DisplayTemplatePagePortraitID` (`DisplayTemplatePagePortraitID`),
  ADD KEY `InfoMessageTemplateDisplayTemplatePagePortraitID` (`InfoMessageTemplateDisplayTemplatePagePortraitID`);

--
-- Indizes für die Tabelle `InfoMessageMicrositeHomePage`
--
ALTER TABLE `InfoMessageMicrositeHomePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplateMicrositeHomePageID` (`InfoMessageTemplateMicrositeHomePageID`),
  ADD KEY `MicrositeHomePageID` (`MicrositeHomePageID`),
  ADD KEY `LinkToID` (`LinkToID`),
  ADD KEY `DownloadID` (`DownloadID`);

--
-- Indizes für die Tabelle `InfoMessageMicrositeNewsPage`
--
ALTER TABLE `InfoMessageMicrositeNewsPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplateMicrositeNewsPageID` (`InfoMessageTemplateMicrositeNewsPageID`),
  ADD KEY `MicrositeNewsPageID` (`MicrositeNewsPageID`);

--
-- Indizes für die Tabelle `InfoMessageMobileTemplatePage`
--
ALTER TABLE `InfoMessageMobileTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplateMobileTemplatePageID` (`InfoMessageTemplateMobileTemplatePageID`),
  ADD KEY `MobileTemplatePageID` (`MobileTemplatePageID`);

--
-- Indizes für die Tabelle `InfoMessagePrintTemplatePage`
--
ALTER TABLE `InfoMessagePrintTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageTemplatePrintTemplatePageID` (`InfoMessageTemplatePrintTemplatePageID`),
  ADD KEY `PrintTemplatePageID` (`PrintTemplatePageID`),
  ADD KEY `PrintHtmlTemplatePageID` (`PrintHtmlTemplatePageID`);

--
-- Indizes für die Tabelle `InfoMessage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `InfoMessage_TimerRecurringDaysOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `InfoMessageID` (`InfoMessageID`),
  ADD KEY `RecurringDayOfWeekID` (`RecurringDayOfWeekID`);

--
-- Indizes für die Tabelle `LoginAttempt`
--
ALTER TABLE `LoginAttempt`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Member`
--
ALTER TABLE `Member`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Email` (`Email`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MemberPassword`
--
ALTER TABLE `MemberPassword`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MenuContainer`
--
ALTER TABLE `MenuContainer`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MenuContainer_Live`
--
ALTER TABLE `MenuContainer_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MenuContainer_versions`
--
ALTER TABLE `MenuContainer_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeContainer`
--
ALTER TABLE `MicrositeContainer`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeContainer_Live`
--
ALTER TABLE `MicrositeContainer_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeContainer_versions`
--
ALTER TABLE `MicrositeContainer_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeContentItem`
--
ALTER TABLE `MicrositeContentItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MicrositePageID` (`MicrositePageID`),
  ADD KEY `MicrositeHomePageID` (`MicrositeHomePageID`),
  ADD KEY `LinkToID` (`LinkToID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MicrositeHomePage`
--
ALTER TABLE `MicrositeHomePage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeHomePage_Live`
--
ALTER TABLE `MicrositeHomePage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeHomePage_versions`
--
ALTER TABLE `MicrositeHomePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeLinkItem`
--
ALTER TABLE `MicrositeLinkItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MicrositePageID` (`MicrositePageID`),
  ADD KEY `LinkToID` (`LinkToID`),
  ADD KEY `DownloadID` (`DownloadID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `MicrositePage`
--
ALTER TABLE `MicrositePage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositePage_Live`
--
ALTER TABLE `MicrositePage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositePage_versions`
--
ALTER TABLE `MicrositePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MicrositeRedirectorPage`
--
ALTER TABLE `MicrositeRedirectorPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeRedirectorPage_Live`
--
ALTER TABLE `MicrositeRedirectorPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `MicrositeRedirectorPage_versions`
--
ALTER TABLE `MicrositeRedirectorPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `MobileInfoPage`
--
ALTER TABLE `MobileInfoPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`);

--
-- Indizes für die Tabelle `MobileInfoPage_Live`
--
ALTER TABLE `MobileInfoPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImageID` (`ImageID`);

--
-- Indizes für die Tabelle `MobileInfoPage_versions`
--
ALTER TABLE `MobileInfoPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `ImageID` (`ImageID`);

--
-- Indizes für die Tabelle `MobileTemplatePage`
--
ALTER TABLE `MobileTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `MobileTemplatePage_Live`
--
ALTER TABLE `MobileTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `MobileTemplatePage_MenuContainers`
--
ALTER TABLE `MobileTemplatePage_MenuContainers`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MobileTemplatePageID` (`MobileTemplatePageID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `MobileTemplatePage_versions`
--
ALTER TABLE `MobileTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`);

--
-- Indizes für die Tabelle `MySiteConfigExtensionPermissions`
--
ALTER TABLE `MySiteConfigExtensionPermissions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `NutritivesMapConfig`
--
ALTER TABLE `NutritivesMapConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `NutritivesMapObject`
--
ALTER TABLE `NutritivesMapObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NutritivesMapConfigID` (`NutritivesMapConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Permission`
--
ALTER TABLE `Permission`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `GroupID` (`GroupID`),
  ADD KEY `Code` (`Code`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PermissionRole`
--
ALTER TABLE `PermissionRole`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PermissionRoleCode`
--
ALTER TABLE `PermissionRoleCode`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RoleID` (`RoleID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PrintHtmlTemplatePage`
--
ALTER TABLE `PrintHtmlTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `HeaderImageID` (`HeaderImageID`);

--
-- Indizes für die Tabelle `PrintHtmlTemplatePage_Live`
--
ALTER TABLE `PrintHtmlTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `HeaderImageID` (`HeaderImageID`);

--
-- Indizes für die Tabelle `PrintHtmlTemplatePage_versions`
--
ALTER TABLE `PrintHtmlTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `HeaderImageID` (`HeaderImageID`);

--
-- Indizes für die Tabelle `PrintManagerConfig`
--
ALTER TABLE `PrintManagerConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`),
  ADD KEY `PrintManagerTemplateID` (`PrintManagerTemplateID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PrintManagerConfig_Dishes`
--
ALTER TABLE `PrintManagerConfig_Dishes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `PrintManagerConfigID` (`PrintManagerConfigID`),
  ADD KEY `DishID` (`DishID`);

--
-- Indizes für die Tabelle `PrintManagerTemplate`
--
ALTER TABLE `PrintManagerTemplate`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `PrintTemplatePage`
--
ALTER TABLE `PrintTemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `WeeklyTableHeaderImageID` (`WeeklyTableHeaderImageID`);

--
-- Indizes für die Tabelle `PrintTemplatePage_Live`
--
ALTER TABLE `PrintTemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `WeeklyTableHeaderImageID` (`WeeklyTableHeaderImageID`);

--
-- Indizes für die Tabelle `PrintTemplatePage_versions`
--
ALTER TABLE `PrintTemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `WeeklyTableHeaderImageID` (`WeeklyTableHeaderImageID`);

--
-- Indizes für die Tabelle `ProcessTaskObject`
--
ALTER TABLE `ProcessTaskObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RecipeSubmissionItem`
--
ALTER TABLE `RecipeSubmissionItem`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RecipeSubmissionPageID` (`RecipeSubmissionPageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RecipeSubmissionPage`
--
ALTER TABLE `RecipeSubmissionPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `RecipeSubmissionPage_Live`
--
ALTER TABLE `RecipeSubmissionPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `RecipeSubmissionPage_versions`
--
ALTER TABLE `RecipeSubmissionPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `RecurringDayOfWeek`
--
ALTER TABLE `RecurringDayOfWeek`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RedirectorPage`
--
ALTER TABLE `RedirectorPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `LinkToID` (`LinkToID`);

--
-- Indizes für die Tabelle `RedirectorPage_Live`
--
ALTER TABLE `RedirectorPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `LinkToID` (`LinkToID`);

--
-- Indizes für die Tabelle `RedirectorPage_versions`
--
ALTER TABLE `RedirectorPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `LinkToID` (`LinkToID`);

--
-- Indizes für die Tabelle `RestaurantDish`
--
ALTER TABLE `RestaurantDish`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantPageID` (`RestaurantPageID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`),
  ADD KEY `DishID` (`DishID`),
  ADD KEY `SideDish1ID` (`SideDish1ID`),
  ADD KEY `SideDish2ID` (`SideDish2ID`),
  ADD KEY `SideDish3ID` (`SideDish3ID`),
  ADD KEY `SideDish4ID` (`SideDish4ID`),
  ADD KEY `SideDish5ID` (`SideDish5ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `RestaurantPage`
--
ALTER TABLE `RestaurantPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantLogoID` (`RestaurantLogoID`);

--
-- Indizes für die Tabelle `RestaurantPage_Live`
--
ALTER TABLE `RestaurantPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RestaurantLogoID` (`RestaurantLogoID`);

--
-- Indizes für die Tabelle `RestaurantPage_versions`
--
ALTER TABLE `RestaurantPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `RestaurantLogoID` (`RestaurantLogoID`);

--
-- Indizes für die Tabelle `SiteConfig`
--
ALTER TABLE `SiteConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteConfig_CreateTopLevelGroups`
--
ALTER TABLE `SiteConfig_CreateTopLevelGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteConfigID` (`SiteConfigID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteConfig_EditorGroups`
--
ALTER TABLE `SiteConfig_EditorGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteConfigID` (`SiteConfigID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteConfig_ViewerGroups`
--
ALTER TABLE `SiteConfig_ViewerGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteConfigID` (`SiteConfigID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteTree`
--
ALTER TABLE `SiteTree`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `URLSegment` (`URLSegment`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteTree_CrossSubsiteLinkTracking`
--
ALTER TABLE `SiteTree_CrossSubsiteLinkTracking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `ChildID` (`ChildID`);

--
-- Indizes für die Tabelle `SiteTree_EditorGroups`
--
ALTER TABLE `SiteTree_EditorGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SiteTree_ImageTracking`
--
ALTER TABLE `SiteTree_ImageTracking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `FileID` (`FileID`);

--
-- Indizes für die Tabelle `SiteTree_LinkTracking`
--
ALTER TABLE `SiteTree_LinkTracking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `ChildID` (`ChildID`);

--
-- Indizes für die Tabelle `SiteTree_Live`
--
ALTER TABLE `SiteTree_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `URLSegment` (`URLSegment`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteTree_versions`
--
ALTER TABLE `SiteTree_versions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `AuthorID` (`AuthorID`),
  ADD KEY `PublisherID` (`PublisherID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ParentID` (`ParentID`),
  ADD KEY `URLSegment` (`URLSegment`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SiteTree_ViewerGroups`
--
ALTER TABLE `SiteTree_ViewerGroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SiteTreeID` (`SiteTreeID`),
  ADD KEY `GroupID` (`GroupID`);

--
-- Indizes für die Tabelle `SpecialLabelsBlacklistConfig`
--
ALTER TABLE `SpecialLabelsBlacklistConfig`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SpecialLabelsBlacklistObject`
--
ALTER TABLE `SpecialLabelsBlacklistObject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SpecialLabelsBlacklistConfigID` (`SpecialLabelsBlacklistConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `Subsite`
--
ALTER TABLE `Subsite`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteAdditive`
--
ALTER TABLE `SubsiteAdditive`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteAllergen`
--
ALTER TABLE `SubsiteAllergen`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteDishLabel`
--
ALTER TABLE `SubsiteDishLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteDomain`
--
ALTER TABLE `SubsiteDomain`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsiteSpecialLabel`
--
ALTER TABLE `SubsiteSpecialLabel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SubsiteID` (`SubsiteID`),
  ADD KEY `ImageID` (`ImageID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `SubsitesVirtualPage`
--
ALTER TABLE `SubsitesVirtualPage`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `SubsitesVirtualPage_Live`
--
ALTER TABLE `SubsitesVirtualPage_Live`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `SubsitesVirtualPage_versions`
--
ALTER TABLE `SubsitesVirtualPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`);

--
-- Indizes für die Tabelle `TemplatePage`
--
ALTER TABLE `TemplatePage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `TemplatePage_FoodCategories`
--
ALTER TABLE `TemplatePage_FoodCategories`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TemplatePageID` (`TemplatePageID`),
  ADD KEY `FoodCategoryID` (`FoodCategoryID`);

--
-- Indizes für die Tabelle `TemplatePage_Live`
--
ALTER TABLE `TemplatePage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `TemplatePage_versions`
--
ALTER TABLE `TemplatePage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `MenuContainerID` (`MenuContainerID`);

--
-- Indizes für die Tabelle `VirtualPage`
--
ALTER TABLE `VirtualPage`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CopyContentFromID` (`CopyContentFromID`);

--
-- Indizes für die Tabelle `VirtualPage_Live`
--
ALTER TABLE `VirtualPage_Live`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CopyContentFromID` (`CopyContentFromID`);

--
-- Indizes für die Tabelle `VirtualPage_versions`
--
ALTER TABLE `VirtualPage_versions`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  ADD KEY `RecordID` (`RecordID`),
  ADD KEY `Version` (`Version`),
  ADD KEY `CopyContentFromID` (`CopyContentFromID`);

--
-- Indizes für die Tabelle `VisitorCounterData`
--
ALTER TABLE `VisitorCounterData`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- Indizes für die Tabelle `VisitorCounterPredictionData`
--
ALTER TABLE `VisitorCounterPredictionData`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ImportVisitorCounterConfigID` (`ImportVisitorCounterConfigID`),
  ADD KEY `ClassName` (`ClassName`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `AdditivesBlacklistConfig`
--
ALTER TABLE `AdditivesBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `AdditivesBlacklistObject`
--
ALTER TABLE `AdditivesBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `AllergensBlacklistConfig`
--
ALTER TABLE `AllergensBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `AllergensBlacklistObject`
--
ALTER TABLE `AllergensBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage`
--
ALTER TABLE `DailyMenuPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage_Dishes`
--
ALTER TABLE `DailyMenuPage_Dishes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage_Live`
--
ALTER TABLE `DailyMenuPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT für Tabelle `DailyMenuPage_versions`
--
ALTER TABLE `DailyMenuPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `Dish`
--
ALTER TABLE `Dish`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `DishesPerRestaurantDishExtensionPermissions`
--
ALTER TABLE `DishesPerRestaurantDishExtensionPermissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishLabelsBlacklistConfig`
--
ALTER TABLE `DishLabelsBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishLabelsBlacklistObject`
--
ALTER TABLE `DishLabelsBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DishRating`
--
ALTER TABLE `DishRating`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT für Tabelle `DishVotingItem`
--
ALTER TABLE `DishVotingItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `DishVotingPage`
--
ALTER TABLE `DishVotingPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT für Tabelle `DishVotingPage_Live`
--
ALTER TABLE `DishVotingPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT für Tabelle `DishVotingPage_versions`
--
ALTER TABLE `DishVotingPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalAdditives`
--
ALTER TABLE `Dish_GlobalAdditives`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalAllergens`
--
ALTER TABLE `Dish_GlobalAllergens`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalDishLabels`
--
ALTER TABLE `Dish_GlobalDishLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `Dish_GlobalSpecialLabels`
--
ALTER TABLE `Dish_GlobalSpecialLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteAdditives`
--
ALTER TABLE `Dish_SubsiteAdditives`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteAllergens`
--
ALTER TABLE `Dish_SubsiteAllergens`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteDishLabels`
--
ALTER TABLE `Dish_SubsiteDishLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `Dish_SubsiteSpecialLabels`
--
ALTER TABLE `Dish_SubsiteSpecialLabels`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplateObject`
--
ALTER TABLE `DisplayTemplateObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplateObject_FoodCategories`
--
ALTER TABLE `DisplayTemplateObject_FoodCategories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplateObject_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplateObject_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage`
--
ALTER TABLE `DisplayTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage_Live`
--
ALTER TABLE `DisplayTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayTemplatePage_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayTemplatePage_versions`
--
ALTER TABLE `DisplayTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT für Tabelle `DisplayVideo`
--
ALTER TABLE `DisplayVideo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `DisplayVideo_TimerRecurringDaysOfWeek`
--
ALTER TABLE `DisplayVideo_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ErrorPage`
--
ALTER TABLE `ErrorPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT für Tabelle `ErrorPage_Live`
--
ALTER TABLE `ErrorPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT für Tabelle `ErrorPage_versions`
--
ALTER TABLE `ErrorPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `ExportConfig`
--
ALTER TABLE `ExportConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportDisplayMenuXmlConfig`
--
ALTER TABLE `ExportDisplayMenuXmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportMenuHtmlConfig`
--
ALTER TABLE `ExportMenuHtmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportMenuHtmlConfig_FoodCategories`
--
ALTER TABLE `ExportMenuHtmlConfig_FoodCategories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ExportMenuXmlConfig`
--
ALTER TABLE `ExportMenuXmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `File`
--
ALTER TABLE `File`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT für Tabelle `File_DeleterGroups`
--
ALTER TABLE `File_DeleterGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT für Tabelle `File_EditorFolderGroups`
--
ALTER TABLE `File_EditorFolderGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `File_UploaderGroups`
--
ALTER TABLE `File_UploaderGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT für Tabelle `File_ViewerGroups`
--
ALTER TABLE `File_ViewerGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `FoodCategory`
--
ALTER TABLE `FoodCategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT für Tabelle `FoodCategoryBlacklistConfig`
--
ALTER TABLE `FoodCategoryBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `FoodCategoryBlacklistObject`
--
ALTER TABLE `FoodCategoryBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `GlobalAdditive`
--
ALTER TABLE `GlobalAdditive`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT für Tabelle `GlobalAllergen`
--
ALTER TABLE `GlobalAllergen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT für Tabelle `GlobalDishLabel`
--
ALTER TABLE `GlobalDishLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `GlobalSpecialLabel`
--
ALTER TABLE `GlobalSpecialLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `Group`
--
ALTER TABLE `Group`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `Group_Members`
--
ALTER TABLE `Group_Members`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `Group_Roles`
--
ALTER TABLE `Group_Roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `Group_Subsites`
--
ALTER TABLE `Group_Subsites`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportConfig`
--
ALTER TABLE `ImportConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportFilterConfig`
--
ALTER TABLE `ImportFilterConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportMenuXmlConfig`
--
ALTER TABLE `ImportMenuXmlConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportRestaurantConfig`
--
ALTER TABLE `ImportRestaurantConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `ImportVisitorCounterConfig`
--
ALTER TABLE `ImportVisitorCounterConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessage`
--
ALTER TABLE `InfoMessage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageDisplayTemplatePageLandscape`
--
ALTER TABLE `InfoMessageDisplayTemplatePageLandscape`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageDisplayTemplatePagePortrait`
--
ALTER TABLE `InfoMessageDisplayTemplatePagePortrait`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageMicrositeHomePage`
--
ALTER TABLE `InfoMessageMicrositeHomePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageMicrositeNewsPage`
--
ALTER TABLE `InfoMessageMicrositeNewsPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `InfoMessageMobileTemplatePage`
--
ALTER TABLE `InfoMessageMobileTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessagePrintTemplatePage`
--
ALTER TABLE `InfoMessagePrintTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `InfoMessage_TimerRecurringDaysOfWeek`
--
ALTER TABLE `InfoMessage_TimerRecurringDaysOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `LoginAttempt`
--
ALTER TABLE `LoginAttempt`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Member`
--
ALTER TABLE `Member`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MemberPassword`
--
ALTER TABLE `MemberPassword`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MenuContainer`
--
ALTER TABLE `MenuContainer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT für Tabelle `MenuContainer_Live`
--
ALTER TABLE `MenuContainer_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT für Tabelle `MenuContainer_versions`
--
ALTER TABLE `MenuContainer_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContainer`
--
ALTER TABLE `MicrositeContainer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContainer_Live`
--
ALTER TABLE `MicrositeContainer_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContainer_versions`
--
ALTER TABLE `MicrositeContainer_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `MicrositeContentItem`
--
ALTER TABLE `MicrositeContentItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `MicrositeHomePage`
--
ALTER TABLE `MicrositeHomePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT für Tabelle `MicrositeHomePage_Live`
--
ALTER TABLE `MicrositeHomePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT für Tabelle `MicrositeHomePage_versions`
--
ALTER TABLE `MicrositeHomePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `MicrositeLinkItem`
--
ALTER TABLE `MicrositeLinkItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `MicrositePage`
--
ALTER TABLE `MicrositePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT für Tabelle `MicrositePage_Live`
--
ALTER TABLE `MicrositePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT für Tabelle `MicrositePage_versions`
--
ALTER TABLE `MicrositePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `MicrositeRedirectorPage`
--
ALTER TABLE `MicrositeRedirectorPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `MicrositeRedirectorPage_Live`
--
ALTER TABLE `MicrositeRedirectorPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `MicrositeRedirectorPage_versions`
--
ALTER TABLE `MicrositeRedirectorPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `MobileInfoPage`
--
ALTER TABLE `MobileInfoPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT für Tabelle `MobileInfoPage_Live`
--
ALTER TABLE `MobileInfoPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT für Tabelle `MobileInfoPage_versions`
--
ALTER TABLE `MobileInfoPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage`
--
ALTER TABLE `MobileTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage_Live`
--
ALTER TABLE `MobileTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage_MenuContainers`
--
ALTER TABLE `MobileTemplatePage_MenuContainers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `MobileTemplatePage_versions`
--
ALTER TABLE `MobileTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT für Tabelle `MySiteConfigExtensionPermissions`
--
ALTER TABLE `MySiteConfigExtensionPermissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `NutritivesMapConfig`
--
ALTER TABLE `NutritivesMapConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `NutritivesMapObject`
--
ALTER TABLE `NutritivesMapObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Permission`
--
ALTER TABLE `Permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT für Tabelle `PermissionRole`
--
ALTER TABLE `PermissionRole`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `PermissionRoleCode`
--
ALTER TABLE `PermissionRoleCode`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2153;
--
-- AUTO_INCREMENT für Tabelle `PrintHtmlTemplatePage`
--
ALTER TABLE `PrintHtmlTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `PrintHtmlTemplatePage_Live`
--
ALTER TABLE `PrintHtmlTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `PrintHtmlTemplatePage_versions`
--
ALTER TABLE `PrintHtmlTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT für Tabelle `PrintManagerConfig`
--
ALTER TABLE `PrintManagerConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintManagerConfig_Dishes`
--
ALTER TABLE `PrintManagerConfig_Dishes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintManagerTemplate`
--
ALTER TABLE `PrintManagerTemplate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PrintTemplatePage`
--
ALTER TABLE `PrintTemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT für Tabelle `PrintTemplatePage_Live`
--
ALTER TABLE `PrintTemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT für Tabelle `PrintTemplatePage_versions`
--
ALTER TABLE `PrintTemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT für Tabelle `ProcessTaskObject`
--
ALTER TABLE `ProcessTaskObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionItem`
--
ALTER TABLE `RecipeSubmissionItem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionPage`
--
ALTER TABLE `RecipeSubmissionPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionPage_Live`
--
ALTER TABLE `RecipeSubmissionPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT für Tabelle `RecipeSubmissionPage_versions`
--
ALTER TABLE `RecipeSubmissionPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RecurringDayOfWeek`
--
ALTER TABLE `RecurringDayOfWeek`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `RedirectorPage`
--
ALTER TABLE `RedirectorPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RedirectorPage_Live`
--
ALTER TABLE `RedirectorPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RedirectorPage_versions`
--
ALTER TABLE `RedirectorPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `RestaurantDish`
--
ALTER TABLE `RestaurantDish`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `RestaurantPage`
--
ALTER TABLE `RestaurantPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT für Tabelle `RestaurantPage_Live`
--
ALTER TABLE `RestaurantPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT für Tabelle `RestaurantPage_versions`
--
ALTER TABLE `RestaurantPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig`
--
ALTER TABLE `SiteConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig_CreateTopLevelGroups`
--
ALTER TABLE `SiteConfig_CreateTopLevelGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig_EditorGroups`
--
ALTER TABLE `SiteConfig_EditorGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `SiteConfig_ViewerGroups`
--
ALTER TABLE `SiteConfig_ViewerGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree`
--
ALTER TABLE `SiteTree`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_CrossSubsiteLinkTracking`
--
ALTER TABLE `SiteTree_CrossSubsiteLinkTracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_EditorGroups`
--
ALTER TABLE `SiteTree_EditorGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_ImageTracking`
--
ALTER TABLE `SiteTree_ImageTracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_LinkTracking`
--
ALTER TABLE `SiteTree_LinkTracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_Live`
--
ALTER TABLE `SiteTree_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_versions`
--
ALTER TABLE `SiteTree_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT für Tabelle `SiteTree_ViewerGroups`
--
ALTER TABLE `SiteTree_ViewerGroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SpecialLabelsBlacklistConfig`
--
ALTER TABLE `SpecialLabelsBlacklistConfig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SpecialLabelsBlacklistObject`
--
ALTER TABLE `SpecialLabelsBlacklistObject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Subsite`
--
ALTER TABLE `Subsite`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `SubsiteAdditive`
--
ALTER TABLE `SubsiteAdditive`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `SubsiteAllergen`
--
ALTER TABLE `SubsiteAllergen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT für Tabelle `SubsiteDishLabel`
--
ALTER TABLE `SubsiteDishLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `SubsiteDomain`
--
ALTER TABLE `SubsiteDomain`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `SubsiteSpecialLabel`
--
ALTER TABLE `SubsiteSpecialLabel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsitesVirtualPage`
--
ALTER TABLE `SubsitesVirtualPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsitesVirtualPage_Live`
--
ALTER TABLE `SubsitesVirtualPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `SubsitesVirtualPage_versions`
--
ALTER TABLE `SubsitesVirtualPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage`
--
ALTER TABLE `TemplatePage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage_FoodCategories`
--
ALTER TABLE `TemplatePage_FoodCategories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage_Live`
--
ALTER TABLE `TemplatePage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT für Tabelle `TemplatePage_versions`
--
ALTER TABLE `TemplatePage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT für Tabelle `VirtualPage`
--
ALTER TABLE `VirtualPage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VirtualPage_Live`
--
ALTER TABLE `VirtualPage_Live`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VirtualPage_versions`
--
ALTER TABLE `VirtualPage_versions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VisitorCounterData`
--
ALTER TABLE `VisitorCounterData`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `VisitorCounterPredictionData`
--
ALTER TABLE `VisitorCounterPredictionData`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
